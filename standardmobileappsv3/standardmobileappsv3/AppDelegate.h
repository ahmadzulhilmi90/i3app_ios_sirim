//
//  AppDelegate.h
//  standardmobileappsv3
//
//  Created by M3Online on 4/24/14.
//  Copyright (c) 2014 M3Online. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <sqlite3.h>
#import "SettingViewController.h"

//version 1.0 :
//@interface AppDelegate : UIResponder <UIApplicationDelegate, CLLocationManagerDelegate> {
    
@interface AppDelegate : UIResponder <UIApplicationDelegate> {
    NSString *apipath;
    NSString *email;
    NSString *user;
    NSString *db;
    
    NSString *databaseName;
    NSString *databasePath;
    
    NSString *nextPage;
    NSString *plugin_id;
    NSString *themeColorCode;
    NSString *themeColorCode1;
    NSString *customer_id;
    NSString *iconname;
    NSString *homepage;
    NSString *backgroundT;
    
    NSString *user_apipath;
    UIApplication *app;
    NSString *uuid;
    
    NSString *event_id;
    
    int beaconFoundTimer;
    
    int intervalNotification;
    
    BOOL ibeaconfrommenu;
    BOOL firsttime;
    
    NSString *cs;
    NSString *QRUrl;
    
    NSString *latitude;
    NSString *longitude;
    
    //BOOL pushNotification;
    
    //NSMutableData* receivedData;
    
    NSString *currentRequest;
    
    /*** Push Notification 27Aug2015 ***/
    NSString *notify_actiontype;
    NSString *notify_actionvalue;
    NSString *notify_contentid;
    /*** Push Notification 27Aug2015 ***/
    
    //Slide Menu (vertical)
    NSMutableArray *listOfVerMenu;
    
    BOOL homeclicked;
    BOOL showloading;
    
    NSString *prefixphone;
    NSString *setbartextcolor;
    
}

@property (strong, nonatomic) UIWindow *window;
@property (strong, nonatomic) NSString *apipath;
@property (strong, nonatomic) NSString *email;
@property (strong, nonatomic) NSString *user;
@property (strong, nonatomic) NSString *db;

@property(strong,nonatomic)NSString *databasePath;
@property(strong,nonatomic)NSString *databaseName;

@property (strong, nonatomic) NSString *nextPage;
@property (strong, nonatomic) NSString *plugin_id;
@property (strong, nonatomic) NSString *themeColorCode;
@property (strong, nonatomic) NSString *themeColorCode1;
@property (strong, nonatomic) NSString *customer_id;
@property (strong, nonatomic) NSString *iconname;
@property (strong, nonatomic) NSString *homepage;
@property (strong, nonatomic) NSString *backgroundT;

@property (strong, nonatomic) NSString *user_apipath;
@property (strong, nonatomic) NSString *uuid;

@property (strong, nonatomic) NSString *cs;
@property (strong, nonatomic) NSString *QRUrl;
@property (strong, nonatomic) NSString *latitude;
@property (strong, nonatomic) NSString *longitude;

@property (strong, nonatomic) NSString *event_id;

@property (strong, nonatomic) SettingViewController* loginViewController;
@property BOOL isNavigatingAwayFromLogin;
@property BOOL hasLoggedIn;
@property BOOL shouldReturnToPrevPage;

@property (strong, nonatomic) CLBeaconRegion *myBeaconRegion;
@property (strong, nonatomic) CLBeaconRegion *myBeaconRegion2;
@property (strong, nonatomic) CLBeaconRegion *myBeaconRegion3;
@property (strong, nonatomic) CLBeaconRegion *myBeaconRegion4;
@property (strong, nonatomic) CLBeaconRegion *myBeaconRegion5;

//version 1.0
//@property (strong, nonatomic) CLLocationManager *locationManager;

@property (nonatomic,retain) UIViewController *originVC;

@property (assign, nonatomic) int beaconFoundTimer;
@property (assign, nonatomic) int intervalNotification;

@property BOOL ibeaconfrommenu;
@property BOOL firsttime;
//@property BOOL pushNotification;

//@property (nonatomic, retain) NSMutableData *receivedData;
@property (strong, nonatomic) NSString *currentRequest;

/*** Push Notification 27Aug2015 ***/
@property (strong, nonatomic) NSString *notify_actiontype;
@property (strong, nonatomic) NSString *notify_actionvalue;
@property (strong, nonatomic) NSString *notify_contentid;

@property(nonatomic, retain) UINavigationController *navController;
/*** Push Notification 27Aug2015 ***/

//Slide Menu (vertical)
@property NSMutableArray *listOfVerMenu;

@property (nonatomic, assign) BOOL homeclicked;
@property (nonatomic, assign) BOOL showloading;

@property (strong, nonatomic) NSString *prefixphone;

@property (strong, nonatomic) NSString *setbartextcolor;

-(void) checksum;
//-(void) checkdevice_info;

@end
