//
//  AppDelegate.m
//  standardmobileappsv3
//
//  Created by M3Online on 4/24/14.
//  Copyright (c) 2014 M3Online. All rights reserved.
//

#import "AppDelegate.h"
#import <SystemConfiguration/SystemConfiguration.h>
#import "TxtFiles.h"
#import "SBJson.h"
#import "RootViewController.h"
#import "ViewController.h"
#import "ThemeViewController.h"
//#import "RNCachingURLProtocol.h"

@implementation AppDelegate
BOOL locationStarted = FALSE;
UIAlertView *alert;
UIActivityIndicatorView *indicator;
//NSString *urlAddress;
NSData *_dToken;
NSString *_device_token_str;

@synthesize apipath;
@synthesize email;
@synthesize user;
@synthesize databaseName;
@synthesize databasePath;
@synthesize nextPage, plugin_id, themeColorCode, themeColorCode1, customer_id, shouldReturnToPrevPage, iconname, homepage, cs, QRUrl, prefixphone, setbartextcolor;

//version 1.0
//@synthesize user_apipath, locationManager, beaconFoundTimer, intervalNotification, ibeaconfrommenu, uuid, firsttime, latitude, longitude, db, /*pushNotification, receivedData,*/ currentRequest, notify_actiontype, notify_actionvalue, notify_contentid, navController, backgroundT, listOfVerMenu, homeclicked, showloading, event_id;

@synthesize user_apipath, beaconFoundTimer, intervalNotification, ibeaconfrommenu, uuid, firsttime, latitude, longitude, db, /*pushNotification, receivedData,*/ currentRequest, notify_actiontype, notify_actionvalue, notify_contentid, navController, backgroundT, listOfVerMenu, homeclicked, showloading, event_id;

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    //[NSURLProtocol registerClass:[RNCachingURLProtocol class]];
    // Set app-wide shared cache (first number is megabyte value)
    NSUInteger cacheSizeMemory = 500*1024*1024; // 500 MB
    NSUInteger cacheSizeDisk = 500*1024*1024; // 500 MB
    NSURLCache *sharedCache = [[NSURLCache alloc] initWithMemoryCapacity:cacheSizeMemory diskCapacity:cacheSizeDisk diskPath:@"nsurlcache"];
    [NSURLCache setSharedURLCache:sharedCache];
    sleep(1); // Critically important line, sadly, but it's worth it!
    
    // Override point for customization after application launch.
    
    //self.apipath =@"http://app.m3online.com/app_%@/api/";
    //self.email =@"maii";
    
    // Setting need to be set by developer
    //zh-Hans, en, ms-MY, th, in
    [[NSUserDefaults standardUserDefaults] setObject:[NSArray arrayWithObjects:@"en", nil] forKey:@"AppleLanguages"];
    //self.db = @"118";
    //self.user = @"187";
    //self.pushNotification = NO;
    /*** Push Notification 27Aug2015 ***/
    self.notify_actiontype = @"";
    self.notify_actionvalue = @"";
    self.notify_contentid = @"";
    /*** Push Notification 27Aug2015 ***/
    self.db = @"100"; //116 105 203 100 100 201 201 203 204 100 100
    self.user = @"1142"; //175 121 224 694 765 247 195vijiajiacomma 229 254 813 1142
    self.user_apipath =[NSString stringWithFormat:@"http://app.getsnapps.com/api/"]; //http://app.m3online.com/api/ http://app.getsnapps.com/api/ http://vjiajia.com.cn/api/
    //self.user_apipath = @"";
    self.showloading = YES;
    self.prefixphone = @""; //Default = "", +92 Toni&guy
    self.intervalNotification = 30; //1800
    self.setbartextcolor = @"";  //default = @""; just put black or white for manually set color expecially when navigation bar put image.
    // end Setting
    
    self.nextPage = @"";
    self.plugin_id = @"";
    self.themeColorCode = @"";
    self.themeColorCode1 = @"";
    self.iconname = @"";
    self.homepage = @"";
    self.backgroundT = @"";
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    if ([defaults objectForKey:@"customer_id"] > 0) {
        self.customer_id = [defaults objectForKey:@"customer_id"];
    } else {
        self.customer_id = @"0";
    }
    self.beaconFoundTimer = 0;
    //NSUUID *myDevice = [NSUUID UUID];
    //self.uuid = myDevice.UUIDString;
    NSUUID *deviceId;
#if TARGET_IPHONE_SIMULATOR
    deviceId = [NSUUID UUID];
#else
    deviceId = [UIDevice currentDevice].identifierForVendor;
#endif
    self.uuid = deviceId.UUIDString;
    //self.uuid = @"55151AC1-6CF7-4638-970F-C9B50FE629D0";
    NSLog(@"uuid :: %@", self.uuid);
    self.event_id = @"";
    // Initialize location manager and set ourselves as the delegate
    //set default value after application starts
    locationStarted = FALSE;
    //self.locationManager = [[CLLocationManager alloc] init];
    //locationManager.delegate = self;
    ibeaconfrommenu = NO;
    [listOfVerMenu removeAllObjects];
    listOfVerMenu = [[NSMutableArray alloc] init];
    homeclicked = NO;
    
    self.cs = @"";
    self.latitude = @"0";
    self.longitude = @"0";
    self.QRUrl = @"";
    [self checksum];
    
    app = [UIApplication sharedApplication];
    
    // Setup some globals
    
    databaseName = @"mbappv3.sql";
    
    // Get the path to the documents directory and append the databaseName
    NSArray *documentPaths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDir = [documentPaths objectAtIndex:0];
    databasePath = [documentsDir stringByAppendingPathComponent:databaseName];
    
    // Execute the "checkAndCreateDatabase" function
    [self checkAndCreateDatabase];
    
    /*if (self.pushNotification) {
        [self checkdevice_info];
        if ([application respondsToSelector:@selector(registerUserNotificationSettings:)])
        {
            // Register for Push Notifications for iOS 8
            UIUserNotificationType userNotificationTypes = (UIUserNotificationTypeAlert |
                                                            UIUserNotificationTypeBadge |
                                                            UIUserNotificationTypeSound);                                                                                                   UIUserNotificationSettings *settings = [UIUserNotificationSettings settingsForTypes:userNotificationTypes categories:nil];                          [application registerUserNotificationSettings:settings];
            [application registerForRemoteNotifications];
        }
        else
        {
            // Register for Push Notifications before iOS 8
            [application registerForRemoteNotificationTypes:(UIRemoteNotificationTypeBadge |
                                                             UIRemoteNotificationTypeAlert |
                                                             UIRemoteNotificationTypeSound)];
        }
    } */
    
    //appsetup
    NSString *fileName = @"appsetup.txt";
    NSString *filePath = [documentsDir stringByAppendingPathComponent:fileName];
    [self checkAndCreateTxtImageFile:filePath :fileName];
    
    //contentcategory
    NSString *fileName1 = @"contentcategory.txt";
    NSString *filePath1 = [documentsDir stringByAppendingPathComponent:fileName1];
    [self checkAndCreateTxtImageFile:filePath1 :fileName1];
    
    //contents
    NSString *fileName2 = @"contents.txt";
    NSString *filePath2 = [documentsDir stringByAppendingPathComponent:fileName2];
    [self checkAndCreateTxtImageFile:filePath2 :fileName2];
    
    //ibeaconcontent
    NSString *fileName3 = @"ibeaconcontent.txt";
    NSString *filePath3 = [documentsDir stringByAppendingPathComponent:fileName3];
    [self checkAndCreateTxtImageFile:filePath3 :fileName3];
    
    //ibeacondevice
    NSString *fileName4 = @"ibeacondevice.txt";
    NSString *filePath4 = [documentsDir stringByAppendingPathComponent:fileName4];
    [self checkAndCreateTxtImageFile:filePath4 :fileName4];
    
    //themecolor
    NSString *fileName5 = @"themecolor.txt";
    NSString *filePath5 = [documentsDir stringByAppendingPathComponent:fileName5];
    [self checkAndCreateTxtImageFile:filePath5 :fileName5];
    
    //themeobject
    NSString *fileName6 = @"themeobject.txt";
    NSString *filePath6 = [documentsDir stringByAppendingPathComponent:fileName6];
    [self checkAndCreateTxtImageFile:filePath6 :fileName6];
    
    //themeplugin
    NSString *fileName7 = @"themeplugin.txt";
    NSString *filePath7 = [documentsDir stringByAppendingPathComponent:fileName7];
    [self checkAndCreateTxtImageFile:filePath7 :fileName7];
    
    //themepage
    NSString *fileName8 = @"themepage.txt";
    NSString *filePath8 = [documentsDir stringByAppendingPathComponent:fileName8];
    [self checkAndCreateTxtImageFile:filePath8 :fileName8];
    
    //ibeaconxref
    NSString *fileName9 = @"ibeaconxref.txt";
    NSString *filePath9 = [documentsDir stringByAppendingPathComponent:fileName9];
    [self checkAndCreateTxtImageFile:filePath9 :fileName9];
    
    /*** Push Notification 27Aug2015 ***/
    UIStoryboard *storyboard;
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone) {
        // For iPhone
        storyboard = [UIStoryboard storyboardWithName:@"Main_iPhone" bundle:nil];
    } else {
        storyboard = [UIStoryboard storyboardWithName:@"Main_iPad" bundle:nil];
    }
    ViewController *ivc = [storyboard instantiateViewControllerWithIdentifier:@"ViewController"];
    //self.navController=[[UINavigationController alloc] initWithRootViewController:ivc];
    self.navController = [[RootViewController alloc] initWithRootViewController:ivc];
    //[navigationcontroller performSegueWithIdentifier:@"pushVC" sender:navigationcontroller];
    self.window.rootViewController =nil;
    self.window.rootViewController = self.navController;
    /*** Push Notification 27Aug2015 ***/
    
    //[self.window addSubview:ViewController.view];
    [self.window makeKeyAndVisible];
    return YES;
}

- (void)applicationWillResignActive:(UIApplication *)application
{
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

//update location
/*-(void)locationManager:(CLLocationManager *)manager didUpdateToLocation:(CLLocation *)newLocation fromLocation:(CLLocation *)oldLocation{
    //NSLog(@"Location: %f, %f", newLocation.coordinates.longtitude, newLocation.coordinates.latitude);
} */

//run background task
-(void)runBackgroundTask: (int) time{
    //check if application is in background mode
    if ([UIApplication sharedApplication].applicationState == UIApplicationStateBackground) {
        
        //create UIBackgroundTaskIdentifier and create tackground task, which starts after time
        __block UIBackgroundTaskIdentifier bgTask = [app beginBackgroundTaskWithExpirationHandler:^{
            [app endBackgroundTask:bgTask];
            bgTask = UIBackgroundTaskInvalid;
        }];
        
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0), ^{
            //NSTimer* t = [NSTimer scheduledTimerWithTimeInterval:time target:self selector:@selector(startTrackingBg) userInfo:nil repeats:YES];
            //[[NSRunLoop currentRunLoop] addTimer:t forMode:NSDefaultRunLoopMode];
            //[[NSRunLoop currentRunLoop] run];
            //[self startTrackingBg];
        });
    }
}

//starts when application switchs into backghround
- (void)applicationDidEnterBackground:(UIApplication *)application
{
    //check if application status is in background
    if ( [UIApplication sharedApplication].applicationState == UIApplicationStateBackground) {
        NSLog(@"start backgroun tracking from appdelegate");
        
        //start updating location with location manager
        // version 1.0 : Enable Location
        //[locationManager startUpdatingLocation];
    }
    
    //change locationManager status after time
    [self runBackgroundTask:1];
}

//starts with background task
-(void)startTrackingBg{
    //write background time remaining
    NSLog(@"backgroundTimeRemaining: %.0f", [[UIApplication sharedApplication] backgroundTimeRemaining]);
    
    //set default time
    //int time = 5;
    //if locationManager is ON
    
    
    // *CODE-LOCATION
    /*if (locationStarted == TRUE ) {
        //stop update location
        [locationManager stopUpdatingLocation];
        locationStarted = FALSE;
    }else{
        //start updating location
        [locationManager startUpdatingLocation];
        locationStarted = TRUE;
        //ime how long the application will update your location
        time = 5;
    }*/
    
    //[self runBackgroundTask:time];
}

- (void)applicationWillEnterForeground:(UIApplication *)application
{
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
    locationStarted = FALSE;
    //[locationManager stopUpdatingLocation];
}

- (void)applicationDidBecomeActive:(UIApplication *)application
{
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    self.beaconFoundTimer = self.intervalNotification-1;
    /* application.applicationIconBadgeNumber = 0;
    
    if (self.pushNotification) {
        
        NSString *appname = [[NSBundle mainBundle] objectForInfoDictionaryKey:@"CFBundleDisplayName"];
        NSString *appversion = [[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleShortVersionString"];
        
        sqlite3 *database;
        // Open the database from the users filessytem
        if(sqlite3_open([databasePath UTF8String], &database) == SQLITE_OK) {
            
            int rows=0;
            NSString *temp = [[NSString alloc] initWithFormat:@"SELECT * FROM device_info WHERE appname='%@' AND appversion='%@' AND deviceuid='%@' ", appname, appversion, uuid];
           NSLog(@"temp:: %@", temp);
            const char *sqlStatement = [temp UTF8String];
            sqlite3_stmt *compiledStatement;
            if(sqlite3_prepare_v2(database, sqlStatement, -1, &compiledStatement, NULL) == SQLITE_OK) {
                // Loop through the results and add them to the feeds array
     
                NSString *tokenID;
                while(sqlite3_step(compiledStatement) == SQLITE_ROW) {
                    rows = rows+1;
                    tokenID = [NSString stringWithUTF8String:(char *)sqlite3_column_text(compiledStatement, 2)];
                    
                    NSString *urlAddress = [[NSString alloc] initWithFormat:@"%@reset_badgeno_ios.php?db=%@&userid=%@&udid=%@&cuid=%@&token_id=%@", user_apipath, db, user, uuid, customer_id, tokenID];
                    NSLog(@"urlAddress:: %@", urlAddress);
                    currentRequest = @"ResetBadgeNo";
                    [self connectToServer:urlAddress];
                }
                
                tokenID = nil;
            }
            // Release the compiled statement from memory
            sqlite3_finalize(compiledStatement);
            
        }
        sqlite3_close(database);
    } */
}

- (void)applicationWillTerminate:(UIApplication *)application
{
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}

-(void)application:(UIApplication *)application didReceiveLocalNotification:(UILocalNotification *)notification{
    // I recieved a notification
    self.beaconFoundTimer = self.intervalNotification-1;
}

-(void) checkAndCreateDatabase{
    // Check if the SQL database has already been saved to the users phone, if not then copy it over
    BOOL success;
    
    // Get the path to the database in the application package
    NSString *databasePathFromApp = [[[NSBundle mainBundle] resourcePath] stringByAppendingPathComponent:databaseName];
    
    // Create a FileManager object, we will use this to check the status
    // of the database and to copy it over if required
    NSFileManager *fileManager = [NSFileManager defaultManager];
    
    // Check if the database has already been created in the users filesystem
    success = [fileManager fileExistsAtPath:databasePath];
    
    // If the database already exists then return without doing anything
    if(success) {
        if ([self checkColumnExists ]) {
            NSLog(@"checkColumnExists YES");
            firsttime = NO;
            return;
        } else {
            NSLog(@"checkColumnExists NO");
            [fileManager removeItemAtPath:databasePath error:nil];
#if TARGET_IPHONE_SIMULATOR
            //
#else
            [fileManager removeItemAtPath:databasePathFromApp error:nil];
#endif
            [self checkAndCreateDatabase];
        }
    } else {
        firsttime = YES;
    }
    
    // If not then proceed to copy the database from the application to the users filesystem
    //NSLog(@"databasePathFromApp %@",databasePathFromApp);
    
    // Copy the database from the package to the users filesystem
    [fileManager copyItemAtPath:databasePathFromApp toPath:databasePath error:nil];
    
}

-(void) checkAndCreateTxtImageFile :(NSString *) filepath :(NSString *) filename{
    // Check if the SQL database has already been saved to the users phone, if not then copy it over
    BOOL success;
    
    // Create a FileManager object, we will use this to check the status
    // of the database and to copy it over if required
    NSFileManager *fileManager = [NSFileManager defaultManager];
    
    // Check if the database has already been created in the users filesystem
    success = [fileManager fileExistsAtPath:filepath];
    
    // If the database already exists then return without doing anything
    if(success) return;
    
    // If not then proceed to copy the database from the application to the users filesystem
    
    // Get the path to the database in the application package
    NSString *databasePathFromApp = [[[NSBundle mainBundle] resourcePath] stringByAppendingPathComponent:filename];
    
    // Copy the database from the package to the users filesystem
    [fileManager copyItemAtPath:databasePathFromApp toPath:filepath error:nil];
    
}

-(BOOL)checkColumnExists
{
    //BOOL columnExists = NO;
    
    /*sqlite3_stmt *selectStmt;
    
    const char *sqlStatement = "select termscons_url from app_setup";
    if(sqlite3_prepare_v2(database, sqlStatement, -1, &selectStmt, NULL) == SQLITE_OK)
        columnExists = YES; */
    /*sqlite3 *database;
    
    if(sqlite3_open([databasePath UTF8String], &database) == SQLITE_OK) {
        //NSString *theme_object = [[NSString alloc] initWithFormat:@"select termscons_url, titlebar_img, show_title from app_setup"];
        NSString *theme_object = [[NSString alloc] initWithFormat:@"select allow_favorite, allow_comments from content_category"];
        NSLog(@"theme_object_plugin columnExists:: %@", theme_object);
        
        const char *sqlStatement1 = [theme_object UTF8String];
        sqlite3_stmt *compiledStatement1;
        if(sqlite3_prepare_v2(database, sqlStatement1, -1, &compiledStatement1, NULL) == SQLITE_OK) {
            columnExists = YES;
            NSLog(@"columnExists");
        }
        // Release the compiled statement from memory
        sqlite3_finalize(compiledStatement1);
    }
    sqlite3_close(database);
    
    return columnExists; */
    BOOL columnExists = NO;
    
    sqlite3_stmt *selectStmt;
    
    sqlite3 *database;
    
    if(sqlite3_open([databasePath UTF8String], &database) == SQLITE_OK) {
        const char *sqlStatement = "select parent_id from content_category";
        if(sqlite3_prepare_v2(database, sqlStatement, -1, &selectStmt, NULL) == SQLITE_OK)
            columnExists = YES;
        //NSLog(@"columnExists:%hhd", columnExists);
    }
    sqlite3_close(database);
    
    return columnExists;
}

-(void) checksum {
    NSString *combinestr = [NSString stringWithFormat:@"%@%@%@%@", self.user, self.user, self.uuid, self.customer_id];
    //NSString *combinestr = [NSString stringWithFormat:@"%@%@%@%@", @"116", @"1116", @"AA-BB-C1", @"124"];
    NSCharacterSet *doNotWant = [NSCharacterSet characterSetWithCharactersInString:@"-"];
    combinestr = [[combinestr componentsSeparatedByCharactersInSet: doNotWant] componentsJoinedByString: @""];
    combinestr = [NSString stringWithFormat:@"%@%lu", combinestr, (unsigned long)[combinestr length]];
    
    NSMutableString *reversedString = [NSMutableString string];
    NSInteger charIndex = [combinestr length];
    while (charIndex > 0) {
        charIndex--;
        NSRange subStrRange = NSMakeRange(charIndex, 1);
        [reversedString appendString:[combinestr substringWithRange:subStrRange]];
    }
    combinestr = reversedString;
    combinestr = [combinestr substringToIndex:15];
    self.cs = combinestr;
    //NSLog(@"combinestr :: %@", combinestr);

}

/*-(void)application:(UIApplication *)application didRegisterForRemoteNotificationsWithDeviceToken:(NSData*)deviceToken
{
    _dToken = deviceToken;
    NSCharacterSet *charsToRemove = [NSCharacterSet characterSetWithCharactersInString:@"< >"];
    NSString *someString = [[deviceToken description] stringByTrimmingCharactersInSet:charsToRemove];
    NSString *finalStr  = [someString stringByReplacingOccurrencesOfString:@" " withString:@""];
    _device_token_str = finalStr;
    //_device_token_str = @"testtest2007c11897d393644c15b812e29c951a9b9bcf8fbd1137304d3aff0d";
    NSLog(@"My token is: %@", deviceToken);
    
    NSString *appname = [[NSBundle mainBundle] objectForInfoDictionaryKey:@"CFBundleDisplayName"];
    NSString *appversion = [[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleShortVersionString"];
    
    sqlite3 *database;
    // Open the database from the users filessytem
    if(sqlite3_open([databasePath UTF8String], &database) == SQLITE_OK) {
        
        int rows=0;
        NSString *temp = [[NSString alloc] initWithFormat:@"SELECT * FROM device_info WHERE appname='%@' AND appversion='%@' AND deviceuid='%@' ", appname, appversion, uuid];
        NSLog(@"tempDeviceInfo:: %@", temp);
        const char *sqlStatement = [temp UTF8String];
        sqlite3_stmt *compiledStatement;
        if(sqlite3_prepare_v2(database, sqlStatement, -1, &compiledStatement, NULL) == SQLITE_OK) {
            // Loop through the results and add them to the feeds array
 
            NSString *tokenID;
            NSString *customerID=@"0";
            while(sqlite3_step(compiledStatement) == SQLITE_ROW) {
                rows = rows+1;
                tokenID = [NSString stringWithUTF8String:(char *)sqlite3_column_text(compiledStatement, 2)];
                //NSLog(@"tokenID:: %@", tokenID);
                customerID = [NSString stringWithUTF8String:(char *)sqlite3_column_text(compiledStatement, 3)];
                //NSLog(@"customerID:: %@", customerID);
            }
            if (rows > 0) {
                //NSLog(@"customer_id:: %@", customer_id);
                //NSLog(@"_device_token_str:: %@", _device_token_str);
                if (![customerID isEqualToString:customer_id] || ![tokenID isEqualToString:_device_token_str]) {
                    //NSLog(@"database:::::::update conferencestheme_page");
                    static sqlite3_stmt *compiledupdateStatement;
                    sqlite3_exec(database, [[NSString stringWithFormat:@"update device_info SET token_id='%@', customer_id=%@, registered=0 WHERE appname='%@' AND appversion='%@' AND deviceuid='%@' ", _device_token_str, customer_id, appname, appversion, uuid] UTF8String], NULL, NULL, NULL);
                    sqlite3_finalize(compiledupdateStatement);
                    //NSLog(@"database:::::::update conferencestheme_page11");
                }
            } else {
                //NSLog(@"database:::::::insert into conferencestheme_page");
                static sqlite3_stmt *compiledinsertStatement;
                sqlite3_exec(database, [[NSString stringWithFormat:@"insert into device_info (user_id, token_id, customer_id, appname, appversion, deviceuid, registered) values (%@, '%@', %@, '%@', '%@', '%@', 0)", user, _device_token_str, customer_id, appname, appversion, uuid] UTF8String], NULL, NULL, NULL);
                sqlite3_finalize(compiledinsertStatement);
            }
            
            tokenID = nil;
            customerID = nil;
        }
        // Release the compiled statement from memory
        sqlite3_finalize(compiledStatement);
        
        NSString *userID;
        NSString *tokenID;
        NSString *customerID;
        NSString *appName;
        NSString *appVersion;
        NSString *deviceUID;
        NSString *tempDevice = [[NSString alloc] initWithFormat:@"SELECT * from device_info WHERE registered=0 AND appname='%@' AND appversion='%@' AND deviceuid='%@' LIMIT 1", appname, appversion, uuid];
        //NSLog(@"temp:: %@", temp);
        const char *sqlStatementDevice = [tempDevice UTF8String];
        sqlite3_stmt *compiledStatementDevice;
        if(sqlite3_prepare_v2(database, sqlStatementDevice, -1, &compiledStatementDevice, NULL) == SQLITE_OK) {
            while(sqlite3_step(compiledStatementDevice) == SQLITE_ROW) {
                
                userID = [NSString stringWithUTF8String:(char *)sqlite3_column_text(compiledStatementDevice, 1)];
                tokenID = [NSString stringWithUTF8String:(char *)sqlite3_column_text(compiledStatementDevice, 2)];
                customerID = [NSString stringWithUTF8String:(char *)sqlite3_column_text(compiledStatementDevice, 3)];
                appName = [NSString stringWithUTF8String:(char *)sqlite3_column_text(compiledStatementDevice, 4)];
                appVersion = [NSString stringWithUTF8String:(char *)sqlite3_column_text(compiledStatementDevice, 5)];
                deviceUID = [NSString stringWithUTF8String:(char *)sqlite3_column_text(compiledStatementDevice, 6)];
                
                NSString *urlAddress = [[NSString alloc] initWithFormat:@"%@register_device_ios.php?db=%@&userid=%@&udid=%@&cuid=%@&token_id=%@&appname=%@&appversion=%@", user_apipath, db, user, uuid, customer_id, tokenID, appName, appVersion];
                //NSLog(@"urlAddress:: %@", urlAddress);
                currentRequest = @"RegisterDevice";
                [self connectToServer:urlAddress];
            }
        }
        // Release the compiled statement from memory
        sqlite3_finalize(compiledStatementDevice);
        userID = nil;
        tokenID = nil;
        customerID = nil;
        appName = nil;
        appVersion = nil;
        deviceUID = nil;
    
    }
    sqlite3_close(database);
}

- (void)application:(UIApplication*)application didFailToRegisterForRemoteNotificationsWithError:(NSError*)error
{
    NSLog(@"Failed to get token, error: %@", error);
}

- (void)application:(UIApplication *)application didReceiveRemoteNotification:(NSDictionary *)userInfo fetchCompletionHandler:(void (^)(UIBackgroundFetchResult))completionHandler
{
    //-ios7
    [[UIApplication sharedApplication] cancelAllLocalNotifications];
}

-(void) checkdevice_info{
    
    sqlite3 *contactDB;
    const char *dbpath = [databasePath UTF8String];
    
    if (sqlite3_open(dbpath, &contactDB) == SQLITE_OK)
    {
        char *err;
        NSString *sql = [NSString stringWithFormat:@"CREATE TABLE IF NOT EXISTS device_info (id INTEGER PRIMARY KEY, user_id INTEGER, token_id TEXT, customer_id INTEGER, appname TEXT, appversion TEXT, deviceuid TEXT, registered INTEGER)"];
        NSLog (@"sql:: %@",sql);
        if (sqlite3_exec(contactDB, [sql UTF8String], NULL, NULL, &err) != SQLITE_OK) {
            sqlite3_close(contactDB);
            NSAssert(0, @"Table failed to create.");
        }
        
    }
    
}

#pragma mark Connect To API
-(void) connectToServer:(NSString *) urlPath
{
    NSString *urlAddress =[NSString stringWithFormat:@"%@", urlPath];
    
    NSURL *url = [NSURL URLWithString:urlAddress];
    //NSLog(@"url:: %@", url);
    NSURLRequest *requestObj = [NSURLRequest requestWithURL:url
                                                cachePolicy:NSURLRequestReloadIgnoringLocalAndRemoteCacheData
                                            timeoutInterval:30];
    
    NSURLConnection *theConnection=[[NSURLConnection alloc] initWithRequest:requestObj delegate:self];
    if (theConnection) {
        // Create the NSMutableData to hold the received data.
        // receivedData is an instance variable declared elsewhere.
        receivedData = [NSMutableData data];
        NSLog(@"theConnection");
    } else {
        // Inform the user that the connection failed.
        //[self displayAlert:@"Connection Error. Please try again later."];
        //[self displayAlert:@"Connection error. Please try again later."];
        //[self downloaddata:appdelegate.firsttime :@""];
        NSLog(@"Connection error. Please try again later.");
    }
}

- (void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response
{
    //NSLog(@"didReceiveResponse");
    [receivedData setLength:0];
}

- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data
{
    [receivedData appendData:data];
    //NSLog(@"receivedData:: %@", receivedData);
}

- (void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error {
    // release the connection, and the data object
    
    //[connection release];
    //[receivedData release];
    
    //[self displayAlert:@"No internet connection available. Standard Mobile Apps requires internet connection to function properly."];
    //[self downloaddata:appdelegate.firsttime :@""];
    NSLog(@"No internet connection available. Standard Mobile Apps requires internet connection to function properly.");
}

- (void)connectionDidFinishLoading:(NSURLConnection *)connection
{
    NSLog(@"Succeed! Received %ld bytes of data",(unsigned long)[receivedData length]);
    NSString *escapedStr = [[NSString alloc] initWithData:receivedData encoding:NSUTF8StringEncoding];
    
    //NSString *escapedStr = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
    //NSString *escapedStr = [[NSString alloc] initWithData:data encoding:encoding];
    NSString *response = [escapedStr stringByReplacingOccurrencesOfString:@"'" withString:@"''"];
    
    if ([response length]==0)
        response = @"[{}]";
    
    //NSLog(@"%@ response:: %@", currentRequestMenu, response);
    if ([currentRequest isEqualToString:@"RegisterDevice"]) {
        sqlite3 *database;
        // Open the database from the users filessytem
        if(sqlite3_open([databasePath UTF8String], &database) == SQLITE_OK) {
            
            //NSLog(@"database:::::::update registered");
            static sqlite3_stmt *compiledupdateStatement;
            sqlite3_exec(database, [[NSString stringWithFormat:@"update device_info SET registered=1"] UTF8String], NULL, NULL, NULL);
            sqlite3_finalize(compiledupdateStatement);
            //NSLog(@"database:::::::update conferencestheme_page11");
            
        }
        sqlite3_close(database);
    }
    //[connection release];
    connection = nil;
    //[response release];
    response = nil;
} */

@end

@implementation NSURLRequest(DataController)
+ (BOOL)allowsAnyHTTPSCertificateForHost:(NSString *)host
{
    return YES;
}
@end
