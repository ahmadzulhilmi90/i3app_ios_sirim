//
//  BasicPluginViewController.h
//  standardmobileappsv3
//
//  Created by M3Online on 5/30/14.
//  Copyright (c) 2014 M3Online. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MapKit/MapKit.h>
#import <MessageUI/MessageUI.h>

@interface BasicPluginViewController : UIViewController <MKMapViewDelegate, UIWebViewDelegate, MFMailComposeViewControllerDelegate, UIViewControllerTransitioningDelegate> {
    UIWebView *webView;
    UIScrollView *scrollView;
    
    UIView *view_popoverMain;
    UIView *view_plain;
    UIButton *buttonclose;
    UIButton *buttonlogin;
    UILabel *lbllogin;
    
    int session_id;
    //NSMutableData* receivedData;
    NSString *currentRequestMenu;
}

@property (strong, nonatomic) IBOutlet MKMapView *mapView;
@property (nonatomic, strong) UIWebView *webView;
@property (strong, nonatomic) IBOutlet UIScrollView *scrollView;

@property (nonatomic, strong) UIView *view_popoverMain;
@property (nonatomic, strong) UIButton *buttonclose;
@property (nonatomic, strong) UIButton *buttonlogin;
@property (nonatomic, strong) UILabel *lbllogin;
@property (nonatomic, strong) UIView *view_plain;

@property (nonatomic, assign) int session_id;
//@property (nonatomic, retain) NSMutableData *receivedData;
@property (nonatomic, retain) NSString *currentRequestMenu;

@end
