//
//  BasicPluginViewController.m
//  standardmobileappsv3
//
//  Created by M3Online on 5/30/14.
//  Copyright (c) 2014 M3Online. All rights reserved.
//

#import "BasicPluginViewController.h"
#import "AppDelegate.h"
#import "myAnnotation.h"
#import "ViewController.h"
#import "RootViewController.h"
#import "SBJson.h"
#import "LoadingView.h"
#import "NSString+HTML.h"
#import "ContentPluginViewController.h"
#import "InitialSlidingViewController.h"
#import "BTSlideInteractor.h"

#define METERS_PER_MILE 1609.344

@interface BasicPluginViewController ()
@property (strong, nonatomic) BTSlideInteractor *interactor;
@end

@implementation BasicPluginViewController
UIActivityIndicatorView *activityIndicator;
UIAlertView *alert;
UIActivityIndicatorView *indicator;

@synthesize webView, scrollView, view_popoverMain, buttonclose, buttonlogin, lbllogin, view_plain, session_id, currentRequestMenu/*, receivedData*/;

__strong NSString *aplugintype;
__strong NSString *apluginvalue1;
__strong NSString *apluginvalue2;
__strong NSString *apluginvalue3;
__strong NSString *aid;
__strong NSString *sub;

NSUInteger countPage;


InitialSlidingViewController *modalController;

AppDelegate *appdelegate;
LoadingView *loadingView;

- (UIColor *) colorWithHexString:(NSString *)hexString {
    NSString *colorString = [[hexString stringByReplacingOccurrencesOfString: @"#" withString: @""] uppercaseString];
    CGFloat alpha, red, blue, green;
    alpha = 1.0f;
    red   = [self colorComponentFrom: colorString start: 0 length: 2];
    green = [self colorComponentFrom: colorString start: 2 length: 2];
    blue  = [self colorComponentFrom: colorString start: 4 length: 2];
    return [UIColor colorWithRed: red green: green blue: blue alpha: alpha];
}

- (CGFloat) colorComponentFrom:(NSString *)string start:(NSUInteger)start length:(NSUInteger)length {
    NSString *substring = [string substringWithRange: NSMakeRange(start, length)];
    NSString *fullHex = length == 2 ? substring : [NSString stringWithFormat: @"%@%@", substring, substring];
    unsigned hexComponent;
    [[NSScanner scannerWithString: fullHex] scanHexInt: &hexComponent];
    return hexComponent / 255.0;
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void) processdata {
    sqlite3 *database;
    
    if(sqlite3_open([appdelegate.databasePath UTF8String], &database) == SQLITE_OK) {
        NSString *theme_object = [[NSString alloc] initWithFormat:@"SELECT * from theme_object_plugin WHERE id = %@", appdelegate.plugin_id];
        //NSLog(@"theme_object_plugin:: %@", theme_object);
        
        const char *sqlStatement1 = [theme_object UTF8String];
        sqlite3_stmt *compiledStatement1;
        if(sqlite3_prepare_v2(database, sqlStatement1, -1, &compiledStatement1, NULL) == SQLITE_OK) {
            while(sqlite3_step(compiledStatement1) == SQLITE_ROW) {
                
                
                aplugintype = [NSString stringWithUTF8String:(char *)sqlite3_column_text(compiledStatement1, 3)];
                apluginvalue1 = [NSString stringWithUTF8String:(char *)sqlite3_column_text(compiledStatement1, 4)];
                apluginvalue2 = [NSString stringWithUTF8String:(char *)sqlite3_column_text(compiledStatement1, 5)];
                
            }
        }
        // Release the compiled statement from memory
        sqlite3_finalize(compiledStatement1);
        
        //NSLog(@"aplugintype:: %@", aplugintype);
        if ([aplugintype isEqualToString:@"web"]) {
            if ([apluginvalue1 length]>0) {
                
                webView = [[UIWebView alloc] init];
                if ([apluginvalue2 isEqualToString:@"yes"]) {
                    [webView setFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height-50)];
                } else {
                    [webView setFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height)];
                }
                webView.backgroundColor = [UIColor clearColor];
                webView.delegate = self;
                //webView.scalesPageToFit = YES;
                //[self.view addSubview:webView];
                
                //initialize the activity indicator
                
                activityIndicator=[[UIActivityIndicatorView alloc]initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhiteLarge];
                
                //Change the color of the indicator, this override the color set by UIActivityIndicatorViewStyleWhiteLarge
                activityIndicator.color=[UIColor blackColor];
                
                //Put the indicator on the center of the webview
                [activityIndicator setCenter:self.view.center];
                
                //Add the indicator to the webView to make it visible
                [webView addSubview:activityIndicator];
                [webView setScalesPageToFit:YES];
                webView.scrollView.bounces = NO;
                
                [activityIndicator startAnimating];
                
                NSString *urlAddressWeb = apluginvalue1;
                NSRange rangeurl = [urlAddressWeb rangeOfString:@"?"];
                if(rangeurl.location != NSNotFound) {
                    //found
                    urlAddressWeb = [NSString stringWithFormat: @"%@&f=1&udid=%@&session=%d", urlAddressWeb, appdelegate.uuid, session_id];
                } else {
                    urlAddressWeb = [NSString stringWithFormat: @"%@?f=1&udid=%@&session=%d", urlAddressWeb, appdelegate.uuid, session_id];
                }
                
                NSString *urlString = [NSString stringWithFormat:@"%@", [NSURL URLWithString:urlAddressWeb]];
                
                //version 1.0
                //NSURL *url = [NSURL URLWithString:[urlString stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]];
                //NSURL *url = [NSURL URLWithString:[urlString stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLFragmentAllowedCharacterSet]]];
                
                
                //verison 1.1
                
                if ([urlString containsString:@"#"]) {
                
                NSRange r1 = [urlString rangeOfString:@"#"];
                NSRange r2 = [urlString rangeOfString:@"?"];
                NSRange rSub = NSMakeRange(r1.location + r1.length, r2.location - r1.location - r1.length);
                sub = [urlString substringWithRange:rSub];
                
                    if(rSub.length < 1){
                        sub = @"";
                    }
                }
                
                NSURL *url = [NSURL URLWithString:urlString];
                
                
                
                NSLog(@"sub %@", sub);
                NSLog(@"urlweb %@", url);
                
                [webView loadRequest:[NSURLRequest requestWithURL:url]];
                [self.view addSubview:webView];
                
                if ([apluginvalue2 isEqualToString:@"yes"]) {
                    __strong UIView* view = [[UIView alloc] initWithFrame:CGRectMake(0, self.view.frame.size.height-50, self.view.frame.size.width, 50)];
                    [view setBackgroundColor: [self colorWithHexString:appdelegate.themeColorCode]];
                    
                    UIButton *button = [UIButton buttonWithType:UIButtonTypeRoundedRect];
                    button.frame = CGRectMake(self.view.frame.size.width-70, 5.0, 60.0, 40.0);
                    [button setTitle:@"Open" forState:UIControlStateNormal];
                    [button setTitleColor: [UIColor colorWithRed:114/255.0f green:114/255.0f blue:114/255.0f alpha:1.0f] forState: UIControlStateNormal];
                    //[button setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
                    [button setBackgroundColor:[UIColor colorWithRed:229.0f/255.0f green:229.0f/255.0f blue:229.0f/255.0f alpha:1.0f]];
                    //[synbutton setImage:[UIImage imageNamed:@"sync.png"] forState:UIControlStateNormal];
                    [button addTarget:self
                               action:@selector(aMethod:)
                     forControlEvents:UIControlEventTouchUpInside];
                    button.layer.cornerRadius = 5;
                    [view addSubview: button];
                    
                    UIButton *buttonImg = [UIButton buttonWithType:UIButtonTypeRoundedRect];
                    buttonImg.frame = CGRectMake(self.view.frame.size.width-100, 5.0, 40.0, 40.0);
                    buttonImg.backgroundColor = [UIColor colorWithRed:229.0f/255.0f green:229.0f/255.0f blue:229.0f/255.0f alpha:1.0f];
                    [buttonImg setBackgroundImage:[UIImage imageNamed:@"website.png"] forState:UIControlStateNormal];
                    [buttonImg addTarget:self action:@selector(aMethod:) forControlEvents:UIControlEventTouchUpInside];
                    buttonImg.layer.cornerRadius = 5;
                    [view addSubview:buttonImg];
                    
                    [self.view addSubview: view];
                }
            }
        } else if ([aplugintype isEqualToString:@"gps"]) {
            self.mapView = [[MKMapView alloc] initWithFrame:CGRectMake(0,0,self.view.frame.size.width,self.view.frame.size.height)];
            [self.view addSubview:self.mapView];
            self.mapView.delegate = self;
            //5
            CLLocationCoordinate2D coordinate1;
            coordinate1.latitude = [apluginvalue1 floatValue];
            coordinate1.longitude = [apluginvalue2 floatValue];
            myAnnotation *annotation = [[myAnnotation alloc] initWithCoordinate:coordinate1 title:@""];
            [self.mapView addAnnotation:annotation];
            //[self.view addSubview:self.mapView];
        } else if ([aplugintype isEqualToString:@"html5"]) {
            self.title = apluginvalue1;
            
            webView = [[UIWebView alloc] init];
            webView.backgroundColor = [UIColor clearColor];
            //[webView setFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height)];
            webView.delegate = self;
            
            //initialize the activity indicator
            
            activityIndicator=[[UIActivityIndicatorView alloc]initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhiteLarge];
            
            //Change the color of the indicator, this override the color set by UIActivityIndicatorViewStyleWhiteLarge
            activityIndicator.color=[UIColor blackColor];
            
            //Put the indicator on the center of the webview
            [activityIndicator setCenter:self.view.center];
            
            //Add the indicator to the webView to make it visible
            [webView addSubview:activityIndicator];
            //[webView setScalesPageToFit:YES];
            
            [activityIndicator startAnimating];
            
            NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
            NSString *login_id = [defaults objectForKey:@"login_id"];
            
            NSString *urlAddress = apluginvalue2;
            NSRange rangeurl = [urlAddress rangeOfString:@"?"];
            if(rangeurl.location != NSNotFound) {
                //found
                urlAddress = [NSString stringWithFormat: @"%@&f=1&login_id=%@&udid=%@&session=%d", urlAddress, login_id, appdelegate.uuid, session_id];
            } else {
                urlAddress = [NSString stringWithFormat: @"%@?f=1&login_id=%@&udid=%@&session=%d", urlAddress, login_id, appdelegate.uuid, session_id];
            }
            //NSLog(@"urlAddresshtml5 %@", urlAddress);
            NSURL *url = [NSURL URLWithString:urlAddress];
            
            NSString *html = [NSString stringWithContentsOfURL:url encoding:NSUTF8StringEncoding error:nil];
            /*NSRange range = [html rangeOfString:@"<body"];
            //NSLog(@"html %@", html);
            if(range.location != NSNotFound) {
                // Adjust style for mobile
                //float inset = 40;
                NSString *style = [NSString stringWithFormat:@"<style>div {max-width: %d%%;width: %fpx;height: %fpx;}</style>", 100, self.view.frame.size.width, self.view.frame.size.height];
                html = [NSString stringWithFormat:@"%@%@%@", [html substringToIndex:range.location], style, [html substringFromIndex:range.location]];
            } */
            [webView loadHTMLString:html baseURL:url];
            
            //float h;
            //h = [[webView stringByEvaluatingJavaScriptFromString: @"document.body.offsetHeight;"] integerValue];
            //NSLog(@"web view is %f high", webView.frame.size.height);
            //NSString *heightString = [webView stringByEvaluatingJavaScriptFromString:@"document.body.offsetHeight;"];
            //NSLog(@"web content is %@ high",heightString);
            //h = [heightString floatValue]; // convert from string to float plus some extra points because calculation is sometimes one line short
            //h = h + 70;
            webView.frame = CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height);
            // get bottom of text field
            //h = h + 70; // extra 70 pixels for UIButton at bottom and padding.
            //[scrollView setContentSize:CGSizeMake(self.view.frame.size.width, h)];
            [self.view addSubview:webView];
            //[self.view addSubview:webView];
            //[awebView setScalesPageToFit:NO];
            //awebView.multipleTouchEnabled=YES;

        } else if ([aplugintype isEqualToString:@"html5c"]) {
            self.title = apluginvalue1;
            
            //session_id = (arc4random() % 8000000001) + 1000000000;
            session_id = (arc4random() % 10000000000000000);
            //session_id = 1;
            //NSLog(@"Random Number: %i", session_id);
            
            NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
            NSString *hasLoggedInFB = [defaults objectForKey:@"hasLoggedInFB"];
            NSString *hasLoggedInMyApp = [defaults objectForKey:@"hasLoggedInMyApp"];
            
            if ([hasLoggedInFB isEqualToString:@"YES"] || [hasLoggedInMyApp isEqualToString:@"YES"])
            {
            
                NSString *urlAddrSession = [[NSString alloc] initWithFormat:@"%@cust_session.php?db=%@&userid=%@&udid=%@&cuid=%@&cusid=%d", appdelegate.user_apipath, [self urlEncode:appdelegate.db], [self urlEncode:appdelegate.user], [self urlEncode:appdelegate.uuid], [self urlEncode:appdelegate.customer_id], session_id];
                currentRequestMenu = @"AddSession";
                [self connectToServer:urlAddrSession];
                //NSLog(@"AddSession: %@", urlAddrSession);
                
            }
            else {
                
                [self.view addSubview:view_popoverMain];
                [view_popoverMain addSubview:view_plain];
                [view_plain addSubview:buttonclose];
                [view_plain addSubview:buttonlogin];
                [view_plain addSubview:lbllogin];
                //[view_popoverMain setHidden:NO];
                //[self.view bringSubviewToFront:view_popoverMain];
            }
        }
        
    }
    sqlite3_close(database);
}

- (void)aMethod:(NSString *)sender {
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:apluginvalue1]];
}


- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    appdelegate.homeclicked = NO;
    
    self.title = appdelegate.iconname;
    self.view.backgroundColor = [UIColor clearColor];
    self.navigationItem.backBarButtonItem = [[UIBarButtonItem alloc]initWithTitle:@"" style:UIBarButtonItemStylePlain target:nil action:nil];
    
    appdelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    
    CGRect rect = CGRectMake(0,0,20,20);
    UIGraphicsBeginImageContext( rect.size );
    [[UIImage imageNamed:@"home.png"] drawInRect:rect];
    UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    UIBarButtonItem *myHomeButton = [[UIBarButtonItem alloc] initWithImage:image style:UIBarButtonItemStyleDone target:self action:@selector(myHomeAction:)];
    
    NSArray *myButtonArray = [[NSArray alloc] initWithObjects:myHomeButton, nil];
    
    self.navigationItem.rightBarButtonItems = myButtonArray;
    
    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:NSLocalizedString(@"back", nil) style:UIBarButtonItemStylePlain target:self action:@selector(goBack:)];
    
    view_popoverMain=[[UIView alloc]initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height)];
    UIColor *background = [[UIColor alloc] initWithPatternImage:[UIImage imageNamed: @"behind_alert_view.png"]];
    [view_popoverMain setBackgroundColor:background];
    //view_popoverMain.alpha = 0.5;
    //view_popoverMain.backgroundColor = [UIColor colorWithWhite:0.6 alpha:0];
    //[self.view addSubview:view_popoverMain];
    //[view_popoverMain setHidden:YES];
    view_plain=[[UIView alloc]initWithFrame:CGRectMake(self.view.frame.size.width/2-140, self.view.frame.size.height/2-130, 280, 220)];
    [view_plain setBackgroundColor:[UIColor whiteColor]];
    
    lbllogin = [[UILabel alloc]initWithFrame:CGRectMake(view_plain.frame.size.width/2-120,view_plain.frame.size.height/2-60, 240, 40)];
    lbllogin.text = @"Please login to your account";
    lbllogin.backgroundColor = [UIColor clearColor];
    lbllogin.textColor = [UIColor blackColor];
    lbllogin.textAlignment = NSTextAlignmentCenter;
    
    buttonclose = [UIButton buttonWithType:UIButtonTypeCustom];
    [buttonclose addTarget:self
                    action:@selector(closePopover:)
          forControlEvents:UIControlEventTouchUpInside];
    //[buttonclose setTitle:@"close" forState:UIControlStateNormal];
    [buttonclose setBackgroundImage:[UIImage imageNamed:@"btn_close_x.png"] forState:UIControlStateNormal];
    buttonclose.frame = CGRectMake(view_plain.frame.size.width-60, 0, 50.0, 50.0);
    //[view_popoverMain addSubview:buttonclose];
    
    buttonlogin = [UIButton buttonWithType:UIButtonTypeCustom];
    [buttonlogin addTarget:self
                    action:@selector(logIn:)
          forControlEvents:UIControlEventTouchUpInside];
    [buttonlogin setTitle:@"Login" forState:UIControlStateNormal];
    [buttonlogin setBackgroundColor:[UIColor grayColor]];
    [buttonlogin setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    buttonlogin.frame = CGRectMake(view_plain.frame.size.width/2-50, view_plain.frame.size.height/2, 100, 40.0);
    
}

- (void)viewDidAppear:(BOOL)animated {
    [self processdata];
    //1
    CLLocationCoordinate2D zoomLocation;
    zoomLocation.latitude = [apluginvalue1 floatValue];
    zoomLocation.longitude= [apluginvalue2 floatValue];
    // 2
    MKCoordinateRegion viewRegion = MKCoordinateRegionMakeWithDistance(zoomLocation, 0.3*METERS_PER_MILE, 0.3*METERS_PER_MILE);
    [self.mapView setRegion:viewRegion animated:YES];

}

//Slide Menu
- (void)willAnimateRotationToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation duration:(NSTimeInterval)duration
{
    //NSLog(@"view frame=%@ bounds=%@", NSStringFromCGRect(self.view.frame), NSStringFromCGRect(self.view.bounds));
}


- (void)showModalButtonWasTouched
{
    self.interactor = [[BTSlideInteractor alloc] init];
    self.interactor.presenting = YES;
    
    //BTModalViewController *modalController = [self.storyboard instantiateViewControllerWithIdentifier:@"ModalViewController"];
    modalController = [[InitialSlidingViewController alloc] init];
    modalController.modalPresentationStyle = UIModalPresentationCustom;
    modalController.transitioningDelegate = self.interactor;
    [self.navigationController presentViewController:modalController animated:YES completion:nil];
    //[self.navigationController popToViewController:modalController animated:NO];
    //[self.navigationController pushViewController:modalController animated:YES];
}
// End Slide Menu

-(void)myHomeAction:(UIBarButtonItem *)sender{
    
    appdelegate.nextPage = @"";
    
    if ([appdelegate.listOfVerMenu count]>0) {
        if (appdelegate.homeclicked==NO) {
            appdelegate.homeclicked = YES;
            [self showModalButtonWasTouched];
        } else {
            appdelegate.homeclicked = NO;
            [modalController dismissViewControllerAnimated:YES completion:nil];
        }
    } else {
        //AppDelegate *appdelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
        //NSLog(@"View Controllers : %@",[appdelegate.navController viewControllers]);
        NSMutableArray *navigationArray = [[NSMutableArray alloc] initWithArray: appdelegate.navController.viewControllers];
        
        for (UIViewController* viewController in appdelegate.navController.viewControllers) {
            
            //This if condition checks whether the viewController's class is MyGroupViewController
            // if true that means its the MyGroupViewController (which has been pushed at some point)
            if ([viewController isKindOfClass:[RootViewController class]] ) {
                
                // Here viewController is a reference of UIViewController base class of MyGroupViewController
                // but viewController holds MyGroupViewController  object so we can type cast it here
                //ThemeViewController *themeViewController = (ThemeViewController*)viewController;
                //[self.navigationController popToViewController:themeViewController animated:YES];
                [navigationArray removeObjectAtIndex: 0];
            }
        }
        appdelegate.navController.viewControllers = navigationArray;
        for (UIViewController* viewController in appdelegate.navController.viewControllers) {
            
            //This if condition checks whether the viewController's class is MyGroupViewController
            // if true that means its the MyGroupViewController (which has been pushed at some point)
            if ([viewController isKindOfClass:[ViewController class]] ) {
                
                // Here viewController is a reference of UIViewController base class of MyGroupViewController
                // but viewController holds MyGroupViewController  object so we can type cast it here
                ViewController *themeViewController = (ViewController*)viewController;
                [appdelegate.navController popToViewController:themeViewController animated:NO];
                //[self.navigationController popToViewController:viewController animated:YES];
            }
        }
        //NSLog(@"View Controllers111 : %@",appdelegate.navController.viewControllers);
    }
    
}

-(void)goBack:(UIBarButtonItem *)sender{
    //varsion 1.0
    /*if ([webView canGoBack]) {
        [webView goBack];
    } else {
        [self.navigationController popViewControllerAnimated:YES];
    }*/
    
    //version 1.1
    if ([webView canGoBack]) {
        //NSLog(@"canGoBack %lu", (unsigned long)countPage);
        if (countPage>0) {
            [webView goBack];
        } else {
            [self.navigationController popViewControllerAnimated:YES];
        }
    } else {
        //NSLog(@"backHome");
        [self.navigationController popViewControllerAnimated:YES];
    }
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
}

- (BOOL)webView:(UIWebView *)webView shouldStartLoadWithRequest:(NSURLRequest *)request navigationType:(UIWebViewNavigationType)navigationType
{
    if ([[[request URL] absoluteString] hasPrefix:@"ios:"]) {
        
        // Call the given selector
        [self performSelector:@selector(webToNativeCall)];
        // Cancel the location change
        return NO;
    }
    return YES;
}

- (void)webToNativeCall
{
    [webView stringByEvaluatingJavaScriptFromString:@"getText()"];
    
}

#pragma mark -MapView Delegate Methods
//6
- (MKAnnotationView *)mapView:(MKMapView *)mapView viewForAnnotation:(id<MKAnnotation>)annotation {
    //7
    if([annotation isKindOfClass:[MKUserLocation class]])
        return nil;
    
    //8
    static NSString *identifier = @"myAnnotation";
    MKPinAnnotationView * annotationView = (MKPinAnnotationView*)[self.mapView dequeueReusableAnnotationViewWithIdentifier:identifier];
    if (!annotationView)
    {
        //9
        annotationView = [[MKPinAnnotationView alloc] initWithAnnotation:annotation reuseIdentifier:identifier];
        annotationView.pinColor = MKPinAnnotationColorPurple;
        annotationView.animatesDrop = YES;
        annotationView.canShowCallout = YES;
    }else {
        annotationView.annotation = annotation;
    }
    annotationView.rightCalloutAccessoryView = [UIButton buttonWithType:UIButtonTypeDetailDisclosure];
    return annotationView;
}

-(void) viewDidDisappear:(BOOL)animated {
    
    [super viewDidDisappear:animated];
    
    [activityIndicator removeFromSuperview];
    [[NSURLCache sharedURLCache] removeAllCachedResponses];
    
    appdelegate.homeclicked = NO;
    [modalController dismissViewControllerAnimated:YES completion:nil];
    
    //[self dismissViewControllerAnimated:YES completion:NULL];
    
}

- (void)webViewDidFinishLoad:(UIWebView *)awebView
{
    //NSLog(@"webViewDidFinishLoad = " + awebView);
    
    NSString *div = [NSString stringWithFormat: @"window.location.href ='#%@'",sub];
    NSLog(@"div = %@",div);
    countPage = webView.pageCount;

    [activityIndicator stopAnimating];
    
    if(![div isEqual: [NSNull null]]){
        [webView stringByEvaluatingJavaScriptFromString:div];
    }
    //webView =nil;
    //[webView stringByEvaluatingJavaScriptFromString:@"window.location.hash"];
    
}

//No Internet Connection error code
-(void)webView:(UIWebView *)webView didFailLoadWithError:(NSError *)error {
    UIAlertView *alert1 = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"error", nil) message:NSLocalizedString(@"MSG_CONNECTION_ERROR", nil) delegate:self cancelButtonTitle:NSLocalizedString(@"close", nil) otherButtonTitles:nil, nil];
    [alert1 show];
    [activityIndicator stopAnimating];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)closePopover:(id)sender
{
    [view_popoverMain removeFromSuperview];
    //[view_popoverMain setHidden:NO];
}

- (IBAction)logIn:(id)sender
{
    NSString *nibFileName;
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone) {
        nibFileName = @"SettingViewController";
    } else {
        nibFileName = @"SettingViewController_ipad";
    }
    SettingViewController *s = [[SettingViewController alloc] initWithNibName:nibFileName bundle:nil];
    appdelegate.shouldReturnToPrevPage = YES;
    //LoyaltyViewController *l = [[LoyaltyViewController alloc] initWithNibName:@"LoyaltyViewController" bundle:nil];
    appdelegate.originVC = self;
    [appdelegate.navController pushViewController:s animated:NO];
    
    [view_popoverMain removeFromSuperview];
}

-(NSString *) urlEncode:(NSString *) theStr {
    /*NSString *encoded = (NSString *)CFURLCreateStringByAddingPercentEscapes(
     NULL,
     (CFStringRef)theStr,
     NULL,
     (CFStringRef)@"!*'\"();:@&=+$,/?%#[]% ",
     //																		NSUnicodeStringEncoding );
     kCFStringEncodingUTF8 ); */
    NSString *encoded = (NSString *)CFBridgingRelease(CFURLCreateStringByAddingPercentEscapes(
                                                                                              NULL,
                                                                                              (__bridge CFStringRef) theStr,
                                                                                              NULL,
                                                                                              CFSTR("!*'();:@&=+$,/?%#[]\" "),
                                                                                              kCFStringEncodingUTF8));
    return encoded;
    //return nil;
}

#pragma mark CONNECT TO SERVER
-(void) connectToServer:(NSString *) urlPath {
    
    [self addLoadingView];
    NSString *urlAddress =[NSString stringWithFormat:@"%@", urlPath];
    
    //NSLog(@"URL: %@", urlAddress);
    //Create a URL object.
    /*NSURL *url = [NSURL URLWithString:urlAddress];
    
    //URL Requst Object
    //NSURLRequest *requestObj = [NSURLRequest requestWithURL:url];
    NSURLRequest *requestObj = [NSURLRequest requestWithURL:url
                                                cachePolicy:NSURLRequestReloadIgnoringLocalAndRemoteCacheData
                                            timeoutInterval:30];
    
    NSURLConnection *theConnection=[[NSURLConnection alloc] initWithRequest:requestObj delegate:self];
    if (theConnection)
    {
        // Create the NSMutableData to hold the received data.
        receivedData = [NSMutableData data];
    }
    else
    {
        // Inform the user that the connection failed.
        //[self displayAlert:@"Connection Error. Please try again later."];
        [self displayAlert:NSLocalizedString(@"MSG_CONNECTION_ERROR", nil)];
        [self removeLoadingView];
    } */
    [self getJsonResponse:urlAddress success:^(NSDictionary *responseDict) {
        
        //NSLog(@"%@",responseDict);
        [self successfunction:responseDict];
        
    } failure:^(NSError *error) {
        // release the connection, and the data object
        
        [self removeLoadingView];
        [self processdata];
        [self displayAlert:NSLocalizedString(@"MSG_CONNECTION_ERROR", nil)];
    }];
}

-(void)getJsonResponse : (NSString *)urlStr success : (void (^)(NSDictionary *responseDict))success failure:(void(^)(NSError* error))failure
{
    
    
    NSURLSession * session = [NSURLSession sharedSession];
    NSURL * url = [NSURL URLWithString: urlStr];
    
    
    // Asynchronously Api is hit here
    NSURLSessionDataTask * dataTask = [session dataTaskWithURL:url completionHandler:^(NSData *data, NSURLResponse *response, NSError *error)
                                       {
                                           
                                           //NSLog(@"%@",data);
                                           NSDictionary * json;
                                           if ([data length]>0) {
                                               json  = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
                                               //NSLog(@"%@",json);
                                               success(json);
                                           } else {
                                               json = nil;
                                               dispatch_async(dispatch_get_main_queue(), ^ {
                                                   [self removeLoadingView];
                                                   [self displayAlert:NSLocalizedString(@"MSG_CONNECTION_ERROR", nil)];
                                               });
                                           }
                                           
                                           
                                       }];
    
    [dataTask resume] ; // Executed First
    
    
}

-(void) successfunction:(NSDictionary *)responseDict {
    //NSString *jsonString = [responseDict JSONRepresentation];
    dispatch_async(dispatch_get_main_queue(), ^{
        if ([currentRequestMenu isEqualToString:@"AddSession"]){
            //if([receivedData length]>10)
            //{
            NSDictionary *dictionaryadddel = responseDict;
            //NSLog(@"AddFavorite %@",dictionaryadddel);
            for(NSDictionary *row in dictionaryadddel)
            {
                if ([[row objectForKey:@"status"] isEqualToString:@"1"]) {
                    
                    //Success
                    webView = [[UIWebView alloc] init];
                    webView.backgroundColor = [UIColor clearColor];
                    //[webView setFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height)];
                    webView.delegate = self;
                    
                    //initialize the activity indicator
                    
                    activityIndicator=[[UIActivityIndicatorView alloc]initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhiteLarge];
                    
                    //Change the color of the indicator, this override the color set by UIActivityIndicatorViewStyleWhiteLarge
                    activityIndicator.color=[UIColor blackColor];
                    
                    //Put the indicator on the center of the webview
                    [activityIndicator setCenter:self.view.center];
                    
                    //Add the indicator to the webView to make it visible
                    [webView addSubview:activityIndicator];
                    //[webView setScalesPageToFit:YES];
                    
                    [activityIndicator startAnimating];
                    
                    NSString *urlAddress = apluginvalue2;
                    NSRange rangeurl = [urlAddress rangeOfString:@"?"];
                    if(rangeurl.location != NSNotFound) {
                        //found
                        urlAddress = [NSString stringWithFormat: @"%@&f=1&cusid=%d&cuid=%@&udid=%@&session=%d", urlAddress, session_id, [self urlEncode:appdelegate.customer_id], appdelegate.uuid, session_id];
                    } else {
                        urlAddress = [NSString stringWithFormat: @"%@?f=1&cusid=%d&cuid=%@&udid=%@&session=%d", urlAddress, session_id, [self urlEncode:appdelegate.customer_id], appdelegate.uuid, session_id];
                    }
                    //NSLog(@"urlAddresshtml5c %@", urlAddress);
                    NSURL *url = [NSURL URLWithString:urlAddress];
                    
                    NSString *html = [NSString stringWithContentsOfURL:url encoding:NSUTF8StringEncoding error:nil];
                    NSRange range = [html rangeOfString:@"<body"];
                    //NSLog(@"html %@", html);
                    if(range.location != NSNotFound) {
                        // Adjust style for mobile
                        //float inset = 40;
                        NSString *style = [NSString stringWithFormat:@"<style>div {max-width: %d%%;width: %fpx;height: %fpx;}</style>", 100, self.view.frame.size.width, self.view.frame.size.height];
                        html = [NSString stringWithFormat:@"%@%@%@", [html substringToIndex:range.location], style, [html substringFromIndex:range.location]];
                    }
                    [webView loadHTMLString:html baseURL:url];
                    
                    float h;
                    h = [[webView stringByEvaluatingJavaScriptFromString: @"document.body.offsetHeight;"] integerValue];
                    //NSLog(@"web view is %f high", webView.frame.size.height);
                    NSString *heightString = [webView stringByEvaluatingJavaScriptFromString:@"document.body.offsetHeight;"];
                    //NSLog(@"web content is %@ high",heightString);
                    h = [heightString floatValue]; // convert from string to float plus some extra points because calculation is sometimes one line short
                    //h = h + 70;
                    webView.frame = CGRectMake(0, 0, self.view.frame.size.width, h);
                    // get bottom of text field
                    //h = h + 70; // extra 70 pixels for UIButton at bottom and padding.
                    //[scrollView setContentSize:CGSizeMake(self.view.frame.size.width, h)];
                    [self.view addSubview:webView];
                    //[self.view addSubview:webView];
                    //[awebView setScalesPageToFit:NO];
                    //awebView.multipleTouchEnabled=YES;
                    
                } else {
                    //Failed
                    [self displayAlert:NSLocalizedString(@"please_retry_again", nil)];
                }
            }
            //[self processdata];
            //}
        }
        //response =nil;
        [self removeLoadingView];
    });
}

/*- (void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response
{
    [receivedData setLength:0];
}

- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data
{
    [receivedData appendData:data];
}

- (void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error {
    // release the connection, and the data object
    
    connection =nil;
    receivedData =nil;
    
    //NSLog(@"%@",error.description);
    
    [self removeLoadingView];
    [self processdata];
    [self displayAlert:NSLocalizedString(@"MSG_CONNECTION_ERROR", nil)];
}

- (void)connectionDidFinishLoading:(NSURLConnection *)connection
{
    
    NSString *response = [[NSString alloc] initWithData:receivedData encoding:NSUTF8StringEncoding];
    
    if ([currentRequestMenu isEqualToString:@"AddSession"]){
        //if([receivedData length]>10)
        //{
        NSDictionary *dictionaryadddel = [response JSONValue];
        //NSLog(@"AddFavorite %@",dictionaryadddel);
        for(NSDictionary *row in dictionaryadddel)
        {
            if ([[row objectForKey:@"status"] isEqualToString:@"1"]) {
                //Success
                webView = [[UIWebView alloc] init];
                webView.backgroundColor = [UIColor clearColor];
                //[webView setFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height)];
                webView.delegate = self;
                
                //initialize the activity indicator
                
                activityIndicator=[[UIActivityIndicatorView alloc]initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhiteLarge];
                
                //Change the color of the indicator, this override the color set by UIActivityIndicatorViewStyleWhiteLarge
                activityIndicator.color=[UIColor blackColor];
                
                //Put the indicator on the center of the webview
                [activityIndicator setCenter:self.view.center];
                
                //Add the indicator to the webView to make it visible
                [webView addSubview:activityIndicator];
                //[webView setScalesPageToFit:YES];
                
                [activityIndicator startAnimating];
                
                NSString *urlAddress = apluginvalue2;
                NSRange rangeurl = [urlAddress rangeOfString:@"?"];
                if(rangeurl.location != NSNotFound) {
                    //found
                    urlAddress = [NSString stringWithFormat: @"%@&f=1&cusid=%d&cuid=%@", urlAddress, session_id, [self urlEncode:appdelegate.customer_id]];
                } else {
                    urlAddress = [NSString stringWithFormat: @"%@?f=1&cusid=%d&cuid=%@", urlAddress, session_id, [self urlEncode:appdelegate.customer_id]];
                }
                //NSLog(@"urlAddresshtml5c %@", urlAddress);
                NSURL *url = [NSURL URLWithString:urlAddress];
                
                NSString *html = [NSString stringWithContentsOfURL:url encoding:NSUTF8StringEncoding error:nil];
                NSRange range = [html rangeOfString:@"<body"];
                //NSLog(@"html %@", html);
                if(range.location != NSNotFound) {
                    // Adjust style for mobile
                    //float inset = 40;
                    NSString *style = [NSString stringWithFormat:@"<style>div {max-width: %d%%;width: %fpx;height: %fpx;}</style>", 100, self.view.frame.size.width, self.view.frame.size.height];
                    html = [NSString stringWithFormat:@"%@%@%@", [html substringToIndex:range.location], style, [html substringFromIndex:range.location]];
                }
                [webView loadHTMLString:html baseURL:url];
                
                float h;
                h = [[webView stringByEvaluatingJavaScriptFromString: @"document.body.offsetHeight;"] integerValue];
                //NSLog(@"web view is %f high", webView.frame.size.height);
                NSString *heightString = [webView stringByEvaluatingJavaScriptFromString:@"document.body.offsetHeight;"];
                //NSLog(@"web content is %@ high",heightString);
                h = [heightString floatValue]; // convert from string to float plus some extra points because calculation is sometimes one line short
                //h = h + 70;
                webView.frame = CGRectMake(0, 0, self.view.frame.size.width, h);
                // get bottom of text field
                //h = h + 70; // extra 70 pixels for UIButton at bottom and padding.
                //[scrollView setContentSize:CGSizeMake(self.view.frame.size.width, h)];
                [self.view addSubview:webView];
                //[self.view addSubview:webView];
                //[awebView setScalesPageToFit:NO];
                //awebView.multipleTouchEnabled=YES;
                
            } else {
                //Failed
                [self displayAlert:NSLocalizedString(@"please_retry_again", nil)];
            }
        }
        //[self processdata];
        //}
    }
    //response =nil;
    [self removeLoadingView];
    
} */

-(void) addLoadingView
{
    loadingView = [LoadingView loadingViewInView:self.view];
    
}

-(void) removeLoadingView
{
    [loadingView removeView];
}

-(void) displayAlert: (NSString *) stralert {
    NSString *str = [[NSString alloc] initWithFormat:@"%@", stralert];
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:appdelegate.iconname
                                                    message:str
                                                   delegate:self
                                          cancelButtonTitle:NSLocalizedString(@"done", nil)
                                          otherButtonTitles:nil];
    
    [alert show];
    str =nil;
    alert =nil;
}

@end
