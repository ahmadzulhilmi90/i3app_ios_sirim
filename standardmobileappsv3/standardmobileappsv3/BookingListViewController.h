//
//  BookingListViewController.h
//  standardmobileapps
//
//  Created by Hazwan on 12/17/13.
//  Copyright (c) 2013 Maggie. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "WEPopoverController.h"
#import <MessageUI/MessageUI.h>

@interface BookingListViewController : UIViewController <UITableViewDelegate, UITableViewDataSource, UIScrollViewDelegate, UITextFieldDelegate, UITextViewDelegate, UITableViewDelegate, WEPopoverControllerDelegate, UIPopoverControllerDelegate, UIAlertViewDelegate, UIGestureRecognizerDelegate, MFMailComposeViewControllerDelegate, UIViewControllerTransitioningDelegate> {
    
    int noOfline;
    
    int labelheight;
    
    UIScrollView *scrollviewAdd;

    NSMutableArray *listOfContent;
    NSMutableArray *listOfbranch;
    NSMutableArray *listOfprod;
    NSMutableArray *listOfItem;
    UITableView *branchTableView;
    UITableView *prodTableView;
    UITableView *dselectTableView;
    UITableView *dendselectTableView;
    UITableView *tselectTableView;
    UITableView *tendselectTableView;
    UITableView *totalGuestTableView;
    UITableView *multiprodTableView;
    UITableView *qtyTableView;
    
    UIViewController *branchContent;
    UIViewController *prodContent;
    UIViewController *dselectContent;
    
    WEPopoverController *branchPopoverController;
    WEPopoverController *prodPopoverController;
    WEPopoverController *dselectPopoverController;
    
    UITextField *tf;
    UITextField *tf1;
    UITextField *tf2;
    UITextField *tf3;
    UITextField *tf4;
    UITextField *tf5;
    UITextField *tf6;
    UITextField *tf7;
    
    NSString *branchid;
    NSString *brtel;
    NSString *bremail;
    NSString *brlat;
    NSString *brlon;
    NSString *brdet;
    NSString *prodid;
    NSString *protel;
    NSString *proemail;
    NSString *prolat;
    NSString *prolon;
    NSString *prodet;
    NSMutableArray *listAvaiDate;
    NSMutableArray *listAvaiDateTime;
    NSMutableArray *listAvaiDateTimeOri;
    
    NSString *prodQty;
    NSString *prodPrice;
    
    UITextField *dqtyselected;
    
    long currentIndexSection;
    
}

@property (nonatomic, strong) NSMutableArray *arrayBookingList;

@property (retain, nonatomic) IBOutlet UITableView *table_bookingList;
@property (retain, nonatomic) IBOutlet UITableView *table_bookingMenu;

@property (retain, nonatomic) IBOutlet UINavigationBar *navBar;
@property (retain, nonatomic) IBOutlet UILabel *label_reservationNo;
@property (retain, nonatomic) IBOutlet UILabel *label_customerName;
@property (retain, nonatomic) IBOutlet UILabel *label_reserveDate;
@property (retain, nonatomic) IBOutlet UILabel *label_reserveStatus;
@property (retain, nonatomic) IBOutlet UILabel *label_branchAddress;
@property (retain, nonatomic) IBOutlet UILabel *label_branchStreet;
@property (retain, nonatomic) IBOutlet UILabel *label_branchPostCodeAndBranchName;
@property (retain, nonatomic) IBOutlet UILabel *label_branchState;
@property (retain, nonatomic) IBOutlet UILabel *label_branchCountry;
@property (retain, nonatomic) IBOutlet UILabel *label_branchName;
@property (retain, nonatomic) IBOutlet UILabel *label_reserveTitle;
@property (retain, nonatomic) IBOutlet UILabel *label_reserveTq;
@property (retain, nonatomic) IBOutlet UILabel *label_hline;

@property (retain, nonatomic) IBOutlet UIView *view_bookingDetail;
@property (retain, nonatomic) IBOutlet UIScrollView *scrollView;
@property (retain, nonatomic) IBOutlet UIScrollView *scrollviewAdd;

@property (strong, nonatomic) NSString *cont_title;

//@property (nonatomic, retain) NSMutableData *receivedData3;
@property (nonatomic, retain) NSMutableArray *listOfContent;
@property (nonatomic, retain) NSMutableArray *listOfbranch;
@property (nonatomic, retain) NSMutableArray *listOfprod;
@property (nonatomic, retain) NSMutableArray *listOfprodcat;
@property (nonatomic, retain) NSMutableArray *listOfItem;

@property (nonatomic, retain) WEPopoverController *branchPopoverController;
@property (nonatomic, retain) WEPopoverController *prodPopoverController;
@property (nonatomic, retain) WEPopoverController *dselectPopoverController;

@property (retain, nonatomic) IBOutlet UIView *view_bookingAdd;
@property (retain, nonatomic) IBOutlet UIView *view_bookingList;
/*@property (retain, nonatomic) IBOutlet UILabel *label_customerName;
@property (retain, nonatomic) IBOutlet UILabel *label_reserveDate;
@property (retain, nonatomic) IBOutlet UILabel *label_reserveStatus; */

@property (retain, nonatomic) IBOutlet UIView *view_popoverMain;

@property (retain, nonatomic) NSMutableArray *listAvaiDate;
@property (retain, nonatomic) NSMutableArray *listAvaiDateTime;
@property (retain, nonatomic) NSMutableArray *listAvaiDateTimeOri;

@property (strong, nonatomic) IBOutlet UIButton *btnList;
@property (strong, nonatomic) IBOutlet UIButton *btnAddNew;

@property (assign, nonatomic) BOOL isFromSubmit;

//@property (nonatomic, retain) IBOutlet UITextField *dqty;

- (IBAction)onListClicked:(id)sender;
- (IBAction)onAddNewClicked:(id)sender;
- (IBAction)logIn:(id)sender;
- (IBAction)closeLoginPopup:(id)sender;

-(void) connectToServer:(NSString *) urlPath;

@end
