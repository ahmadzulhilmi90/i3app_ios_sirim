//
//  BookingListViewController.m
//  standardmobileapps
//
//  Created by Hazwan on 12/17/13.
//  Copyright (c) 2013 Maggie. All rights reserved.
//

#import "BookingListViewController.h"
#import "AppDelegate.h"
#import "LoadingView.h"
#import "SBJSON.h"
#import "MapViewController.h"
#import <QuartzCore/QuartzCore.h>
#import <SystemConfiguration/SystemConfiguration.h>
#import "NSString+HTML.h"
#import "RootViewController.h"
#import "ViewController.h"
#import "InitialSlidingViewController.h"
#import "BTSlideInteractor.h"
#import "Reachability.h"

@interface BookingListViewController () {
    NSMutableArray      *sectionTitleArray;
    NSMutableDictionary *sectionContentDict;
    NSMutableArray      *arrayForBool;
    NSString *val_multiprod, *val_bookingtitle, *val_tq_message, *val_booking_title, *display_price, *display_total, *productname, *display_qtt, *product_err, *qtyselected;
    NSMutableArray *arSelectedRows, *textfieldSelectedRows, *prodSelectedRows;
    NSArray *strings;
}

@property (strong, nonatomic) BTSlideInteractor *interactor;
@end

@implementation BookingListViewController
LoadingView *loadingView;

InitialSlidingViewController *modalController;

AppDelegate *appdelegate;
//NSMutableData *receivedData;
CGFloat scrollViewHeight;
UITextView *newTextView;
NSString *max_guesstotal;

NSString *currentRequest, *branchTel, *branchdetails, *reservationId, *statusOrder;

@synthesize arrayBookingList;
@synthesize table_bookingList,table_bookingMenu;
@synthesize navBar, view_bookingDetail;
@synthesize label_branchAddress,label_branchCountry,label_branchPostCodeAndBranchName,label_branchState,label_branchStreet,label_customerName,label_reservationNo,label_reserveDate,label_reserveStatus, label_branchName, label_reserveTitle, label_reserveTq, label_hline;
@synthesize scrollviewAdd, cont_title, view_bookingAdd, view_bookingList, scrollView;
@synthesize listOfContent, listOfbranch, listOfprod, listOfprodcat, listOfItem, prodPopoverController, branchPopoverController, dselectPopoverController;
@synthesize view_popoverMain, listAvaiDate, listAvaiDateTime, listAvaiDateTimeOri, btnAddNew, btnList, isFromSubmit;
//@synthesize dqty;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        appdelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    }
    return self;
}

- (UIColor *) colorWithHexString:(NSString *)hexString {
    NSString *colorString = [[hexString stringByReplacingOccurrencesOfString: @"#" withString: @""] uppercaseString];
    CGFloat alpha, red, blue, green;
    alpha = 1.0f;
    red   = [self colorComponentFrom: colorString start: 0 length: 2];
    green = [self colorComponentFrom: colorString start: 2 length: 2];
    blue  = [self colorComponentFrom: colorString start: 4 length: 2];
    return [UIColor colorWithRed: red green: green blue: blue alpha: alpha];
}

- (CGFloat) colorComponentFrom:(NSString *)string start:(NSUInteger)start length:(NSUInteger)length {
    NSString *substring = [string substringWithRange: NSMakeRange(start, length)];
    NSString *fullHex = length == 2 ? substring : [NSString stringWithFormat: @"%@%@", substring, substring];
    unsigned hexComponent;
    [[NSScanner scannerWithString: fullHex] scanHexInt: &hexComponent];
    return hexComponent / 255.0;
}

-(void)viewWillDisappear:(BOOL)animated {
    [branchPopoverController dismissPopoverAnimated:NO];
    //[branchPopoverController release];
    branchPopoverController = nil;
    [prodPopoverController dismissPopoverAnimated:NO];
    //[prodPopoverController release];
    prodPopoverController = nil;
    [dselectPopoverController dismissPopoverAnimated:NO];
    //[dselectPopoverController release];
    dselectPopoverController = nil;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    [btnAddNew setFrame:CGRectMake(1, 1, (self.view.frame.size.width-4)/2, 47)];
    btnAddNew.backgroundColor = [self colorWithHexString:appdelegate.themeColorCode1];
    [btnList setFrame:CGRectMake(((self.view.frame.size.width-4)/2)+2, 1, (self.view.frame.size.width-4)/2, 47)];
    btnList.backgroundColor = [self colorWithHexString:appdelegate.themeColorCode1];

    scrollviewAdd = [[UIScrollView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height)];
    
    self.title = cont_title;
    appdelegate.homeclicked = NO;
    
    self.navigationItem.backBarButtonItem = [[UIBarButtonItem alloc]initWithTitle:@"" style:UIBarButtonItemStylePlain target:nil action:nil];
    CGRect rect = CGRectMake(0,0,20,20);
    UIGraphicsBeginImageContext( rect.size );
    [[UIImage imageNamed:@"home.png"] drawInRect:rect];
    UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    UIBarButtonItem *myHomeButton = [[UIBarButtonItem alloc] initWithImage:image style:UIBarButtonItemStyleDone target:self action:@selector(myHomeAction:)];
    
    
    NSArray *myButtonArray = [[NSArray alloc] initWithObjects:myHomeButton, nil];
    
    self.navigationItem.rightBarButtonItems = myButtonArray;
    
    branchTableView = [[UITableView alloc] init];
	branchTableView.delegate = self;
	branchTableView.dataSource = self;
    
    prodTableView = [[UITableView alloc] init];
	prodTableView.delegate = self;
	prodTableView.dataSource = self;
    
    dselectTableView = [[UITableView alloc] init];
	dselectTableView.delegate = self;
	dselectTableView.dataSource = self;
    
    dendselectTableView = [[UITableView alloc] init];
	dendselectTableView.delegate = self;
	dendselectTableView.dataSource = self;
    
    tselectTableView = [[UITableView alloc] init];
	tselectTableView.delegate = self;
	tselectTableView.dataSource = self;
    
    tendselectTableView = [[UITableView alloc] init];
	tendselectTableView.delegate = self;
	tendselectTableView.dataSource = self;
    
    totalGuestTableView = [[UITableView alloc] init];
	totalGuestTableView.delegate = self;
	totalGuestTableView.dataSource = self;
    
    qtyTableView = [[UITableView alloc] init];
    qtyTableView.delegate = self;
    qtyTableView.dataSource = self;
    
    multiprodTableView = [[UITableView alloc] init];
    multiprodTableView.delegate = self;
    multiprodTableView.dataSource = self;
    
    listAvaiDate = [[NSMutableArray alloc] init];
    listAvaiDateTime = [[NSMutableArray alloc] init];
    listAvaiDateTimeOri = [[NSMutableArray alloc] init];
    
    [table_bookingList reloadData];
    
    arSelectedRows = [[NSMutableArray alloc] init];
    textfieldSelectedRows = [[NSMutableArray alloc] init];
    prodSelectedRows = [[NSMutableArray alloc] init];
    
    branchid = @"";
    prodid = @"";
    prodQty = @"";
    prodPrice = @"";
    
    tf = [[UITextField alloc] init];
    tf1 = [[UITextField alloc] init];
    tf2 = [[UITextField alloc] init];
    tf3 = [[UITextField alloc] init];
    tf4 = [[UITextField alloc] init];
    tf6 = [[UITextField alloc] init];
    tf5 = [[UITextField alloc] init];
    tf7 = [[UITextField alloc] init];
    newTextView = [[UITextView alloc] init];
    dqtyselected = [[UITextField alloc] init];
    
    multiprodTableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
    
    [view_bookingAdd setAutoresizingMask:UIViewAutoresizingFlexibleWidth|
     UIViewAutoresizingFlexibleHeight];
    
}

//Slide Menu
- (void)willAnimateRotationToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation duration:(NSTimeInterval)duration
{
    //NSLog(@"view frame=%@ bounds=%@", NSStringFromCGRect(self.view.frame), NSStringFromCGRect(self.view.bounds));
}


- (void)showModalButtonWasTouched
{
    self.interactor = [[BTSlideInteractor alloc] init];
    self.interactor.presenting = YES;
    
    //BTModalViewController *modalController = [self.storyboard instantiateViewControllerWithIdentifier:@"ModalViewController"];
    modalController = [[InitialSlidingViewController alloc] init];
    modalController.modalPresentationStyle = UIModalPresentationCustom;
    modalController.transitioningDelegate = self.interactor;
    [self.navigationController presentViewController:modalController animated:YES completion:nil];
    //[self.navigationController popToViewController:modalController animated:NO];
    //[self.navigationController pushViewController:modalController animated:YES];
}
// End Slide Menu

-(void)myHomeAction:(UIBarButtonItem *)sender{
    
    appdelegate.nextPage = @"";
    
    if ([appdelegate.listOfVerMenu count]>0) {
        if (appdelegate.homeclicked==NO) {
            appdelegate.homeclicked = YES;
            [self showModalButtonWasTouched];
        } else {
            appdelegate.homeclicked = NO;
            [modalController dismissViewControllerAnimated:YES completion:nil];
        }
    } else {
        //AppDelegate *appdelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
        //NSLog(@"View Controllers : %@",[appdelegate.navController viewControllers]);
        NSMutableArray *navigationArray = [[NSMutableArray alloc] initWithArray: appdelegate.navController.viewControllers];
        
        for (UIViewController* viewController in appdelegate.navController.viewControllers) {
            
            //This if condition checks whether the viewController's class is MyGroupViewController
            // if true that means its the MyGroupViewController (which has been pushed at some point)
            if ([viewController isKindOfClass:[RootViewController class]] ) {
                
                // Here viewController is a reference of UIViewController base class of MyGroupViewController
                // but viewController holds MyGroupViewController  object so we can type cast it here
                //ThemeViewController *themeViewController = (ThemeViewController*)viewController;
                //[self.navigationController popToViewController:themeViewController animated:YES];
                [navigationArray removeObjectAtIndex: 0];
            }
        }
        appdelegate.navController.viewControllers = navigationArray;
        for (UIViewController* viewController in appdelegate.navController.viewControllers) {
            
            //This if condition checks whether the viewController's class is MyGroupViewController
            // if true that means its the MyGroupViewController (which has been pushed at some point)
            if ([viewController isKindOfClass:[ViewController class]] ) {
                
                // Here viewController is a reference of UIViewController base class of MyGroupViewController
                // but viewController holds MyGroupViewController  object so we can type cast it here
                ViewController *themeViewController = (ViewController*)viewController;
                [appdelegate.navController popToViewController:themeViewController animated:NO];
                //[self.navigationController popToViewController:viewController animated:YES];
            }
        }
        //NSLog(@"View Controllers111 : %@",appdelegate.navController.viewControllers);
    }
    
}

- (void)viewDidAppear:(BOOL)animated
{
    [self aMethod:nil];

}

-(void)viewWillAppear:(BOOL)animated
{
    
}

-(void) viewDidDisappear:(BOOL)animated {
    
    [super viewDidDisappear:animated];
    
    appdelegate.homeclicked = NO;
    [modalController dismissViewControllerAnimated:YES completion:nil];
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)goback
{
    [appdelegate.navController popViewControllerAnimated:NO];
}


-(void) getItem {
    sqlite3 *database;
    
    if(sqlite3_open([appdelegate.databasePath UTF8String], &database) == SQLITE_OK) {
        // Setup the SQL Statement and compile it for faster access
        
        NSString *sqlItem;
        NSString *telephoneTitle, *telephone, *emailTitle, *email, *mapTitle, *mapLon, *mapLat;
        [listOfItem removeAllObjects];
        
        sqlItem = [[NSString alloc] initWithFormat:@"SELECT * FROM contents WHERE id = '%@' ", branchTel];
        
        const char *sqlStatement = [sqlItem UTF8String];
        sqlite3_stmt *compiledStatement;
        if(sqlite3_prepare_v2(database, sqlStatement, -1, &compiledStatement, NULL) == SQLITE_OK) {
            
            NSDictionary *dictionary;
            listOfItem = [[NSMutableArray alloc] init];
            while(sqlite3_step(compiledStatement) == SQLITE_ROW) {
                
                telephoneTitle = [NSString stringWithUTF8String:(char *)sqlite3_column_text(compiledStatement, 5)];
                telephone = [NSString stringWithUTF8String:(char *)sqlite3_column_text(compiledStatement, 6)];
                emailTitle = [NSString stringWithUTF8String:(char *)sqlite3_column_text(compiledStatement, 7)];
                email = [NSString stringWithUTF8String:(char *)sqlite3_column_text(compiledStatement, 8)];
                branchdetails = [NSString stringWithUTF8String:(char *)sqlite3_column_text(compiledStatement, 9)];
                mapTitle = [NSString stringWithUTF8String:(char *)sqlite3_column_text(compiledStatement, 12)];
                mapLon = [NSString stringWithUTF8String:(char *)sqlite3_column_text(compiledStatement, 14)];
                mapLat = [NSString stringWithUTF8String:(char *)sqlite3_column_text(compiledStatement, 13)];
                
                //NSLog(@"branchdetails :: %@", branchdetails);
                
                telephoneTitle = [telephoneTitle stringByDecodingHTMLEntities];
                telephoneTitle = [telephoneTitle stringByReplacingOccurrencesOfString:@"''" withString:@"'"];
                
                emailTitle = [emailTitle stringByDecodingHTMLEntities];
                emailTitle = [emailTitle stringByReplacingOccurrencesOfString:@"''" withString:@"'"];
                
                mapTitle = [mapTitle stringByDecodingHTMLEntities];
                mapTitle = [mapTitle stringByReplacingOccurrencesOfString:@"''" withString:@"'"];
                
                if ([telephone length]>0) {
                    dictionary = [NSDictionary dictionaryWithObjectsAndKeys:@"Call",@"title",telephoneTitle,@"titlename",telephone,@"value1",@"",@"value2",nil];
                    [listOfItem addObject:dictionary];
                }
                if ([email length]>0) {
                    dictionary = [NSDictionary dictionaryWithObjectsAndKeys:@"Email",@"title",emailTitle,@"titlename",email,@"value1",@"",@"value2",nil];
                    [listOfItem addObject:dictionary];
                }
                if ([mapLon floatValue]>0 && [mapLat floatValue]>0) {
                    dictionary = [NSDictionary dictionaryWithObjectsAndKeys:@"Map",@"title",mapTitle,@"titlename",mapLat,@"value1",mapLon,@"value2",nil];
                    [listOfItem addObject:dictionary];
                }
            }
            //NSLog(@"statusOrderMaggie");
            if ([statusOrder isEqualToString:@"1"]) {
                dictionary = [NSDictionary dictionaryWithObjectsAndKeys:@"Cancel",@"title",@"Cancel",@"titlename",@"",@"value1",@"",@"value2",nil];
                [listOfItem addObject:dictionary];
            }
        }
        sqlite3_finalize(compiledStatement);
        
    }
    sqlite3_close(database);
    [table_bookingMenu reloadData];
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    if(tableView == multiprodTableView){
        return [sectionTitleArray count];
    }
    
    return 1;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    if(tableView == multiprodTableView){
        UIView *headerView              = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, 50)];
        headerView.tag                  = section;
        headerView.backgroundColor      = [UIColor whiteColor];
        UILabel *headerString           = [[UILabel alloc] initWithFrame:CGRectMake(10, 0, self.view.frame.size.width-20-50, 50)];
        BOOL manyCells                  = [[arrayForBool objectAtIndex:section] boolValue];
        /*if (!manyCells) {
            headerString.text = @"click to enlarge";
        }else{
            headerString.text = @"click again to reduce";
        } */
        headerString.text = [sectionTitleArray objectAtIndex:section];
        headerString.textAlignment      = NSTextAlignmentLeft;
        headerString.lineBreakMode      = NSLineBreakByTruncatingTail;
        headerString.textColor          = [UIColor blackColor];
        headerString.adjustsFontSizeToFitWidth = YES;
        //headerString.minimumFontSize = 0;
        [headerView addSubview:headerString];
        
        UITapGestureRecognizer  *headerTapped   = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(sectionHeaderTapped:)];
        [headerView addGestureRecognizer:headerTapped];
        
        //up or down arrow depending on the bool
        UIImageView *upDownArrow        = [[UIImageView alloc] initWithImage:manyCells ? [UIImage imageNamed:@"upArrowBlack"] : [UIImage imageNamed:@"downArrowBlack"]];
        upDownArrow.autoresizingMask    = UIViewAutoresizingFlexibleLeftMargin;
        upDownArrow.frame               = CGRectMake(self.view.frame.size.width-40, 10, 30, 30);
        headerView.backgroundColor = [UIColor colorWithRed:238/255.0 green:238/255.0 blue:238/255.0 alpha:1];
        [headerView addSubview:upDownArrow];
        
        return headerView;
    }
    
    return nil;
}

/*- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section{
    if(tableView == multiprodTableView){
        UIView *footer  = [[UIView alloc] initWithFrame:CGRectZero];
        return footer;
    }
    
    return nil;
} */

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    if(tableView == multiprodTableView){
        return 50;
    }
    return 0;
}
/*- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section{
    if(tableView == multiprodTableView){
        return 1;
    }
    return 0;
} */

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (tableView == table_bookingList)
        return [arrayBookingList count];
    
    if (tableView == table_bookingMenu)
        return [listOfItem count];
    
    if(tableView == branchTableView){
		return [listOfbranch count];
    }
    
    if(tableView == prodTableView){
		return [listOfprod count];
    }
    
    if(tableView == dselectTableView){
		return [listAvaiDate count];
    }
    
    if(tableView == dendselectTableView){
		return [listAvaiDate count];
    }
    
    if(tableView == tselectTableView){
        if ([listAvaiDateTime count]> 0) {
            return [listAvaiDateTime count];
        } else {
            return 1;
        }
    }
    
    if(tableView == tendselectTableView){
        if ([listAvaiDateTime count]> 0) {
            return [listAvaiDateTime count];
        } else {
            return 1;
        }
    }
    
    if(tableView == totalGuestTableView){
        return [max_guesstotal intValue];
    }
    
    if(tableView == qtyTableView){
        return 10;
    }
    
    if(tableView == multiprodTableView){
        if ([[arrayForBool objectAtIndex:section] boolValue]) {
            return [[sectionContentDict valueForKey:[sectionTitleArray objectAtIndex:section]] count];
        }
    }
    
    return 1;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (tableView == table_bookingMenu){
        return 44;
    } else if(tableView == branchTableView){
		return 30;
    } else if(tableView == prodTableView){
		return 30;
    } else if(tableView == dselectTableView){
		return 30;
    } else if(tableView == dendselectTableView){
		return 30;
    } else if(tableView == tselectTableView){
		return 30;
    } else if(tableView == tendselectTableView){
		return 30;
    } else if(tableView == totalGuestTableView){
		return 30;
    } else if(tableView == qtyTableView){
        return 30;
    } else if (tableView == table_bookingList){
        if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone) {
            return 70;
        } else {
            return 100;
        }
    }
    
    if(tableView == multiprodTableView){
        if ([[arrayForBool objectAtIndex:indexPath.section] boolValue]) {
            return 50;
        }
    }
    
    //return 44;
    return 1;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell;
    
    if (tableView == table_bookingList)
    {
        
        UILabel *orderNo;
        const NSInteger LEFT_TAG = 2000;
        UILabel *date;
        const NSInteger RIGHT_TAG = 2001;
        UILabel *branch;
        const NSInteger RIGHT_TAG1 = 2002;
        UILabel *status;
        const NSInteger RIGHT_TAG2 = 2003;
        
        if (cell == nil)
        {
            
            cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:@"bookingCell"];
            //cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
            
            if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone) {
                orderNo = [[UILabel alloc] initWithFrame: CGRectMake(5, 5, table_bookingList.frame.size.width-10, 20)];
                [orderNo setFont:[UIFont boldSystemFontOfSize:16]];
            } else {
                orderNo = [[UILabel alloc] initWithFrame: CGRectMake(5, 5, table_bookingList.frame.size.width-10, 30)];
                [orderNo setFont:[UIFont boldSystemFontOfSize:18]];
            }
            orderNo.tag = LEFT_TAG;
            orderNo.textColor = [UIColor grayColor];
            orderNo.textAlignment = NSTextAlignmentLeft;
            orderNo.backgroundColor = [UIColor clearColor];
            orderNo.numberOfLines = 0;
            [cell.contentView addSubview:orderNo];
            
            if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone) {
                date = [[UILabel alloc] initWithFrame: CGRectMake(5, 25, table_bookingList.frame.size.width-10-80, 20)];
                [date setFont:[UIFont boldSystemFontOfSize:16]];
            } else {
                date = [[UILabel alloc] initWithFrame: CGRectMake(5, 35, table_bookingList.frame.size.width-10-80, 30)];
                [date setFont:[UIFont boldSystemFontOfSize:18]];
            }
            date.tag = RIGHT_TAG;
            date.textColor = [UIColor grayColor];
            date.textAlignment = NSTextAlignmentLeft;
            date.backgroundColor = [UIColor clearColor];
            date.numberOfLines = 0;
            [cell.contentView addSubview:date];
            
            if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone) {
                branch = [[UILabel alloc] initWithFrame: CGRectMake(5,45, table_bookingList.frame.size.width-10, 20)];
                [branch setFont:[UIFont boldSystemFontOfSize:16]];
            } else {
                branch = [[UILabel alloc] initWithFrame: CGRectMake(5,65, table_bookingList.frame.size.width-10, 30)];
                [branch setFont:[UIFont boldSystemFontOfSize:18]];
            }
            branch.tag = RIGHT_TAG1;
            branch.textColor = [UIColor grayColor];
            branch.textAlignment = NSTextAlignmentLeft;
            branch.backgroundColor = [UIColor clearColor];
            branch.numberOfLines = 0;
            [cell.contentView addSubview:branch];
            
            if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone) {
                status = [[UILabel alloc] initWithFrame: CGRectMake(25+table_bookingList.frame.size.width-10-90, 25, 70, 20)];
                [status setFont:[UIFont boldSystemFontOfSize:16]];
            } else {
                status = [[UILabel alloc] initWithFrame: CGRectMake(35+table_bookingList.frame.size.width-10-90, 35, 70, 30)];
                [status setFont:[UIFont boldSystemFontOfSize:18]];
            }
            status.tag = RIGHT_TAG2;
            status.textColor = [UIColor grayColor];
            status.textAlignment = NSTextAlignmentCenter;
            status.backgroundColor = [UIColor clearColor];
            status.numberOfLines = 0;
            [cell.contentView addSubview:status];
            
        }
        else
        {
            orderNo = (UILabel *)[cell viewWithTag:LEFT_TAG];
            //} else if ([[dictionary objectForKey:@"object_type"] isEqualToString:@"text"]) {
            date = (UILabel *)[cell viewWithTag:RIGHT_TAG];
            branch = (UILabel *)[cell viewWithTag:RIGHT_TAG1];
            status = (UILabel *)[cell viewWithTag:RIGHT_TAG2];
        }
        
        if([arrayBookingList count]>0)
        {
            NSMutableDictionary *dic = [[NSMutableDictionary alloc] initWithDictionary:[arrayBookingList objectAtIndex:indexPath.row]];
            
            orderNo.text = [NSString stringWithFormat:@"#%@", [dic objectForKey:@"booking_no"]];
            
            NSString *hrDateString=@"";
            NSString *TimeString=@"";
            
            if (![[dic objectForKey:@"date_start"] isEqual:[NSNull null]] && ![[dic objectForKey:@"date_start"] isEqualToString:@""]) {
                NSDateFormatter *df = [[NSDateFormatter alloc] init];
                [df setDateFormat:@"yyyy-MM-dd"];
                NSDate *d = [df dateFromString:[dic objectForKey:@"date_start"]];
                [df setDateFormat:@"EEE dd MMM, yyyy"];
                hrDateString = [df stringFromDate:d];
                
                NSString *dats1 = [dic objectForKey:@"time_start"];
                NSDateFormatter *dateFormatter3 = [[NSDateFormatter alloc] init];
                [dateFormatter3 setDateStyle:NSDateFormatterMediumStyle];
                [dateFormatter3 setDateFormat:@"HH:mm"];
                NSDate *date1 = [dateFormatter3 dateFromString:dats1];
                // NSLog(@"date1 : %@", date1);// Here returning (null)**
                
                NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
                [formatter setDateFormat:@"hh:mm a"];
                TimeString = [formatter stringFromDate:date1];
            }
            //NSLog(@"Current Date: %@", [formatter stringFromDate:date1]); // Here also returning (null)**
            date.text = [NSString stringWithFormat:@"%@ %@",hrDateString, TimeString];
            
            if (![[dic objectForKey:@"branch_name"] isEqual:[NSNull null]] && ![[dic objectForKey:@"branch_name"] isEqualToString:@""]) {
                if ([[dic objectForKey:@"branch_id"] intValue]> 0) {
                    branch .text = [NSString stringWithFormat:@"%@", [dic objectForKey:@"branch_name"]];
                } /*else if ([[dic objectForKey:@"product_id"] intValue]> 0) {
                    branch .text = [NSString stringWithFormat:@"%@", [dic objectForKey:@"product_name"]];
                } */else {
                    branch.text = @"";
                }
            } else {
                 branch.text = @"";
            }
        
            if ([[dic objectForKey:@"status"] isEqualToString:@"2"]) {
                status.text = [NSString stringWithFormat:@"%@", [dic objectForKey:@"status_desc"]];
                status.backgroundColor = [UIColor greenColor];
            } else if ([[dic objectForKey:@"status"] isEqualToString:@"3"]) {
                status.text = [NSString stringWithFormat:@"%@", [dic objectForKey:@"status_desc"]];
                status.backgroundColor = [UIColor blueColor];
            } else {
                status.text = [NSString stringWithFormat:@"%@", [dic objectForKey:@"status_desc"]];
                status.backgroundColor = [UIColor yellowColor];
            }
        }
        
        //return cell;
    }
    
    if (tableView == table_bookingMenu)
    {
            
            UILabel *title;
            const NSInteger LEFT_TAG = 3000;
            UILabel *value;
            const NSInteger RIGHT_TAG = 3001;
            
            if (cell == nil)
            {
                
                cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:@"bookingCell"];
                cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
                
                title = [[UILabel alloc] initWithFrame: CGRectMake(5, 12, 60, 20)];
                title.tag = LEFT_TAG;
                [title setFont:[UIFont systemFontOfSize:16]];
                title.textAlignment = NSTextAlignmentLeft;
                //title.backgroundColor = [UIColor clearColor];
                title.backgroundColor = [UIColor colorWithRed:238/255.0 green:238/255.0 blue:238/255.0 alpha:1];
                title.numberOfLines = 0;
                [cell.contentView addSubview:title];
                
                value = [[UILabel alloc] initWithFrame: CGRectMake(70, 12, table_bookingMenu.frame.size.width-70-25, 20)];
                value.tag = RIGHT_TAG;
                [value setFont:[UIFont systemFontOfSize:16]];
                title.textColor = [UIColor blackColor];
                value.textAlignment = NSTextAlignmentRight;
                value.backgroundColor = [UIColor clearColor];
                value.numberOfLines = 0;
                [cell.contentView addSubview:value];
            }
            else
            {
                title = (UILabel *)[cell viewWithTag:LEFT_TAG];
                value = (UILabel *)[cell viewWithTag:RIGHT_TAG];
            }
        
            NSMutableDictionary *dic = [[NSMutableDictionary alloc] initWithDictionary:[listOfItem objectAtIndex:indexPath.row]];
        
            title.text = [NSString stringWithFormat:@"%@", [dic objectForKey:@"titlename"]];
            if ([[dic objectForKey:@"value1"] length]>0 && [[dic objectForKey:@"value2"] length]>0) {
                value.text = [NSString stringWithFormat:@"%@ , %@", [dic objectForKey:@"value1"], [dic objectForKey:@"value2"]];
            } else {
                value.text = [NSString stringWithFormat:@"%@", [dic objectForKey:@"value1"]];
            }
        
        if ([[dic objectForKey:@"titlename"] isEqualToString:@"Cancel"]) {
            cell.accessoryType = UITableViewCellAccessoryNone;
            [title setTextAlignment:NSTextAlignmentCenter];
            [title setFrame:CGRectMake(0, 0, table_bookingMenu.frame.size.width, 44)];
        }
        
        //return cell;
    }
    
    if(tableView == branchTableView) {
        static NSString *CellIdentifier = @"CellIdentifier";
		cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
		cell.selectionStyle = UITableViewCellSelectionStyleNone;
		UILabel *cname;
		
		const NSInteger LEFT_TOP_TAG = 2000;
		
		if (cell == nil)
        {
			cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
			
			cname = [[UILabel alloc] initWithFrame: CGRectMake(5, 10, 310, 20)];
			cname.tag = LEFT_TOP_TAG;
			cname.textColor = [UIColor blackColor];
			cname.textAlignment = NSTextAlignmentLeft;
			cname.backgroundColor = [UIColor clearColor];
			cname.numberOfLines = 0;
			[cname setFont:[UIFont systemFontOfSize:14]];
			[cell.contentView addSubview:cname];
		}
        else
			cname = (UILabel *)[cell viewWithTag:LEFT_TOP_TAG];
		
        if ([listOfbranch count]>0)
        {
            NSDictionary *data = [listOfbranch objectAtIndex:indexPath.row];
            NSString *sname = [data objectForKey:@"branch_title"];
            
            cname.text = [NSString stringWithFormat:@"%@",sname];
        }
	}
    
    if(tableView == prodTableView) {
        static NSString *CellIdentifier = @"CellIdentifier";
		cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
		cell.selectionStyle = UITableViewCellSelectionStyleNone;
		UILabel *cname;
		
		const NSInteger LEFT_TOP_TAG = 2000;
		
		if (cell == nil)
        {
			cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
			
			cname = [[UILabel alloc] initWithFrame: CGRectMake(5, 10, 310, 20)];
			cname.tag = LEFT_TOP_TAG;
			cname.textColor = [UIColor blackColor];
			cname.textAlignment = NSTextAlignmentLeft;
			cname.backgroundColor = [UIColor clearColor];
			cname.numberOfLines = 0;
			[cname setFont:[UIFont systemFontOfSize:14]];
			[cell.contentView addSubview:cname];
		}
        else
        	cname = (UILabel *)[cell viewWithTag:LEFT_TOP_TAG];
		
        if ([listOfprod count]>0)
        {
            NSDictionary *data = [listOfprod objectAtIndex:indexPath.row];
            NSString *sname = [data objectForKey:@"product_title"];
            
            cname.text = [NSString stringWithFormat:@"%@",sname];
        }
	}
    
    if(tableView == dselectTableView) {
        static NSString *CellIdentifier = @"CellIdentifier";
		cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
		cell.selectionStyle = UITableViewCellSelectionStyleNone;
		UILabel *cname;
		
		const NSInteger LEFT_TOP_TAG = 2000;
		
		if (cell == nil)
        {
			cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
			
			cname = [[UILabel alloc] initWithFrame: CGRectMake(5, 10, 310, 20)];
			cname.tag = LEFT_TOP_TAG;
			cname.textColor = [UIColor blackColor];
			cname.textAlignment = NSTextAlignmentLeft;
			cname.backgroundColor = [UIColor clearColor];
			cname.numberOfLines = 0;
			[cname setFont:[UIFont systemFontOfSize:14]];
			[cell.contentView addSubview:cname];
		}
        else
			cname = (UILabel *)[cell viewWithTag:LEFT_TOP_TAG];
        
        if ([listAvaiDate count]>0)
        {
            NSDictionary *data = [listAvaiDate objectAtIndex:indexPath.row];
            NSString *sname = [data objectForKey:@"dateReserv"];
            
            cname.text = [NSString stringWithFormat:@"%@",sname];
        }
	}
    
    if(tableView == dendselectTableView) {
        static NSString *CellIdentifier = @"CellIdentifier";
		cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
		cell.selectionStyle = UITableViewCellSelectionStyleNone;
		UILabel *cname;
		
		const NSInteger LEFT_TOP_TAG = 2000;
		
		if (cell == nil)
        {
			cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
			
			cname = [[UILabel alloc] initWithFrame: CGRectMake(5, 10, 310, 20)];
			cname.tag = LEFT_TOP_TAG;
			cname.textColor = [UIColor blackColor];
			cname.textAlignment = NSTextAlignmentLeft;
			cname.backgroundColor = [UIColor clearColor];
			cname.numberOfLines = 0;
			[cname setFont:[UIFont systemFontOfSize:14]];
			[cell.contentView addSubview:cname];
		}
        else
			cname = (UILabel *)[cell viewWithTag:LEFT_TOP_TAG];
        
        if ([listAvaiDate count]>0)
        {
            NSDictionary *data = [listAvaiDate objectAtIndex:indexPath.row];
            NSString *sname = [data objectForKey:@"dateReserv"];
            
            cname.text = [NSString stringWithFormat:@"%@",sname];
        }
	}
    
    if(tableView == totalGuestTableView)
    {
        static NSString *CellIdentifier = @"CellIdentifier";
		cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
		cell.selectionStyle = UITableViewCellSelectionStyleNone;
		UILabel *cname;
		
		const NSInteger LEFT_TOP_TAG = 2000;
		
		if (cell == nil)
        {
			cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
			
			cname = [[UILabel alloc] initWithFrame: CGRectMake(5, 10, 310, 20)];
			cname.tag = LEFT_TOP_TAG;
			cname.textColor = [UIColor blackColor];
			cname.textAlignment = NSTextAlignmentLeft;
			cname.backgroundColor = [UIColor clearColor];
			cname.numberOfLines = 0;
			[cname setFont:[UIFont systemFontOfSize:14]];
			[cell.contentView addSubview:cname];
		}
        else
			cname = (UILabel *)[cell viewWithTag:LEFT_TOP_TAG];
		
        if (indexPath.row < [max_guesstotal intValue] )
            cname.text = [NSString stringWithFormat:@"%i",indexPath.row + 1];
        
	}
    
    if(tableView == qtyTableView)
    {
        static NSString *CellIdentifier = @"CellIdentifier";
        cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        UILabel *cname;
        
        const NSInteger LEFT_TOP_TAG = 2000;
        
        if (cell == nil)
        {
            cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
            
            cname = [[UILabel alloc] initWithFrame: CGRectMake(5, 10, 310, 20)];
            cname.tag = LEFT_TOP_TAG;
            cname.textColor = [UIColor blackColor];
            cname.textAlignment = NSTextAlignmentLeft;
            cname.backgroundColor = [UIColor clearColor];
            cname.numberOfLines = 0;
            [cname setFont:[UIFont systemFontOfSize:14]];
            [cell.contentView addSubview:cname];
        }
        else
            cname = (UILabel *)[cell viewWithTag:LEFT_TOP_TAG];
        
        //if (indexPath.row < [max_guesstotal intValue] )
        cname.text = [NSString stringWithFormat:@"%i",indexPath.row + 1];
        
    }
    
    if(tableView == tselectTableView) {
        static NSString *CellIdentifier = @"CellIdentifier";
		cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
		cell.selectionStyle = UITableViewCellSelectionStyleNone;
		UILabel *cname;
		
		const NSInteger LEFT_TOP_TAG = 2000;
        
		if (cell == nil)
        {
            
			cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
			
			cname = [[UILabel alloc] initWithFrame: CGRectMake(5, 10, 310, 20)];
			cname.tag = LEFT_TOP_TAG;
			cname.textColor = [UIColor blackColor];
			cname.textAlignment = NSTextAlignmentLeft;
			cname.backgroundColor = [UIColor clearColor];
			cname.numberOfLines = 0;
			[cname setFont:[UIFont systemFontOfSize:14]];
			[cell.contentView addSubview:cname];
		}
        else
			cname = (UILabel *)[cell viewWithTag:LEFT_TOP_TAG];
		
        if ([listAvaiDateTime count]>0)
        {
            
            NSDictionary *data = [listAvaiDateTime objectAtIndex:indexPath.row];
            NSString *sname = [data objectForKey:@"dateReserv"];
            
            cname.text = [NSString stringWithFormat:@"%@",sname];
        }
	}
    
    if(tableView == tendselectTableView) {
        static NSString *CellIdentifier = @"CellIdentifier";
		cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
		cell.selectionStyle = UITableViewCellSelectionStyleNone;
		UILabel *cname;
		
		const NSInteger LEFT_TOP_TAG = 2000;
		
		if (cell == nil)
        {
			cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
			
			cname = [[UILabel alloc] initWithFrame: CGRectMake(5, 10, 310, 20)];
			cname.tag = LEFT_TOP_TAG;
			cname.textColor = [UIColor blackColor];
			cname.textAlignment = NSTextAlignmentLeft;
			cname.backgroundColor = [UIColor clearColor];
			cname.numberOfLines = 0;
			[cname setFont:[UIFont systemFontOfSize:14]];
			[cell.contentView addSubview:cname];
		}
        else
			cname = (UILabel *)[cell viewWithTag:LEFT_TOP_TAG];
        
        if ([listAvaiDateTime count]>0) {
            
            NSDictionary *data = [listAvaiDateTime objectAtIndex:indexPath.row];
            NSString *sname = [data objectForKey:@"dateReserv"];
            
            cname.text = [NSString stringWithFormat:@"%@",sname];
        }
        //return cell;
	}
    
    if (tableView == multiprodTableView) {
        
        static NSString *CellIdentifier = @"Cell";
        //cell = nil;
        cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
        //UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];
        //cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
        UILabel *prodname, *dprice, *dpricetitle;
        UIButton *imgchecked;
        UITextField *dqty;
        
        const NSInteger LEFT_TOP_TAG = 5000;
        const NSInteger LEFT_BOTTOM_TAG = 5001;
        const NSInteger RIGHT_TAG = 5002;
        const NSInteger LEFT_TAG = 5003;
        const NSInteger LEFT_BOTTOM_LEFT_TAG = 5004;
        
        //cell = nil;
        if (cell == nil) {
            cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
            if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPhone) {
                cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
            }
            
            cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
            
            imgchecked = [UIButton buttonWithType:UIButtonTypeCustom];
            CGRect frame = CGRectMake(5, 5, 20, 20);
            imgchecked.frame = frame;
            imgchecked.tag = LEFT_TAG;
            [cell.contentView addSubview:imgchecked];
            //[imgchecked setBackgroundImage:image forState:UIControlStateNormal];
            //[imgchecked addTarget:self action:@selector(checkButtonTapped:event:)  forControlEvents:UIControlEventTouchUpInside];
            //imgchecked.backgroundColor = [UIColor clearColor];
            //cell.accessoryView = button;
            
            prodname = [[UILabel alloc] initWithFrame: CGRectMake(35, 5, self.view.frame.size.width-40, 20)];
            prodname.tag = LEFT_TOP_TAG;
            prodname.textColor = [UIColor blackColor];
            prodname.textAlignment = NSTextAlignmentLeft;
            prodname.backgroundColor = [UIColor clearColor];
            prodname.numberOfLines = 0;
            [prodname setFont:[UIFont systemFontOfSize:14]];
            prodname.adjustsFontSizeToFitWidth = YES;
            [cell.contentView addSubview:prodname];
            
            if ([display_price isEqualToString:@"1"]) {
                
                dpricetitle = [[UILabel alloc] initWithFrame: CGRectMake(10, 25, 50, 20)];
                dpricetitle.tag = LEFT_BOTTOM_LEFT_TAG;
                dpricetitle.textColor = [UIColor blackColor];
                dpricetitle.textAlignment = NSTextAlignmentRight;
                dpricetitle.backgroundColor = [UIColor clearColor];
                dpricetitle.numberOfLines = 0;
                [dpricetitle setFont:[UIFont systemFontOfSize:14]];
                [cell.contentView addSubview:dpricetitle];
                
                dprice = [[UILabel alloc] initWithFrame: CGRectMake(65, 25, 200, 20)];
                dprice.tag = LEFT_BOTTOM_TAG;
                dprice.textColor = [UIColor blackColor];
                dprice.textAlignment = NSTextAlignmentLeft;
                dprice.backgroundColor = [UIColor clearColor];
                dprice.numberOfLines = 0;
                [dprice setFont:[UIFont systemFontOfSize:14]];
                [cell.contentView addSubview:dprice];
            }
            
            if ([display_qtt isEqualToString:@"1"]) {
                
                dqty = [[UITextField alloc] initWithFrame: CGRectMake(270, 22, 30, 20)];
                //NSString *tagValue = [NSString stringWithFormat:@"%ld%ld", (long)indexPath.section, (long)indexPath.row];
                dqty.tag = RIGHT_TAG;
                //dqty.tag = [[dictionary objectForKey:@"product_id"] intValue];
                dqty.textColor = [UIColor blackColor];
                dqty.borderStyle = UITextBorderStyleRoundedRect;
                dqty.backgroundColor = [UIColor clearColor];
                [dqty setFont:[UIFont systemFontOfSize:14]];
                //dqty.clearsOnBeginEditing = YES;
                dqty.delegate = self;
                //[dqty addTarget:self action:@selector(textChanged:event:)  forControlEvents:UIControlEventTouchUpInside];
                //[dqty addTarget:self action:@selector(textChanged:event:) forControlEvents:UIControlEventEditingChanged];
                [cell.contentView addSubview:dqty];
            }

            
        } else {
            prodname = (UILabel *)[cell viewWithTag:LEFT_TOP_TAG];
            dprice = (UILabel *)[cell viewWithTag:LEFT_BOTTOM_TAG];
            imgchecked = (UIButton *)[cell viewWithTag:LEFT_TAG];
            //NSString *tagValue = [NSString stringWithFormat:@"%ld%ld", (long)indexPath.section, (long)indexPath.row];
            //dqty.tag = [tagValue intValue];
            dqty = (UITextField *)[cell viewWithTag:RIGHT_TAG];
            dpricetitle = (UILabel *)[cell viewWithTag:LEFT_BOTTOM_LEFT_TAG];
        }
        
        BOOL manyCells  = [[arrayForBool objectAtIndex:indexPath.section] boolValue];
        if (!manyCells) {
            //cell.textLabel.text = @"click to enlarge";
        }
        else{
            
            NSArray *content = [sectionContentDict valueForKey:[sectionTitleArray objectAtIndex:indexPath.section]];
            //cell.textLabel.text = [content objectAtIndex:indexPath.row];
            //if([arSelectedRows containsObject:indexPath]) {
                //cell.accessoryType = UITableViewCellAccessoryCheckmark;
                //prodname.text = [NSString stringWithFormat:@"%@ %@", @"\u2611", [content objectAtIndex:indexPath.row]];
            //} else {
                //cell.accessoryType = UITableViewCellAccessoryNone;
                //prodname.text = [NSString stringWithFormat:@"%@ %@", @"\u2610", [content objectAtIndex:indexPath.row]];
            //}
            
            if ([arSelectedRows containsObject:indexPath]) {
                [imgchecked setBackgroundImage:[UIImage imageNamed:@"checkedbox.png"] forState:UIControlStateNormal];
                dqty.hidden = NO;
            }
            else {
                [imgchecked setBackgroundImage:[UIImage imageNamed:@"uncheckedbox.png"] forState:UIControlStateNormal];
                dqty.hidden = YES;
            }
            
            [imgchecked addTarget:self action:@selector(handleChecking:event:)  forControlEvents:UIControlEventTouchUpInside];
            
            //NSLog(@"textfieldSelectedRows11::%@", textfieldSelectedRows);
            NSPredicate *predicate = [NSPredicate predicateWithFormat:@"(indexPath == %@)", indexPath];
            //NSLog(@"filtertextfieldSelectedRows: %@", [textfieldSelectedRows filteredArrayUsingPredicate:predicate]);
            if ([[textfieldSelectedRows filteredArrayUsingPredicate:predicate] count]>0) {
                NSDictionary *dictionary = [[textfieldSelectedRows filteredArrayUsingPredicate:predicate] objectAtIndex:0];
                dqty.text = [dictionary objectForKey:@"textFieldString"];
                //[cname removeFromSuperview];
            } else {
                dqty.text = @"1";
            }
            
            /*long idxrow1;
            long totalrows=0;
            if (indexPath.section>0) {
                if (currentIndexSection != indexPath.section) {
                    for (int ids=0; ids<indexPath.section; ids++) {
                        totalrows = totalrows + [self tableView:multiprodTableView numberOfRowsInSection:ids];
                    }
                }
                currentIndexSection = indexPath.section;
            } else {
                totalrows = 0;
                currentIndexSection = indexPath.section;
            }
            NSLog(@"totalrows:%lu", totalrows);
            idxrow1 = totalrows + (long)indexPath.row;
            
            //NSInteger sections = multiprodTableView.numberOfSections;
            NSInteger cellCount = 0;
            for (NSInteger i = 0; i < indexPath.section; i++) {
                cellCount += [multiprodTableView numberOfRowsInSection:0];
            }
            
            NSLog(@"rowNumber:%lu", cellCount);
            NSDictionary *dictionary = [listOfprod objectAtIndex:idxrow1]; */
            //NSLog(@"listOfprod:%@", listOfprod);
            
            prodname.text = [content objectAtIndex:indexPath.row];
            
            NSPredicate *predicate1 = [NSPredicate predicateWithFormat:@"(product_title == %@)", [content objectAtIndex:indexPath.row]];
            //NSLog(@"filtertextfieldSelectedRows: %@", [textfieldSelectedRows filteredArrayUsingPredicate:predicate]);
            if ([[listOfprod filteredArrayUsingPredicate:predicate1] count]>0) {
                NSDictionary *dictionary = [[listOfprod filteredArrayUsingPredicate:predicate1] objectAtIndex:0];
                dprice.text = [dictionary objectForKey:@"price"];
                dpricetitle.text = [dictionary objectForKey:@"price_title"];
                //[cname removeFromSuperview];
            } else {
                dprice.text = @"";
                dpricetitle.text = @"";
            }
            
        }
        
    }
    
    return cell;
}

- (void) handleChecking:(id)sender event:(id)event {
    NSSet *touches = [event allTouches];
    UITouch *touch = [touches anyObject];
    CGPoint currentTouchPosition = [touch locationInView:multiprodTableView];
    NSIndexPath *indexPath = [multiprodTableView indexPathForRowAtPoint: currentTouchPosition];
    
    sqlite3 *database;
    NSString *sqlcontents;
    int countrows = 0;
    
    if(sqlite3_open([appdelegate.databasePath UTF8String], &database) == SQLITE_OK) {
    
        for (int st=0; st<indexPath.section; st++) {
            sqlcontents = [[NSString alloc] initWithFormat:@"SELECT COUNT(*) FROM contents WHERE category_id =%@ ORDER BY orderno ", [strings objectAtIndex:st]];
            
            const char *sqlStatement4 = [sqlcontents UTF8String];
            sqlite3_stmt *compiledStatement4;
            if(sqlite3_prepare_v2(database, sqlStatement4, -1, &compiledStatement4, NULL) == SQLITE_OK) {
                
                while(sqlite3_step(compiledStatement4) == SQLITE_ROW) {
                    
                    countrows = countrows + (int)sqlite3_column_int(compiledStatement4, 0);
                    
                }
                
            }
            sqlite3_finalize(compiledStatement4);
            
        }
    }
    sqlite3_close(database);
    
    NSDictionary *dictionary = [listOfprod objectAtIndex:countrows+indexPath.row];
    
    NSDictionary *txtfieldval;
    txtfieldval = [NSDictionary dictionaryWithObjectsAndKeys:indexPath,@"indexPath",@"1",@"textFieldString", nil];

    if (indexPath != nil)
    {
        if ([arSelectedRows containsObject:indexPath]) {
            [arSelectedRows removeObject:indexPath];
            [textfieldSelectedRows removeObject:txtfieldval];
            [prodSelectedRows removeObject:[dictionary objectForKey:@"product_id"]];
        }
        else {
            [arSelectedRows addObject:indexPath];
            [textfieldSelectedRows addObject:txtfieldval];
            [prodSelectedRows addObject:[dictionary objectForKey:@"product_id"]];
        }
        [multiprodTableView reloadRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation: UITableViewRowAnimationFade];
    }

}

- (void) textChanged:(id)sender event:(id)event {
    
    UITextField *clicked = (UITextField *) sender;
    //NSString *textFieldString = [(UITextField *)[self.view viewWithTag:clicked.tag] text];
    //NSLog(@"clicked::%@", clicked);
    NSString *textFieldString = clicked.text;
    
    if ([textFieldString length]>0) {
    UITableViewCell *cell = (UITableViewCell*) clicked.superview.superview;
    NSIndexPath *indexPath = [multiprodTableView indexPathForCell:cell];
   
    if (indexPath != nil)
    {
       
        NSDictionary *txtfieldval;
        txtfieldval = [NSDictionary dictionaryWithObjectsAndKeys:indexPath,@"indexPath",textFieldString,@"textFieldString", nil];
        
        NSUInteger index = [arSelectedRows indexOfObject:indexPath];
        [textfieldSelectedRows replaceObjectAtIndex:index withObject:txtfieldval];
        
        [multiprodTableView reloadRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation: UITableViewRowAnimationFade];
    }
    }
    
}

-(void) displayContentDetails :(NSInteger)idx {
    
    int heightY;
    int heightYiPad;
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone) {
        // For iPhone
        heightY = 137;
    } else {
        heightYiPad = 208;
    }
    
    NSMutableDictionary *dic = [[NSMutableDictionary alloc] initWithDictionary:[arrayBookingList objectAtIndex:idx]];
    //NSLog(@"dic :: %@", dic);
    //[[navBar topItem]setTitle:[dic objectForKey:@"branch_name"]];
    [[navBar topItem]setTitle:val_booking_title];
    if (![[dic objectForKey:@"customer_name"] isEqual:[NSNull null]])
        [label_customerName setText:[dic objectForKey:@"customer_name"]];
    else
        [label_customerName setText:@""];
    //[label_branchCountry setText:[dic objectForKey:@"branch_country"]];
    if ([[dic objectForKey:@"branch_id"] intValue] > 0 ) {
        [label_branchName setText:[dic objectForKey:@"branch_name"]];
        branchTel = [dic objectForKey:@"branch_id"];
    } /*else if ([[dic objectForKey:@"product_id"] intValue] > 0 ){
       [label_branchName setText:[dic objectForKey:@"product_name"]];
       branchTel = [dic objectForKey:@"product_id"];
       }*/ else {
           [label_branchName setText:@""];
           branchTel = @"";
       }
    /*NSArray *stringArray;
     if ([[dic objectForKey:@"product_id"] length]>0) {
     if ([[dic objectForKey:@"product_id"] rangeOfString:@","].location == NSNotFound) {
     stringArray = [dic objectForKey:@"product_id"];
     } else {
     stringArray = [[dic objectForKey:@"product_id"] componentsSeparatedByString:@","];
     }
     }
     NSArray *stringArray1;
     if ([[dic objectForKey:@"qtt_list"] length]>0) {
     if ([[dic objectForKey:@"qtt_list"] rangeOfString:@","].location == NSNotFound) {
     stringArray1 = [dic objectForKey:@"qtt_list"];
     } else {
     stringArray1 = [[dic objectForKey:@"qtt_list"] componentsSeparatedByString:@","];
     }
     }
     NSArray *stringArray2;
     if ([[dic objectForKey:@"price_list"] length]>0) {
     if ([[dic objectForKey:@"price_list"] rangeOfString:@","].location == NSNotFound) {
     stringArray2 = [dic objectForKey:@"price_list"];
     } else {
     stringArray2 = [[dic objectForKey:@"price_list"] componentsSeparatedByString:@","];
     }
     } */
    
    NSArray *stringArray;
    if (![[dic objectForKey:@"product_id"] isEqual:[NSNull null]]) {
        if ([[dic objectForKey:@"product_id"] rangeOfString:@","].location == NSNotFound) {
            //[stringArray arrayByAddingObject:];
            stringArray = [NSArray arrayWithObjects:[dic objectForKey:@"product_id"], nil];
        } else {
            stringArray = [[[dic objectForKey:@"product_id"] componentsSeparatedByString:@","] mutableCopy];
        }
    }
    
    NSArray *stringArray1;
    if (![[dic objectForKey:@"qtt_list"] isEqual:[NSNull null]]) {
        if ([[dic objectForKey:@"qtt_list"] rangeOfString:@","].location == NSNotFound) {
            stringArray1 = [NSArray arrayWithObjects:[dic objectForKey:@"qtt_list"], nil];
        } else {
            stringArray1 = [[[dic objectForKey:@"qtt_list"] componentsSeparatedByString:@","] mutableCopy];
        }
    }
    
    NSArray *stringArray2;
    if (![[dic objectForKey:@"price_list"] isEqual:[NSNull null]]) {
        if ([[dic objectForKey:@"price_list"] rangeOfString:@","].location == NSNotFound) {
            stringArray2 = [NSArray arrayWithObjects:[dic objectForKey:@"price_list"], nil];
        } else {
            stringArray2 = [[[dic objectForKey:@"price_list"] componentsSeparatedByString:@","] mutableCopy];
        }
    }
    
    //NSLog(@"stringArray :: %@", stringArray);
    NSPredicate *predicate;
    UILabel *pricetotal, *pricename;
    
    for (int a=0; a<[stringArray count]; a++) {
        predicate = [NSPredicate predicateWithFormat:@"(product_id == %@)", [stringArray objectAtIndex:a]];
        
        UILabel *cname;
        cname = [[UILabel alloc] initWithFrame: CGRectMake(2, heightY, self.view.frame.size.width/7*3-5, 20)];
        cname.textColor = [UIColor blackColor];
        cname.tag = 1;
        cname.textAlignment = NSTextAlignmentLeft;
        cname.backgroundColor = [UIColor clearColor];
        cname.numberOfLines = 0;
        cname.lineBreakMode = NSLineBreakByWordWrapping;
        if ([[listOfprod filteredArrayUsingPredicate:predicate] count]>0) {
            NSDictionary *dictionary = [[listOfprod filteredArrayUsingPredicate:predicate] objectAtIndex:0];
            cname.text = [dictionary objectForKey:@"product_title"];
            [scrollView addSubview:cname];
            //[cname removeFromSuperview];
        }
        
        if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone) {
            // For iPhone
            if (a==0) {
                heightY = heightY+20;
            }
            
            UILabel *qtylist;
            qtylist = [[UILabel alloc] initWithFrame: CGRectMake(self.view.frame.size.width/7*3, heightY, self.view.frame.size.width/7-5, 20)];
            qtylist.textColor = [UIColor blackColor];
            qtylist.tag = 2;
            qtylist.textAlignment = NSTextAlignmentCenter;
            qtylist.backgroundColor = [UIColor clearColor];
            qtylist.text = [stringArray1 objectAtIndex:a];
            [scrollView addSubview:qtylist];
            
            UILabel *pricelist;
            pricelist = [[UILabel alloc] initWithFrame: CGRectMake(self.view.frame.size.width/7*4, heightY, self.view.frame.size.width/7*3-5, 20)];
            pricelist.textColor = [UIColor blackColor];
            pricelist.tag = 3;
            pricelist.textAlignment = NSTextAlignmentRight;
            pricelist.backgroundColor = [UIColor clearColor];
            pricelist.text = [stringArray2 objectAtIndex:a];
            [scrollView addSubview:pricelist];
            
            CGSize expectedLabelSize = [cname sizeThatFits:CGSizeMake(cname.frame.size.width, FLT_MAX)];
            if ([val_multiprod isEqualToString:@"1"] ) {
                [cname setFrame:CGRectMake(2, heightY, self.view.frame.size.width/7*3-5, expectedLabelSize.height)];
                cname.textAlignment = NSTextAlignmentLeft;
            } else {
                [cname setFrame:CGRectMake(2, heightY, self.view.frame.size.width-5, expectedLabelSize.height)];
                cname.textAlignment = NSTextAlignmentCenter;
            }
            heightY = heightY+expectedLabelSize.height+5;
        } else {
            if (a==0) {
                heightYiPad = heightYiPad+20;
            }
            
            UILabel *qtylist;
            qtylist = [[UILabel alloc] initWithFrame: CGRectMake(self.view.frame.size.width/7*3, heightYiPad, self.view.frame.size.width/7-5, 20)];
            qtylist.textColor = [UIColor blackColor];
            qtylist.tag = 2;
            qtylist.textAlignment = NSTextAlignmentCenter;
            qtylist.backgroundColor = [UIColor clearColor];
            qtylist.text = [stringArray1 objectAtIndex:a];
            [scrollView addSubview:qtylist];
            
            UILabel *pricelist;
            pricelist = [[UILabel alloc] initWithFrame: CGRectMake(self.view.frame.size.width/7*4, heightYiPad, self.view.frame.size.width/7*3-5, 20)];
            pricelist.textColor = [UIColor blackColor];
            pricelist.tag = 3;
            pricelist.textAlignment = NSTextAlignmentRight;
            pricelist.backgroundColor = [UIColor clearColor];
            pricelist.text = [stringArray2 objectAtIndex:a];
            [scrollView addSubview:pricelist];
            
            CGSize expectedLabelSize = [cname sizeThatFits:CGSizeMake(cname.frame.size.width, FLT_MAX)];
            if ([val_multiprod isEqualToString:@"1"] ) {
                [cname setFrame:CGRectMake(2, heightYiPad, self.view.frame.size.width/7*3-5, expectedLabelSize.height)];
                cname.textAlignment = NSTextAlignmentLeft;
            } else {
                [cname setFrame:CGRectMake(2, heightYiPad, self.view.frame.size.width-5, expectedLabelSize.height)];
                cname.textAlignment = NSTextAlignmentCenter;
            }
            heightYiPad = heightYiPad+expectedLabelSize.height+5;
        }
        
        //NSLog(@"filteredArray: %@", [listOfprod filteredArrayUsingPredicate:predicate]);
        
    }
    
    if ([stringArray count]>0 && [[dic objectForKey:@"price_total"] floatValue]>0) {
        if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone) {
            // For iPhone
            pricename = [[UILabel alloc] initWithFrame: CGRectMake(2, heightY, self.view.frame.size.width/7*4-5, 20)];
        } else {
            pricename = [[UILabel alloc] initWithFrame: CGRectMake(2, heightYiPad, self.view.frame.size.width/7*4-5, 20)];
        }
        pricename.tag = 5;
        pricename.textAlignment = NSTextAlignmentLeft;
        pricename.backgroundColor = [UIColor clearColor];
        pricename.text = NSLocalizedString(@"total", nil);
        [scrollView addSubview:pricename];
        
        if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone) {
            // For iPhone
            pricetotal = [[UILabel alloc] initWithFrame: CGRectMake(self.view.frame.size.width/7*4, heightY, self.view.frame.size.width/7*3-5, 25)];
            heightY = heightY+25;
        } else {
            pricetotal = [[UILabel alloc] initWithFrame: CGRectMake(self.view.frame.size.width/7*4, heightYiPad, self.view.frame.size.width/7*3-5, 25)];
            heightYiPad = heightYiPad+25;
        }
        pricetotal.textColor = [UIColor blackColor];
        pricetotal.tag = 4;
        pricetotal.textAlignment = NSTextAlignmentRight;
        pricetotal.backgroundColor = [UIColor clearColor];
        pricetotal.text = [dic objectForKey:@"price_total"];
        CALayer *topBorder = [CALayer layer];
        topBorder.borderColor = [UIColor darkGrayColor].CGColor;
        topBorder.borderWidth = 1;
        topBorder.frame = CGRectMake(0, 1, CGRectGetWidth(pricetotal.frame), 1);
        [pricetotal.layer addSublayer:topBorder];
        [scrollView addSubview:pricetotal];
        
    }
    
    statusOrder = [dic objectForKey:@"status"];
    //NSLog(@"statusOrder :: %@", statusOrder);
    [self getItem];
    [label_reservationNo setText:[dic objectForKey:@"booking_no"]];
    [label_reserveStatus setText:[NSString stringWithFormat:@"Status: %@",[[dic objectForKey:@"status_desc"] uppercaseString]]];
    if([[dic objectForKey:@"status"] isEqualToString:@"1"]){
        [label_reserveTitle setText:NSLocalizedString(@"your_prersv_no_is", nil)];
        //[label_reserveTq setText:NSLocalizedString(@"rsv_tq_msg", nil)];
        //NSLog(@"val_tq_message :: %@", val_tq_message);
        [label_reserveTq setText:val_tq_message];
        /*CGRect textRect = [[label_reserveTq text] boundingRectWithSize:label_reserveTq.frame.size
                                                               options:NSStringDrawingUsesLineFragmentOrigin|NSStringDrawingUsesFontLeading
                                                            attributes:@{NSFontAttributeName:[label_reserveTq font]}
                                                               context:nil]; */
        //CGSize expectedLabelSize = textRect.size;
        CGSize expectedLabelSize = [label_reserveTq sizeThatFits:CGSizeMake(label_reserveTq.frame.size.width, FLT_MAX)];
        //CGFloat textHeight = textSize.height;
        if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone) {
            // For iPhone
            [label_hline setFrame:CGRectMake(2, heightY, self.view.frame.size.width, 21)];
            heightY = heightY + 21;
            [label_reserveTq setFrame:CGRectMake(2, heightY, self.view.frame.size.width, expectedLabelSize.height)];
            heightY = heightY + expectedLabelSize.height;
            table_bookingMenu.frame = CGRectMake(2, heightY, table_bookingMenu.frame.size.width, 200);
            heightY = heightY + 200;
            
        } else {
            label_hline.translatesAutoresizingMaskIntoConstraints = YES;
            [label_hline setFrame:CGRectMake(2, heightYiPad, self.view.frame.size.width, 21)];
            heightYiPad = heightYiPad + 21;
            label_reserveTq.translatesAutoresizingMaskIntoConstraints = YES;
            [label_reserveTq setFrame:CGRectMake(2, heightYiPad, self.view.frame.size.width, expectedLabelSize.height)];
            heightYiPad = heightYiPad + expectedLabelSize.height;
            table_bookingMenu.translatesAutoresizingMaskIntoConstraints = YES;
            table_bookingMenu.frame = CGRectMake(2, heightYiPad, table_bookingMenu.frame.size.width, 400);
            heightYiPad = heightYiPad + 400;
        }
    } else {
        [label_reserveTitle setText:NSLocalizedString(@"your_rsv_no_is", nil)];
        [label_reserveTq setText:branchdetails];
        //[label_reserveTq setText:val_tq_message];
        
        CGSize expectedLabelSize = [label_reserveTq sizeThatFits:CGSizeMake(label_reserveTq.frame.size.width, FLT_MAX)];
        if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone) {
            // For iPhone
            [label_hline setFrame:CGRectMake(2, heightY, self.view.frame.size.width, 21)];
            heightY = heightY + 21;
            label_reserveTq.frame = CGRectMake(2, heightY, label_reserveTq.frame.size.width, expectedLabelSize.height);
            heightY = heightY + expectedLabelSize.height;
            table_bookingMenu.frame = CGRectMake(2, heightY, table_bookingMenu.frame.size.width, 200);
            heightY = heightY + 200;
            
        } else {
            label_hline.translatesAutoresizingMaskIntoConstraints = YES;
            [label_hline setFrame:CGRectMake(2, heightYiPad, self.view.frame.size.width, 21)];
            heightYiPad = heightYiPad + 21;
            label_reserveTq.translatesAutoresizingMaskIntoConstraints = YES;
            label_reserveTq.frame = CGRectMake(2, heightYiPad, label_reserveTq.frame.size.width, expectedLabelSize.height);
            heightYiPad = heightYiPad + expectedLabelSize.height;
            table_bookingMenu.translatesAutoresizingMaskIntoConstraints = YES;
            table_bookingMenu.frame = CGRectMake(2, heightYiPad, table_bookingMenu.frame.size.width, 400);
            heightYiPad = heightYiPad + 10 + 400;
        }
        
    }
    
    reservationId = [dic objectForKey:@"id"];
    
    NSString *hrDateString=@"";
    NSString *TimeString=@"";
    
    if (![[dic objectForKey:@"date_start"] isEqual:[NSNull null]] && ![[dic objectForKey:@"date_start"] isEqualToString:@""]) {
        NSDateFormatter *df = [[NSDateFormatter alloc] init];
        [df setDateFormat:@"yyyy-MM-dd"];
        NSDate *d = [df dateFromString:[dic objectForKey:@"date_start"]];
        [df setDateFormat:@"EEE dd MMM, yyyy"];
        hrDateString = [df stringFromDate:d];
        
        NSString *dats1 = [dic objectForKey:@"time_start"];
        NSDateFormatter *dateFormatter3 = [[NSDateFormatter alloc] init];
        [dateFormatter3 setDateStyle:NSDateFormatterMediumStyle];
        [dateFormatter3 setDateFormat:@"HH:mm"];
        NSDate *date1 = [dateFormatter3 dateFromString:dats1];
        // NSLog(@"date1 : %@", date1);// Here returning (null)**
        
        NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
        [formatter setDateFormat:@"hh:mm a"];
        TimeString = [formatter stringFromDate:date1];
    }
    //NSLog(@"Current Date: %@", [formatter stringFromDate:date1]); // Here also returning (null)**
    
    [label_reserveDate setText:[NSString stringWithFormat:@"%@ %@",hrDateString, TimeString]];
    
    [view_bookingDetail setFrame:CGRectMake(0, 48, self.view.frame.size.width, self.view.frame.size.height-48)];
    [self.view addSubview:view_bookingDetail];
    [view_bookingList removeFromSuperview];
    [view_bookingAdd removeFromSuperview];
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    //NSLog(@"CellSelected");
    if (tableView == table_bookingList)
    {
        for (UIView *subview in [scrollView subviews]) {
            // Only remove the subviews with tag not equal to 1
            if (subview.tag == 1 || subview.tag == 2 || subview.tag == 3 || subview.tag == 4 ||subview.tag == 5) {
                [subview removeFromSuperview];
            }
        }
        
        [self displayContentDetails:indexPath.row];
        
    }
    
    if (tableView == table_bookingMenu)
    {
        NSMutableDictionary *dic = [[NSMutableDictionary alloc] initWithDictionary:[listOfItem objectAtIndex:indexPath.row]];
        
        if ([[dic objectForKey:@"title"] isEqualToString:@"Call"])
        {
            NSString *phoneNumber = [@"telprompt://" stringByAppendingString:[dic objectForKey:@"value1"]];
            [[UIApplication sharedApplication] openURL:[NSURL URLWithString:phoneNumber]];
        }
        else if ([[dic objectForKey:@"title"] isEqualToString:@"Email"])
        {
            NSArray *toRecipents = [NSArray arrayWithObject:[dic objectForKey:@"value1"]];
            
            MFMailComposeViewController *mc = [[MFMailComposeViewController alloc] init];
            mc.mailComposeDelegate = self;
            //[mc setSubject:emailTitle];
            //[mc setMessageBody:messageBody isHTML:YES];
            [mc setToRecipients:toRecipents];
            
            // Present mail view controller on screen
            //[viewCtrl presentViewController:mc animated:YES completion:NULL];
            if([MFMailComposeViewController canSendMail])
            {
                [self presentViewController:mc animated:NO completion:NULL];
                //[viewCtrl.navigationController pushViewController:mc animated:YES];
            }
        }
        else if ([[dic objectForKey:@"title"] isEqualToString:@"Map"])
        {
            NSString *nibFileName;
            if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone) {
                nibFileName = @"MapViewController";
            } else {
                nibFileName = @"MapViewController_ipad";
            }
            MapViewController *f = [[MapViewController alloc] initWithNibName:nibFileName bundle:[NSBundle mainBundle]];
            f.latitude = [[dic objectForKey:@"value1"] floatValue];
            f.longitude = [[dic objectForKey:@"value2"] floatValue];
            f.cont_title = cont_title;
            [appdelegate.navController pushViewController:f animated:NO];
        }
        else if ([[dic objectForKey:@"title"] isEqualToString:@"Cancel"])
        {
            if ([statusOrder isEqualToString:@"1"]){
                currentRequest = @"cancelBooking";
                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:appdelegate.iconname
                                                                message:@"Are you sure you want to cancel the reservation?"
                                                               delegate:self
                                                      cancelButtonTitle:@"Cancel"
                                                      otherButtonTitles:@"Yes", nil];
                
                [alert show];
            }
            //[alert release];
        }
        [table_bookingMenu deselectRowAtIndexPath:indexPath animated:NO];
    }
    
    if(tableView == branchTableView){
		NSDictionary *data = [listOfbranch objectAtIndex:indexPath.row];
		NSString *cname = [data objectForKey:@"branch_title"];
		NSString *ccode = [data objectForKey:@"branch_id"];
		tf1.text = cname;
		branchid = ccode;
        branchTel = ccode;
        [branchPopoverController dismissPopoverAnimated:NO];
        self.branchPopoverController = nil;
	}
    
    if(tableView == prodTableView){
		NSDictionary *data = [listOfprod objectAtIndex:indexPath.row];
		NSString *cname = [data objectForKey:@"product_title"];
		NSString *ccode = [data objectForKey:@"product_id"];
		tf.text = cname;
		prodid = ccode;
        branchTel = ccode;
        [prodPopoverController dismissPopoverAnimated:NO];
        self.prodPopoverController = nil;
	}
    
    if(tableView == dselectTableView){
		NSDictionary *data = [listAvaiDate objectAtIndex:indexPath.row];
		NSString *sname = [data objectForKey:@"dateReserv"];
		tf2.text = [NSString stringWithFormat:@"%@",sname];
        [self separateDateTime:dselectTableView];
        [dselectPopoverController dismissPopoverAnimated:NO];
        self.dselectPopoverController = nil;
	}
    
    if(tableView == dendselectTableView){
		NSDictionary *data = [listAvaiDate objectAtIndex:indexPath.row];
		NSString *sname = [data objectForKey:@"dateReserv"];
		tf4.text = [NSString stringWithFormat:@"%@",sname];
        [self separateDateTime:dendselectTableView];
        [dselectPopoverController dismissPopoverAnimated:NO];
        self.dselectPopoverController = nil;
	}
    
    if(tableView == tselectTableView){
		NSDictionary *data = [listAvaiDateTime objectAtIndex:indexPath.row];
		NSString *sname = [data objectForKey:@"dateReserv"];
		tf3.text = [NSString stringWithFormat:@"%@",sname];
        [dselectPopoverController dismissPopoverAnimated:NO];
        self.dselectPopoverController = nil;
	}
    
    if(tableView == tendselectTableView){
		NSDictionary *data = [listAvaiDateTime objectAtIndex:indexPath.row];
		NSString *sname = [data objectForKey:@"dateReserv"];
		tf6.text = [NSString stringWithFormat:@"%@",sname];
        [dselectPopoverController dismissPopoverAnimated:NO];
        self.dselectPopoverController = nil;
	}
    
    if(tableView == totalGuestTableView){
		tf5.text = [NSString stringWithFormat:@"%ld", (long)indexPath.row + 1];
		[dselectPopoverController dismissPopoverAnimated:NO];
        self.dselectPopoverController = nil;
	}
    
    if(tableView == qtyTableView){
        //[dqtyselected addTarget:self action:@selector(textChanged:event:) forControlEvents:UIControlEventEditingChanged];
        dqtyselected.text = [NSString stringWithFormat:@"%ld", (long)indexPath.row + 1];
        [self textChanged:dqtyselected event:dqtyselected];
        [dselectPopoverController dismissPopoverAnimated:NO];
        self.dselectPopoverController = nil;
    }
    
    if(tableView == multiprodTableView){
        
        //[dqty setUserInteractionEnabled:YES];
        //[dqty becomeFirstResponder];
    }
}

#pragma mark - gesture tapped
- (void)sectionHeaderTapped:(UITapGestureRecognizer *)gestureRecognizer{
    NSIndexPath *indexPath = [NSIndexPath indexPathForRow:0 inSection:gestureRecognizer.view.tag];
    //NSLog(@"arrayForBool::%@", arrayForBool);
    for (int b=0; b<[arrayForBool count]; b++) {
        
        [arrayForBool replaceObjectAtIndex:b withObject:[NSNumber numberWithBool:NO]];
        
        //reload specific section animated
        NSRange range   = NSMakeRange(b, 1);
        NSIndexSet *sectionToReload = [NSIndexSet indexSetWithIndexesInRange:range];
        [multiprodTableView reloadSections:sectionToReload withRowAnimation:UITableViewRowAnimationFade];
    }
    //[arrayForBool replaceObjectAtIndex:indexPath.section withObject:[NSNumber numberWithBool:NO]];
    if (indexPath.row == 0) {
        BOOL collapsed  = [[arrayForBool objectAtIndex:indexPath.section] boolValue];
        collapsed       = !collapsed;
        [arrayForBool replaceObjectAtIndex:indexPath.section withObject:[NSNumber numberWithBool:collapsed]];
        
        //reload specific section animated
        NSRange range   = NSMakeRange(indexPath.section, 1);
        NSIndexSet *sectionToReload = [NSIndexSet indexSetWithIndexesInRange:range];
        [multiprodTableView reloadSections:sectionToReload withRowAnimation:UITableViewRowAnimationFade];
        
        [self displayContent];
    }
}

-(void)mailComposeController:(MFMailComposeViewController *)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError *)error {
    
    switch (result)
    {
        case MFMailComposeResultCancelled:
            NSLog(@"Mail cancelled");
            break;
        case MFMailComposeResultSaved:
            NSLog(@"Mail saved");
            break;
        case MFMailComposeResultSent:
            NSLog(@"Mail sent");
            break;
        case MFMailComposeResultFailed:
            NSLog(@"Mail sent failure: %@", [error localizedDescription]);
            break;
        default:
            break;
    }
    
    [self dismissViewControllerAnimated:NO completion:nil];
}

- (NSMutableArray *)createMutableArray:(NSArray *)array
{
    return [array mutableCopy];
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    [textField resignFirstResponder];
    return YES;
}

- (void)textViewDidBeginEditing:(UITextView *)textField
{
    CGPoint scrollPoint = CGPointMake(0, textField.frame.origin.y - 100);
    [scrollviewAdd setContentOffset:scrollPoint animated:NO];
}

- (void)textViewDidEndEditing:(UITextView *)textView
{
    [scrollviewAdd setContentOffset:CGPointZero animated:NO];
}

- (void)textFieldDidBeginEditing:(UITextField *)textField
{
    CGPoint scrollPoint = CGPointMake(0, textField.frame.origin.y/2.0);
    [scrollviewAdd setContentOffset:scrollPoint animated:NO];
}

- (void)textFieldDidEndEditing:(UITextField *)textView
{
    [scrollviewAdd setContentOffset:CGPointZero animated:NO];
}

- (BOOL) textView: (UITextView *)textView shouldChangeTextInRange:(NSRange )range replacementText:(NSString *)text
{
    if ([text isEqualToString:@"\n"])
    {
        [textView resignFirstResponder];
        return NO;
    }
    return YES;
}

- (void) touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
    [view_bookingAdd endEditing:YES];
}

-(void)viewDidLayoutSubviews
{
    CGFloat scrollViewHeight = 0.0f;
    
    for (UIView* view in scrollView.subviews)
        scrollViewHeight += view.frame.size.height;
    
    [scrollView setContentSize: CGSizeMake(self.view.frame.size.width, scrollViewHeight)];
}

#pragma mark Connect To API
-(void) connectToServer:(NSString *) urlPath
{
    [self addLoadingView];
    
	NSString *urlAddress =[NSString stringWithFormat:@"%@", urlPath];
	
	/*NSURL *url = [NSURL URLWithString:urlAddress];
	
	NSURLRequest *requestObj = [NSURLRequest requestWithURL:url
												cachePolicy:NSURLRequestReloadIgnoringLocalAndRemoteCacheData
											timeoutInterval:30];
	
	NSURLConnection *theConnection=[[NSURLConnection alloc] initWithRequest:requestObj delegate:self];
	if (theConnection) {
		// Create the NSMutableData to hold the received data.
		// receivedData is an instance variable declared elsewhere.
		receivedData = [NSMutableData data];
	} else {
		// Inform the user that the connection failed.
		//[self displayAlert:@"Connection Error. Please try again later."];
		[self displayAlert:NSLocalizedString(@"MSG_CONNECTION_ERROR", nil)];
        [self removeLoadingView];
	} */
    [self getJsonResponse:urlAddress success:^(NSDictionary *responseDict) {
        
        //NSLog(@"%@",responseDict);
        [self successfunction:responseDict];
        
    } failure:^(NSError *error) {
        // release the connection, and the data object
        
        [self removeLoadingView];
        [self displayAlert:NSLocalizedString(@"MSG_CONNECTION_ERROR", nil)];
    }];
}

-(void)getJsonResponse : (NSString *)urlStr success : (void (^)(NSDictionary *responseDict))success failure:(void(^)(NSError* error))failure
{
    
    
    NSURLSession * session = [NSURLSession sharedSession];
    NSURL * url = [NSURL URLWithString: urlStr];
    
    
    // Asynchronously Api is hit here
    NSURLSessionDataTask * dataTask = [session dataTaskWithURL:url completionHandler:^(NSData *data, NSURLResponse *response, NSError *error)
                                       {
                                           
                                           //NSLog(@"%@",data);
                                           NSDictionary * json;
                                           if ([data length]>0) {
                                               json  = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
                                               //NSLog(@"%@",json);
                                               success(json);
                                           } else {
                                               json = nil;
                                               dispatch_async(dispatch_get_main_queue(), ^ {
                                                   [self removeLoadingView];
                                                   [self displayAlert:NSLocalizedString(@"MSG_CONNECTION_ERROR", nil)];
                                               });
                                           }
                                           
                                           
                                       }];
    
    [dataTask resume] ; // Executed First
    
    
}

-(void) successfunction:(NSDictionary *)responseDict {
    dispatch_async(dispatch_get_main_queue(), ^{
        NSString *jsonString = [responseDict JSONRepresentation];
        
        [self removeLoadingView];
        
        if ([currentRequest isEqualToString:@"getBookingList"])
        {
            arrayBookingList = [[NSMutableArray alloc]init];
            
            NSDictionary *dictionary = responseDict;
            
            for(NSDictionary *row in dictionary)
            {
                NSMutableDictionary *mutableDic = [NSMutableDictionary dictionaryWithObjectsAndKeys:
                                                   [row objectForKey:@"id"],@"id",
                                                   [row objectForKey:@"booking_no"],@"booking_no",
                                                   [row objectForKey:@"customer_id"],@"customer_id",
                                                   [row objectForKey:@"product_id"],@"product_id",
                                                   [row objectForKey:@"product_name"],@"product_name",
                                                   
                                                   [row objectForKey:@"branch_id"],@"branch_id",
                                                   [row objectForKey:@"branch_name"],@"branch_name",
                                                   [row objectForKey:@"branch_tel"],@"branch_tel",
                                                   [row objectForKey:@"branch_email"],@"branch_email",
                                                   [row objectForKey:@"branch_latitude"],@"branch_latitude",
                                                   [row objectForKey:@"branch_longitude"],@"branch_longitude",
                                                   
                                                   [row objectForKey:@"date_start"],@"date_start",
                                                   [row objectForKey:@"time_start"],@"time_start",
                                                   [row objectForKey:@"date_end"],@"date_end",
                                                   [row objectForKey:@"time_end"],@"time_end",
                                                   [row objectForKey:@"guess_total"],@"guess_total",
                                                   [row objectForKey:@"notes"],@"notes",
                                                   [row objectForKey:@"status"],@"status",
                                                   [row objectForKey:@"status_desc"],@"status_desc",
                                                   [row objectForKey:@"user_id"],@"user_id",
                                                   [row objectForKey:@"cdate"],@"cdate",
                                                   [row objectForKey:@"mdate"],@"mdate",
                                                   [row objectForKey:@"customer_name"],@"customer_name",
                                                   [row objectForKey:@"qtt_list"],@"qtt_list",
                                                   [row objectForKey:@"price_list"],@"price_list",
                                                   [row objectForKey:@"price_total"],@"price_total",
                                                   nil];
                [arrayBookingList addObject:mutableDic];
            }
            [table_bookingList reloadData];
            
            if ([self isConnectionAvailable])
            {
                NSString *urlAddress = [[NSString alloc] initWithFormat:@"%@booking_setting.php?db=%@&userid=%@&udid=%@&cuid=%@&device=ios&sc=%@&top_id=%@", appdelegate.user_apipath, appdelegate.db, appdelegate.user, appdelegate.uuid, appdelegate.customer_id, appdelegate.cs, appdelegate.plugin_id];
                currentRequest = @"setting";
                //NSLog(@"urlAddress :: %@", urlAddress);
                [self connectToServer:urlAddress];
            }
            else {
                
                [self getDataFromSqlite];
            }
            
            if (isFromSubmit) {
                [self displayContentDetails:0];
            }
        }
        
        if ([currentRequest isEqualToString:@"cancelBooking"])
        {
            NSDictionary *dictionary = responseDict;
            
            for(NSDictionary *row in dictionary)
            {
                [self displayAlert:[row objectForKey:@"desc"]];
                
                if ([[row objectForKey:@"status"] isEqualToString:@"1"])
                {
                    arrayBookingList = nil;
                    
                    [self getbookinglist];
                    //[self.navigationController popViewControllerAnimated:YES];
                }
            }
        }
        
        if ([currentRequest isEqualToString:@"setting"])
        {
            
            sqlite3 *database;
            
            if(sqlite3_open([appdelegate.databasePath UTF8String], &database) == SQLITE_OK) {
                // Setup the SQL Statement and compile it for faster access
                
                int rows=0;
                NSString *theme_object;
                theme_object = [[NSString alloc] initWithFormat:@"SELECT COUNT(*) rec_count FROM data_content WHERE name='booking_setting.php' "];
                
                const char *sqlStatement1 = [theme_object UTF8String];
                sqlite3_stmt *compiledStatement;
                if(sqlite3_prepare_v2(database, sqlStatement1, -1, &compiledStatement, NULL) == SQLITE_OK) {
                    
                    if (sqlite3_step(compiledStatement) == SQLITE_ERROR) {
                        NSAssert1(0,@"Error when counting rows  %s",sqlite3_errmsg(database));
                    } else {
                        rows = sqlite3_column_int(compiledStatement, 0);
                        //NSLog(@"SQLite Rows: %i", rows);
                    }
                    
                    if (rows > 0) {
                        //NSLog(@"database:::::::update conferences");
                        static sqlite3_stmt *compiledupdateStatement;
                        sqlite3_exec(database, [[NSString stringWithFormat:@"UPDATE data_content SET value='%@' WHERE name='booking_setting.php' ", jsonString] UTF8String], NULL, NULL, NULL);
                        //NSLog(@"compiledupdateStatement: %@", compiledupdateStatement);
                        sqlite3_finalize(compiledupdateStatement);
                    } else {
                        //NSLog(@"database:::::::insert into conferences");
                        static sqlite3_stmt *compiledinsertStatement;
                        sqlite3_exec(database, [[NSString stringWithFormat:@"INSERT INTO data_content (name, value) VALUES ('booking_setting.php', '%@') ", jsonString] UTF8String], NULL, NULL, NULL);
                        //NSLog(@"compiledinsertStatement: %@", compiledinsertStatement);
                        sqlite3_finalize(compiledinsertStatement);
                    }
                    
                    [self getDataFromSqlite];

                }
                // Release the compiled statement from memory
                sqlite3_finalize(compiledStatement);
            }
            sqlite3_close(database);
        }
        
        if ([currentRequest isEqualToString:@"submit"])
        {
            
            NSDictionary *dictionary = responseDict;
            
            for(NSDictionary *row in dictionary)
            {
                if ([[row objectForKey:@"status"] isEqualToString:@"1"])
                {
                    isFromSubmit = YES;
                    currentRequest = @"getBookingList";
                    NSString *urlAddress = [[NSString alloc] initWithFormat:@"%@booking_details.php?db=%@&userid=%@&udid=%@&cuid=%@&device=ios&sc=%@&top_id=%@",appdelegate.user_apipath,appdelegate.db,appdelegate.user,appdelegate.uuid,appdelegate.customer_id,appdelegate.cs,appdelegate.plugin_id];
                    [self connectToServer:urlAddress];
                    
                }
                else
                    [self displayAlert:NSLocalizedString(@"rsv_failed_msg", nil)];
            }
        }
    });
}

/*- (void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response
{
    [receivedData setLength:0];
}

- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data
{
    [receivedData appendData:data];
}

- (void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error {
    // release the connection, and the data object
	
    //[connection release];
	//[receivedData release];
	
    [self removeLoadingView];
	[self displayAlert:NSLocalizedString(@"MSG_CONNECTION_ERROR", nil)];
}

- (void)connectionDidFinishLoading:(NSURLConnection *)connection
{
	NSLog(@"Succeed! Received %ld bytes of data",(unsigned long)[receivedData length]);
    [self removeLoadingView];
    
	NSString *response = [[NSString alloc] initWithData:receivedData encoding:NSUTF8StringEncoding];
    //response = [response stringByDecodingHTMLEntities];
    response = [response stringByReplacingOccurrencesOfString:@"'" withString:@"''"];
    //NSLog(@"response: %@", response);
	
	if ([currentRequest isEqualToString:@"getBookingList"])
    {
        arrayBookingList = [[NSMutableArray alloc]init];
        
		NSDictionary *dictionary = [response JSONValue];
        
		for(NSDictionary *row in dictionary)
        {
            NSMutableDictionary *mutableDic = [NSMutableDictionary dictionaryWithObjectsAndKeys:
                                               [row objectForKey:@"id"],@"id",
                                               [row objectForKey:@"booking_no"],@"booking_no",
                                               [row objectForKey:@"customer_id"],@"customer_id",
                                               [row objectForKey:@"product_id"],@"product_id",
                                               [row objectForKey:@"product_name"],@"product_name",
                                               
                                               [row objectForKey:@"branch_id"],@"branch_id",
                                               [row objectForKey:@"branch_name"],@"branch_name",
                                               [row objectForKey:@"branch_tel"],@"branch_tel",
                                               [row objectForKey:@"branch_email"],@"branch_email",
                                               [row objectForKey:@"branch_latitude"],@"branch_latitude",
                                               [row objectForKey:@"branch_longitude"],@"branch_longitude",
                                               
                                               [row objectForKey:@"date_start"],@"date_start",
                                               [row objectForKey:@"time_start"],@"time_start",
                                               [row objectForKey:@"date_end"],@"date_end",
                                               [row objectForKey:@"time_end"],@"time_end",
                                               [row objectForKey:@"guess_total"],@"guess_total",
                                               [row objectForKey:@"notes"],@"notes",
                                               [row objectForKey:@"status"],@"status",
                                               [row objectForKey:@"status_desc"],@"status_desc",
                                               [row objectForKey:@"user_id"],@"user_id",
                                               [row objectForKey:@"cdate"],@"cdate",
                                               [row objectForKey:@"mdate"],@"mdate",
                                               [row objectForKey:@"customer_name"],@"customer_name",
                                               [row objectForKey:@"qtt_list"],@"qtt_list",
                                               [row objectForKey:@"price_list"],@"price_list",
                                               [row objectForKey:@"price_total"],@"price_total",
                                               nil];
            [arrayBookingList addObject:mutableDic];
        }
        [table_bookingList reloadData];
        
        if ([self isConnectionAvailable])
        {
            NSString *urlAddress = [[NSString alloc] initWithFormat:@"%@booking_setting.php?db=%@&userid=%@&udid=%@&cuid=%@&device=ios&sc=%@&top_id=%@", appdelegate.user_apipath, appdelegate.db, appdelegate.user, appdelegate.uuid, appdelegate.customer_id, appdelegate.cs, appdelegate.plugin_id];
            currentRequest = @"setting";
            //NSLog(@"urlAddress :: %@", urlAddress);
            [self connectToServer:urlAddress];
        }
        else
            [self getDataFromSqlite];
        
        if (isFromSubmit) {
            [self displayContentDetails:0];
        }
    }
    
    if ([currentRequest isEqualToString:@"cancelBooking"])
    {
        NSDictionary *dictionary = [response JSONValue];
        
        for(NSDictionary *row in dictionary)
        {
            [self displayAlert:[row objectForKey:@"desc"]];
            
            if ([[row objectForKey:@"status"] isEqualToString:@"1"])
            {
                arrayBookingList = nil;
                
                [self getbookinglist];
                //[self.navigationController popViewControllerAnimated:YES];
            }
        }
    }
    
    if ([currentRequest isEqualToString:@"setting"])
    {
        
        sqlite3 *database;
        
        if(sqlite3_open([appdelegate.databasePath UTF8String], &database) == SQLITE_OK) {
            // Setup the SQL Statement and compile it for faster access
            
            int rows=0;
            NSString *theme_object;
            theme_object = [[NSString alloc] initWithFormat:@"SELECT COUNT(*) rec_count FROM data_content WHERE name='booking_setting.php' "];
            
            const char *sqlStatement1 = [theme_object UTF8String];
            sqlite3_stmt *compiledStatement;
            if(sqlite3_prepare_v2(database, sqlStatement1, -1, &compiledStatement, NULL) == SQLITE_OK) {
                
                if (sqlite3_step(compiledStatement) == SQLITE_ERROR) {
                    NSAssert1(0,@"Error when counting rows  %s",sqlite3_errmsg(database));
                } else {
                    rows = sqlite3_column_int(compiledStatement, 0);
                    //NSLog(@"SQLite Rows: %i", rows);
                }
                
                if (rows > 0) {
                    //NSLog(@"database:::::::update conferences");
                    static sqlite3_stmt *compiledupdateStatement;
                    sqlite3_exec(database, [[NSString stringWithFormat:@"UPDATE data_content SET value='%@' WHERE name='booking_setting.php' ", response] UTF8String], NULL, NULL, NULL);
                    //NSLog(@"compiledupdateStatement: %@", compiledupdateStatement);
                    sqlite3_finalize(compiledupdateStatement);
                } else {
                    //NSLog(@"database:::::::insert into conferences");
                    static sqlite3_stmt *compiledinsertStatement;
                    sqlite3_exec(database, [[NSString stringWithFormat:@"INSERT INTO data_content (name, value) VALUES ('booking_setting.php', '%@') ", response] UTF8String], NULL, NULL, NULL);
                    //NSLog(@"compiledinsertStatement: %@", compiledinsertStatement);
                    sqlite3_finalize(compiledinsertStatement);
                }
                
                [self getDataFromSqlite];
            }
            // Release the compiled statement from memory
            sqlite3_finalize(compiledStatement);
        }
        sqlite3_close(database);
	}
    
    if ([currentRequest isEqualToString:@"submit"])
    {
        
        NSDictionary *dictionary = [response JSONValue];
        
        for(NSDictionary *row in dictionary)
        {
            if ([[row objectForKey:@"status"] isEqualToString:@"1"])
            {
                isFromSubmit = YES;
                currentRequest = @"getBookingList";
                NSString *urlAddress = [[NSString alloc] initWithFormat:@"%@booking_details.php?db=%@&userid=%@&udid=%@&cuid=%@&device=ios&sc=%@&top_id=%@",appdelegate.user_apipath,appdelegate.db,appdelegate.user,appdelegate.uuid,appdelegate.customer_id,appdelegate.cs,appdelegate.plugin_id];
                [self connectToServer:urlAddress];
                
            }
            else
                [self displayAlert:NSLocalizedString(@"rsv_failed_msg", nil)];
        }
    }
    
	//[connection release];
    connection = nil;
	//[response release];
    response = nil;
} */

#pragma mark LoadingView
-(void) addLoadingView
{
    if ([currentRequest isEqualToString:@"cancelBooking"])
        loadingView = [LoadingView loadingViewInView:view_bookingDetail];
    else
        loadingView = [LoadingView loadingViewInView:self.view];
	
}

-(void) removeLoadingView
{
	[loadingView removeView];
}

#pragma mark displayAlert
-(void) displayAlert: (NSString *) stralert {
	NSString *str = [[NSString alloc] initWithFormat:@"%@", stralert];
	UIAlertView *alert = [[UIAlertView alloc] initWithTitle:appdelegate.iconname
													message:str
												   delegate:self
										  cancelButtonTitle:NSLocalizedString(@"done", nil)
										  otherButtonTitles:nil];
	
	[alert show];
    str = nil;
	//[str release];
	//[alert release];
}

- (void)alertView:(UIAlertView *)alertView willDismissWithButtonIndex:(NSInteger)buttonIndex
{
	if (buttonIndex == 0)
    {
		//Clicked Cancel Button
	}
	else
    {
		//Clicked Yes Button
		if ([currentRequest isEqualToString:@"cancelBooking"]) {
			
			currentRequest = @"cancelBooking";
            NSString *urlAddress = [[NSString alloc] initWithFormat:@"%@booking_cancel.php?db=%@&userid=%@&udid=%@&cuid=%@&bookid=%@&device=ios&sc=%@",appdelegate.user_apipath,appdelegate.db,appdelegate.user,appdelegate.uuid,appdelegate.customer_id,reservationId,appdelegate.cs];
            [self connectToServer:urlAddress];
		}
	}
}

- (void) displayContent
{
    [[scrollviewAdd subviews] makeObjectsPerformSelector:@selector(removeFromSuperview)];
    labelheight = 10;
    max_guesstotal = @"";
    NSString *v_product = @"";
    NSString *v_branch = @"";
    NSString *v_datestart = @"";
    NSString *v_timestart = @"";
    NSString *v_dateend = @"";
    NSString *v_timeend = @"";
    NSString *v_guesstotal = @"";
    NSString *v_notes = @"";
    //NSLog(@"listOfContent :%@", listOfContent);
    if([listOfContent count]>0)
    {
        for (int i=0;i<[listOfContent count]; i++)
        {
            NSDictionary *dictionary = [listOfContent objectAtIndex:i];
            
            max_guesstotal = [dictionary objectForKey:@"max_guesstotal"];
            v_product = [dictionary objectForKey:@"v_product"];
            v_branch = [dictionary objectForKey:@"v_branch"];
            v_datestart = [dictionary objectForKey:@"v_datestart"];
            v_timestart = [dictionary objectForKey:@"v_timestart"];
            v_dateend = [dictionary objectForKey:@"v_dateend"];
            v_timeend = [dictionary objectForKey:@"v_timeend"];
            v_guesstotal = [dictionary objectForKey:@"v_guesstotal"];
            v_notes = [dictionary objectForKey:@"v_notes"];
        }
    }
    
    if (![v_product isEqualToString:@"0"] && ![v_product isEqualToString:@""]) {
        UILabel *lblproduct = [[UILabel alloc] initWithFrame:CGRectMake(5, labelheight, self.view.frame.size.width-10, 20)];
        lblproduct.numberOfLines = 0;
        lblproduct.lineBreakMode = NSLineBreakByWordWrapping;
        UIFont *Font1 = [UIFont fontWithName:@"Helvetica" size:16];
        lblproduct.font = Font1;
        lblproduct.text = productname;
        lblproduct.backgroundColor = [UIColor clearColor];
        lblproduct.textAlignment = NSTextAlignmentJustified;
        [scrollviewAdd addSubview:lblproduct];
        CGSize expectedLabelSize = [lblproduct sizeThatFits:CGSizeMake(lblproduct.frame.size.width, FLT_MAX)];
        [lblproduct setFrame:CGRectMake(5, labelheight, self.view.frame.size.width-10, expectedLabelSize.height)];
        labelheight = labelheight + expectedLabelSize.height +10;
        //NSLog(@"labelheight: %d", labelheight);
        lblproduct = nil;
        
        if (![val_multiprod isEqualToString:@"0"]) {
            long heightSection = [sectionTitleArray count]*50;
            for (int ab=0; ab<[arrayForBool count]; ab++) {
                if ([[arrayForBool objectAtIndex:ab] boolValue]==YES) {
                    heightSection = heightSection + [[sectionContentDict valueForKey:[sectionTitleArray objectAtIndex:ab]] count]*50;
                    //NSLog(@"Count:: %lu", [[sectionContentDict valueForKey:[sectionTitleArray objectAtIndex:ab]] count]);
                }
                
            }
            [multiprodTableView setFrame:CGRectMake(5, labelheight, self.view.frame.size.width-10, heightSection+10)];
            [scrollviewAdd addSubview:multiprodTableView];
            labelheight = labelheight + (int)heightSection+10 +10;
        } else {
            [tf setFrame:CGRectMake(5, labelheight, self.view.frame.size.width-10, 40)];
            tf.delegate = self ;
            tf.borderStyle = UITextBorderStyleRoundedRect;
            tf.backgroundColor=[UIColor whiteColor];
            tf.placeholder=NSLocalizedString(@"select_product", nil);
            [scrollviewAdd addSubview:tf];
            labelheight = labelheight + 50;

        }
    }
    
    if (![v_branch isEqualToString:@"0"] && ![v_branch isEqualToString:@""]) {
        UILabel *lblbranch = [[UILabel alloc] initWithFrame:CGRectMake(5, labelheight, self.view.frame.size.width-10, 20)];
        lblbranch.numberOfLines = 0;
        lblbranch.lineBreakMode = NSLineBreakByWordWrapping;
        UIFont *Font = [UIFont fontWithName:@"Helvetica" size:16];
        lblbranch.font = Font;
        lblbranch.text = NSLocalizedString(@"branch", nil);
        lblbranch.backgroundColor = [UIColor clearColor];
        //lbladdr.textColor = [UIColor blackColor];
        [lblbranch sizeToFit];
        //lbladdr.shadowColor = [UIColor blackColor];
        //lbladdr.shadowOffset = CGSizeMake(0, 1);
        [scrollviewAdd addSubview:lblbranch];
        //[lblbranch release];
        lblbranch = nil;
        labelheight = labelheight + 20;
        //NSLog(@"labelheight: %d", labelheight);
        
        [tf1 setFrame:CGRectMake(5, labelheight, self.view.frame.size.width-10, 40)];
        tf1.delegate = self ;
        tf1.borderStyle = UITextBorderStyleRoundedRect;
        tf1.backgroundColor=[UIColor whiteColor];
        tf1.placeholder=NSLocalizedString(@"select_branch", nil);
        [scrollviewAdd addSubview:tf1];
        labelheight = labelheight + 50;
        //NSLog(@"labelheight: %d", labelheight);
    }
    
    if ([v_datestart isEqualToString:@"1"]) {
        
        UILabel *lbldatestart = [[UILabel alloc] initWithFrame:CGRectMake(5, labelheight, (self.view.frame.size.width-20)/2, 20)];
        lbldatestart.numberOfLines = 0;
        lbldatestart.lineBreakMode = NSLineBreakByWordWrapping;
        UIFont *Font2 = [UIFont fontWithName:@"Helvetica" size:16];
        lbldatestart.font = Font2;
        lbldatestart.text = NSLocalizedString(@"date", nil);
        lbldatestart.backgroundColor = [UIColor clearColor];
        //lblpostcode.textColor = [UIColor blackColor];
        [lbldatestart sizeToFit];
        //lblpostcode.shadowColor = [UIColor blackColor];
        //lbladdr.shadowOffset = CGSizeMake(0, 1);
        [scrollviewAdd addSubview:lbldatestart];
        //[lbldatestart release];
        lbldatestart = nil;
        
        UILabel *lbltimestart = [[UILabel alloc] initWithFrame:CGRectMake(self.view.frame.size.width/2+5, labelheight, (self.view.frame.size.width-20)/2, 20)];
        lbltimestart.numberOfLines = 0;
        lbltimestart.lineBreakMode = NSLineBreakByWordWrapping;
        UIFont *Font3 = [UIFont fontWithName:@"Helvetica" size:16];
        lbltimestart.font = Font3;
        lbltimestart.text = NSLocalizedString(@"time", nil);
        lbltimestart.backgroundColor = [UIColor clearColor];
        //lblcity.textColor = [UIColor blackColor];
        [lbltimestart sizeToFit];
        //lblcity.shadowColor = [UIColor blackColor];
        //lbladdr.shadowOffset = CGSizeMake(0, 1);
        [scrollviewAdd addSubview:lbltimestart];
        //[lbltimestart release];
        lbltimestart = nil;
        
        labelheight = labelheight + 20;
        //NSLog(@"labelheight: %d", labelheight);
        
        [tf2 setFrame:CGRectMake(5, labelheight, (self.view.frame.size.width-20)/2, 40)];
        tf2.delegate = self ;
        tf2.borderStyle = UITextBorderStyleRoundedRect;
        tf2.backgroundColor=[UIColor whiteColor];
        tf2.placeholder=NSLocalizedString(@"select_date", nil);
        [scrollviewAdd addSubview:tf2];
        
        [tf3 setFrame:CGRectMake(self.view.frame.size.width/2+5, labelheight, (self.view.frame.size.width-20)/2, 40)];
        tf3.delegate = self ;
        tf3.borderStyle = UITextBorderStyleRoundedRect;
        tf3.backgroundColor=[UIColor whiteColor];
        tf3.placeholder=NSLocalizedString(@"select_time", nil);
        [scrollviewAdd addSubview:tf3];
        
        labelheight = labelheight + 50;
        //NSLog(@"labelheight: %d", labelheight);
    }
    
    if ([v_dateend isEqualToString:@"1"]) {
        UILabel *lbldateend = [[UILabel alloc] initWithFrame:CGRectMake(5, labelheight, (self.view.frame.size.width-20)/2, 20)];
        lbldateend.numberOfLines = 0;
        lbldateend.lineBreakMode = NSLineBreakByWordWrapping;
        UIFont *Font4 = [UIFont fontWithName:@"Helvetica" size:16];
        lbldateend.font = Font4;
        lbldateend.text = @"To Date";
        lbldateend.backgroundColor = [UIColor clearColor];
        //lblstreet.textColor = [UIColor blackColor];
        [lbldateend sizeToFit];
        //lblstreet.shadowColor = [UIColor blackColor];
        //lbladdr.shadowOffset = CGSizeMake(0, 1);
        [scrollviewAdd addSubview:lbldateend];
        //[lbldateend release];
        lbldateend = nil;
        
        UILabel *lbltimeend = [[UILabel alloc] initWithFrame:CGRectMake(self.view.frame.size.width/2+5, labelheight, (self.view.frame.size.width-20)/2, 20)];
        lbltimeend.numberOfLines = 0;
        lbltimeend.lineBreakMode = NSLineBreakByWordWrapping;
        UIFont *Font7 = [UIFont fontWithName:@"Helvetica" size:16];
        lbltimeend.font = Font7;
        lbltimeend.text = @"Time";
        lbltimeend.backgroundColor = [UIColor clearColor];
        //lblstreet.textColor = [UIColor blackColor];
        [lbltimeend sizeToFit];
        //lblstreet.shadowColor = [UIColor blackColor];
        //lbladdr.shadowOffset = CGSizeMake(0, 1);
        [scrollviewAdd addSubview:lbltimeend];
        //[lbltimeend release];
        lbltimeend = nil;
        
        labelheight = labelheight + 20;
        //NSLog(@"labelheight: %d", labelheight);
        
        [tf4 setFrame:CGRectMake(5, labelheight, (self.view.frame.size.width-20)/2, 40)];
        tf4.delegate = self ;
        tf4.borderStyle = UITextBorderStyleRoundedRect;
        tf4.backgroundColor=[UIColor whiteColor];
        tf4.placeholder=@"Select Date";
        [scrollviewAdd addSubview:tf4];
        
        [tf6 setFrame:CGRectMake(self.view.frame.size.width/2+5, labelheight, (self.view.frame.size.width-20)/2, 40)];
        tf6.delegate = self ;
        tf6.borderStyle = UITextBorderStyleRoundedRect;
        tf6.backgroundColor=[UIColor whiteColor];
        tf6.placeholder=@"Select Time";
        [scrollviewAdd addSubview:tf6];
        
        labelheight = labelheight + 50;
        //NSLog(@"labelheight: %d", labelheight);
    }
    
    if ([v_guesstotal isEqualToString:@"1"]) {
        UILabel *lbltotperson = [[UILabel alloc] initWithFrame:CGRectMake(self.view.frame.size.width/2+5, labelheight, (self.view.frame.size.width-20)/2, 20)];
        lbltotperson.numberOfLines = 0;
        lbltotperson.lineBreakMode = NSLineBreakByWordWrapping;
        UIFont *Font5 = [UIFont fontWithName:@"Helvetica" size:16];
        lbltotperson.font = Font5;
        lbltotperson.text = @"Total Person";
        lbltotperson.backgroundColor = [UIColor clearColor];
        //lbladdr.textColor = [UIColor blackColor];
        [lbltotperson sizeToFit];
        //lbladdr.shadowColor = [UIColor blackColor];
        //lbladdr.shadowOffset = CGSizeMake(0, 1);
        [scrollviewAdd addSubview:lbltotperson];
        //[lbltotperson release];
        lbltotperson = nil;
        //labelheight = labelheight + 20;
        //NSLog(@"labelheight: %d", labelheight);
    }
    
    UILabel *lblphone = [[UILabel alloc] initWithFrame:CGRectMake(5, labelheight, (self.view.frame.size.width-20)/2, 20)];
    lblphone.numberOfLines = 0;
    lblphone.lineBreakMode = NSLineBreakByWordWrapping;
    UIFont *Font5 = [UIFont fontWithName:@"Helvetica" size:16];
    lblphone.font = Font5;
    lblphone.text = @"Phone";
    lblphone.backgroundColor = [UIColor clearColor];
    //lbladdr.textColor = [UIColor blackColor];
    [lblphone sizeToFit];
    //lbladdr.shadowColor = [UIColor blackColor];
    //lbladdr.shadowOffset = CGSizeMake(0, 1);
    [scrollviewAdd addSubview:lblphone];
    //[lbltotperson release];
    lblphone = nil;
    labelheight = labelheight + 20;
    //NSLog(@"labelheight: %d", labelheight);
    
    if ([v_guesstotal isEqualToString:@"1"]) {
        [tf5 setFrame:CGRectMake(self.view.frame.size.width/2+5, labelheight, (self.view.frame.size.width-20)/2, 40)];
        //[tf5 setKeyboardType:UIKeyboardTypeNumberPad];
        tf5.delegate = self ;
        tf5.borderStyle = UITextBorderStyleRoundedRect;
        tf5.backgroundColor=[UIColor whiteColor];
        tf5.placeholder = [NSString stringWithFormat:@"Select Total (1 - %@)", max_guesstotal];
        [scrollviewAdd addSubview:tf5];
        //labelheight = labelheight + 50;
        //NSLog(@"labelheight: %d", labelheight);
    }
    
    [tf7 setFrame:CGRectMake(5, labelheight, (self.view.frame.size.width-20)/2, 40)];
    //[tf7 setKeyboardType:UIKeyboardTypeNamePhonePad];
    tf7.delegate = self ;
    tf7.borderStyle = UITextBorderStyleRoundedRect;
    tf7.backgroundColor=[UIColor whiteColor];
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    tf7.text = [defaults objectForKey:@"customer_phone"];
    //tf7.placeholder = [NSString stringWithFormat:@"Select Total (1 - %@)", max_guesstotal];
    [scrollviewAdd addSubview:tf7];
    labelheight = labelheight + 50;
    
    if ([v_notes isEqualToString:@"1"]) {
        UILabel *lblnote = [[UILabel alloc] initWithFrame:CGRectMake(5, labelheight, self.view.frame.size.width-10, 20)];
        lblnote.numberOfLines = 0;
        lblnote.lineBreakMode = NSLineBreakByWordWrapping;
        UIFont *Font6 = [UIFont fontWithName:@"Helvetica" size:16];
        lblnote.font = Font6;
        lblnote.text = @"Notes";
        lblnote.backgroundColor = [UIColor clearColor];
        //lbladdr.textColor = [UIColor blackColor];
        [lblnote sizeToFit];
        //lbladdr.shadowColor = [UIColor blackColor];
        //lbladdr.shadowOffset = CGSizeMake(0, 1);
        [scrollviewAdd addSubview:lblnote];
        //[lblnote release];
        lblnote = nil;
        labelheight = labelheight + 20;
        //NSLog(@"labelheight: %d", labelheight);
        
        [newTextView setFrame:CGRectMake(5, labelheight, self.view.frame.size.width-10, 100)];
        newTextView.delegate = self ;
        [newTextView setFont:[UIFont systemFontOfSize:16]];
        //newTextView.text = contDetails;
        //To make the border look very close to a UITextField
        [newTextView.layer setBorderColor:[[[UIColor grayColor] colorWithAlphaComponent:0.5] CGColor]];
        [newTextView.layer setBorderWidth:1.0];
        
        //The rounded corner part, where you specify your view's corner radius:
        newTextView.layer.cornerRadius = 5;
        newTextView.clipsToBounds = YES;
        newTextView.userInteractionEnabled = YES;
        [scrollviewAdd addSubview:newTextView];
        labelheight = labelheight + 110;
    }
    
    UIButton *buttonsubmit = [UIButton buttonWithType:UIButtonTypeRoundedRect];
    buttonsubmit.tag = 1001;
    [buttonsubmit addTarget:self action:@selector(aMethod:) forControlEvents:UIControlEventTouchUpInside];
    [buttonsubmit setTitle:@"Submit" forState:UIControlStateNormal];
    [buttonsubmit setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    [buttonsubmit setBackgroundColor:[UIColor clearColor]];
    
    float buttonWidth = 200.0f;
    buttonsubmit.frame = CGRectMake(CGRectGetMidX(self.view.frame)-(buttonWidth/2), labelheight, buttonWidth, 44.0);
    [scrollviewAdd addSubview:buttonsubmit];
    
    scrollViewHeight = 0.0f;
    
    for (UIView* view in scrollviewAdd.subviews)
        scrollViewHeight += view.frame.size.height;
    
    scrollviewAdd.contentSize = CGSizeMake(scrollviewAdd.frame.size.width, labelheight + 400);
    
    UITapGestureRecognizer *gestureRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:[self scrollviewAdd] action:@selector(touchesBegan:withEvent:)];
    gestureRecognizer.delegate = self;
    [scrollviewAdd addGestureRecognizer:gestureRecognizer];
}

- (void)aMethod:(id)sender
{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSString *hasLoggedInFB = [defaults objectForKey:@"hasLoggedInFB"];
    NSString *hasLoggedInMyApp = [defaults objectForKey:@"hasLoggedInMyApp"];
    
    if ([hasLoggedInFB isEqualToString:@"YES"] || [hasLoggedInMyApp isEqualToString:@"YES"])
    {
        UIButton *clicked = (UIButton *) sender;
        
        //Submit button
        if (clicked.tag==1001)
        {
            //if ([self internetConnectionExists])
            if ([self isConnectionAvailable])
            {
                
                NSString *max_guesstotal=@"";
                NSString *v_product=@"";
                NSString *v_branch=@"";
                NSString *v_datestart=@"";
                NSString *v_timestart=@"";
                NSString *v_dateend=@"";
                NSString *v_timeend=@"";
                
                if([listOfContent count]>0)
                {
                    for (int i=0;i<[listOfContent count]; i++)
                    {
                        NSDictionary *dictionary = [listOfContent objectAtIndex:i];
                        max_guesstotal = [dictionary objectForKey:@"max_guesstotal"];
                        v_product = [dictionary objectForKey:@"v_product"];
                        v_branch = [dictionary objectForKey:@"v_branch"];
                        v_datestart = [dictionary objectForKey:@"v_datestart"];
                        v_timestart = [dictionary objectForKey:@"v_timestart"];
                        v_dateend = [dictionary objectForKey:@"v_dateend"];
                        v_timeend = [dictionary objectForKey:@"v_timeend"];
                    }
                }
                
                float prodTotal = 0;
                
                if ([val_multiprod isEqualToString:@"1"] ) {
                    NSMutableArray *selections = [[NSMutableArray alloc] init];
                    NSMutableArray *selectionsQty = [[NSMutableArray alloc] init];
                    NSMutableArray *selectionsPrice = [[NSMutableArray alloc] init];
                    float curtot = 0;
                    //NSLog(@"arSelectedRows:: %@", arSelectedRows);
                    for(NSIndexPath *indexPath in arSelectedRows) {
                        NSUInteger index = [arSelectedRows indexOfObject:indexPath];
                        NSDictionary *dictxtfield = [textfieldSelectedRows objectAtIndex:index];
                        NSDictionary *dicprod = [prodSelectedRows objectAtIndex:index];
                        //NSLog(@"prodSelectedRows:: %@", prodSelectedRows);
                        [selectionsQty addObject:[dictxtfield objectForKey:@"textFieldString"]];
                        
                        /*long idxrow=0;
                        if (indexPath.section>0) {
                            int totalrows=0;
                            for (int ids=0; ids<indexPath.section; ids++) {
                                totalrows += [self tableView:multiprodTableView numberOfRowsInSection:ids];
                            }
                            idxrow = totalrows + indexPath.row;
                        } else {
                            idxrow = indexPath.row;
                        } */
                        //NSLog(@"textfieldSelectedRows11::%@", textfieldSelectedRows);
                        NSPredicate *predicate = [NSPredicate predicateWithFormat:@"(product_id == %@)", dicprod];
                        //NSLog(@"filtertextfieldSelectedRows: %@", [textfieldSelectedRows filteredArrayUsingPredicate:predicate]);
                        if ([[listOfprod filteredArrayUsingPredicate:predicate] count]>0) {
                            NSDictionary *dictionary = [[listOfprod filteredArrayUsingPredicate:predicate] objectAtIndex:0];
                            //[cname removeFromSuperview];
                            //NSDictionary *dictionary = [listOfprod objectAtIndex:indexprod];
                            curtot = floorf(([[dictionary objectForKey:@"price"] floatValue] * [[dictxtfield objectForKey:@"textFieldString"] floatValue]) * 100 + 0.5) / 100;
                            [selections addObject:[dictionary objectForKey:@"product_id"]];
                            [selectionsPrice addObject:[NSString stringWithFormat:@"%.02f", curtot]];
                            prodTotal += curtot;
                        }
                        
                    }
                    
                    prodid = [selections componentsJoinedByString:@","];
                    prodQty = [selectionsQty componentsJoinedByString:@","];
                    prodPrice = [selectionsPrice componentsJoinedByString:@","];
                    //NSLog(@"prodid::%@", prodid);
                    //NSLog(@"prodQty::%@", prodQty);
                    //NSLog(@"prodPrice::%@", prodPrice);
                }
                
                if ([tf5.text intValue] > [max_guesstotal intValue])
                {
                    NSString *msg = [NSString stringWithFormat:@"Total Person cannot more than %@! ", max_guesstotal];
                    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Information" message:msg delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
                    [alert show];
                    //[alert release];
                    alert = nil;
                } else if ([self validPhone:[tf7 text]] == NO) {
                    NSString *msg = [NSString stringWithFormat:@"Phone Number Required/Invalid Phone Number! "];
                    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Information" message:msg delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
                    [alert show];
                    //[alert release];
                    alert = nil;
                } else if ((![v_product isEqualToString:@"0"] && ![v_product isEqualToString:@""]) && [prodid length]==0) {
                    NSString *msg = product_err;
                    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Information" message:msg delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
                    [alert show];
                    //[alert release];
                    alert = nil;
                    alert = nil;
                } else if ((![v_branch isEqualToString:@"0"] && ![v_branch isEqualToString:@""]) && [[tf1 text] length]==0) {
                    NSString *msg = [NSString stringWithFormat:@"Branch Required! "];;
                    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Information" message:msg delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
                    [alert show];
                    //[alert release];
                    alert = nil;
                } else if ([v_datestart isEqualToString:@"1"] && ([[tf3 text] length]==0 || [[tf2 text] length]==0)) {
                    NSString *msg = [NSString stringWithFormat:@"Date Time Required! "];;
                    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Information" message:msg delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
                    [alert show];
                    //[alert release];
                    alert = nil;
                } else if ([v_dateend isEqualToString:@"1"] && ([[tf6 text] length]==0 || [[tf4 text] length]==0)) {
                    NSString *msg = [NSString stringWithFormat:@"Date Time Required! "];;
                    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Information" message:msg delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
                    [alert show];
                    //[alert release];
                    alert = nil;
                }
                else
                {
                    
                    NSString *urlAddressSubmit = [[NSString alloc] initWithFormat:@"%@booking_submit.php?db=%@&userid=%@&udid=%@&cuid=%@&product_id=%@&branch_id=%@&date_start=%@&time_start=%@&date_end=%@&time_end=%@&guess_total=%@&notes=%@&cust_phone=%@&device=ios&sc=%@&top_id=%@&qtt_list=%@&price_list=%@&price_total=%f", appdelegate.user_apipath, appdelegate.db, appdelegate.user, appdelegate.uuid, appdelegate.customer_id, prodid, branchid, [tf2 text], [tf3 text], [tf4 text], [tf6 text], [tf5 text], [self urlEncode:[newTextView text]], [tf7 text], appdelegate.cs, appdelegate.plugin_id, prodQty, prodPrice, prodTotal];
                    currentRequest = @"submit";
                    [self connectToServer:urlAddressSubmit];
                    //NSLog(@"urlAddressSubmit:: %@", urlAddressSubmit);
                    //Maggie 18Dec2013 link to Pre-reservation page & Thank you message
                }
            }
        } else
            [self getbookinglist];
    }
    else
        [self.view addSubview:view_popoverMain];
}

- (BOOL) validPhone:(NSString*) phoneString {
    
    NSError *error = NULL;
    NSDataDetector *detector = [NSDataDetector dataDetectorWithTypes:NSTextCheckingTypePhoneNumber error:&error];
    
    NSRange inputRange = NSMakeRange(0, [phoneString length]);
    NSArray *matches = [detector matchesInString:phoneString options:0 range:inputRange];
    
    // no match at all
    if ([matches count] == 0) {
        return NO;
    }
    
    // found match but we need to check if it matched the whole string
    NSTextCheckingResult *result = (NSTextCheckingResult *)[matches objectAtIndex:0];
    
    if ([result resultType] == NSTextCheckingTypePhoneNumber && result.range.location == inputRange.location && result.range.length == inputRange.length) {
        // it matched the whole string
        return YES;
    }
    else {
        // it only matched partial string
        return NO;
    }
}

- (IBAction)logIn:(id)sender
{
    NSString *nibFileName;
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone) {
        nibFileName = @"SettingViewController";
    } else {
        nibFileName = @"SettingViewController_ipad";
    }
    SettingViewController *settingViewController = [[SettingViewController alloc] initWithNibName:nibFileName bundle:nil];
    appdelegate.shouldReturnToPrevPage = YES;
    appdelegate.originVC = self;
    [appdelegate.navController pushViewController:settingViewController animated:NO];
    
    [view_popoverMain removeFromSuperview];
}

- (IBAction)closeLoginPopup:(id)sender
{
    [view_popoverMain removeFromSuperview];
}

-(NSString *) urlEncode:(NSString *) theStr {
    NSString *encoded = (NSString *)CFBridgingRelease(CFURLCreateStringByAddingPercentEscapes(
                                                                                              NULL,
                                                                                              (CFStringRef)theStr,
                                                                                              NULL,
                                                                                              (CFStringRef)@"!*'\"();:@&=+$,/?%#[]% ",
                                                                                              //																		NSUnicodeStringEncoding );
                                                                                              kCFStringEncodingUTF8 ));
    return encoded;
}

#pragma mark -
#pragma mark WEPopoverControllerDelegate implementation

- (void)popoverControllerDidDismissPopover:(WEPopoverController *)thePopoverController {
	//Safe to release the popover here
	self.branchPopoverController = nil;
    self.prodPopoverController = nil;
    self.dselectPopoverController = nil;
}

- (BOOL)popoverControllerShouldDismissPopover:(WEPopoverController *)thePopoverController {
	//The popover is automatically dismissed if you click outside it, unless you return NO here
	return YES;
}

- (WEPopoverContainerViewProperties *)improvedContainerViewProperties {
	
	WEPopoverContainerViewProperties *props = [WEPopoverContainerViewProperties alloc];
	NSString *bgImageName = nil;
	CGFloat bgMargin = 0.0;
	CGFloat bgCapSize = 0.0;
	CGFloat contentMargin = 4.0;
	
	bgImageName = @"popoverBg.png";
	
	// These constants are determined by the popoverBg.png image file and are image dependent
	bgMargin = 13; // margin width of 13 pixels on all sides popoverBg.png (62 pixels wide - 36 pixel background) / 2 == 26 / 2 == 13
	bgCapSize = 31; // ImageSize/2  == 62 / 2 == 31 pixels
	
	props.leftBgMargin = bgMargin;
	props.rightBgMargin = bgMargin;
	props.topBgMargin = bgMargin;
	props.bottomBgMargin = bgMargin;
	props.leftBgCapSize = bgCapSize;
	props.topBgCapSize = bgCapSize;
	props.bgImageName = bgImageName;
	props.leftContentMargin = contentMargin;
	props.rightContentMargin = contentMargin - 1; // Need to shift one pixel for border to look correct
	props.topContentMargin = contentMargin;
	props.bottomContentMargin = contentMargin;
	
	props.arrowMargin = 4.0;
	
	props.upArrowImageName = @"popoverArrowUp.png";
	props.downArrowImageName = @"popoverArrowDown.png";
	props.leftArrowImageName = @"popoverArrowLeft.png";
	props.rightArrowImageName = @"popoverArrowRight.png";
	return props;
}

- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField {
    
    if(textField == tf1)
    {
        [branchTableView reloadData];
        
        if (branchContent == nil )
        {
            branchContent = [[UIViewController alloc] init];
            branchContent.view = branchTableView;
        }
        else
        {
            if (branchContent.view != branchTableView) {
                branchContent.view = branchTableView;
            }
        }
        
        if (!self.branchPopoverController)
        {
            self.branchPopoverController = [[WEPopoverController alloc] initWithContentViewController:branchContent];
            self.branchPopoverController.popoverContentSize = CGSizeMake(400.0f, 600.0f);
            
            if ([self.branchPopoverController respondsToSelector:@selector(setContainerViewProperties:)])
                [self.branchPopoverController setContainerViewProperties:[self improvedContainerViewProperties]];
            
            self.branchPopoverController.delegate = self;
            self.branchPopoverController.passthroughViews = [NSArray arrayWithObject:appdelegate.navController.navigationBar];
            [self.branchPopoverController presentPopoverFromRect:textField.bounds inView:textField permittedArrowDirections:UIPopoverArrowDirectionUp animated:NO];
        }
        else
        {
            [self.branchPopoverController dismissPopoverAnimated:NO];
            self.branchPopoverController = nil;
        }
        return NO;  // Hide both keyboard and blinking cursor.
    } else
    
    if(textField == tf){
        
        [prodTableView reloadData];
        
        if (prodContent == nil )
        {
            prodContent = [[UIViewController alloc] init];
            prodContent.view = prodTableView;
        }
        else
        {
            if (prodContent.view != prodTableView) {
                prodContent.view = prodTableView;
            }
        }
        
        if (!self.prodPopoverController)
        {
            self.prodPopoverController = [[WEPopoverController alloc] initWithContentViewController:prodContent];
            self.prodPopoverController.popoverContentSize = CGSizeMake(400.0f, 600.0f);
            if ([self.prodPopoverController respondsToSelector:@selector(setContainerViewProperties:)])
                [self.prodPopoverController setContainerViewProperties:[self improvedContainerViewProperties]];
            
            self.prodPopoverController.delegate = self;
            self.prodPopoverController.passthroughViews = [NSArray arrayWithObject:appdelegate.navController.navigationBar];
            [self.prodPopoverController presentPopoverFromRect:textField.bounds inView:textField permittedArrowDirections:UIPopoverArrowDirectionUp animated:NO];
        }
        else
        {
            [self.prodPopoverController dismissPopoverAnimated:NO];
            self.prodPopoverController = nil;
        }
        
        return NO;  // Hide both keyboard and blinking cursor.
        
    } else
    
    if(textField == tf2){
        
        [dselectTableView reloadData];
        
        if (dselectContent == nil )
        {
            dselectContent = [[UIViewController alloc] init];
            dselectContent.view = dselectTableView;
        }
        else
        {
            if (dselectContent.view != dselectTableView) {
                dselectContent.view = dselectTableView;
            }
        }
        
        if (!self.dselectPopoverController) {
            self.dselectPopoverController = [[WEPopoverController alloc] initWithContentViewController:dselectContent];
            self.dselectPopoverController.popoverContentSize = CGSizeMake(400.0f, 600.0f);
            if ([self.dselectPopoverController respondsToSelector:@selector(setContainerViewProperties:)]) {
                [self.dselectPopoverController setContainerViewProperties:[self improvedContainerViewProperties]];
            }
            self.dselectPopoverController.delegate = self;
            self.dselectPopoverController.passthroughViews = [NSArray arrayWithObject:appdelegate.navController.navigationBar];
            [self.dselectPopoverController presentPopoverFromRect:textField.bounds inView:textField permittedArrowDirections:UIPopoverArrowDirectionUp animated:NO];
            
        } else {
            [self.dselectPopoverController dismissPopoverAnimated:NO];
            self.dselectPopoverController = nil;
        }
        
        return NO;  // Hide both keyboard and blinking cursor.
        
    } else
    
    if(textField == tf3){
        
        [tselectTableView reloadData];
        
        if (dselectContent == nil )
        {
            dselectContent = [[UIViewController alloc] init];
            dselectContent.view = tselectTableView;
        }
        else
        {
            if (dselectContent.view != tselectTableView) {
                dselectContent.view = tselectTableView;
            }
        }
        
        if (!self.dselectPopoverController) {
            self.dselectPopoverController = [[WEPopoverController alloc] initWithContentViewController:dselectContent];
            self.dselectPopoverController.popoverContentSize = CGSizeMake(400.0f, 600.0f);
            if ([self.dselectPopoverController respondsToSelector:@selector(setContainerViewProperties:)]) {
                [self.dselectPopoverController setContainerViewProperties:[self improvedContainerViewProperties]];
            }
            self.dselectPopoverController.delegate = self;
            self.dselectPopoverController.passthroughViews = [NSArray arrayWithObject:appdelegate.navController.navigationBar];
            [self.dselectPopoverController presentPopoverFromRect:textField.bounds inView:textField permittedArrowDirections:UIPopoverArrowDirectionUp animated:NO];
            
        } else {
            [self.dselectPopoverController dismissPopoverAnimated:NO];
            self.dselectPopoverController = nil;
        }
        
        return NO;  // Hide both keyboard and blinking cursor.
        
    } else
    
    if(textField == tf4){
        
        [dendselectTableView reloadData];
        
        if (dselectContent == nil )
        {
            dselectContent = [[UIViewController alloc] init];
            dselectContent.view = dendselectTableView;
        }
        else
        {
            if (dselectContent.view != dendselectTableView) {
                dselectContent.view = dendselectTableView;
            }
        }
        
        if (!self.dselectPopoverController) {
            self.dselectPopoverController = [[WEPopoverController alloc] initWithContentViewController:dselectContent];
            self.dselectPopoverController.popoverContentSize = CGSizeMake(400.0f, 600.0f);
            if ([self.dselectPopoverController respondsToSelector:@selector(setContainerViewProperties:)]) {
                [self.dselectPopoverController setContainerViewProperties:[self improvedContainerViewProperties]];
            }
            self.dselectPopoverController.delegate = self;
            self.dselectPopoverController.passthroughViews = [NSArray arrayWithObject:appdelegate.navController.navigationBar];
            [self.dselectPopoverController presentPopoverFromRect:textField.bounds inView:textField permittedArrowDirections:UIPopoverArrowDirectionUp animated:NO];
            
        } else {
            [self.dselectPopoverController dismissPopoverAnimated:NO];
            self.dselectPopoverController = nil;
        }
        
        return NO;  // Hide both keyboard and blinking cursor.
        
    } else
    
    if(textField == tf5){
        
        [totalGuestTableView reloadData];
        
        if (dselectContent == nil )
        {
            dselectContent = [[UIViewController alloc] init];
            dselectContent.view = totalGuestTableView;
        }
        else
        {
            if (dselectContent.view != totalGuestTableView) {
                dselectContent.view = totalGuestTableView;
            }
        }
        
        if (!self.dselectPopoverController) {
            self.dselectPopoverController = [[WEPopoverController alloc] initWithContentViewController:dselectContent];
            self.dselectPopoverController.popoverContentSize = CGSizeMake(400.0f, 600.0f);
            if ([self.dselectPopoverController respondsToSelector:@selector(setContainerViewProperties:)]) {
                [self.dselectPopoverController setContainerViewProperties:[self improvedContainerViewProperties]];
            }
            self.dselectPopoverController.delegate = self;
            self.dselectPopoverController.passthroughViews = [NSArray arrayWithObject:appdelegate.navController.navigationBar];
            [self.dselectPopoverController presentPopoverFromRect:textField.bounds inView:textField permittedArrowDirections:UIPopoverArrowDirectionUp animated:NO];
            
        } else {
            [self.dselectPopoverController dismissPopoverAnimated:NO];
            self.dselectPopoverController = nil;
        }
        
        return NO;  // Hide both keyboard and blinking cursor.
    } else
    
    
    if(textField == tf6){
        
        [tendselectTableView reloadData];
        
        if (dselectContent == nil )
        {
            dselectContent = [[UIViewController alloc] init];
            dselectContent.view = tendselectTableView;
        }
        else
        {
            if (dselectContent.view != tendselectTableView) {
                dselectContent.view = tendselectTableView;
            }
        }
        
        if (!self.dselectPopoverController) {
            self.dselectPopoverController = [[WEPopoverController alloc] initWithContentViewController:dselectContent];
            self.dselectPopoverController.popoverContentSize = CGSizeMake(400.0f, 600.0f);
            if ([self.dselectPopoverController respondsToSelector:@selector(setContainerViewProperties:)]) {
                [self.dselectPopoverController setContainerViewProperties:[self improvedContainerViewProperties]];
            }
            self.dselectPopoverController.delegate = self;
            self.dselectPopoverController.passthroughViews = [NSArray arrayWithObject:appdelegate.navController.navigationBar];
            [self.dselectPopoverController presentPopoverFromRect:textField.bounds inView:textField permittedArrowDirections:UIPopoverArrowDirectionUp animated:NO];
            
        } else {
            [self.dselectPopoverController dismissPopoverAnimated:NO];
            self.dselectPopoverController = nil;
        }
        
        return NO;  // Hide both keyboard and blinking cursor.
        
    }else if(textField == tf7){
        
    } else {
        
        dqtyselected = textField;
        
        [qtyTableView reloadData];
        
        if (dselectContent == nil )
        {
            dselectContent = [[UIViewController alloc] init];
            dselectContent.view = qtyTableView;
        }
        else
        {
            if (dselectContent.view != qtyTableView) {
                dselectContent.view = qtyTableView;
            }
        }
        
        if (!self.dselectPopoverController) {
            self.dselectPopoverController = [[WEPopoverController alloc] initWithContentViewController:dselectContent];
            self.dselectPopoverController.popoverContentSize = CGSizeMake(400.0f, 600.0f);
            if ([self.dselectPopoverController respondsToSelector:@selector(setContainerViewProperties:)]) {
                [self.dselectPopoverController setContainerViewProperties:[self improvedContainerViewProperties]];
            }
            self.dselectPopoverController.delegate = self;
            self.dselectPopoverController.passthroughViews = [NSArray arrayWithObject:appdelegate.navController.navigationBar];
            [self.dselectPopoverController presentPopoverFromRect:textField.bounds inView:textField permittedArrowDirections:UIPopoverArrowDirectionUp animated:NO];
            
        } else {
            [self.dselectPopoverController dismissPopoverAnimated:NO];
            self.dselectPopoverController = nil;
        }
        
        return NO;  // Hide both keyboard and blinking cursor.
    }
        
    return YES;
}

- (void)getDataFromSqlite
{
    
    sqlite3 *database;
    
    if(sqlite3_open([appdelegate.databasePath UTF8String], &database) == SQLITE_OK) {
        // Setup the SQL Statement and compile it for faster access
        
        NSString *sqllocal1, *sqlproduct, *sqlbranch, *sqlprodcat, *val_branch, *val_product;
        [listOfContent removeAllObjects];
        [listOfbranch removeAllObjects];
        [listOfprod removeAllObjects];
        [listAvaiDate removeAllObjects];
        [listAvaiDateTime removeAllObjects];
        [listAvaiDateTimeOri removeAllObjects];
        [sectionTitleArray removeAllObjects];
        [arrayForBool removeAllObjects];
        [sectionContentDict removeAllObjects];
        
        sqllocal1 = [[NSString alloc] initWithFormat:@"SELECT * FROM data_content WHERE TRIM(name) = 'booking_setting.php'"];
        
        NSString *apivalue;
        const char *sqlStatement1 = [sqllocal1 UTF8String];
        sqlite3_stmt *compiledStatement1;
        if(sqlite3_prepare_v2(database, sqlStatement1, -1, &compiledStatement1, NULL) == SQLITE_OK) {
            
            while(sqlite3_step(compiledStatement1) == SQLITE_ROW) {
                
                apivalue = [NSString stringWithUTF8String:(char *)sqlite3_column_text(compiledStatement1, 2)];
                
                NSDictionary *dictionary = [apivalue JSONValue];
                
                listOfContent = [[NSMutableArray alloc] init];
                for(NSDictionary *row in dictionary)
                    [listOfContent addObject:row];
                
            }
            
        }
        
        //NSLog(@"listOfContent:: %@",listOfContent);
        
        NSMutableArray *listAvaiDateTemp;
        NSMutableArray *listAvaiTime;
        listAvaiDateTemp = [[NSMutableArray alloc] init];
        listAvaiTime = [[NSMutableArray alloc] init];
        
        if([listOfContent count]>0){
            for (int i=0;i<[listOfContent count]; i++) {
                NSDictionary *dictionary = [listOfContent objectAtIndex:i];
                
                NSString *avai_date = [dictionary objectForKey:@"avai_date"];
                NSString *avai_time = [dictionary objectForKey:@"avai_time"];
                val_branch = [dictionary objectForKey:@"v_branch"];
                val_product = [dictionary objectForKey:@"v_product"];
                val_multiprod = [dictionary objectForKey:@"multiple_product"];
                val_bookingtitle = [dictionary objectForKey:@"booking_title"];
                val_tq_message = [dictionary objectForKey:@"tq_message"];
                display_price = [dictionary objectForKey:@"display_price"];
                display_total = [dictionary objectForKey:@"display_total"];
                productname = [dictionary objectForKey:@"product_title"];
                display_qtt = [dictionary objectForKey:@"display_qtt"];
                product_err = [dictionary objectForKey:@"product_err"];
                
                listAvaiDateTemp = [[avai_date componentsSeparatedByString: @","] mutableCopy];
                listAvaiTime = [[avai_time componentsSeparatedByString: @","] mutableCopy];
                
                //NSLog(@"display_price:: %@",display_price);
            }
        }
        
        NSDictionary *dicSummTemp1;
        
        for (int j=0;j<[listAvaiDateTemp count]; j++) {
            NSDictionary *data = [listAvaiDateTemp objectAtIndex:j];
            NSString *datebook = [NSString stringWithFormat:@"%@",data];
            
            NSDateFormatter *df = [[NSDateFormatter alloc] init];
            [df setDateFormat:@"yyyy-MM-dd"];
            NSDate *d = [df dateFromString:datebook];
            //[df setDateFormat:@"yyyy-MM-dd"];
            NSString *DateString = [df stringFromDate:d];
            //NSLog(@"listAvaiDate %@",DateString);
            
            dicSummTemp1 = [NSDictionary dictionaryWithObjectsAndKeys:DateString,@"dateReserv",nil];
            [listAvaiDate addObject:dicSummTemp1];
            
            NSArray *copy = [listAvaiDate copy];
            NSInteger index = [copy count] - 1;
            for (id object in [copy reverseObjectEnumerator]) {
                if ([listAvaiDate indexOfObject:object inRange:NSMakeRange(0, index)] != NSNotFound) {
                    [listAvaiDate removeObjectAtIndex:index];
                }
                index--;
            }
            copy = nil;;
        }
        
        for (int j=0;j<[listAvaiTime count]; j++)
        {
            NSDictionary *data = [listAvaiTime objectAtIndex:j];
            NSString *datebook = [NSString stringWithFormat:@"%@",data];
            
            NSDateFormatter *df = [[NSDateFormatter alloc] init];
            [df setDateFormat:@"HH:mm"];
            NSDate *d = [df dateFromString:datebook];
            
            NSString *DateString = [df stringFromDate:d];
            
            dicSummTemp1 = [NSDictionary dictionaryWithObjectsAndKeys:DateString,@"dateReserv",nil];
            [listAvaiDateTimeOri addObject:dicSummTemp1];
        }
        
        // Release the compiled statement from memory
        sqlite3_finalize(compiledStatement1);
        
        if ([val_multiprod isEqualToString:@"1"] ) {
            
            if (![val_product isEqual:[NSNull null]]) {
                if ([val_product rangeOfString:@","].location == NSNotFound) {
                    //[stringArray arrayByAddingObject:];
                    strings = [NSArray arrayWithObjects:val_product, nil];
                } else {
                    strings = [[val_product componentsSeparatedByString:@","] mutableCopy];
                }
            }
            //NSLog(@"strings::%@",strings);
            listOfprodcat = [[NSMutableArray alloc] init];
            sectionTitleArray = [[NSMutableArray alloc] init];
            arrayForBool = [[NSMutableArray alloc] init];
            for (int st=0; st<[strings count]; st++) {
                sqlprodcat = [[NSString alloc] initWithFormat:@"SELECT * FROM content_category WHERE id =%@ ", [strings objectAtIndex:st]];
                
                NSString *prodcat_name, *cat_id;
                const char *sqlStatement4 = [sqlprodcat UTF8String];
                sqlite3_stmt *compiledStatement4;
                if(sqlite3_prepare_v2(database, sqlStatement4, -1, &compiledStatement4, NULL) == SQLITE_OK) {
                    
                    NSDictionary *dictionaryprodcat;
                    while(sqlite3_step(compiledStatement4) == SQLITE_ROW) {
                        
                        cat_id = [NSString stringWithUTF8String:(char *)sqlite3_column_text(compiledStatement4, 0)];
                        prodcat_name = [NSString stringWithUTF8String:(char *)sqlite3_column_text(compiledStatement4, 1)];
                        
                        prodcat_name = [prodcat_name stringByDecodingHTMLEntities];
                        prodcat_name = [prodcat_name stringByReplacingOccurrencesOfString:@"''" withString:@"'"];
                        
                        
                        dictionaryprodcat = [NSDictionary dictionaryWithObjectsAndKeys:cat_id,@"cat_id",prodcat_name,@"prodcat_name",nil];
                        [listOfprodcat addObject:dictionaryprodcat];
                        [sectionTitleArray addObject:prodcat_name];
                        
                        [arrayForBool addObject:[NSNumber numberWithBool:NO]];
                        
                    }
                    
                }
                sqlite3_finalize(compiledStatement4);
                
            }
            
            sectionContentDict  = [[NSMutableDictionary alloc] init];
            listOfprod = [[NSMutableArray alloc] init];
            for (int p=0;p<[sectionTitleArray count]; p++)
            {
                NSDictionary *data = [listOfprodcat objectAtIndex:p];
                
                NSString *cat_id = [NSString stringWithFormat:@"%@",[data objectForKey:@"cat_id"]];
                
                sqlproduct = [[NSString alloc] initWithFormat:@"SELECT * FROM contents WHERE category_id = '%@' ORDER BY orderno ", cat_id];
                
                NSString *product_id, *product_title, *price_title;
                double price;
                const char *sqlStatement2 = [sqlproduct UTF8String];
                sqlite3_stmt *compiledStatement2;
                if(sqlite3_prepare_v2(database, sqlStatement2, -1, &compiledStatement2, NULL) == SQLITE_OK) {
                    
                    NSDictionary *dictionarypro;
                    NSArray *array  = [NSArray array];
                    while(sqlite3_step(compiledStatement2) == SQLITE_ROW) {
                        product_id = [NSString stringWithUTF8String:(char *)sqlite3_column_text(compiledStatement2, 0)];
                        product_title = [NSString stringWithUTF8String:(char *)sqlite3_column_text(compiledStatement2, 1)];
                        price = (float) sqlite3_column_double(compiledStatement2, 24);
                        price_title = [NSString stringWithUTF8String:(char *)sqlite3_column_text(compiledStatement2, 23)];
                        //NSLog(@"price :: %@", [NSString stringWithFormat:@"%.02f", price]);
                        product_title = [product_title stringByDecodingHTMLEntities];
                        product_title = [product_title stringByReplacingOccurrencesOfString:@"''" withString:@"'"];
                        
                        price_title = [price_title stringByDecodingHTMLEntities];
                        price_title = [price_title stringByReplacingOccurrencesOfString:@"''" withString:@"'"];
                        
                        dictionarypro = [NSDictionary dictionaryWithObjectsAndKeys:product_id,@"product_id",product_title,@"product_title",[NSString stringWithFormat:@"%.02f", price],@"price",price_title,@"price_title",nil];
                        [listOfprod addObject:dictionarypro];
                        array = [array arrayByAddingObject:product_title];
                        [sectionContentDict setValue:array forKey:[sectionTitleArray objectAtIndex:p]];
                        //NSLog(@"sectionContentDict :: %@", sectionContentDict);
                    }
                    
                }
                sqlite3_finalize(compiledStatement2);
                
            }
            
            //NSLog(@"listOfprod :: %@", listOfprod);
           
        } else {
        
            sqlproduct = [[NSString alloc] initWithFormat:@"SELECT * FROM contents WHERE category_id IN (%@) ", val_product];
            
            NSString *product_id, *product_title;
            const char *sqlStatement2 = [sqlproduct UTF8String];
            sqlite3_stmt *compiledStatement2;
            if(sqlite3_prepare_v2(database, sqlStatement2, -1, &compiledStatement2, NULL) == SQLITE_OK) {
                
                NSDictionary *dictionarypro;
                listOfprod = [[NSMutableArray alloc] init];
                while(sqlite3_step(compiledStatement2) == SQLITE_ROW) {
                    
                    product_id = [NSString stringWithUTF8String:(char *)sqlite3_column_text(compiledStatement2, 0)];
                    product_title = [NSString stringWithUTF8String:(char *)sqlite3_column_text(compiledStatement2, 1)];
                    
                    product_title = [product_title stringByDecodingHTMLEntities];
                    product_title = [product_title stringByReplacingOccurrencesOfString:@"''" withString:@"'"];
                    
                    dictionarypro = [NSDictionary dictionaryWithObjectsAndKeys:product_id,@"product_id",product_title,@"product_title",nil];
                    [listOfprod addObject:dictionarypro];
                    //NSLog(@"listOfprod :: %@", listOfprod);
                }
                
            }
            sqlite3_finalize(compiledStatement2);
        }
        
        sqlbranch = [[NSString alloc] initWithFormat:@"SELECT * FROM contents WHERE category_id IN (%@) ", val_branch];
        
        NSString *branch_id, *branch_title, *telephone, *email, *maplat, *maplon;
        const char *sqlStatement3 = [sqlbranch UTF8String];
        sqlite3_stmt *compiledStatement3;
        if(sqlite3_prepare_v2(database, sqlStatement3, -1, &compiledStatement3, NULL) == SQLITE_OK) {
            
            NSDictionary *dictionarybran;
            listOfbranch = [[NSMutableArray alloc] init];
            while(sqlite3_step(compiledStatement3) == SQLITE_ROW) {
                
                branch_id = [NSString stringWithUTF8String:(char *)sqlite3_column_text(compiledStatement3, 0)];
                branch_title = [NSString stringWithUTF8String:(char *)sqlite3_column_text(compiledStatement3, 1)];
                telephone = [NSString stringWithUTF8String:(char *)sqlite3_column_text(compiledStatement3, 6)];
                email = [NSString stringWithUTF8String:(char *)sqlite3_column_text(compiledStatement3, 8)];
                maplat = [NSString stringWithUTF8String:(char *)sqlite3_column_text(compiledStatement3, 13)];
                maplon = [NSString stringWithUTF8String:(char *)sqlite3_column_text(compiledStatement3, 14)];
                
                branch_title = [branch_title stringByDecodingHTMLEntities];
                branch_title = [branch_title stringByReplacingOccurrencesOfString:@"''" withString:@"'"];
                
                telephone = [telephone stringByDecodingHTMLEntities];
                telephone = [telephone stringByReplacingOccurrencesOfString:@"''" withString:@"'"];
                
                email = [email stringByDecodingHTMLEntities];
                email = [email stringByReplacingOccurrencesOfString:@"''" withString:@"'"];
                
                dictionarybran = [NSDictionary dictionaryWithObjectsAndKeys:branch_id,@"branch_id",branch_title,@"branch_title",telephone,@"telephone",email,@"email",maplat,@"maplat",maplon,@"maplon",nil];
                [listOfbranch addObject:dictionarybran];
                //NSLog(@"listOfbranch :: %@", listOfbranch);
            }
            
        }
        sqlite3_finalize(compiledStatement3);
        
    }
    sqlite3_close(database);
    
    //NSLog(@"listOfContent: %@", listOfContent);
    [self displayContent];
    //}
    
}

- (void)separateDateTime:(id)sender
{
    [listAvaiDateTime removeAllObjects];
    [sender reloadData];
    
    //Find the time available for the particular date selected
    // NSPredicate *predicate = [NSPredicate predicateWithFormat:@"dateReserv CONTAINS %@", tf2.text];
    NSMutableArray *listDateTime = [self createMutableArray:listAvaiDateTimeOri];
    
    NSDictionary *dicDatetime;
    if ([listDateTime count]>0) {
        for (int k=0; k<[listDateTime count]; k++) {
            NSDictionary *data = [listDateTime objectAtIndex:k];
            NSString *dateReserv = [data objectForKey:@"dateReserv"];
            
            //Format it to time format
            NSDateFormatter *df = [[NSDateFormatter alloc] init];
            [df setDateFormat:@"HH:mm"];
            NSDate *d = [df dateFromString:dateReserv];
            [df setDateFormat:@"HH:mm"];
            NSString *DateString = [df stringFromDate:d];
            
            dicDatetime = [NSDictionary dictionaryWithObjectsAndKeys:DateString,@"dateReserv",nil];
            [self.listAvaiDateTime addObject:dicDatetime];
        }
    }
}

- (BOOL) isConnectionAvailable
{
    Reachability *networkReachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus networkStatus = [networkReachability currentReachabilityStatus];
    if (networkStatus == NotReachable) {
        //NSLog(@"There IS NO internet connection");
        return FALSE;
    } else {
        //NSLog(@"There IS internet connection");
        return TRUE;
    }
}

- (IBAction)onListClicked:(id)sender
{
    //[self.navigationController popToRootViewControllerAnimated:YES];
    [self getbookinglist];
}

- (void) getbookinglist {
    
    [view_bookingList setFrame:CGRectMake(0, 48, self.view.frame.size.width, self.view.frame.size.height-48)];
    [self.view addSubview:view_bookingList];
    [view_bookingAdd removeFromSuperview];
    [view_bookingDetail removeFromSuperview];
    
    isFromSubmit = NO;
    currentRequest = @"getBookingList";
    NSString *urlAddress = [[NSString alloc] initWithFormat:@"%@booking_details.php?db=%@&userid=%@&udid=%@&cuid=%@&device=ios&sc=%@&top_id=%@",appdelegate.user_apipath,appdelegate.db,appdelegate.user,appdelegate.uuid,appdelegate.customer_id,appdelegate.cs,appdelegate.plugin_id];
    [self connectToServer:urlAddress];
    
}

- (IBAction)onAddNewClicked:(id)sender
{
    [view_bookingAdd setFrame:CGRectMake(0, 48, self.view.frame.size.width, self.view.frame.size.height-48)];
    [self.view addSubview:view_bookingAdd];
    [view_bookingAdd addSubview:scrollviewAdd];
    [view_bookingDetail removeFromSuperview];
    [view_bookingList removeFromSuperview];
    
    for(UIView *subview in scrollviewAdd.subviews)
        [subview removeFromSuperview];
    
    if ([self isConnectionAvailable])
    {
        NSString *urlAddress = [[NSString alloc] initWithFormat:@"%@booking_setting.php?db=%@&userid=%@&udid=%@&cuid=%@&device=ios&sc=%@&top_id=%@", appdelegate.user_apipath, appdelegate.db, appdelegate.user, appdelegate.uuid, appdelegate.customer_id, appdelegate.cs, appdelegate.plugin_id];
        currentRequest = @"setting";
        //NSLog(@"urlAddress :: %@", urlAddress);
        [self connectToServer:urlAddress];
    }
    else
        [self getDataFromSqlite];
}

- (void)dealloc {
    arrayBookingList = nil;
    table_bookingList = nil;
    navBar = nil;
    label_reservationNo = nil;
    label_customerName = nil;
    label_reserveDate = nil;
    label_reserveStatus = nil;
    label_branchAddress = nil;
    label_branchStreet = nil;
    label_branchPostCodeAndBranchName = nil;
    label_branchState = nil;
    label_branchCountry = nil;
    label_reserveTitle = nil;
    label_reserveTq = nil;
    table_bookingMenu = nil;
    view_bookingDetail = nil;
    scrollView = nil;
    scrollviewAdd = nil;
    label_branchName = nil;
    [branchPopoverController dismissPopoverAnimated:NO];
    //[branchPopoverController release];
    branchPopoverController = nil;
    [prodPopoverController dismissPopoverAnimated:NO];
    //[prodPopoverController release];
    prodPopoverController = nil;
    [dselectPopoverController dismissPopoverAnimated:NO];
    //[dselectPopoverController release];
    dselectPopoverController = nil;
    //[listOfbranch release];
    listOfbranch = nil;
    //[listOfContent release];
    listOfContent = nil;
    //[listOfprod release];
    listOfprod = nil;
    listOfprodcat = nil;
    //[tf release];
    tf = nil;
    //[tf1 release];
    tf1 = nil;
    //[tf2 release];
    tf2 = nil;
    //[tf3 release];
    tf3 = nil;
    //[tf4 release];
    tf4 =nil;
    //[tf5 release];
    tf5 = nil;
    //[listAvaiDate release];
    listAvaiDate = nil;
    //[listAvaiDateTime release];
    listAvaiDateTime = nil;
    //[listAvaiDateTimeOri release];
    listAvaiDateTimeOri = nil;
    //[view_bookingAdd release];
    view_bookingAdd = nil;
    //[label_reserveNo release];
    //label_reserveNo = nil;
    //[label_customerName release];
    label_customerName = nil;
    //[label_reserveDate release];
    label_reserveDate = nil;
    //[label_reserveStatus release];
    label_reserveStatus = nil;
    //[view_popoverMain release];
    view_popoverMain = nil;
    //[super dealloc];
}
@end
