//
//  ChangePasswordViewController.h
//  standardmobileappsv3
//
//  Created by M3Online on 10/24/14.
//  Copyright (c) 2014 M3Online. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "LoadingView.h"

@interface ChangePasswordViewController : UIViewController <UITextFieldDelegate, UIScrollViewDelegate, UIViewControllerTransitioningDelegate> {
    CGFloat animatedDistance;
    NSString *contId;
    NSString *conttitle;
    
    //NSMutableData* receivedData;
    NSString *currentRequest;
    UIScrollView *scrollview;
    
    LoadingView *loadingView;
    
    NSString *alertmsgstatus;
}

@property (nonatomic, strong) NSString *contId;
@property (nonatomic, strong) NSString *conttitle;

//@property (nonatomic, strong) NSMutableData *receivedData;
@property (nonatomic, strong) NSString *currentRequest;
@property (nonatomic, strong) UIScrollView *scrollview;

@property (strong, nonatomic) IBOutlet UITextField *txt_password;
@property (strong, nonatomic) IBOutlet UITextField *txt_newpassword;
@property (strong, nonatomic) IBOutlet UITextField *txt_newpassword1;
@property (strong, nonatomic) IBOutlet UITextField *txt_loginid;

@property (nonatomic, strong) NSString *alertmsgstatus;

@property (strong, nonatomic) NSString *loginpluginname;


@end
