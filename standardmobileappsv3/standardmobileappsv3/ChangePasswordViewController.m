//
//  ChangePasswordViewController.m
//  standardmobileappsv3
//
//  Created by M3Online on 10/24/14.
//  Copyright (c) 2014 M3Online. All rights reserved.
//

#import "ChangePasswordViewController.h"
#import "AppDelegate.h"
#import "LoadingView.h"
#import "SBJSON.h"
#import "RootViewController.h"
#import "ViewController.h"
#import "SettingViewController.h"
#import "InitialSlidingViewController.h"
#import "BTSlideInteractor.h"

#define CELL_CONTENT_MARGIN 8.0f
#define CELL_CONTENT_WIDTH self.view.frame.size.width-CELL_CONTENT_MARGIN

static const CGFloat KEYBOARD_ANIMATION_DURATION = 0.3;
static const CGFloat MINIMUM_SCROLL_FRACTION = 0.2;
static const CGFloat MAXIMUM_SCROLL_FRACTION = 0.8;
static const CGFloat PORTRAIT_KEYBOARD_HEIGHT = 216;

@interface ChangePasswordViewController ()
@property (strong, nonatomic) BTSlideInteractor *interactor;
@end

@implementation ChangePasswordViewController

@synthesize contId, scrollview;
@synthesize /*receivedData, */currentRequest, conttitle;
@synthesize txt_newpassword, txt_newpassword1, txt_password, alertmsgstatus, loginpluginname, txt_loginid;

InitialSlidingViewController *modalController;

AppDelegate *appdelegate;
__strong LoadingView *loadingView;

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
    //////NSLog(@">>> Entering *** %s <<<", __PRETTY_FUNCTION__);
    [self hideKeyPad];
}

- (IBAction) callHideKeyBoard:(id)sender{
    //////NSLog(@"testing...");
    [self hideKeyPad];
}

-(void) hideKeyPad {
    //////NSLog(@"#### %s",__PRETTY_FUNCTION__);
    [txt_password resignFirstResponder];
    [txt_newpassword resignFirstResponder];
    [txt_newpassword1 resignFirstResponder];
    [txt_loginid resignFirstResponder];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    [self checkmbflogin];
    appdelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    
    if (![appdelegate.backgroundT isEqualToString:@""]) {
        
        NSString *filePath = appdelegate.backgroundT;
        
        NSRange end = [filePath rangeOfString:@"/" options:NSBackwardsSearch];
        NSString *shortString;
        if (end.location != NSNotFound) {
            shortString =[filePath substringWithRange:NSMakeRange(end.location+1, filePath.length-(end.location+1))];
        } else {
            shortString = filePath;
        }
        
        BOOL success;
        
        // Create a FileManager object, we will use this to check the status
        // of the database and to copy it over if required
        NSFileManager *fileManager = [NSFileManager defaultManager];
        
        // Check if the database has already been created in the users filesystem
        success = [fileManager fileExistsAtPath:[[[NSBundle mainBundle] resourcePath] stringByAppendingPathComponent:shortString]];
        
        UIImageView *imgV = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height)];
        //[self addSubview:imgV];
        
        //__block UIActivityIndicatorView *activityIndicator;
        __weak UIImageView *weakImageView = imgV;
        NSString *imagePath;
        if(success) {
            imagePath = [[[NSBundle mainBundle] resourcePath] stringByAppendingPathComponent:shortString];
            [weakImageView setImage:[UIImage imageNamed:shortString]];
            
            //NSLog(@"shortStringimagePath : %@", imagePath);
            imgV.contentMode  = UIViewContentModeScaleAspectFit;
            [imgV setClipsToBounds:YES];
            
            UIGraphicsBeginImageContext(self.view.frame.size);
            CGRect rect=CGRectMake(0,-64,self.view.bounds.size.width,self.view.bounds.size.height+64);
            [imgV.image drawInRect:rect];
            UIImage *image1 = UIGraphicsGetImageFromCurrentImageContext();
            UIGraphicsEndImageContext();
            self.view.backgroundColor = [UIColor colorWithPatternImage:image1];
        } else {
            imagePath = filePath;
            NSURL *imageURL = [NSURL URLWithString:imagePath];
            NSData *imageData = [NSData dataWithContentsOfURL:imageURL];
            //self.backgroundColor = [UIColor redColor];
            UIGraphicsBeginImageContext(self.view.frame.size);
            CGRect rect=CGRectMake(0,-64,self.view.bounds.size.width,self.view.bounds.size.height+64);
            [[UIImage imageWithData:imageData] drawInRect:rect];
            UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
            UIGraphicsEndImageContext();
            self.view.backgroundColor = [UIColor colorWithPatternImage:image];
        }
    } else {
        self.view.backgroundColor = [UIColor clearColor];
    }
    
    //self.title = conttitle;
    self.title = NSLocalizedString(@"change_password", nil);
    appdelegate.homeclicked = NO;
    
    self.navigationItem.backBarButtonItem = [[UIBarButtonItem alloc]initWithTitle:@"" style:UIBarButtonItemStylePlain target:nil action:nil];
    
    CGRect rect = CGRectMake(0,0,20,20);
    UIGraphicsBeginImageContext( rect.size );
    [[UIImage imageNamed:@"home.png"] drawInRect:rect];
    UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    UIBarButtonItem *myHomeButton = [[UIBarButtonItem alloc] initWithImage:image style:UIBarButtonItemStyleDone target:self action:@selector(myHomeAction:)];
    
    
    NSArray *myButtonArray = [[NSArray alloc] initWithObjects:myHomeButton, nil];
    
    self.navigationItem.rightBarButtonItems = myButtonArray;
    
    [self displaycontent];
}

-(void) displaycontent {
    
    scrollview = [[UIScrollView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height)];
    scrollview.backgroundColor = [UIColor clearColor];
    scrollview.bounces = NO;
    [self.view addSubview:scrollview];
    
    /*UILabel *title = [[UILabel alloc] initWithFrame: CGRectMake(10, 10, scrollview.frame.size.width-20, 60)];
    [title setFont:[UIFont boldSystemFontOfSize:24]];
    title.textColor = [UIColor blackColor];
    title.textAlignment = NSTextAlignmentLeft;
    title.backgroundColor = [UIColor clearColor];
    title.numberOfLines = 0;
    [title setText:NSLocalizedString(@"change_password", nil)];
    [scrollview addSubview:title]; */
    
    int height = 10;
    if ([loginpluginname isEqualToString:@"53"]) {
        UILabel *lblloginid = [[UILabel alloc] initWithFrame: CGRectMake(10, height, scrollview.frame.size.width-20, 30)];
        [lblloginid setFont:[UIFont systemFontOfSize:18]];
        lblloginid.textColor = [UIColor grayColor];
        lblloginid.textAlignment = NSTextAlignmentLeft;
        lblloginid.backgroundColor = [UIColor clearColor];
        lblloginid.numberOfLines = 0;
        [lblloginid setText:@"Card Number"];
        [scrollview addSubview:lblloginid];
        height = height + 40;
        
        txt_loginid = [[UITextField alloc] initWithFrame:CGRectMake(10, height, scrollview.frame.size.width-20, 40)];
        txt_loginid.textColor = [UIColor blackColor];
        [txt_loginid setBorderStyle:UITextBorderStyleRoundedRect];
        txt_loginid.keyboardType = UIKeyboardTypeNumberPad;
        [scrollview addSubview:txt_loginid];
        txt_loginid.delegate = self;
        height = height + 50;
    }
    
    UILabel *lblpassword = [[UILabel alloc] initWithFrame: CGRectMake(10, height, scrollview.frame.size.width-20, 30)];
    [lblpassword setFont:[UIFont systemFontOfSize:18]];
    lblpassword.textColor = [UIColor grayColor];
    lblpassword.textAlignment = NSTextAlignmentLeft;
    lblpassword.backgroundColor = [UIColor clearColor];
    lblpassword.numberOfLines = 0;
    [lblpassword setText:NSLocalizedString(@"current_password", nil)];
    [scrollview addSubview:lblpassword];
    height = height + 40;

    txt_password = [[UITextField alloc] initWithFrame:CGRectMake(10, height, scrollview.frame.size.width-20, 40)];
    txt_password.textColor = [UIColor blackColor];
    [txt_password setBorderStyle:UITextBorderStyleRoundedRect];
    txt_password.secureTextEntry = YES;
    [scrollview addSubview:txt_password];
    txt_password.delegate = self;
    height = height + 50;
    
    UILabel *lblnewpassword = [[UILabel alloc] initWithFrame: CGRectMake(10, height, scrollview.frame.size.width-20, 30)];
    [lblnewpassword setFont:[UIFont systemFontOfSize:18]];
    lblnewpassword.textColor = [UIColor grayColor];
    lblnewpassword.textAlignment = NSTextAlignmentLeft;
    lblnewpassword.backgroundColor = [UIColor clearColor];
    lblnewpassword.numberOfLines = 0;
    [lblnewpassword setText:NSLocalizedString(@"new_password", nil)];
    [scrollview addSubview:lblnewpassword];
    height = height + 40;
    
    txt_newpassword = [[UITextField alloc] initWithFrame:CGRectMake(10, height, scrollview.frame.size.width-20, 40)];
    txt_newpassword.textColor = [UIColor blackColor];
    [txt_newpassword setBorderStyle:UITextBorderStyleRoundedRect];
    txt_newpassword.secureTextEntry = YES;
    [scrollview addSubview:txt_newpassword];
    txt_newpassword.delegate = self;
    height = height + 50;
    
    UILabel *lblnewpassword1 = [[UILabel alloc] initWithFrame: CGRectMake(10, height, scrollview.frame.size.width-20, 30)];
    [lblnewpassword1 setFont:[UIFont systemFontOfSize:18]];
    lblnewpassword1.textColor = [UIColor grayColor];
    lblnewpassword1.textAlignment = NSTextAlignmentLeft;
    lblnewpassword1.backgroundColor = [UIColor clearColor];
    lblnewpassword1.numberOfLines = 0;
    [lblnewpassword1 setText:NSLocalizedString(@"re_new_password", nil)];
    [scrollview addSubview:lblnewpassword1];
    height = height + 40;
    
    txt_newpassword1 = [[UITextField alloc] initWithFrame:CGRectMake(10, height, scrollview.frame.size.width-20, 40)];
    txt_newpassword1.textColor = [UIColor blackColor];
    [txt_newpassword1 setBorderStyle:UITextBorderStyleRoundedRect];
    txt_newpassword1.secureTextEntry = YES;
    [scrollview addSubview:txt_newpassword1];
    txt_newpassword1.delegate = self;
    height = height + 60;
    
    UIButton *buttonsubmit = [UIButton buttonWithType:UIButtonTypeRoundedRect];
    buttonsubmit.tag = 1001;
    [buttonsubmit addTarget:self action:@selector(aMethod:) forControlEvents:UIControlEventTouchUpInside];
    [buttonsubmit setTitle:NSLocalizedString(@"save", nil) forState:UIControlStateNormal];
    [buttonsubmit setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    [buttonsubmit setBackgroundColor:[UIColor colorWithRed:230/255.0 green:230/255.0 blue:230/255.0 alpha:1]];
    buttonsubmit.frame = CGRectMake(10, height, 210, 44.0);
    [scrollview addSubview:buttonsubmit];
    
    scrollview.contentSize = CGSizeMake(scrollview.frame.size.width, height+60);
   // scrollview =nil;

}

- (void)aMethod:(id)sender
{
    UIButton *clicked = (UIButton *) sender;
    
    //Submit button
    if (clicked.tag==1001)
    {
        NSString *urlAddress;
        if ([loginpluginname isEqualToString:@"53"]) {
           urlAddress = [[NSString alloc] initWithFormat:@"%@mbf_cust_update.php?db=%@&userid=%@&udid=%@&cuid=%@&device=ios&sc=%@&login_id=%@&password=%@&newpassword=%@&newpassword1=%@", appdelegate.user_apipath, appdelegate.db, appdelegate.user, appdelegate.uuid, appdelegate.customer_id, appdelegate.cs, [txt_loginid text], [txt_password text], [txt_newpassword text], [txt_newpassword1 text]];
        } else {
            urlAddress = [[NSString alloc] initWithFormat:@"%@forgot.php?db=%@&userid=%@&udid=%@&cuid=%@&device=ios&sc=%@&task=changepassword&password=%@&newpassword=%@&newpassword1=%@", appdelegate.user_apipath, appdelegate.db, appdelegate.user, appdelegate.uuid, appdelegate.customer_id, appdelegate.cs, [txt_password text], [txt_newpassword text], [txt_newpassword1 text]];
        }
        currentRequest = @"changePassword";
        [self connectToServer:urlAddress];
        
    }
}

//Slide Menu
- (void)willAnimateRotationToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation duration:(NSTimeInterval)duration
{
    //NSLog(@"view frame=%@ bounds=%@", NSStringFromCGRect(self.view.frame), NSStringFromCGRect(self.view.bounds));
}


- (void)showModalButtonWasTouched
{
    self.interactor = [[BTSlideInteractor alloc] init];
    self.interactor.presenting = YES;
    
    //BTModalViewController *modalController = [self.storyboard instantiateViewControllerWithIdentifier:@"ModalViewController"];
    modalController = [[InitialSlidingViewController alloc] init];
    modalController.modalPresentationStyle = UIModalPresentationCustom;
    modalController.transitioningDelegate = self.interactor;
    [self.navigationController presentViewController:modalController animated:YES completion:nil];
    //[self.navigationController popToViewController:modalController animated:NO];
    //[self.navigationController pushViewController:modalController animated:YES];
}
// End Slide Menu

-(void)myHomeAction:(UIBarButtonItem *)sender{
    
    appdelegate.nextPage = @"";
    
    if ([appdelegate.listOfVerMenu count]>0) {
        if (appdelegate.homeclicked==NO) {
            appdelegate.homeclicked = YES;
            [self showModalButtonWasTouched];
        } else {
            appdelegate.homeclicked = NO;
            [modalController dismissViewControllerAnimated:YES completion:nil];
        }
    } else {
        //AppDelegate *appdelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
        //NSLog(@"View Controllers : %@",[appdelegate.navController viewControllers]);
        NSMutableArray *navigationArray = [[NSMutableArray alloc] initWithArray: appdelegate.navController.viewControllers];
        
        for (UIViewController* viewController in appdelegate.navController.viewControllers) {
            
            //This if condition checks whether the viewController's class is MyGroupViewController
            // if true that means its the MyGroupViewController (which has been pushed at some point)
            if ([viewController isKindOfClass:[RootViewController class]] ) {
                
                // Here viewController is a reference of UIViewController base class of MyGroupViewController
                // but viewController holds MyGroupViewController  object so we can type cast it here
                //ThemeViewController *themeViewController = (ThemeViewController*)viewController;
                //[self.navigationController popToViewController:themeViewController animated:YES];
                [navigationArray removeObjectAtIndex: 0];
            }
        }
        appdelegate.navController.viewControllers = navigationArray;
        for (UIViewController* viewController in appdelegate.navController.viewControllers) {
            
            //This if condition checks whether the viewController's class is MyGroupViewController
            // if true that means its the MyGroupViewController (which has been pushed at some point)
            if ([viewController isKindOfClass:[ViewController class]] ) {
                
                // Here viewController is a reference of UIViewController base class of MyGroupViewController
                // but viewController holds MyGroupViewController  object so we can type cast it here
                ViewController *themeViewController = (ViewController*)viewController;
                [appdelegate.navController popToViewController:themeViewController animated:NO];
                //[self.navigationController popToViewController:viewController animated:YES];
            }
        }
        //NSLog(@"View Controllers111 : %@",appdelegate.navController.viewControllers);
    }
    
}

#pragma mark CONNECT TO SERVER
-(void) connectToServer:(NSString *) urlPath {
    [self addLoadingView];
    NSString *urlAddress =[NSString stringWithFormat:@"%@", urlPath];
    
    /*NSURL *url = [NSURL URLWithString:urlAddress];
    
    NSURLRequest *requestObj = [NSURLRequest requestWithURL:url
                                                cachePolicy:NSURLRequestReloadIgnoringLocalAndRemoteCacheData
                                            timeoutInterval:30];
    
    NSURLConnection *theConnection=[[NSURLConnection alloc] initWithRequest:requestObj delegate:self];
    if (theConnection) {
        // Create the NSMutableData to hold the received data.
        // receivedData is an instance variable declared elsewhere.
        receivedData = [NSMutableData data];
    } else {
        // Inform the user that the connection failed.
        //[self displayAlert:@"Connection Error. Please try again later."];
        [self displayAlert:@"Connection error. Please try again later."];
        [self removeLoadingView];
    } */
    [self getJsonResponse:urlAddress success:^(NSDictionary *responseDict) {
        
        //NSLog(@"%@",responseDict);
        [self successfunction:responseDict];
        
    } failure:^(NSError *error) {
        // release the connection, and the data object
        
        [self removeLoadingView];
        [self displayAlert:@"No internet connection available. Standard Mobile Apps requires internet connection to function properly."];
    }];
}

-(void)getJsonResponse : (NSString *)urlStr success : (void (^)(NSDictionary *responseDict))success failure:(void(^)(NSError* error))failure
{
    
    
    NSURLSession * session = [NSURLSession sharedSession];
    NSURL * url = [NSURL URLWithString: urlStr];
    
    
    // Asynchronously Api is hit here
    NSURLSessionDataTask * dataTask = [session dataTaskWithURL:url completionHandler:^(NSData *data, NSURLResponse *response, NSError *error)
                                       {
                                           //NSLog(@"%@",data);
                                           NSDictionary * json;
                                           if ([data length]>0) {
                                               json  = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
                                               //NSLog(@"%@",json);
                                               success(json);
                                           } else {
                                               json = nil;
                                               dispatch_async(dispatch_get_main_queue(), ^ {
                                                   [self removeLoadingView];
                                                   [self displayAlert:NSLocalizedString(@"MSG_CONNECTION_ERROR", nil)];
                                               });
                                           }
                                           
                                           
                                       }];
    
    [dataTask resume] ; // Executed First
    
    
}

-(void) successfunction:(NSDictionary *)responseDict {
    dispatch_async(dispatch_get_main_queue(), ^{
        //NSString *jsonString = [responseDict JSONRepresentation];
        [self removeLoadingView];
        
        if ([currentRequest isEqualToString:@"changePassword"])
        {
            NSDictionary *dictionary = responseDict;
            
            for(NSDictionary *row in dictionary)
            {
                alertmsgstatus = [row objectForKey:@"status"];
                [self displayAlert:[row objectForKey:@"desc"]];
            }
        }
    });
    
}

/*- (void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response
{
    [receivedData setLength:0];
}

- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data
{
    [receivedData appendData:data];
}

- (void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error {
    // release the connection, and the data object
    
    connection =nil;
    receivedData =nil;
    
    [self removeLoadingView];
    [self displayAlert:@"No internet connection available. Standard Mobile Apps requires internet connection to function properly."];
}

- (void)connectionDidFinishLoading:(NSURLConnection *)connection
{
    //NSLog(@"Succeed! Received %d bytes of data",[receivedData length]);
    [self removeLoadingView];
    
    NSString *response = [[NSString alloc] initWithData:receivedData encoding:NSUTF8StringEncoding];
    //NSLog(@"response :: %@", response);
    if ([currentRequest isEqualToString:@"changePassword"])
    {
        NSDictionary *dictionary = [response JSONValue];
        
        for(NSDictionary *row in dictionary)
        {
            alertmsgstatus = [row objectForKey:@"status"];
            [self displayAlert:[row objectForKey:@"desc"]];
        }
    }
    
    connection =nil;
    response =nil;
    receivedData =nil;
} */

#pragma mark LoadingView
-(void) addLoadingView
{
    loadingView = [LoadingView loadingViewInView:self.view];
}

-(void) removeLoadingView
{
    [loadingView removeView];
}

#pragma mark displayAlert
-(void) displayAlert: (NSString *) stralert {
    NSString *str = [[NSString alloc] initWithFormat:@"%@", stralert];
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:appdelegate.iconname
                                                    message:str
                                                   delegate:self
                                          cancelButtonTitle:NSLocalizedString(@"done", nil)
                                          otherButtonTitles:nil];
    
    [alert show];
    str =nil;
    alert =nil;
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    //NSLog(@"buttonIndex%d", buttonIndex);
    if (buttonIndex == 0) {
        //cancel clicked ...do your action
        if ([alertmsgstatus isEqualToString:@"1"]) {
            NSString *nibFileName;
            if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone) {
                nibFileName = @"SettingViewController";
            } else {
                nibFileName = @"SettingViewController_ipad";
            }
            SettingViewController *settingViewController = [[SettingViewController alloc] initWithNibName:nibFileName bundle:nil];
            settingViewController.cont_title = conttitle;
            [appdelegate.navController pushViewController:settingViewController animated:NO];
        }
    }else{
        //reset clicked
    }
    alertmsgstatus = @"";
}

-(void)textFieldDidBeginEditing:(UITextField *)textField
{
    CGRect textFieldRect =
    [self.view.window convertRect:textField.bounds fromView:textField];
    CGRect viewRect =
    [self.view.window convertRect:self.view.bounds fromView:self.view];
    
    CGFloat midline = textFieldRect.origin.y + 0.5 * textFieldRect.size.height;
    CGFloat numerator =
    midline - viewRect.origin.y
    - MINIMUM_SCROLL_FRACTION * viewRect.size.height;
    CGFloat denominator =
    (MAXIMUM_SCROLL_FRACTION - MINIMUM_SCROLL_FRACTION)
    * viewRect.size.height;
    CGFloat heightFraction = numerator / denominator;
    if (heightFraction < 0.0)
    {
        heightFraction = 0.0;
    }
    else if (heightFraction > 1.0)
    {
        heightFraction = 1.0;
    }
    
    
    animatedDistance = floor(PORTRAIT_KEYBOARD_HEIGHT * heightFraction);
    
    CGRect viewFrame = self.view.frame;
    viewFrame.origin.y -= animatedDistance;
    
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationBeginsFromCurrentState:YES];
    [UIView setAnimationDuration:KEYBOARD_ANIMATION_DURATION];
    
    [self.view setFrame:viewFrame];
    
    [UIView commitAnimations];
}

- (void)textFieldDidEndEditing:(UITextField *)textField

{
    CGRect viewFrame = self.view.frame;
    viewFrame.origin.y += animatedDistance;
    
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationBeginsFromCurrentState:YES];
    [UIView setAnimationDuration:KEYBOARD_ANIMATION_DURATION];
    
    [self.view setFrame:viewFrame];
    
    [UIView commitAnimations];
    
}

-(void) viewDidDisappear:(BOOL)animated {
    
    [super viewDidDisappear:animated];
    
    appdelegate.homeclicked = NO;
    [modalController dismissViewControllerAnimated:YES completion:nil];
    
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    [textField resignFirstResponder];
    return YES;
}

- (void) checkmbflogin {
    sqlite3 *database;
    
    if(sqlite3_open([appdelegate.databasePath UTF8String], &database) == SQLITE_OK) {
        NSString *theme_object = [[NSString alloc] initWithFormat:@"SELECT plugin_id FROM theme_object_plugin WHERE plugin_type='customer' AND user_id='%@' AND plugin_id='53' ", appdelegate.user];
        //NSLog(@"theme_object_plugin:: %@", theme_object);
        loginpluginname = @"";
        //int rows=0;
        const char *sqlStatement = [theme_object UTF8String];
        sqlite3_stmt *compiledStatement;
        if(sqlite3_prepare_v2(database, sqlStatement, -1, &compiledStatement, NULL) == SQLITE_OK) {
            // Loop through the results and add them to the feeds array
            
            /*if (sqlite3_step(compiledStatement) == SQLITE_ERROR) {
             NSAssert1(0,@"Error when counting rows  %s",sqlite3_errmsg(database));
             } else {
             rows = sqlite3_column_int(compiledStatement, 0);
             //NSLog(@"SQLite Rows: %i", rows);
             } */
            
            //while(sqlite3_step(compiledStatement) == SQLITE_ROW) {
            
            //NSLog(@"testtest111 ::");
            while(sqlite3_step(compiledStatement) == SQLITE_ROW) {
                loginpluginname = [NSString stringWithUTF8String:(char *)sqlite3_column_text(compiledStatement, 0)];
                //NSLog(@"loginpluginname :: %@", loginpluginname);
            }
            
        }
        // Release the compiled statement from memory
        sqlite3_finalize(compiledStatement);
        
    }
    sqlite3_close(database);
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)dealloc {
    currentRequest =nil;
    //receivedData =nil;
    alertmsgstatus = nil;
    scrollview =nil;
    //[super dealloc];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
