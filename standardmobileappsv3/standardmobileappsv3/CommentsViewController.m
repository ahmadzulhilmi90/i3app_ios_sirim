//
//  InboxViewController.m
//  standardmobileappsv3
//
//  Created by M3Online on 9/4/14.
//  Copyright (c) 2014 M3Online. All rights reserved.
//

#import "CommentsViewController.h"
#import "AppDelegate.h"
#import "LoadingView.h"
#import "SBJSON.h"
#import "RootViewController.h"
#import "ViewController.h"
#import "ThemeViewController.h"
#import "BasicPluginViewController.h"
#import "ContentPluginViewController.h"
#import "ContentPluginDetailsViewController.h"
#import "LoyaltyViewController.h"
#import "WarrantyViewController.h"
#import "ContentSearchViewController.h"
#import "SettingViewController.h"
#import "BookingListViewController.h"
#import "ScorePointViewController.h"
#import "ContentCatPluginViewController.h"
#import "InboxViewController.h"
#import "ContentProductCatPluginViewController.h"
#import "InitialSlidingViewController.h"
#import "BTSlideInteractor.h"

#define CELL_CONTENT_MARGIN 8.0f
#define CELL_CONTENT_WIDTH self.view.frame.size.width-CELL_CONTENT_MARGIN

static const CGFloat KEYBOARD_ANIMATION_DURATION = 0.3;
static const CGFloat MINIMUM_SCROLL_FRACTION = 0.2;
static const CGFloat MAXIMUM_SCROLL_FRACTION = 0.8;
static const CGFloat PORTRAIT_KEYBOARD_HEIGHT = 216;

@interface CommentsViewController ()
@property (strong, nonatomic) BTSlideInteractor *interactor;
@end

@implementation CommentsViewController

@synthesize contId, scrollview;
@synthesize /*receivedData, */currentRequest, listOfContent, conttitle, topId;
@synthesize imageViewPhoto,choosePhotoBtn, takePhotoBtn, buttonsubmit, heightPhotoView, height;

UIAlertView *alert;
__strong UITextView *newTextView;

InitialSlidingViewController *modalController;

AppDelegate *appdelegate;
__strong LoadingView *loadingView;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (UIColor *) colorWithHexString:(NSString *)hexString {
    NSString *colorString = [[hexString stringByReplacingOccurrencesOfString: @"#" withString: @""] uppercaseString];
    CGFloat alpha, red, blue, green;
    alpha = 1.0f;
    red   = [self colorComponentFrom: colorString start: 0 length: 2];
    green = [self colorComponentFrom: colorString start: 2 length: 2];
    blue  = [self colorComponentFrom: colorString start: 4 length: 2];
    return [UIColor colorWithRed: red green: green blue: blue alpha: alpha];
}

- (CGFloat) colorComponentFrom:(NSString *)string start:(NSUInteger)start length:(NSUInteger)length {
    NSString *substring = [string substringWithRange: NSMakeRange(start, length)];
    NSString *fullHex = length == 2 ? substring : [NSString stringWithFormat: @"%@%@", substring, substring];
    unsigned hexComponent;
    [[NSScanner scannerWithString: fullHex] scanHexInt: &hexComponent];
    return hexComponent / 255.0;
}

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
    //////NSLog(@">>> Entering *** %s <<<", __PRETTY_FUNCTION__);
    [self hideKeyPad];
}

- (IBAction) callHideKeyBoard:(id)sender{
	//////NSLog(@"testing...");
	[self hideKeyPad];
}

-(void) hideKeyPad {
    //////NSLog(@"#### %s",__PRETTY_FUNCTION__);
	[newTextView resignFirstResponder];
}

-(IBAction) getPhoto:(id) sender {
    UIImagePickerController * picker = [[UIImagePickerController alloc] init];
    picker.delegate = self;
    
    if((UIButton *) sender == choosePhotoBtn) {
        picker.sourceType = UIImagePickerControllerSourceTypeSavedPhotosAlbum;
    } else {
        picker.sourceType = UIImagePickerControllerSourceTypeCamera;
    }
    
    [self presentViewController:picker animated:NO completion:nil];
    
    if (imageViewPhoto != nil) {
    [imageViewPhoto setBackgroundColor:[UIColor groupTableViewBackgroundColor]];
    heightPhotoView = height - 44 + 110;
    float buttonWidth = 200.0f;
    buttonsubmit.frame = CGRectMake(CGRectGetMidX(self.view.frame)-(buttonWidth/2), heightPhotoView, buttonWidth, 44.0);
    [scrollview addSubview:buttonsubmit];
    heightPhotoView = heightPhotoView + 50;
    scrollview.contentSize = CGSizeMake(scrollview.frame.size.width, heightPhotoView);
    }
}

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info {
    [picker dismissViewControllerAnimated:NO completion:nil];
    imageViewPhoto.image = [info objectForKey:@"UIImagePickerControllerOriginalImage"];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    self.view.backgroundColor = [UIColor whiteColor];
    appdelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    self.title = conttitle;
    appdelegate.homeclicked = NO;
    
    [self getInboxList];
    
    self.navigationItem.backBarButtonItem = [[UIBarButtonItem alloc]initWithTitle:@"" style:UIBarButtonItemStylePlain target:nil action:nil];
    
    CGRect rect = CGRectMake(0,0,20,20);
    UIGraphicsBeginImageContext( rect.size );
    [[UIImage imageNamed:@"home.png"] drawInRect:rect];
    UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    UIBarButtonItem *myHomeButton = [[UIBarButtonItem alloc] initWithImage:image style:UIBarButtonItemStyleDone target:self action:@selector(myHomeAction:)];
    
    
    NSArray *myButtonArray = [[NSArray alloc] initWithObjects:myHomeButton, nil];
    
    self.navigationItem.rightBarButtonItems = myButtonArray;
    
}

- (void) getInboxList {
    NSString *urlAddress = [[NSString alloc] initWithFormat:@"%@comments_list.php?db=%@&userid=%@&udid=%@&cuid=%@&top=%@&cid=%@",appdelegate.user_apipath,appdelegate.db,appdelegate.user,appdelegate.uuid,appdelegate.customer_id,topId,contId];
    currentRequest = @"InboxList";
    NSLog(@"urlAddressInboxList :: %@", urlAddress);
    [self connectToServer:urlAddress];
}

-(void) displaycontent {
    
    scrollview = nil;
    scrollview = [[UIScrollView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height)];
    scrollview.backgroundColor = [UIColor whiteColor];
    scrollview.bounces = NO;
    [self.view addSubview:scrollview];
    
    height=10;
    int heightInside=5;
    NSString *top_id, *m_date, *comments_id, *message, *content_id, *custname, *image_url;
    if([listOfContent count]>0)
    {
        for (int i=0;i<[listOfContent count]; i++)
        {
            NSDictionary *dictionary = [listOfContent objectAtIndex:i];
            
            top_id = [dictionary objectForKey:@"top_id"];
            content_id = [dictionary objectForKey:@"content_id"];
            m_date = [dictionary objectForKey:@"m_date"];
            comments_id = [dictionary objectForKey:@"comments_id"];
            message = [dictionary objectForKey:@"message"];
            custname = [dictionary objectForKey:@"name"];
            image_url = [dictionary objectForKey:@"image_url"];
            
            UIView *listView=[[UIView alloc]initWithFrame:CGRectMake(10,height,self.view.frame.size.width-20,60)];
            
            [scrollview addSubview:listView];
            
            if ([custname length] > 0) {
                
                UILabel *lblsender = [[UILabel alloc] init];
                lblsender.font = [UIFont boldSystemFontOfSize:18.0f];
                lblsender.lineBreakMode = NSLineBreakByWordWrapping;
                lblsender.numberOfLines = 0;
                lblsender.text = custname;
                lblsender.backgroundColor = [UIColor clearColor];
                //if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone) {
                // For iPhone
                CGSize constraint = CGSizeMake(CELL_CONTENT_WIDTH - (CELL_CONTENT_MARGIN * 2), 20000.0f);
                
                CGSize size = [lblsender.text boundingRectWithSize:constraint options: NSStringDrawingUsesLineFragmentOrigin attributes: @{ NSFontAttributeName: [UIFont boldSystemFontOfSize:18.0f] } context: nil].size;
                
                lblsender.frame = CGRectMake(CELL_CONTENT_MARGIN/2, heightInside, listView.frame.size.width-CELL_CONTENT_MARGIN, MAX(size.height, 25.0f));
                //} else {
                //myLabel.frame = CGRectMake(20, 0, self.view.frame.size.width-20, 50);
                //}
                
                lblsender.textColor = [UIColor blackColor];
                
                heightInside = heightInside + size.height+5;
                [listView addSubview:lblsender];
                lblsender =nil;
            }
            
            if ([image_url length]>0)
            {
                //[myButton setBackgroundImageWithURL:[NSURL URLWithString:content_img] forState:UIControlStateNormal placeholderImage:[UIImage imageNamed:@"No-Image-icon.png"]];
                UIImageView *imgV = [[UIImageView alloc] initWithFrame:CGRectMake(10, heightInside, listView.frame.size.width-20, listView.frame.size.width)];
                
                heightInside = heightInside +listView.frame.size.width;
                [listView addSubview:imgV];
                
                __block UIActivityIndicatorView *activityIndicator;
                __weak UIImageView *weakImageView = imgV;
                /*[weakImageView setImageWithURL:[NSURL URLWithString:image_url] placeholderImage:[UIImage imageNamed:@"No-Image-icon.png"] options:SDWebImageProgressiveDownload progress:^(NSInteger receivedSize, NSInteger expectedSize)
                 {
                     if (!activityIndicator)
                     {
                         [weakImageView addSubview:activityIndicator = [UIActivityIndicatorView.alloc initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray]];
                         activityIndicator.center = weakImageView.center;
                         [activityIndicator startAnimating];
                     }
                 }
                                     completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType)
                 {
                     [activityIndicator removeFromSuperview];
                     activityIndicator = nil;
                 }]; */
                dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0), ^ {
                    [weakImageView sd_setImageWithURL:[NSURL URLWithString:image_url]
                                     placeholderImage:[UIImage imageNamed:@"No-Image-icon.png"]
                                              options:SDWebImageProgressiveDownload
                                             progress:^(NSInteger receivedSize, NSInteger expectedSize, NSURL *targetURL) {
                                                 dispatch_async(dispatch_get_main_queue(), ^ {
                                                     if (!activityIndicator) {
                                                         
                                                         [weakImageView addSubview:activityIndicator = [UIActivityIndicatorView.alloc initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray]];
                                                         activityIndicator.center = weakImageView.center;
                                                         [activityIndicator startAnimating];
                                                     }
                                                 });
                                             }
                                            completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
                                                dispatch_async(dispatch_get_main_queue(), ^ {
                                                    //[MBProgressHUD hideHUDForView:self.view animated:YES];
                                                    [activityIndicator removeFromSuperview];
                                                    activityIndicator = nil;
                                                });
                                            }];
                });
                
                imgV.contentMode  = UIViewContentModeScaleAspectFill;
                [imgV setClipsToBounds:YES];
                imgV = nil;
            }
            
            if ([message length] > 0) {
                
                UILabel *lblmessage = [[UILabel alloc] init];
                lblmessage.font = [UIFont systemFontOfSize:16.0f];
                lblmessage.lineBreakMode = NSLineBreakByWordWrapping;
                lblmessage.numberOfLines = 0;
                lblmessage.text = message;
                lblmessage.backgroundColor = [UIColor clearColor];
                //if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone) {
                // For iPhone
                CGSize constraint = CGSizeMake(CELL_CONTENT_WIDTH - (CELL_CONTENT_MARGIN * 2), 20000.0f);
                
                CGSize size = [lblmessage.text boundingRectWithSize:constraint options: NSStringDrawingUsesLineFragmentOrigin attributes: @{ NSFontAttributeName: [UIFont systemFontOfSize:16.0f] } context: nil].size;
                
                lblmessage.frame = CGRectMake(CELL_CONTENT_MARGIN/2, heightInside, listView.frame.size.width-CELL_CONTENT_MARGIN, MAX(size.height, 25.0f));
                //} else {
                //myLabel.frame = CGRectMake(20, 0, self.view.frame.size.width-20, 50);
                //}
                
                lblmessage.textColor = [UIColor blackColor];
                
                heightInside = heightInside + size.height+5;
                [listView addSubview:lblmessage];
                lblmessage =nil;
            }
            
            UILabel *lblline = [[UILabel alloc] init];
            //lblmessage.font = [UIFont systemFontOfSize:16.0f];
            //lblmessage.lineBreakMode = NSLineBreakByWordWrapping;
            lblline.numberOfLines = 0;
            //lblmessage.text = message;
            lblline.backgroundColor = [UIColor blackColor];
            //if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone) {
            // For iPhone
            //CGSize constraint = CGSizeMake(CELL_CONTENT_WIDTH - (CELL_CONTENT_MARGIN * 2), 20000.0f);
            
            //CGSize size = [lblmessage.text boundingRectWithSize:constraint options: NSStringDrawingUsesLineFragmentOrigin attributes: @{ NSFontAttributeName: [UIFont systemFontOfSize:16.0f] } context: nil].size;
            
            lblline.frame = CGRectMake(CELL_CONTENT_MARGIN/2, heightInside, listView.frame.size.width-CELL_CONTENT_MARGIN, 1.0f);
            //} else {
            //myLabel.frame = CGRectMake(20, 0, self.view.frame.size.width-20, 50);
            //}
            
            lblline.textColor = [UIColor blackColor];
            
            heightInside = heightInside + 1.0f + 5;
            [listView addSubview:lblline];
            lblline =nil;
            
            //if ([m_date length] > 0) {
                
                UILabel *lblmdate = [[UILabel alloc] init];
                lblmdate.font = [UIFont systemFontOfSize:16.0f];
                lblmdate.lineBreakMode = NSLineBreakByWordWrapping;
                lblmdate.numberOfLines = 0;
                lblmdate.text = m_date;
                lblmdate.backgroundColor = [UIColor clearColor];
                //if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone) {
                // For iPhone
                CGSize constraint = CGSizeMake(CELL_CONTENT_WIDTH - (CELL_CONTENT_MARGIN * 2), 20000.0f);
                
                CGSize size = [lblmdate.text boundingRectWithSize:constraint options: NSStringDrawingUsesLineFragmentOrigin attributes: @{ NSFontAttributeName: [UIFont systemFontOfSize:16.0f] } context: nil].size;
                
                lblmdate.frame = CGRectMake(CELL_CONTENT_MARGIN/2, heightInside, listView.frame.size.width-CELL_CONTENT_MARGIN, MAX(size.height, 25.0f));
                //} else {
                //myLabel.frame = CGRectMake(20, 0, self.view.frame.size.width-20, 50);
                //}
                
                lblmdate.textColor = [UIColor blackColor];
                [listView addSubview:lblmdate];
                lblmdate =nil;
                
                UIButton *buttondelete = [UIButton buttonWithType:UIButtonTypeRoundedRect];
                buttondelete.tag = [comments_id intValue];
                [buttondelete addTarget:self action:@selector(aMethodDelete:) forControlEvents:UIControlEventTouchUpInside];
                [buttondelete setTitle:NSLocalizedString(@"delete", nil) forState:UIControlStateNormal];
                [buttondelete setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
                [buttondelete setBackgroundColor:[UIColor clearColor]];
                
                float buttonWidth = 70.0f;
                buttondelete.frame = CGRectMake(listView.frame.size.width-80, heightInside, buttonWidth, 25.0);
                //height = height + 50;
                heightInside = heightInside + size.height+5;
                [listView addSubview:buttondelete];
            //}
            
            [listView setFrame:CGRectMake(10,height,self.view.frame.size.width-20,heightInside)];
            //if ([sender_id isEqualToString:@"0"]) {
                //listView.backgroundColor = [UIColor whiteColor];
            //} else {
                listView.backgroundColor = [UIColor colorWithRed:230/255.0 green:230/255.0 blue:230/255.0 alpha:1];
            //}
            listView.layer.cornerRadius = 6;
            listView.layer.borderWidth = 1.0;
            listView.layer.borderColor = [UIColor grayColor].CGColor;
            height = height + heightInside +10;
            heightInside=5;
            listView=nil;
            
        }
    }
    //NSLog(@"listOfContentMaggie :: %@", listOfContent);
    UILabel *lblnote = [[UILabel alloc] initWithFrame:CGRectMake(10, height, self.view.frame.size.width-20, 20)];
    lblnote.numberOfLines = 0;
    lblnote.lineBreakMode = NSLineBreakByWordWrapping;
    UIFont *Font6 = [UIFont fontWithName:@"Helvetica" size:16];
    lblnote.font = Font6;
    lblnote.text = [NSString stringWithFormat:@"%@:", NSLocalizedString(@"message", nil)];;
    lblnote.backgroundColor = [UIColor clearColor];
    //lbladdr.textColor = [UIColor blackColor];
    [lblnote sizeToFit];
    //lbladdr.shadowColor = [UIColor blackColor];
    //lbladdr.shadowOffset = CGSizeMake(0, 1);
    [scrollview addSubview:lblnote];
    lblnote = nil;
    height = height + 20;
    //NSLog(@"labelheight: %d", labelheight);
    
    newTextView = [[UITextView alloc] initWithFrame:CGRectMake(10, height, self.view.frame.size.width-20, 100)];
    newTextView.delegate = self ;
    [newTextView setFont:[UIFont systemFontOfSize:16]];
    //newTextView.text = contDetails;
    //To make the border look very close to a UITextField
    [newTextView.layer setBorderColor:[[[UIColor grayColor] colorWithAlphaComponent:0.5] CGColor]];
    [newTextView.layer setBorderWidth:1.0];
    
    //The rounded corner part, where you specify your view's corner radius:
    newTextView.layer.cornerRadius = 5;
    newTextView.clipsToBounds = YES;
    newTextView.userInteractionEnabled = YES;
    [scrollview addSubview:newTextView];
    height = height + 110;
    
    choosePhotoBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    [choosePhotoBtn addTarget:self
               action:@selector(getPhoto:)
     forControlEvents:UIControlEventTouchUpInside];
    [choosePhotoBtn setTitle:NSLocalizedString(@"choosePhoto", nil) forState:UIControlStateNormal];
    [choosePhotoBtn setTitleColor:[UIColor blueColor] forState:UIControlStateNormal];
    choosePhotoBtn.frame = CGRectMake(CGRectGetMidX(self.view.frame)-140, height, 130, 40);
    [choosePhotoBtn.layer setBorderColor:[[[UIColor grayColor] colorWithAlphaComponent:0.5] CGColor]];
    [choosePhotoBtn.layer setBorderWidth:1.0];
    choosePhotoBtn.layer.cornerRadius = 20;
    [scrollview addSubview:choosePhotoBtn];
    
    takePhotoBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    [takePhotoBtn addTarget:self
               action:@selector(getPhoto:)
     forControlEvents:UIControlEventTouchUpInside];
    [takePhotoBtn setTitle:NSLocalizedString(@"takePhoto", nil) forState:UIControlStateNormal];
    [takePhotoBtn setTitleColor:[UIColor blueColor] forState:UIControlStateNormal];
    takePhotoBtn.frame = CGRectMake(CGRectGetMidX(self.view.frame)+10, height, 130, 40);
    [takePhotoBtn.layer setBorderColor:[[[UIColor grayColor] colorWithAlphaComponent:0.5] CGColor]];
    [takePhotoBtn.layer setBorderWidth:1.0];
    takePhotoBtn.layer.cornerRadius = 20;
    [scrollview addSubview:takePhotoBtn];
    height = height + 50;
    
    imageViewPhoto = [[UIImageView alloc] initWithFrame:CGRectMake(CGRectGetMidX(self.view.frame)-75, height, 150, 100)];
    //[imageViewPhoto setBackgroundColor:[UIColor groupTableViewBackgroundColor]];
    [scrollview addSubview:imageViewPhoto];
    
    buttonsubmit = [UIButton buttonWithType:UIButtonTypeRoundedRect];
    buttonsubmit.tag = 1001;
    [buttonsubmit addTarget:self action:@selector(aMethod:) forControlEvents:UIControlEventTouchUpInside];
    [buttonsubmit setTitle:NSLocalizedString(@"send", nil) forState:UIControlStateNormal];
    [buttonsubmit setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    [buttonsubmit setBackgroundColor:[UIColor colorWithRed:230/255.0 green:230/255.0 blue:230/255.0 alpha:1]];
    
    float buttonWidth = 200.0f;
    buttonsubmit.frame = CGRectMake(CGRectGetMidX(self.view.frame)-(buttonWidth/2), height, buttonWidth, 44.0);
    height = height + 50;
    [scrollview addSubview:buttonsubmit];
    
    scrollview.contentSize = CGSizeMake(scrollview.frame.size.width, height);
}

- (void)aMethod:(id)sender
{
    UIButton *clicked = (UIButton *) sender;
    
    //Submit button
    if (clicked.tag==1001)
    {
        
        NSData   *imageFileData;
        NSString *imageFileName;
        // check if there is an image to upload
        if (imageViewPhoto.image != nil) {
            // yes, let's convert the image
            // UIImageJPEGRepresentation accepts a UIImage and compression parameter
            // use UIImagePNGRepresentation(self.userImage) for .PNG types
            imageFileData = UIImageJPEGRepresentation(imageViewPhoto.image , 0.33f);
            imageFileName = @"comments";
            // post to server
            [self uploadToServerUsingImage:imageFileData andFileName:imageFileName];
        } else {
            //NSLog(@"processImageThenPostToServer:self.userImage IS nil.");
            NSString *urlAddress = [[NSString alloc] initWithFormat:@"%@comments_add.php?db=%@&userid=%@&udid=%@&cuid=%@&message=%@&top=%@&cid=%@",appdelegate.user_apipath,appdelegate.db,appdelegate.user,appdelegate.uuid,appdelegate.customer_id, [self urlEncode:[newTextView text]], topId, contId];
            currentRequest = @"submit";
            [self connectToServer:urlAddress];
        }
        
    }
    
    //NSLog([NSString stringWithFormat:@"Image Return String: %@", returnString]);
}

// HTTP method to upload file to web server
- (void)uploadToServerUsingImage:(NSData *)imageData andFileName:(NSString *)filename {
    // set this to your server's address
    NSString *urlString = [NSString stringWithFormat:@"%@comments_add.php?db=%@&userid=%@&udid=%@&cuid=%@&message=%@&top=%@&cid=%@",appdelegate.user_apipath,appdelegate.db,appdelegate.user,appdelegate.uuid,appdelegate.customer_id, [self urlEncode:[newTextView text]], topId, contId];
    // set the content type, in this case it needs to be: "Content-Type: image/jpg"
    // Extract 'jpg' or 'png' from the last three characters of 'filename'
    if (([filename length] -3 ) > 0) {
        //NSString *contentType = [NSString stringWithFormat:@"Content-Type: image/%@", [filename substringFromIndex:[filename length] - 3]];
        [NSString stringWithFormat:@"Content-Type: image/%@", [filename substringFromIndex:[filename length] - 3]];
    }
    
    // allocate and initialize the mutable URLRequest, set URL and method.
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
    [request setURL:[NSURL URLWithString:urlString]];
    [request setHTTPMethod:@"POST"];
    // define the boundary and newline values
    NSString *boundary = @"uwhQ9Ho7y873Ha";
    NSString *kNewLine = @"\r\n";
    
    // Set the URLRequest value property for the HTTP Header
    // Set Content-Type as a multi-part form with boundary identifier
    NSString *contentType = [NSString stringWithFormat:@"multipart/form-data; boundary=%@",boundary];
    [request setValue:contentType forHTTPHeaderField: @"Content-Type"];
    
    // prepare a mutable data object used to build message body
    NSMutableData *body = [NSMutableData data];
    
    // set the first boundary
    [body appendData:[[NSString stringWithFormat:@"--%@%@", boundary, kNewLine] dataUsingEncoding:NSUTF8StringEncoding]];
    
    // Set the form type and format
    [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"%@\"; filename=\"%@\"%@", @"uploaded_file", filename, kNewLine] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[[NSString stringWithFormat:@"Content-Type: image/jpg"] dataUsingEncoding:NSUTF8StringEncoding]];
    
    // Now append the image itself.  For some servers, two carriage-return line-feeds are necessary before the image
    [body appendData:[[NSString stringWithFormat:@"%@%@", kNewLine, kNewLine] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:imageData];
    [body appendData:[kNewLine dataUsingEncoding:NSUTF8StringEncoding]];
    
    // Add the terminating boundary marker & append a newline
    [body appendData:[[NSString stringWithFormat:@"--%@--", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[kNewLine dataUsingEncoding:NSUTF8StringEncoding]];
    
    // Setting the body of the post to the request.
    [request setHTTPBody:body];
    
    // TODO: Next three lines are only used for testing using synchronous conn.
    /*NSData *returnData = [NSURLConnection sendSynchronousRequest:request returningResponse:nil error:nil];
    NSString *returnString = [[NSString alloc] initWithData:returnData encoding:NSUTF8StringEncoding];
    NSLog(@"==> sendSyncReq returnString: %@", returnString); */
    NSURLSession *session = [NSURLSession sharedSession];
    
    [[session dataTaskWithRequest:request completionHandler:^(NSData * _Nullable data, NSURLResponse * _Nullable response, NSError * _Nullable error) {
        
        if (data) {
            
            //if response is in string format
            NSString *returnString = [[NSString alloc]initWithData:data encoding:NSUTF8StringEncoding];
            NSDictionary *dictionary = [returnString JSONValue];
            
            for(NSDictionary *row in dictionary)
            {
                if ([[row objectForKey:@"status"] isEqualToString:@"1"]) {
                    [self getInboxList];
                } else {
                    [self displayAlert:[row objectForKey:@"desc"]];
                }
            }
            
            //if response in json format then
            //id jsonObject = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:nil];
            
        }
        
    }]resume];
    
    // You will probably want to replace above 3 lines with asynchronous connection
    //    NSURLConnection *connection = [[NSURLConnection alloc]initWithRequest:request delegate:self];

}


- (void)aMethodDelete:(id)sender
{
    UIButton *clicked = (UIButton *) sender;

    NSString *commentsid = [NSString stringWithFormat:@"%d", clicked.tag];
    
    //Delete button
    NSString *urlAddressSubmit = [[NSString alloc] initWithFormat:@"%@comments_delete.php?db=%@&userid=%@&udid=%@&cuid=%@&commentsid=%@&cid=%@",appdelegate.user_apipath,appdelegate.db,appdelegate.user,appdelegate.uuid,appdelegate.customer_id, commentsid, contId];
    currentRequest = @"delete";
    [self connectToServer:urlAddressSubmit];
    
}

//Slide Menu
- (void)willAnimateRotationToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation duration:(NSTimeInterval)duration
{
    //NSLog(@"view frame=%@ bounds=%@", NSStringFromCGRect(self.view.frame), NSStringFromCGRect(self.view.bounds));
}


- (void)showModalButtonWasTouched
{
    self.interactor = [[BTSlideInteractor alloc] init];
    self.interactor.presenting = YES;
    
    //BTModalViewController *modalController = [self.storyboard instantiateViewControllerWithIdentifier:@"ModalViewController"];
    modalController = [[InitialSlidingViewController alloc] init];
    modalController.modalPresentationStyle = UIModalPresentationCustom;
    modalController.transitioningDelegate = self.interactor;
    [self.navigationController presentViewController:modalController animated:YES completion:nil];
    //[self.navigationController popToViewController:modalController animated:NO];
    //[self.navigationController pushViewController:modalController animated:YES];
}
// End Slide Menu

-(void)myHomeAction:(UIBarButtonItem *)sender{
    
    appdelegate.nextPage = @"";
    
    if ([appdelegate.listOfVerMenu count]>0) {
        if (appdelegate.homeclicked==NO) {
            appdelegate.homeclicked = YES;
            [self showModalButtonWasTouched];
        } else {
            appdelegate.homeclicked = NO;
            [modalController dismissViewControllerAnimated:YES completion:nil];
        }
    } else {
        //AppDelegate *appdelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
        //NSLog(@"View Controllers : %@",[appdelegate.navController viewControllers]);
        NSMutableArray *navigationArray = [[NSMutableArray alloc] initWithArray: appdelegate.navController.viewControllers];
        
        for (UIViewController* viewController in appdelegate.navController.viewControllers) {
            
            //This if condition checks whether the viewController's class is MyGroupViewController
            // if true that means its the MyGroupViewController (which has been pushed at some point)
            if ([viewController isKindOfClass:[RootViewController class]] ) {
                
                // Here viewController is a reference of UIViewController base class of MyGroupViewController
                // but viewController holds MyGroupViewController  object so we can type cast it here
                //ThemeViewController *themeViewController = (ThemeViewController*)viewController;
                //[self.navigationController popToViewController:themeViewController animated:YES];
                [navigationArray removeObjectAtIndex: 0];
            }
        }
        appdelegate.navController.viewControllers = navigationArray;
        for (UIViewController* viewController in appdelegate.navController.viewControllers) {
            
            //This if condition checks whether the viewController's class is MyGroupViewController
            // if true that means its the MyGroupViewController (which has been pushed at some point)
            if ([viewController isKindOfClass:[ViewController class]] ) {
                
                // Here viewController is a reference of UIViewController base class of MyGroupViewController
                // but viewController holds MyGroupViewController  object so we can type cast it here
                ViewController *themeViewController = (ViewController*)viewController;
                [appdelegate.navController popToViewController:themeViewController animated:NO];
                //[self.navigationController popToViewController:viewController animated:YES];
            }
        }
        //NSLog(@"View Controllers111 : %@",appdelegate.navController.viewControllers);
    }
    
}

-(NSString *) urlEncode:(NSString *) theStr {
    NSString *encoded = (NSString *)CFBridgingRelease(CFURLCreateStringByAddingPercentEscapes(
                                                                                              NULL,
                                                                                              (__bridge CFStringRef) theStr,
                                                                                              NULL,
                                                                                              CFSTR("!*'();:@&=+$,/?%#[]\" "),
                                                                                              kCFStringEncodingUTF8));
    return encoded;
}

#pragma mark CONNECT TO SERVER
-(void) connectToServer:(NSString *) urlPath {
    [self addLoadingView];
	NSString *urlAddress =[NSString stringWithFormat:@"%@", urlPath];
	
	/*NSURL *url = [NSURL URLWithString:urlAddress];
	
	NSURLRequest *requestObj = [NSURLRequest requestWithURL:url
												cachePolicy:NSURLRequestReloadIgnoringLocalAndRemoteCacheData
											timeoutInterval:30];
	
	NSURLConnection *theConnection=[[NSURLConnection alloc] initWithRequest:requestObj delegate:self];
	if (theConnection) {
		// Create the NSMutableData to hold the received data.
		// receivedData is an instance variable declared elsewhere.
		receivedData = [NSMutableData data];
	} else {
		// Inform the user that the connection failed.
		//[self displayAlert:@"Connection Error. Please try again later."];
		[self displayAlert:@"Connection error. Please try again later."];
        [self removeLoadingView];
	} */
    [self getJsonResponse:urlAddress success:^(NSDictionary *responseDict) {
        
        //NSLog(@"%@",responseDict);
        [self successfunction:responseDict];
        
    } failure:^(NSError *error) {
        // release the connection, and the data object
        
        [self removeLoadingView];
        [self displayAlert:@"No internet connection available. Standard Mobile Apps requires internet connection to function properly."];
    }];
}

-(void)getJsonResponse : (NSString *)urlStr success : (void (^)(NSDictionary *responseDict))success failure:(void(^)(NSError* error))failure
{
    
    
    NSURLSession * session = [NSURLSession sharedSession];
    NSURL * url = [NSURL URLWithString: urlStr];
    
    
    // Asynchronously Api is hit here
    NSURLSessionDataTask * dataTask = [session dataTaskWithURL:url completionHandler:^(NSData *data, NSURLResponse *response, NSError *error)
                                       {
                                           
                                           //NSLog(@"%@",data);
                                           NSDictionary * json;
                                           if ([data length]>0) {
                                               json  = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
                                               //NSLog(@"%@",json);
                                               success(json);
                                           } else {
                                               json = nil;
                                               dispatch_async(dispatch_get_main_queue(), ^ {
                                                   [self removeLoadingView];
                                                   [self displayAlert:NSLocalizedString(@"MSG_CONNECTION_ERROR", nil)];
                                               });
                                           }
                                           
                                           
                                       }];
    
    [dataTask resume] ; // Executed First
    
    
}

-(void) successfunction:(NSDictionary *)responseDict {
    dispatch_async(dispatch_get_main_queue(), ^{
        //NSString *jsonString = [responseDict JSONRepresentation];
        
        [self removeLoadingView];
        
        if ([currentRequest isEqualToString:@"InboxList"])
        {
            NSDictionary *dictionary = responseDict;
            
            listOfContent = [[NSMutableArray alloc] init];
            
            for(NSDictionary *row in dictionary)
            {
                NSMutableDictionary *mutableDic = [NSMutableDictionary dictionaryWithObjectsAndKeys:
                                                   [row objectForKey:@"id"],@"comments_id",
                                                   [row objectForKey:@"top_id"],@"top_id",
                                                   [row objectForKey:@"content_id"],@"content_id",
                                                   [row objectForKey:@"message"],@"message",
                                                   [row objectForKey:@"m_date"],@"m_date",
                                                   [row objectForKey:@"name"],@"name",
                                                   [row objectForKey:@"image_url"],@"image_url",
                                                   nil];
                
                [listOfContent addObject:mutableDic];
            }
            
            [self displaycontent];
            //[table_loyaltyList reloadData];
            
            //if ([listOfContent count]==0)
            //[self displayAlert:@"No loyalty program(s) currently available at the moment."];
        }
        
        if ([currentRequest isEqualToString:@"submit"] || [currentRequest isEqualToString:@"delete"])
        {
            NSDictionary *dictionary = responseDict;
            //NSLog(@"dictionary:%@", dictionary);
            for(NSDictionary *row in dictionary)
            {
                
                if ([[row objectForKey:@"status"] isEqualToString:@"1"])
                {
                    [self getInboxList];
                    
                } else {
                    [self displayAlert:[row objectForKey:@"desc"]];
                }
            }
        }
    });
    
}

/*- (void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response
{
    [receivedData setLength:0];
}

- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data
{
    [receivedData appendData:data];
}

- (void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error {
    // release the connection, and the data object
	
    connection =nil;
	receivedData =nil;
	
    [self removeLoadingView];
	[self displayAlert:@"No internet connection available. Standard Mobile Apps requires internet connection to function properly."];
}

- (void)connectionDidFinishLoading:(NSURLConnection *)connection
{
	//NSLog(@"Succeed! Received %d bytes of data",[receivedData length]);
    [self removeLoadingView];
    
	NSString *response = [[NSString alloc] initWithData:receivedData encoding:NSUTF8StringEncoding];
	//NSLog(@"response :: %@", response);
	if ([currentRequest isEqualToString:@"InboxList"])
    {
		NSDictionary *dictionary = [response JSONValue];
        
		listOfContent = [[NSMutableArray alloc] init];
        
		for(NSDictionary *row in dictionary)
        {
            NSMutableDictionary *mutableDic = [NSMutableDictionary dictionaryWithObjectsAndKeys:
                                               [row objectForKey:@"id"],@"comments_id",
                                               [row objectForKey:@"top_id"],@"top_id",
                                               [row objectForKey:@"content_id"],@"content_id",
                                               [row objectForKey:@"message"],@"message",
                                               [row objectForKey:@"m_date"],@"m_date",
                                               [row objectForKey:@"name"],@"name",
                                               [row objectForKey:@"image_url"],@"image_url",
                                               nil];
            
			[listOfContent addObject:mutableDic];
        }
        
        [self displaycontent];
        //[table_loyaltyList reloadData];
        
        //if ([listOfContent count]==0)
            //[self displayAlert:@"No loyalty program(s) currently available at the moment."];
        connection =nil;
        response =nil;
        receivedData =nil;
	}
    
    if ([currentRequest isEqualToString:@"submit"] || [currentRequest isEqualToString:@"delete"])
    {
        NSDictionary *dictionary = [response JSONValue];
        //NSLog(@"dictionary:%@", dictionary);
        for(NSDictionary *row in dictionary)
        {
            
            if ([[row objectForKey:@"status"] isEqualToString:@"1"])
            {
                
                [self getInboxList];
            } else {
                [self displayAlert:[row objectForKey:@"desc"]];
                connection =nil;
                response =nil;
                receivedData =nil;
            }
        }
    }
    
} */

#pragma mark LoadingView
-(void) addLoadingView
{
	loadingView = [LoadingView loadingViewInView:self.view];
}

-(void) removeLoadingView
{
	[loadingView removeView];
}

#pragma mark displayAlert
-(void) displayAlert: (NSString *) stralert {
	NSString *str = [[NSString alloc] initWithFormat:@"%@", stralert];
	UIAlertView *alert = [[UIAlertView alloc] initWithTitle:appdelegate.iconname
													message:str
												   delegate:self
										  cancelButtonTitle:NSLocalizedString(@"done", nil)
										  otherButtonTitles:nil];
	
	[alert show];
	str =nil;
	alert =nil;
}

-(BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString*)string
{
    if (range.length==0) {
		if ([string isEqualToString:@"\n"]) {
			[textView resignFirstResponder];
			return NO;
		}
	}
    
    return YES;
}

-(void)textViewDidBeginEditing:(UITextView *)textField
{
    CGRect textFieldRect =
    [self.view.window convertRect:textField.bounds fromView:textField];
    CGRect viewRect =
    [self.view.window convertRect:self.view.bounds fromView:self.view];
    
    CGFloat midline = textFieldRect.origin.y + 0.5 * textFieldRect.size.height;
    CGFloat numerator =
    midline - viewRect.origin.y
    - MINIMUM_SCROLL_FRACTION * viewRect.size.height;
    CGFloat denominator =
    (MAXIMUM_SCROLL_FRACTION - MINIMUM_SCROLL_FRACTION)
    * viewRect.size.height;
    CGFloat heightFraction = numerator / denominator;
    if (heightFraction < 0.0)
    {
        heightFraction = 0.0;
    }
    else if (heightFraction > 1.0)
    {
        heightFraction = 1.0;
    }
    
    
    animatedDistance = floor(PORTRAIT_KEYBOARD_HEIGHT * heightFraction);
    
    CGRect viewFrame = self.view.frame;
    viewFrame.origin.y -= animatedDistance;
    
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationBeginsFromCurrentState:YES];
    [UIView setAnimationDuration:KEYBOARD_ANIMATION_DURATION];
    
    [self.view setFrame:viewFrame];
    
    [UIView commitAnimations];
}

- (void)textViewDidEndEditing:(UITextView *)textField

{
    CGRect viewFrame = self.view.frame;
    viewFrame.origin.y += animatedDistance;
    
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationBeginsFromCurrentState:YES];
    [UIView setAnimationDuration:KEYBOARD_ANIMATION_DURATION];
    
    [self.view setFrame:viewFrame];
    
    [UIView commitAnimations];
    
}

-(void) viewDidDisappear:(BOOL)animated {
    
    [super viewDidDisappear:animated];
    
    appdelegate.homeclicked = NO;
    [modalController dismissViewControllerAnimated:YES completion:nil];
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)dealloc {
	currentRequest =nil;
    //receivedData =nil;
    listOfContent =nil;
    scrollview =nil;
    //[super dealloc];
}

@end
