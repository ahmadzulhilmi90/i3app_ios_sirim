//
//  ContentPluginViewController.h
//  standardmobileappsv3
//
//  Created by M3Online on 5/30/14.
//  Copyright (c) 2014 M3Online. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <SDWebImage/UIButton+WebCache.h>
#import <SDWebImage/UIImageView+WebCache.h>
#import <MessageUI/MessageUI.h>

@interface ContentCatPluginViewController : UIViewController <UITableViewDataSource, UITableViewDelegate, UITextFieldDelegate, UIViewControllerTransitioningDelegate> {
    NSMutableArray *listOfContents;
    NSDictionary *dicContents;
    
    IBOutlet UITableView *GridtableView;
    NSMutableArray *sections;
    
    IBOutlet UITableView *MenutableView;
    
    UIScrollView *scrollview;
    
    NSMutableArray *listOfSameDate;
    NSDictionary *dicSameDate;
    
    NSString *pluginValue1;
    NSString *pluginValue2;
    NSString *pluginValue3;
    
    NSMutableArray *listOfcat;

}

@property (nonatomic, strong) NSMutableArray *listOfContents;

@property (nonatomic, strong) UITableView *GridtableView;
@property (nonatomic, strong) NSMutableArray *sections;

@property (nonatomic, strong) UITableView *MenutableView;

@property (strong, nonatomic) IBOutlet UIScrollView *scrollView;

@property (strong, nonatomic) IBOutlet UITextField *txt_search;
@property (strong, nonatomic) IBOutlet UIButton *button_searchDetail;

@property (nonatomic, strong) NSMutableArray *listOfSameDate;

@property (nonatomic, strong) NSString *pluginValue1;
@property (nonatomic, strong) NSString *pluginValue2;
@property (nonatomic, strong) NSString *pluginValue3;

@property (nonatomic, strong) NSMutableArray *listOfcat;

@end
