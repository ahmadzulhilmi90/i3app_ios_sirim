//
//  ContentPluginViewController.m
//  standardmobileappsv3
//
//  Created by M3Online on 5/30/14.
//  Copyright (c) 2014 M3Online. All rights reserved.
//

#import "ContentCatPluginViewController.h"
#import "AppDelegate.h"
#import <sqlite3.h>
#import "Item.h"
#import "NSString+HTML.h"
#import "ContentPluginViewController.h"
#import <CoreLocation/CoreLocation.h>
#import "RootViewController.h"
#import "ViewController.h"
#import "InitialSlidingViewController.h"
#import "BTSlideInteractor.h"

@interface ContentCatPluginViewController ()
@property (strong, nonatomic) BTSlideInteractor *interactor;
@end

@implementation ContentCatPluginViewController
UIAlertView *alert;
UIActivityIndicatorView *indicator;
CLLocationManager *locationManager;

@synthesize listOfContents, GridtableView, sections, scrollView, listOfSameDate;
@synthesize MenutableView, txt_search, button_searchDetail, pluginValue1, pluginValue2, pluginValue3, listOfcat;

__strong NSString *cont_id;
__strong NSString *cont_title;

InitialSlidingViewController *modalController;

//__strong NSString *list_template;
//__strong NSString *catname;

AppDelegate *appdelegate;

- (UIColor *) colorWithHexString:(NSString *)hexString {
    NSString *colorString = [[hexString stringByReplacingOccurrencesOfString: @"#" withString: @""] uppercaseString];
    CGFloat alpha, red, blue, green;
    alpha = 1.0f;
    red   = [self colorComponentFrom: colorString start: 0 length: 2];
    green = [self colorComponentFrom: colorString start: 2 length: 2];
    blue  = [self colorComponentFrom: colorString start: 4 length: 2];
    return [UIColor colorWithRed: red green: green blue: blue alpha: alpha];
}

- (CGFloat) colorComponentFrom:(NSString *)string start:(NSUInteger)start length:(NSUInteger)length {
    NSString *substring = [string substringWithRange: NSMakeRange(start, length)];
    NSString *fullHex = length == 2 ? substring : [NSString stringWithFormat: @"%@%@", substring, substring];
    unsigned hexComponent;
    [[NSScanner scannerWithString: fullHex] scanHexInt: &hexComponent];
    return hexComponent / 255.0;
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    appdelegate.homeclicked = NO;
    
    //self.title = appdelegate.iconname;
    self.view.backgroundColor = [UIColor clearColor];
    self.navigationItem.backBarButtonItem = [[UIBarButtonItem alloc]initWithTitle:@"" style:UIBarButtonItemStylePlain target:nil action:nil];
    
    appdelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    
    scrollview = [[UIScrollView alloc] initWithFrame:CGRectMake(0, 5, self.view.frame.size.width, self.view.frame.size.height-5)];
    scrollview.backgroundColor = [UIColor clearColor];
    scrollview.bounces = NO;
    [self.view addSubview:scrollview];
    
    CGRect rect = CGRectMake(0,0,20,20);
    UIGraphicsBeginImageContext( rect.size );
    [[UIImage imageNamed:@"home.png"] drawInRect:rect];
    UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    UIBarButtonItem *myHomeButton = [[UIBarButtonItem alloc] initWithImage:image style:UIBarButtonItemStyleDone target:self action:@selector(myHomeAction:)];
    
    
    NSArray *myButtonArray = [[NSArray alloc] initWithObjects:myHomeButton, nil];
    
    self.navigationItem.rightBarButtonItems = myButtonArray;
    
    txt_search = [[UITextField alloc] initWithFrame:CGRectMake(10, 15, self.view.frame.size.width-130, 40)];
    txt_search.textColor = [UIColor blackColor];
    [txt_search setBorderStyle:UITextBorderStyleRoundedRect];
    txt_search.delegate = self;
    
    listOfSameDate = [[NSMutableArray alloc] init];
    listOfcat = [[NSMutableArray alloc] init];
}

/*- (NSString *)deviceLocation {
    return [NSString stringWithFormat:@"latitude: %f longitude: %f", locationManager.location.coordinate.latitude, locationManager.location.coordinate.longitude];
} */

-(void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    [self processdata];

}

//Slide Menu
- (void)willAnimateRotationToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation duration:(NSTimeInterval)duration
{
    //NSLog(@"view frame=%@ bounds=%@", NSStringFromCGRect(self.view.frame), NSStringFromCGRect(self.view.bounds));
}


- (void)showModalButtonWasTouched
{
    self.interactor = [[BTSlideInteractor alloc] init];
    self.interactor.presenting = YES;
    
    //BTModalViewController *modalController = [self.storyboard instantiateViewControllerWithIdentifier:@"ModalViewController"];
    modalController = [[InitialSlidingViewController alloc] init];
    modalController.modalPresentationStyle = UIModalPresentationCustom;
    modalController.transitioningDelegate = self.interactor;
    [self.navigationController presentViewController:modalController animated:YES completion:nil];
    //[self.navigationController popToViewController:modalController animated:NO];
    //[self.navigationController pushViewController:modalController animated:YES];
}
// End Slide Menu

-(void)myHomeAction:(UIBarButtonItem *)sender{
    
    appdelegate.nextPage = @"";
    
    if ([appdelegate.listOfVerMenu count]>0) {
        if (appdelegate.homeclicked==NO) {
            appdelegate.homeclicked = YES;
            [self showModalButtonWasTouched];
        } else {
            appdelegate.homeclicked = NO;
            [modalController dismissViewControllerAnimated:YES completion:nil];
        }
    } else {
        //AppDelegate *appdelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
        //NSLog(@"View Controllers : %@",[appdelegate.navController viewControllers]);
        NSMutableArray *navigationArray = [[NSMutableArray alloc] initWithArray: appdelegate.navController.viewControllers];
        
        for (UIViewController* viewController in appdelegate.navController.viewControllers) {
            
            //This if condition checks whether the viewController's class is MyGroupViewController
            // if true that means its the MyGroupViewController (which has been pushed at some point)
            if ([viewController isKindOfClass:[RootViewController class]] ) {
                
                // Here viewController is a reference of UIViewController base class of MyGroupViewController
                // but viewController holds MyGroupViewController  object so we can type cast it here
                //ThemeViewController *themeViewController = (ThemeViewController*)viewController;
                //[self.navigationController popToViewController:themeViewController animated:YES];
                [navigationArray removeObjectAtIndex: 0];
            }
        }
        appdelegate.navController.viewControllers = navigationArray;
        for (UIViewController* viewController in appdelegate.navController.viewControllers) {
            
            //This if condition checks whether the viewController's class is MyGroupViewController
            // if true that means its the MyGroupViewController (which has been pushed at some point)
            if ([viewController isKindOfClass:[ViewController class]] ) {
                
                // Here viewController is a reference of UIViewController base class of MyGroupViewController
                // but viewController holds MyGroupViewController  object so we can type cast it here
                ViewController *themeViewController = (ViewController*)viewController;
                [appdelegate.navController popToViewController:themeViewController animated:NO];
                //[self.navigationController popToViewController:viewController animated:YES];
            }
        }
        //NSLog(@"View Controllers111 : %@",appdelegate.navController.viewControllers);
    }
    
}

- (void) processdata {
    
    sqlite3 *database;
    
    NSString *catid;
    
    //NSLog(@"deviceLocation: %@", [self deviceLocation]);
    
    if(sqlite3_open([appdelegate.databasePath UTF8String], &database) == SQLITE_OK) {
        
        //if ([aplugintype isEqualToString:@"content"]) {
        
        self.title = pluginValue1;
        
        /*Maggie NSString *content_category;
         content_category = [[NSString alloc] initWithFormat:@"SELECT id, name, top_id from content_category WHERE id IN (%@) order by plugin_id", pluginValue3];
         
         NSString *cid;
         NSString *ctopid;
         NSString *ctitle;
         
         const char *sqlStatement2 = [content_category UTF8String];
         sqlite3_stmt *compiledStatement2;
         if(sqlite3_prepare_v2(database, sqlStatement2, -1, &compiledStatement2, NULL) == SQLITE_OK) {
         
         NSDictionary *dictionarypro;
         [listOfcat removeAllObjects];
         while(sqlite3_step(compiledStatement2) == SQLITE_ROW) {
         
         cid = [NSString stringWithUTF8String:(char *)sqlite3_column_text(compiledStatement2, 0)];
         ctitle = [NSString stringWithUTF8String:(char *)sqlite3_column_text(compiledStatement2, 1)];
         ctopid = [NSString stringWithUTF8String:(char *)sqlite3_column_text(compiledStatement2, 2)];
         
         ctitle = [ctitle stringByDecodingHTMLEntities];
         ctitle = [ctitle stringByReplacingOccurrencesOfString:@"''" withString:@"'"];
         
         dictionarypro = [NSDictionary dictionaryWithObjectsAndKeys:cid,@"id",ctitle,@"title",ctopid,@"topid",nil];
         [listOfcat addObject:dictionarypro];
         //NSLog(@"listOfprod :: %@", listOfprod);
         }
         
         }
         sqlite3_finalize(compiledStatement2);
         
         for(int i=0; i<[listOfcat count]; i++)
         {
         NSDictionary *data = [listOfcat objectAtIndex:i];
         NSString *sname = [data objectForKey:@"product_title"];
         
         cname.text = [NSString stringWithFormat:@"%@",sname];
         } */
        
        NSString *theme_object;
        theme_object = [[NSString alloc] initWithFormat:@" SELECT cc.id, cc.name, cc.top_id, c.date_updated, c.title, c.image from content_category cc JOIN contents c ON cc.id = c.category_id WHERE cc.id IN (%@) Group By cc.id having MAX(c.date_updated) order by cc.plugin_id, c.date_updated", pluginValue3];
        //NSLog(@"theme_object:: %@", theme_object);
        
        NSString *cimage;
        NSString *cthumbnail;
        NSString *cid;
        NSString *ctopid;
        NSString *ctitle;
        
        const char *sqlStatement3 = [theme_object UTF8String];
        sqlite3_stmt *compiledStatement3;
        if(sqlite3_prepare_v2(database, sqlStatement3, -1, &compiledStatement3, NULL) == SQLITE_OK) {
            listOfContents = [[NSMutableArray alloc] init];
            while(sqlite3_step(compiledStatement3) == SQLITE_ROW) {
                
                cid = [NSString stringWithUTF8String:(char *)sqlite3_column_text(compiledStatement3, 0)];
                ctitle = [NSString stringWithUTF8String:(char *)sqlite3_column_text(compiledStatement3, 1)];
                ctopid = [NSString stringWithUTF8String:(char *)sqlite3_column_text(compiledStatement3, 2)];
                
                ctitle = [ctitle stringByDecodingHTMLEntities];
                ctitle = [ctitle stringByReplacingOccurrencesOfString:@"''" withString:@"'"];
                cimage = [NSString stringWithUTF8String:(char *)sqlite3_column_text(compiledStatement3, 5)];
                cthumbnail = [NSString stringWithUTF8String:(char *)sqlite3_column_text(compiledStatement3, 5)];
                
                dicContents = [NSDictionary dictionaryWithObjectsAndKeys:cid,@"id",ctitle,@"title",ctopid,@"topid",cimage,@"image",cthumbnail,@"thumbnail", nil];
                [listOfContents addObject:dicContents];
                
            }
        }
        // Release the compiled statement from memory
        sqlite3_finalize(compiledStatement3);
        
        //NSLog(@"listOfContents::%@", listOfContents);
        //AppDelegate *appdelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
        
        if([listOfContents count]>0){
            
            int i;
            
            if ([pluginValue2 isEqualToString:@"2"]) {
                
                int height2;
                
                if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone) {
                    // For iPhone
                    height2=200;
                } else {
                    height2=400;
                }
                scrollview.contentSize = CGSizeMake(self.view.frame.size.width, ((5*[listOfContents count]) + (height2*([listOfContents count])))+130 + 20);
                //[self.view addSubview:scrollview];
                
                int pos = 0;
                for (i=0;i<[listOfContents count]; i++) {
                    //float x = (float)(67 + ((95 + 22) * i));
                    int x;
                    
                    //x = (10 + (50*pos) + (200*pos));
                    x = ((5*pos) + (height2*pos));
                    
                    NSDictionary *dictionary = [listOfContents objectAtIndex:i];
                    
                    NSString *content_img = [dictionary objectForKey:@"image"];
                    //NSRange range = [content_img rangeOfString:@"upload/"];
                    //NSString *content_img_file = [[content_img substringFromIndex:NSMaxRange(range)] stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
                    NSString *bar_name = [dictionary objectForKey:@"title"];
                    //cont_id = [dictionary objectForKey:@"id"];
                    
                    UIButton *myButton = [UIButton buttonWithType:UIButtonTypeCustom];
                    myButton.frame = CGRectMake(5, x, self.view.frame.size.width-10, height2);
                    myButton.tag = i;
                    [myButton addTarget:self
                                 action:@selector(checkButtonClick:)
                       forControlEvents:UIControlEventTouchUpInside];
                    
                    if ([content_img length]>0 || [bar_name length]>0) {
                        
                        if ([content_img length]>0)
                        {
                            //[myButton setBackgroundImageWithURL:[NSURL URLWithString:content_img] forState:UIControlStateNormal placeholderImage:[UIImage imageNamed:@"No-Image-icon.png"]];
                            UIImageView *imgV = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, myButton.frame.size.width, myButton.frame.size.height)];
                            
                            [myButton addSubview:imgV];
                            
                            __block UIActivityIndicatorView *activityIndicator;
                            __weak UIImageView *weakImageView = imgV;
                            /*[weakImageView setImageWithURL:[NSURL URLWithString:content_img] placeholderImage:[UIImage imageNamed:@"No-Image-icon.png"] options:SDWebImageProgressiveDownload progress:^(NSInteger receivedSize, NSInteger expectedSize)
                             {
                                 if (!activityIndicator)
                                 {
                                     [weakImageView addSubview:activityIndicator = [UIActivityIndicatorView.alloc initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray]];
                                     activityIndicator.center = weakImageView.center;
                                     [activityIndicator startAnimating];
                                 }
                             }
                                                 completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType)
                             {
                                 [activityIndicator removeFromSuperview];
                                 activityIndicator = nil;
                             }]; */
                            dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0), ^ {
                                [weakImageView sd_setImageWithURL:[NSURL URLWithString:content_img]
                                                 placeholderImage:[UIImage imageNamed:@"No-Image-icon.png"]
                                                          options:SDWebImageProgressiveDownload
                                                         progress:^(NSInteger receivedSize, NSInteger expectedSize, NSURL *targetURL) {
                                                             dispatch_async(dispatch_get_main_queue(), ^ {
                                                                 if (!activityIndicator) {
                                                                     
                                                                     [weakImageView addSubview:activityIndicator = [UIActivityIndicatorView.alloc initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray]];
                                                                     activityIndicator.center = weakImageView.center;
                                                                     [activityIndicator startAnimating];
                                                                 }
                                                             });
                                                         }
                                                        completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
                                                            dispatch_async(dispatch_get_main_queue(), ^ {
                                                                //[MBProgressHUD hideHUDForView:self.view animated:YES];
                                                                [activityIndicator removeFromSuperview];
                                                                activityIndicator = nil;
                                                            });
                                                        }];
                            });
                            
                            imgV.contentMode  = UIViewContentModeScaleAspectFill;
                            [imgV setClipsToBounds:YES];
                        }
                        else
                        {
                            [myButton setTitle:@"No Image" forState:UIControlStateNormal];
                            [myButton setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
                            [myButton setBackgroundColor:[UIColor colorWithRed:230/255.0 green:230/255.0 blue:230/255.0 alpha:1]];
                        }
                        
                        [scrollview addSubview:myButton];
                        
                        UIImageView *ImageBarView = [[UIImageView alloc] initWithFrame:CGRectMake(5, x+self.view.frame.size.width/2-10, self.view.frame.size.width-10, 50)];
                        //ImageBarView.backgroundColor = [self colorWithHexString:appdelegate.themeColorCode];
                        //ImageBarView.alpha = 0.6;
                        
                        UIView *overlay;
                        overlay = [[UIView alloc] initWithFrame:CGRectMake(0, 0, ImageBarView.frame.size.width, ImageBarView.frame.size.height)];
                        overlay.backgroundColor = [self colorWithHexString:appdelegate.themeColorCode];
                        overlay.alpha = 0.6f;
                        //overlay.opaque = YES;
                        
                        [ImageBarView addSubview:overlay];
                        
                        UILabel *lblTitle = [[UILabel alloc] initWithFrame:CGRectMake(5, 0, self.view.frame.size.width-10, 60)];
                        lblTitle.numberOfLines = 0;
                        lblTitle.lineBreakMode = NSLineBreakByWordWrapping;
                        UIFont *yourFont = [UIFont fontWithName:@"Helvetica-Bold" size:22];
                        lblTitle.font = yourFont;
                        lblTitle.text = bar_name;
                        lblTitle.backgroundColor = [UIColor clearColor];
                        lblTitle.textColor = [UIColor whiteColor];
                        [lblTitle sizeToFit];
                        lblTitle.shadowColor = [UIColor blackColor];
                        lblTitle.shadowOffset = CGSizeMake(0, 1);
                        [ImageBarView addSubview:lblTitle];
                        CGSize constrainedSize = CGSizeMake(lblTitle.frame.size.width  , 9999);
                        
                        NSDictionary *attributesDictionary = [NSDictionary dictionaryWithObjectsAndKeys:
                                                              yourFont, NSFontAttributeName,
                                                              nil];
                        
                        NSMutableAttributedString *string = [[NSMutableAttributedString alloc] initWithString:lblTitle.text attributes:attributesDictionary];
                        
                        CGRect requiredHeight = [string boundingRectWithSize:constrainedSize options:NSStringDrawingUsesLineFragmentOrigin context:nil];
                        
                        if (requiredHeight.size.width > lblTitle.frame.size.width) {
                            requiredHeight = CGRectMake(0,0, lblTitle.frame.size.width, requiredHeight.size.height);
                        }
                        CGRect newFrame = lblTitle.frame;
                        newFrame.size.height = requiredHeight.size.height;
                        lblTitle.frame = newFrame;
                        ImageBarView.frame = CGRectMake(5, x+height2-newFrame.size.height, self.view.frame.size.width-10, newFrame.size.height);
                        overlay.frame = CGRectMake(0, 0, ImageBarView.frame.size.width, ImageBarView.frame.size.height);
                        [scrollview addSubview:ImageBarView];
                        
                        pos++;
                        
                        lblTitle =nil;
                        ImageBarView =nil;
                        
                    }
                    
                }
            } else if ([pluginValue2 isEqualToString:@"3"] || [pluginValue2 isEqualToString:@"4"] || [pluginValue2 isEqualToString:@"8"]) {
                
                sections = [[NSMutableArray alloc] init];
                //GridtableView = [[UITableView alloc] initWithFrame:CGRectMake(0, 5, self.view.frame.size.width, self.view.frame.size.height-5)];
                GridtableView = [[UITableView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height)];
                GridtableView.autoresizingMask = UIViewAutoresizingFlexibleBottomMargin | UIViewAutoresizingFlexibleHeight;
                GridtableView.separatorStyle = UITableViewCellSeparatorStyleNone;
                GridtableView.dataSource = self;
                GridtableView.delegate   = self;
                //GridtableView.frame = self.view.frame;
                
                for(int s=0;s<1;s++)
                { // 4 sections
                    NSMutableArray *section = [[NSMutableArray alloc] init];
                    //for(int i=0;i<12;i++) {  // 12 items in each section
                    for (int i=0;i<[listOfContents count]; i++) {
                        NSDictionary *dictionary = [listOfContents objectAtIndex:i];
                        
                        NSString *cont_id3 = [dictionary objectForKey:@"id"];
                        NSString *bar_name;
                        NSString *content_img;
                        NSString *price;
                        NSString *var1;
                        NSString *pricetitle;
                        bar_name = [dictionary objectForKey:@"title"];
                        content_img = [dictionary objectForKey:@"image"];
                        price = [dictionary objectForKey:@"price"];
                        var1 = [dictionary objectForKey:@"var1"];
                        pricetitle = [dictionary objectForKey:@"price_title"];
                        //NSLog(@"content_img : %@", content_img);
                        
                        NSString *cont_name;
                        cont_name = [dictionary objectForKey:@"title"];
                        
                        Item *item = [[Item alloc] init];
                        //item.link=@"New Screen";
                        item.title=bar_name;
                        item.image=content_img;
                        item.contid=cont_id3;
                        item.contname=cont_name;
                        item.price=price;
                        item.var1=var1;
                        item.pricetitle=pricetitle;
                        item.topid=[dictionary objectForKey:@"topid"];
                        
                        [section addObject:item];
                    }
                    [sections addObject:section];
                }
                
                //[GridtableView reloadData];
                [self.view addSubview:GridtableView];
                //GridtableView.hidden = NO;
                
            } else if (/*[list_template isEqualToString:@"4"] || */[pluginValue2 isEqualToString:@"5"]) {
                
                if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone) {
                    // For iPhone
                    if ([listOfContents count]*70 > self.view.frame.size.height) {
                        MenutableView = [[UITableView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height)];
                    } else {
                        MenutableView = [[UITableView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, [listOfContents count]*70)];
                    }
                } else {
                    if ([listOfContents count]*110 > self.view.frame.size.height) {
                        MenutableView = [[UITableView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height)];
                    } else {
                        MenutableView = [[UITableView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, [listOfContents count]*110)];
                    }
                }
                MenutableView.autoresizingMask = UIViewAutoresizingFlexibleHeight | UIViewAutoresizingFlexibleWidth;
                MenutableView.delegate = self;
                MenutableView.dataSource = self;
                
                [self.view addSubview:MenutableView];
                
            } else if ([pluginValue2 isEqualToString:@"6"]) { //POI - not show template in cms content template
                
                [self.view addSubview:txt_search];
                
                button_searchDetail = [UIButton buttonWithType:UIButtonTypeRoundedRect];
                [button_searchDetail addTarget:self
                                        action:@selector(processdata) forControlEvents:UIControlEventTouchUpInside];
                [button_searchDetail setTitle:NSLocalizedString(@"search", nil) forState:UIControlStateNormal];
                [button_searchDetail setTitleColor:[UIColor blackColor] forState:UIControlStateNormal ];
                button_searchDetail.frame = CGRectMake(self.view.frame.size.width-110, 15.0, 90.0, 40.0);
                button_searchDetail.backgroundColor = [UIColor grayColor];
                button_searchDetail.layer.cornerRadius=6;
                [self.view addSubview:button_searchDetail];
                
                if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone) {
                    // For iPhone
                    if ([listOfContents count]*70 > self.view.frame.size.height-60) {
                        MenutableView = [[UITableView alloc] initWithFrame:CGRectMake(0, 60, self.view.frame.size.width, self.view.frame.size.height-60)];
                    } else {
                        MenutableView = [[UITableView alloc] initWithFrame:CGRectMake(0, 60, self.view.frame.size.width, [listOfContents count]*70)];
                    }
                } else {
                    if ([listOfContents count]*110 > self.view.frame.size.height-80) {
                        MenutableView = [[UITableView alloc] initWithFrame:CGRectMake(0, 80, self.view.frame.size.width, self.view.frame.size.height-80)];
                    } else {
                        MenutableView = [[UITableView alloc] initWithFrame:CGRectMake(0, 80, self.view.frame.size.width, [listOfContents count]*110)];
                    }
                }
                MenutableView.autoresizingMask = UIViewAutoresizingFlexibleHeight | UIViewAutoresizingFlexibleWidth;
                MenutableView.delegate = self;
                MenutableView.dataSource = self;
                
                [self.view addSubview:MenutableView];
                
            } else if ([pluginValue2 isEqualToString:@"7"]) {
                
                if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone) {
                    // For iPhone
                    if ([listOfContents count]*30 > self.view.frame.size.height) {
                        MenutableView = [[UITableView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height)];
                    } else {
                        MenutableView = [[UITableView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, [listOfContents count]*30)];
                    }
                } else {
                    if ([listOfContents count]*50 > self.view.frame.size.height) {
                        MenutableView = [[UITableView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height)];
                    } else {
                        MenutableView = [[UITableView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, [listOfContents count]*50)];
                    }
                }
                MenutableView.autoresizingMask = UIViewAutoresizingFlexibleHeight | UIViewAutoresizingFlexibleWidth;
                MenutableView.delegate = self;
                MenutableView.dataSource = self;
                
                [self.view addSubview:MenutableView];
                
            } else if ([pluginValue2 isEqualToString:@"9"]) {
                NSString *newString;
                NSString *currentDate = @"";
                NSString *sameDate;
                [listOfSameDate removeAllObjects];
                for(int lfsd=0; lfsd < [listOfContents count]; lfsd++) {
                    NSDictionary *dictionary = [listOfContents objectAtIndex:lfsd];
                    newString = [[dictionary objectForKey:@"date_updated"] substringToIndex:10];
                    
                    if ([currentDate isEqualToString:newString]) {
                        sameDate = @"1";
                    } else {
                        sameDate = @"0";
                    }
                    dicSameDate = [NSDictionary dictionaryWithObjectsAndKeys:newString,@"dateUpdated",sameDate,@"SameDate",nil];
                    [listOfSameDate addObject:dicSameDate];
                    
                    currentDate = newString;
                }
                currentDate = nil;
                newString = nil;
                sameDate = nil;
                //NSLog(@"listOfSameDate %@", listOfSameDate);
                int heightrow=0;
                if([listOfSameDate count]>0)
                {
                    for(int i=0; i<[listOfSameDate count]; i++ ) {
                        NSDictionary *dictionary = [listOfSameDate objectAtIndex:i];
                        
                        if ([[dictionary objectForKey:@"SameDate"] isEqualToString:@"0"]) {
                            if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone) {
                                heightrow = heightrow + self.view.frame.size.width/2-10+20+5+40;
                            } else {
                                heightrow = heightrow + self.view.frame.size.width/2-10+33+5+66;
                            }
                        } else {
                            if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone) {
                                heightrow = heightrow + 70;
                            } else {
                                heightrow = heightrow + 110;
                            }
                        }
                    }
                }
                
                [MenutableView removeFromSuperview];
                //if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone) {
                // For iPhone
                if (heightrow > self.view.frame.size.height) {
                    MenutableView = [[UITableView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height)];
                } else {
                    MenutableView = [[UITableView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, heightrow)];
                }
                /*} else {
                 if ([listOfContents count]*110 > self.view.frame.size.height) {
                 MenutableView = [[UITableView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height)];
                 } else {
                 MenutableView = [[UITableView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, [listOfContents count]*110)];
                 }
                 } */
                MenutableView.autoresizingMask = UIViewAutoresizingFlexibleHeight | UIViewAutoresizingFlexibleWidth;
                MenutableView.delegate = self;
                MenutableView.dataSource = self;
                
                [self.view addSubview:MenutableView];
                heightrow = 0;
                
            } else if ([pluginValue2 isEqualToString:@"1"]) {
                
            }
        }
        
        catid =nil;
        cid =nil;
        ctitle =nil;
        cimage =nil;
        cthumbnail =nil;
        
    }
    sqlite3_close(database);
}

-(void) checkButtonClick:(id)sender {
    
    UIButton *clicked = (UIButton *) sender;
    NSDictionary *dictionary = [listOfContents objectAtIndex:clicked.tag];
    
    appdelegate.plugin_id = [dictionary objectForKey:@"topid"];
    cont_id = [dictionary objectForKey:@"id"];
    cont_title = [dictionary objectForKey:@"title"];
    
    if (appdelegate.showloading) {
        alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"loading", nil) message:nil delegate:self cancelButtonTitle:nil otherButtonTitles: nil];
        
        //a simple activity indicator:
        UIActivityIndicatorView *activityIndicator = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
        activityIndicator.frame= CGRectMake(50, 10, 37, 37);
        activityIndicator.autoresizingMask = UIViewAutoresizingFlexibleTopMargin | UIViewAutoresizingFlexibleRightMargin | UIViewAutoresizingFlexibleBottomMargin | UIViewAutoresizingFlexibleLeftMargin;
        [activityIndicator startAnimating];
        
        //the magic line below,
        //we associate the activity indicator to the alert view: (addSubview is not used)
        [alert setValue:activityIndicator forKey:@"accessoryView"];
        
        [alert show];
    
        // Adjust the indicator so it is up a few pixels from the bottom of the alert
        [UIView beginAnimations:nil context:nil];
        [UIView setAnimationDuration:0.0];
        [UIView setAnimationDelay:0.0];
        [UIView setAnimationTransition:UIViewAnimationTransitionNone forView:alert cache:YES];
        [UIView setAnimationDelegate:self];
        [UIView setAnimationDidStopSelector:@selector(startupAnimationDoneIndicator:finished:context:)];
        alert.alpha = 0.5;
        [UIView commitAnimations];
    } else {
        [self goNextPage];
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    //NSLog(@"numberOfSectionsInTableView");
	if (tableView == GridtableView)
    {
        return [sections count];
        
    }
    
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
	//NSLog(@">>> Entering *** %s <<<", __PRETTY_FUNCTION__);
    
    //NSLog(@"tableView %@", tableView);
    
    if (tableView == MenutableView)
    {
        return [listOfContents count];
        
    }
    
    if (tableView == GridtableView) {
        return 1;
    }
    
    return 0;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (tableView == MenutableView) {
        if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone) {
            // For iPhone
            if ([pluginValue2 isEqualToString:@"7"])
                return 30;
            else if ([pluginValue2 isEqualToString:@"9"]) {
                if([listOfSameDate count]>0)
                {
                    
                    NSDictionary *dictionary = [listOfSameDate objectAtIndex:indexPath.row];
                    
                    if ([[dictionary objectForKey:@"SameDate"] isEqualToString:@"0"]) {
                        
                        return self.view.frame.size.width/2-10+20+5+40;
                        
                    } else {
                        
                        return 70;
                        
                    }
                }
            } else
                return 70;
            
        } else {
            if ([pluginValue2 isEqualToString:@"7"])
                return 50;
            else if ([pluginValue2 isEqualToString:@"9"]) {
                if([listOfSameDate count]>0)
                {
                    
                    NSDictionary *dictionary = [listOfSameDate objectAtIndex:indexPath.row];
                    
                    if ([[dictionary objectForKey:@"SameDate"] isEqualToString:@"0"]) {
                        
                        return self.view.frame.size.width/2-10+33+5+66;;
                        
                    } else {
                        
                        return 110;
                      
                    }
                    
                }
            
            } else
                return 110;
        }
    }
    
    if (tableView == GridtableView)
    {
        int count = [listOfContents count];
        
		int numRows = count/2;
        int numRows1 = (count)%2;
        
        if (numRows1>0) {
            if ([pluginValue2 isEqualToString:@"4"]) {
                return (numRows+1) * self.view.frame.size.width/4*3;
            } else if ([pluginValue2 isEqualToString:@"8"]) {
                return (numRows+1) * self.view.frame.size.width/5*3;
            } else {
                return (numRows+1) * self.view.frame.size.width/2;
            }
        } else {
            if ([pluginValue2 isEqualToString:@"4"]) {
                return (numRows) * self.view.frame.size.width/4*3;
            } else if ([pluginValue2 isEqualToString:@"8"]) {
                return (numRows) * self.view.frame.size.width/5*3;
            } else {
                return (numRows) * self.view.frame.size.width/2;
            }
        }
    }
    
	return 44;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    //NSLog(@"cellForRowAtIndexPath");
    if (tableView == MenutableView)
    {
        //static NSString *CellIdentifier = @"CellIdentifier";
        //UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
        NSString *cellIdentifier = [NSString stringWithFormat:@"cell%d",indexPath.row];
        UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
        
        UIImageView *imgViewMenu;
        const NSInteger LEFT_TAG = 1000;
        UILabel *menutext;
        const NSInteger RIGHT_TAG = 1001;
        UILabel *pricetitle;
        const NSInteger RIGHT_TAG1 = 1002;
        UILabel *price;
        const NSInteger RIGHT_TAG2 = 1003;
        UILabel *var1;
        const NSInteger RIGHT_TAG3 = 1004;
        UILabel *distance;
        const NSInteger RIGHT_TAG4 = 1005;
        UIButton *btnimgViewMenu;
        const NSInteger RIGHT_TAG5 = 1006;
        UILabel *desc;
        const NSInteger RIGHT_TAG6 = 1007;
        UILabel *datetime;
        const NSInteger RIGHT_TAG7 = 1008;
        UIImageView *ImageBarView;
        const NSInteger RIGHT_TAG8 = 1009;
        UIView *overlay;
        const NSInteger RIGHT_TAG9 = 1010;
        
        //if (cell == nil)
        //{
            
            //cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
        if(!cell) {
            cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier];
            //cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
            
            imgViewMenu = [[UIImageView alloc] init];
            imgViewMenu.tag = LEFT_TAG;
            
            menutext = [[UILabel alloc] init];
            menutext.tag = RIGHT_TAG;
            
            pricetitle = [[UILabel alloc] init];
            pricetitle.tag = RIGHT_TAG1;
            
            price = [[UILabel alloc] init];
            price.tag = RIGHT_TAG2;
            
            var1 = [[UILabel alloc] init];
            var1.tag = RIGHT_TAG3;
            
            distance = [[UILabel alloc] init];
            distance.tag = RIGHT_TAG4;
            
            btnimgViewMenu = [UIButton buttonWithType:UIButtonTypeCustom];
            btnimgViewMenu = [[UIButton alloc] init];
            btnimgViewMenu.tag = RIGHT_TAG5;
            
            desc = [[UILabel alloc] init];
            desc.tag = RIGHT_TAG6;
            
            datetime = [[UILabel alloc] init];
            datetime.tag = RIGHT_TAG7;
            
            ImageBarView = [[UIImageView alloc] init];
            ImageBarView.tag = RIGHT_TAG8;
            
            overlay = [[UIView alloc] init];
            overlay.tag = RIGHT_TAG9;
            
            if ([pluginValue2 isEqualToString:@"6"]) {
                
            
                if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone) {
                        // For iPhone
                    [menutext setFrame:CGRectMake(10, 5, self.view.frame.size.width-70, 20)];
                } else {
                    [menutext setFrame: CGRectMake(10, 5, self.view.frame.size.width-110, 33)];
                }
            
                [menutext setFont:[UIFont boldSystemFontOfSize:16]];
                menutext.textColor = [self colorWithHexString:appdelegate.themeColorCode];
                menutext.textAlignment = NSTextAlignmentLeft;
                menutext.backgroundColor = [UIColor clearColor];
                menutext.numberOfLines = 0;
                [cell.contentView addSubview:menutext];
                
                if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone) {
                    // For iPhone
                    [btnimgViewMenu setFrame:CGRectMake(self.view.frame.size.width-70, 10, 40, 40)];
                } else {
                    [btnimgViewMenu setFrame:CGRectMake(self.view.frame.size.width-110, 10, 80, 80)];
                }
                [cell.contentView addSubview:btnimgViewMenu];
                
                if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone) {
                    // For iPhone
                    [var1 setFrame: CGRectMake(10, 25, self.view.frame.size.width-70, 20)];
                } else {
                    [var1 setFrame: CGRectMake(10, 38, self.view.frame.size.width-110, 33)];
                }
                [var1 setFont:[UIFont systemFontOfSize:14]];
                var1.textColor = [UIColor grayColor];
                var1.textAlignment = NSTextAlignmentLeft;
                var1.backgroundColor = [UIColor clearColor];
                var1.numberOfLines = 0;
                [cell.contentView addSubview:var1];
                
                if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone) {
                    // For iPhone
                    [distance setFrame: CGRectMake(10, 45, self.view.frame.size.width-70, 20)];
                } else {
                    [distance setFrame: CGRectMake(10, 71, self.view.frame.size.width-110, 33)];
                }
                [distance setFont:[UIFont systemFontOfSize:14]];
                distance.textColor = [UIColor grayColor];
                distance.textAlignment = NSTextAlignmentLeft;
                distance.backgroundColor = [UIColor clearColor];
                distance.numberOfLines = 0;
                [cell.contentView addSubview:distance];
                
            } else if ([pluginValue2 isEqualToString:@"7"]) {
                
                
                if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone) {
                    // For iPhone
                    [menutext setFrame: CGRectMake(10, 5, self.view.frame.size.width-70, 20)];
                } else {
                    [menutext setFrame: CGRectMake(10, 5, self.view.frame.size.width-110, 33)];
                }
                
                [menutext setFont:[UIFont boldSystemFontOfSize:16]];
                menutext.textColor = [self colorWithHexString:appdelegate.themeColorCode];
                menutext.textAlignment = NSTextAlignmentLeft;
                menutext.backgroundColor = [UIColor clearColor];
                menutext.numberOfLines = 0;
                [cell.contentView addSubview:menutext];
                
            } else if ([pluginValue2 isEqualToString:@"9"]) {
                
                if([listOfSameDate count]>0)
                {
                    NSLog(@"listOfSameDate: %@", [listOfSameDate objectAtIndex:indexPath.row]);
                    NSDictionary *dictionary = [listOfSameDate objectAtIndex:indexPath.row];
                    
                    if ([[dictionary objectForKey:@"SameDate"] isEqualToString:@"0"]) {
                        
                        if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone) {
                            // For iPhone
                            [datetime setFrame: CGRectMake(5, 5, self.view.frame.size.width-10, 20)];
                        } else {
                            [datetime setFrame: CGRectMake(5, 5, self.view.frame.size.width-10, 33)];
                        }
                        
                        [datetime setFont:[UIFont boldSystemFontOfSize:12]];
                        datetime.textColor = [UIColor grayColor];
                        datetime.textAlignment = NSTextAlignmentLeft;
                        datetime.backgroundColor = [UIColor clearColor];
                        datetime.numberOfLines = 0;
                        [cell.contentView addSubview:datetime];
                        
                        if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone) {
                            // For iPhone
                            [imgViewMenu setFrame:CGRectMake(5, 20+5, self.view.frame.size.width-10, self.view.frame.size.width/2-10)];
                        } else {
                            [imgViewMenu setFrame:CGRectMake(5, 33+5, self.view.frame.size.width-10, self.view.frame.size.width/2-10)];
                        }
                        [cell.contentView addSubview:imgViewMenu];
                        
                        if([listOfContents count]>0) {
                            NSDictionary *dictionary1 = [listOfContents objectAtIndex:indexPath.row];
                        
                            if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone) {
                                // For iPhone
                                [ImageBarView setFrame:CGRectMake(0, imgViewMenu.frame.size.height, self.view.frame.size.width-10, 30)];
                            } else {
                                [ImageBarView setFrame:CGRectMake(0, imgViewMenu.frame.size.height, self.view.frame.size.width-10, 43)];
                            }
                            
                            [overlay setFrame:CGRectMake(0, 0, ImageBarView.frame.size.width, ImageBarView.frame.size.height)];
                            overlay.backgroundColor = [UIColor blackColor];
                            overlay.alpha = 0.4f;
                            
                            [ImageBarView addSubview:overlay];
                            
                            if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone) {
                                // For iPhone
                                [menutext setFrame: CGRectMake(5, 0, self.view.frame.size.width-10, 20)];
                            } else {
                                [menutext setFrame: CGRectMake(5, 0, self.view.frame.size.width-10, 33)];
                            }
                            
                            [menutext setFont:[UIFont boldSystemFontOfSize:16]];
                            menutext.textColor = [UIColor whiteColor];
                            menutext.textAlignment = NSTextAlignmentLeft;
                            menutext.backgroundColor = [UIColor clearColor];
                            menutext.numberOfLines = 0;
                            [ImageBarView addSubview:menutext];
                        
                            menutext.text = [NSString stringWithFormat:@"%@", [dictionary1 objectForKey:@"title"]];
                            //NSLog(@"dictionary1 %@", menutext.text);
                            CGSize constrainedSize = CGSizeMake(menutext.frame.size.width  , 9999);
                            
                            NSDictionary *attributesDictionary = [NSDictionary dictionaryWithObjectsAndKeys:
                                                                  [UIFont boldSystemFontOfSize:16], NSFontAttributeName,
                                                                  nil];
                            
                            NSMutableAttributedString *string = [[NSMutableAttributedString alloc] initWithString:menutext.text attributes:attributesDictionary];
                            
                            CGRect requiredHeight = [string boundingRectWithSize:constrainedSize options:NSStringDrawingUsesLineFragmentOrigin context:nil];
                            
                            if (requiredHeight.size.width > menutext.frame.size.width) {
                                requiredHeight = CGRectMake(5,0, menutext.frame.size.width-10, requiredHeight.size.height);
                            } else {
                                requiredHeight = CGRectMake(5,0, menutext.frame.size.width-10, menutext.frame.size.height);
                            }
                            CGRect newFrame = menutext.frame;
                            newFrame.size.height = requiredHeight.size.height;
                            menutext.frame = newFrame;
                            ImageBarView.frame = CGRectMake(0, imgViewMenu.frame.size.height-newFrame.size.height, self.view.frame.size.width-10, newFrame.size.height);
                            
                            overlay.frame = CGRectMake(0, 0, ImageBarView.frame.size.width, ImageBarView.frame.size.height);
                            
                            [imgViewMenu addSubview:ImageBarView];
                        }
                        
                        if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone) {
                            // For iPhone
                            [desc setFrame: CGRectMake(5, self.view.frame.size.width/2-10+20+5, self.view.frame.size.width-10, 40)];
                        } else {
                            [desc setFrame: CGRectMake(5, self.view.frame.size.width/2-10+33+5, self.view.frame.size.width-10, 66)];
                        }
                        [desc setFont:[UIFont systemFontOfSize:14]];
                        desc.textColor = [UIColor grayColor];
                        desc.textAlignment = NSTextAlignmentLeft;
                        desc.backgroundColor = [UIColor clearColor];
                        desc.numberOfLines = 0;
                        desc.lineBreakMode = NSLineBreakByTruncatingTail;
                        [cell.contentView addSubview:desc];
                        
                    } else {
                        if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone) {
                            // For iPhone
                            [imgViewMenu setFrame:CGRectMake(self.view.frame.size.width-65, 5, 60, 60)];
                        } else {
                            [imgViewMenu setFrame:CGRectMake(self.view.frame.size.width-105, 5, 100, 100)];
                        }
                        [cell.contentView addSubview:imgViewMenu];
                        
                        //} else if ([[dictionary objectForKey:@"object_type"] isEqualToString:@"text"]) {
                    
                        if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone) {
                            // For iPhone
                            [menutext setFrame: CGRectMake(5, 20, self.view.frame.size.width-75, 20)];
                        } else {
                            [menutext setFrame: CGRectMake(5, 40, self.view.frame.size.width-115, 33)];
                        }
                        //}
                        [menutext setFont:[UIFont boldSystemFontOfSize:16]];
                        menutext.textColor = [UIColor grayColor];
                        menutext.textAlignment = NSTextAlignmentLeft;
                        menutext.backgroundColor = [UIColor clearColor];
                        menutext.numberOfLines = 0;
                        [cell.contentView addSubview:menutext];
                        
                    }
                }
                
            } else {
                
                //if ([[dictionary objectForKey:@"object_type"] isEqualToString:@"img"]) {
                if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone) {
                    // For iPhone
                    [imgViewMenu setFrame:CGRectMake(5, 5, 60, 60)];
                } else {
                    [imgViewMenu setFrame:CGRectMake(5, 5, 100, 100)];
                }
                [cell.contentView addSubview:imgViewMenu];
                
                //} else if ([[dictionary objectForKey:@"object_type"] isEqualToString:@"text"]) {
                
                /*if ([list_template isEqualToString:@"4"]) {
                    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone) {
                        // For iPhone
                        menutext = [[UILabel alloc] initWithFrame: CGRectMake(70, 0, self.view.frame.size.width-70, 60)];
                    } else {
                        menutext = [[UILabel alloc] initWithFrame: CGRectMake(110, 0, self.view.frame.size.width-110, 100)];
                    }
                } else { */
                    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone) {
                        // For iPhone
                        [menutext setFrame: CGRectMake(70, 5, self.view.frame.size.width-70, 20)];
                    } else {
                        [menutext setFrame: CGRectMake(110, 5, self.view.frame.size.width-110, 33)];
                    }
                //}
                [menutext setFont:[UIFont boldSystemFontOfSize:16]];
                if ([pluginValue2 isEqualToString:@"5"]) {
                    menutext.textColor = [self colorWithHexString:appdelegate.themeColorCode];
                } else {
                    menutext.textColor = [UIColor grayColor];
                }
                menutext.textAlignment = NSTextAlignmentLeft;
                menutext.backgroundColor = [UIColor clearColor];
                menutext.numberOfLines = 0;
                [cell.contentView addSubview:menutext];
                
                if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone) {
                    // For iPhone
                    [pricetitle setFrame: CGRectMake(70, 25, 30, 20)];
                } else {
                    [pricetitle setFrame: CGRectMake(110, 38, 30, 33)];
                }
                [pricetitle setFont:[UIFont boldSystemFontOfSize:16]];
                pricetitle.textColor = [UIColor grayColor];
                pricetitle.textAlignment = NSTextAlignmentLeft;
                pricetitle.backgroundColor = [UIColor clearColor];
                pricetitle.numberOfLines = 0;
                [cell.contentView addSubview:pricetitle];
                
                if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone) {
                    // For iPhone
                    [price setFrame: CGRectMake(70+30, 25, self.view.frame.size.width-70-30, 20)];
                } else {
                    [price setFrame: CGRectMake(110+30, 38, self.view.frame.size.width-110-30, 33)];
                }
                [price setFont:[UIFont boldSystemFontOfSize:16]];
                price.textColor = [UIColor grayColor];
                price.textAlignment = NSTextAlignmentLeft;
                price.backgroundColor = [UIColor clearColor];
                price.numberOfLines = 0;
                [cell.contentView addSubview:price];
                
                if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone) {
                    // For iPhone
                    [var1 setFrame: CGRectMake(70, 45, self.view.frame.size.width-70, 20)];
                } else {
                    [var1 setFrame: CGRectMake(110, 71, self.view.frame.size.width-110, 33)];
                }
                [var1 setFont:[UIFont systemFontOfSize:14]];
                var1.textColor = [UIColor grayColor];
                var1.textAlignment = NSTextAlignmentLeft;
                var1.backgroundColor = [UIColor clearColor];
                var1.numberOfLines = 0;
                [cell.contentView addSubview:var1];
                //}
            }
        }
        else
        {
            //if ([[dictionary objectForKey:@"object_type"] isEqualToString:@"img"]) {
            imgViewMenu = (UIImageView *)[cell viewWithTag:LEFT_TAG];
            //} else if ([[dictionary objectForKey:@"object_type"] isEqualToString:@"text"]) {
            menutext = (UILabel *)[cell viewWithTag:RIGHT_TAG];
            pricetitle = (UILabel *)[cell viewWithTag:RIGHT_TAG1];
            price = (UILabel *)[cell viewWithTag:RIGHT_TAG2];
            var1 = (UILabel *)[cell viewWithTag:RIGHT_TAG3];
            distance = (UILabel *)[cell viewWithTag:RIGHT_TAG4];
            btnimgViewMenu = (UIButton *)[cell viewWithTag:RIGHT_TAG5];
            desc = (UILabel *)[cell viewWithTag:RIGHT_TAG6];
            datetime = (UILabel *)[cell viewWithTag:RIGHT_TAG7];
            ImageBarView = (UIImageView *)[cell viewWithTag:RIGHT_TAG8];
            overlay = (UIView *)[cell viewWithTag:RIGHT_TAG9];
            //}
        }
        
        if([listOfContents count]>0)
        {
            NSDictionary *dictionary = [listOfContents objectAtIndex:indexPath.row];
            
            NSString *imgURL = [dictionary objectForKey:@"image"];
            
            __block UIActivityIndicatorView *activityIndicator;
            __weak UIImageView *weakImageView = imgViewMenu;
            /*[weakImageView setImageWithURL:[NSURL URLWithString:imgURL] placeholderImage:[UIImage imageNamed:@"No-Image-icon.png"] options:SDWebImageProgressiveDownload progress:^(NSInteger receivedSize, NSInteger expectedSize)
             {
                 if (!activityIndicator)
                 {
                     [weakImageView addSubview:activityIndicator = [UIActivityIndicatorView.alloc initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray]];
                     activityIndicator.center = weakImageView.center;
                     [activityIndicator startAnimating];
                 }
             }
                                 completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType)
             {
                 [activityIndicator removeFromSuperview];
                 activityIndicator = nil;
             }]; */
            dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0), ^ {
                [weakImageView sd_setImageWithURL:[NSURL URLWithString:imgURL]
                                 placeholderImage:[UIImage imageNamed:@"No-Image-icon.png"]
                                          options:SDWebImageProgressiveDownload
                                         progress:^(NSInteger receivedSize, NSInteger expectedSize, NSURL *targetURL) {
                                             dispatch_async(dispatch_get_main_queue(), ^ {
                                                 if (!activityIndicator) {
                                                     
                                                     [weakImageView addSubview:activityIndicator = [UIActivityIndicatorView.alloc initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray]];
                                                     activityIndicator.center = weakImageView.center;
                                                     [activityIndicator startAnimating];
                                                 }
                                             });
                                         }
                                        completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
                                            dispatch_async(dispatch_get_main_queue(), ^ {
                                                //[MBProgressHUD hideHUDForView:self.view animated:YES];
                                                [activityIndicator removeFromSuperview];
                                                activityIndicator = nil;
                                            });
                                        }];
            });
            
            imgViewMenu.contentMode  = UIViewContentModeScaleAspectFill;
            [imgViewMenu setClipsToBounds:YES];
            
            /*if ([list_template isEqualToString:@"4"]) {
                menutext.text = [NSString stringWithFormat:@"%@", [dictionary objectForKey:@"title"]];
                pricetitle.text =nil;
                price.text =nil;
                var1.text =nil;
                distance.text =nil;
            } else */if ([pluginValue2 isEqualToString:@"6"]) {
                [btnimgViewMenu setImage:[UIImage imageNamed:@"info.png"] forState:UIControlStateNormal];
                [btnimgViewMenu addTarget:self action:@selector(checkButtonTapped:event:) forControlEvents:UIControlEventTouchUpInside];
                //btnimgViewMenu.contentMode  = UIViewContentModeScaleAspectFill;
                //[btnimgViewMenu setClipsToBounds:YES];
                menutext.text = [NSString stringWithFormat:@"%@", [dictionary objectForKey:@"title"]];
                pricetitle.text =nil;
                price.text =nil;
                var1.text = [NSString stringWithFormat:@"%@", [dictionary objectForKey:@"var1"]];
                CLLocation *location1 = [[CLLocation alloc] initWithLatitude:[[dictionary objectForKey:@"latitude"] floatValue] longitude:[[dictionary objectForKey:@"longitude"] floatValue]];
                CLLocation *location2 = [[CLLocation alloc] initWithLatitude:locationManager.location.coordinate.latitude longitude:locationManager.location.coordinate.longitude];
                //NSLog(@"Distance i meters: %f", [location1 distanceFromLocation:location2]);
                //NSLog(@"Latitude :: %f", locationManager.location.coordinate.latitude);
                //NSLog(@"longitude :: %f", locationManager.location.coordinate.longitude);
                //NSLog(@"Latitude11 :: %f", [[dictionary objectForKey:@"latitude"] floatValue]);
                //NSLog(@"longitude11 :: %f", [[dictionary objectForKey:@"longitude"] floatValue]);
                __strong NSString *strdistance;
                if (locationManager.location.coordinate.latitude > 0 && locationManager.location.coordinate.longitude > 0 && [[dictionary objectForKey:@"latitude"] floatValue] > 0 && [[dictionary objectForKey:@"longitude"] floatValue] > 0) {
                    
                    float fdistance = [location1 distanceFromLocation:location2];
                    fdistance = fdistance / 1000;
                    if (fdistance > 9) {
                        NSNumberFormatter* numberFormatter = [[NSNumberFormatter alloc] init];
                        [numberFormatter setFormatterBehavior: NSNumberFormatterBehavior10_4];
                        [numberFormatter setNumberStyle: NSNumberFormatterDecimalStyle];
                        NSString *numberString = [numberFormatter stringFromNumber: [NSNumber numberWithInteger: fdistance]];
                        strdistance = [NSString stringWithFormat:@"%@ km", numberString];
                    } else if (fdistance > 1) {
                        strdistance = [NSString stringWithFormat:@"%.1f km", fdistance];
                    } else if (fdistance >= 0) {
                        strdistance = [NSString stringWithFormat:@"%.2f", fdistance];
                        float f = [strdistance floatValue] * 1000;
                        strdistance = [NSString stringWithFormat:@"%.0f m", f];
                    }
                    
                    strdistance = [NSString stringWithFormat:@"%@ away", strdistance];
                } else {
                    strdistance = @"";
                }
                
                distance.text = strdistance;
                desc.text = nil;
                datetime.text = nil;
                
            } else if ([pluginValue2 isEqualToString:@"9"]) {
                //NSLog(@"description :: %@", [dictionary objectForKey:@"description"]);
                menutext.text = [NSString stringWithFormat:@"%@", [dictionary objectForKey:@"title"]];
                desc.text = [NSString stringWithFormat:@"%@", [dictionary objectForKey:@"description"]];
                NSDateFormatter *inputFormatter = [[NSDateFormatter alloc] init];
                [inputFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
                NSDate *formatterDate = [inputFormatter dateFromString:[dictionary objectForKey:@"date_updated"]];
                NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
                [formatter setDateFormat:@"hh:mm:ss a"];
                NSDateFormatter *formatterdate = [[NSDateFormatter alloc] init];
                [formatterdate setDateFormat:@"dd/M/yy"];
                //NSLog(@"Current Date: %@", [formatter stringFromDate:formatterDate]);
                //[[dictionary objectForKey:@"date_updated"] substringToIndex:10]
                datetime.text = [NSString stringWithFormat:@"%@ %@", [formatterdate stringFromDate:formatterDate], [formatter stringFromDate:formatterDate]];
                distance.text = nil;
            } else {
                menutext.text = [NSString stringWithFormat:@"%@", [dictionary objectForKey:@"title"]];
                if ([[dictionary objectForKey:@"price"] floatValue]>0) {
                    pricetitle.text = [NSString stringWithFormat:@"%@", [dictionary objectForKey:@"price_title"]];
                    price.text = [NSString stringWithFormat:@"%@", [dictionary objectForKey:@"price"]];
                } else {
                    pricetitle.text =nil;
                    price.text =nil;
                }
                var1.text = [NSString stringWithFormat:@"%@", [dictionary objectForKey:@"var1"]];
                distance.text = nil;
                desc.text = nil;
                datetime.text = nil;
            }
            
        }
        return cell;
        imgViewMenu =nil;
        menutext =nil;
        pricetitle =nil;
        price =nil;
        var1 =nil;
        distance =nil;
        btnimgViewMenu =nil;
        datetime =nil;
        ImageBarView =nil;
        overlay =nil;
        desc =nil;
    }
    
    if (tableView == GridtableView)
    {
        if ([pluginValue2 isEqualToString:@"8"]) {
            static NSString *hlCellID = @"hlCellID";
            
            UITableViewCell *hlcell = [tableView dequeueReusableCellWithIdentifier:hlCellID];
            if(hlcell == nil)
            {
                hlcell =  [[UITableViewCell alloc]
                           initWithStyle:UITableViewCellStyleDefault reuseIdentifier:hlCellID];
                hlcell.accessoryType = UITableViewCellAccessoryNone;
                hlcell.selectionStyle = UITableViewCellSelectionStyleNone;
            }
            
            int section = indexPath.section;
            NSMutableArray *sectionItems = [sections objectAtIndex:section];
            //NSLog(@"sectionItems: %@", sectionItems);
            
            int n = [sectionItems count];
            int i=0,i1=0;
            
            //NSLog(@"cellForRowAtIndexPath");
            while(i<n)
            {
                int yy;
                yy = i1*self.view.frame.size.width/5*3;
                //yy = 5 +i1*(self.view.frame.size.width/2+20);
                int j=0;
                
                for(j=0; j<2;j++)
                {
                    if (i>=n)
                        break;
                    
                    Item *item = [sectionItems objectAtIndex:i];
                    
                    UIButton *button;
                    UIImageView *imgV;
                    if ([item.image length]>0 || [item.title length]>0)
                    {
                        
                        UIView *rectView=[[UIView alloc]initWithFrame:CGRectMake(0, 140, 320, 458)];
                        [rectView setBackgroundColor:[UIColor clearColor]];
                        [hlcell.contentView addSubview:rectView];
                        
                        CGRect rect = CGRectMake(0, 0, self.view.frame.size.width/2-10, self.view.frame.size.width/5*3);
                        button=[[UIButton alloc] initWithFrame:rect];
                        [button setFrame:rect];
                        
                        UIImageView *ImageBarView;
                        ImageBarView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 10, self.view.frame.size.width/2-10, 25)];
                        
                        UIView *overlay;
                        overlay = [[UIView alloc] initWithFrame:CGRectMake(0, 0, ImageBarView.frame.size.width, ImageBarView.frame.size.height)];
                        overlay.backgroundColor = [UIColor clearColor];
                        overlay.alpha = 0.6f;
                        
                        [ImageBarView addSubview:overlay];
                        
                        UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(5, 0, self.view.frame.size.width/2-20, 15)];
                        label.numberOfLines = 0;
                        label.lineBreakMode = NSLineBreakByWordWrapping;
                        UIFont *yourFont;
                        yourFont = [UIFont boldSystemFontOfSize:14];
                        label.font = yourFont;
                        label.text = item.title;
                        label.backgroundColor = [UIColor clearColor];
                        label.textColor = [self colorWithHexString:appdelegate.themeColorCode];
                        [label sizeToFit];
                        [label setTextAlignment:NSTextAlignmentCenter];
                        [ImageBarView addSubview:label];
                        
                        CGSize constrainedSize = CGSizeMake(label.frame.size.width  , 9999);
                        
                        NSDictionary *attributesDictionary = [NSDictionary dictionaryWithObjectsAndKeys:
                                                              yourFont, NSFontAttributeName,
                                                              nil];
                        
                        NSMutableAttributedString *string = [[NSMutableAttributedString alloc] initWithString:label.text attributes:attributesDictionary];
                        
                        CGRect requiredHeight = [string boundingRectWithSize:constrainedSize options:NSStringDrawingUsesLineFragmentOrigin context:nil];
                        
                        if (requiredHeight.size.width > label.frame.size.width) {
                            requiredHeight = CGRectMake(0,0, label.frame.size.width, requiredHeight.size.height);
                        } else {
                            requiredHeight = CGRectMake(0,0, label.frame.size.width, label.frame.size.height);
                        }
                        CGRect newFrame = label.frame;
                        newFrame.size.height = requiredHeight.size.height;
                        label.frame = newFrame;
                        ImageBarView.frame = CGRectMake(0, 10, self.view.frame.size.width/2-10, newFrame.size.height);
                        
                        overlay.frame = CGRectMake(0, 0, ImageBarView.frame.size.width, ImageBarView.frame.size.height);
                        
                        [rectView addSubview:ImageBarView];
                        
                        CGRect newFrame1;
                        CGRect newFrame2;
                        newFrame1 = CGRectMake(0, 0, 0, 0);
                        newFrame2 = CGRectMake(0, 0, 0, 0);
                        
                        //NSLog(@"itempic: %@", item.pic);
                        if ([item.image length]>0)
                        {
                            [button setFrame:CGRectMake(0, newFrame.size.height+10, self.view.frame.size.width/2-10, self.view.frame.size.width/5*3-newFrame.size.height-10)];
                            imgV = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, button.frame.size.width, button.frame.size.height)];
                            //[imgV setFrame:CGRectMake(0, 0, self.view.frame.size.width/2-10, self.view.frame.size.width/2-5)];
                            if ([item.image length]>0)
                            {
                                //[button setBackgroundImageWithURL:[NSURL URLWithString:item.image] forState:UIControlStateNormal placeholderImage:[UIImage imageNamed:@"No-Image-icon.png"]];
                                imgV = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, button.frame.size.width, button.frame.size.height)];
                                
                                [button addSubview:imgV];
                                
                                __block UIActivityIndicatorView *activityIndicator;
                                __weak UIImageView *weakImageView = imgV;
                                /*[weakImageView setImageWithURL:[NSURL URLWithString:item.image] placeholderImage:[UIImage imageNamed:@"No-Image-icon.png"] options:SDWebImageProgressiveDownload progress:^(NSInteger receivedSize, NSInteger expectedSize)
                                 {
                                     if (!activityIndicator)
                                     {
                                         [weakImageView addSubview:activityIndicator = [UIActivityIndicatorView.alloc initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray]];
                                         activityIndicator.center = weakImageView.center;
                                         [activityIndicator startAnimating];
                                     }
                                 }
                                                     completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType)
                                 {
                                     [activityIndicator removeFromSuperview];
                                     activityIndicator = nil;
                                 }]; */
                                //dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0), ^ {
                                    [weakImageView sd_setImageWithURL:[NSURL URLWithString:item.image]
                                                     placeholderImage:[UIImage imageNamed:@"No-Image-icon.png"]
                                                              options:SDWebImageProgressiveDownload
                                                             progress:^(NSInteger receivedSize, NSInteger expectedSize, NSURL *targetURL) {
                                                                 dispatch_async(dispatch_get_main_queue(), ^ {
                                                                     if (!activityIndicator) {
                                                                         
                                                                         [weakImageView addSubview:activityIndicator = [UIActivityIndicatorView.alloc initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray]];
                                                                         activityIndicator.center = weakImageView.center;
                                                                         [activityIndicator startAnimating];
                                                                     }
                                                                 });
                                                             }
                                                            completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
                                                                dispatch_async(dispatch_get_main_queue(), ^ {
                                                                    //[MBProgressHUD hideHUDForView:self.view animated:YES];
                                                                    [activityIndicator removeFromSuperview];
                                                                    activityIndicator = nil;
                                                                });
                                                            }];
                                //});
                                
                                imgV.contentMode  = UIViewContentModeScaleAspectFill;
                                [imgV setClipsToBounds:YES];
                            }
                            else
                            {
                                [button setTitle:@"No Image" forState:UIControlStateNormal];
                                [button setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
                                [button setBackgroundColor:[UIColor colorWithRed:230/255.0 green:230/255.0 blue:230/255.0 alpha:1]];
                            }
                            item.image =nil;
                            
                            //[button addSubview:imgV];
                            
                            /*__block UIActivityIndicatorView *activityIndicator;
                             __weak UIImageView *weakImageView = imgV;
                             [weakImageView setImageWithURL:[NSURL URLWithString:item.pic] placeholderImage:[UIImage imageNamed:@"page.png"] options:SDWebImageProgressiveDownload progress:^(NSInteger receivedSize, NSInteger expectedSize)
                             {
                             if (!activityIndicator)
                             {
                             [weakImageView addSubview:activityIndicator = [UIActivityIndicatorView.alloc initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray]];
                             activityIndicator.center = weakImageView.center;
                             [activityIndicator startAnimating];
                             }
                             }
                             completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType)
                             {
                             [activityIndicator removeFromSuperview];
                             activityIndicator = nil;
                             }];
                             
                             imgV.contentMode  = UIViewContentModeScaleAspectFill;
                             [imgV setClipsToBounds:YES]; */
                        }
                        else
                        {
                            [button setTitle:@"No Image" forState:UIControlStateNormal];
                            [button setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
                            [button setBackgroundColor:[UIColor colorWithRed:230/255.0 green:230/255.0 blue:230/255.0 alpha:1]];
                        }
                        //item.pic =nil;
                        
                        [button setContentMode:UIViewContentModeCenter];
                        
                        NSString *tagValue = [NSString stringWithFormat:@"%d%d", indexPath.section+1, i];
                        button.tag = [tagValue intValue];
                        
                        [button addTarget:self action:@selector(buttonPressed:) forControlEvents:UIControlEventTouchUpInside];
                        [rectView addSubview:button];
                        
                        //rectView.frame = CGRectMake(5+self.view.frame.size.width/2*j, yy, self.view.frame.size.width/2-10, self.view.frame.size.width/4*3-5);
                        rectView.frame = CGRectMake(5+self.view.frame.size.width/2*j, yy, self.view.frame.size.width/2-10, self.view.frame.size.width/5*3);
                    }
                    
                    i++;
                }
                i1 = i1+1;
            }
                
            return hlcell;
        } else {
            static NSString *hlCellID = @"hlCellID";
            
            UITableViewCell *hlcell = [tableView dequeueReusableCellWithIdentifier:hlCellID];
            if(hlcell == nil)
            {
                hlcell =  [[UITableViewCell alloc]
                           initWithStyle:UITableViewCellStyleDefault reuseIdentifier:hlCellID];
                hlcell.accessoryType = UITableViewCellAccessoryNone;
                hlcell.selectionStyle = UITableViewCellSelectionStyleNone;
            }
            
            int section = indexPath.section;
            NSMutableArray *sectionItems = [sections objectAtIndex:section];
            
            int n = [sectionItems count];
            int i=0,i1=0;
            
            while(i<n)
            {
                int yy;
                if ([pluginValue2 isEqualToString:@"4"]) {
                    yy = 5 +i1*self.view.frame.size.width/4*3;
                } else {
                    yy = 5 +i1*self.view.frame.size.width/2;
                }
                int j=0;
                
                for(j=0; j<2;j++)
                {
                    if (i>=n)
                        break;
                    
                    Item *item = [sectionItems objectAtIndex:i];
                    
                    UIButton *button;
                    UIImageView *imgV;
                    if ([item.image length]>0 || [item.title length]>0)
                    {
                        
                        UIView *rectView=[[UIView alloc]initWithFrame:CGRectMake(0, 50, 320, 430)];
                        [rectView setBackgroundColor:[UIColor clearColor]];
                        rectView.layer.borderColor = [UIColor grayColor].CGColor;
                        rectView.layer.borderWidth = 0.5f;
                        [hlcell.contentView addSubview:rectView];
                        
                        CGRect rect = CGRectMake(0, 0, self.view.frame.size.width/2-10, self.view.frame.size.width/4*3);
                        button=[[UIButton alloc] initWithFrame:rect];
                        [button setFrame:rect];
                        //[[button layer] setBorderWidth:1.0f];
                        //[[button layer] setBorderColor:[UIColor grayColor].CGColor];
                        
                        if ([item.image length]>0)
                        {
                            //[button setBackgroundImageWithURL:[NSURL URLWithString:item.image] forState:UIControlStateNormal placeholderImage:[UIImage imageNamed:@"No-Image-icon.png"]];
                            imgV = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, button.frame.size.width, button.frame.size.height)];
                            
                            [button addSubview:imgV];
                            
                            __block UIActivityIndicatorView *activityIndicator;
                            __weak UIImageView *weakImageView = imgV;
                            /*[weakImageView setImageWithURL:[NSURL URLWithString:item.image] placeholderImage:[UIImage imageNamed:@"No-Image-icon.png"] options:SDWebImageProgressiveDownload progress:^(NSInteger receivedSize, NSInteger expectedSize)
                             {
                                 if (!activityIndicator)
                                 {
                                     [weakImageView addSubview:activityIndicator = [UIActivityIndicatorView.alloc initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray]];
                                     activityIndicator.center = weakImageView.center;
                                     [activityIndicator startAnimating];
                                 }
                             }
                                                 completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType)
                             {
                                 [activityIndicator removeFromSuperview];
                                 activityIndicator = nil;
                             }]; */
                            //dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0), ^ {
                                [weakImageView sd_setImageWithURL:[NSURL URLWithString:item.image]
                                                 placeholderImage:[UIImage imageNamed:@"No-Image-icon.png"]
                                                          options:SDWebImageProgressiveDownload
                                                         progress:^(NSInteger receivedSize, NSInteger expectedSize, NSURL *targetURL) {
                                                             dispatch_async(dispatch_get_main_queue(), ^ {
                                                                 if (!activityIndicator) {
                                                                     
                                                                     [weakImageView addSubview:activityIndicator = [UIActivityIndicatorView.alloc initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray]];
                                                                     activityIndicator.center = weakImageView.center;
                                                                     [activityIndicator startAnimating];
                                                                 }
                                                             });
                                                         }
                                                        completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
                                                            dispatch_async(dispatch_get_main_queue(), ^ {
                                                                //[MBProgressHUD hideHUDForView:self.view animated:YES];
                                                                [activityIndicator removeFromSuperview];
                                                                activityIndicator = nil;
                                                            });
                                                        }];
                            //});
                            
                            imgV.contentMode  = UIViewContentModeScaleAspectFill;
                            [imgV setClipsToBounds:YES];
                        }
                        else
                        {
                            [button setTitle:@"No Image" forState:UIControlStateNormal];
                            [button setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
                            [button setBackgroundColor:[UIColor colorWithRed:230/255.0 green:230/255.0 blue:230/255.0 alpha:1]];
                        }
                        item.image =nil;
                        
                        [button setContentMode:UIViewContentModeCenter];
                        
                        NSString *tagValue = [NSString stringWithFormat:@"%d%d", indexPath.section+1, i];
                        button.tag = [tagValue intValue];
                        
                        [button addTarget:self action:@selector(buttonPressed:) forControlEvents:UIControlEventTouchUpInside];
                        [rectView addSubview:button];
                        
                        UIImageView *ImageBarView;
                        if ([pluginValue2 isEqualToString:@"4"]) {
                            ImageBarView = [[UIImageView alloc] initWithFrame:CGRectMake((self.view.frame.size.width/2-10*j)+5, self.view.frame.size.width/4*3-10, self.view.frame.size.width/2-10, 25)];
                        } else {
                            ImageBarView = [[UIImageView alloc] initWithFrame:CGRectMake((self.view.frame.size.width/2-10*j)+5, self.view.frame.size.width/2-5, self.view.frame.size.width/2-10, 25)];
                        }
                        
                        UIView *overlay;
                        overlay = [[UIView alloc] initWithFrame:CGRectMake(0, 0, ImageBarView.frame.size.width, ImageBarView.frame.size.height)];
                        if ([pluginValue2 isEqualToString:@"4"]) {
                            overlay.backgroundColor = [UIColor whiteColor];
                        } else {
                            overlay.backgroundColor = [self colorWithHexString:appdelegate.themeColorCode];
                        }
                        overlay.alpha = 0.6f;
                        //overlay.opaque = YES;
                        
                        [ImageBarView addSubview:overlay];
                        
                        UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(5, 0, self.view.frame.size.width/2-20, 15)];
                        label.numberOfLines = 0;
                        label.lineBreakMode = NSLineBreakByWordWrapping;
                        UIFont *yourFont;
                        if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone) {
                            // For iPhone
                            yourFont = [UIFont fontWithName:@"Helvetica-Bold" size:14];
                        } else {
                            yourFont = [UIFont fontWithName:@"Helvetica-Bold" size:20];
                        }
                        label.font = yourFont;
                        label.text = item.title;
                        label.backgroundColor = [UIColor clearColor];
                        if ([pluginValue2 isEqualToString:@"4"]) {
                            label.textColor = [self colorWithHexString:appdelegate.themeColorCode];
                            label.shadowColor = [UIColor blackColor];
                            label.shadowOffset = CGSizeMake(0, 1);
                        } else {
                            label.textColor = [UIColor whiteColor];
                        }
                        [label sizeToFit];
                        [ImageBarView addSubview:label];
                        
                        CGSize constrainedSize = CGSizeMake(label.frame.size.width  , 9999);
                        
                        NSDictionary *attributesDictionary = [NSDictionary dictionaryWithObjectsAndKeys:
                                                              yourFont, NSFontAttributeName,
                                                              nil];
                        
                        NSMutableAttributedString *string = [[NSMutableAttributedString alloc] initWithString:label.text attributes:attributesDictionary];
                        
                        CGRect requiredHeight = [string boundingRectWithSize:constrainedSize options:NSStringDrawingUsesLineFragmentOrigin context:nil];
                        
                        if (requiredHeight.size.width > label.frame.size.width) {
                            requiredHeight = CGRectMake(0,0, label.frame.size.width, requiredHeight.size.height);
                        }
                        CGRect newFrame = label.frame;
                        newFrame.size.height = requiredHeight.size.height;
                        label.frame = newFrame;
                        if ([pluginValue2 isEqualToString:@"4"]) {
                            ImageBarView.frame = CGRectMake(0, self.view.frame.size.width/4*3-newFrame.size.height-10, self.view.frame.size.width/2-10, newFrame.size.height);
                        } else {
                            ImageBarView.frame = CGRectMake(0, self.view.frame.size.width/2-newFrame.size.height-5, self.view.frame.size.width/2-10, newFrame.size.height);
                        }
                        overlay.frame = CGRectMake(0, 0, ImageBarView.frame.size.width, ImageBarView.frame.size.height);
                        
                        CGRect newFrame1;
                        CGRect newFrame2;
                        newFrame1 = CGRectMake(0, 0, 0, 0);
                        newFrame2 = CGRectMake(0, 0, 0, 0);
                        if ([pluginValue2 isEqualToString:@"4"]) {
                            if ([item.price floatValue]>0) {
                                UILabel *label1 = [[UILabel alloc] initWithFrame:CGRectMake(5, newFrame.size.height, self.view.frame.size.width/2-20, 15)];
                                label1.numberOfLines = 0;
                                label1.lineBreakMode = NSLineBreakByWordWrapping;
                                UIFont *yourFont1;
                                if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone) {
                                    // For iPhone
                                    yourFont1 = [UIFont fontWithName:@"Helvetica" size:12];
                                } else {
                                    yourFont1 = [UIFont fontWithName:@"Helvetica" size:18];
                                }
                                label1.font = yourFont1;
                                label1.text = [NSString stringWithFormat:@"%@ %.2f", item.pricetitle, [item.price floatValue]];
                                label1.backgroundColor = [UIColor clearColor];
                                label1.textColor = [UIColor blackColor];
                                [label1 sizeToFit];
                                //label1.shadowColor = [UIColor blackColor];
                                //label1.shadowOffset = CGSizeMake(0, 1);
                                [ImageBarView addSubview:label1];
                                
                                CGSize constrainedSize1 = CGSizeMake(label1.frame.size.width  , 9999);
                                
                                NSDictionary *attributesDictionary1 = [NSDictionary dictionaryWithObjectsAndKeys:
                                                                       yourFont1, NSFontAttributeName,
                                                                       nil];
                                
                                NSMutableAttributedString *string1 = [[NSMutableAttributedString alloc] initWithString:label1.text attributes:attributesDictionary1];
                                
                                CGRect requiredHeight1 = [string1 boundingRectWithSize:constrainedSize1 options:NSStringDrawingUsesLineFragmentOrigin context:nil];
                                
                                if (requiredHeight1.size.width > label1.frame.size.width) {
                                    requiredHeight1 = CGRectMake(0,0, label1.frame.size.width, requiredHeight1.size.height);
                                }
                                newFrame1 = label1.frame;
                                newFrame1.size.height = requiredHeight1.size.height;
                                label1.frame = newFrame1;
                                //if ([list_template isEqualToString:@"4"]) {
                                ImageBarView.frame = CGRectMake(0, self.view.frame.size.width/4*3-newFrame.size.height-newFrame1.size.height-10, self.view.frame.size.width/2-10, newFrame.size.height+newFrame1.size.height);
                                //} else {
                                //ImageBarView.frame = CGRectMake(0, self.view.frame.size.width/2-newFrame.size.height-5, self.view.frame.size.width/2-10, newFrame.size.height);
                                //}
                                overlay.frame = CGRectMake(0, 0, ImageBarView.frame.size.width, ImageBarView.frame.size.height);
                            }
                            
                            if ([item.var1 length]>0) {
                                UILabel *label2 = [[UILabel alloc] initWithFrame:CGRectMake(5, newFrame.size.height+newFrame1.size.height, self.view.frame.size.width/2-20, 15)];
                                label2.numberOfLines = 0;
                                label2.lineBreakMode = NSLineBreakByWordWrapping;
                                UIFont *yourFont2;
                                if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone) {
                                    // For iPhone
                                    yourFont2 = [UIFont fontWithName:@"Helvetica" size:12];
                                } else {
                                    yourFont2 = [UIFont fontWithName:@"Helvetica" size:18];
                                }
                                label2.font = yourFont2;
                                label2.text = item.var1;
                                label2.backgroundColor = [UIColor clearColor];
                                label2.textColor = [UIColor blackColor];
                                [label2 sizeToFit];
                                //label2.shadowColor = [UIColor blackColor];
                                //label2.shadowOffset = CGSizeMake(0, 1);
                                [ImageBarView addSubview:label2];
                                
                                CGSize constrainedSize2 = CGSizeMake(label2.frame.size.width  , 9999);
                                
                                NSDictionary *attributesDictionary2 = [NSDictionary dictionaryWithObjectsAndKeys:
                                                                       yourFont2, NSFontAttributeName,
                                                                       nil];
                                
                                NSMutableAttributedString *string2 = [[NSMutableAttributedString alloc] initWithString:label2.text attributes:attributesDictionary2];
                                
                                CGRect requiredHeight2 = [string2 boundingRectWithSize:constrainedSize2 options:NSStringDrawingUsesLineFragmentOrigin context:nil];
                                
                                if (requiredHeight2.size.width > label2.frame.size.width) {
                                    requiredHeight2 = CGRectMake(0,0, label2.frame.size.width, requiredHeight2.size.height);
                                }
                                newFrame2 = label2.frame;
                                newFrame2.size.height = requiredHeight2.size.height;
                                label2.frame = newFrame2;
                                //if ([list_template isEqualToString:@"4"]) {
                                ImageBarView.frame = CGRectMake(0, self.view.frame.size.width/4*3-newFrame.size.height-newFrame1.size.height-newFrame2.size.height-10, self.view.frame.size.width/2-10, newFrame.size.height+newFrame1.size.height+newFrame2.size.height);
                                //} else {
                                //ImageBarView.frame = CGRectMake(0, self.view.frame.size.width/2-newFrame.size.height-5, self.view.frame.size.width/2-10, newFrame.size.height);
                                //}
                                overlay.frame = CGRectMake(0, 0, ImageBarView.frame.size.width, ImageBarView.frame.size.height);
                            }
                            
                            [imgV setFrame:CGRectMake(0, 0, self.view.frame.size.width/2-10, self.view.frame.size.width/4*3-newFrame.size.height-newFrame1.size.height-newFrame2.size.height-10)];
                            [button setFrame:CGRectMake(0, 0, self.view.frame.size.width/2-10, self.view.frame.size.width/4*3-newFrame.size.height-newFrame1.size.height-newFrame2.size.height-10)];
                        } else {
                            [imgV setFrame:CGRectMake(0, 0, self.view.frame.size.width/2-10, self.view.frame.size.width/2-5)];
                            [button setFrame:CGRectMake(0, 0, self.view.frame.size.width/2-10, self.view.frame.size.width/2-5)];
                        }
                        
                        [rectView addSubview:ImageBarView];
                        
                        if ([pluginValue2 isEqualToString:@"4"]) {
                            rectView.frame = CGRectMake(5+self.view.frame.size.width/2*j, yy, self.view.frame.size.width/2-10, self.view.frame.size.width/4*3-5);
                        } else {
                            rectView.frame = CGRectMake(5+self.view.frame.size.width/2*j, yy, self.view.frame.size.width/2-10, self.view.frame.size.width/2-5);
                        }
                    }
                    
                    i++;
                }
                i1 = i1+1;
            }
            return hlcell;
        }
    }
    
    return nil;
}

- (void)checkButtonTapped:(id)sender event:(id)event
{
    
    NSSet *touches = [event allTouches];
    UITouch *touch = [touches anyObject];
    CGPoint currentTouchPosition = [touch locationInView:MenutableView];
    NSIndexPath *indexPath = [MenutableView indexPathForRowAtPoint: currentTouchPosition];
    if (indexPath != nil)
    {
        [self getContentdetailsAtIndex:indexPath.row];
        
    }
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
	if (tableView == MenutableView)
    {
        
        [self getContentdetailsAtIndex:indexPath.row];
        
        
    }
    
}

- (void) getContentdetailsAtIndex:(NSUInteger)idx {
    NSDictionary *dictionary = [listOfContents objectAtIndex:idx];
    
    appdelegate.plugin_id = [dictionary objectForKey:@"topid"];
    cont_id = [dictionary objectForKey:@"id"];
    cont_title = [dictionary objectForKey:@"title"];
    
    if (appdelegate.showloading) {
        alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"loading", nil) message:nil delegate:self cancelButtonTitle:nil otherButtonTitles: nil];
        
        //a simple activity indicator:
        UIActivityIndicatorView *activityIndicator = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
        activityIndicator.frame= CGRectMake(50, 10, 37, 37);
        activityIndicator.autoresizingMask = UIViewAutoresizingFlexibleTopMargin | UIViewAutoresizingFlexibleRightMargin | UIViewAutoresizingFlexibleBottomMargin | UIViewAutoresizingFlexibleLeftMargin;
        [activityIndicator startAnimating];
        
        //the magic line below,
        //we associate the activity indicator to the alert view: (addSubview is not used)
        [alert setValue:activityIndicator forKey:@"accessoryView"];
        
        [alert show];
    
        // Adjust the indicator so it is up a few pixels from the bottom of the alert
        [UIView beginAnimations:nil context:nil];
        [UIView setAnimationDuration:0.0];
        [UIView setAnimationDelay:0.0];
        [UIView setAnimationTransition:UIViewAnimationTransitionNone forView:alert cache:YES];
        [UIView setAnimationDelegate:self];
        [UIView setAnimationDidStopSelector:@selector(startupAnimationDoneIndicator:finished:context:)];
        alert.alpha = 0.5;
        [UIView commitAnimations];
        
    } else {
        [self goNextPage];
    }
}

-(IBAction)buttonPressed:(id)sender {
    
    int tagId = [sender tag];
    int divNum = 0;
    if(tagId<100)
        divNum=10;
    else
        divNum=100;
    int section = [sender tag]/divNum;
    section -=1; // we had incremented at tag assigning time
    int itemId = [sender tag]%divNum;
    
    NSMutableArray *sectionItems = [sections objectAtIndex:section];
    Item *item = [sectionItems objectAtIndex:itemId];
    
    appdelegate.plugin_id = item.topid;
    cont_id = item.contid;
    cont_title = item.contname;
    
    if (appdelegate.showloading) {
        //NSLog(@"cont_id:: %@", cont_id);
        alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"loading", nil) message:nil delegate:self cancelButtonTitle:nil otherButtonTitles: nil];
        
        //a simple activity indicator:
        UIActivityIndicatorView *activityIndicator = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
        activityIndicator.frame= CGRectMake(50, 10, 37, 37);
        activityIndicator.autoresizingMask = UIViewAutoresizingFlexibleTopMargin | UIViewAutoresizingFlexibleRightMargin | UIViewAutoresizingFlexibleBottomMargin | UIViewAutoresizingFlexibleLeftMargin;
        [activityIndicator startAnimating];
        
        //the magic line below,
        //we associate the activity indicator to the alert view: (addSubview is not used)
        [alert setValue:activityIndicator forKey:@"accessoryView"];
        
        [alert show];
    
        // Adjust the indicator so it is up a few pixels from the bottom of the alert
        [UIView beginAnimations:nil context:nil];
        [UIView setAnimationDuration:0.0];
        [UIView setAnimationDelay:0.0];
        [UIView setAnimationTransition:UIViewAnimationTransitionNone forView:alert cache:YES];
        [UIView setAnimationDelegate:self];
        [UIView setAnimationDidStopSelector:@selector(startupAnimationDoneIndicator:finished:context:)];
        alert.alpha = 0.5;
        [UIView commitAnimations];
    } else {
        [self goNextPage];
    }
    
}

-(void) dissmissIndicator {
    [alert dismissWithClickedButtonIndex:0 animated:NO];
}

-(void) goNextPage {
    ContentPluginViewController *contentPluginViewController = [[ContentPluginViewController alloc] init];
    [appdelegate.navController pushViewController:contentPluginViewController animated:NO];
}

- (void)startupAnimationDoneIndicator:(NSString *)animationID finished:(NSNumber *)finished context:(void *)context
{
    [self goNextPage];
    [self dissmissIndicator];
    alert =nil;
}

-(BOOL) textFieldShouldReturn:(UITextField *)textField{
    [textField resignFirstResponder];
    return YES;
}

/*** Push Notification 27Aug2015 ***/
-(void) viewDidDisappear:(BOOL)animated {
    
    appdelegate.notify_actiontype = @"";
    appdelegate.notify_actionvalue = @"";
    appdelegate.notify_contentid = @"";
    
    appdelegate.homeclicked = NO;
    [modalController dismissViewControllerAnimated:YES completion:nil];
    
    [super viewDidDisappear:animated];
    
}
/*** Push Notification 27Aug2015 ***/

@end
