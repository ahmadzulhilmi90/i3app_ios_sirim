//
//  ContentPluginDetailsViewController.h
//  standardmobileappsv3
//
//  Created by M3Online on 5/26/14.
//  Copyright (c) 2014 M3Online. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MessageUI/MessageUI.h>
#import <Foundation/Foundation.h>
#import <SDWebImage/UIButton+WebCache.h>
#import <SDWebImage/UIImageView+WebCache.h>

@interface ContentPluginDetailsViewController : UIViewController <UITableViewDelegate, UITableViewDataSource, MFMailComposeViewControllerDelegate, UIViewControllerTransitioningDelegate> {
    NSString *contId;
    NSString *conttitle;
    NSString *listtemplate;
    NSString *contvar1;
    NSString *menutitle;
    
    //NSMutableData* receivedData;
    NSString *currentRequest;
    NSMutableArray *listOfContent;
    NSDictionary *dicContent;
    UIScrollView *scrollview;
    
    NSMutableArray *MenuItems;
    
    NSMutableArray *galleryImages_;
    NSDictionary *dicImages;
    
    NSMutableArray *listOfContentsFav;
    NSMutableArray *listOfContentsComments;
    NSDictionary *dicContentsFav;
    NSDictionary *dicContentsComments;
    
    NSMutableArray *listOfFav;
    NSDictionary *dicFav;
    
    UIView *view_popoverMain;
    UIView *view_plain;
    UIButton *buttonclose;
    UIButton *buttonlogin;
    UILabel *lbllogin;
}

@property (nonatomic, strong) NSString *contId;
@property (nonatomic, strong) NSString *conttitle;
@property (nonatomic, strong) NSString *listtemplate;
@property (nonatomic, strong) NSString *contvar1;
@property (nonatomic, strong) NSString *menutitle;

//@property (nonatomic, strong) NSMutableData *receivedData;
@property (nonatomic, strong) NSString *currentRequest;
@property (nonatomic, strong) NSMutableArray *listOfContent;
@property (nonatomic, strong) UIScrollView *scrollview;

@property(nonatomic, strong)NSMutableArray *galleryImages; //Array holding the image file paths

@property (nonatomic, strong) UIWebView *webView;
@property (nonatomic, strong) UIWebView *wv;

@property (nonatomic, strong) NSMutableArray *listOfContentsFav;
@property (nonatomic, strong) NSMutableArray *listOfContentsComments;

@property (nonatomic, strong) NSMutableArray *listOfFav;

@property (nonatomic, strong) UIView *view_popoverMain;
@property (nonatomic, strong) UIButton *buttonclose;
@property (nonatomic, strong) UIButton *buttonlogin;
@property (nonatomic, strong) UILabel *lbllogin;
@property (nonatomic, strong) UIView *view_plain;

- (void)setupScrollView:(UIScrollView*)scrMain ;


@end
