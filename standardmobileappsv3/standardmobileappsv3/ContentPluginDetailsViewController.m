//
//  ViewControllerPlugin1.m
//  standardmobileappsv3
//
//  Created by M3Online on 5/26/14.
//  Copyright (c) 2014 M3Online. All rights reserved.
//

#import "ContentPluginDetailsViewController.h"
#import "AppDelegate.h"
#import "NSString+HTML.h"
#import "ItemCell.h"
//#import "ImgCache.h"
#import "ContentPluginDetailsViewController1.h"
#import "RootViewController.h"
#import "ViewController.h"
#import "CommentsViewController.h"
#import "SBJson.h"
#import "InitialSlidingViewController.h"
#import "BTSlideInteractor.h"
#import "Reachability.h"

NSString *cont_value1;
NSString *cont_value2;
NSString *cont_type;
NSString *cont_title;

@interface ContentPluginDetailsViewController ()
@property (strong, nonatomic) BTSlideInteractor *interactor;
@end

@implementation ContentPluginDetailsViewController {
    UITableView *tableView;
    UIAlertView *alert;
    UIActivityIndicatorView *indicator;
}

@synthesize contId, scrollview, view_popoverMain, buttonclose, buttonlogin, lbllogin, view_plain;
@synthesize /*receivedData, */currentRequest, listOfContent, conttitle, listtemplate, contvar1, menutitle, webView, wv, listOfContentsFav, listOfContentsComments, listOfFav;
@synthesize galleryImages = galleryImages_;

float delaytime = 5;

InitialSlidingViewController *modalController;

AppDelegate *appdelegate;

- (UIColor *) colorWithHexString:(NSString *)hexString {
    NSString *colorString = [[hexString stringByReplacingOccurrencesOfString: @"#" withString: @""] uppercaseString];
    CGFloat alpha, red, blue, green;
    alpha = 1.0f;
    red   = [self colorComponentFrom: colorString start: 0 length: 2];
    green = [self colorComponentFrom: colorString start: 2 length: 2];
    blue  = [self colorComponentFrom: colorString start: 4 length: 2];
    return [UIColor colorWithRed: red green: green blue: blue alpha: alpha];
}

- (CGFloat) colorComponentFrom:(NSString *)string start:(NSUInteger)start length:(NSUInteger)length {
    NSString *substring = [string substringWithRange: NSMakeRange(start, length)];
    NSString *fullHex = length == 2 ? substring : [NSString stringWithFormat: @"%@%@", substring, substring];
    unsigned hexComponent;
    [[NSScanner scannerWithString: fullHex] scanHexInt: &hexComponent];
    return hexComponent / 255.0;
}


- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        listOfContentsFav = [[NSMutableArray alloc] init];
        listOfContentsComments = [[NSMutableArray alloc] init];
        listOfFav = [[NSMutableArray alloc] init];
    }
    return self;
}

- (IBAction)closePopover:(id)sender
{
    [view_popoverMain removeFromSuperview];
    //[view_popoverMain setHidden:NO];
}

- (IBAction)logIn:(id)sender
{
    NSString *nibFileName;
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone) {
        nibFileName = @"SettingViewController";
    } else {
        nibFileName = @"SettingViewController_ipad";
    }
    SettingViewController *s = [[SettingViewController alloc] initWithNibName:nibFileName bundle:nil];
    appdelegate.shouldReturnToPrevPage = YES;
    //LoyaltyViewController *l = [[LoyaltyViewController alloc] initWithNibName:@"LoyaltyViewController" bundle:nil];
    appdelegate.originVC = self;
    [appdelegate.navController pushViewController:s animated:NO];
    
    [view_popoverMain removeFromSuperview];
}


- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    appdelegate.homeclicked = NO;
    
    self.view.backgroundColor = [UIColor whiteColor];
    appdelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    //self.title = appdelegate.iconname;
    self.title = conttitle;
    
    view_popoverMain=[[UIView alloc]initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height)];
    UIColor *background = [[UIColor alloc] initWithPatternImage:[UIImage imageNamed: @"behind_alert_view.png"]];
    [view_popoverMain setBackgroundColor:background];
    //view_popoverMain.alpha = 0.5;
    //view_popoverMain.backgroundColor = [UIColor colorWithWhite:0.6 alpha:0];
    //[self.view addSubview:view_popoverMain];
    //[view_popoverMain setHidden:YES];
    view_plain=[[UIView alloc]initWithFrame:CGRectMake(self.view.frame.size.width/2-140, self.view.frame.size.height/2-130, 280, 220)];
    [view_plain setBackgroundColor:[UIColor whiteColor]];
    
    lbllogin = [[UILabel alloc]initWithFrame:CGRectMake(view_plain.frame.size.width/2-120,view_plain.frame.size.height/2-60, 240, 40)];
    lbllogin.text = @"Please login to your account";
    lbllogin.backgroundColor = [UIColor clearColor];
    lbllogin.textColor = [UIColor blackColor];
    lbllogin.textAlignment = NSTextAlignmentCenter;
    
    buttonclose = [UIButton buttonWithType:UIButtonTypeCustom];
    [buttonclose addTarget:self
                    action:@selector(closePopover:)
          forControlEvents:UIControlEventTouchUpInside];
    //[buttonclose setTitle:@"close" forState:UIControlStateNormal];
    [buttonclose setBackgroundImage:[UIImage imageNamed:@"btn_close_x.png"] forState:UIControlStateNormal];
    buttonclose.frame = CGRectMake(view_plain.frame.size.width-60, 0, 50.0, 50.0);
    //[view_popoverMain addSubview:buttonclose];
    
    buttonlogin = [UIButton buttonWithType:UIButtonTypeCustom];
    [buttonlogin addTarget:self
                    action:@selector(logIn:)
          forControlEvents:UIControlEventTouchUpInside];
    [buttonlogin setTitle:@"Login" forState:UIControlStateNormal];
    [buttonlogin setBackgroundColor:[UIColor grayColor]];
    [buttonlogin setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    buttonlogin.frame = CGRectMake(view_plain.frame.size.width/2-50, view_plain.frame.size.height/2, 100, 40.0);
    
    self.navigationItem.backBarButtonItem = [[UIBarButtonItem alloc]initWithTitle:@"" style:UIBarButtonItemStylePlain target:nil action:nil];
    
    CGRect rect = CGRectMake(0,0,20,20);
    UIGraphicsBeginImageContext( rect.size );
    [[UIImage imageNamed:@"home.png"] drawInRect:rect];
    UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    UIBarButtonItem *myHomeButton = [[UIBarButtonItem alloc] initWithImage:image style:UIBarButtonItemStyleDone target:self action:@selector(myHomeAction:)];
    
    
    NSArray *myButtonArray = [[NSArray alloc] initWithObjects:myHomeButton, nil];
    
    self.navigationItem.rightBarButtonItems = myButtonArray;
}

-(void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    if ([self isConnectionAvailable]) {
        NSString *urlAddress = [[NSString alloc] initWithFormat:@"%@favorite_counter.php?db=%@&userid=%@&udid=%@&cuid=%@&top=%@", appdelegate.user_apipath, [self urlEncode:appdelegate.db], [self urlEncode:appdelegate.user], [self urlEncode:appdelegate.uuid], [self urlEncode:appdelegate.customer_id], appdelegate.plugin_id];
        currentRequest = @"counterFav";
        //NSLog(@"appdelegate.user_apipath:: %@", appdelegate.user_apipath);
        //NSLog(@"urlAddress:: %@", urlAddress);
        [self connectToServer:urlAddress];
    } else {
        [self processdata];
    }
}

//Slide Menu
- (void)willAnimateRotationToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation duration:(NSTimeInterval)duration
{
    //NSLog(@"view frame=%@ bounds=%@", NSStringFromCGRect(self.view.frame), NSStringFromCGRect(self.view.bounds));
}


- (void)showModalButtonWasTouched
{
    self.interactor = [[BTSlideInteractor alloc] init];
    self.interactor.presenting = YES;
    
    //BTModalViewController *modalController = [self.storyboard instantiateViewControllerWithIdentifier:@"ModalViewController"];
    modalController = [[InitialSlidingViewController alloc] init];
    modalController.modalPresentationStyle = UIModalPresentationCustom;
    modalController.transitioningDelegate = self.interactor;
    [self.navigationController presentViewController:modalController animated:YES completion:nil];
    //[self.navigationController popToViewController:modalController animated:NO];
    //[self.navigationController pushViewController:modalController animated:YES];
}
// End Slide Menu

-(void)myHomeAction:(UIBarButtonItem *)sender{
    
    appdelegate.nextPage = @"";
    
    if ([appdelegate.listOfVerMenu count]>0) {
        if (appdelegate.homeclicked==NO) {
            appdelegate.homeclicked = YES;
            [self showModalButtonWasTouched];
        } else {
            appdelegate.homeclicked = NO;
            [modalController dismissViewControllerAnimated:YES completion:nil];
        }
    } else {
        //AppDelegate *appdelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
        //NSLog(@"View Controllers : %@",[appdelegate.navController viewControllers]);
        NSMutableArray *navigationArray = [[NSMutableArray alloc] initWithArray: appdelegate.navController.viewControllers];
        
        for (UIViewController* viewController in appdelegate.navController.viewControllers) {
            
            //This if condition checks whether the viewController's class is MyGroupViewController
            // if true that means its the MyGroupViewController (which has been pushed at some point)
            if ([viewController isKindOfClass:[RootViewController class]] ) {
                
                // Here viewController is a reference of UIViewController base class of MyGroupViewController
                // but viewController holds MyGroupViewController  object so we can type cast it here
                //ThemeViewController *themeViewController = (ThemeViewController*)viewController;
                //[self.navigationController popToViewController:themeViewController animated:YES];
                [navigationArray removeObjectAtIndex: 0];
            }
        }
        appdelegate.navController.viewControllers = navigationArray;
        for (UIViewController* viewController in appdelegate.navController.viewControllers) {
            
            //This if condition checks whether the viewController's class is MyGroupViewController
            // if true that means its the MyGroupViewController (which has been pushed at some point)
            if ([viewController isKindOfClass:[ViewController class]] ) {
                
                // Here viewController is a reference of UIViewController base class of MyGroupViewController
                // but viewController holds MyGroupViewController  object so we can type cast it here
                ViewController *themeViewController = (ViewController*)viewController;
                [appdelegate.navController popToViewController:themeViewController animated:NO];
                //[self.navigationController popToViewController:viewController animated:YES];
            }
        }
        //NSLog(@"View Controllers111 : %@",appdelegate.navController.viewControllers);
    }
    
}

- (void) processdata {
    sqlite3 *database;
    
    if(sqlite3_open([appdelegate.databasePath UTF8String], &database) == SQLITE_OK) {
        NSString *theme_object = [[NSString alloc] initWithFormat:@"SELECT * from contents WHERE id = %@", contId];
        //NSLog(@"theme_object Plugin 1:: %@", theme_object);
        
        NSString *cid;
        NSString *ctitle;
        NSString *ccategory;
        NSString *cimage;
        NSString *cthumbnail;
        NSString *ccalltitle;
        NSString *ctelephone;
        NSString *cemailtitle;
        NSString *cemail;
        NSString *cdetails;
        NSString *clinktitle;
        NSString *clinkurl;
        NSString *cmaptitle;
        NSString *cmaplat;
        NSString *cmaplon;
        NSString *cvideotitle;
        NSString *cvideourl;
        NSString *cfbtitle;
        NSString *cfburl;
        NSString *cpricetitle;
        NSString *cprice;
        NSString *cvar1title;
        NSString *cvar1;
        NSString *cvar2title;
        NSString *cvar2;
        NSString *cvar3title;
        NSString *cvar3;
        NSString *cvar4title;
        NSString *cvar4;
        NSString *cvar5title;
        NSString *cvar5;
        
        const char *sqlStatement3 = [theme_object UTF8String];
        sqlite3_stmt *compiledStatement3;
        if(sqlite3_prepare_v2(database, sqlStatement3, -1, &compiledStatement3, NULL) == SQLITE_OK) {
            listOfContent = [[NSMutableArray alloc] init];
            while(sqlite3_step(compiledStatement3) == SQLITE_ROW) {
                
                cid = [NSString stringWithUTF8String:(char *)sqlite3_column_text(compiledStatement3, 0)];
                ctitle = [NSString stringWithUTF8String:(char *)sqlite3_column_text(compiledStatement3, 1)];
                ccategory = [NSString stringWithUTF8String:(char *)sqlite3_column_text(compiledStatement3, 2)];
                cimage = [NSString stringWithUTF8String:(char *)sqlite3_column_text(compiledStatement3, 3)];
                cthumbnail = [NSString stringWithUTF8String:(char *)sqlite3_column_text(compiledStatement3, 4)];
                ccalltitle = [NSString stringWithUTF8String:(char *)sqlite3_column_text(compiledStatement3, 5)];
                ctelephone = [NSString stringWithUTF8String:(char *)sqlite3_column_text(compiledStatement3, 6)];
                cemailtitle = [NSString stringWithUTF8String:(char *)sqlite3_column_text(compiledStatement3, 7)];
                cemail = [NSString stringWithUTF8String:(char *)sqlite3_column_text(compiledStatement3, 8)];
                cdetails = [NSString stringWithUTF8String:(char *)sqlite3_column_text(compiledStatement3, 9)];
                clinktitle = [NSString stringWithUTF8String:(char *)sqlite3_column_text(compiledStatement3, 10)];
                clinkurl = [NSString stringWithUTF8String:(char *)sqlite3_column_text(compiledStatement3, 11)];
                cmaptitle = [NSString stringWithUTF8String:(char *)sqlite3_column_text(compiledStatement3, 12)];
                cmaplat = [NSString stringWithUTF8String:(char *)sqlite3_column_text(compiledStatement3, 13)];
                cmaplon = [NSString stringWithUTF8String:(char *)sqlite3_column_text(compiledStatement3, 14)];
                cvideotitle = [NSString stringWithUTF8String:(char *)sqlite3_column_text(compiledStatement3, 15)];
                cvideourl = [NSString stringWithUTF8String:(char *)sqlite3_column_text(compiledStatement3, 16)];
                cfbtitle = [NSString stringWithUTF8String:(char *)sqlite3_column_text(compiledStatement3, 17)];
                cfburl = [NSString stringWithUTF8String:(char *)sqlite3_column_text(compiledStatement3, 18)];
                cpricetitle = [NSString stringWithUTF8String:(char *)sqlite3_column_text(compiledStatement3, 23)];
                cprice = [NSString stringWithUTF8String:(char *)sqlite3_column_text(compiledStatement3, 24)];
                cvar1title = [NSString stringWithUTF8String:(char *)sqlite3_column_text(compiledStatement3, 25)];
                cvar1 = [NSString stringWithUTF8String:(char *)sqlite3_column_text(compiledStatement3, 26)];
                cvar2title = [NSString stringWithUTF8String:(char *)sqlite3_column_text(compiledStatement3, 27)];
                cvar2 = [NSString stringWithUTF8String:(char *)sqlite3_column_text(compiledStatement3, 28)];
                cvar3title = [NSString stringWithUTF8String:(char *)sqlite3_column_text(compiledStatement3, 29)];
                cvar3 = [NSString stringWithUTF8String:(char *)sqlite3_column_text(compiledStatement3, 30)];
                cvar4title = [NSString stringWithUTF8String:(char *)sqlite3_column_text(compiledStatement3, 31)];
                cvar4 = [NSString stringWithUTF8String:(char *)sqlite3_column_text(compiledStatement3, 32)];
                cvar5title = [NSString stringWithUTF8String:(char *)sqlite3_column_text(compiledStatement3, 33)];
                cvar5 = [NSString stringWithUTF8String:(char *)sqlite3_column_text(compiledStatement3, 34)];
                
                ctitle = [ctitle stringByDecodingHTMLEntities];
                ccalltitle = [ccalltitle stringByDecodingHTMLEntities];
                cemailtitle = [cemailtitle stringByDecodingHTMLEntities];
                cdetails = [cdetails stringByDecodingHTMLEntities];
                clinktitle = [clinktitle stringByDecodingHTMLEntities];
                cmaptitle = [cmaptitle stringByDecodingHTMLEntities];
                cvideotitle = [cvideotitle stringByDecodingHTMLEntities];
                cfbtitle = [cfbtitle stringByDecodingHTMLEntities];
                cpricetitle = [cpricetitle stringByDecodingHTMLEntities];
                cvar1title = [cvar1title stringByDecodingHTMLEntities];
                cvar1 = [cvar1 stringByDecodingHTMLEntities];
                cvar2title = [cvar2title stringByDecodingHTMLEntities];
                cvar2 = [cvar2 stringByDecodingHTMLEntities];
                cvar3title = [cvar3title stringByDecodingHTMLEntities];
                cvar3 = [cvar3 stringByDecodingHTMLEntities];
                cvar4title = [cvar4title stringByDecodingHTMLEntities];
                cvar4 = [cvar4 stringByDecodingHTMLEntities];
                cvar5title = [cvar5title stringByDecodingHTMLEntities];
                cvar5 = [cvar5 stringByDecodingHTMLEntities];
                ctitle = [ctitle stringByReplacingOccurrencesOfString:@"''" withString:@"'"];
                ccalltitle = [ccalltitle stringByReplacingOccurrencesOfString:@"''" withString:@"'"];
                cemailtitle = [cemailtitle stringByReplacingOccurrencesOfString:@"''" withString:@"'"];
                cdetails = [cdetails stringByReplacingOccurrencesOfString:@"''" withString:@"'"];
                clinktitle = [clinktitle stringByReplacingOccurrencesOfString:@"''" withString:@"'"];
                cmaptitle = [cmaptitle stringByReplacingOccurrencesOfString:@"''" withString:@"'"];
                cvideotitle = [cvideotitle stringByReplacingOccurrencesOfString:@"''" withString:@"'"];
                cfbtitle = [cfbtitle stringByReplacingOccurrencesOfString:@"''" withString:@"'"];
                cpricetitle = [cpricetitle stringByReplacingOccurrencesOfString:@"''" withString:@"'"];
                cvar1title = [cvar1title stringByReplacingOccurrencesOfString:@"''" withString:@"'"];
                cvar1 = [cvar1 stringByReplacingOccurrencesOfString:@"''" withString:@"'"];
                cvar2title = [cvar2title stringByReplacingOccurrencesOfString:@"''" withString:@"'"];
                cvar2 = [cvar2 stringByReplacingOccurrencesOfString:@"''" withString:@"'"];
                cvar3title = [cvar3title stringByReplacingOccurrencesOfString:@"''" withString:@"'"];
                cvar3 = [cvar3 stringByReplacingOccurrencesOfString:@"''" withString:@"'"];
                cvar4title = [cvar4title stringByReplacingOccurrencesOfString:@"''" withString:@"'"];
                cvar4 = [cvar4 stringByReplacingOccurrencesOfString:@"''" withString:@"'"];
                cvar5title = [cvar5title stringByReplacingOccurrencesOfString:@"''" withString:@"'"];
                cvar5 = [cvar5 stringByReplacingOccurrencesOfString:@"''" withString:@"'"];
                
                dicContent = [NSDictionary dictionaryWithObjectsAndKeys:cid,@"id",ctitle,@"title",ccategory,@"category_id",cimage,@"image",cthumbnail,@"thumbnail",ccalltitle,@"call_title",ctelephone,@"telephone",cemailtitle,@"email_title",cemail,@"email",cdetails,@"details",clinktitle,@"link_title",clinkurl,@"link_url",cmaptitle,@"map_title",cmaplat,@"map_lat",cmaplon,@"map_lon",cvideotitle,@"video_title",cvideourl,@"video_url",cfbtitle,@"fb_title",cfburl,@"fb_url",cpricetitle,@"price_title",cprice,@"price",cvar1title,@"var1_title",cvar1,@"var1",cvar2title,@"var2_title",cvar2,@"var2",cvar3title,@"var3_title",cvar3,@"var3",cvar4title,@"var4_title",cvar4,@"var4",cvar5title,@"var5_title",cvar5,@"var5",nil];
                [listOfContent addObject:dicContent];
                
                //NSLog(@"listOfContent::%@", listOfContent);
                
            }
        }
        // Release the compiled statement from memory
        sqlite3_finalize(compiledStatement3);
        
        scrollview = [[UIScrollView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height)];
        scrollview.backgroundColor = [UIColor whiteColor];
        scrollview.bounces = NO;
        [self.view addSubview:scrollview];
        
        int height=0;
        int noOfline;
        NSString *pluginId;
        if([listOfContent count]>0){
            
            NSDictionary *dictionary = [listOfContent objectAtIndex:0];
            
            NSString *contid = [dictionary objectForKey:@"id"];
            NSString *content_img = [dictionary objectForKey:@"image"];
            //NSRange range = [content_img rangeOfString:@"upload/"];
            //NSString *content_img_file = [[content_img substringFromIndex:NSMaxRange(range)] stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
            NSString *bar_name = [dictionary objectForKey:@"title"];
            NSString *contDetails = [dictionary objectForKey:@"details"];
            //NSString *linkTitle = [dictionary objectForKey:@"link_title"];
            NSString *linkUrl = [dictionary objectForKey:@"link_url"];
            NSString *contEmailTitle = [dictionary objectForKey:@"email_title"];
            NSString *contEmail = [dictionary objectForKey:@"email"];
            NSString *contCallTitle = [dictionary objectForKey:@"call_title"];
            NSString *contTel = [dictionary objectForKey:@"telephone"];
            //NSString *contSms = [dictionary objectForKey:@"sms"];
            //NSString *contMapTitle = [dictionary objectForKey:@"map_title"];
            float contLatitud = [[dictionary objectForKey:@"map_lat"] floatValue];
            float contLongitud = [[dictionary objectForKey:@"map_lon"] floatValue];
            //NSString *videoTitle = [dictionary objectForKey:@"video_title"];
            NSString *videoUrl = [dictionary objectForKey:@"video_url"];
            //NSString *fbtitle = [dictionary objectForKey:@"fb_title"];
            NSString *fbUrl = [dictionary objectForKey:@"fb_url"];
            // NSString *twitterAcct = [dictionary objectForKey:@"twitter_acct"];
            NSString *pricetitle = [dictionary objectForKey:@"price_title"];
            NSString *price = [dictionary objectForKey:@"price"];
            NSString *var1title = [dictionary objectForKey:@"var1_title"];
            NSString *var1 = [dictionary objectForKey:@"var1"];
            NSString *var2title = [dictionary objectForKey:@"var2_title"];
            NSString *var2 = [dictionary objectForKey:@"var2"];
            NSString *var3title = [dictionary objectForKey:@"var3_title"];
            NSString *var3 = [dictionary objectForKey:@"var3"];
            NSString *var4title = [dictionary objectForKey:@"var4_title"];
            NSString *var4 = [dictionary objectForKey:@"var4"];
            NSString *var5title = [dictionary objectForKey:@"var5_title"];
            NSString *var5 = [dictionary objectForKey:@"var5"];
            NSString *category_id = [dictionary objectForKey:@"category_id"];
            
            NSString *contCat = [[NSString alloc] initWithFormat:@"SELECT * from content_category WHERE id = %@", category_id];
            const char *sqlStatement4 = [contCat UTF8String];
            sqlite3_stmt *compiledStatement4;
            if(sqlite3_prepare_v2(database, sqlStatement4, -1, &compiledStatement4, NULL) == SQLITE_OK) {
                while(sqlite3_step(compiledStatement4) == SQLITE_ROW) {
                    
                    pluginId = [NSString stringWithUTF8String:(char *)sqlite3_column_text(compiledStatement4, 2)];
                }
            }
            // Release the compiled statement from memory
            sqlite3_finalize(compiledStatement4);
            
            self.galleryImages = [[NSMutableArray alloc] init];
            NSString *photoGalley = [[NSString alloc] initWithFormat:@"SELECT * FROM contents WHERE var1 = '%@' ORDER BY orderno", contid];
            const char *sqlStatement5 = [photoGalley UTF8String];
            sqlite3_stmt *compiledStatement5;
            if(sqlite3_prepare_v2(database, sqlStatement5, -1, &compiledStatement5, NULL) == SQLITE_OK) {
                while(sqlite3_step(compiledStatement5) == SQLITE_ROW) {
                    
                    NSString *content_img = [NSString stringWithUTF8String:(char *)sqlite3_column_text(compiledStatement5, 3)];
                    
                    dicImages = [NSDictionary dictionaryWithObjectsAndKeys:[dictionary objectForKey:@"id"],@"id",content_img,@"image",nil];
                    [self.galleryImages addObject:dicImages];
                    //[self.galleryImages addObject:content_img];
                    content_img =nil;
                    
                }
            }
            // Release the compiled statement from memory
            sqlite3_finalize(compiledStatement5);
            
            //NSLog(@"self.galleryImages: %@", self.galleryImages);
            UIButton *btnimgViewFav;
            UIButton *btnimgViewCom;
            UILabel *lblFav;
            UILabel *lblCom;
            btnimgViewFav = [UIButton buttonWithType:UIButtonTypeCustom];
            btnimgViewFav = [[UIButton alloc] init];
            lblFav = [[UILabel alloc] init];
            btnimgViewCom = [UIButton buttonWithType:UIButtonTypeCustom];
            btnimgViewCom = [[UIButton alloc] init];
            lblCom = [[UILabel alloc] init];
            
            if ([pluginId isEqualToString:@"20"]) {  //photo gallery
                
                if ([bar_name length]>0) {
                    
                    UILabel *lblTitle = [[UILabel alloc] initWithFrame:CGRectMake(5, 5, self.view.frame.size.width, 60)];
                    lblTitle.numberOfLines = 0;
                    lblTitle.lineBreakMode = NSLineBreakByWordWrapping;
                    UIFont *yourFont = [UIFont fontWithName:@"Helvetica" size:22];
                    lblTitle.font = yourFont;
                    lblTitle.text = bar_name;
                    lblTitle.backgroundColor = [UIColor clearColor];
                    lblTitle.textColor = [self colorWithHexString:appdelegate.themeColorCode];
                    //lblTitle.text = @"This is some text in a UILabel which is long enough to wrap around the lines in said UILabel. This is a test, this is only a test.";
                    [lblTitle sizeToFit];
                    lblTitle.shadowColor = [UIColor blackColor];
                    lblTitle.shadowOffset = CGSizeMake(0, 1);
                    CGSize constrainedSize = CGSizeMake(lblTitle.frame.size.width  , 9999);
                    
                    NSDictionary *attributesDictionary = [NSDictionary dictionaryWithObjectsAndKeys:
                                                          yourFont, NSFontAttributeName,
                                                          nil];
                    
                    NSMutableAttributedString *string = [[NSMutableAttributedString alloc] initWithString:lblTitle.text attributes:attributesDictionary];
                    
                    CGRect requiredHeight = [string boundingRectWithSize:constrainedSize options:NSStringDrawingUsesLineFragmentOrigin context:nil];
                    
                    if (requiredHeight.size.width > lblTitle.frame.size.width) {
                        requiredHeight = CGRectMake(0,0, lblTitle.frame.size.width, requiredHeight.size.height);
                    }
                    CGRect newFrame = lblTitle.frame;
                    newFrame.size.height = requiredHeight.size.height;
                    lblTitle.frame = newFrame;
                    
                    noOfline = newFrame.size.height;
                    
                    lblTitle.frame = CGRectMake(0, height, self.view.frame.size.width, noOfline+10);
                    [scrollview addSubview:lblTitle];
                    
                    lblTitle =nil;
                    
                    height = height+noOfline+10+10;
                }
                
                if ([self.galleryImages count]>0) {
                    
                    UIScrollView *scrollviewGalery = [[UIScrollView alloc] initWithFrame:CGRectMake(0, 0, 0, 0)];
                    
                    //scrollview.contentSize = CGSizeMake(scrollview.frame.size.height/4*7, scrollview.frame.size.height);
                    //scrollview.pagingEnabled=YES;
                    
                    [scrollviewGalery setBackgroundColor:[UIColor clearColor]];
                    [scrollview addSubview:scrollviewGalery];
                    
                    scrollviewGalery.frame = CGRectMake(0, height, self.view.frame.size.width, self.view.frame.size.width);
                    
                    [self setImagesGallery:scrollviewGalery];
                    
                    height = height + self.view.frame.size.width;
                    
                }
                
                if ([self showFavorite]==1) {
                    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone) {
                        // For iPhone
                        [lblFav setFrame:CGRectMake(self.view.frame.size.width/4+10, height+5, (self.view.frame.size.width)/4, 20)];
                    } else {
                        [lblFav setFrame: CGRectMake(self.view.frame.size.width/4+10, height+5, (self.view.frame.size.width)/4, 33)];
                    }
                    
                    [lblFav setFont:[UIFont boldSystemFontOfSize:16]];
                    lblFav.textColor = [UIColor blackColor];
                    lblFav.textAlignment = NSTextAlignmentLeft;
                    lblFav.backgroundColor = [UIColor clearColor];
                    lblFav.numberOfLines = 0;
                    [scrollview addSubview:lblFav];
                    
                    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone) {
                        // For iPhone
                        [btnimgViewFav setFrame:CGRectMake(self.view.frame.size.width/4-40, height, 25, 25)];
                    } else {
                        [btnimgViewFav setFrame:CGRectMake(self.view.frame.size.width/4-50, height, 38, 38)];
                    }
                    [scrollview addSubview:btnimgViewFav];
                    
                }
                
                if ([self showComments]==1) {
                    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone) {
                        // For iPhone
                        [lblCom setFrame:CGRectMake((self.view.frame.size.width/4*3)+10, height+5, self.view.frame.size.width/4, 20)];
                    } else {
                        [lblCom setFrame: CGRectMake((self.view.frame.size.width/4*3)+10, height+5, self.view.frame.size.width/4, 33)];
                    }
                    
                    [lblCom setFont:[UIFont boldSystemFontOfSize:16]];
                    lblCom.textColor = [UIColor blackColor];
                    lblCom.textAlignment = NSTextAlignmentLeft;
                    lblCom.backgroundColor = [UIColor clearColor];
                    lblCom.numberOfLines = 0;
                    [scrollview addSubview:lblCom];
                    
                    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone) {
                        // For iPhone
                        [btnimgViewCom setFrame:CGRectMake((self.view.frame.size.width/4*3)-40, height, 25, 25)];
                    } else {
                        [btnimgViewCom setFrame:CGRectMake((self.view.frame.size.width/4*3)-50, height, 38, 38)];
                    }
                    [scrollview addSubview:btnimgViewCom];
                }

                if ([self showFavorite]==1) {
                    NSPredicate *predicate0 = [NSPredicate predicateWithFormat:@"id == %@", [dictionary objectForKey:@"id"]];
                    NSArray *filteredArrayLike = [listOfFav filteredArrayUsingPredicate:predicate0];
                    if ([filteredArrayLike count]>0) {
                        [btnimgViewFav setImage:[UIImage imageNamed:@"favorite_red.png"] forState:UIControlStateNormal];
                    } else {
                        [btnimgViewFav setImage:[UIImage imageNamed:@"favorite_grey.png"] forState:UIControlStateNormal];
                    }
                    [btnimgViewFav addTarget:self action:@selector(checkButtonFavTapped:event:) forControlEvents:UIControlEventTouchUpInside];
                    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"id == %@", [dictionary objectForKey:@"id"]];
                    NSArray *filteredArray = [listOfContentsFav filteredArrayUsingPredicate:predicate];
                    //NSLog(@"filteredArray %@", filteredArray);
                    NSString *countFav=@"0";
                    if ([filteredArray count]>0) {
                        NSDictionary* favorite = [filteredArray objectAtIndex:0];
                        //NSLog(@"%@",[favorite objectForKey:@"cntFav"]);
                        countFav = [favorite objectForKey:@"cntFav"];
                    }
                    lblFav.text = [NSString stringWithFormat:@"%@", countFav];
                } else {
                    btnimgViewFav = nil;
                    lblFav.text = nil;
                }
                
                if ([self showComments]==1) {
                    [btnimgViewCom setImage:[UIImage imageNamed:@"comments.png"] forState:UIControlStateNormal];
                    [btnimgViewCom addTarget:self action:@selector(checkButtonCommentsTapped:event:) forControlEvents:UIControlEventTouchUpInside];
                    NSPredicate *predicate1 = [NSPredicate predicateWithFormat:@"id == %@", [dictionary objectForKey:@"id"]];
                    NSArray *filteredArrayComm = [listOfContentsComments filteredArrayUsingPredicate:predicate1];
                    NSString *countComm=@"0";
                    if ([filteredArrayComm count]>0) {
                        NSDictionary* comments = [filteredArrayComm objectAtIndex:0];
                        //NSLog(@"%@",[favorite objectForKey:@"cntFav"]);
                        countComm = [comments objectForKey:@"cntComments"];
                    }
                    lblCom.text = [NSString stringWithFormat:@"%@", countComm];
                    //NSLog(@"filteredArrayComm %@", filteredArrayComm);
                } else {
                    btnimgViewCom = nil;
                    lblCom.text = nil;
                }
                
                
                if ([self showFavorite] == 1 || [self showComments] == 1) {
                    height = height + 40;
                }
                
                int heightwvphoto;
                if ([contDetails length] > 0) {
                    /*contDetails = [contDetails stringByReplacingOccurrencesOfString:@"/n" withString:@"\n"];
                     
                     //CGSize size = [contDetails sizeWithFont:[UIFont systemFontOfSize:16]
                     //constrainedToSize:CGSizeMake(self.view.frame.size.width, FLT_MAX)
                     //lineBreakMode:NSLineBreakByTruncatingTail];
                     CGSize size;
                     CGRect frame = [contDetails boundingRectWithSize:CGSizeMake(self.view.frame.size.width, CGFLOAT_MAX)
                     options:NSStringDrawingUsesLineFragmentOrigin
                     attributes:@{NSFontAttributeName:[UIFont systemFontOfSize:18]}
                     context:nil];
                     size = CGSizeMake(frame.size.width, frame.size.height);
                     
                     UITextView *newTextView = [[UITextView alloc] initWithFrame:CGRectMake(0, height, self.view.frame.size.width, size.height)];
                     [newTextView setFont:[UIFont systemFontOfSize:16]];
                     newTextView.text = contDetails;
                     newTextView.userInteractionEnabled = NO;
                     height = height + size.height;
                     NSAttributedString *attributedString = [[NSAttributedString alloc] initWithData:[newTextView.text dataUsingEncoding:NSUTF8StringEncoding] options:@{ NSDocumentTypeDocumentAttribute: NSPlainTextDocumentType } documentAttributes:nil error:nil];
                     newTextView.attributedText = attributedString;
                     [scrollview addSubview:newTextView]; */
                    contDetails = [contDetails stringByReplacingOccurrencesOfString:@"/n" withString:@"\n"];
                    
                    UIFont *yourFont = [UIFont fontWithName:@"Helvetica" size:14];
                    CGSize size;
                    CGRect frame = [contDetails boundingRectWithSize:CGSizeMake(self.view.frame.size.width-20, CGFLOAT_MAX)
                                                             options:NSStringDrawingUsesLineFragmentOrigin
                                                          attributes:@{NSFontAttributeName:yourFont}
                                                             context:nil];
                    size = CGSizeMake(frame.size.width, frame.size.height);
                    
                    wv = [[UIWebView alloc] init];
                    heightwvphoto = size.height+100;
                    [wv setFrame:CGRectMake(0, height, self.view.frame.size.width, heightwvphoto)];
                    NSString *htmlstring = [NSString stringWithFormat:@"<html><body><div style='max-width: 100%%;font-size:14;font-family:Helvetica;color:#000000;'>%@</body></html>", contDetails];
                    [wv loadHTMLString:htmlstring baseURL:nil];
                    //wv.userInteractionEnabled = NO;
                    UIView* row = nil;
                    for(row in wv.subviews){
                        if([row isKindOfClass:[UIScrollView class] ]){
                            UIScrollView* scrollRow = (UIScrollView*) row;
                            scrollRow.scrollEnabled = YES;
                            scrollRow.bounces = NO;
                            scrollRow.backgroundColor=[UIColor clearColor];
                        }
                    }
                    wv.alpha=0.6f;
                    [wv setBackgroundColor:[UIColor whiteColor]];
                    [wv setOpaque:NO];
                    [scrollview addSubview:wv];
                }
                
                scrollview.contentSize = CGSizeMake(scrollview.frame.size.width, 40+height+heightwvphoto);
                
            } else if ([pluginId isEqualToString:@"21"]) {  //video gallery
                
                if ([videoUrl length]>0) {
                    [webView removeFromSuperview];
                    webView = [[UIWebView alloc] init];
                    webView.backgroundColor = [UIColor clearColor];
                    [webView setFrame:CGRectMake(0, height, self.view.frame.size.width, self.view.frame.size.width)];
                    //[[UIApplication sharedApplication] openURL:[NSURL URLWithString:[dictionary objectForKey:@"object_value"]]];
                    //[webView loadRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:[dictionary objectForKey:@"object_value"]]]];
                    //webView.autoresizesSubviews = YES;
                    //webView.autoresizingMask=(UIViewAutoresizingFlexibleHeight | UIViewAutoresizingFlexibleWidth);
                    
                    // if your url is not in embed format or it is dynamic then you have to convert it in embed format.
                    NSString *string = @"watch?v=";
                    if ([videoUrl rangeOfString:string].location == NSNotFound) {
                        //NSLog(@"1");
                        
                        //https://www.youtube.com/watch?v=qQMaav1N7vU&feature=youtu.be
                        //http://youtu.be/qQMaav1N7vU
                        
                        NSString *string1 = @"v/";
                        NSString *string2 = @"youtu.be/";
                        if ([videoUrl rangeOfString:string1].location == NSNotFound) {
                            //NSLog(@"Just for IRMalaysia");
                            if ([videoUrl rangeOfString:string2].location == NSNotFound) {
                                [webView loadRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:videoUrl]]];
                            } else {
                                NSString *preTel;
                                NSString *vid;
                                
                                NSScanner *scanner = [NSScanner scannerWithString:videoUrl];
                                [scanner scanUpToString:string2 intoString:&preTel];
                                
                                [scanner scanString:string2 intoString:nil];
                                vid = [videoUrl substringFromIndex:scanner.scanLocation];
                                
                                NSString *newVideoUrl = @"https://www.youtube.com/watch?v=vid&feature=youtu.be";
                                newVideoUrl = [newVideoUrl stringByReplacingOccurrencesOfString:@"vid" withString:vid];
                                //NSLog(@"vid: %@", vid);
                                //NSLog(@"newVideoUrl: %@", newVideoUrl);
                                
                                newVideoUrl = [newVideoUrl stringByReplacingOccurrencesOfString:@"watch?v=" withString:@"embed/"];
                                
                                NSRange range = [newVideoUrl rangeOfString:@"&"];
                                @try {
                                    newVideoUrl = [newVideoUrl substringToIndex:range.location];
                                }
                                @catch (NSException *exception) {
                                    
                                }
                                
                                NSString *youTubeVideoHTML = [NSString stringWithFormat:@"<html><body style=\"margin:0\"><iframe class=\"youtube-player\" type=\"text/html\" width=\"%f\" height=\"%f\" style=\"max-width:100%%\" src=\"%@\" frameborder=\"0\"></iframe></body></html>", scrollview.frame.size.width, scrollview.frame.size.width, newVideoUrl];
                                
                                // Populate HTML with the URL and requested frame size
                                //NSString *html = [NSString stringWithFormat:youTubeVideoHTML];
                                
                                // Load the html into the webview
                                [webView loadHTMLString:youTubeVideoHTML baseURL:nil];
                            }
                        } else {
                            //NSLog(@"1b");
                            videoUrl = [videoUrl stringByReplacingOccurrencesOfString:@"v/" withString:@"embed/"];
                            
                            NSString *youTubeVideoHTML = [NSString stringWithFormat:@"<html><body style=\"margin:0\"><iframe class=\"youtube-player\" type=\"text/html\" width=\"%f\" height=\"%f\" style=\"max-width:100%%\" src=\"%@\" frameborder=\"0\"></iframe></body></html>", scrollview.frame.size.width, scrollview.frame.size.width, videoUrl];
                            
                            // Populate HTML with the URL and requested frame size
                            //NSString *html = [NSString stringWithFormat:youTubeVideoHTML];
                            
                            // Load the html into the webview
                            [webView loadHTMLString:youTubeVideoHTML baseURL:nil];
                        }
                        
                    } else {
                        //NSLog(@"string contains substring!");
                        //NSLog(@"2");
                        videoUrl = [videoUrl stringByReplacingOccurrencesOfString:@"watch?v=" withString:@"embed/"];
                        
                        NSRange range = [videoUrl rangeOfString:@"&"];
                        @try {
                            videoUrl = [videoUrl substringToIndex:range.location];
                        }
                        @catch (NSException *exception) {
                            
                        }
                        
                        NSString *youTubeVideoHTML = [NSString stringWithFormat:@"<html><body style=\"margin:0\"><iframe class=\"youtube-player\" type=\"text/html\" width=\"%f\" height=\"%f\" style=\"max-width:100%%\" src=\"%@\" frameborder=\"0\"></iframe></body></html>", scrollview.frame.size.width, scrollview.frame.size.width, videoUrl];
                        
                        // Populate HTML with the URL and requested frame size
                        //NSString *html = [NSString stringWithFormat:youTubeVideoHTML];
                        
                        // Load the html into the webview
                        [webView loadHTMLString:youTubeVideoHTML baseURL:nil];
                    }
                    height = height + self.view.frame.size.width;
                    [scrollview addSubview:webView];
                    webView =nil;
                }
                
                if ([bar_name length]>0) {
                    
                    UIImageView *ImageBarView = [[UIImageView alloc] initWithFrame:CGRectMake(0, height, self.view.frame.size.width, 60)];
                    //NSString *myImage = [NSString stringWithFormat:@"%@_bar_maincategory.png", appdelegate.themecolor];
                    //ImageBarView.image = [UIImage imageNamed:myImage];
                    ImageBarView.alpha = 1.0;
                    
                    //AppDelegate *appdelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
                    UIView *overlay;
                    overlay = [[UIView alloc] initWithFrame:CGRectMake(0, 0, ImageBarView.frame.size.width, ImageBarView.frame.size.height)];
                    overlay.backgroundColor = [self colorWithHexString:appdelegate.themeColorCode];
                    overlay.alpha = 0.6f;
                    //overlay.opaque = YES;
                    
                    [ImageBarView addSubview:overlay];
                    
                    UILabel *lblTitle = [[UILabel alloc] initWithFrame:CGRectMake(5, 5, self.view.frame.size.width, 60)];
                    lblTitle.numberOfLines = 0;
                    lblTitle.lineBreakMode = NSLineBreakByWordWrapping;
                    UIFont *yourFont = [UIFont fontWithName:@"Helvetica-Bold" size:22];
                    lblTitle.font = yourFont;
                    lblTitle.text = bar_name;
                    lblTitle.backgroundColor = [UIColor clearColor];
                    lblTitle.textColor = [UIColor whiteColor];
                    //lblTitle.text = @"This is some text in a UILabel which is long enough to wrap around the lines in said UILabel. This is a test, this is only a test.";
                    [lblTitle sizeToFit];
                    lblTitle.shadowColor = [UIColor blackColor];
                    lblTitle.shadowOffset = CGSizeMake(0, 1);
                    [ImageBarView addSubview:lblTitle];
                    CGSize constrainedSize = CGSizeMake(lblTitle.frame.size.width  , 9999);
                    
                    NSDictionary *attributesDictionary = [NSDictionary dictionaryWithObjectsAndKeys:
                                                          yourFont, NSFontAttributeName,
                                                          nil];
                    
                    NSMutableAttributedString *string = [[NSMutableAttributedString alloc] initWithString:lblTitle.text attributes:attributesDictionary];
                    
                    CGRect requiredHeight = [string boundingRectWithSize:constrainedSize options:NSStringDrawingUsesLineFragmentOrigin context:nil];
                    
                    if (requiredHeight.size.width > lblTitle.frame.size.width) {
                        requiredHeight = CGRectMake(0,0, lblTitle.frame.size.width, requiredHeight.size.height);
                    }
                    CGRect newFrame = lblTitle.frame;
                    newFrame.size.height = requiredHeight.size.height;
                    lblTitle.frame = newFrame;
                    
                    noOfline = newFrame.size.height;
                    
                    ImageBarView.frame = CGRectMake(0, height, self.view.frame.size.width, noOfline+10);
                    overlay.frame = CGRectMake(0, 0, ImageBarView.frame.size.width, ImageBarView.frame.size.height);
                    [scrollview addSubview:ImageBarView];
                    
                    lblTitle =nil;
                    ImageBarView =nil;
                    
                    height = height+noOfline+10+10;
                }
                
                if ([self showFavorite]==1) {
                    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone) {
                        // For iPhone
                        [lblFav setFrame:CGRectMake(self.view.frame.size.width/4+10, height+5, (self.view.frame.size.width)/4, 20)];
                    } else {
                        [lblFav setFrame: CGRectMake(self.view.frame.size.width/4+10, height+5, (self.view.frame.size.width)/4, 33)];
                    }
                    
                    [lblFav setFont:[UIFont boldSystemFontOfSize:16]];
                    lblFav.textColor = [UIColor blackColor];
                    lblFav.textAlignment = NSTextAlignmentLeft;
                    lblFav.backgroundColor = [UIColor clearColor];
                    lblFav.numberOfLines = 0;
                    [scrollview addSubview:lblFav];
                    
                    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone) {
                        // For iPhone
                        [btnimgViewFav setFrame:CGRectMake((self.view.frame.size.width/4)-40, height, 25, 25)];
                    } else {
                        [btnimgViewFav setFrame:CGRectMake((self.view.frame.size.width/4)-50, height, 38, 38)];
                    }
                    [scrollview addSubview:btnimgViewFav];
                }
                
                if ([self showComments]==1) {
                    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone) {
                        // For iPhone
                        [lblCom setFrame:CGRectMake((self.view.frame.size.width/4*3)+10, height+5, self.view.frame.size.width/4, 20)];
                    } else {
                        [lblCom setFrame: CGRectMake((self.view.frame.size.width/4*3)+10, height+5, self.view.frame.size.width/4, 33)];
                    }
                    
                    [lblCom setFont:[UIFont boldSystemFontOfSize:16]];
                    lblCom.textColor = [UIColor blackColor];
                    lblCom.textAlignment = NSTextAlignmentLeft;
                    lblCom.backgroundColor = [UIColor clearColor];
                    lblCom.numberOfLines = 0;
                    [scrollview addSubview:lblCom];
                    
                    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone) {
                        // For iPhone
                        [btnimgViewCom setFrame:CGRectMake((self.view.frame.size.width/4*3)-40, height, 25, 25)];
                    } else {
                        [btnimgViewCom setFrame:CGRectMake((self.view.frame.size.width/4*3)-50, height, 38, 38)];
                    }
                    [scrollview addSubview:btnimgViewCom];
                }
                
                if ([self showFavorite]==1) {
                    NSPredicate *predicate0 = [NSPredicate predicateWithFormat:@"id == %@", [dictionary objectForKey:@"id"]];
                    NSArray *filteredArrayLike = [listOfFav filteredArrayUsingPredicate:predicate0];
                    if ([filteredArrayLike count]>0) {
                        [btnimgViewFav setImage:[UIImage imageNamed:@"favorite_red.png"] forState:UIControlStateNormal];
                    } else {
                        [btnimgViewFav setImage:[UIImage imageNamed:@"favorite_grey.png"] forState:UIControlStateNormal];
                    }
                    [btnimgViewFav addTarget:self action:@selector(checkButtonFavTapped:event:) forControlEvents:UIControlEventTouchUpInside];
                    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"id == %@", [dictionary objectForKey:@"id"]];
                    NSArray *filteredArray = [listOfContentsFav filteredArrayUsingPredicate:predicate];
                    //NSLog(@"filteredArray %@", filteredArray);
                    NSString *countFav=@"0";
                    if ([filteredArray count]>0) {
                        NSDictionary* favorite = [filteredArray objectAtIndex:0];
                        //NSLog(@"%@",[favorite objectForKey:@"cntFav"]);
                        countFav = [favorite objectForKey:@"cntFav"];
                    }
                    lblFav.text = [NSString stringWithFormat:@"%@", countFav];
                } else {
                    btnimgViewFav = nil;
                    lblFav.text = nil;
                }
                
                if ([self showComments]==1) {
                    [btnimgViewCom setImage:[UIImage imageNamed:@"comments.png"] forState:UIControlStateNormal];
                    [btnimgViewCom addTarget:self action:@selector(checkButtonCommentsTapped:event:) forControlEvents:UIControlEventTouchUpInside];
                    NSPredicate *predicate1 = [NSPredicate predicateWithFormat:@"id == %@", [dictionary objectForKey:@"id"]];
                    NSArray *filteredArrayComm = [listOfContentsComments filteredArrayUsingPredicate:predicate1];
                    NSString *countComm=@"0";
                    if ([filteredArrayComm count]>0) {
                        NSDictionary* comments = [filteredArrayComm objectAtIndex:0];
                        //NSLog(@"%@",[favorite objectForKey:@"cntFav"]);
                        countComm = [comments objectForKey:@"cntComments"];
                    }
                    lblCom.text = [NSString stringWithFormat:@"%@", countComm];
                    //NSLog(@"filteredArrayComm %@", filteredArrayComm);
                } else {
                    btnimgViewCom = nil;
                    lblCom.text = nil;
                }
                
                
                if ([self showFavorite] == 1 || [self showComments] == 1) {
                    height = height + 40;
                }
                
                int heightwvvideo;
                if ([contDetails length] > 0) {
                    /*contDetails = [contDetails stringByReplacingOccurrencesOfString:@"/n" withString:@"\n"];
                     
                     //CGSize size = [contDetails sizeWithFont:[UIFont systemFontOfSize:16]
                     //constrainedToSize:CGSizeMake(self.view.frame.size.width, FLT_MAX)
                     //lineBreakMode:NSLineBreakByTruncatingTail];
                     CGSize size;
                     CGRect frame = [contDetails boundingRectWithSize:CGSizeMake(self.view.frame.size.width, CGFLOAT_MAX)
                     options:(NSStringDrawingUsesLineFragmentOrigin | NSStringDrawingUsesFontLeading)
                     attributes:@{NSFontAttributeName:[UIFont systemFontOfSize:18]}
                     context:nil];
                     size = CGSizeMake(frame.size.width, frame.size.height);
                     
                     UITextView *newTextView = [[UITextView alloc] initWithFrame:CGRectMake(0, height, self.view.frame.size.width, size.height)];
                     [newTextView setFont:[UIFont systemFontOfSize:16]];
                     newTextView.text = contDetails;
                     newTextView.userInteractionEnabled = NO;
                     NSAttributedString *attributedString = [[NSAttributedString alloc] initWithData:[newTextView.text dataUsingEncoding:NSUTF8StringEncoding] options:@{ NSDocumentTypeDocumentAttribute: NSPlainTextDocumentType } documentAttributes:nil error:nil];
                     newTextView.attributedText = attributedString;
                     [scrollview addSubview:newTextView];
                     height = height + size.height; */
                    contDetails = [contDetails stringByReplacingOccurrencesOfString:@"/n" withString:@"\n"];
                    
                    UIFont *yourFont = [UIFont fontWithName:@"Helvetica" size:14];
                    CGSize size;
                    CGRect frame = [contDetails boundingRectWithSize:CGSizeMake(self.view.frame.size.width-20, CGFLOAT_MAX)
                                                             options:NSStringDrawingUsesLineFragmentOrigin
                                                          attributes:@{NSFontAttributeName:yourFont}
                                                             context:nil];
                    size = CGSizeMake(frame.size.width, frame.size.height);
                    
                    wv = [[UIWebView alloc] init];
                    heightwvvideo = size.height+100;
                    [wv setFrame:CGRectMake(0, height, self.view.frame.size.width, heightwvvideo)];
                    NSString *htmlstring = [NSString stringWithFormat:@"<html><body><div style='max-width: 100%%;font-size:14;font-family:Helvetica;color:#000000;'>%@</body></html>", contDetails];
                    [wv loadHTMLString:htmlstring baseURL:nil];
                    //wv.userInteractionEnabled = NO;
                    UIView* row = nil;
                    for(row in wv.subviews){
                        if([row isKindOfClass:[UIScrollView class] ]){
                            UIScrollView* scrollRow = (UIScrollView*) row;
                            scrollRow.scrollEnabled = YES;
                            scrollRow.bounces = NO;
                            scrollRow.backgroundColor=[UIColor clearColor];
                        }
                    }
                    wv.alpha=0.6f;
                    [wv setBackgroundColor:[UIColor whiteColor]];
                    [wv setOpaque:NO];
                    [scrollview addSubview:wv];
                    
                    //scrollview.contentSize = CGSizeMake(scrollview.frame.size.width, heightwv);
                }
                
                scrollview.contentSize = CGSizeMake(scrollview.frame.size.width, 40+height+heightwvvideo);
                
            } else {
                MenuItems = [[NSMutableArray alloc] init];
                [MenuItems addObject:@"Map"];
                [MenuItems addObject:@"Link"];
                [MenuItems addObject:@"Video"];
                [MenuItems addObject:@"Facebook"];
                //[MenuItems addObject:@"Twitter"];
                //NSLog(@"MenuItems::%@", MenuItems);
                
                int j;
                j=0;
                for(j=0; j<[MenuItems count]; j++) {
                    //NSLog(@"[listOfMenuItems objectAtIndex:i] :: %@", [listOfMenuItems objectAtIndex:i]);
                    if (/*([[MenuItems objectAtIndex:j] isEqualToString:@"Call"] && [contTel length]==0) ||([[MenuItems objectAtIndex:j] isEqualToString:@"Email"] && [contEmail length]==0) ||
                         */([[MenuItems objectAtIndex:j] isEqualToString:@"Map"] && contLatitud==0  && contLongitud==0) ||
                        ([[MenuItems objectAtIndex:j] isEqualToString:@"Link"] && [linkUrl length]==0) ||
                        ([[MenuItems objectAtIndex:j] isEqualToString:@"Video"] && [videoUrl length]==0) ||
                        ([[MenuItems objectAtIndex:j] isEqualToString:@"Facebook"] && [fbUrl length]==0)) {
                        [MenuItems removeObjectAtIndex:j];
                        j--;
                        
                    }
                }
                
                if (![listtemplate isEqualToString:@"1"] && ![listtemplate isEqualToString:@"2"] && ![listtemplate isEqualToString:@"3"]) {
                    if ([bar_name length]>0) {
                        
                        UILabel *lblTitle = [[UILabel alloc] initWithFrame:CGRectMake(5, 5, self.view.frame.size.width, 60)];
                        lblTitle.numberOfLines = 0;
                        lblTitle.lineBreakMode = NSLineBreakByWordWrapping;
                        UIFont *yourFont = [UIFont fontWithName:@"Helvetica" size:22];
                        lblTitle.font = yourFont;
                        lblTitle.text = bar_name;
                        lblTitle.backgroundColor = [UIColor clearColor];
                        lblTitle.textColor = [self colorWithHexString:appdelegate.themeColorCode];
                        //lblTitle.text = @"This is some text in a UILabel which is long enough to wrap around the lines in said UILabel. This is a test, this is only a test.";
                        [lblTitle sizeToFit];
                        lblTitle.shadowColor = [UIColor blackColor];
                        lblTitle.shadowOffset = CGSizeMake(0, 1);
                        CGSize constrainedSize = CGSizeMake(lblTitle.frame.size.width  , 9999);
                        
                        NSDictionary *attributesDictionary = [NSDictionary dictionaryWithObjectsAndKeys:
                                                              yourFont, NSFontAttributeName,
                                                              nil];
                        
                        NSMutableAttributedString *string = [[NSMutableAttributedString alloc] initWithString:lblTitle.text attributes:attributesDictionary];
                        
                        CGRect requiredHeight = [string boundingRectWithSize:constrainedSize options:NSStringDrawingUsesLineFragmentOrigin context:nil];
                        
                        if (requiredHeight.size.width > lblTitle.frame.size.width) {
                            requiredHeight = CGRectMake(0,0, lblTitle.frame.size.width, requiredHeight.size.height);
                        }
                        CGRect newFrame = lblTitle.frame;
                        newFrame.size.height = requiredHeight.size.height;
                        lblTitle.frame = newFrame;
                        
                        noOfline = newFrame.size.height;
                        
                        lblTitle.frame = CGRectMake(0, height, self.view.frame.size.width, noOfline+10);
                        [scrollview addSubview:lblTitle];
                        
                        lblTitle =nil;
                        
                        height = height+noOfline+10+10;
                    }
                }
                
                if ([content_img length]>0) {
                    
                    UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
                    button.frame = CGRectMake(0, height, self.view.frame.size.width, self.view.frame.size.width);
                    
                    NSString *imgURL = content_img;
                    
                    if ([imgURL length]>0)
                    {
                        //[button setBackgroundImageWithURL:[NSURL URLWithString:imgURL] forState:UIControlStateNormal placeholderImage:[UIImage imageNamed:@"No-Image-icon.png"]];
                        UIImageView *imgV = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, button.frame.size.width, button.frame.size.height)];
                        [button addSubview:imgV];
                        
                        __block UIActivityIndicatorView *activityIndicator;
                        __weak UIImageView *weakImageView = imgV;
                        
                        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0), ^ {
                            [weakImageView sd_setImageWithURL:[NSURL URLWithString:imgURL]
                                             placeholderImage:[UIImage imageNamed:@"No-Image-icon.png"]
                                                      options:SDWebImageProgressiveDownload
                                                     progress:^(NSInteger receivedSize, NSInteger expectedSize, NSURL *targetURL) {
                                                         dispatch_async(dispatch_get_main_queue(), ^ {
                                                             if (!activityIndicator) {
                                                                 
                                                                 [weakImageView addSubview:activityIndicator = [UIActivityIndicatorView.alloc initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray]];
                                                                 activityIndicator.center = weakImageView.center;
                                                                 [activityIndicator startAnimating];
                                                             }
                                                         });
                                                     }
                                                    completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
                                                        dispatch_async(dispatch_get_main_queue(), ^ {
                                                            //[MBProgressHUD hideHUDForView:self.view animated:YES];
                                                            [activityIndicator removeFromSuperview];
                                                            activityIndicator = nil;
                                                        });
                                                    }];
                        });
            
                        button.contentMode  = UIViewContentModeScaleAspectFill;
                        [button setClipsToBounds:YES];
                    }
                    else
                    {
                        [button setTitle:@"No Image" forState:UIControlStateNormal];
                        [button setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
                        [button setBackgroundColor:[UIColor colorWithRed:230/255.0 green:230/255.0 blue:230/255.0 alpha:1]];
                    }
                    imgURL =nil;
                    //[button setTitle:@"No Image" forState:UIControlStateNormal];
                    
                    [scrollview addSubview:button];
                    
                    height = height + self.view.frame.size.width;
                    
                }
                
                if ([listtemplate isEqualToString:@"1"] || [listtemplate isEqualToString:@"2"] || [listtemplate isEqualToString:@"3"]) {
                    if ([bar_name length]>0) {
                        
                        UIImageView *ImageBarView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 260+60, self.view.frame.size.width, 60)];
                        //NSString *myImage = [NSString stringWithFormat:@"%@_bar_maincategory.png", appdelegate.themecolor];
                        //ImageBarView.image = [UIImage imageNamed:myImage];
                        ImageBarView.alpha = 1.0;
                        
                        //AppDelegate *appdelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
                        UIView *overlay;
                        overlay = [[UIView alloc] initWithFrame:CGRectMake(0, 0, ImageBarView.frame.size.width, ImageBarView.frame.size.height)];
                        overlay.backgroundColor = [self colorWithHexString:appdelegate.themeColorCode];
                        overlay.alpha = 0.6f;
                        //overlay.opaque = YES;
                        
                        [ImageBarView addSubview:overlay];
                        
                        UILabel *lblTitle = [[UILabel alloc] initWithFrame:CGRectMake(5, 5, self.view.frame.size.width, 60)];
                        lblTitle.numberOfLines = 0;
                        lblTitle.lineBreakMode = NSLineBreakByWordWrapping;
                        UIFont *yourFont = [UIFont fontWithName:@"Helvetica-Bold" size:22];
                        lblTitle.font = yourFont;
                        lblTitle.text = bar_name;
                        lblTitle.backgroundColor = [UIColor clearColor];
                        lblTitle.textColor = [UIColor whiteColor];
                        //lblTitle.text = @"This is some text in a UILabel which is long enough to wrap around the lines in said UILabel. This is a test, this is only a test.";
                        [lblTitle sizeToFit];
                        lblTitle.shadowColor = [UIColor blackColor];
                        lblTitle.shadowOffset = CGSizeMake(0, 1);
                        [ImageBarView addSubview:lblTitle];
                        CGSize constrainedSize = CGSizeMake(lblTitle.frame.size.width  , 9999);
                        
                        NSDictionary *attributesDictionary = [NSDictionary dictionaryWithObjectsAndKeys:
                                                              yourFont, NSFontAttributeName,
                                                              nil];
                        
                        NSMutableAttributedString *string = [[NSMutableAttributedString alloc] initWithString:lblTitle.text attributes:attributesDictionary];
                        
                        CGRect requiredHeight = [string boundingRectWithSize:constrainedSize options:NSStringDrawingUsesLineFragmentOrigin context:nil];
                        
                        if (requiredHeight.size.width > lblTitle.frame.size.width) {
                            requiredHeight = CGRectMake(0,0, lblTitle.frame.size.width, requiredHeight.size.height);
                        }
                        CGRect newFrame = lblTitle.frame;
                        newFrame.size.height = requiredHeight.size.height;
                        lblTitle.frame = newFrame;
                        
                        noOfline = newFrame.size.height;
                        
                        ImageBarView.frame = CGRectMake(0, height, self.view.frame.size.width, noOfline+10);
                        overlay.frame = CGRectMake(0, 0, ImageBarView.frame.size.width, ImageBarView.frame.size.height);
                        [scrollview addSubview:ImageBarView];
                        
                        lblTitle =nil;
                        ImageBarView =nil;
                        
                        height = height+noOfline+10+10;
                    }
                }
                
                if ([self showFavorite]==1) {
                    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone) {
                        // For iPhone
                        [lblFav setFrame:CGRectMake(self.view.frame.size.width/4+10, height+5, (self.view.frame.size.width)/4, 20)];
                    } else {
                        [lblFav setFrame: CGRectMake(self.view.frame.size.width/4+10, height+5, (self.view.frame.size.width)/4, 33)];
                    }
                    
                    [lblFav setFont:[UIFont boldSystemFontOfSize:16]];
                    lblFav.textColor = [UIColor blackColor];
                    lblFav.textAlignment = NSTextAlignmentLeft;
                    lblFav.backgroundColor = [UIColor clearColor];
                    lblFav.numberOfLines = 0;
                    [scrollview addSubview:lblFav];
                    
                    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone) {
                        // For iPhone
                        [btnimgViewFav setFrame:CGRectMake(self.view.frame.size.width/4-40, height, 25, 25)];
                    } else {
                        [btnimgViewFav setFrame:CGRectMake(self.view.frame.size.width/4-50, height, 38, 38)];
                    }
                    [scrollview addSubview:btnimgViewFav];
                }
                
                if ([self showComments]==1) {
                    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone) {
                        // For iPhone
                        [lblCom setFrame:CGRectMake((self.view.frame.size.width/4*3)+10, height+5, self.view.frame.size.width/4, 20)];
                    } else {
                        [lblCom setFrame: CGRectMake((self.view.frame.size.width/4*3)+10, height+5, self.view.frame.size.width/4, 33)];
                    }
                    
                    [lblCom setFont:[UIFont boldSystemFontOfSize:16]];
                    lblCom.textColor = [UIColor blackColor];
                    lblCom.textAlignment = NSTextAlignmentLeft;
                    lblCom.backgroundColor = [UIColor clearColor];
                    lblCom.numberOfLines = 0;
                    [scrollview addSubview:lblCom];
                    
                    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone) {
                        // For iPhone
                        [btnimgViewCom setFrame:CGRectMake((self.view.frame.size.width/4*3)-40, height, 25, 25)];
                    } else {
                        [btnimgViewCom setFrame:CGRectMake((self.view.frame.size.width/4*3)-50, height, 38, 38)];
                    }
                    [scrollview addSubview:btnimgViewCom];
                }
                
                if ([self showFavorite]==1) {
                    NSPredicate *predicate0 = [NSPredicate predicateWithFormat:@"id == %@", [dictionary objectForKey:@"id"]];
                    NSArray *filteredArrayLike = [listOfFav filteredArrayUsingPredicate:predicate0];
                    if ([filteredArrayLike count]>0) {
                        [btnimgViewFav setImage:[UIImage imageNamed:@"favorite_red.png"] forState:UIControlStateNormal];
                    } else {
                        [btnimgViewFav setImage:[UIImage imageNamed:@"favorite_grey.png"] forState:UIControlStateNormal];
                    }
                    [btnimgViewFav addTarget:self action:@selector(checkButtonFavTapped:event:) forControlEvents:UIControlEventTouchUpInside];
                    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"id == %@", [dictionary objectForKey:@"id"]];
                    NSArray *filteredArray = [listOfContentsFav filteredArrayUsingPredicate:predicate];
                    //NSLog(@"filteredArray %@", filteredArray);
                    NSString *countFav=@"0";
                    if ([filteredArray count]>0) {
                        NSDictionary* favorite = [filteredArray objectAtIndex:0];
                        //NSLog(@"%@",[favorite objectForKey:@"cntFav"]);
                        countFav = [favorite objectForKey:@"cntFav"];
                    }
                    lblFav.text = [NSString stringWithFormat:@"%@", countFav];
                } else {
                    btnimgViewFav = nil;
                    lblFav.text = nil;
                }
                
                if ([self showComments]==1) {
                    [btnimgViewCom setImage:[UIImage imageNamed:@"comments.png"] forState:UIControlStateNormal];
                    [btnimgViewCom addTarget:self action:@selector(checkButtonCommentsTapped:event:) forControlEvents:UIControlEventTouchUpInside];
                    NSPredicate *predicate1 = [NSPredicate predicateWithFormat:@"id == %@", [dictionary objectForKey:@"id"]];
                    NSArray *filteredArrayComm = [listOfContentsComments filteredArrayUsingPredicate:predicate1];
                    NSString *countComm=@"0";
                    if ([filteredArrayComm count]>0) {
                        NSDictionary* comments = [filteredArrayComm objectAtIndex:0];
                        //NSLog(@"%@",[favorite objectForKey:@"cntFav"]);
                        countComm = [comments objectForKey:@"cntComments"];
                    }
                    lblCom.text = [NSString stringWithFormat:@"%@", countComm];
                    //NSLog(@"filteredArrayComm %@", filteredArrayComm);
                } else {
                    btnimgViewCom = nil;
                    lblCom.text = nil;
                }
                
                
                if ([self showFavorite] == 1 || [self showComments] == 1) {
                    height = height + 40;
                }
                
                int i;
                BOOL callExit = NO;
                BOOL emailExit = NO;
                int tag1 = 0;
                int tag2 = 0;
                int tag3 = 0;
                NSString *titlename = @"";
                //NSString *imagename = @"";
                NSString *titlevalue = @"";
                
                for (i=0; i<2; i++) {
                    titlevalue = @"";
                    if ([contTel length]>0 && !callExit) {
                        
                        //NSLog(@"callExit : %@", callExit ? @"Yes" : @"No");
                        tag1 = 1001;
                        tag2 = 1002;
                        tag3 = 1003;
                        //imagename = [NSString stringWithFormat:@"%@_sgrey_call.png", appdelegate.themecolor];
                        titlename = contCallTitle;
                        titlevalue = contTel;
                        callExit = YES;
                        
                    } else if ([contEmail length]>0 && !emailExit) {
                        
                        tag1 = 2001;
                        tag2 = 2002;
                        tag3 = 2003;
                        //imagename = [NSString stringWithFormat:@"%@_sgrey_email.png", appdelegate.themecolor];
                        titlename = contEmailTitle;
                        titlevalue = contEmail;
                        emailExit = YES;
                        
                    }
                    
                    if ([titlevalue length]>0) {
                        
                        UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
                        button.tag = tag2;
                        [button addTarget:self action:@selector(checkButtonTapped:) forControlEvents:UIControlEventTouchUpInside];
                        [button setTitle:titlename forState:UIControlStateNormal];
                        button.titleLabel.font = [UIFont systemFontOfSize:12];
                        button.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
                        [button setTitleColor: [UIColor blackColor] forState: UIControlStateNormal];
                        if (i==0) {
                            button.frame = CGRectMake(5, height, (self.view.frame.size.width-10/2), 15);
                        } else {
                            button.frame = CGRectMake((self.view.frame.size.width/2)+5, height, (self.view.frame.size.width-10/2), 15);
                        }
                        [scrollview addSubview:button];
                        
                        UIButton *button1 = [UIButton buttonWithType:UIButtonTypeCustom];
                        button1.tag = tag3;
                        [button1 addTarget:self action:@selector(checkButtonTapped:) forControlEvents:UIControlEventTouchUpInside];
                        [button1 setTitle:titlevalue forState:UIControlStateNormal];
                        button1.titleLabel.font = [UIFont systemFontOfSize:12];
                        button1.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
                        [button1 setTitleColor: [UIColor blackColor] forState: UIControlStateNormal];
                        if (i==0) {
                            button1.frame = CGRectMake(5, height+15, (self.view.frame.size.width-10/2), 15);
                        } else {
                            button1.frame = CGRectMake((self.view.frame.size.width/2)+5, height+15, (self.view.frame.size.width-10/2), 15);
                        }
                        [scrollview addSubview:button1];
                    }
                    
                }
                
                if ([contTel length]>0 || [contEmail length]>0)
                    height = height + 35;
                
                
                if ([listtemplate isEqualToString:@"5"]) {
                    if ([price floatValue] > 0) {
                        pricetitle = [pricetitle stringByReplacingOccurrencesOfString:@"/n" withString:@"\n"];
                        
                        CGSize sizepricetitle;
                        CGRect framepricetitle = [pricetitle boundingRectWithSize:CGSizeMake(self.view.frame.size.width, CGFLOAT_MAX)
                                                                          options:NSStringDrawingUsesLineFragmentOrigin
                                                                       attributes:@{NSFontAttributeName:[UIFont systemFontOfSize:16]}
                                                                          context:nil];
                        sizepricetitle = CGSizeMake(framepricetitle.size.width, framepricetitle.size.height+1);
                        
                        UITextView *pricetitleTextView = [[UITextView alloc] initWithFrame:CGRectMake(0, height, self.view.frame.size.width, sizepricetitle.height+20)];
                        [pricetitleTextView setFont:[UIFont systemFontOfSize:16]];
                        pricetitleTextView.text = [NSString stringWithFormat:@"%@ :  %@", pricetitle, price];
                        pricetitleTextView.userInteractionEnabled = NO;
                        height = height +sizepricetitle.height+20;
                        //NSAttributedString *attributedString = [[NSAttributedString alloc] initWithData:[pricetitleTextView.text dataUsingEncoding:NSUnicodeStringEncoding] options:@{ NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType } documentAttributes:nil error:nil];
                        NSAttributedString *attributedString = [[NSAttributedString alloc] initWithData:[pricetitleTextView.text dataUsingEncoding:NSUTF8StringEncoding] options:@{ NSDocumentTypeDocumentAttribute: NSPlainTextDocumentType } documentAttributes:nil error:nil];
                        pricetitleTextView.attributedText = attributedString;
                        [scrollview addSubview:pricetitleTextView];
                    }
                    
                    if ([var1 length] > 0) {
                        var1title = [var1title stringByReplacingOccurrencesOfString:@"/n" withString:@"\n"];
                        
                        //CGSize size = [contDetails sizeWithFont:[UIFont systemFontOfSize:16]
                        //constrainedToSize:CGSizeMake(self.view.frame.size.width, FLT_MAX)
                        //lineBreakMode:NSLineBreakByTruncatingTail];
                        CGSize sizevar1title;
                        CGRect framevar1title = [var1title boundingRectWithSize:CGSizeMake(self.view.frame.size.width, CGFLOAT_MAX)
                                                                        options:NSStringDrawingUsesLineFragmentOrigin
                                                                     attributes:@{NSFontAttributeName:[UIFont systemFontOfSize:16]}
                                                                        context:nil];
                        sizevar1title = CGSizeMake(framevar1title.size.width, framevar1title.size.height+1);
                        
                        UITextView *var1titleTextView = [[UITextView alloc] initWithFrame:CGRectMake(0, height, self.view.frame.size.width, sizevar1title.height+20)];
                        [var1titleTextView setFont:[UIFont systemFontOfSize:14]];
                        var1titleTextView.text = [NSString stringWithFormat:@"%@ :  %@", var1title, var1];
                        var1titleTextView.userInteractionEnabled = NO;
                        height = height +sizevar1title.height+20;
                        NSAttributedString *attributedString = [[NSAttributedString alloc] initWithData:[var1titleTextView.text dataUsingEncoding:NSUTF8StringEncoding] options:@{ NSDocumentTypeDocumentAttribute: NSPlainTextDocumentType } documentAttributes:nil error:nil];
                        var1titleTextView.attributedText = attributedString;
                        [scrollview addSubview:var1titleTextView];
                    }
                    
                    if ([var2 length] > 0) {
                        var2title = [var2title stringByReplacingOccurrencesOfString:@"/n" withString:@"\n"];
                        
                        //CGSize size = [contDetails sizeWithFont:[UIFont systemFontOfSize:16]
                        //constrainedToSize:CGSizeMake(self.view.frame.size.width, FLT_MAX)
                        //lineBreakMode:NSLineBreakByTruncatingTail];
                        CGSize sizevar2title;
                        CGRect framevar2title = [var2title boundingRectWithSize:CGSizeMake(self.view.frame.size.width, CGFLOAT_MAX)
                                                                        options:NSStringDrawingUsesLineFragmentOrigin
                                                                     attributes:@{NSFontAttributeName:[UIFont systemFontOfSize:16]}
                                                                        context:nil];
                        sizevar2title = CGSizeMake(framevar2title.size.width, framevar2title.size.height+1);
                        
                        UITextView *var2titleTextView = [[UITextView alloc] initWithFrame:CGRectMake(0, height, self.view.frame.size.width, sizevar2title.height+20)];
                        [var2titleTextView setFont:[UIFont systemFontOfSize:14]];
                        var2titleTextView.text = [NSString stringWithFormat:@"%@ :  %@", var2title, var2];
                        var2titleTextView.userInteractionEnabled = NO;
                        height = height +sizevar2title.height+20;
                        NSAttributedString *attributedString = [[NSAttributedString alloc] initWithData:[var2titleTextView.text dataUsingEncoding:NSUTF8StringEncoding] options:@{ NSDocumentTypeDocumentAttribute: NSPlainTextDocumentType } documentAttributes:nil error:nil];
                        var2titleTextView.attributedText = attributedString;
                        [scrollview addSubview:var2titleTextView];
                    }
                    
                    if ([var3 length] > 0) {
                        var3title = [var3title stringByReplacingOccurrencesOfString:@"/n" withString:@"\n"];
                        
                        //CGSize size = [contDetails sizeWithFont:[UIFont systemFontOfSize:16]
                        //constrainedToSize:CGSizeMake(self.view.frame.size.width, FLT_MAX)
                        //lineBreakMode:NSLineBreakByTruncatingTail];
                        CGSize sizevar3title;
                        CGRect framevar3title = [var3title boundingRectWithSize:CGSizeMake(self.view.frame.size.width, CGFLOAT_MAX)
                                                                        options:NSStringDrawingUsesLineFragmentOrigin
                                                                     attributes:@{NSFontAttributeName:[UIFont systemFontOfSize:16]}
                                                                        context:nil];
                        sizevar3title = CGSizeMake(framevar3title.size.width, framevar3title.size.height+1);
                        
                        UITextView *var3titleTextView = [[UITextView alloc] initWithFrame:CGRectMake(0, height, self.view.frame.size.width, sizevar3title.height+20)];
                        [var3titleTextView setFont:[UIFont systemFontOfSize:14]];
                        var3titleTextView.text = [NSString stringWithFormat:@"%@ :  %@", var3title, var3];
                        var3titleTextView.userInteractionEnabled = NO;
                        height = height +sizevar3title.height+20;
                        NSAttributedString *attributedString = [[NSAttributedString alloc] initWithData:[var3titleTextView.text dataUsingEncoding:NSUTF8StringEncoding] options:@{ NSDocumentTypeDocumentAttribute: NSPlainTextDocumentType } documentAttributes:nil error:nil];
                        var3titleTextView.attributedText = attributedString;
                        [scrollview addSubview:var3titleTextView];
                    }
                    
                    if ([var4 length] > 0) {
                        var4title = [var4title stringByReplacingOccurrencesOfString:@"/n" withString:@"\n"];
                        
                        //CGSize size = [contDetails sizeWithFont:[UIFont systemFontOfSize:16]
                        //constrainedToSize:CGSizeMake(self.view.frame.size.width, FLT_MAX)
                        //lineBreakMode:NSLineBreakByTruncatingTail];
                        CGSize sizevar4title;
                        CGRect framevar4title = [var4title boundingRectWithSize:CGSizeMake(self.view.frame.size.width, CGFLOAT_MAX)
                                                                        options:NSStringDrawingUsesLineFragmentOrigin
                                                                     attributes:@{NSFontAttributeName:[UIFont systemFontOfSize:16]}
                                                                        context:nil];
                        sizevar4title = CGSizeMake(framevar4title.size.width, framevar4title.size.height+1);
                        
                        UITextView *var4titleTextView = [[UITextView alloc] initWithFrame:CGRectMake(0, height, self.view.frame.size.width, sizevar4title.height+20)];
                        [var4titleTextView setFont:[UIFont systemFontOfSize:14]];
                        var4titleTextView.text = [NSString stringWithFormat:@"%@ :  %@", var4title, var4];
                        var4titleTextView.userInteractionEnabled = NO;
                        height = height +sizevar4title.height+20;
                        NSAttributedString *attributedString = [[NSAttributedString alloc] initWithData:[var4titleTextView.text dataUsingEncoding:NSUTF8StringEncoding] options:@{ NSDocumentTypeDocumentAttribute: NSPlainTextDocumentType } documentAttributes:nil error:nil];
                        var4titleTextView.attributedText = attributedString;
                        [scrollview addSubview:var4titleTextView];
                    }
                    
                    if ([var5 length] > 0) {
                        var5title = [var5title stringByReplacingOccurrencesOfString:@"/n" withString:@"\n"];
                        
                        //CGSize size = [contDetails sizeWithFont:[UIFont systemFontOfSize:16]
                        //constrainedToSize:CGSizeMake(self.view.frame.size.width, FLT_MAX)
                        //lineBreakMode:NSLineBreakByTruncatingTail];
                        CGSize sizevar5title;
                        CGRect framevar5title = [var5title boundingRectWithSize:CGSizeMake(self.view.frame.size.width, CGFLOAT_MAX)
                                                                        options:NSStringDrawingUsesLineFragmentOrigin
                                                                     attributes:@{NSFontAttributeName:[UIFont systemFontOfSize:16]}
                                                                        context:nil];
                        sizevar5title = CGSizeMake(framevar5title.size.width, framevar5title.size.height+1);
                        
                        UITextView *var5titleTextView = [[UITextView alloc] initWithFrame:CGRectMake(0, height, self.view.frame.size.width, sizevar5title.height+20)];
                        [var5titleTextView setFont:[UIFont systemFontOfSize:14]];
                        var5titleTextView.text = [NSString stringWithFormat:@"%@ :  %@", var5title, var5];
                        var5titleTextView.userInteractionEnabled = NO;
                        height = height +sizevar5title.height+20;
                        NSAttributedString *attributedString = [[NSAttributedString alloc] initWithData:[var5titleTextView.text dataUsingEncoding:NSUTF8StringEncoding] options:@{ NSDocumentTypeDocumentAttribute: NSPlainTextDocumentType } documentAttributes:nil error:nil];
                        var5titleTextView.attributedText = attributedString;
                        [scrollview addSubview:var5titleTextView];
                    }
                }
                
                int heightwv;
                if ([contDetails length] > 0) {
                    /*contDetails = [contDetails stringByReplacingOccurrencesOfString:@"/n" withString:@"\n"];
                     
                     //CGSize size = [contDetails sizeWithFont:[UIFont systemFontOfSize:16]
                     //constrainedToSize:CGSizeMake(self.view.frame.size.width, FLT_MAX)
                     //lineBreakMode:NSLineBreakByTruncatingTail];
                     CGSize size;
                     CGRect frame = [contDetails boundingRectWithSize:CGSizeMake(self.view.frame.size.width-20, CGFLOAT_MAX)
                     options:NSStringDrawingUsesLineFragmentOrigin
                     attributes:@{NSFontAttributeName:[UIFont systemFontOfSize:18]}
                     context:nil];
                     size = CGSizeMake(frame.size.width, frame.size.height);
                     
                     UITextView *newTextView = [[UITextView alloc] initWithFrame:CGRectMake(0, height, self.view.frame.size.width, size.height)];
                     [newTextView setFont:[UIFont systemFontOfSize:16]];
                     newTextView.text = contDetails;
                     newTextView.userInteractionEnabled = NO;
                     height = height + size.height;
                     NSAttributedString *attributedString = [[NSAttributedString alloc] initWithData:[newTextView.text dataUsingEncoding:NSUTF8StringEncoding] options:@{ NSDocumentTypeDocumentAttribute: NSPlainTextDocumentType } documentAttributes:nil error:nil];
                     newTextView.attributedText = attributedString;
                     [scrollview addSubview:newTextView]; */
                    contDetails = [contDetails stringByReplacingOccurrencesOfString:@"/n" withString:@"\n"];
                    
                    UIFont *yourFont = [UIFont fontWithName:@"Helvetica" size:14];
                    CGSize size;
                    CGRect frame = [contDetails boundingRectWithSize:CGSizeMake(self.view.frame.size.width-20, CGFLOAT_MAX)
                                                             options:NSStringDrawingUsesLineFragmentOrigin
                                                          attributes:@{NSFontAttributeName:yourFont}
                                                             context:nil];
                    size = CGSizeMake(frame.size.width, frame.size.height);
                    
                    wv = [[UIWebView alloc] init];
                    heightwv = size.height+40;
                    [wv setFrame:CGRectMake(0, height, self.view.frame.size.width, heightwv)];
                    NSString *htmlstring = [NSString stringWithFormat:@"<html><body><div style='max-width: 100%%;font-size:14;font-family:Helvetica;color:#000000;'>%@</body></html>", contDetails];
                    [wv loadHTMLString:htmlstring baseURL:nil];
                    //wv.userInteractionEnabled = NO;
                    UIView* row = nil;
                    for(row in wv.subviews){
                        if([row isKindOfClass:[UIScrollView class] ]){
                            UIScrollView* scrollRow = (UIScrollView*) row;
                            scrollRow.scrollEnabled = YES;
                            scrollRow.bounces = NO;
                            scrollRow.backgroundColor=[UIColor clearColor];
                        }
                    }
                    wv.alpha=0.6f;
                    [wv setBackgroundColor:[UIColor whiteColor]];
                    [wv setOpaque:NO];
                    [scrollview addSubview:wv];
                }
                
                tableView = [[UITableView alloc] initWithFrame:CGRectMake(0, height+heightwv, self.view.frame.size.width, ([MenuItems count]*44)+50) style:UITableViewStyleGrouped];
                
                // must set delegate & dataSource, otherwise the the table will be empty and not responsive
                tableView.delegate = self;
                tableView.dataSource = self;
                
                tableView.backgroundView = nil;
                tableView.backgroundColor = [UIColor clearColor];
                
                // add to canvas
                [scrollview addSubview:tableView];
                
                scrollview.contentSize = CGSizeMake(scrollview.frame.size.width, 40+height+heightwv +([MenuItems count]*44));
            }
            
        }
        
        scrollview =nil;
        
    }
    sqlite3_close(database);
}


- (void)checkButtonTapped:(id)sender {
    
    UIButton *clicked = (UIButton *) sender;
    //NSLog(@"%d",clicked.tag);
    if ([listOfContent count] > 0) {
        NSDictionary *dictionary = [listOfContent objectAtIndex:0];
        
        NSString *contEmail = [dictionary objectForKey:@"email"];
        NSString *contTel = [dictionary objectForKey:@"telephone"];
        if (clicked.tag==1001 || clicked.tag==1002 || clicked.tag==1003) {
            //[[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"tel:+60123456789"]];
            if ([contTel length]>0) {
                NSString *strPhone = [NSString stringWithFormat:@"telprompt://%@", contTel];
                //NSLog(@"strPhone:: %@", strPhone);
                [[UIApplication sharedApplication] openURL:[NSURL URLWithString:strPhone]];
            }
        }
        if (clicked.tag==2001 || clicked.tag==2002 || clicked.tag==2003) {
            if ([contEmail length]>0) {
                NSArray *toRecipents = [NSArray arrayWithObject:contEmail];
            
                MFMailComposeViewController *mc = [[MFMailComposeViewController alloc] init];
                mc.mailComposeDelegate = self;
                //[mc setSubject:emailTitle];
                //[mc setMessageBody:messageBody isHTML:YES];
                [mc setToRecipients:toRecipents];
            
                // Present mail view controller on screen
                //[self.view.window.rootViewController presentViewController:mc animated:YES completion:NULL];
                if([MFMailComposeViewController canSendMail])
                {
                    [self presentViewController:mc animated:NO completion:NULL];
                    //[self.navigationController pushViewController:mc animated:YES];
                }
            }
        }
        
    }
    
}

-(void)mailComposeController:(MFMailComposeViewController *)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError *)error {
    
    switch (result)
    {
        case MFMailComposeResultCancelled:
            NSLog(@"Mail cancelled");
            break;
        case MFMailComposeResultSaved:
            NSLog(@"Mail saved");
            break;
        case MFMailComposeResultSent:
            NSLog(@"Mail sent");
            break;
        case MFMailComposeResultFailed:
            NSLog(@"Mail sent failure: %@", [error localizedDescription]);
            break;
        default:
            break;
    }
    
    [self dismissViewControllerAnimated:NO completion:nil];
}

#pragma mark - UITableViewDataSource
// number of section(s), now I assume there is only 1 section
- (NSInteger)numberOfSectionsInTableView:(UITableView *)theTableView
{
    return 1;
}

// number of row in the section, I assume there is only 1 row
- (NSInteger)tableView:(UITableView *)theTableView numberOfRowsInSection:(NSInteger)section
{
    //return 8;
    
    return [MenuItems count];
}

// the cell will be returned to the tableView
- (UITableViewCell *)tableView:(UITableView *)theTableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    //AppDelegate *appdelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    
    static NSString *cellIdentifier = @"HistoryCell";
    
    // Similar to UITableViewCell, but
    ItemCell *cell = (ItemCell *)[theTableView dequeueReusableCellWithIdentifier:cellIdentifier];
    
    if (cell == nil) {
        cell = [[ItemCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier];
        cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
    }
    
    if ([listOfContent count] > 0) {
        NSDictionary *dictionary = [listOfContent objectAtIndex:0];
        
        NSString *linkTitle = [dictionary objectForKey:@"link_title"];
        NSString *linkUrl = [dictionary objectForKey:@"link_url"];
        NSString *contMapTitle = [dictionary objectForKey:@"map_title"];
        float contLatitud = [[dictionary objectForKey:@"map_lat"] floatValue];
        float contLongitud = [[dictionary objectForKey:@"map_lon"] floatValue];
        NSString *videoTitle = [dictionary objectForKey:@"video_title"];
        NSString *videoUrl = [dictionary objectForKey:@"video_url"];
        NSString *fbtitle = [dictionary objectForKey:@"fb_title"];
        NSString *fbUrl = [dictionary objectForKey:@"fb_url"];
        
        
        if ([[MenuItems objectAtIndex:indexPath.row] isEqualToString:@"Map"]) {
            
            cell.labelname.text = contMapTitle;
            cell.labelvalue.text = [NSString stringWithFormat:@"%f, %f", contLatitud,contLongitud];
            //break;
            
        }
        
        if ([[MenuItems objectAtIndex:indexPath.row] isEqualToString:@"Link"]) {
            
            cell.labelname.text = linkTitle;
            cell.labelvalue.text = linkUrl;
            //break;
            
        }
        
        if ([[MenuItems objectAtIndex:indexPath.row] isEqualToString:@"Video"]) {
            
            cell.labelname.text = videoTitle;
            cell.labelvalue.text = videoUrl;
            //break;
            
        }
        
        if ([[MenuItems objectAtIndex:indexPath.row] isEqualToString:@"Facebook"]) {
            
            cell.labelname.text = fbtitle;
            cell.labelvalue.text = fbUrl;
            //break;
            
        }
        
    }
    return cell;
    
    //return nil;
}

#pragma mark - UITableViewDelegate
// when user tap the row, what action you want to perform
- (void)tableView:(UITableView *)theTableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    //NSLog(@"selected %d row", indexPath.row);
    if ([listOfContent count] > 0) {
        NSDictionary *dictionary = [listOfContent objectAtIndex:0];
        
        NSString *linkTitle = [dictionary objectForKey:@"link_title"];
        NSString *linkUrl = [dictionary objectForKey:@"link_url"];
        NSString *contMapTitle = [dictionary objectForKey:@"map_title"];
        float contLatitud = [[dictionary objectForKey:@"map_lat"] floatValue];
        float contLongitud = [[dictionary objectForKey:@"map_lon"] floatValue];
        NSString *videoTitle = [dictionary objectForKey:@"video_title"];
        NSString *videoUrl = [dictionary objectForKey:@"video_url"];
        NSString *fbtitle = [dictionary objectForKey:@"fb_title"];
        NSString *fbUrl = [dictionary objectForKey:@"fb_url"];
        NSString *var1 = [dictionary objectForKey:@"var1"];
        NSString *menuTitle = [dictionary objectForKey:@"title"];
        
        if ([[MenuItems objectAtIndex:indexPath.row] isEqualToString:@"Map"]) {
            //break;
            cont_type = @"Map";
            cont_title = contMapTitle;
            cont_value1 = [NSString stringWithFormat:@"%f", contLatitud];
            cont_value2 = [NSString stringWithFormat:@"%f", contLongitud];
            contvar1 = [NSString stringWithFormat:@"%@", var1];
            menutitle = [NSString stringWithFormat:@"%@", menuTitle];
            [self goNextPage] ;
        }
        
        if ([[MenuItems objectAtIndex:indexPath.row] isEqualToString:@"Link"]) {
            //break;
            cont_type = @"Link";
            cont_title = linkTitle;
            cont_value1 = linkUrl;
            cont_value2 = @"";
            [self goNextPage] ;
        }
        
        if ([[MenuItems objectAtIndex:indexPath.row] isEqualToString:@"Video"]) {
            //break;
            cont_type = @"Video";
            cont_title = videoTitle;
            cont_value1 = videoUrl;
            cont_value2 = @"";
            [self goNextPage] ;
        }
        //case 6: {
        if ([[MenuItems objectAtIndex:indexPath.row] isEqualToString:@"Facebook"]) {
            //break;
            cont_type = @"Facebook";
            cont_title = fbtitle;
            cont_value1 = fbUrl;
            cont_value2 = @"";
            [self goNextPage] ;
        }
        //}
    }

}

-(void)goNextPage {
    
    //NSLog(@"segue.identifier=%@", segue.identifier);
    appdelegate.latitude = cont_value1;
    appdelegate.longitude = cont_value2;
    ContentPluginDetailsViewController1 *contentPluginDetailsViewController1 = [[ContentPluginDetailsViewController1 alloc] init];
    contentPluginDetailsViewController1.cont_value1 = cont_value1;
    contentPluginDetailsViewController1.cont_value2 = cont_value2;
    contentPluginDetailsViewController1.cont_type = cont_type;
    contentPluginDetailsViewController1.cont_title = cont_title;
    contentPluginDetailsViewController1.contId = contId;
    contentPluginDetailsViewController1.listtemplate = listtemplate;
    contentPluginDetailsViewController1.cont_var1 = contvar1;
    contentPluginDetailsViewController1.menu_title = menutitle;
    [appdelegate.navController pushViewController:contentPluginDetailsViewController1 animated:NO];
}

- (void) viewWillDisappear:(BOOL)animated {
    cont_value1 =nil;
    cont_value2 =nil;
    cont_title =nil;
    cont_type =nil;
    listtemplate =nil;
    contvar1 =nil;
    menutitle =nil;
    
    [scrollview removeFromSuperview];
    scrollview = nil;
}

- (void) setImagesGallery:(UIScrollView *)scrollview1 {
    
    if ([self.galleryImages count]>0) {
        scrollview1.tag = 1;
        scrollview1.autoresizingMask=UIViewAutoresizingNone;
        [self setupScrollView:scrollview1];
    }
}

- (void)setupScrollView:(UIScrollView*)scrMain {
    // we have 10 images here.
    // we will add all images into a scrollView & set the appropriate size.
    
    for (int i=1; i<=[self.galleryImages count]; i++) {
        
        UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
        button.frame = CGRectMake((i-1)*scrMain.frame.size.width, 0, scrMain.frame.size.width, scrMain.frame.size.height);
        //button.tag = i;
        NSDictionary *dictionary = [self.galleryImages objectAtIndex:i-1];
        //NSString *filePath = [self.galleryImages objectAtIndex:i-1];
        NSString *filePath = [dictionary objectForKey:@"image"];
        
        NSRange end = [filePath rangeOfString:@"/" options:NSBackwardsSearch];
        NSString *shortString;
        if (end.location != NSNotFound) {
            shortString =[filePath substringWithRange:NSMakeRange(end.location+1, filePath.length-(end.location+1))];
        } else {
            shortString = filePath;
        }
        
        BOOL success;
        
        // Create a FileManager object, we will use this to check the status
        // of the database and to copy it over if required
        NSFileManager *fileManager = [NSFileManager defaultManager];
        
        // Check if the database has already been created in the users filesystem
        success = [fileManager fileExistsAtPath:[[[NSBundle mainBundle] resourcePath] stringByAppendingPathComponent:shortString]];
        
        /*if ([[dictionary objectForKey:@"action_value"] intValue] > 0) {
            if ([[dictionary objectForKey:@"action_type"] isEqualToString:@"page"] || [[dictionary objectForKey:@"action_type"] isEqualToString:@"plugin"]) {
                button.tag = [[dictionary objectForKey:@"action_value"] intValue];
                
                if ([[dictionary objectForKey:@"action_type"] isEqualToString:@"page"]) {
                    [button addTarget:self
                               action:@selector(checkButtonClick:)
                     forControlEvents:UIControlEventTouchUpInside];
                } else {
                    [button addTarget:self
                               action:@selector(checkButtonPluginClick:)
                     forControlEvents:UIControlEventTouchUpInside];
                }
            }
        } */
        //UIImageView *imgV = [[UIImageView alloc] initWithFrame:CGRectMake((i-1)*scrMain.frame.size.width, 0, scrMain.frame.size.width, scrMain.frame.size.height)];
        UIImageView *imgV = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, button.frame.size.width, button.frame.size.height)];
        //UIImageView *imgV = [[UIImageView alloc] initWithFrame:CGRectMake((button.frame.size.width/4)/2, 0, button.frame.size.width-(button.frame.size.width/4), button.frame.size.height)];
        // set scale to fill
        [scrMain addSubview:button];
        
        [button addSubview:imgV];
        
        __block UIActivityIndicatorView *activityIndicator;
        __weak UIImageView *weakImageView = imgV;
        NSString *imagePath;
        if(success) {
            imagePath = [[[NSBundle mainBundle] resourcePath] stringByAppendingPathComponent:shortString];
            [weakImageView setImage:[UIImage imageNamed:shortString]];
        } else {
            imagePath = filePath;
            /*[weakImageView setImageWithURL:[NSURL URLWithString:imagePath] placeholderImage:[UIImage imageNamed:@"No-Image-icon.png"] options:SDWebImageProgressiveDownload progress:^(NSInteger receivedSize, NSInteger expectedSize)
             {
                 if (!activityIndicator)
                 {
                     [weakImageView addSubview:activityIndicator = [UIActivityIndicatorView.alloc initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray]];
                     activityIndicator.center = weakImageView.center;
                     [activityIndicator startAnimating];
                 }
             }
                                 completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType)
             {
                 [activityIndicator removeFromSuperview];
                 activityIndicator = nil;
             }]; */
            dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0), ^ {
                [weakImageView sd_setImageWithURL:[NSURL URLWithString:imagePath]
                                 placeholderImage:[UIImage imageNamed:@"No-Image-icon.png"]
                                          options:SDWebImageProgressiveDownload
                                         progress:^(NSInteger receivedSize, NSInteger expectedSize, NSURL *targetURL) {
                                             dispatch_async(dispatch_get_main_queue(), ^ {
                                                 if (!activityIndicator) {
                                                     
                                                     [weakImageView addSubview:activityIndicator = [UIActivityIndicatorView.alloc initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray]];
                                                     activityIndicator.center = weakImageView.center;
                                                     [activityIndicator startAnimating];
                                                 }
                                             });
                                         }
                                        completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
                                            dispatch_async(dispatch_get_main_queue(), ^ {
                                                //[MBProgressHUD hideHUDForView:self.view animated:YES];
                                                [activityIndicator removeFromSuperview];
                                                activityIndicator = nil;
                                            });
                                        }];
            });

        }
        //NSLog(@"shortStringimagePath : %@", imagePath);
        imgV.contentMode  = UIViewContentModeScaleAspectFill;
        [imgV setClipsToBounds:YES];
    }
    // set the content size to 10 image width
    [scrMain setContentSize:CGSizeMake(scrMain.frame.size.width*[self.galleryImages count], 40+scrMain.frame.size.height)];
    // enable timer after each 2 seconds for scrolling.
    if ([self.galleryImages count]>1) {
        [NSTimer scheduledTimerWithTimeInterval:delaytime target:self selector:@selector(scrollingTimer) userInfo:nil repeats:YES];
    }
    
}

- (void)scrollingTimer {
    // access the scroll view with the tag
    UIScrollView *scrMain = (UIScrollView*) [self.view viewWithTag:1];
    // same way, access pagecontroll access
    UIPageControl *pgCtr = (UIPageControl*) [self.view viewWithTag:12];
    // get the current offset ( which page is being displayed )
    CGFloat contentOffset = scrMain.contentOffset.x;
    // calculate next page to display
    int nextPage = (int)(contentOffset/scrMain.frame.size.width) + 1 ;
    // if page is not 10, display it
    if( nextPage!=[self.galleryImages count] )  {
        [scrMain scrollRectToVisible:CGRectMake(nextPage*scrMain.frame.size.width, 0, scrMain.frame.size.width, scrMain.frame.size.height) animated:NO];
        pgCtr.currentPage=nextPage;
        // else start sliding form 1 :)
    } else {
        [scrMain scrollRectToVisible:CGRectMake(0, 0, scrMain.frame.size.width, scrMain.frame.size.height) animated:NO];
        pgCtr.currentPage=0;
    }
}

/*** Push Notification 27Aug2015 ***/
-(void) viewDidDisappear:(BOOL)animated {
    
    appdelegate.notify_actiontype = @"";
    appdelegate.notify_actionvalue = @"";
    appdelegate.notify_contentid = @"";
    
    appdelegate.homeclicked = NO;
    [modalController dismissViewControllerAnimated:YES completion:nil];
    
    [super viewDidDisappear:animated];
    
}
/*** Push Notification 27Aug2015 ***/

-(NSInteger) showFavorite {
    sqlite3 *database;
    NSString *allowFav;
    
    if(sqlite3_open([appdelegate.databasePath UTF8String], &database) == SQLITE_OK) {
        
        //if ([aplugintype isEqualToString:@"content"]) {
        
        NSString *sqlFav;
        /*** Push Notification 27Aug2015 ***/
        sqlFav = [[NSString alloc] initWithFormat:@"SELECT * from content_category WHERE top_id = %@", appdelegate.plugin_id];
        //NSLog(@"showFavorite:: %@", sqlFav);
        
        const char *sqlStatementFav = [sqlFav UTF8String];
        sqlite3_stmt *compiledStatementFav;
        if(sqlite3_prepare_v2(database, sqlStatementFav, -1, &compiledStatementFav, NULL) == SQLITE_OK) {
            while(sqlite3_step(compiledStatementFav) == SQLITE_ROW) {
                
                allowFav = [NSString stringWithUTF8String:(char *)sqlite3_column_text(compiledStatementFav, 9)];
                
            }
        }
        // Release the compiled statement from memory
        sqlite3_finalize(compiledStatementFav);
        
    }
    sqlite3_close(database);
    
    if ([allowFav isEqualToString:@"1"]) {
        return 1;
    } else {
        return 0;
    }
}

-(NSInteger) showComments {
    sqlite3 *database;
    NSString *allowCom;
    
    if(sqlite3_open([appdelegate.databasePath UTF8String], &database) == SQLITE_OK) {
        
        //if ([aplugintype isEqualToString:@"content"]) {
        
        NSString *sqlCom;
        /*** Push Notification 27Aug2015 ***/
        sqlCom = [[NSString alloc] initWithFormat:@"SELECT * from content_category WHERE top_id = %@", appdelegate.plugin_id];
        //NSLog(@"appdelegate.plugin_id:: %@", appdelegate.plugin_id);
        
        const char *sqlStatementCom = [sqlCom UTF8String];
        sqlite3_stmt *compiledStatementCom;
        if(sqlite3_prepare_v2(database, sqlStatementCom, -1, &compiledStatementCom, NULL) == SQLITE_OK) {
            while(sqlite3_step(compiledStatementCom) == SQLITE_ROW) {
                
                allowCom = [NSString stringWithUTF8String:(char *)sqlite3_column_text(compiledStatementCom, 10)];
                
            }
        }
        // Release the compiled statement from memory
        sqlite3_finalize(compiledStatementCom);
        
    }
    sqlite3_close(database);
    
    if ([allowCom isEqualToString:@"1"]) {
        return 1;
    } else {
        return 0;
    }
}

- (void)checkButtonFavTapped:(id)sender event:(id)event
{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSString *hasLoggedInFB = [defaults objectForKey:@"hasLoggedInFB"];
    NSString *hasLoggedInMyApp = [defaults objectForKey:@"hasLoggedInMyApp"];
    
    if ([hasLoggedInFB isEqualToString:@"YES"] || [hasLoggedInMyApp isEqualToString:@"YES"])
    {
                NSString *urlAddress2 = [[NSString alloc] initWithFormat:@"%@favorite_process.php?db=%@&userid=%@&udid=%@&cuid=%@&top=%@&cid=%@", appdelegate.user_apipath, [self urlEncode:appdelegate.db], [self urlEncode:appdelegate.user], [self urlEncode:appdelegate.uuid], [self urlEncode:appdelegate.customer_id], appdelegate.plugin_id, contId];
                currentRequest = @"AddFavorite";
                [self connectToServer:urlAddress2];
    }
    else {
        [self.view addSubview:view_popoverMain];
        [view_popoverMain addSubview:view_plain];
        [view_plain addSubview:buttonclose];
        [view_plain addSubview:buttonlogin];
        [view_plain addSubview:lbllogin];
        //[view_popoverMain setHidden:NO];
        //[self.view bringSubviewToFront:view_popoverMain];
    }
}

- (void)checkButtonCommentsTapped:(id)sender event:(id)event
{
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSString *hasLoggedInFB = [defaults objectForKey:@"hasLoggedInFB"];
    NSString *hasLoggedInMyApp = [defaults objectForKey:@"hasLoggedInMyApp"];
    
    if ([hasLoggedInFB isEqualToString:@"YES"] || [hasLoggedInMyApp isEqualToString:@"YES"])
    {
        
                CommentsViewController *commentsViewController = [[CommentsViewController alloc] init];
                commentsViewController.conttitle = conttitle;
                commentsViewController.contId = contId;
                commentsViewController.topId = appdelegate.plugin_id;
                [appdelegate.navController pushViewController:commentsViewController animated:NO];
    }
    else {
        [self.view addSubview:view_popoverMain];
        [view_popoverMain addSubview:view_plain];
        [view_plain addSubview:buttonclose];
        [view_plain addSubview:buttonlogin];
        [view_plain addSubview:lbllogin];
        //[view_popoverMain setHidden:NO];
        //[self.view bringSubviewToFront:view_popoverMain];
    }
}

-(NSString *) urlEncode:(NSString *) theStr {
    /*NSString *encoded = (NSString *)CFURLCreateStringByAddingPercentEscapes(
     NULL,
     (CFStringRef)theStr,
     NULL,
     (CFStringRef)@"!*'\"();:@&=+$,/?%#[]% ",
     //																		NSUnicodeStringEncoding );
     kCFStringEncodingUTF8 ); */
    NSString *encoded = (NSString *)CFBridgingRelease(CFURLCreateStringByAddingPercentEscapes(
                                                                                              NULL,
                                                                                              (__bridge CFStringRef) theStr,
                                                                                              NULL,
                                                                                              CFSTR("!*'();:@&=+$,/?%#[]\" "),
                                                                                              kCFStringEncodingUTF8));
    return encoded;
    //return nil;
}

#pragma mark CONNECT TO SERVER
-(void) connectToServer:(NSString *) urlPath {
    
    //[self addLoadingView];
    NSString *urlAddress =[NSString stringWithFormat:@"%@", urlPath];
    
    //NSLog(@"URL: %@", urlAddress);
    //Create a URL object.
    /*NSURL *url = [NSURL URLWithString:urlAddress];
    
    //URL Requst Object
    //NSURLRequest *requestObj = [NSURLRequest requestWithURL:url];
    NSURLRequest *requestObj = [NSURLRequest requestWithURL:url
                                                cachePolicy:NSURLRequestReloadIgnoringLocalAndRemoteCacheData
                                            timeoutInterval:30];
    
    NSURLConnection *theConnection=[[NSURLConnection alloc] initWithRequest:requestObj delegate:self];
    if (theConnection)
    {
        // Create the NSMutableData to hold the received data.
        receivedData = [NSMutableData data];
    }
    else
    {
        // Inform the user that the connection failed.
        //[self displayAlert:@"Connection Error. Please try again later."];
        [self displayAlert:NSLocalizedString(@"MSG_CONNECTION_ERROR", nil)];
        //[self removeLoadingView];
    } */
    [self getJsonResponse:urlAddress success:^(NSDictionary *responseDict) {
        
        //NSLog(@"%@",responseDict);
        [self successfunction:responseDict];
        
    } failure:^(NSError *error) {
        // release the connection, and the data object
        
        [self processdata];
        [self displayAlert:NSLocalizedString(@"MSG_CONNECTION_ERROR", nil)];
    }];
}

-(void)getJsonResponse : (NSString *)urlStr success : (void (^)(NSDictionary *responseDict))success failure:(void(^)(NSError* error))failure
{
    
    
    NSURLSession * session = [NSURLSession sharedSession];
    NSURL * url = [NSURL URLWithString: urlStr];
    
    
    // Asynchronously Api is hit here
    NSURLSessionDataTask * dataTask = [session dataTaskWithURL:url completionHandler:^(NSData *data, NSURLResponse *response, NSError *error)
                                       {
                                           
                                           //NSLog(@"%@",data);
                                           NSDictionary * json;
                                           if ([data length]>0) {
                                               json  = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
                                               //NSLog(@"%@",json);
                                               success(json);
                                           } else {
                                               json = nil;
                                               dispatch_async(dispatch_get_main_queue(), ^ {
                                                   [self displayAlert:NSLocalizedString(@"MSG_CONNECTION_ERROR", nil)];
                                               });
                                           }              
                                           
                                           
                                       }];
    
    [dataTask resume] ; // Executed First
    
    
}

-(void) successfunction:(NSDictionary *)responseDict {
    dispatch_async(dispatch_get_main_queue(), ^{
        //NSString *jsonString = [responseDict JSONRepresentation];
        if ([currentRequest isEqualToString:@"counterFav"]) {
            //if([receivedData length]>10)
            //{
            NSDictionary *dictionary = responseDict;
            //NSLog(@"dictionaryFav %@",dictionary);
            [listOfContentsFav removeAllObjects];
            for(NSDictionary *row in dictionary)
            {
                dicContentsFav = [NSDictionary dictionaryWithObjectsAndKeys:[row objectForKey:@"content_id"],@"id",[row objectForKey:@"cntFav"],@"cntFav",nil];
                [listOfContentsFav addObject:dicContentsFav];
            }
            //NSLog(@"listOfContentsFav %@",listOfContentsFav);
            //}
            
            NSString *urlAddress1 = [[NSString alloc] initWithFormat:@"%@comments_counter.php?db=%@&userid=%@&udid=%@&cuid=%@&top=%@", appdelegate.user_apipath, [self urlEncode:appdelegate.db], [self urlEncode:appdelegate.user], [self urlEncode:appdelegate.uuid], [self urlEncode:appdelegate.customer_id], appdelegate.plugin_id];
            currentRequest = @"counterComments";
            //NSLog(@"appdelegate.user_apipath:: %@", appdelegate.user_apipath);
            //NSLog(@"urlAddress:: %@", urlAddress);
            [self connectToServer:urlAddress1];
            
        } else if ([currentRequest isEqualToString:@"counterComments"]) {
            //if([receivedData length]>10)
            //{
            NSDictionary *dictionary = responseDict;
            //NSLog(@"dictionaryComments %@",dictionary);
            [listOfContentsComments removeAllObjects];
            for(NSDictionary *row in dictionary)
            {
                dicContentsComments = [NSDictionary dictionaryWithObjectsAndKeys:[row objectForKey:@"content_id"],@"id",[row objectForKey:@"cntComments"],@"cntComments",nil];
                [listOfContentsComments addObject:dicContentsComments];
            }
            //NSLog(@"listOfContentsComments %@",listOfContentsComments);
            //}
            NSString *urlAddress3 = [[NSString alloc] initWithFormat:@"%@favorite_list.php?db=%@&userid=%@&udid=%@&cuid=%@&top=%@", appdelegate.user_apipath, [self urlEncode:appdelegate.db], [self urlEncode:appdelegate.user], [self urlEncode:appdelegate.uuid], [self urlEncode:appdelegate.customer_id], appdelegate.plugin_id];
            currentRequest = @"favoriteList";
            //NSLog(@"appdelegate.user_apipath:: %@", appdelegate.user_apipath);
            //NSLog(@"urlAddress:: %@", urlAddress);
            [self connectToServer:urlAddress3];
            
        } else if ([currentRequest isEqualToString:@"favoriteList"]) {
            //if([receivedData length]>10)
            //{
            NSDictionary *dictionary = responseDict;
            //NSLog(@"FavoritelisDictionary %@",dictionary);
            [listOfFav removeAllObjects];
            for(NSDictionary *row in dictionary)
            {
                dicFav = [NSDictionary dictionaryWithObjectsAndKeys:[row objectForKey:@"content_id"],@"id",nil];
                [listOfFav addObject:dicFav];
            }
            //NSLog(@"contentfavid %@",contentfavid);
            [self processdata];
            //}
            
        } else if ([currentRequest isEqualToString:@"AddFavorite"]){
            //if([receivedData length]>10)
            //{
            NSDictionary *dictionaryadddel = responseDict;
            //NSLog(@"AddFavorite %@",dictionaryadddel);
            for(NSDictionary *row in dictionaryadddel)
            {
                if ([[row objectForKey:@"status"] isEqualToString:@"1"]) {
                    NSString *urlAddress = [[NSString alloc] initWithFormat:@"%@favorite_counter.php?db=%@&userid=%@&udid=%@&cuid=%@&top=%@", appdelegate.user_apipath, [self urlEncode:appdelegate.db], [self urlEncode:appdelegate.user], [self urlEncode:appdelegate.uuid], [self urlEncode:appdelegate.customer_id], appdelegate.plugin_id];
                    currentRequest = @"counterFav";
                    //NSLog(@"appdelegate.user_apipath:: %@", appdelegate.user_apipath);
                    //NSLog(@"urlAddress:: %@", urlAddress);
                    [self connectToServer:urlAddress];
                }
            }
            //[self processdata];
            //}
        }
        //response =nil;
    });

}

/*- (void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response
{
    [receivedData setLength:0];
}

- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data
{
    [receivedData appendData:data];
}

- (void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error {
    // release the connection, and the data object
    
    connection =nil;
    receivedData =nil;
    
    //NSLog(@"%@",error.description);
    
    //[self removeLoadingView];
    [self processdata];
    [self displayAlert:NSLocalizedString(@"MSG_CONNECTION_ERROR", nil)];
}

- (void)connectionDidFinishLoading:(NSURLConnection *)connection
{
    
    NSString *response = [[NSString alloc] initWithData:receivedData encoding:NSUTF8StringEncoding];
    //NSLog(@"response %@",response);
    if ([currentRequest isEqualToString:@"counterFav"]) {
        //if([receivedData length]>10)
        //{
        NSDictionary *dictionary = [response JSONValue];
        //NSLog(@"dictionaryFav %@",dictionary);
        [listOfContentsFav removeAllObjects];
        for(NSDictionary *row in dictionary)
        {
            dicContentsFav = [NSDictionary dictionaryWithObjectsAndKeys:[row objectForKey:@"content_id"],@"id",[row objectForKey:@"cntFav"],@"cntFav",nil];
            [listOfContentsFav addObject:dicContentsFav];
        }
        //NSLog(@"listOfContentsFav %@",listOfContentsFav);
        //}
        
        NSString *urlAddress1 = [[NSString alloc] initWithFormat:@"%@comments_counter.php?db=%@&userid=%@&udid=%@&cuid=%@&top=%@", appdelegate.user_apipath, [self urlEncode:appdelegate.db], [self urlEncode:appdelegate.user], [self urlEncode:appdelegate.uuid], [self urlEncode:appdelegate.customer_id], appdelegate.plugin_id];
        currentRequest = @"counterComments";
        //NSLog(@"appdelegate.user_apipath:: %@", appdelegate.user_apipath);
        //NSLog(@"urlAddress:: %@", urlAddress);
        [self connectToServer:urlAddress1];
        
    } else if ([currentRequest isEqualToString:@"counterComments"]) {
        //if([receivedData length]>10)
        //{
        NSDictionary *dictionary = [response JSONValue];
        //NSLog(@"dictionaryComments %@",dictionary);
        [listOfContentsComments removeAllObjects];
        for(NSDictionary *row in dictionary)
        {
            dicContentsComments = [NSDictionary dictionaryWithObjectsAndKeys:[row objectForKey:@"content_id"],@"id",[row objectForKey:@"cntComments"],@"cntComments",nil];
            [listOfContentsComments addObject:dicContentsComments];
        }
        //NSLog(@"listOfContentsComments %@",listOfContentsComments);
        //}
        NSString *urlAddress3 = [[NSString alloc] initWithFormat:@"%@favorite_list.php?db=%@&userid=%@&udid=%@&cuid=%@&top=%@", appdelegate.user_apipath, [self urlEncode:appdelegate.db], [self urlEncode:appdelegate.user], [self urlEncode:appdelegate.uuid], [self urlEncode:appdelegate.customer_id], appdelegate.plugin_id];
        currentRequest = @"favoriteList";
        //NSLog(@"appdelegate.user_apipath:: %@", appdelegate.user_apipath);
        //NSLog(@"urlAddress:: %@", urlAddress);
        [self connectToServer:urlAddress3];
        
    } else if ([currentRequest isEqualToString:@"favoriteList"]) {
        //if([receivedData length]>10)
        //{
        NSDictionary *dictionary = [response JSONValue];
        //NSLog(@"FavoritelisDictionary %@",dictionary);
        [listOfFav removeAllObjects];
        for(NSDictionary *row in dictionary)
        {
            dicFav = [NSDictionary dictionaryWithObjectsAndKeys:[row objectForKey:@"content_id"],@"id",nil];
            [listOfFav addObject:dicFav];
        }
        //NSLog(@"contentfavid %@",contentfavid);
        [self processdata];
        //}
        
    } else if ([currentRequest isEqualToString:@"AddFavorite"]){
        //if([receivedData length]>10)
        //{
        NSDictionary *dictionaryadddel = [response JSONValue];
        //NSLog(@"AddFavorite %@",dictionaryadddel);
        for(NSDictionary *row in dictionaryadddel)
        {
            if ([[row objectForKey:@"status"] isEqualToString:@"1"]) {
                NSString *urlAddress = [[NSString alloc] initWithFormat:@"%@favorite_counter.php?db=%@&userid=%@&udid=%@&cuid=%@&top=%@", appdelegate.user_apipath, [self urlEncode:appdelegate.db], [self urlEncode:appdelegate.user], [self urlEncode:appdelegate.uuid], [self urlEncode:appdelegate.customer_id], appdelegate.plugin_id];
                currentRequest = @"counterFav";
                //NSLog(@"appdelegate.user_apipath:: %@", appdelegate.user_apipath);
                //NSLog(@"urlAddress:: %@", urlAddress);
                [self connectToServer:urlAddress];
            }
        }
        //[self processdata];
        //}
    }
    //response =nil;
    
} */

-(void) displayAlert: (NSString *) stralert {
    NSString *str = [[NSString alloc] initWithFormat:@"%@", stralert];
    UIAlertView *alert1 = [[UIAlertView alloc] initWithTitle:appdelegate.iconname
                                                    message:str
                                                   delegate:self
                                          cancelButtonTitle:NSLocalizedString(@"done", nil)
                                          otherButtonTitles:nil];
    
    [alert1 show];
    str =nil;
    alert1 =nil;
}

- (BOOL) isConnectionAvailable
{
    Reachability *networkReachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus networkStatus = [networkReachability currentReachabilityStatus];
    if (networkStatus == NotReachable) {
        //NSLog(@"There IS NO internet connection");
        return FALSE;
    } else {
        //NSLog(@"There IS internet connection");
        return TRUE;
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
