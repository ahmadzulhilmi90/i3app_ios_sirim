//
//  ContentPluginDetailsViewController1.h
//  standardmobileappsv3
//
//  Created by M3Online on 5/27/14.
//  Copyright (c) 2014 M3Online. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MapKit/MapKit.h>
#import <MessageUI/MessageUI.h>

@interface ContentPluginDetailsViewController1 : UIViewController <MKMapViewDelegate, UIWebViewDelegate, MFMailComposeViewControllerDelegate, UIViewControllerTransitioningDelegate>{
    UIWebView *webView;
    
    NSString *cont_value1;
    NSString *cont_value2;
    NSString *cont_type;
    NSString *cont_title;
    NSString *contId;
    NSString *listtemplate;
    NSString *cont_var1;
    NSString *menu_title;
}

@property (strong, nonatomic) IBOutlet MKMapView *mapView;
@property (nonatomic, strong) UIWebView *webView;
@property (nonatomic, strong) NSString *cont_value1;
@property (nonatomic, strong) NSString *cont_value2;
@property (nonatomic, strong) NSString *cont_type;
@property (nonatomic, strong) NSString *cont_title;
@property (nonatomic, strong) NSString *contId;
@property (nonatomic, strong) NSString *listtemplate;
@property (nonatomic, strong) NSString *cont_var1;
@property (nonatomic, strong) NSString *menu_title;

@end
