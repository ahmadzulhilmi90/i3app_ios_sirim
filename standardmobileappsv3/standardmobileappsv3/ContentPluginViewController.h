//
//  ContentPluginViewController.h
//  standardmobileappsv3
//
//  Created by M3Online on 5/30/14.
//  Copyright (c) 2014 M3Online. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <SDWebImage/UIButton+WebCache.h>
#import <SDWebImage/UIImageView+WebCache.h>
#import "LoadingView.h"
#import <MessageUI/MessageUI.h>

@interface ContentPluginViewController : UIViewController <UITableViewDataSource, UITableViewDelegate, UITextFieldDelegate, UIViewControllerTransitioningDelegate> {
    NSMutableArray *listOfContents;
    NSDictionary *dicContents;
    
    IBOutlet UITableView *GridtableView;
    NSMutableArray *sections;
    
    IBOutlet UITableView *MenutableView;
    
    UIScrollView *scrollview;
    
    NSMutableArray *listOfSameDate;
    NSDictionary *dicSameDate;
    
    //NSMutableData* receivedData;
    NSString *currentRequestMenu;
    
    NSMutableArray *listOfContentsFav;
    NSMutableArray *listOfContentsComments;
    NSDictionary *dicContentsFav;
    NSDictionary *dicContentsComments;
    
    UIView *view_popoverMain;
    UIView *view_plain;
    UIButton *buttonclose;
    UIButton *buttonlogin;
    UILabel *lbllogin;
    
    NSMutableArray *listOfFav;
    NSDictionary *dicFav;

}

@property (nonatomic, strong) NSMutableArray *listOfContents;

@property (nonatomic, strong) UITableView *GridtableView;
@property (nonatomic, strong) NSMutableArray *sections;

@property (nonatomic, strong) UITableView *MenutableView;

@property (strong, nonatomic) IBOutlet UIScrollView *scrollView;

@property (strong, nonatomic) IBOutlet UITextField *txt_search;
@property (strong, nonatomic) IBOutlet UIButton *button_searchDetail;

@property (nonatomic, strong) NSMutableArray *listOfSameDate;

//@property (nonatomic, retain) NSMutableData *receivedData;
@property (nonatomic, retain) NSString *currentRequestMenu;

@property (nonatomic, strong) NSMutableArray *listOfContentsFav;
@property (nonatomic, strong) NSMutableArray *listOfContentsComments;

@property (nonatomic, strong) UIView *view_popoverMain;
@property (nonatomic, strong) UIButton *buttonclose;
@property (nonatomic, strong) UIButton *buttonlogin;
@property (nonatomic, strong) UILabel *lbllogin;
@property (nonatomic, strong) UIView *view_plain;

@property (nonatomic, strong) NSMutableArray *listOfFav;
@property (nonatomic, strong) NSMutableArray * array1;
@property (nonatomic, strong) NSString *list_favorite;

@end
