//
//  ContentPluginViewController.m
//  standardmobileappsv3
//
//  Created by M3Online on 5/30/14.
//  Copyright (c) 2014 M3Online. All rights reserved.
//

#import "ContentPluginViewController.h"
#import "AppDelegate.h"
#import <sqlite3.h>
#import "Item.h"
#import "NSString+HTML.h"
#import "ContentPluginDetailsViewController.h"
#import "ContentPluginDetailsViewController1.h"
#import <CoreLocation/CoreLocation.h>
#import "RootViewController.h"
#import "ViewController.h"
#import "SBJson.h"
#import "CommentsViewController.h"
#import "InitialSlidingViewController.h"
#import "BTSlideInteractor.h"
#import "Reachability.h"

@interface ContentPluginViewController ()
@property (strong, nonatomic) BTSlideInteractor *interactor;
@end

@implementation ContentPluginViewController
UIAlertView *alert;
UIActivityIndicatorView *indicator;
CLLocationManager *locationManager;

@synthesize listOfContents, GridtableView, sections, scrollView, listOfSameDate;
@synthesize MenutableView, txt_search, button_searchDetail/*, receivedData*/, currentRequestMenu, listOfContentsComments, listOfContentsFav, view_popoverMain, buttonclose, buttonlogin, lbllogin, view_plain, listOfFav, array1, list_favorite;

__strong NSString *cont_id;
__strong NSString *cont_title;
__strong NSString *link_url;

__strong NSString *list_template;
__strong NSString *catname;
__strong NSString *themepluginid;

__strong NSString *contentfavid;

InitialSlidingViewController *modalController;

AppDelegate *appdelegate;

- (UIColor *) colorWithHexString:(NSString *)hexString {
    NSString *colorString = [[hexString stringByReplacingOccurrencesOfString: @"#" withString: @""] uppercaseString];
    CGFloat alpha, red, blue, green;
    alpha = 1.0f;
    red   = [self colorComponentFrom: colorString start: 0 length: 2];
    green = [self colorComponentFrom: colorString start: 2 length: 2];
    blue  = [self colorComponentFrom: colorString start: 4 length: 2];
    return [UIColor colorWithRed: red green: green blue: blue alpha: alpha];
}

- (CGFloat) colorComponentFrom:(NSString *)string start:(NSUInteger)start length:(NSUInteger)length {
    NSString *substring = [string substringWithRange: NSMakeRange(start, length)];
    NSString *fullHex = length == 2 ? substring : [NSString stringWithFormat: @"%@%@", substring, substring];
    unsigned hexComponent;
    [[NSScanner scannerWithString: fullHex] scanHexInt: &hexComponent];
    return hexComponent / 255.0;
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (IBAction)closePopover:(id)sender
{
    [view_popoverMain removeFromSuperview];
    //[view_popoverMain setHidden:NO];
}

- (IBAction)logIn:(id)sender
{
    NSString *nibFileName;
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone) {
        nibFileName = @"SettingViewController";
    } else {
        nibFileName = @"SettingViewController_ipad";
    }
    SettingViewController *s = [[SettingViewController alloc] initWithNibName:nibFileName bundle:nil];
    appdelegate.shouldReturnToPrevPage = YES;
    //LoyaltyViewController *l = [[LoyaltyViewController alloc] initWithNibName:@"LoyaltyViewController" bundle:nil];
    appdelegate.originVC = self;
    [appdelegate.navController pushViewController:s animated:NO];
    
    [view_popoverMain removeFromSuperview];
}


- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    //self.title = appdelegate.iconname;
    self.view.backgroundColor = [UIColor clearColor];
    self.navigationItem.backBarButtonItem = [[UIBarButtonItem alloc]initWithTitle:@"" style:UIBarButtonItemStylePlain target:nil action:nil];
    
    appdelegate.homeclicked = NO;
    
    appdelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    
    CGRect rect = CGRectMake(0,0,20,20);
    UIGraphicsBeginImageContext( rect.size );
    [[UIImage imageNamed:@"home.png"] drawInRect:rect];
    UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    UIBarButtonItem *myHomeButton = [[UIBarButtonItem alloc] initWithImage:image style:UIBarButtonItemStyleDone target:self action:@selector(myHomeAction:)];
    
    
    NSArray *myButtonArray = [[NSArray alloc] initWithObjects:myHomeButton, nil];
    
    self.navigationItem.rightBarButtonItems = myButtonArray;
    
    txt_search = [[UITextField alloc] initWithFrame:CGRectMake(10, 15, self.view.frame.size.width-130, 40)];
    txt_search.textColor = [UIColor blackColor];
    [txt_search setBorderStyle:UITextBorderStyleRoundedRect];
    txt_search.delegate = self;
    
    listOfSameDate = [[NSMutableArray alloc] init];
    listOfContentsFav = [[NSMutableArray alloc] init];
    listOfContentsComments = [[NSMutableArray alloc] init];
    listOfFav = [[NSMutableArray alloc] init];
    array1 = [NSMutableArray array];

    
    view_popoverMain=[[UIView alloc]initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height)];
    UIColor *background = [[UIColor alloc] initWithPatternImage:[UIImage imageNamed: @"behind_alert_view.png"]];
    [view_popoverMain setBackgroundColor:background];
    //view_popoverMain.alpha = 0.5;
    //view_popoverMain.backgroundColor = [UIColor colorWithWhite:0.6 alpha:0];
    //[self.view addSubview:view_popoverMain];
    //[view_popoverMain setHidden:YES];
    view_plain=[[UIView alloc]initWithFrame:CGRectMake(self.view.frame.size.width/2-140, self.view.frame.size.height/2-130, 280, 220)];
    [view_plain setBackgroundColor:[UIColor whiteColor]];
    
    lbllogin = [[UILabel alloc]initWithFrame:CGRectMake(view_plain.frame.size.width/2-120,view_plain.frame.size.height/2-60, 240, 40)];
    lbllogin.text = @"Please login to your account";
    lbllogin.backgroundColor = [UIColor clearColor];
    lbllogin.textColor = [UIColor blackColor];
    lbllogin.textAlignment = NSTextAlignmentCenter;
    
    buttonclose = [UIButton buttonWithType:UIButtonTypeCustom];
    [buttonclose addTarget:self
                    action:@selector(closePopover:)
          forControlEvents:UIControlEventTouchUpInside];
    //[buttonclose setTitle:@"close" forState:UIControlStateNormal];
    [buttonclose setBackgroundImage:[UIImage imageNamed:@"btn_close_x.png"] forState:UIControlStateNormal];
    buttonclose.frame = CGRectMake(view_plain.frame.size.width-60, 0, 50.0, 50.0);
    //[view_popoverMain addSubview:buttonclose];
    
    buttonlogin = [UIButton buttonWithType:UIButtonTypeCustom];
    [buttonlogin addTarget:self
                    action:@selector(logIn:)
          forControlEvents:UIControlEventTouchUpInside];
    [buttonlogin setTitle:@"Login" forState:UIControlStateNormal];
    [buttonlogin setBackgroundColor:[UIColor grayColor]];
    [buttonlogin setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    buttonlogin.frame = CGRectMake(view_plain.frame.size.width/2-50, view_plain.frame.size.height/2, 100, 40.0);
    //[view_popoverMain addSubview:buttonlogin];

}

/*- (NSString *)deviceLocation {
    return [NSString stringWithFormat:@"latitude: %f longitude: %f", locationManager.location.coordinate.latitude, locationManager.location.coordinate.longitude];
} */

-(void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    if ([self isConnectionAvailable])
    {
        if ([list_favorite isEqualToString:@"ListOut"]) {
            list_template = @"5";
            NSString *urlAddress3 = [[NSString alloc] initWithFormat:@"%@favorite_list.php?db=%@&userid=%@&udid=%@&cuid=%@", appdelegate.user_apipath, [self urlEncode:appdelegate.db], [self urlEncode:appdelegate.user], [self urlEncode:appdelegate.uuid], [self urlEncode:appdelegate.customer_id]];
            currentRequestMenu = @"favoriteList";
            //NSLog(@"appdelegate.user_apipath:: %@", appdelegate.user_apipath);
            //NSLog(@"urlAddress:: %@", urlAddress);
            [self connectToServer:urlAddress3];
        } else {
            NSString *urlAddress = [[NSString alloc] initWithFormat:@"%@favorite_counter.php?db=%@&userid=%@&udid=%@&cuid=%@&top=%@", appdelegate.user_apipath, [self urlEncode:appdelegate.db], [self urlEncode:appdelegate.user], [self urlEncode:appdelegate.uuid], [self urlEncode:appdelegate.customer_id], appdelegate.plugin_id];
            currentRequestMenu = @"counterFav";
            //NSLog(@"appdelegate.user_apipath:: %@", appdelegate.user_apipath);
            //NSLog(@"urlAddress:: %@", urlAddress);
            [self connectToServer:urlAddress];
        }
    } else {
        [self processdata];
    }
}

//Slide Menu
- (void)willAnimateRotationToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation duration:(NSTimeInterval)duration
{
    //NSLog(@"view frame=%@ bounds=%@", NSStringFromCGRect(self.view.frame), NSStringFromCGRect(self.view.bounds));
}


- (void)showModalButtonWasTouched
{
    self.interactor = [[BTSlideInteractor alloc] init];
    self.interactor.presenting = YES;
    
    //BTModalViewController *modalController = [self.storyboard instantiateViewControllerWithIdentifier:@"ModalViewController"];
    modalController = [[InitialSlidingViewController alloc] init];
    modalController.modalPresentationStyle = UIModalPresentationCustom;
    modalController.transitioningDelegate = self.interactor;
    [self.navigationController presentViewController:modalController animated:YES completion:nil];
    //[self.navigationController popToViewController:modalController animated:NO];
    //[self.navigationController pushViewController:modalController animated:YES];
}
// End Slide Menu

-(void)myHomeAction:(UIBarButtonItem *)sender{
    
    appdelegate.nextPage = @"";
    
    if ([appdelegate.listOfVerMenu count]>0) {
        if (appdelegate.homeclicked==NO) {
            appdelegate.homeclicked = YES;
            [self showModalButtonWasTouched];
        } else {
            appdelegate.homeclicked = NO;
            [modalController dismissViewControllerAnimated:YES completion:nil];
        }
    } else {
        //AppDelegate *appdelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
        //NSLog(@"View Controllers : %@",[appdelegate.navController viewControllers]);
        NSMutableArray *navigationArray = [[NSMutableArray alloc] initWithArray: appdelegate.navController.viewControllers];
        
        for (UIViewController* viewController in appdelegate.navController.viewControllers) {
            
            //This if condition checks whether the viewController's class is MyGroupViewController
            // if true that means its the MyGroupViewController (which has been pushed at some point)
            if ([viewController isKindOfClass:[RootViewController class]] ) {
                
                // Here viewController is a reference of UIViewController base class of MyGroupViewController
                // but viewController holds MyGroupViewController  object so we can type cast it here
                //ThemeViewController *themeViewController = (ThemeViewController*)viewController;
                //[self.navigationController popToViewController:themeViewController animated:YES];
                [navigationArray removeObjectAtIndex: 0];
            }
        }
        appdelegate.navController.viewControllers = navigationArray;
        for (UIViewController* viewController in appdelegate.navController.viewControllers) {
            
            //This if condition checks whether the viewController's class is MyGroupViewController
            // if true that means its the MyGroupViewController (which has been pushed at some point)
            if ([viewController isKindOfClass:[ViewController class]] ) {
                
                // Here viewController is a reference of UIViewController base class of MyGroupViewController
                // but viewController holds MyGroupViewController  object so we can type cast it here
                ViewController *themeViewController = (ViewController*)viewController;
                [appdelegate.navController popToViewController:themeViewController animated:NO];
                //[self.navigationController popToViewController:viewController animated:YES];
            }
        }
        //NSLog(@"View Controllers111 : %@",appdelegate.navController.viewControllers);
    }
    
}

- (void) processdata {
    
    sqlite3 *database;
    
    NSString *catid;
    
    //NSLog(@"deviceLocation: %@", [self deviceLocation]);
    
    if(sqlite3_open([appdelegate.databasePath UTF8String], &database) == SQLITE_OK) {
        
        //if ([aplugintype isEqualToString:@"content"]) {
        if ([list_favorite isEqualToString:@"ListOut"]) {
            self.title = [NSString stringWithFormat:NSLocalizedString(@"favorite", nil)];
        } else {
            NSString *content_category;
            /*** Push Notification 27Aug2015 ***/
            if (![appdelegate.notify_actionvalue isEqualToString:@""]) {
                content_category = [[NSString alloc] initWithFormat:@"SELECT * from content_category WHERE top_id = %@", appdelegate.notify_actionvalue];
                appdelegate.notify_actionvalue = @"";
                appdelegate.notify_actiontype = @"";
                appdelegate.notify_contentid = @"";
                /*** Push Notification 27Aug2015 ***/
            } else {
                content_category = [[NSString alloc] initWithFormat:@"SELECT * from content_category WHERE top_id = %@", appdelegate.plugin_id];
            }
            //NSLog(@"appdelegate.plugin_id:: %@", appdelegate.plugin_id);
            
            const char *sqlStatement2 = [content_category UTF8String];
            sqlite3_stmt *compiledStatement2;
            if(sqlite3_prepare_v2(database, sqlStatement2, -1, &compiledStatement2, NULL) == SQLITE_OK) {
                while(sqlite3_step(compiledStatement2) == SQLITE_ROW) {
                    
                    catid = [NSString stringWithUTF8String:(char *)sqlite3_column_text(compiledStatement2, 0)];
                    catname = [NSString stringWithUTF8String:(char *)sqlite3_column_text(compiledStatement2, 1)];
                    themepluginid = [NSString stringWithUTF8String:(char *)sqlite3_column_text(compiledStatement2, 2)];
                    list_template = [NSString stringWithUTF8String:(char *)sqlite3_column_text(compiledStatement2, 3)];
                    
                }
            }
            // Release the compiled statement from memory
            sqlite3_finalize(compiledStatement2);
            
            self.title = catname;
        }
        
        NSString *theme_object;
        if ([list_template isEqualToString:@"6"]) {
            locationManager = [[CLLocationManager alloc] init];
            locationManager.distanceFilter = kCLDistanceFilterNone; // whenever we move
            locationManager.desiredAccuracy = kCLLocationAccuracyHundredMeters; // 100 m
            [locationManager startUpdatingLocation];
            
            [txt_search resignFirstResponder];
            
            [listOfContents removeAllObjects];
            [MenutableView removeFromSuperview];
            
            NSString *strsym = @"%";
            NSString *textsearch = [[NSString alloc] initWithFormat:@"%@%@%@", strsym, txt_search.text, strsym];
            
            //theme_object = [[NSString alloc] initWithFormat:@"SELECT * FROM contents WHERE category_id = %@ ORDER BY abs(map_lat - (%@)) + abs( map_lon - (%@))", catid, catid, catid];
            theme_object = [[NSString alloc] initWithFormat:@"SELECT * FROM contents WHERE (title like '%@' OR var1 like '%@') AND category_id = %@ ORDER BY abs(map_lat - (%@)) + abs( map_lon - (%@))", textsearch, textsearch, catid, catid, catid];
        } else if ([list_template isEqualToString:@"9"]) {
            theme_object = [[NSString alloc] initWithFormat:@"SELECT * from contents WHERE category_id = %@ order by date_updated desc", catid];
        } else {
            if ([list_favorite isEqualToString:@"ListOut"]) {
                
                theme_object = [[NSString alloc] initWithFormat:@"SELECT * from contents WHERE id IN (%@) order by orderno, date_updated desc", contentfavid];
                
            } else {
                theme_object = [[NSString alloc] initWithFormat:@"SELECT * from contents WHERE category_id = %@ order by orderno, date_updated desc", catid];
            }
        }
        //NSLog(@"theme_object:: %@", theme_object);
        
        NSString *cid;
        NSString *ctitle;
        NSString *cimage;
        NSString *cthumbnail;
        NSString *cpricetitle;
        NSString *cprice;
        NSString *cvar1;
        NSString *cmaptitle;
        NSString *clat;
        NSString *clong;
        NSString *cdesc;
        NSString *cdateupdated;
        NSString *clinkurl;
        const char *sqlStatement3 = [theme_object UTF8String];
        sqlite3_stmt *compiledStatement3;
        if(sqlite3_prepare_v2(database, sqlStatement3, -1, &compiledStatement3, NULL) == SQLITE_OK) {
            listOfContents = [[NSMutableArray alloc] init];
            while(sqlite3_step(compiledStatement3) == SQLITE_ROW) {
                
                cid = [NSString stringWithUTF8String:(char *)sqlite3_column_text(compiledStatement3, 0)];
                ctitle = [NSString stringWithUTF8String:(char *)sqlite3_column_text(compiledStatement3, 1)];
                cimage = [NSString stringWithUTF8String:(char *)sqlite3_column_text(compiledStatement3, 3)];
                cthumbnail = [NSString stringWithUTF8String:(char *)sqlite3_column_text(compiledStatement3, 4)];
                cdesc = [NSString stringWithUTF8String:(char *)sqlite3_column_text(compiledStatement3, 9)];
                clinkurl = [NSString stringWithUTF8String:(char *)sqlite3_column_text(compiledStatement3, 11)];
                cmaptitle = [NSString stringWithUTF8String:(char *)sqlite3_column_text(compiledStatement3, 12)];
                clat = [NSString stringWithUTF8String:(char *)sqlite3_column_text(compiledStatement3, 13)];
                clong = [NSString stringWithUTF8String:(char *)sqlite3_column_text(compiledStatement3, 14)];
                cdateupdated = [NSString stringWithUTF8String:(char *)sqlite3_column_text(compiledStatement3, 21)];
                cpricetitle = [NSString stringWithUTF8String:(char *)sqlite3_column_text(compiledStatement3, 23)];
                cprice = [NSString stringWithUTF8String:(char *)sqlite3_column_text(compiledStatement3, 24)];
                cvar1 = [NSString stringWithUTF8String:(char *)sqlite3_column_text(compiledStatement3, 26)];
                
                ctitle = [ctitle stringByDecodingHTMLEntities];
                cpricetitle = [cpricetitle stringByDecodingHTMLEntities];
                cvar1 = [cvar1 stringByDecodingHTMLEntities];
                cmaptitle = [cmaptitle stringByDecodingHTMLEntities];
                cdesc = [cdesc stringByDecodingHTMLEntities];
                ctitle = [ctitle stringByReplacingOccurrencesOfString:@"''" withString:@"'"];
                cpricetitle = [cpricetitle stringByReplacingOccurrencesOfString:@"''" withString:@"'"];
                cvar1 = [cvar1 stringByReplacingOccurrencesOfString:@"''" withString:@"'"];
                cmaptitle = [cmaptitle stringByReplacingOccurrencesOfString:@"''" withString:@"'"];
                cdesc = [cdesc stringByReplacingOccurrencesOfString:@"''" withString:@"'"];
                
                dicContents = [NSDictionary dictionaryWithObjectsAndKeys:cid,@"id",ctitle,@"title",cimage,@"image",cthumbnail,@"thumbnail",cpricetitle,@"price_title",cprice,@"price",cvar1,@"var1",cmaptitle,@"map_title",clat,@"latitude",clong,@"longitude,",cdesc,@"description",cdateupdated,@"date_updated",clinkurl,@"linkurl",nil];
                [listOfContents addObject:dicContents];
                
            }
        }
        // Release the compiled statement from memory
        sqlite3_finalize(compiledStatement3);
        
        //NSLog(@"list_template::%@", list_template);
        //NSLog(@"listOfContents::%@", listOfContents);
        //AppDelegate *appdelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
        
        if([listOfContents count]>0){
            
            int i;
            
            if ([list_template isEqualToString:@"2"]) {
                
                scrollview = nil;
                scrollview = [[UIScrollView alloc] initWithFrame:CGRectMake(0, 5, self.view.frame.size.width, self.view.frame.size.height-5)];
                scrollview.backgroundColor = [UIColor clearColor];
                scrollview.bounces = NO;
                [self.view addSubview:scrollview];
                
                int height2;
                
                if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone) {
                    // For iPhone
                    height2=240;
                } else {
                    height2=453;
                }
                scrollview.contentSize = CGSizeMake(self.view.frame.size.width, ((5*[listOfContents count]) + (height2*([listOfContents count])))+130 + 20);
                //[self.view addSubview:scrollview];
                UIButton *btnimgViewFav2;
                UIButton *btnimgViewCom2;
                UILabel *lblFav2;
                UILabel *lblCom2;
                [lblFav2 removeFromSuperview];
                [lblCom2 removeFromSuperview];
                [btnimgViewCom2 removeFromSuperview];
                [btnimgViewFav2 removeFromSuperview];
                int pos = 0;
                for (i=0;i<[listOfContents count]; i++) {
                    //float x = (float)(67 + ((95 + 22) * i));
                    int x;
                    
                    //x = (10 + (50*pos) + (200*pos));
                    x = ((5*pos) + (height2*pos));
                    
                    UIView *rectView;
                    if ([self showFavorite] == 1 || [self showComments] == 1) {
                        if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone) {
                            // For iPhone
                            rectView = [[UIView alloc]initWithFrame:CGRectMake(5, x, self.view.frame.size.width-10, height2-40+35)];
                        } else {
                            rectView = [[UIView alloc]initWithFrame:CGRectMake(5, x, self.view.frame.size.width-10, height2-53+43)];
                        }
                    } else {
                        if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone) {
                            // For iPhone
                            rectView = [[UIView alloc]initWithFrame:CGRectMake(5, x, self.view.frame.size.width-10, height2-40)];
                        } else {
                            rectView = [[UIView alloc]initWithFrame:CGRectMake(5, x, self.view.frame.size.width-10, height2-53)];
                        }
                    }
                    [rectView setBackgroundColor:[UIColor clearColor]];
                    rectView.layer.borderColor = [UIColor grayColor].CGColor;
                    rectView.layer.borderWidth = 0.5f;
                    [scrollview addSubview:rectView];
                    
                    NSDictionary *dictionary = [listOfContents objectAtIndex:i];
                    
                    NSString *content_img = [dictionary objectForKey:@"image"];
                    //NSRange range = [content_img rangeOfString:@"upload/"];
                    //NSString *content_img_file = [[content_img substringFromIndex:NSMaxRange(range)] stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
                    NSString *bar_name = [dictionary objectForKey:@"title"];
                    //cont_id = [dictionary objectForKey:@"id"];
                    
                    UIButton *myButton = [UIButton buttonWithType:UIButtonTypeCustom];
                    if ([self showFavorite] == 1 || [self showComments] == 1) {
                        if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone) {
                            // For iPhone
                            myButton.frame = CGRectMake(5, x, self.view.frame.size.width-10, height2-40);
                        } else {
                            myButton.frame = CGRectMake(5, x, self.view.frame.size.width-10, height2-53);
                        }
                    } else {
                        if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone) {
                            // For iPhone
                            myButton.frame = CGRectMake(5, x, self.view.frame.size.width-10, height2);
                        } else {
                            myButton.frame = CGRectMake(5, x, self.view.frame.size.width-10, height2);
                        }
                    }
                    myButton.tag = i;
                    [myButton addTarget:self
                                 action:@selector(checkButtonClick:)
                       forControlEvents:UIControlEventTouchUpInside];
                    
                    if ([content_img length]>0 || [bar_name length]>0) {
                        
                        if ([content_img length]>0)
                        {
                            //[myButton setBackgroundImageWithURL:[NSURL URLWithString:content_img] forState:UIControlStateNormal placeholderImage:[UIImage imageNamed:@"No-Image-icon.png"]];
                            UIImageView *imgV = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, myButton.frame.size.width, myButton.frame.size.height)];
                            
                            [myButton addSubview:imgV];
                            
                            __block UIActivityIndicatorView *activityIndicator;
                            __weak UIImageView *weakImageView = imgV;
                            /*[weakImageView setImageWithURL:[NSURL URLWithString:content_img] placeholderImage:[UIImage imageNamed:@"No-Image-icon.png"] options:SDWebImageProgressiveDownload progress:^(NSInteger receivedSize, NSInteger expectedSize)
                             {
                                 if (!activityIndicator)
                                 {
                                     [weakImageView addSubview:activityIndicator = [UIActivityIndicatorView.alloc initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray]];
                                     activityIndicator.center = weakImageView.center;
                                     [activityIndicator startAnimating];
                                 }
                             }
                                                 completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType)
                             {
                                 [activityIndicator removeFromSuperview];
                                 activityIndicator = nil;
                             }]; */
                            dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0), ^ {
                                [weakImageView sd_setImageWithURL:[NSURL URLWithString:content_img]
                                                 placeholderImage:[UIImage imageNamed:@"No-Image-icon.png"]
                                                          options:SDWebImageProgressiveDownload
                                                         progress:^(NSInteger receivedSize, NSInteger expectedSize, NSURL *targetURL) {
                                                             dispatch_async(dispatch_get_main_queue(), ^ {
                                                                 if (!activityIndicator) {
                                                                     
                                                                     [weakImageView addSubview:activityIndicator = [UIActivityIndicatorView.alloc initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray]];
                                                                     activityIndicator.center = weakImageView.center;
                                                                     [activityIndicator startAnimating];
                                                                 }
                                                             });
                                                         }
                                                        completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
                                                            dispatch_async(dispatch_get_main_queue(), ^ {
                                                                //[MBProgressHUD hideHUDForView:self.view animated:YES];
                                                                [activityIndicator removeFromSuperview];
                                                                activityIndicator = nil;
                                                            });
                                                        }];
                            });
                            
                            imgV.contentMode  = UIViewContentModeScaleAspectFill;
                            [imgV setClipsToBounds:YES];
                        }
                        else
                        {
                            [myButton setTitle:@"No Image" forState:UIControlStateNormal];
                            [myButton setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
                            [myButton setBackgroundColor:[UIColor colorWithRed:230/255.0 green:230/255.0 blue:230/255.0 alpha:1]];
                        }
                        
                        [scrollview addSubview:myButton];
                        
                        UIImageView *ImageBarView;
                        if ([self showFavorite] == 1 || [self showComments] == 1) {
                            if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone) {
                                // For iPhone
                                ImageBarView = [[UIImageView alloc] initWithFrame:CGRectMake(5, x+self.view.frame.size.width/2-10-40, self.view.frame.size.width-10, 50)];
                            } else {
                                ImageBarView = [[UIImageView alloc] initWithFrame:CGRectMake(5, x+self.view.frame.size.width/2-10-53, self.view.frame.size.width-10, 50)];
                            }
                        } else {
                            if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone) {
                                // For iPhone
                                ImageBarView = [[UIImageView alloc] initWithFrame:CGRectMake(5, x+self.view.frame.size.width/2-10, self.view.frame.size.width-10, 50)];
                            } else {
                                ImageBarView = [[UIImageView alloc] initWithFrame:CGRectMake(5, x+self.view.frame.size.width/2-10, self.view.frame.size.width-10, 50)];
                            }
                        }
                        //ImageBarView.backgroundColor = [self colorWithHexString:appdelegate.themeColorCode];
                        //ImageBarView.alpha = 0.6;
                        
                        UIView *overlay;
                        overlay = [[UIView alloc] initWithFrame:CGRectMake(0, 0, ImageBarView.frame.size.width, ImageBarView.frame.size.height)];
                        overlay.backgroundColor = [self colorWithHexString:appdelegate.themeColorCode];
                        overlay.alpha = 0.6f;
                        //overlay.opaque = YES;
                        
                        [ImageBarView addSubview:overlay];
                        
                        UILabel *lblTitle = [[UILabel alloc] initWithFrame:CGRectMake(5, 0, self.view.frame.size.width-10, 60)];
                        lblTitle.numberOfLines = 0;
                        lblTitle.lineBreakMode = NSLineBreakByWordWrapping;
                        UIFont *yourFont = [UIFont fontWithName:@"Helvetica-Bold" size:22];
                        lblTitle.font = yourFont;
                        lblTitle.text = bar_name;
                        lblTitle.backgroundColor = [UIColor clearColor];
                        lblTitle.textColor = [UIColor whiteColor];
                        [lblTitle sizeToFit];
                        lblTitle.shadowColor = [UIColor blackColor];
                        lblTitle.shadowOffset = CGSizeMake(0, 1);
                        [ImageBarView addSubview:lblTitle];
                        CGSize constrainedSize = CGSizeMake(lblTitle.frame.size.width  , 9999);
                        
                        NSDictionary *attributesDictionary = [NSDictionary dictionaryWithObjectsAndKeys:
                                                              yourFont, NSFontAttributeName,
                                                              nil];
                        
                        NSMutableAttributedString *string = [[NSMutableAttributedString alloc] initWithString:lblTitle.text attributes:attributesDictionary];
                        
                        CGRect requiredHeight = [string boundingRectWithSize:constrainedSize options:NSStringDrawingUsesLineFragmentOrigin context:nil];
                        
                        if (requiredHeight.size.width > lblTitle.frame.size.width) {
                            requiredHeight = CGRectMake(0,0, lblTitle.frame.size.width, requiredHeight.size.height);
                        }
                        CGRect newFrame = lblTitle.frame;
                        newFrame.size.height = requiredHeight.size.height;
                        lblTitle.frame = newFrame;
                        if ([self showFavorite] == 1 || [self showComments] == 1) {
                            if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone) {
                                // For iPhone
                                ImageBarView.frame = CGRectMake(5, x+height2-newFrame.size.height-40, self.view.frame.size.width-10, newFrame.size.height);
                            } else {
                                ImageBarView.frame = CGRectMake(5, x+height2-newFrame.size.height-53, self.view.frame.size.width-10, newFrame.size.height);
                            }
                        } else {
                            if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone) {
                                // For iPhone
                                ImageBarView.frame = CGRectMake(5, x+height2-newFrame.size.height, self.view.frame.size.width-10, newFrame.size.height);
                            } else {
                                ImageBarView.frame = CGRectMake(5, x+height2-newFrame.size.height, self.view.frame.size.width-10, newFrame.size.height);
                            }
                        }
                        overlay.frame = CGRectMake(0, 0, ImageBarView.frame.size.width, ImageBarView.frame.size.height);
                        [scrollview addSubview:ImageBarView];
                        
                        if ([self showFavorite]==1) {
                            NSPredicate *predicate0 = [NSPredicate predicateWithFormat:@"id == %@", [dictionary objectForKey:@"id"]];
                            NSArray *filteredArrayLike = [listOfFav filteredArrayUsingPredicate:predicate0];
                            btnimgViewFav2 = [UIButton buttonWithType:UIButtonTypeCustom];
                            if ([filteredArrayLike count]>0) {
                                [btnimgViewFav2 setImage:[UIImage imageNamed:@"favorite_red.png"] forState:UIControlStateNormal];
                            } else {
                                [btnimgViewFav2 setImage:[UIImage imageNamed:@"favorite_grey.png"] forState:UIControlStateNormal];
                            }
                            if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone) {
                                // For iPhone
                                [btnimgViewFav2 setFrame:CGRectMake(self.view.frame.size.width/4-40, x+height2-35, 25, 25)];
                            } else {
                                [btnimgViewFav2 setFrame:CGRectMake(self.view.frame.size.width/4-53, x+height2-48, 38, 38)];
                            }
                            btnimgViewFav2.tag = i;
                            [btnimgViewFav2 addTarget:self action:@selector(checkButtonFavClick:) forControlEvents:UIControlEventTouchUpInside];
                            NSPredicate *predicate = [NSPredicate predicateWithFormat:@"id == %@", [dictionary objectForKey:@"id"]];
                            NSArray *filteredArray = [listOfContentsFav filteredArrayUsingPredicate:predicate];
                            //NSLog(@"filteredArray %@", filteredArray);
                            NSString *countFav=@"0";
                            if ([filteredArray count]>0) {
                                NSDictionary* favorite = [filteredArray objectAtIndex:0];
                                //NSLog(@"%@",[favorite objectForKey:@"cntFav"]);
                                countFav = [favorite objectForKey:@"cntFav"];
                            }
                            if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone) {
                                // For iPhone
                                lblFav2 = [[UILabel alloc] initWithFrame:CGRectMake(self.view.frame.size.width/4, x+height2-30, self.view.frame.size.width/4, 20)];
                            } else {
                                lblFav2 = [[UILabel alloc] initWithFrame:CGRectMake(self.view.frame.size.width/4, x+height2-43, self.view.frame.size.width/4, 33)];
                            }
                            lblFav2.text = [NSString stringWithFormat:@"%@", countFav];
                            
                            [scrollview addSubview:btnimgViewFav2];
                            [scrollview addSubview:lblFav2];
                        }
                        
                        if ([self showComments]==1) {
                            btnimgViewCom2 = [UIButton buttonWithType:UIButtonTypeCustom];
                            [btnimgViewCom2 setImage:[UIImage imageNamed:@"comments.png"] forState:UIControlStateNormal];
                            if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone) {
                                // For iPhone
                                [btnimgViewCom2 setFrame:CGRectMake(self.view.frame.size.width/4*3-40, x+height2-35, 25, 25)];
                            } else {
                                [btnimgViewCom2 setFrame:CGRectMake(self.view.frame.size.width/4*3-53, x+height2-48, 38, 38)];
                            }
                            btnimgViewCom2.tag = i;
                            [btnimgViewCom2 addTarget:self action:@selector(checkButtonCommentsClick:) forControlEvents:UIControlEventTouchUpInside];
                            NSPredicate *predicate1 = [NSPredicate predicateWithFormat:@"id == %@", [dictionary objectForKey:@"id"]];
                            NSArray *filteredArrayComm = [listOfContentsComments filteredArrayUsingPredicate:predicate1];
                            NSString *countComm=@"0";
                            if ([filteredArrayComm count]>0) {
                                NSDictionary* comments = [filteredArrayComm objectAtIndex:0];
                                //NSLog(@"%@",[favorite objectForKey:@"cntFav"]);
                                countComm = [comments objectForKey:@"cntComments"];
                            }
                            if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone) {
                                // For iPhone
                                lblCom2 = [[UILabel alloc] initWithFrame:CGRectMake(self.view.frame.size.width/4*3, x+height2-30, self.view.frame.size.width/4, 20)];
                            } else {
                                lblCom2 = [[UILabel alloc] initWithFrame:CGRectMake(self.view.frame.size.width/4*3, x+height2-43, self.view.frame.size.width/4, 33)];
                            }
                            lblCom2.text = [NSString stringWithFormat:@"%@", countComm];
                            //NSLog(@"lblCom2.text %@", lblCom2.text);
                            
                            [scrollview addSubview:btnimgViewCom2];
                            [scrollview addSubview:lblCom2];
                            
                        }
                        
                        pos++;
                        
                        lblTitle =nil;
                        ImageBarView =nil;
                        
                    }
                }
            } else if ([list_template isEqualToString:@"3"] || [list_template isEqualToString:@"4"] || [list_template isEqualToString:@"8"]) {
                
                sections = [[NSMutableArray alloc] init];
                //GridtableView = [[UITableView alloc] initWithFrame:CGRectMake(0, 5, self.view.frame.size.width, self.view.frame.size.height-5)];
                GridtableView = [[UITableView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height)];
                GridtableView.autoresizingMask = UIViewAutoresizingFlexibleBottomMargin | UIViewAutoresizingFlexibleHeight;
                GridtableView.separatorStyle = UITableViewCellSeparatorStyleNone;
                GridtableView.dataSource = self;
                GridtableView.delegate   = self;
                //GridtableView.frame = self.view.frame;
                
                for(int s=0;s<1;s++)
                { // 4 sections
                    NSMutableArray *section = [[NSMutableArray alloc] init];
                    //for(int i=0;i<12;i++) {  // 12 items in each section
                    for (int i=0;i<[listOfContents count]; i++) {
                        NSDictionary *dictionary = [listOfContents objectAtIndex:i];
                        
                        NSString *cont_id3 = [dictionary objectForKey:@"id"];
                        NSString *bar_name;
                        NSString *content_img;
                        NSString *price;
                        NSString *var1;
                        NSString *pricetitle;
                        bar_name = [dictionary objectForKey:@"title"];
                        content_img = [dictionary objectForKey:@"image"];
                        price = [dictionary objectForKey:@"price"];
                        var1 = [dictionary objectForKey:@"var1"];
                        pricetitle = [dictionary objectForKey:@"price_title"];
                        //NSLog(@"content_img : %@", content_img);
                        
                        NSString *cont_name;
                        cont_name = [dictionary objectForKey:@"title"];
                        
                        Item *item = [[Item alloc] init];
                        //item.link=@"New Screen";
                        item.title=bar_name;
                        item.image=content_img;
                        item.contid=cont_id3;
                        item.contname=cont_name;
                        item.price=price;
                        item.var1=var1;
                        item.pricetitle=pricetitle;
                        item.linkurl=[dictionary objectForKey:@"linkurl"];
                        
                        [section addObject:item];
                    }
                    [sections addObject:section];
                }
                
                //[GridtableView reloadData];
                [self.view addSubview:GridtableView];
                //GridtableView.hidden = NO;
                
            } else if (/*[list_template isEqualToString:@"4"] || */[list_template isEqualToString:@"5"]) {
                
                if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone) {
                    // For iPhone
                    int cellheight;
                    /*if (![self showFavorite]==1 && ![self showComments]==1) {
                        cellheight = 70;
                    } else {
                        cellheight = 100;
                    } */
                    if ([self showFavorite]==1 || [self showComments]==1) {
                        cellheight = 100;
                    } else {
                        cellheight = 70;
                    }
                    if ([listOfContents count]*cellheight > self.view.frame.size.height) {
                        MenutableView = [[UITableView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height)];
                    } else {
                        MenutableView = [[UITableView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, [listOfContents count]*cellheight)];
                    }
                } else {
                    int cellheight;
                    
                    if ([self showFavorite]==1 || [self showComments]==1) {
                        cellheight = 150;
                    } else {
                        cellheight = 110;
                    }
                    
                    if ([listOfContents count]*cellheight > self.view.frame.size.height) {
                        MenutableView = [[UITableView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height)];
                    } else {
                        MenutableView = [[UITableView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, [listOfContents count]*cellheight)];
                    }
                }
                MenutableView.autoresizingMask = UIViewAutoresizingFlexibleHeight | UIViewAutoresizingFlexibleWidth;
                MenutableView.delegate = self;
                MenutableView.dataSource = self;
                
                [self.view addSubview:MenutableView];
                
            } else if ([list_template isEqualToString:@"6"]) { //POI - not show template in cms content template
                
                [self.view addSubview:txt_search];
                
                button_searchDetail = [UIButton buttonWithType:UIButtonTypeRoundedRect];
                [button_searchDetail addTarget:self
                                        action:@selector(processdata) forControlEvents:UIControlEventTouchUpInside];
                [button_searchDetail setTitle:NSLocalizedString(@"search", nil) forState:UIControlStateNormal];
                [button_searchDetail setTitleColor:[UIColor blackColor] forState:UIControlStateNormal ];
                button_searchDetail.frame = CGRectMake(self.view.frame.size.width-110, 15.0, 90.0, 40.0);
                button_searchDetail.backgroundColor = [UIColor grayColor];
                button_searchDetail.layer.cornerRadius=6;
                [self.view addSubview:button_searchDetail];
                
                if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone) {
                    // For iPhone
                    int cellheight;
                    if ([self showFavorite]==1 || [self showComments]==1) {
                        cellheight = 100;
                    } else {
                        cellheight = 70;
                    }
                    if ([listOfContents count]*cellheight > self.view.frame.size.height-60) {
                        MenutableView = [[UITableView alloc] initWithFrame:CGRectMake(0, 60, self.view.frame.size.width, self.view.frame.size.height-60)];
                    } else {
                        MenutableView = [[UITableView alloc] initWithFrame:CGRectMake(0, 60, self.view.frame.size.width, [listOfContents count]*cellheight)];
                    }
                } else {
                    int cellheightpad;
                    if ([self showFavorite]==1 || [self showComments]==1) {
                        cellheightpad = 150;
                    } else {
                        cellheightpad = 110;
                    }
                    if ([listOfContents count]*cellheightpad > self.view.frame.size.height-80) {
                        MenutableView = [[UITableView alloc] initWithFrame:CGRectMake(0, 80, self.view.frame.size.width, self.view.frame.size.height-80)];
                    } else {
                        MenutableView = [[UITableView alloc] initWithFrame:CGRectMake(0, 80, self.view.frame.size.width, [listOfContents count]*cellheightpad)];
                    }
                }
                MenutableView.autoresizingMask = UIViewAutoresizingFlexibleHeight | UIViewAutoresizingFlexibleWidth;
                MenutableView.delegate = self;
                MenutableView.dataSource = self;
                
                [self.view addSubview:MenutableView];
                
            } else if ([list_template isEqualToString:@"7"]) {
                
                if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone) {
                    int cellheight;
                    if ([self showFavorite]==1 || [self showComments]==1) {
                        cellheight = 60;
                    } else {
                        cellheight = 30;
                    }
                    // For iPhone
                    if ([listOfContents count]*cellheight > self.view.frame.size.height) {
                        MenutableView = [[UITableView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height)];
                    } else {
                        MenutableView = [[UITableView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, [listOfContents count]*cellheight)];
                    }
                } else {
                    int cellheight;
                    if ([self showFavorite]==1 || [self showComments]==1) {
                        cellheight = 80;
                    } else {
                        cellheight = 50;
                    }
                    if ([listOfContents count]*cellheight > self.view.frame.size.height) {
                        MenutableView = [[UITableView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height)];
                    } else {
                        MenutableView = [[UITableView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, [listOfContents count]*cellheight)];
                    }
                }
                MenutableView.autoresizingMask = UIViewAutoresizingFlexibleHeight | UIViewAutoresizingFlexibleWidth;
                MenutableView.delegate = self;
                MenutableView.dataSource = self;
                
                [self.view addSubview:MenutableView];
                
            } else if ([list_template isEqualToString:@"9"]) {
                NSString *newString;
                NSString *currentDate = @"";
                NSString *sameDate;
                [listOfSameDate removeAllObjects];
                for(int lfsd=0; lfsd < [listOfContents count]; lfsd++) {
                    NSDictionary *dictionary = [listOfContents objectAtIndex:lfsd];
                    newString = [[dictionary objectForKey:@"date_updated"] substringToIndex:10];
                    
                    if ([currentDate isEqualToString:newString]) {
                        sameDate = @"1";
                    } else {
                        sameDate = @"0";
                    }
                    dicSameDate = [NSDictionary dictionaryWithObjectsAndKeys:newString,@"dateUpdated",sameDate,@"SameDate",nil];
                    [listOfSameDate addObject:dicSameDate];
                    
                    currentDate = newString;
                }
                currentDate = nil;
                newString = nil;
                sameDate = nil;
                //NSLog(@"listOfSameDate %@", listOfSameDate);
                int heightrow=0;
                if([listOfSameDate count]>0)
                {
                    for(int i=0; i<[listOfSameDate count]; i++ ) {
                        NSDictionary *dictionary = [listOfSameDate objectAtIndex:i];
                        
                        if ([[dictionary objectForKey:@"SameDate"] isEqualToString:@"0"]) {
                            if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone) {
                                int cellheight;
                                if ([self showFavorite]==1 || [self showComments]==1) {
                                    cellheight = 70;
                                } else {
                                    cellheight = 40;
                                }
                                heightrow = heightrow + self.view.frame.size.width/2-10+20+5+cellheight;
                            } else {
                                int cellheight;
                                if ([self showFavorite]==1 || [self showComments]==1) {
                                    cellheight = 105;
                                } else {
                                    cellheight = 66;
                                }
                                heightrow = heightrow + self.view.frame.size.width/2-10+33+5+cellheight;
                            }
                        } else {
                            if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone) {
                                heightrow = heightrow + 70;
                            } else {
                                heightrow = heightrow + 110;
                            }
                        }
                    }
                }
                
                [MenutableView removeFromSuperview];
                //if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone) {
                // For iPhone
                if (heightrow > self.view.frame.size.height) {
                    MenutableView = [[UITableView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height)];
                } else {
                    MenutableView = [[UITableView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, heightrow)];
                }
                /*} else {
                 if ([listOfContents count]*110 > self.view.frame.size.height) {
                 MenutableView = [[UITableView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height)];
                 } else {
                 MenutableView = [[UITableView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, [listOfContents count]*110)];
                 }
                 } */
                MenutableView.autoresizingMask = UIViewAutoresizingFlexibleHeight | UIViewAutoresizingFlexibleWidth;
                MenutableView.delegate = self;
                MenutableView.dataSource = self;
                
                [self.view addSubview:MenutableView];
                heightrow = 0;
                
            } else if ([list_template isEqualToString:@"1"]) {
                
            }
        }
        
        catid =nil;
        cid =nil;
        ctitle =nil;
        cimage =nil;
        cthumbnail =nil;
    
    }
    sqlite3_close(database);
}

-(NSInteger) showFavorite {
    sqlite3 *database;
    NSString *allowFav;
    
    if(sqlite3_open([appdelegate.databasePath UTF8String], &database) == SQLITE_OK) {
        
        //if ([aplugintype isEqualToString:@"content"]) {
        
        NSString *sqlFav;
        /*** Push Notification 27Aug2015 ***/
        sqlFav = [[NSString alloc] initWithFormat:@"SELECT * from content_category WHERE top_id = %@", appdelegate.plugin_id];
        //NSLog(@"showFavorite:: %@", sqlFav);
        
        const char *sqlStatementFav = [sqlFav UTF8String];
        sqlite3_stmt *compiledStatementFav;
        if(sqlite3_prepare_v2(database, sqlStatementFav, -1, &compiledStatementFav, NULL) == SQLITE_OK) {
            while(sqlite3_step(compiledStatementFav) == SQLITE_ROW) {
                
                allowFav = [NSString stringWithUTF8String:(char *)sqlite3_column_text(compiledStatementFav, 9)];
                
            }
        }
        // Release the compiled statement from memory
        sqlite3_finalize(compiledStatementFav);
        
    }
    sqlite3_close(database);
    
    if ([allowFav isEqualToString:@"1"]) {
        return 1;
    } else {
        return 0;
    }
}

-(NSInteger) showComments {
    sqlite3 *database;
    NSString *allowCom;
    
    if(sqlite3_open([appdelegate.databasePath UTF8String], &database) == SQLITE_OK) {
        
        //if ([aplugintype isEqualToString:@"content"]) {
        
        NSString *sqlCom;
        /*** Push Notification 27Aug2015 ***/
        sqlCom = [[NSString alloc] initWithFormat:@"SELECT * from content_category WHERE top_id = %@", appdelegate.plugin_id];
        //NSLog(@"appdelegate.plugin_id:: %@", appdelegate.plugin_id);
        
        const char *sqlStatementCom = [sqlCom UTF8String];
        sqlite3_stmt *compiledStatementCom;
        if(sqlite3_prepare_v2(database, sqlStatementCom, -1, &compiledStatementCom, NULL) == SQLITE_OK) {
            while(sqlite3_step(compiledStatementCom) == SQLITE_ROW) {
                
                allowCom = [NSString stringWithUTF8String:(char *)sqlite3_column_text(compiledStatementCom, 10)];
                
            }
        }
        // Release the compiled statement from memory
        sqlite3_finalize(compiledStatementCom);
        
    }
    sqlite3_close(database);
    
    if ([allowCom isEqualToString:@"1"]) {
        return 1;
    } else {
        return 0;
    }
}

-(void) checkButtonClick:(id)sender {
    
    UIButton *clicked = (UIButton *) sender;
    NSDictionary *dictionary = [listOfContents objectAtIndex:clicked.tag];
    
    cont_id = [dictionary objectForKey:@"id"];
    cont_title = [dictionary objectForKey:@"title"];
    link_url = [dictionary objectForKey:@"linkurl"];
    
    if (appdelegate.showloading) {
        alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"loading", nil) message:nil delegate:self cancelButtonTitle:nil otherButtonTitles: nil];
        
        //a simple activity indicator:
        UIActivityIndicatorView *activityIndicator = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
        activityIndicator.frame= CGRectMake(50, 10, 37, 37);
        activityIndicator.autoresizingMask = UIViewAutoresizingFlexibleTopMargin | UIViewAutoresizingFlexibleRightMargin | UIViewAutoresizingFlexibleBottomMargin | UIViewAutoresizingFlexibleLeftMargin;
        [activityIndicator startAnimating];
        
        //the magic line below,
        //we associate the activity indicator to the alert view: (addSubview is not used)
        [alert setValue:activityIndicator forKey:@"accessoryView"];
        
        [alert show];
    
        // Adjust the indicator so it is up a few pixels from the bottom of the alert
        [UIView beginAnimations:nil context:nil];
        [UIView setAnimationDuration:0.0];
        [UIView setAnimationDelay:0.0];
        [UIView setAnimationTransition:UIViewAnimationTransitionNone forView:alert cache:YES];
        [UIView setAnimationDelegate:self];
        [UIView setAnimationDidStopSelector:@selector(startupAnimationDoneIndicator:finished:context:)];
        alert.alpha = 0.5;
        [UIView commitAnimations];
    } else {
        [self goNextPage];
    }
}

-(void) checkButtonFavClick:(id)sender {
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSString *hasLoggedInFB = [defaults objectForKey:@"hasLoggedInFB"];
    NSString *hasLoggedInMyApp = [defaults objectForKey:@"hasLoggedInMyApp"];
    
    if ([hasLoggedInFB isEqualToString:@"YES"] || [hasLoggedInMyApp isEqualToString:@"YES"])
    {
        UIButton *clicked = (UIButton *) sender;
        NSDictionary *dictionary = [listOfContents objectAtIndex:clicked.tag];
        
        NSString *cid = [dictionary objectForKey:@"id"];
        NSString *urlAddress2 = [[NSString alloc] initWithFormat:@"%@favorite_process.php?db=%@&userid=%@&udid=%@&cuid=%@&top=%@&cid=%@", appdelegate.user_apipath, [self urlEncode:appdelegate.db], [self urlEncode:appdelegate.user], [self urlEncode:appdelegate.uuid], [self urlEncode:appdelegate.customer_id], appdelegate.plugin_id, cid];
        currentRequestMenu = @"AddFavorite";
        [self connectToServer:urlAddress2];
    }
    else {
        [self.view addSubview:view_popoverMain];
        [view_popoverMain addSubview:view_plain];
        [view_plain addSubview:buttonclose];
        [view_plain addSubview:buttonlogin];
        [view_plain addSubview:lbllogin];
        //[view_popoverMain setHidden:NO];
        //[self.view bringSubviewToFront:view_popoverMain];
    }
    
}

-(void) checkButtonCommentsClick:(id)sender {
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSString *hasLoggedInFB = [defaults objectForKey:@"hasLoggedInFB"];
    NSString *hasLoggedInMyApp = [defaults objectForKey:@"hasLoggedInMyApp"];
    
    if ([hasLoggedInFB isEqualToString:@"YES"] || [hasLoggedInMyApp isEqualToString:@"YES"])
    {
        UIButton *clicked = (UIButton *) sender;
        NSDictionary *dictionary = [listOfContents objectAtIndex:clicked.tag];
        
        NSString *cid = [dictionary objectForKey:@"id"];
        CommentsViewController *commentsViewController = [[CommentsViewController alloc] init];
        commentsViewController.conttitle = [dictionary objectForKey:@"title"];
        commentsViewController.contId = cid;
        commentsViewController.topId = appdelegate.plugin_id;
        [appdelegate.navController pushViewController:commentsViewController animated:NO];
    }
    else {
        [self.view addSubview:view_popoverMain];
        [view_popoverMain addSubview:view_plain];
        [view_plain addSubview:buttonclose];
        [view_plain addSubview:buttonlogin];
        [view_plain addSubview:lbllogin];
        //[view_popoverMain setHidden:NO];
        //[self.view bringSubviewToFront:view_popoverMain];
    }
    
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    //NSLog(@"numberOfSectionsInTableView");
	if (tableView == GridtableView)
    {
        return [sections count];
        
    }
    
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
	//NSLog(@">>> Entering *** %s <<<", __PRETTY_FUNCTION__);
    
    //NSLog(@"tableView %@", tableView);
    
    if (tableView == MenutableView)
    {
        return [listOfContents count];
        
    }
    
    if (tableView == GridtableView) {
        return 1;
    }
    
    return 0;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (tableView == MenutableView) {
        if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone) {
            // For iPhone
            if ([list_template isEqualToString:@"7"]) {
                if ([self showFavorite]==1 || [self showComments]==1) {
                    return 60;
                } else {
                    return 30;
                }
                //return 30;
            } else if ([list_template isEqualToString:@"9"]) {
                if([listOfSameDate count]>0)
                {
                    
                    NSDictionary *dictionary = [listOfSameDate objectAtIndex:indexPath.row];
                    
                    if ([[dictionary objectForKey:@"SameDate"] isEqualToString:@"0"]) {
                        
                        if ([self showFavorite]==1 || [self showComments]==1) {
                            return self.view.frame.size.width/2-10+20+5+70;
                        } else {
                            return self.view.frame.size.width/2-10+20+5+40;
                        }
                        //return self.view.frame.size.width/2-10+20+5+40;
                        
                    } else {
                        
                        return 70;
                        
                    }
                }
            } else if ([list_template isEqualToString:@"6"]) {
        
                if ([self showFavorite]==1 || [self showComments]==1) {
                    return 100;
                } else {
                    return 70;
                }
            } else {
                if ([self showFavorite]==1 || [self showComments]==1) {
                    return 100;
                } else {
                    return 70;
                }
                //return 70;
            }
            
        } else {
            if ([list_template isEqualToString:@"7"]) {
                if ([self showFavorite]==1 || [self showComments]==1) {
                    return 80;
                } else {
                    return 50;
                }
                //return 50;
            } else if ([list_template isEqualToString:@"9"]) {
                if([listOfSameDate count]>0)
                {
                    
                    NSDictionary *dictionary = [listOfSameDate objectAtIndex:indexPath.row];
                    
                    if ([[dictionary objectForKey:@"SameDate"] isEqualToString:@"0"]) {
                        
                        if ([self showFavorite]==1 || [self showComments]==1) {
                            return self.view.frame.size.width/2-10+20+5+105;
                        } else {
                            return self.view.frame.size.width/2-10+20+5+66;
                        }
                        //return self.view.frame.size.width/2-10+33+5+66;;
                        
                    } else {
                        
                        return 110;
                      
                    }
                    
                }
            } else if ([list_template isEqualToString:@"6"]) {

                if ([self showFavorite]==1 || [self showComments]==1) {
                    return 150;
                } else {
                    return 110;
                }
            } else {
    
                if ([self showFavorite]==1 || [self showComments]==1) {
                    return 150;
                } else {
                    return 110;
                }
                //return 110;
            }
        }
    }
    
    if (tableView == GridtableView)
    {
        int count = [listOfContents count];
        
		int numRows = count/2;
        int numRows1 = (count)%2;
        
        if (numRows1>0) {
            if ([self showFavorite]==1 || [self showComments]==1) {
                if ([list_template isEqualToString:@"4"]) {
                    return (numRows+1) * self.view.frame.size.width/6*5; //4*3
                } else if ([list_template isEqualToString:@"8"]) {
                    return (numRows+1) * self.view.frame.size.width/7*5; //5*3
                } else {
                    return (numRows+1) * self.view.frame.size.width/6*4; //2
                }
            } else {
                if ([list_template isEqualToString:@"4"]) {
                    return (numRows+1) * self.view.frame.size.width/4*3; //4*3
                } else if ([list_template isEqualToString:@"8"]) {
                    return (numRows+1) * self.view.frame.size.width/5*3; //5*3
                } else {
                    return (numRows+1) * self.view.frame.size.width/2; //2
                }
            }
        } else {
            if ([self showFavorite]==1 || [self showComments]==1) {
                if ([list_template isEqualToString:@"4"]) {
                    return (numRows) * self.view.frame.size.width/6*5;
                } else if ([list_template isEqualToString:@"8"]) {
                    return (numRows) * self.view.frame.size.width/7*5;
                } else {
                    return (numRows) * self.view.frame.size.width/6*4;
                }
            } else {
                if ([list_template isEqualToString:@"4"]) {
                    return (numRows) * self.view.frame.size.width/4*3;
                } else if ([list_template isEqualToString:@"8"]) {
                    return (numRows) * self.view.frame.size.width/5*3;
                } else {
                    return (numRows) * self.view.frame.size.width/2;
                }
            }
        }
    }
    
	return 44;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    //NSLog(@"cellForRowAtIndexPath");
    if (tableView == MenutableView)
    {
        //static NSString *CellIdentifier = @"CellIdentifier";
        //UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
        NSString *cellIdentifier = [NSString stringWithFormat:@"cell%d",indexPath.row];
        UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
        
        UIImageView *imgViewMenu;
        const NSInteger LEFT_TAG = 1000;
        UILabel *menutext;
        const NSInteger RIGHT_TAG = 1001;
        UILabel *pricetitle;
        const NSInteger RIGHT_TAG1 = 1002;
        UILabel *price;
        const NSInteger RIGHT_TAG2 = 1003;
        UILabel *var1;
        const NSInteger RIGHT_TAG3 = 1004;
        UILabel *distance;
        const NSInteger RIGHT_TAG4 = 1005;
        UIButton *btnimgViewMenu;
        const NSInteger RIGHT_TAG5 = 1006;
        UILabel *desc;
        const NSInteger RIGHT_TAG6 = 1007;
        UILabel *datetime;
        const NSInteger RIGHT_TAG7 = 1008;
        UIImageView *ImageBarView;
        const NSInteger RIGHT_TAG8 = 1009;
        UIView *overlay;
        const NSInteger RIGHT_TAG9 = 1010;
        UIButton *btnimgViewFav;
        const NSInteger TAG1 = 1011;
        UILabel *lblFav;
        const NSInteger TAG2 = 1012;
        UIButton *btnimgViewCom;
        const NSInteger TAG3 = 1013;
        UILabel *lblCom;
        const NSInteger TAG4 = 1014;
        
        //if (cell == nil)
        //{
            
            //cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
        if(!cell) {
            cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier];
            //cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
            
            imgViewMenu = [[UIImageView alloc] init];
            imgViewMenu.tag = LEFT_TAG;
            
            menutext = [[UILabel alloc] init];
            menutext.tag = RIGHT_TAG;
            
            pricetitle = [[UILabel alloc] init];
            pricetitle.tag = RIGHT_TAG1;
            
            price = [[UILabel alloc] init];
            price.tag = RIGHT_TAG2;
            
            var1 = [[UILabel alloc] init];
            var1.tag = RIGHT_TAG3;
            
            distance = [[UILabel alloc] init];
            distance.tag = RIGHT_TAG4;
            
            btnimgViewMenu = [UIButton buttonWithType:UIButtonTypeCustom];
            btnimgViewMenu = [[UIButton alloc] init];
            btnimgViewMenu.tag = RIGHT_TAG5;
            
            desc = [[UILabel alloc] init];
            desc.tag = RIGHT_TAG6;
            
            datetime = [[UILabel alloc] init];
            datetime.tag = RIGHT_TAG7;
            
            ImageBarView = [[UIImageView alloc] init];
            ImageBarView.tag = RIGHT_TAG8;
            
            overlay = [[UIView alloc] init];
            overlay.tag = RIGHT_TAG9;
            
            btnimgViewFav = [UIButton buttonWithType:UIButtonTypeCustom];
            btnimgViewFav = [[UIButton alloc] init];
            btnimgViewFav.tag = TAG1;
            
            lblFav = [[UILabel alloc] init];
            lblFav.tag = TAG2;
            
            btnimgViewCom = [UIButton buttonWithType:UIButtonTypeCustom];
            btnimgViewCom = [[UIButton alloc] init];
            btnimgViewCom.tag = TAG3;
            
            lblCom = [[UILabel alloc] init];
            lblCom.tag = TAG4;
            
            if ([list_template isEqualToString:@"6"]) {
                
            
                if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone) {
                        // For iPhone
                    [menutext setFrame:CGRectMake(10, 5, self.view.frame.size.width-70, 20)];
                } else {
                    [menutext setFrame: CGRectMake(10, 5, self.view.frame.size.width-110, 33)];
                }
            
                [menutext setFont:[UIFont boldSystemFontOfSize:16]];
                menutext.textColor = [self colorWithHexString:appdelegate.themeColorCode];
                menutext.textAlignment = NSTextAlignmentLeft;
                menutext.backgroundColor = [UIColor clearColor];
                menutext.numberOfLines = 0;
                [cell.contentView addSubview:menutext];
                
                if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone) {
                    // For iPhone
                    [btnimgViewMenu setFrame:CGRectMake(self.view.frame.size.width-70, 10, 40, 40)];
                } else {
                    [btnimgViewMenu setFrame:CGRectMake(self.view.frame.size.width-110, 10, 80, 80)];
                }
                [cell.contentView addSubview:btnimgViewMenu];
                
                if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone) {
                    // For iPhone
                    [var1 setFrame: CGRectMake(10, 25, self.view.frame.size.width-70, 20)];
                } else {
                    [var1 setFrame: CGRectMake(10, 38, self.view.frame.size.width-110, 33)];
                }
                [var1 setFont:[UIFont systemFontOfSize:14]];
                var1.textColor = [UIColor grayColor];
                var1.textAlignment = NSTextAlignmentLeft;
                var1.backgroundColor = [UIColor clearColor];
                var1.numberOfLines = 0;
                [cell.contentView addSubview:var1];
                
                if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone) {
                    // For iPhone
                    [distance setFrame: CGRectMake(10, 45, self.view.frame.size.width-70, 20)];
                } else {
                    [distance setFrame: CGRectMake(10, 71, self.view.frame.size.width-110, 33)];
                }
                [distance setFont:[UIFont systemFontOfSize:14]];
                distance.textColor = [UIColor grayColor];
                distance.textAlignment = NSTextAlignmentLeft;
                distance.backgroundColor = [UIColor clearColor];
                distance.numberOfLines = 0;
                [cell.contentView addSubview:distance];
                
                if ([self showFavorite]==1) {
                    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone) {
                        // For iPhone
                        [lblFav setFrame:CGRectMake(self.view.frame.size.width/4, 70, self.view.frame.size.width/4, 20)];
                    } else {
                        [lblFav setFrame: CGRectMake(self.view.frame.size.width/4, 113, self.view.frame.size.width/4, 33)];
                    }
                    
                    [lblFav setFont:[UIFont boldSystemFontOfSize:16]];
                    lblFav.textColor = [UIColor blackColor];
                    lblFav.textAlignment = NSTextAlignmentLeft;
                    lblFav.backgroundColor = [UIColor clearColor];
                    lblFav.numberOfLines = 0;
                    [cell.contentView addSubview:lblFav];
                    
                    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone) {
                        // For iPhone
                        [btnimgViewFav setFrame:CGRectMake(self.view.frame.size.width/4-40, 65, 25, 25)];
                    } else {
                        [btnimgViewFav setFrame:CGRectMake(self.view.frame.size.width/4-53, 108, 38, 38)];
                    }
                    [cell.contentView addSubview:btnimgViewFav];
                }
                
                if ([self showComments]==1) {
                    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone) {
                        // For iPhone
                        [lblCom setFrame:CGRectMake(self.view.frame.size.width/4*3, 70, self.view.frame.size.width/4, 20)];
                    } else {
                        [lblCom setFrame: CGRectMake(self.view.frame.size.width/4*3, 113, self.view.frame.size.width/4, 33)];
                    }
                    
                    [lblCom setFont:[UIFont boldSystemFontOfSize:16]];
                    lblCom.textColor = [UIColor blackColor];
                    lblCom.textAlignment = NSTextAlignmentLeft;
                    lblCom.backgroundColor = [UIColor clearColor];
                    lblCom.numberOfLines = 0;
                    [cell.contentView addSubview:lblCom];
                    
                    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone) {
                        // For iPhone
                        [btnimgViewCom setFrame:CGRectMake(self.view.frame.size.width/4*3-40, 65, 25, 25)];
                    } else {
                        [btnimgViewCom setFrame:CGRectMake(self.view.frame.size.width/4*3-53, 108, 38, 38)];
                    }
                    [cell.contentView addSubview:btnimgViewCom];
                }
                
            } else if ([list_template isEqualToString:@"7"]) {
                
                
                if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone) {
                    // For iPhone
                    [menutext setFrame: CGRectMake(10, 5, self.view.frame.size.width-20, 20)];
                } else {
                    [menutext setFrame: CGRectMake(10, 5, self.view.frame.size.width-20, 33)];
                }
                
                [menutext setFont:[UIFont boldSystemFontOfSize:16]];
                menutext.textColor = [self colorWithHexString:appdelegate.themeColorCode];
                menutext.textAlignment = NSTextAlignmentLeft;
                menutext.backgroundColor = [UIColor clearColor];
                menutext.numberOfLines = 0;
                [cell.contentView addSubview:menutext];
                
                if ([self showFavorite]==1) {
                    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone) {
                        // For iPhone
                        [lblFav setFrame:CGRectMake(self.view.frame.size.width/4, 30, self.view.frame.size.width/4, 20)];
                    } else {
                        [lblFav setFrame: CGRectMake(self.view.frame.size.width/4, 43, self.view.frame.size.width/4, 33)];
                    }
                    
                    [lblFav setFont:[UIFont boldSystemFontOfSize:16]];
                    lblFav.textColor = [UIColor blackColor];
                    lblFav.textAlignment = NSTextAlignmentLeft;
                    lblFav.backgroundColor = [UIColor clearColor];
                    lblFav.numberOfLines = 0;
                    [cell.contentView addSubview:lblFav];
                    
                    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone) {
                        // For iPhone
                        [btnimgViewFav setFrame:CGRectMake(self.view.frame.size.width/4-40, 25, 25, 25)];
                    } else {
                        [btnimgViewFav setFrame:CGRectMake(self.view.frame.size.width/4-53, 38, 38, 38)];
                    }
                    [cell.contentView addSubview:btnimgViewFav];
                }
                
                if ([self showComments]==1) {
                    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone) {
                        // For iPhone
                        [lblCom setFrame:CGRectMake(self.view.frame.size.width/4*3, 30, self.view.frame.size.width/4, 20)];
                    } else {
                        [lblCom setFrame: CGRectMake(self.view.frame.size.width/4*3, 43, self.view.frame.size.width/4, 33)];
                    }
                    
                    [lblCom setFont:[UIFont boldSystemFontOfSize:16]];
                    lblCom.textColor = [UIColor blackColor];
                    lblCom.textAlignment = NSTextAlignmentLeft;
                    lblCom.backgroundColor = [UIColor clearColor];
                    lblCom.numberOfLines = 0;
                    [cell.contentView addSubview:lblCom];
                    
                    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone) {
                        // For iPhone
                        [btnimgViewCom setFrame:CGRectMake(self.view.frame.size.width/4*3-40, 25, 25, 25)];
                    } else {
                        [btnimgViewCom setFrame:CGRectMake(self.view.frame.size.width/4*3-53, 38, 38, 38)];
                    }
                    [cell.contentView addSubview:btnimgViewCom];
                }
                
            } else if ([list_template isEqualToString:@"9"]) {
                
                if([listOfSameDate count]>0)
                {
                    //NSLog(@"listOfSameDate: %@", [listOfSameDate objectAtIndex:indexPath.row]);
                    NSDictionary *dictionary = [listOfSameDate objectAtIndex:indexPath.row];
                    
                    if ([[dictionary objectForKey:@"SameDate"] isEqualToString:@"0"]) {
                        
                        if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone) {
                            // For iPhone
                            [datetime setFrame: CGRectMake(5, 5, self.view.frame.size.width-10, 20)];
                        } else {
                            [datetime setFrame: CGRectMake(5, 5, self.view.frame.size.width-10, 33)];
                        }
                        
                        [datetime setFont:[UIFont boldSystemFontOfSize:12]];
                        datetime.textColor = [UIColor grayColor];
                        datetime.textAlignment = NSTextAlignmentLeft;
                        datetime.backgroundColor = [UIColor clearColor];
                        datetime.numberOfLines = 0;
                        [cell.contentView addSubview:datetime];
                        
                        if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone) {
                            // For iPhone
                            [imgViewMenu setFrame:CGRectMake(5, 20+5, self.view.frame.size.width-10, self.view.frame.size.width/2-10)];
                        } else {
                            [imgViewMenu setFrame:CGRectMake(5, 33+5, self.view.frame.size.width-10, self.view.frame.size.width/2-10)];
                        }
                        [cell.contentView addSubview:imgViewMenu];
                        
                        if([listOfContents count]>0) {
                            NSDictionary *dictionary1 = [listOfContents objectAtIndex:indexPath.row];
                        
                            if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone) {
                                // For iPhone
                                [ImageBarView setFrame:CGRectMake(0, imgViewMenu.frame.size.height, self.view.frame.size.width-10, 30)];
                            } else {
                                [ImageBarView setFrame:CGRectMake(0, imgViewMenu.frame.size.height, self.view.frame.size.width-10, 43)];
                            }
                            
                            [overlay setFrame:CGRectMake(0, 0, ImageBarView.frame.size.width, ImageBarView.frame.size.height)];
                            overlay.backgroundColor = [UIColor blackColor];
                            overlay.alpha = 0.4f;
                            
                            [ImageBarView addSubview:overlay];
                            
                            if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone) {
                                // For iPhone
                                [menutext setFrame: CGRectMake(5, 0, self.view.frame.size.width-10, 20)];
                            } else {
                                [menutext setFrame: CGRectMake(5, 0, self.view.frame.size.width-10, 33)];
                            }
                            
                            [menutext setFont:[UIFont boldSystemFontOfSize:16]];
                            menutext.textColor = [UIColor whiteColor];
                            menutext.textAlignment = NSTextAlignmentLeft;
                            menutext.backgroundColor = [UIColor clearColor];
                            menutext.numberOfLines = 0;
                            [ImageBarView addSubview:menutext];
                        
                            menutext.text = [NSString stringWithFormat:@"%@", [dictionary1 objectForKey:@"title"]];
                            //NSLog(@"dictionary1 %@", menutext.text);
                            CGSize constrainedSize = CGSizeMake(menutext.frame.size.width  , 9999);
                            
                            NSDictionary *attributesDictionary = [NSDictionary dictionaryWithObjectsAndKeys:
                                                                  [UIFont boldSystemFontOfSize:16], NSFontAttributeName,
                                                                  nil];
                            
                            NSMutableAttributedString *string = [[NSMutableAttributedString alloc] initWithString:menutext.text attributes:attributesDictionary];
                            
                            CGRect requiredHeight = [string boundingRectWithSize:constrainedSize options:NSStringDrawingUsesLineFragmentOrigin context:nil];
                            
                            if (requiredHeight.size.width > menutext.frame.size.width) {
                                requiredHeight = CGRectMake(5,0, menutext.frame.size.width-10, requiredHeight.size.height);
                            } else {
                                requiredHeight = CGRectMake(5,0, menutext.frame.size.width-10, menutext.frame.size.height);
                            }
                            CGRect newFrame = menutext.frame;
                            newFrame.size.height = requiredHeight.size.height;
                            menutext.frame = newFrame;
                            ImageBarView.frame = CGRectMake(0, imgViewMenu.frame.size.height-newFrame.size.height, self.view.frame.size.width-10, newFrame.size.height);
                            
                            overlay.frame = CGRectMake(0, 0, ImageBarView.frame.size.width, ImageBarView.frame.size.height);
                            
                            [imgViewMenu addSubview:ImageBarView];
                        }
                        
                        if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone) {
                            // For iPhone
                            [desc setFrame: CGRectMake(5, self.view.frame.size.width/2-10+20+5, self.view.frame.size.width-10, 40)];
                        } else {
                            [desc setFrame: CGRectMake(5, self.view.frame.size.width/2-10+33+5, self.view.frame.size.width-10, 66)];
                        }
                        [desc setFont:[UIFont systemFontOfSize:14]];
                        desc.textColor = [UIColor grayColor];
                        desc.textAlignment = NSTextAlignmentLeft;
                        desc.backgroundColor = [UIColor clearColor];
                        desc.numberOfLines = 0;
                        desc.lineBreakMode = NSLineBreakByTruncatingTail;
                        [cell.contentView addSubview:desc];
                        
                        if ([self showFavorite]==1) {
                            if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone) {
                                // For iPhone
                                [lblFav setFrame:CGRectMake(self.view.frame.size.width/4, self.view.frame.size.width/2-10+20+5+45, self.view.frame.size.width/4, 20)];
                            } else {
                                [lblFav setFrame: CGRectMake(self.view.frame.size.width/4, self.view.frame.size.width/2-10+20+5+71, self.view.frame.size.width/4, 33)];
                            }
                            
                            [lblFav setFont:[UIFont boldSystemFontOfSize:16]];
                            lblFav.textColor = [UIColor blackColor];
                            lblFav.textAlignment = NSTextAlignmentLeft;
                            lblFav.backgroundColor = [UIColor clearColor];
                            lblFav.numberOfLines = 0;
                            [cell.contentView addSubview:lblFav];
                            
                            if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone) {
                                // For iPhone
                                [btnimgViewFav setFrame:CGRectMake(self.view.frame.size.width/4-40, self.view.frame.size.width/2-10+20+5+40, 25, 25)];
                            } else {
                                [btnimgViewFav setFrame:CGRectMake(self.view.frame.size.width/4-53, self.view.frame.size.width/2-10+20+5+66, 38, 38)];
                            }
                            [cell.contentView addSubview:btnimgViewFav];
                        }
                        
                        if ([self showComments]==1) {
                            if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone) {
                                // For iPhone
                                [lblCom setFrame:CGRectMake(self.view.frame.size.width/4*3, self.view.frame.size.width/2-10+20+5+45, self.view.frame.size.width/4, 20)];
                            } else {
                                [lblCom setFrame: CGRectMake(self.view.frame.size.width/4*3, self.view.frame.size.width/2-10+20+5+71, self.view.frame.size.width/4, 33)];
                            }
                            
                            [lblCom setFont:[UIFont boldSystemFontOfSize:16]];
                            lblCom.textColor = [UIColor blackColor];
                            lblCom.textAlignment = NSTextAlignmentLeft;
                            lblCom.backgroundColor = [UIColor clearColor];
                            lblCom.numberOfLines = 0;
                            [cell.contentView addSubview:lblCom];
                            
                            if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone) {
                                // For iPhone
                                [btnimgViewCom setFrame:CGRectMake(self.view.frame.size.width/4*3-40, self.view.frame.size.width/2-10+20+5+40, 25, 25)];
                            } else {
                                [btnimgViewCom setFrame:CGRectMake(self.view.frame.size.width/4*3-53, self.view.frame.size.width/2-10+20+5+66, 38, 38)];
                            }
                            [cell.contentView addSubview:btnimgViewCom];
                        }
                        
                    } else {
                        if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone) {
                            // For iPhone
                            [imgViewMenu setFrame:CGRectMake(self.view.frame.size.width-65, 5, 60, 60)];
                        } else {
                            [imgViewMenu setFrame:CGRectMake(self.view.frame.size.width-105, 5, 100, 100)];
                        }
                        [cell.contentView addSubview:imgViewMenu];
                        
                        //} else if ([[dictionary objectForKey:@"object_type"] isEqualToString:@"text"]) {
                    
                        if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone) {
                            // For iPhone
                            [menutext setFrame: CGRectMake(5, 10, self.view.frame.size.width-75, 20)];
                        } else {
                            [menutext setFrame: CGRectMake(5, 10, self.view.frame.size.width-115, 33)];
                        }
                        //}
                        [menutext setFont:[UIFont boldSystemFontOfSize:16]];
                        menutext.textColor = [UIColor grayColor];
                        menutext.textAlignment = NSTextAlignmentLeft;
                        menutext.backgroundColor = [UIColor clearColor];
                        menutext.numberOfLines = 0;
                        [cell.contentView addSubview:menutext];
                        
                        if ([self showFavorite]==1) {
                            if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone) {
                                // For iPhone
                                [lblFav setFrame:CGRectMake((self.view.frame.size.width-65)/4, 40, (self.view.frame.size.width-65)/4, 20)];
                            } else {
                                [lblFav setFrame: CGRectMake(self.view.frame.size.width/4, 53, (self.view.frame.size.width-105)/4, 33)];
                            }
                            
                            [lblFav setFont:[UIFont boldSystemFontOfSize:16]];
                            lblFav.textColor = [UIColor blackColor];
                            lblFav.textAlignment = NSTextAlignmentLeft;
                            lblFav.backgroundColor = [UIColor clearColor];
                            lblFav.numberOfLines = 0;
                            [cell.contentView addSubview:lblFav];
                            
                            if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone) {
                                // For iPhone
                                [btnimgViewFav setFrame:CGRectMake((self.view.frame.size.width-65)/4-40, 35, 25, 25)];
                            } else {
                                [btnimgViewFav setFrame:CGRectMake((self.view.frame.size.width-105)/4-53, 48, 38, 38)];
                            }
                            [cell.contentView addSubview:btnimgViewFav];
                        }
                        
                        if ([self showComments]==1) {
                            if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone) {
                                // For iPhone
                                [lblCom setFrame:CGRectMake((self.view.frame.size.width-65)/4*3, 40, (self.view.frame.size.width-65)/4, 20)];
                            } else {
                                [lblCom setFrame: CGRectMake((self.view.frame.size.width-105)/4*3, 53, (self.view.frame.size.width-105)/4, 33)];
                            }
                            
                            [lblCom setFont:[UIFont boldSystemFontOfSize:16]];
                            lblCom.textColor = [UIColor blackColor];
                            lblCom.textAlignment = NSTextAlignmentLeft;
                            lblCom.backgroundColor = [UIColor clearColor];
                            lblCom.numberOfLines = 0;
                            [cell.contentView addSubview:lblCom];
                            
                            if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone) {
                                // For iPhone
                                [btnimgViewCom setFrame:CGRectMake((self.view.frame.size.width-65)/4*3-40, 35, 25, 25)];
                            } else {
                                [btnimgViewCom setFrame:CGRectMake((self.view.frame.size.width-105)/4*3-53, 48, 38, 38)];
                            }
                            [cell.contentView addSubview:btnimgViewCom];
                        }
                        
                    }
                }
                
            } else {
                
                //if ([[dictionary objectForKey:@"object_type"] isEqualToString:@"img"]) {
                if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone) {
                    // For iPhone
                    if ([self showFavorite]==1 || [self showComments]==1) {
                        [imgViewMenu setFrame:CGRectMake(5, 20, 60, 60)];
                    } else {
                        [imgViewMenu setFrame:CGRectMake(5, 5, 60, 60)];
                    }
                } else {
                    if ([self showFavorite]==1 || [self showComments]==1) {
                        [imgViewMenu setFrame:CGRectMake(5, 20, 100, 100)];
                    } else {
                        [imgViewMenu setFrame:CGRectMake(5, 5, 100, 100)];
                    }
                }
                [cell.contentView addSubview:imgViewMenu];
                
                //} else if ([[dictionary objectForKey:@"object_type"] isEqualToString:@"text"]) {
                
                /*if ([list_template isEqualToString:@"4"]) {
                    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone) {
                        // For iPhone
                        menutext = [[UILabel alloc] initWithFrame: CGRectMake(70, 0, self.view.frame.size.width-70, 60)];
                    } else {
                        menutext = [[UILabel alloc] initWithFrame: CGRectMake(110, 0, self.view.frame.size.width-110, 100)];
                    }
                } else { */
                    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone) {
                        // For iPhone
                        [menutext setFrame: CGRectMake(70, 5, self.view.frame.size.width-70, 20)];
                    } else {
                        [menutext setFrame: CGRectMake(110, 5, self.view.frame.size.width-110, 33)];
                    }
                //}
                [menutext setFont:[UIFont boldSystemFontOfSize:16]];
                if ([list_template isEqualToString:@"5"]) {
                    menutext.textColor = [self colorWithHexString:appdelegate.themeColorCode];
                } else {
                    menutext.textColor = [UIColor grayColor];
                }
                menutext.textAlignment = NSTextAlignmentLeft;
                menutext.backgroundColor = [UIColor clearColor];
                menutext.numberOfLines = 0;
                [cell.contentView addSubview:menutext];
                
                if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone) {
                    // For iPhone
                    [pricetitle setFrame: CGRectMake(70, 25, 30, 20)];
                } else {
                    [pricetitle setFrame: CGRectMake(110, 38, 30, 33)];
                }
                [pricetitle setFont:[UIFont boldSystemFontOfSize:16]];
                pricetitle.textColor = [UIColor grayColor];
                pricetitle.textAlignment = NSTextAlignmentLeft;
                pricetitle.backgroundColor = [UIColor clearColor];
                pricetitle.numberOfLines = 0;
                [cell.contentView addSubview:pricetitle];
                
                if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone) {
                    // For iPhone
                    [price setFrame: CGRectMake(70+30, 25, self.view.frame.size.width-70-30, 20)];
                } else {
                    [price setFrame: CGRectMake(110+30, 38, self.view.frame.size.width-110-30, 33)];
                }
                [price setFont:[UIFont boldSystemFontOfSize:16]];
                price.textColor = [UIColor grayColor];
                price.textAlignment = NSTextAlignmentLeft;
                price.backgroundColor = [UIColor clearColor];
                price.numberOfLines = 0;
                [cell.contentView addSubview:price];
                
                if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone) {
                    // For iPhone
                    [pricetitle setFrame: CGRectMake(70, 25, 30, 20)];
                } else {
                    [pricetitle setFrame: CGRectMake(110, 38, 30, 33)];
                }
                [pricetitle setFont:[UIFont boldSystemFontOfSize:16]];
                pricetitle.textColor = [UIColor grayColor];
                pricetitle.textAlignment = NSTextAlignmentLeft;
                pricetitle.backgroundColor = [UIColor clearColor];
                pricetitle.numberOfLines = 0;
                [cell.contentView addSubview:pricetitle];
                
                if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone) {
                    // For iPhone
                    [var1 setFrame: CGRectMake(70, 45, self.view.frame.size.width-70, 20)];
                } else {
                    [var1 setFrame: CGRectMake(110, 71, self.view.frame.size.width-110, 33)];
                }
                [var1 setFont:[UIFont systemFontOfSize:14]];
                var1.textColor = [UIColor grayColor];
                var1.textAlignment = NSTextAlignmentLeft;
                var1.backgroundColor = [UIColor clearColor];
                var1.numberOfLines = 0;
                [cell.contentView addSubview:var1];
                //}
                
                if ([themepluginid isEqualToString:@"45"]) {
                    if ([list_template isEqualToString:@"5"]) {
                        if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone) {
                            // For iPhone
                            [desc setFrame: CGRectMake(70, 25, self.view.frame.size.width-10, 40)];
                        } else {
                            [desc setFrame: CGRectMake(110, 38, self.view.frame.size.width-10, 66)];
                        }
                        [desc setFont:[UIFont systemFontOfSize:14]];
                        desc.textColor = [UIColor grayColor];
                        desc.textAlignment = NSTextAlignmentLeft;
                        desc.backgroundColor = [UIColor clearColor];
                        desc.numberOfLines = 0;
                        desc.lineBreakMode = NSLineBreakByTruncatingTail;
                        [cell.contentView addSubview:desc];
                    }
                }
                
                if ([self showFavorite]==1) {
                    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone) {
                        // For iPhone
                        [lblFav setFrame:CGRectMake((self.view.frame.size.width-70)/4+70, 75, (self.view.frame.size.width-70)/4, 20)];
                    } else {
                        [lblFav setFrame: CGRectMake((self.view.frame.size.width-70)/4+110, 115, (self.view.frame.size.width-110)/4, 33)];
                    }
                    
                    [lblFav setFont:[UIFont boldSystemFontOfSize:16]];
                    lblFav.textColor = [UIColor blackColor];
                    lblFav.textAlignment = NSTextAlignmentLeft;
                    lblFav.backgroundColor = [UIColor clearColor];
                    lblFav.numberOfLines = 0;
                    [cell.contentView addSubview:lblFav];
                    
                    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone) {
                        // For iPhone
                        [btnimgViewFav setFrame:CGRectMake((self.view.frame.size.width-70)/4-40+70, 70, 25, 25)];
                    } else {
                        [btnimgViewFav setFrame:CGRectMake((self.view.frame.size.width-110)/4-53+110, 110, 38, 38)];
                    }
                    [cell.contentView addSubview:btnimgViewFav];
                }
                
                if ([self showComments]==1) {
                    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone) {
                        // For iPhone
                        [lblCom setFrame:CGRectMake((self.view.frame.size.width-70)/4*3+70, 75, self.view.frame.size.width/4, 20)];
                    } else {
                        [lblCom setFrame: CGRectMake((self.view.frame.size.width-110)/4*3+110, 115, self.view.frame.size.width/4, 33)];
                    }
                    
                    [lblCom setFont:[UIFont boldSystemFontOfSize:16]];
                    lblCom.textColor = [UIColor blackColor];
                    lblCom.textAlignment = NSTextAlignmentLeft;
                    lblCom.backgroundColor = [UIColor clearColor];
                    lblCom.numberOfLines = 0;
                    [cell.contentView addSubview:lblCom];
                    
                    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone) {
                        // For iPhone
                        [btnimgViewCom setFrame:CGRectMake((self.view.frame.size.width-70)/4*3-40+70, 70, 25, 25)];
                    } else {
                        [btnimgViewCom setFrame:CGRectMake((self.view.frame.size.width-110)/4*3-53+110, 110, 38, 38)];
                    }
                    [cell.contentView addSubview:btnimgViewCom];
                }
            }
        }
        else
        {
            //if ([[dictionary objectForKey:@"object_type"] isEqualToString:@"img"]) {
            imgViewMenu = (UIImageView *)[cell viewWithTag:LEFT_TAG];
            //} else if ([[dictionary objectForKey:@"object_type"] isEqualToString:@"text"]) {
            menutext = (UILabel *)[cell viewWithTag:RIGHT_TAG];
            pricetitle = (UILabel *)[cell viewWithTag:RIGHT_TAG1];
            price = (UILabel *)[cell viewWithTag:RIGHT_TAG2];
            var1 = (UILabel *)[cell viewWithTag:RIGHT_TAG3];
            distance = (UILabel *)[cell viewWithTag:RIGHT_TAG4];
            btnimgViewMenu = (UIButton *)[cell viewWithTag:RIGHT_TAG5];
            desc = (UILabel *)[cell viewWithTag:RIGHT_TAG6];
            datetime = (UILabel *)[cell viewWithTag:RIGHT_TAG7];
            ImageBarView = (UIImageView *)[cell viewWithTag:RIGHT_TAG8];
            overlay = (UIView *)[cell viewWithTag:RIGHT_TAG9];
            btnimgViewFav = (UIButton *)[cell viewWithTag:TAG1];
            lblFav = (UILabel *)[cell viewWithTag:TAG2];
            btnimgViewCom = (UIButton *)[cell viewWithTag:TAG3];
            lblCom = (UILabel *)[cell viewWithTag:TAG4];
            //}
        }
        
        if([listOfContents count]>0)
        {
            NSDictionary *dictionary = [listOfContents objectAtIndex:indexPath.row];
            
            NSString *imgURL = [dictionary objectForKey:@"image"];
            
            __block UIActivityIndicatorView *activityIndicator;
            __weak UIImageView *weakImageView = imgViewMenu;
            /*[weakImageView setImageWithURL:[NSURL URLWithString:imgURL] placeholderImage:[UIImage imageNamed:@"No-Image-icon.png"] options:SDWebImageProgressiveDownload progress:^(NSInteger receivedSize, NSInteger expectedSize)
             {
                 if (!activityIndicator)
                 {
                     [weakImageView addSubview:activityIndicator = [UIActivityIndicatorView.alloc initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray]];
                     activityIndicator.center = weakImageView.center;
                     [activityIndicator startAnimating];
                 }
             }
                                 completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType)
             {
                 [activityIndicator removeFromSuperview];
                 activityIndicator = nil;
             }]; */
            dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0), ^ {
                [weakImageView sd_setImageWithURL:[NSURL URLWithString:imgURL]
                                 placeholderImage:[UIImage imageNamed:@"No-Image-icon.png"]
                                          options:SDWebImageProgressiveDownload
                                         progress:^(NSInteger receivedSize, NSInteger expectedSize, NSURL *targetURL) {
                                             dispatch_async(dispatch_get_main_queue(), ^ {
                                                 if (!activityIndicator) {
                                                     
                                                     [weakImageView addSubview:activityIndicator = [UIActivityIndicatorView.alloc initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray]];
                                                     activityIndicator.center = weakImageView.center;
                                                     [activityIndicator startAnimating];
                                                 }
                                             });
                                         }
                                        completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
                                            dispatch_async(dispatch_get_main_queue(), ^ {
                                                //[MBProgressHUD hideHUDForView:self.view animated:YES];
                                                [activityIndicator removeFromSuperview];
                                                activityIndicator = nil;
                                            });
                                        }];
            });
            
            imgViewMenu.contentMode  = UIViewContentModeScaleAspectFill;
            [imgViewMenu setClipsToBounds:YES];
            
            /*if ([list_template isEqualToString:@"4"]) {
                menutext.text = [NSString stringWithFormat:@"%@", [dictionary objectForKey:@"title"]];
                pricetitle.text =nil;
                price.text =nil;
                var1.text =nil;
                distance.text =nil;
            } else */if ([list_template isEqualToString:@"6"]) {
                [btnimgViewMenu setImage:[UIImage imageNamed:@"info.png"] forState:UIControlStateNormal];
                [btnimgViewMenu addTarget:self action:@selector(checkButtonTapped:event:) forControlEvents:UIControlEventTouchUpInside];
                //btnimgViewMenu.contentMode  = UIViewContentModeScaleAspectFill;
                //[btnimgViewMenu setClipsToBounds:YES];
                menutext.text = [NSString stringWithFormat:@"%@", [dictionary objectForKey:@"title"]];
                pricetitle.text =nil;
                price.text =nil;
                var1.text = [NSString stringWithFormat:@"%@", [dictionary objectForKey:@"var1"]];
                CLLocation *location1 = [[CLLocation alloc] initWithLatitude:[[dictionary objectForKey:@"latitude"] floatValue] longitude:[[dictionary objectForKey:@"longitude"] floatValue]];
                CLLocation *location2 = [[CLLocation alloc] initWithLatitude:locationManager.location.coordinate.latitude longitude:locationManager.location.coordinate.longitude];
                //NSLog(@"Distance i meters: %f", [location1 distanceFromLocation:location2]);
                //NSLog(@"Latitude :: %f", locationManager.location.coordinate.latitude);
                //NSLog(@"longitude :: %f", locationManager.location.coordinate.longitude);
                //NSLog(@"Latitude11 :: %f", [[dictionary objectForKey:@"latitude"] floatValue]);
                //NSLog(@"longitude11 :: %f", [[dictionary objectForKey:@"longitude"] floatValue]);
                __strong NSString *strdistance;
                if (locationManager.location.coordinate.latitude > 0 && locationManager.location.coordinate.longitude > 0 && [[dictionary objectForKey:@"latitude"] floatValue] > 0 && [[dictionary objectForKey:@"longitude"] floatValue] > 0) {
                    
                    float fdistance = [location1 distanceFromLocation:location2];
                    fdistance = fdistance / 1000;
                    if (fdistance > 9) {
                        NSNumberFormatter* numberFormatter = [[NSNumberFormatter alloc] init];
                        [numberFormatter setFormatterBehavior: NSNumberFormatterBehavior10_4];
                        [numberFormatter setNumberStyle: NSNumberFormatterDecimalStyle];
                        NSString *numberString = [numberFormatter stringFromNumber: [NSNumber numberWithInteger: fdistance]];
                        strdistance = [NSString stringWithFormat:@"%@ %@", numberString, NSLocalizedString(@"km", nil)];
                    } else if (fdistance > 1) {
                        strdistance = [NSString stringWithFormat:@"%.1f %@", fdistance, NSLocalizedString(@"km", nil)];
                    } else if (fdistance >= 0) {
                        strdistance = [NSString stringWithFormat:@"%.2f", fdistance];
                        float f = [strdistance floatValue] * 1000;
                        strdistance = [NSString stringWithFormat:@"%.0f %@", f, NSLocalizedString(@"m", nil)];
                    }
                    
                    strdistance = [NSString stringWithFormat:@"%@ %@", strdistance, NSLocalizedString(@"away", nil)];
                } else {
                    strdistance = @"";
                }
                
                distance.text = strdistance;
                desc.text = nil;
                datetime.text = nil;
                
            } else if ([list_template isEqualToString:@"9"]) {
                //NSLog(@"description :: %@", [dictionary objectForKey:@"description"]);
                menutext.text = [NSString stringWithFormat:@"%@", [dictionary objectForKey:@"title"]];
                desc.text = [NSString stringWithFormat:@"%@", [dictionary objectForKey:@"description"]];
                NSDateFormatter *inputFormatter = [[NSDateFormatter alloc] init];
                [inputFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
                NSDate *formatterDate = [inputFormatter dateFromString:[dictionary objectForKey:@"date_updated"]];
                NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
                [formatter setDateFormat:@"hh:mm:ss a"];
                NSDateFormatter *formatterdate = [[NSDateFormatter alloc] init];
                [formatterdate setDateFormat:@"dd/M/yy"];
                //NSLog(@"Current Date: %@", [formatter stringFromDate:formatterDate]);
                //[[dictionary objectForKey:@"date_updated"] substringToIndex:10]
                datetime.text = [NSString stringWithFormat:@"%@ %@", [formatterdate stringFromDate:formatterDate], [formatter stringFromDate:formatterDate]];
                distance.text = nil;
            } else {
                menutext.text = [NSString stringWithFormat:@"%@", [dictionary objectForKey:@"title"]];
                if ([[dictionary objectForKey:@"price"] floatValue]>0) {
                    pricetitle.text = [NSString stringWithFormat:@"%@", [dictionary objectForKey:@"price_title"]];
                    price.text = [NSString stringWithFormat:@"%@", [dictionary objectForKey:@"price"]];
                } else {
                    pricetitle.text =nil;
                    price.text =nil;
                    if ([list_template isEqualToString:@"5"]) {
                        if ([self showFavorite]==1 || [self showComments]==1) {
                            if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone) {
                                // For iPhone
                                [menutext setFrame: CGRectMake(70, 20, self.view.frame.size.width-70, 20)];
                            } else {
                                [menutext setFrame: CGRectMake(110, 33, self.view.frame.size.width-110, 33)];
                            }
                        }
                    }
                }
                
                var1.text = [NSString stringWithFormat:@"%@", [dictionary objectForKey:@"var1"]];
                distance.text = nil;
                desc.text = nil;
                datetime.text = nil;
                
                //NSLog(@"description :: %@", [dictionary objectForKey:@"description"]);
                if ([themepluginid isEqualToString:@"45"]) {
                    if ([list_template isEqualToString:@"5"]) {
                        desc.text = [NSString stringWithFormat:@"%@", [dictionary objectForKey:@"description"]];
                    }
                }
            }
            
            if ([self showFavorite]==1) {
                NSPredicate *predicate0 = [NSPredicate predicateWithFormat:@"id == %@", [dictionary objectForKey:@"id"]];
                NSArray *filteredArrayLike = [listOfFav filteredArrayUsingPredicate:predicate0];
                if ([filteredArrayLike count]>0) {
                    [btnimgViewFav setImage:[UIImage imageNamed:@"favorite_red.png"] forState:UIControlStateNormal];
                } else {
                    [btnimgViewFav setImage:[UIImage imageNamed:@"favorite_grey.png"] forState:UIControlStateNormal];
                }
                [btnimgViewFav addTarget:self action:@selector(checkButtonFavTapped:event:) forControlEvents:UIControlEventTouchUpInside];
                NSPredicate *predicate = [NSPredicate predicateWithFormat:@"id == %@", [dictionary objectForKey:@"id"]];
                NSArray *filteredArray = [listOfContentsFav filteredArrayUsingPredicate:predicate];
                //NSLog(@"filteredArray %@", filteredArray);
                NSString *countFav=@"0";
                if ([filteredArray count]>0) {
                    NSDictionary* favorite = [filteredArray objectAtIndex:0];
                    //NSLog(@"%@",[favorite objectForKey:@"cntFav"]);
                    countFav = [favorite objectForKey:@"cntFav"];
                }
                lblFav.text = [NSString stringWithFormat:@"%@", countFav];
            } else {
                btnimgViewFav = nil;
                lblFav.text = nil;
            }
            
            if ([self showComments]==1) {
                [btnimgViewCom setImage:[UIImage imageNamed:@"comments.png"] forState:UIControlStateNormal];
                [btnimgViewCom addTarget:self action:@selector(checkButtonCommentsTapped:event:) forControlEvents:UIControlEventTouchUpInside];
                NSPredicate *predicate1 = [NSPredicate predicateWithFormat:@"id == %@", [dictionary objectForKey:@"id"]];
                NSArray *filteredArrayComm = [listOfContentsComments filteredArrayUsingPredicate:predicate1];
                NSString *countComm=@"0";
                if ([filteredArrayComm count]>0) {
                    NSDictionary* comments = [filteredArrayComm objectAtIndex:0];
                    //NSLog(@"%@",[favorite objectForKey:@"cntFav"]);
                    countComm = [comments objectForKey:@"cntComments"];
                }
                lblCom.text = [NSString stringWithFormat:@"%@", countComm];
                //NSLog(@"filteredArrayComm %@", filteredArrayComm);
            } else {
                btnimgViewCom = nil;
                lblCom.text = nil;
            }
            
        }
        return cell;
        imgViewMenu =nil;
        menutext =nil;
        pricetitle =nil;
        price =nil;
        var1 =nil;
        distance =nil;
        btnimgViewMenu =nil;
        datetime =nil;
        ImageBarView =nil;
        overlay =nil;
        desc =nil;
        btnimgViewCom =nil;
        lblFav =nil;
        btnimgViewFav =nil;
        lblCom =nil;
    }
    
    if (tableView == GridtableView)
    {
        if ([list_template isEqualToString:@"8"]) {
            static NSString *hlCellID = @"hlCellID";
            
            UITableViewCell *hlcell = [tableView dequeueReusableCellWithIdentifier:hlCellID];
            if(hlcell == nil)
            {
                hlcell =  [[UITableViewCell alloc]
                           initWithStyle:UITableViewCellStyleDefault reuseIdentifier:hlCellID];
                hlcell.accessoryType = UITableViewCellAccessoryNone;
                hlcell.selectionStyle = UITableViewCellSelectionStyleNone;
            }
            
            int section = indexPath.section;
            NSMutableArray *sectionItems = [sections objectAtIndex:section];
            //NSLog(@"sectionItems: %@", sectionItems);
            
            int n = [sectionItems count];
            int i=0,i1=0;
            
            //NSLog(@"cellForRowAtIndexPath");
            while(i<n)
            {
                int yy;
                if ([self showFavorite]==1 || [self showComments]==1) {
                    yy = i1*self.view.frame.size.width/7*5;
                } else {
                    yy = i1*self.view.frame.size.width/5*3;
                }
                //yy = 5 +i1*(self.view.frame.size.width/2+20);
                int j=0;
                
                for(j=0; j<2;j++)
                {
                    if (i>=n)
                        break;
                    
                    Item *item = [sectionItems objectAtIndex:i];
                    
                    UIButton *button;
                    UIImageView *imgV;
                    if ([item.image length]>0 || [item.title length]>0)
                    {
                        
                        UIView *rectView=[[UIView alloc]initWithFrame:CGRectMake(0, 140, 320, 458)];
                        [rectView setBackgroundColor:[UIColor clearColor]];
                        [hlcell.contentView addSubview:rectView];
                        
                        CGRect rect = CGRectMake(0, 0, self.view.frame.size.width/2-10, self.view.frame.size.width/5*3);
                        button=[[UIButton alloc] initWithFrame:rect];
                        [button setFrame:rect];
                        
                        UIImageView *ImageBarView;
                        ImageBarView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 10, self.view.frame.size.width/2-10, 25)];
                        
                        UIView *overlay;
                        overlay = [[UIView alloc] initWithFrame:CGRectMake(0, 0, ImageBarView.frame.size.width, ImageBarView.frame.size.height)];
                        overlay.backgroundColor = [UIColor clearColor];
                        overlay.alpha = 0.6f;
                        
                        [ImageBarView addSubview:overlay];
                        
                        UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(5, 0, self.view.frame.size.width/2-20, 15)];
                        label.numberOfLines = 0;
                        label.lineBreakMode = NSLineBreakByWordWrapping;
                        UIFont *yourFont;
                        yourFont = [UIFont boldSystemFontOfSize:14];
                        label.font = yourFont;
                        label.text = item.title;
                        label.backgroundColor = [UIColor clearColor];
                        label.textColor = [self colorWithHexString:appdelegate.themeColorCode];
                        [label sizeToFit];
                        [label setTextAlignment:NSTextAlignmentCenter];
                        [ImageBarView addSubview:label];
                        
                        CGSize constrainedSize = CGSizeMake(label.frame.size.width  , 9999);
                        
                        NSDictionary *attributesDictionary = [NSDictionary dictionaryWithObjectsAndKeys:
                                                              yourFont, NSFontAttributeName,
                                                              nil];
                        
                        NSMutableAttributedString *string = [[NSMutableAttributedString alloc] initWithString:label.text attributes:attributesDictionary];
                        
                        CGRect requiredHeight = [string boundingRectWithSize:constrainedSize options:NSStringDrawingUsesLineFragmentOrigin context:nil];
                        
                        if (requiredHeight.size.width > label.frame.size.width) {
                            requiredHeight = CGRectMake(0,0, label.frame.size.width, requiredHeight.size.height);
                        } else {
                            requiredHeight = CGRectMake(0,0, label.frame.size.width, label.frame.size.height);
                        }
                        CGRect newFrame = label.frame;
                        newFrame.size.height = requiredHeight.size.height;
                        label.frame = newFrame;
                        ImageBarView.frame = CGRectMake(0, 10, self.view.frame.size.width/2-10, newFrame.size.height);
                        
                        overlay.frame = CGRectMake(0, 0, ImageBarView.frame.size.width, ImageBarView.frame.size.height);
                        
                        [rectView addSubview:ImageBarView];
                        
                        CGRect newFrame1;
                        CGRect newFrame2;
                        newFrame1 = CGRectMake(0, 0, 0, 0);
                        newFrame2 = CGRectMake(0, 0, 0, 0);
                        
                        //NSLog(@"itempic: %@", item.pic);
                        if ([item.image length]>0)
                        {
                            [button setFrame:CGRectMake(0, newFrame.size.height+10, self.view.frame.size.width/2-10, self.view.frame.size.width/5*3-newFrame.size.height-10)];
                            imgV = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, button.frame.size.width, button.frame.size.height)];
                            //[imgV setFrame:CGRectMake(0, 0, self.view.frame.size.width/2-10, self.view.frame.size.width/2-5)];
                            if ([item.image length]>0)
                            {
                                //[button setBackgroundImageWithURL:[NSURL URLWithString:item.image] forState:UIControlStateNormal placeholderImage:[UIImage imageNamed:@"No-Image-icon.png"]];
                                imgV = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, button.frame.size.width, button.frame.size.height)];
                                
                                [button addSubview:imgV];
                                
                                __block UIActivityIndicatorView *activityIndicator;
                                __weak UIImageView *weakImageView = imgV;
                                /*[weakImageView setImageWithURL:[NSURL URLWithString:item.image] placeholderImage:[UIImage imageNamed:@"No-Image-icon.png"] options:SDWebImageProgressiveDownload progress:^(NSInteger receivedSize, NSInteger expectedSize)
                                 {
                                     if (!activityIndicator)
                                     {
                                         [weakImageView addSubview:activityIndicator = [UIActivityIndicatorView.alloc initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray]];
                                         activityIndicator.center = weakImageView.center;
                                         [activityIndicator startAnimating];
                                     }
                                 }
                                                     completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType)
                                 {
                                     [activityIndicator removeFromSuperview];
                                     activityIndicator = nil;
                                 }]; */
                                //dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0), ^ {
                                    [weakImageView sd_setImageWithURL:[NSURL URLWithString:item.image]
                                                     placeholderImage:[UIImage imageNamed:@"No-Image-icon.png"]
                                                              options:SDWebImageProgressiveDownload
                                                             progress:^(NSInteger receivedSize, NSInteger expectedSize, NSURL *targetURL) {
                                                                 dispatch_async(dispatch_get_main_queue(), ^ {
                                                                     if (!activityIndicator) {
                                                                         
                                                                         [weakImageView addSubview:activityIndicator = [UIActivityIndicatorView.alloc initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray]];
                                                                         activityIndicator.center = weakImageView.center;
                                                                         [activityIndicator startAnimating];
                                                                     }
                                                                 });
                                                             }
                                                            completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
                                                                dispatch_async(dispatch_get_main_queue(), ^ {
                                                                    //[MBProgressHUD hideHUDForView:self.view animated:YES];
                                                                    [activityIndicator removeFromSuperview];
                                                                    activityIndicator = nil;
                                                                });
                                                            }];
                                //});
                                
                                imgV.contentMode  = UIViewContentModeScaleAspectFill;
                                [imgV setClipsToBounds:YES];
                            }
                            else
                            {
                                [button setTitle:@"No Image" forState:UIControlStateNormal];
                                [button setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
                                [button setBackgroundColor:[UIColor colorWithRed:230/255.0 green:230/255.0 blue:230/255.0 alpha:1]];
                            }
                            item.image =nil;
                            
                            //[button addSubview:imgV];
                            
                            /*__block UIActivityIndicatorView *activityIndicator;
                             __weak UIImageView *weakImageView = imgV;
                             [weakImageView setImageWithURL:[NSURL URLWithString:item.pic] placeholderImage:[UIImage imageNamed:@"page.png"] options:SDWebImageProgressiveDownload progress:^(NSInteger receivedSize, NSInteger expectedSize)
                             {
                             if (!activityIndicator)
                             {
                             [weakImageView addSubview:activityIndicator = [UIActivityIndicatorView.alloc initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray]];
                             activityIndicator.center = weakImageView.center;
                             [activityIndicator startAnimating];
                             }
                             }
                             completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType)
                             {
                             [activityIndicator removeFromSuperview];
                             activityIndicator = nil;
                             }];
                             
                             imgV.contentMode  = UIViewContentModeScaleAspectFill;
                             [imgV setClipsToBounds:YES]; */
                        }
                        else
                        {
                            [button setTitle:@"No Image" forState:UIControlStateNormal];
                            [button setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
                            [button setBackgroundColor:[UIColor colorWithRed:230/255.0 green:230/255.0 blue:230/255.0 alpha:1]];
                        }
                        //item.pic =nil;
                        
                        [button setContentMode:UIViewContentModeCenter];
                        
                        NSString *tagValue = [NSString stringWithFormat:@"%d%d", indexPath.section+1, i];
                        button.tag = [tagValue intValue];
                        
                        [button addTarget:self action:@selector(buttonPressed:) forControlEvents:UIControlEventTouchUpInside];
                        [rectView addSubview:button];
                        
                        UIButton *btnimgViewFav2;
                        UIButton *btnimgViewCom2;
                        UILabel *lblFav2;
                        UILabel *lblCom2;
                        [lblFav2 removeFromSuperview];
                        [lblCom2 removeFromSuperview];
                        [btnimgViewCom2 removeFromSuperview];
                        [btnimgViewFav2 removeFromSuperview];
                        if ([self showFavorite]==1) {
                            NSPredicate *predicate0 = [NSPredicate predicateWithFormat:@"id == %@", item.contid];
                            NSArray *filteredArrayLike = [listOfFav filteredArrayUsingPredicate:predicate0];
                            btnimgViewFav2 = [UIButton buttonWithType:UIButtonTypeCustom];
                            if ([filteredArrayLike count]>0) {
                                [btnimgViewFav2 setImage:[UIImage imageNamed:@"favorite_red.png"] forState:UIControlStateNormal];
                            } else {
                                [btnimgViewFav2 setImage:[UIImage imageNamed:@"favorite_grey.png"] forState:UIControlStateNormal];
                            }
                            if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone) {
                                // For iPhone
                                [btnimgViewFav2 setFrame:CGRectMake((self.view.frame.size.width/2-10)/4-30, self.view.frame.size.width/5*3+5, 25, 25)];
                            } else {
                                [btnimgViewFav2 setFrame:CGRectMake((self.view.frame.size.width/2-10)/4-43, self.view.frame.size.width/5*3+5, 38, 38)];
                            }
                            btnimgViewFav2.tag = [tagValue intValue];
                            [btnimgViewFav2 addTarget:self action:@selector(buttonPressedFav:) forControlEvents:UIControlEventTouchUpInside];
                            NSPredicate *predicate = [NSPredicate predicateWithFormat:@"id == %@", item.contid];
                            NSArray *filteredArray = [listOfContentsFav filteredArrayUsingPredicate:predicate];
                            //NSLog(@"filteredArray %@", filteredArray);
                            NSString *countFav=@"0";
                            if ([filteredArray count]>0) {
                                NSDictionary* favorite = [filteredArray objectAtIndex:0];
                                //NSLog(@"%@",[favorite objectForKey:@"cntFav"]);
                                countFav = [favorite objectForKey:@"cntFav"];
                            }
                            if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone) {
                                // For iPhone
                                lblFav2 = [[UILabel alloc] initWithFrame:CGRectMake((self.view.frame.size.width/2-10)/4, self.view.frame.size.width/5*3+10, (self.view.frame.size.width/2-10)/4, 20)];
                            } else {
                                lblFav2 = [[UILabel alloc] initWithFrame:CGRectMake((self.view.frame.size.width/2-10)/4, self.view.frame.size.width/5*3+10, (self.view.frame.size.width/2-10)/4, 33)];
                            }
                            lblFav2.text = [NSString stringWithFormat:@"%@", countFav];
                            
                            [rectView addSubview:btnimgViewFav2];
                            [rectView addSubview:lblFav2];
                        }
                        
                        if ([self showComments]==1) {
                            btnimgViewCom2 = [UIButton buttonWithType:UIButtonTypeCustom];
                            [btnimgViewCom2 setImage:[UIImage imageNamed:@"comments.png"] forState:UIControlStateNormal];
                            if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone) {
                                // For iPhone
                                [btnimgViewCom2 setFrame:CGRectMake((self.view.frame.size.width/2-10)/4*3-30, self.view.frame.size.width/5*3+5, 25, 25)];
                            } else {
                                [btnimgViewCom2 setFrame:CGRectMake((self.view.frame.size.width/2-10)/4*3-43, self.view.frame.size.width/5*3+5, 38, 38)];
                            }
                            btnimgViewCom2.tag = [tagValue intValue];
                            [btnimgViewCom2 addTarget:self action:@selector(buttonPressedComments:) forControlEvents:UIControlEventTouchUpInside];
                            NSPredicate *predicate1 = [NSPredicate predicateWithFormat:@"id == %@", item.contid];
                            NSArray *filteredArrayComm = [listOfContentsComments filteredArrayUsingPredicate:predicate1];
                            NSString *countComm=@"0";
                            if ([filteredArrayComm count]>0) {
                                NSDictionary* comments = [filteredArrayComm objectAtIndex:0];
                                //NSLog(@"%@",[favorite objectForKey:@"cntFav"]);
                                countComm = [comments objectForKey:@"cntComments"];
                            }
                            if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone) {
                                // For iPhone
                                lblCom2 = [[UILabel alloc] initWithFrame:CGRectMake((self.view.frame.size.width/2-10)/4*3, self.view.frame.size.width/5*3+10, (self.view.frame.size.width/2-10)/4, 20)];
                            } else {
                                lblCom2 = [[UILabel alloc] initWithFrame:CGRectMake((self.view.frame.size.width/2-10)/4*3, self.view.frame.size.width/5*3+10, (self.view.frame.size.width/2-10)/4, 33)];
                            }
                            lblCom2.text = [NSString stringWithFormat:@"%@", countComm];
                            //NSLog(@"lblCom2.text %@", lblCom2.text);
                            
                            [rectView addSubview:btnimgViewCom2];
                            [rectView addSubview:lblCom2];
                            
                        }
                        
                        //rectView.frame = CGRectMake(5+self.view.frame.size.width/2*j, yy, self.view.frame.size.width/2-10, self.view.frame.size.width/4*3-5);
                        if ([self showFavorite]==1 || [self showComments]==1) {
                            rectView.frame = CGRectMake(5+self.view.frame.size.width/2*j, yy, self.view.frame.size.width/2-10, self.view.frame.size.width/7*5);
                        } else {
                            rectView.frame = CGRectMake(5+self.view.frame.size.width/2*j, yy, self.view.frame.size.width/2-10, self.view.frame.size.width/5*3);
                        }
                    }
                    
                    i++;
                }
                i1 = i1+1;
            }
                
            return hlcell;
        } else {
            static NSString *hlCellID = @"hlCellID";
            
            UITableViewCell *hlcell = [tableView dequeueReusableCellWithIdentifier:hlCellID];
            if(hlcell == nil)
            {
                hlcell =  [[UITableViewCell alloc]
                           initWithStyle:UITableViewCellStyleDefault reuseIdentifier:hlCellID];
                hlcell.accessoryType = UITableViewCellAccessoryNone;
                hlcell.selectionStyle = UITableViewCellSelectionStyleNone;
            }
            
            int section = indexPath.section;
            NSMutableArray *sectionItems = [sections objectAtIndex:section];
            
            int n = [sectionItems count];
            int i=0,i1=0;
            
            while(i<n)
            {
                int yy;
                if ([list_template isEqualToString:@"4"]) {
                    if ([self showFavorite]==1 || [self showComments]==1) {
                        yy = 5 +i1*self.view.frame.size.width/6*5;
                    } else {
                        yy = 5 +i1*self.view.frame.size.width/4*3;
                    }
                } else {
                    if ([self showFavorite]==1 || [self showComments]==1) {
                        yy = 5 +i1*self.view.frame.size.width/6*4;
                    } else {
                        yy = 5 +i1*self.view.frame.size.width/2;
                    }
                }
                int j=0;
                
                for(j=0; j<2;j++)
                {
                    if (i>=n)
                        break;
                    
                    Item *item = [sectionItems objectAtIndex:i];
                    
                    UIButton *button;
                    UIImageView *imgV;
                    if ([item.image length]>0 || [item.title length]>0)
                    {
                        
                        UIView *rectView=[[UIView alloc]initWithFrame:CGRectMake(0, 50, 320, 430)];
                        [rectView setBackgroundColor:[UIColor clearColor]];
                        rectView.layer.borderColor = [UIColor grayColor].CGColor;
                        rectView.layer.borderWidth = 0.5f;
                        [hlcell.contentView addSubview:rectView];
                        
                        CGRect rect = CGRectMake(0, 0, self.view.frame.size.width/2-10, self.view.frame.size.width/4*3);
                        button=[[UIButton alloc] initWithFrame:rect];
                        [button setFrame:rect];
                        //[[button layer] setBorderWidth:1.0f];
                        //[[button layer] setBorderColor:[UIColor grayColor].CGColor];
                        
                        if ([item.image length]>0)
                        {
                            //[button setBackgroundImageWithURL:[NSURL URLWithString:item.image] forState:UIControlStateNormal placeholderImage:[UIImage imageNamed:@"No-Image-icon.png"]];
                            imgV = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, button.frame.size.width, button.frame.size.height)];
                            
                            [button addSubview:imgV];
                            
                            __block UIActivityIndicatorView *activityIndicator;
                            __weak UIImageView *weakImageView = imgV;
                            /*[weakImageView setImageWithURL:[NSURL URLWithString:item.image] placeholderImage:[UIImage imageNamed:@"No-Image-icon.png"] options:SDWebImageProgressiveDownload progress:^(NSInteger receivedSize, NSInteger expectedSize)
                             {
                                 if (!activityIndicator)
                                 {
                                     [weakImageView addSubview:activityIndicator = [UIActivityIndicatorView.alloc initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray]];
                                     activityIndicator.center = weakImageView.center;
                                     [activityIndicator startAnimating];
                                 }
                             }
                                                 completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType)
                             {
                                 [activityIndicator removeFromSuperview];
                                 activityIndicator = nil;
                             }]; */
                            //dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0), ^ {
                                [weakImageView sd_setImageWithURL:[NSURL URLWithString:item.image]
                                                 placeholderImage:[UIImage imageNamed:@"No-Image-icon.png"]
                                                          options:SDWebImageProgressiveDownload
                                                         progress:^(NSInteger receivedSize, NSInteger expectedSize, NSURL *targetURL) {
                                                             dispatch_async(dispatch_get_main_queue(), ^ {
                                                                 if (!activityIndicator) {
                                                                     
                                                                     [weakImageView addSubview:activityIndicator = [UIActivityIndicatorView.alloc initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray]];
                                                                     activityIndicator.center = weakImageView.center;
                                                                     [activityIndicator startAnimating];
                                                                 }
                                                             });
                                                         }
                                                        completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
                                                            dispatch_async(dispatch_get_main_queue(), ^ {
                                                                //[MBProgressHUD hideHUDForView:self.view animated:YES];
                                                                [activityIndicator removeFromSuperview];
                                                                activityIndicator = nil;
                                                            });
                                                        }];
                            //});
                            
                            imgV.contentMode  = UIViewContentModeScaleAspectFill;
                            [imgV setClipsToBounds:YES];
                        }
                        else
                        {
                            [button setTitle:@"No Image" forState:UIControlStateNormal];
                            [button setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
                            [button setBackgroundColor:[UIColor colorWithRed:230/255.0 green:230/255.0 blue:230/255.0 alpha:1]];
                        }
                        item.image =nil;
                        
                        [button setContentMode:UIViewContentModeCenter];
                        
                        NSString *tagValue = [NSString stringWithFormat:@"%d%d", indexPath.section+1, i];
                        button.tag = [tagValue intValue];
                        
                        [button addTarget:self action:@selector(buttonPressed:) forControlEvents:UIControlEventTouchUpInside];
                        [rectView addSubview:button];
                        
                        UIImageView *ImageBarView;
                        if ([list_template isEqualToString:@"4"]) {
                            ImageBarView = [[UIImageView alloc] initWithFrame:CGRectMake((self.view.frame.size.width/2-10*j)+5, self.view.frame.size.width/4*3-10, self.view.frame.size.width/2-10, 25)];
                        } else {
                            ImageBarView = [[UIImageView alloc] initWithFrame:CGRectMake((self.view.frame.size.width/2-10*j)+5, self.view.frame.size.width/2-5, self.view.frame.size.width/2-10, 25)];
                        }
                        
                        UIView *overlay;
                        overlay = [[UIView alloc] initWithFrame:CGRectMake(0, 0, ImageBarView.frame.size.width, ImageBarView.frame.size.height)];
                        if ([list_template isEqualToString:@"4"]) {
                            overlay.backgroundColor = [UIColor whiteColor];
                        } else {
                            overlay.backgroundColor = [self colorWithHexString:appdelegate.themeColorCode];
                        }
                        overlay.alpha = 0.6f;
                        //overlay.opaque = YES;
                        
                        [ImageBarView addSubview:overlay];
                        
                        UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(5, 0, self.view.frame.size.width/2-20, 15)];
                        label.numberOfLines = 0;
                        label.lineBreakMode = NSLineBreakByWordWrapping;
                        UIFont *yourFont;
                        if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone) {
                            // For iPhone
                            yourFont = [UIFont fontWithName:@"Helvetica-Bold" size:14];
                        } else {
                            yourFont = [UIFont fontWithName:@"Helvetica-Bold" size:20];
                        }
                        label.font = yourFont;
                        label.text = item.title;
                        label.backgroundColor = [UIColor clearColor];
                        if ([list_template isEqualToString:@"4"]) {
                            label.textColor = [self colorWithHexString:appdelegate.themeColorCode];
                            label.shadowColor = [UIColor blackColor];
                            label.shadowOffset = CGSizeMake(0, 1);
                        } else {
                            label.textColor = [UIColor whiteColor];
                        }
                        [label sizeToFit];
                        [ImageBarView addSubview:label];
                        
                        CGSize constrainedSize = CGSizeMake(label.frame.size.width  , 9999);
                        
                        NSDictionary *attributesDictionary = [NSDictionary dictionaryWithObjectsAndKeys:
                                                              yourFont, NSFontAttributeName,
                                                              nil];
                        
                        NSMutableAttributedString *string = [[NSMutableAttributedString alloc] initWithString:label.text attributes:attributesDictionary];
                        
                        CGRect requiredHeight = [string boundingRectWithSize:constrainedSize options:NSStringDrawingUsesLineFragmentOrigin context:nil];
                        
                        if (requiredHeight.size.width > label.frame.size.width) {
                            requiredHeight = CGRectMake(0,0, label.frame.size.width, requiredHeight.size.height);
                        }
                        CGRect newFrame = label.frame;
                        newFrame.size.height = requiredHeight.size.height;
                        label.frame = newFrame;
                        if ([list_template isEqualToString:@"4"]) {
                            ImageBarView.frame = CGRectMake(0, self.view.frame.size.width/4*3-newFrame.size.height-10, self.view.frame.size.width/2-10, newFrame.size.height);
                        } else {
                            ImageBarView.frame = CGRectMake(0, self.view.frame.size.width/2-newFrame.size.height-5, self.view.frame.size.width/2-10, newFrame.size.height);
                        }
                        overlay.frame = CGRectMake(0, 0, ImageBarView.frame.size.width, ImageBarView.frame.size.height);
                        
                        CGRect newFrame1;
                        CGRect newFrame2;
                        newFrame1 = CGRectMake(0, 0, 0, 0);
                        newFrame2 = CGRectMake(0, 0, 0, 0);
                        if ([list_template isEqualToString:@"4"]) {
                            if ([item.price floatValue]>0) {
                                UILabel *label1 = [[UILabel alloc] initWithFrame:CGRectMake(5, newFrame.size.height, self.view.frame.size.width/2-20, 15)];
                                label1.numberOfLines = 0;
                                label1.lineBreakMode = NSLineBreakByWordWrapping;
                                UIFont *yourFont1;
                                if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone) {
                                    // For iPhone
                                    yourFont1 = [UIFont fontWithName:@"Helvetica" size:12];
                                } else {
                                    yourFont1 = [UIFont fontWithName:@"Helvetica" size:18];
                                }
                                label1.font = yourFont1;
                                label1.text = [NSString stringWithFormat:@"%@ %.2f", item.pricetitle, [item.price floatValue]];
                                label1.backgroundColor = [UIColor clearColor];
                                label1.textColor = [UIColor blackColor];
                                [label1 sizeToFit];
                                //label1.shadowColor = [UIColor blackColor];
                                //label1.shadowOffset = CGSizeMake(0, 1);
                                [ImageBarView addSubview:label1];
                                
                                CGSize constrainedSize1 = CGSizeMake(label1.frame.size.width  , 9999);
                                
                                NSDictionary *attributesDictionary1 = [NSDictionary dictionaryWithObjectsAndKeys:
                                                                       yourFont1, NSFontAttributeName,
                                                                       nil];
                                
                                NSMutableAttributedString *string1 = [[NSMutableAttributedString alloc] initWithString:label1.text attributes:attributesDictionary1];
                                
                                CGRect requiredHeight1 = [string1 boundingRectWithSize:constrainedSize1 options:NSStringDrawingUsesLineFragmentOrigin context:nil];
                                
                                if (requiredHeight1.size.width > label1.frame.size.width) {
                                    requiredHeight1 = CGRectMake(0,0, label1.frame.size.width, requiredHeight1.size.height);
                                }
                                newFrame1 = label1.frame;
                                newFrame1.size.height = requiredHeight1.size.height;
                                label1.frame = newFrame1;
                                //if ([list_template isEqualToString:@"4"]) {
                                ImageBarView.frame = CGRectMake(0, self.view.frame.size.width/4*3-newFrame.size.height-newFrame1.size.height-10, self.view.frame.size.width/2-10, newFrame.size.height+newFrame1.size.height);
                                //} else {
                                //ImageBarView.frame = CGRectMake(0, self.view.frame.size.width/2-newFrame.size.height-5, self.view.frame.size.width/2-10, newFrame.size.height);
                                //}
                                overlay.frame = CGRectMake(0, 0, ImageBarView.frame.size.width, ImageBarView.frame.size.height);
                            }
                            
                            if ([item.var1 length]>0) {
                                UILabel *label2 = [[UILabel alloc] initWithFrame:CGRectMake(5, newFrame.size.height+newFrame1.size.height, self.view.frame.size.width/2-20, 15)];
                                label2.numberOfLines = 0;
                                label2.lineBreakMode = NSLineBreakByWordWrapping;
                                UIFont *yourFont2;
                                if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone) {
                                    // For iPhone
                                    yourFont2 = [UIFont fontWithName:@"Helvetica" size:12];
                                } else {
                                    yourFont2 = [UIFont fontWithName:@"Helvetica" size:18];
                                }
                                label2.font = yourFont2;
                                label2.text = item.var1;
                                label2.backgroundColor = [UIColor clearColor];
                                label2.textColor = [UIColor blackColor];
                                [label2 sizeToFit];
                                //label2.shadowColor = [UIColor blackColor];
                                //label2.shadowOffset = CGSizeMake(0, 1);
                                [ImageBarView addSubview:label2];
                                
                                CGSize constrainedSize2 = CGSizeMake(label2.frame.size.width  , 9999);
                                
                                NSDictionary *attributesDictionary2 = [NSDictionary dictionaryWithObjectsAndKeys:
                                                                       yourFont2, NSFontAttributeName,
                                                                       nil];
                                
                                NSMutableAttributedString *string2 = [[NSMutableAttributedString alloc] initWithString:label2.text attributes:attributesDictionary2];
                                
                                CGRect requiredHeight2 = [string2 boundingRectWithSize:constrainedSize2 options:NSStringDrawingUsesLineFragmentOrigin context:nil];
                                
                                if (requiredHeight2.size.width > label2.frame.size.width) {
                                    requiredHeight2 = CGRectMake(0,0, label2.frame.size.width, requiredHeight2.size.height);
                                }
                                newFrame2 = label2.frame;
                                newFrame2.size.height = requiredHeight2.size.height;
                                label2.frame = newFrame2;
                                //if ([list_template isEqualToString:@"4"]) {
                                ImageBarView.frame = CGRectMake(0, self.view.frame.size.width/4*3-newFrame.size.height-newFrame1.size.height-newFrame2.size.height-10, self.view.frame.size.width/2-10, newFrame.size.height+newFrame1.size.height+newFrame2.size.height);
                                //} else {
                                //ImageBarView.frame = CGRectMake(0, self.view.frame.size.width/2-newFrame.size.height-5, self.view.frame.size.width/2-10, newFrame.size.height);
                                //}
                                overlay.frame = CGRectMake(0, 0, ImageBarView.frame.size.width, ImageBarView.frame.size.height);
                            }
                            
                            [imgV setFrame:CGRectMake(0, 0, self.view.frame.size.width/2-10, self.view.frame.size.width/4*3-newFrame.size.height-newFrame1.size.height-newFrame2.size.height-10)];
                            [button setFrame:CGRectMake(0, 0, self.view.frame.size.width/2-10, self.view.frame.size.width/4*3-newFrame.size.height-newFrame1.size.height-newFrame2.size.height-10)];
                            
                            UIButton *btnimgViewFav2;
                            UIButton *btnimgViewCom2;
                            UILabel *lblFav2;
                            UILabel *lblCom2;
                            [lblFav2 removeFromSuperview];
                            [lblCom2 removeFromSuperview];
                            [btnimgViewCom2 removeFromSuperview];
                            [btnimgViewFav2 removeFromSuperview];
                            if ([self showFavorite]==1) {
                                NSPredicate *predicate0 = [NSPredicate predicateWithFormat:@"id == %@", item.contid];
                                NSArray *filteredArrayLike = [listOfFav filteredArrayUsingPredicate:predicate0];
                                btnimgViewFav2 = [UIButton buttonWithType:UIButtonTypeCustom];
                                if ([filteredArrayLike count]>0) {
                                    [btnimgViewFav2 setImage:[UIImage imageNamed:@"favorite_red.png"] forState:UIControlStateNormal];
                                } else {
                                    [btnimgViewFav2 setImage:[UIImage imageNamed:@"favorite_grey.png"] forState:UIControlStateNormal];
                                }
                                if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone) {
                                    // For iPhone
                                    [btnimgViewFav2 setFrame:CGRectMake((self.view.frame.size.width/2-10)/4-30, self.view.frame.size.width/4*3-5, 25, 25)];
                                } else {
                                    [btnimgViewFav2 setFrame:CGRectMake((self.view.frame.size.width/2-10)/4-43, self.view.frame.size.width/4*3-5, 38, 38)];
                                }
                                btnimgViewFav2.tag = [tagValue intValue];
                                [btnimgViewFav2 addTarget:self action:@selector(buttonPressedFav:) forControlEvents:UIControlEventTouchUpInside];
                                NSPredicate *predicate = [NSPredicate predicateWithFormat:@"id == %@", item.contid];
                                NSArray *filteredArray = [listOfContentsFav filteredArrayUsingPredicate:predicate];
                                //NSLog(@"filteredArray %@", filteredArray);
                                NSString *countFav=@"0";
                                if ([filteredArray count]>0) {
                                    NSDictionary* favorite = [filteredArray objectAtIndex:0];
                                    //NSLog(@"%@",[favorite objectForKey:@"cntFav"]);
                                    countFav = [favorite objectForKey:@"cntFav"];
                                }
                                if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone) {
                                    // For iPhone
                                    lblFav2 = [[UILabel alloc] initWithFrame:CGRectMake((self.view.frame.size.width/2-10)/4, self.view.frame.size.width/4*3, (self.view.frame.size.width/2-10)/4, 20)];
                                } else {
                                    lblFav2 = [[UILabel alloc] initWithFrame:CGRectMake((self.view.frame.size.width/2-10)/4, self.view.frame.size.width/4*3,(self.view.frame.size.width/2-10)/4, 33)];
                                }
                                lblFav2.text = [NSString stringWithFormat:@"%@", countFav];
                                
                                [rectView addSubview:btnimgViewFav2];
                                [rectView addSubview:lblFav2];
                            }
                            
                            if ([self showComments]==1) {
                                btnimgViewCom2 = [UIButton buttonWithType:UIButtonTypeCustom];
                                [btnimgViewCom2 setImage:[UIImage imageNamed:@"comments.png"] forState:UIControlStateNormal];
                                if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone) {
                                    // For iPhone
                                    [btnimgViewCom2 setFrame:CGRectMake((self.view.frame.size.width/2-10)/4*3-30, self.view.frame.size.width/4*3-5, 25, 25)];
                                } else {
                                    [btnimgViewCom2 setFrame:CGRectMake((self.view.frame.size.width/2-10)/4*3-43, self.view.frame.size.width/4*3-5, 38, 38)];
                                }
                                btnimgViewCom2.tag = [tagValue intValue];
                                [btnimgViewCom2 addTarget:self action:@selector(buttonPressedComments:) forControlEvents:UIControlEventTouchUpInside];
                                NSPredicate *predicate1 = [NSPredicate predicateWithFormat:@"id == %@", item.contid];
                                NSArray *filteredArrayComm = [listOfContentsComments filteredArrayUsingPredicate:predicate1];
                                NSString *countComm=@"0";
                                if ([filteredArrayComm count]>0) {
                                    NSDictionary* comments = [filteredArrayComm objectAtIndex:0];
                                    //NSLog(@"%@",[favorite objectForKey:@"cntFav"]);
                                    countComm = [comments objectForKey:@"cntComments"];
                                }
                                if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone) {
                                    // For iPhone
                                    lblCom2 = [[UILabel alloc] initWithFrame:CGRectMake((self.view.frame.size.width/2-10)/4*3, self.view.frame.size.width/4*3, (self.view.frame.size.width/2-10)/4, 20)];
                                } else {
                                    lblCom2 = [[UILabel alloc] initWithFrame:CGRectMake((self.view.frame.size.width/2-10)/4*3, self.view.frame.size.width/4*3, (self.view.frame.size.width/2-10)/4, 33)];
                                }
                                lblCom2.text = [NSString stringWithFormat:@"%@", countComm];
                                //NSLog(@"lblCom2.text %@", lblCom2.text);
                                
                                [rectView addSubview:btnimgViewCom2];
                                [rectView addSubview:lblCom2];
                                
                            }
                        } else {
                            [imgV setFrame:CGRectMake(0, 0, self.view.frame.size.width/2-10, self.view.frame.size.width/2-5)];
                            [button setFrame:CGRectMake(0, 0, self.view.frame.size.width/2-10, self.view.frame.size.width/2-5)];
                            UIButton *btnimgViewFav2;
                            UIButton *btnimgViewCom2;
                            UILabel *lblFav2;
                            UILabel *lblCom2;
                            [lblFav2 removeFromSuperview];
                            [lblCom2 removeFromSuperview];
                            [btnimgViewCom2 removeFromSuperview];
                            [btnimgViewFav2 removeFromSuperview];
                            if ([self showFavorite]==1) {
                                NSPredicate *predicate0 = [NSPredicate predicateWithFormat:@"id == %@", item.contid];
                                NSArray *filteredArrayLike = [listOfFav filteredArrayUsingPredicate:predicate0];
                                btnimgViewFav2 = [UIButton buttonWithType:UIButtonTypeCustom];
                                if ([filteredArrayLike count]>0) {
                                    [btnimgViewFav2 setImage:[UIImage imageNamed:@"favorite_red.png"] forState:UIControlStateNormal];
                                } else {
                                    [btnimgViewFav2 setImage:[UIImage imageNamed:@"favorite_grey.png"] forState:UIControlStateNormal];
                                }
                                if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone) {
                                    // For iPhone
                                    [btnimgViewFav2 setFrame:CGRectMake((self.view.frame.size.width/2-10)/4-30, self.view.frame.size.width/2+10, 25, 25)];
                                } else {
                                    [btnimgViewFav2 setFrame:CGRectMake((self.view.frame.size.width/2-10)/4-43, self.view.frame.size.width/2+10, 38, 38)];
                                }
                                btnimgViewFav2.tag = [tagValue intValue];
                                [btnimgViewFav2 addTarget:self action:@selector(buttonPressedFav:) forControlEvents:UIControlEventTouchUpInside];
                                NSPredicate *predicate = [NSPredicate predicateWithFormat:@"id == %@", item.contid];
                                NSArray *filteredArray = [listOfContentsFav filteredArrayUsingPredicate:predicate];
                                //NSLog(@"filteredArray %@", filteredArray);
                                NSString *countFav=@"0";
                                if ([filteredArray count]>0) {
                                    NSDictionary* favorite = [filteredArray objectAtIndex:0];
                                    //NSLog(@"%@",[favorite objectForKey:@"cntFav"]);
                                    countFav = [favorite objectForKey:@"cntFav"];
                                }
                                if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone) {
                                    // For iPhone
                                    lblFav2 = [[UILabel alloc] initWithFrame:CGRectMake((self.view.frame.size.width/2-10)/4, self.view.frame.size.width/2+10, (self.view.frame.size.width/2-10)/4, 20)];
                                } else {
                                    lblFav2 = [[UILabel alloc] initWithFrame:CGRectMake((self.view.frame.size.width/2-10)/4, self.view.frame.size.width/2+10, (self.view.frame.size.width/2-10)/4, 33)];
                                }
                                lblFav2.text = [NSString stringWithFormat:@"%@", countFav];
                                
                                [rectView addSubview:btnimgViewFav2];
                                [rectView addSubview:lblFav2];
                            }
                            
                            if ([self showComments]==1) {
                                btnimgViewCom2 = [UIButton buttonWithType:UIButtonTypeCustom];
                                [btnimgViewCom2 setImage:[UIImage imageNamed:@"comments.png"] forState:UIControlStateNormal];
                                if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone) {
                                    // For iPhone
                                    [btnimgViewCom2 setFrame:CGRectMake((self.view.frame.size.width/2-10)/4*3-30, self.view.frame.size.width/2+10, 25, 25)];
                                } else {
                                    [btnimgViewCom2 setFrame:CGRectMake((self.view.frame.size.width/2-10)/4*3-43, self.view.frame.size.width/2+10, 38, 38)];
                                }
                                btnimgViewCom2.tag = [tagValue intValue];
                                [btnimgViewCom2 addTarget:self action:@selector(buttonPressedComments:) forControlEvents:UIControlEventTouchUpInside];
                                NSPredicate *predicate1 = [NSPredicate predicateWithFormat:@"id == %@", item.contid];
                                NSArray *filteredArrayComm = [listOfContentsComments filteredArrayUsingPredicate:predicate1];
                                NSString *countComm=@"0";
                                if ([filteredArrayComm count]>0) {
                                    NSDictionary* comments = [filteredArrayComm objectAtIndex:0];
                                    //NSLog(@"%@",[favorite objectForKey:@"cntFav"]);
                                    countComm = [comments objectForKey:@"cntComments"];
                                }
                                if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone) {
                                    // For iPhone
                                    lblCom2 = [[UILabel alloc] initWithFrame:CGRectMake((self.view.frame.size.width/2-10)/4*3, self.view.frame.size.width/2+10, (self.view.frame.size.width/2-10)/4, 20)];
                                } else {
                                    lblCom2 = [[UILabel alloc] initWithFrame:CGRectMake((self.view.frame.size.width/2-10)/4*3, self.view.frame.size.width/2+10, (self.view.frame.size.width/2-10)/4, 33)];
                                }
                                lblCom2.text = [NSString stringWithFormat:@"%@", countComm];
                                //NSLog(@"lblCom2.text %@", lblCom2.text);
                                
                                [rectView addSubview:btnimgViewCom2];
                                [rectView addSubview:lblCom2];
                                
                            }
                        }
                        
                        [rectView addSubview:ImageBarView];
                        
                        if ([list_template isEqualToString:@"4"]) {
                            if ([self showFavorite]==1 || [self showComments]==1) {
                                rectView.frame = CGRectMake(5+self.view.frame.size.width/2*j, yy, self.view.frame.size.width/2-10, self.view.frame.size.width/6*5-5);
                            } else {
                                rectView.frame = CGRectMake(5+self.view.frame.size.width/2*j, yy, self.view.frame.size.width/2-10, self.view.frame.size.width/4*3-5);
                            }
                        } else {
                            if ([self showFavorite]==1 || [self showComments]==1) {
                                rectView.frame = CGRectMake(5+self.view.frame.size.width/2*j, yy, self.view.frame.size.width/2-10, self.view.frame.size.width/6*4-5);
                            } else {
                                rectView.frame = CGRectMake(5+self.view.frame.size.width/2*j, yy, self.view.frame.size.width/2-10, self.view.frame.size.width/2-5);
                            }
                        }
                    }
                    
                    i++;
                }
                i1 = i1+1;
            }
            return hlcell;
        }
    }

    return nil;
}

- (void)checkButtonTapped:(id)sender event:(id)event
{
    
    NSSet *touches = [event allTouches];
    UITouch *touch = [touches anyObject];
    CGPoint currentTouchPosition = [touch locationInView:MenutableView];
    NSIndexPath *indexPath = [MenutableView indexPathForRowAtPoint: currentTouchPosition];
    if (indexPath != nil)
    {
        [self getContentdetailsAtIndex:indexPath.row];
        
    }
}

- (void)checkButtonFavTapped:(id)sender event:(id)event
{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSString *hasLoggedInFB = [defaults objectForKey:@"hasLoggedInFB"];
    NSString *hasLoggedInMyApp = [defaults objectForKey:@"hasLoggedInMyApp"];
    
    if ([hasLoggedInFB isEqualToString:@"YES"] || [hasLoggedInMyApp isEqualToString:@"YES"])
    {
        NSSet *touches = [event allTouches];
        UITouch *touch = [touches anyObject];
        CGPoint currentTouchPosition = [touch locationInView:MenutableView];
        NSIndexPath *indexPath = [MenutableView indexPathForRowAtPoint: currentTouchPosition];
        if (indexPath != nil)
        {
            if([listOfContents count]>0)
            {
                NSDictionary *dictionary = [listOfContents objectAtIndex:indexPath.row];
                
                NSString *cid = [dictionary objectForKey:@"id"];
                NSString *urlAddress2 = [[NSString alloc] initWithFormat:@"%@favorite_process.php?db=%@&userid=%@&udid=%@&cuid=%@&top=%@&cid=%@", appdelegate.user_apipath, [self urlEncode:appdelegate.db], [self urlEncode:appdelegate.user], [self urlEncode:appdelegate.uuid], [self urlEncode:appdelegate.customer_id], appdelegate.plugin_id, cid];
                currentRequestMenu = @"AddFavorite";
                [self connectToServer:urlAddress2];
            }
            
        }
    }
    else {
        [self.view addSubview:view_popoverMain];
        [view_popoverMain addSubview:view_plain];
        [view_plain addSubview:buttonclose];
        [view_plain addSubview:buttonlogin];
        [view_plain addSubview:lbllogin];
        //[view_popoverMain setHidden:NO];
        //[self.view bringSubviewToFront:view_popoverMain];
    }
}

- (void)checkButtonCommentsTapped:(id)sender event:(id)event
{
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSString *hasLoggedInFB = [defaults objectForKey:@"hasLoggedInFB"];
    NSString *hasLoggedInMyApp = [defaults objectForKey:@"hasLoggedInMyApp"];
    
    if ([hasLoggedInFB isEqualToString:@"YES"] || [hasLoggedInMyApp isEqualToString:@"YES"])
    {
        NSSet *touches = [event allTouches];
        UITouch *touch = [touches anyObject];
        CGPoint currentTouchPosition = [touch locationInView:MenutableView];
        NSIndexPath *indexPath = [MenutableView indexPathForRowAtPoint: currentTouchPosition];
        if (indexPath != nil)
        {
            if([listOfContents count]>0)
            {
                NSDictionary *dictionary = [listOfContents objectAtIndex:indexPath.row];
                
                NSString *cid = [dictionary objectForKey:@"id"];
                CommentsViewController *commentsViewController = [[CommentsViewController alloc] init];
                commentsViewController.conttitle = [dictionary objectForKey:@"title"];
                commentsViewController.contId = cid;
                commentsViewController.topId = appdelegate.plugin_id;
                [appdelegate.navController pushViewController:commentsViewController animated:NO];
            }
            
        }
    }
    else {
        [self.view addSubview:view_popoverMain];
        [view_popoverMain addSubview:view_plain];
        [view_plain addSubview:buttonclose];
        [view_plain addSubview:buttonlogin];
        [view_plain addSubview:lbllogin];
        //[view_popoverMain setHidden:NO];
        //[self.view bringSubviewToFront:view_popoverMain];
    }
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
	if (tableView == MenutableView)
    {
        //NSLog(@"appdelegate.plugin_id %@", themepluginid);
       if ([themepluginid isEqualToString:@"45"]) {
        
           NSDictionary *dictionary = [listOfContents objectAtIndex:indexPath.row];
           
           NSString *contId = [dictionary objectForKey:@"id"];
           NSString *contvar1 = [dictionary objectForKey:@"var1"];
           NSString *menutitle = [dictionary objectForKey:@"title"];
           NSString *linkurl = [dictionary objectForKey:@"linkurl"];
           
               ContentPluginDetailsViewController1 *contentPluginDetailsViewController1 = [[ContentPluginDetailsViewController1 alloc] init];
               contentPluginDetailsViewController1.cont_value1 = linkurl;
               contentPluginDetailsViewController1.cont_value2 = @"";
               contentPluginDetailsViewController1.cont_type = @"Link";
               contentPluginDetailsViewController1.cont_title = menutitle;
               contentPluginDetailsViewController1.contId = contId;
               contentPluginDetailsViewController1.listtemplate = list_template;
               contentPluginDetailsViewController1.cont_var1 = contvar1;
               contentPluginDetailsViewController1.menu_title = menutitle;
               [appdelegate.navController pushViewController:contentPluginDetailsViewController1 animated:NO];
           
       } else {
           if ([list_template isEqualToString:@"6"]) {
               NSDictionary *dictionary = [listOfContents objectAtIndex:indexPath.row];
               
               NSString *latitude = [dictionary objectForKey:@"latitude"];
               NSString *longitude = [dictionary objectForKey:@"longitude"];
               NSString *map_title = [dictionary objectForKey:@"map_title"];
               NSString *contId = [dictionary objectForKey:@"id"];
               NSString *contvar1 = [dictionary objectForKey:@"var1"];
               NSString *menutitle = [dictionary objectForKey:@"title"];
               
               if ([latitude floatValue]>0 && [longitude floatValue]>0) {
                   appdelegate.latitude = latitude;
                   appdelegate.longitude = longitude;
                   ContentPluginDetailsViewController1 *contentPluginDetailsViewController1 = [[ContentPluginDetailsViewController1 alloc] init];
                   contentPluginDetailsViewController1.cont_value1 = latitude;
                   contentPluginDetailsViewController1.cont_value2 = longitude;
                   contentPluginDetailsViewController1.cont_type = @"Map";
                   contentPluginDetailsViewController1.cont_title = map_title;
                   contentPluginDetailsViewController1.contId = contId;
                   contentPluginDetailsViewController1.listtemplate = list_template;
                   contentPluginDetailsViewController1.cont_var1 = contvar1;
                   contentPluginDetailsViewController1.menu_title = menutitle;
                   [appdelegate.navController pushViewController:contentPluginDetailsViewController1 animated:NO];
                   
               } else {
                   
                   [self getContentdetailsAtIndex:indexPath.row];
                   
               }
               
           } else {
               
               [self getContentdetailsAtIndex:indexPath.row];
               
           }
       }
        
    }
    
}

- (void) getContentdetailsAtIndex:(NSUInteger)idx {
    NSDictionary *dictionary = [listOfContents objectAtIndex:idx];
    
    cont_id = [dictionary objectForKey:@"id"];
    cont_title = [dictionary objectForKey:@"title"];
    link_url = [dictionary objectForKey:@"linkurl"];
    
    if (appdelegate.showloading) {
        alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"loading", nil) message:nil delegate:self cancelButtonTitle:nil otherButtonTitles: nil];
        
        //a simple activity indicator:
        UIActivityIndicatorView *activityIndicator = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
        activityIndicator.frame= CGRectMake(50, 10, 37, 37);
        activityIndicator.autoresizingMask = UIViewAutoresizingFlexibleTopMargin | UIViewAutoresizingFlexibleRightMargin | UIViewAutoresizingFlexibleBottomMargin | UIViewAutoresizingFlexibleLeftMargin;
        [activityIndicator startAnimating];
        
        //the magic line below,
        //we associate the activity indicator to the alert view: (addSubview is not used)
        [alert setValue:activityIndicator forKey:@"accessoryView"];
        
        [alert show];
        
        // Adjust the indicator so it is up a few pixels from the bottom of the alert
        [UIView beginAnimations:nil context:nil];
        [UIView setAnimationDuration:0.0];
        [UIView setAnimationDelay:0.0];
        [UIView setAnimationTransition:UIViewAnimationTransitionNone forView:alert cache:YES];
        [UIView setAnimationDelegate:self];
        [UIView setAnimationDidStopSelector:@selector(startupAnimationDoneIndicator:finished:context:)];
        alert.alpha = 0.5;
        [UIView commitAnimations];
    } else {
        [self goNextPage];
    }
}

-(IBAction)buttonPressed:(id)sender {
    
    int tagId = [sender tag];
    int divNum = 0;
    if(tagId<100)
        divNum=10;
    else
        divNum=100;
    int section = [sender tag]/divNum;
    section -=1; // we had incremented at tag assigning time
    int itemId = [sender tag]%divNum;
    
    NSMutableArray *sectionItems = [sections objectAtIndex:section];
    Item *item = [sectionItems objectAtIndex:itemId];
    
    cont_id = item.contid;
    cont_title = item.contname;
    link_url = item.linkurl;
    
    if (appdelegate.showloading) {
        //NSLog(@"cont_id:: %@", cont_id);
        alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"loading", nil) message:nil delegate:self cancelButtonTitle:nil otherButtonTitles: nil];
        
        //a simple activity indicator:
        UIActivityIndicatorView *activityIndicator = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
        activityIndicator.frame= CGRectMake(50, 10, 37, 37);
        activityIndicator.autoresizingMask = UIViewAutoresizingFlexibleTopMargin | UIViewAutoresizingFlexibleRightMargin | UIViewAutoresizingFlexibleBottomMargin | UIViewAutoresizingFlexibleLeftMargin;
        [activityIndicator startAnimating];
        
        //the magic line below,
        //we associate the activity indicator to the alert view: (addSubview is not used)
        [alert setValue:activityIndicator forKey:@"accessoryView"];
        
        [alert show];
    
        // Adjust the indicator so it is up a few pixels from the bottom of the alert
        [UIView beginAnimations:nil context:nil];
        [UIView setAnimationDuration:0.0];
        [UIView setAnimationDelay:0.0];
        [UIView setAnimationTransition:UIViewAnimationTransitionNone forView:alert cache:YES];
        [UIView setAnimationDelegate:self];
        [UIView setAnimationDidStopSelector:@selector(startupAnimationDoneIndicator:finished:context:)];
        alert.alpha = 0.5;
        [UIView commitAnimations];
        
    } else {
        [self goNextPage];
    }
    
}

-(IBAction)buttonPressedFav:(id)sender {
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSString *hasLoggedInFB = [defaults objectForKey:@"hasLoggedInFB"];
    NSString *hasLoggedInMyApp = [defaults objectForKey:@"hasLoggedInMyApp"];
    
    if ([hasLoggedInFB isEqualToString:@"YES"] || [hasLoggedInMyApp isEqualToString:@"YES"])
    {
        int tagId = [sender tag];
        int divNum = 0;
        if(tagId<100)
            divNum=10;
        else
            divNum=100;
        int section = [sender tag]/divNum;
        section -=1; // we had incremented at tag assigning time
        int itemId = [sender tag]%divNum;
        
        NSMutableArray *sectionItems = [sections objectAtIndex:section];
        Item *item = [sectionItems objectAtIndex:itemId];
        
        cont_id = item.contid;
        cont_title = item.contname;
        link_url = item.linkurl;
        
        NSString *cid = cont_id;
        NSString *urlAddress2 = [[NSString alloc] initWithFormat:@"%@favorite_process.php?db=%@&userid=%@&udid=%@&cuid=%@&top=%@&cid=%@", appdelegate.user_apipath, [self urlEncode:appdelegate.db], [self urlEncode:appdelegate.user], [self urlEncode:appdelegate.uuid], [self urlEncode:appdelegate.customer_id], appdelegate.plugin_id, cid];
        currentRequestMenu = @"AddFavorite";
        [self connectToServer:urlAddress2];
    }
    else {
        [self.view addSubview:view_popoverMain];
        [view_popoverMain addSubview:view_plain];
        [view_plain addSubview:buttonclose];
        [view_plain addSubview:buttonlogin];
        [view_plain addSubview:lbllogin];
        //[view_popoverMain setHidden:NO];
        //[self.view bringSubviewToFront:view_popoverMain];
    }
    
}

-(IBAction)buttonPressedComments:(id)sender {
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSString *hasLoggedInFB = [defaults objectForKey:@"hasLoggedInFB"];
    NSString *hasLoggedInMyApp = [defaults objectForKey:@"hasLoggedInMyApp"];
    
    if ([hasLoggedInFB isEqualToString:@"YES"] || [hasLoggedInMyApp isEqualToString:@"YES"])
    {
        int tagId = [sender tag];
        int divNum = 0;
        if(tagId<100)
            divNum=10;
        else
            divNum=100;
        int section = [sender tag]/divNum;
        section -=1; // we had incremented at tag assigning time
        int itemId = [sender tag]%divNum;
        
        NSMutableArray *sectionItems = [sections objectAtIndex:section];
        Item *item = [sectionItems objectAtIndex:itemId];
        
        cont_id = item.contid;
        cont_title = item.contname;
        link_url = item.linkurl;
        
        NSString *cid = cont_id;
        CommentsViewController *commentsViewController = [[CommentsViewController alloc] init];
        commentsViewController.conttitle = cont_title;
        commentsViewController.contId = cid;
        commentsViewController.topId = appdelegate.plugin_id;
        [appdelegate.navController pushViewController:commentsViewController animated:NO];
    }
    else {
        [self.view addSubview:view_popoverMain];
        [view_popoverMain addSubview:view_plain];
        [view_plain addSubview:buttonclose];
        [view_plain addSubview:buttonlogin];
        [view_plain addSubview:lbllogin];
        //[view_popoverMain setHidden:NO];
        //[self.view bringSubviewToFront:view_popoverMain];
    }
    
}

-(void) dissmissIndicator {
    [alert dismissWithClickedButtonIndex:0 animated:NO];
}

-(void) goNextPage {
    if ([themepluginid isEqualToString:@"45"]) {
        
            ContentPluginDetailsViewController1 *contentPluginDetailsViewController1 = [[ContentPluginDetailsViewController1 alloc] init];
            contentPluginDetailsViewController1.cont_value1 = link_url;
            contentPluginDetailsViewController1.cont_value2 = @"";
            contentPluginDetailsViewController1.cont_type = @"Link";
            contentPluginDetailsViewController1.cont_title = cont_title;
            contentPluginDetailsViewController1.contId = cont_id;
            contentPluginDetailsViewController1.listtemplate = list_template;
            contentPluginDetailsViewController1.cont_var1 = @"";
            contentPluginDetailsViewController1.menu_title = @"";
            [appdelegate.navController pushViewController:contentPluginDetailsViewController1 animated:NO];
    } else {
        ContentPluginDetailsViewController *contentPluginDetailsViewController = [[ContentPluginDetailsViewController alloc] init];
        contentPluginDetailsViewController.contId = cont_id;
        contentPluginDetailsViewController.conttitle = catname;
        contentPluginDetailsViewController.listtemplate = list_template;
        contentPluginDetailsViewController.listOfContentsFav = listOfContentsFav;
        contentPluginDetailsViewController.listOfContentsComments = listOfContentsComments;
        contentPluginDetailsViewController.listOfFav = listOfFav;
        [appdelegate.navController pushViewController:contentPluginDetailsViewController animated:NO];
    }
}

- (void)startupAnimationDoneIndicator:(NSString *)animationID finished:(NSNumber *)finished context:(void *)context
{
    [self goNextPage];
    [self dissmissIndicator];
    alert =nil;
}

-(BOOL) textFieldShouldReturn:(UITextField *)textField{
    [textField resignFirstResponder];
    return YES;
}

/*** Push Notification 27Aug2015 ***/
-(void) viewDidDisappear:(BOOL)animated {
    
    appdelegate.notify_actiontype = @"";
    appdelegate.notify_actionvalue = @"";
    appdelegate.notify_contentid = @"";
    
    appdelegate.homeclicked = NO;
    [modalController dismissViewControllerAnimated:YES completion:nil];
    
    [super viewDidDisappear:animated];
    
    [GridtableView removeFromSuperview];
    GridtableView = nil;
    [MenutableView removeFromSuperview];
    MenutableView = nil;
    [scrollview removeFromSuperview];
    scrollview = nil;
    
}
/*** Push Notification 27Aug2015 ***/

-(NSString *) urlEncode:(NSString *) theStr {
    /*NSString *encoded = (NSString *)CFURLCreateStringByAddingPercentEscapes(
     NULL,
     (CFStringRef)theStr,
     NULL,
     (CFStringRef)@"!*'\"();:@&=+$,/?%#[]% ",
     //																		NSUnicodeStringEncoding );
     kCFStringEncodingUTF8 ); */
    NSString *encoded = (NSString *)CFBridgingRelease(CFURLCreateStringByAddingPercentEscapes(
                                                                                              NULL,
                                                                                              (__bridge CFStringRef) theStr,
                                                                                              NULL,
                                                                                              CFSTR("!*'();:@&=+$,/?%#[]\" "),
                                                                                              kCFStringEncodingUTF8));
    return encoded;
    //return nil;
}

#pragma mark CONNECT TO SERVER
-(void) connectToServer:(NSString *) urlPath {
    
    //[self addLoadingView];
    NSString *urlAddress =[NSString stringWithFormat:@"%@", urlPath];
    
    //NSLog(@"URL: %@", urlAddress);
    //Create a URL object.
    /*NSURL *url = [NSURL URLWithString:urlAddress];
    
    //URL Requst Object
    //NSURLRequest *requestObj = [NSURLRequest requestWithURL:url];
    NSURLRequest *requestObj = [NSURLRequest requestWithURL:url
                                                cachePolicy:NSURLRequestReloadIgnoringLocalAndRemoteCacheData
                                            timeoutInterval:30];
    
    NSURLConnection *theConnection=[[NSURLConnection alloc] initWithRequest:requestObj delegate:self];
    if (theConnection)
    {
        // Create the NSMutableData to hold the received data.
            receivedData = [NSMutableData data];
    }
    else
    {
        // Inform the user that the connection failed.
        //[self displayAlert:@"Connection Error. Please try again later."];
        [self displayAlert:NSLocalizedString(@"MSG_CONNECTION_ERROR", nil)];
        //[self removeLoadingView];
    } */
    [self getJsonResponse:urlAddress success:^(NSDictionary *responseDict) {
        
        //NSLog(@"%@",responseDict);
        [self successfunction:responseDict];
        
    } failure:^(NSError *error) {
        // release the connection, and the data object
        
        [self processdata];
        [self displayAlert:NSLocalizedString(@"MSG_CONNECTION_ERROR", nil)];
    }];
}

-(void)getJsonResponse : (NSString *)urlStr success : (void (^)(NSDictionary *responseDict))success failure:(void(^)(NSError* error))failure
{
    
    
    NSURLSession * session = [NSURLSession sharedSession];
    NSURL * url = [NSURL URLWithString: urlStr];
    
    
    // Asynchronously Api is hit here
    NSURLSessionDataTask * dataTask = [session dataTaskWithURL:url completionHandler:^(NSData *data, NSURLResponse *response, NSError *error)
                                       {
                                           
                                           //NSLog(@"%@",data);
                                           NSDictionary * json;
                                           if ([data length]>0) {
                                               json  = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
                                               //NSLog(@"%@",json);
                                               success(json);
                                           } else {
                                               json = nil;
                                               dispatch_async(dispatch_get_main_queue(), ^ {
                                                   [self displayAlert:NSLocalizedString(@"MSG_CONNECTION_ERROR", nil)];
                                               });
                                           }
                                           
                                           
                                       }];
    
    [dataTask resume] ; // Executed First
    
    
}

-(void) successfunction:(NSDictionary *)responseDict {
    dispatch_async(dispatch_get_main_queue(), ^{
        //NSString *jsonString = [responseDict JSONRepresentation];
        
        if ([currentRequestMenu isEqualToString:@"counterFav"]) {
            //if([receivedData length]>10)
            //{
            NSDictionary *dictionary = responseDict;
            //NSLog(@"dictionaryFav %@",dictionary);
            [listOfContentsFav removeAllObjects];
            for(NSDictionary *row in dictionary)
            {
                dicContentsFav = [NSDictionary dictionaryWithObjectsAndKeys:[row objectForKey:@"content_id"],@"id",[row objectForKey:@"cntFav"],@"cntFav",nil];
                [listOfContentsFav addObject:dicContentsFav];
            }
            //NSLog(@"listOfContentsFav %@",listOfContentsFav);
            //}
            
            NSString *urlAddress1 = [[NSString alloc] initWithFormat:@"%@comments_counter.php?db=%@&userid=%@&udid=%@&cuid=%@&top=%@", appdelegate.user_apipath, [self urlEncode:appdelegate.db], [self urlEncode:appdelegate.user], [self urlEncode:appdelegate.uuid], [self urlEncode:appdelegate.customer_id], appdelegate.plugin_id];
            currentRequestMenu = @"counterComments";
            //NSLog(@"appdelegate.user_apipath:: %@", appdelegate.user_apipath);
            //NSLog(@"urlAddress:: %@", urlAddress);
            [self connectToServer:urlAddress1];
            
        } else if ([currentRequestMenu isEqualToString:@"counterComments"]) {
            //if([receivedData length]>10)
            //{
            NSDictionary *dictionary = responseDict;
            //NSLog(@"dictionaryComments %@",dictionary);
            [listOfContentsComments removeAllObjects];
            for(NSDictionary *row in dictionary)
            {
                dicContentsComments = [NSDictionary dictionaryWithObjectsAndKeys:[row objectForKey:@"content_id"],@"id",[row objectForKey:@"cntComments"],@"cntComments",nil];
                [listOfContentsComments addObject:dicContentsComments];
            }
            //NSLog(@"listOfContentsComments %@",listOfContentsComments);
            //}
            
            NSString *urlAddress3 = [[NSString alloc] initWithFormat:@"%@favorite_list.php?db=%@&userid=%@&udid=%@&cuid=%@&top=%@", appdelegate.user_apipath, [self urlEncode:appdelegate.db], [self urlEncode:appdelegate.user], [self urlEncode:appdelegate.uuid], [self urlEncode:appdelegate.customer_id], appdelegate.plugin_id];
            currentRequestMenu = @"favoriteList";
            //NSLog(@"appdelegate.user_apipath:: %@", appdelegate.user_apipath);
            //NSLog(@"urlAddress:: %@", urlAddress);
            [self connectToServer:urlAddress3];
            
        } else if ([currentRequestMenu isEqualToString:@"favoriteList"]) {
            //if([receivedData length]>10)
            //{
            NSDictionary *dictionary = responseDict;
            //NSLog(@"FavoritelisDictionary %@",dictionary);
            [listOfFav removeAllObjects];
            for(NSDictionary *row in dictionary)
            {
                dicFav = [NSDictionary dictionaryWithObjectsAndKeys:[row objectForKey:@"content_id"],@"id",nil];
                [listOfFav addObject:dicFav];
            }
            [array1 removeAllObjects];
            for(id item in listOfFav){
                [array1 addObject:[item objectForKey:@"id"]]; // Add the values to this created mutable array
            }
            contentfavid = [array1 componentsJoinedByString:@","];
            //NSLog(@"contentfavid %@",contentfavid);
            [self processdata];
            
            //}
        } else if ([currentRequestMenu isEqualToString:@"AddFavorite"]){
            //if([receivedData length]>10)
            //{
            NSDictionary *dictionaryadddel = responseDict;
            //NSLog(@"AddFavorite %@",dictionaryadddel);
            for(NSDictionary *row in dictionaryadddel)
            {
                if ([[row objectForKey:@"status"] isEqualToString:@"1"]) {
                    NSString *urlAddress = [[NSString alloc] initWithFormat:@"%@favorite_counter.php?db=%@&userid=%@&udid=%@&cuid=%@&top=%@", appdelegate.user_apipath, [self urlEncode:appdelegate.db], [self urlEncode:appdelegate.user], [self urlEncode:appdelegate.uuid], [self urlEncode:appdelegate.customer_id], appdelegate.plugin_id];
                    currentRequestMenu = @"counterFav";
                    //NSLog(@"appdelegate.user_apipath:: %@", appdelegate.user_apipath);
                    //NSLog(@"urlAddress:: %@", urlAddress);
                    [self connectToServer:urlAddress];
                }
            }
            //[self processdata];
            //}
        }
    });
    
}

/*- (void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response
{
        [receivedData setLength:0];
}

- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data
{
        [receivedData appendData:data];
}

- (void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error {
    // release the connection, and the data object

    connection =nil;
    receivedData =nil;
    
    //NSLog(@"%@",error.description);
    
    //[self removeLoadingView];
    [self processdata];
    [self displayAlert:NSLocalizedString(@"MSG_CONNECTION_ERROR", nil)];
}

- (void)connectionDidFinishLoading:(NSURLConnection *)connection
{
        
    NSString *response = [[NSString alloc] initWithData:receivedData encoding:NSUTF8StringEncoding];
    
    if ([currentRequestMenu isEqualToString:@"counterFav"]) {
        //if([receivedData length]>10)
        //{
            NSDictionary *dictionary = [response JSONValue];
            //NSLog(@"dictionaryFav %@",dictionary);
            [listOfContentsFav removeAllObjects];
            for(NSDictionary *row in dictionary)
            {
                dicContentsFav = [NSDictionary dictionaryWithObjectsAndKeys:[row objectForKey:@"content_id"],@"id",[row objectForKey:@"cntFav"],@"cntFav",nil];
                [listOfContentsFav addObject:dicContentsFav];
            }
            //NSLog(@"listOfContentsFav %@",listOfContentsFav);
        //}
        
        NSString *urlAddress1 = [[NSString alloc] initWithFormat:@"%@comments_counter.php?db=%@&userid=%@&udid=%@&cuid=%@&top=%@", appdelegate.user_apipath, [self urlEncode:appdelegate.db], [self urlEncode:appdelegate.user], [self urlEncode:appdelegate.uuid], [self urlEncode:appdelegate.customer_id], appdelegate.plugin_id];
        currentRequestMenu = @"counterComments";
        //NSLog(@"appdelegate.user_apipath:: %@", appdelegate.user_apipath);
        //NSLog(@"urlAddress:: %@", urlAddress);
        [self connectToServer:urlAddress1];
        
    } else if ([currentRequestMenu isEqualToString:@"counterComments"]) {
        //if([receivedData length]>10)
        //{
            NSDictionary *dictionary = [response JSONValue];
            //NSLog(@"dictionaryComments %@",dictionary);
            [listOfContentsComments removeAllObjects];
            for(NSDictionary *row in dictionary)
            {
                dicContentsComments = [NSDictionary dictionaryWithObjectsAndKeys:[row objectForKey:@"content_id"],@"id",[row objectForKey:@"cntComments"],@"cntComments",nil];
                [listOfContentsComments addObject:dicContentsComments];
            }
            //NSLog(@"listOfContentsComments %@",listOfContentsComments);
        //}
            
        NSString *urlAddress3 = [[NSString alloc] initWithFormat:@"%@favorite_list.php?db=%@&userid=%@&udid=%@&cuid=%@&top=%@", appdelegate.user_apipath, [self urlEncode:appdelegate.db], [self urlEncode:appdelegate.user], [self urlEncode:appdelegate.uuid], [self urlEncode:appdelegate.customer_id], appdelegate.plugin_id];
        currentRequestMenu = @"favoriteList";
        //NSLog(@"appdelegate.user_apipath:: %@", appdelegate.user_apipath);
        //NSLog(@"urlAddress:: %@", urlAddress);
        [self connectToServer:urlAddress3];
            
    } else if ([currentRequestMenu isEqualToString:@"favoriteList"]) {
        //if([receivedData length]>10)
        //{
            NSDictionary *dictionary = [response JSONValue];
            //NSLog(@"FavoritelisDictionary %@",dictionary);
            [listOfFav removeAllObjects];
            for(NSDictionary *row in dictionary)
            {
                dicFav = [NSDictionary dictionaryWithObjectsAndKeys:[row objectForKey:@"content_id"],@"id",nil];
                [listOfFav addObject:dicFav];
            }
            [array1 removeAllObjects];
            for(id item in listOfFav){
                [array1 addObject:[item objectForKey:@"id"]]; // Add the values to this created mutable array
            }
            contentfavid = [array1 componentsJoinedByString:@","];
            //NSLog(@"contentfavid %@",contentfavid);
            [self processdata];
        //}
    } else if ([currentRequestMenu isEqualToString:@"AddFavorite"]){
        //if([receivedData length]>10)
        //{
            NSDictionary *dictionaryadddel = [response JSONValue];
            //NSLog(@"AddFavorite %@",dictionaryadddel);
            for(NSDictionary *row in dictionaryadddel)
            {
                if ([[row objectForKey:@"status"] isEqualToString:@"1"]) {
                    NSString *urlAddress = [[NSString alloc] initWithFormat:@"%@favorite_counter.php?db=%@&userid=%@&udid=%@&cuid=%@&top=%@", appdelegate.user_apipath, [self urlEncode:appdelegate.db], [self urlEncode:appdelegate.user], [self urlEncode:appdelegate.uuid], [self urlEncode:appdelegate.customer_id], appdelegate.plugin_id];
                    currentRequestMenu = @"counterFav";
                    //NSLog(@"appdelegate.user_apipath:: %@", appdelegate.user_apipath);
                    //NSLog(@"urlAddress:: %@", urlAddress);
                    [self connectToServer:urlAddress];
                }
            }
            //[self processdata];
        //}
    }
    //response =nil;
    
} */

-(void) addLoadingView {
    //loadingView = [LoadingView loadingViewInView:appdelegate.window];
}

-(void) removeLoadingView
{
   // [loadingView removeView];
}

-(void) displayAlert: (NSString *) stralert {
    NSString *str = [[NSString alloc] initWithFormat:@"%@", stralert];
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:appdelegate.iconname
                                                    message:str
                                                   delegate:self
                                          cancelButtonTitle:NSLocalizedString(@"done", nil)
                                          otherButtonTitles:nil];
    
    [alert show];
    str =nil;
    alert =nil;
}

- (BOOL) isConnectionAvailable
{
    Reachability *networkReachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus networkStatus = [networkReachability currentReachabilityStatus];
    if (networkStatus == NotReachable) {
        //NSLog(@"There IS NO internet connection");
        return FALSE;
    } else {
        //NSLog(@"There IS internet connection");
        return TRUE;
    }
}


@end
