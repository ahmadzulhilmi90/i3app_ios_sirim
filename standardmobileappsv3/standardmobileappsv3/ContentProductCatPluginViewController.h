//
//  ContentPluginViewController.h
//  standardmobileappsv3
//
//  Created by M3Online on 5/30/14.
//  Copyright (c) 2014 M3Online. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <SDWebImage/UIButton+WebCache.h>
#import <SDWebImage/UIImageView+WebCache.h>
#import "WEPopoverController.h"
#import <MessageUI/MessageUI.h>

@interface ContentProductCatPluginViewController : UIViewController <UITextFieldDelegate, WEPopoverControllerDelegate, UITableViewDelegate, UITableViewDataSource, UIViewControllerTransitioningDelegate> {
    
    CGFloat animatedDistance;
    
    WEPopoverController *maincatPopoverController;
    WEPopoverController *subcatPopoverController;
    
    UITableView *maincatTableView;
    UITableView *subcatTableView;
    
    UIViewController *maincatContent;
    UIViewController *subcatContent;
    
    NSMutableArray *listOfmaincat;
    NSDictionary *dicmaincat;
    NSMutableArray *listOfsubcat;
    NSDictionary *dicsubcat;
    
    NSString *maincatcode;
    NSString *subcatcode;
    
    UIScrollView *scrollview;
    
    NSString *pluginValue1;
    NSString *pluginValue2;
    NSString *pluginValue3;
}

@property (strong, nonatomic) IBOutlet UITextField *txt_maincat;
@property (strong, nonatomic) IBOutlet UITextField *txt_subcat;

@property (nonatomic, strong) WEPopoverController *maincatPopoverController;
@property (nonatomic, strong) WEPopoverController *subcatPopoverController;

@property (nonatomic, retain) NSMutableArray *listOfmaincat;
@property (nonatomic, retain) NSMutableArray *listOfsubcat;

@property (strong, nonatomic) IBOutlet UIScrollView *scrollView;

@property (nonatomic, strong) NSString *pluginValue1;
@property (nonatomic, strong) NSString *pluginValue2;
@property (nonatomic, strong) NSString *pluginValue3;

@end
