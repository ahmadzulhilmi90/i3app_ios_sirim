//
//  ContentPluginViewController.m
//  standardmobileappsv3
//
//  Created by M3Online on 5/30/14.
//  Copyright (c) 2014 M3Online. All rights reserved.
//

#import "ContentProductCatPluginViewController.h"
#import "AppDelegate.h"
#import <sqlite3.h>
#import "Item.h"
#import "NSString+HTML.h"
#import "ContentPluginViewController.h"
#import <CoreLocation/CoreLocation.h>
#import "RootViewController.h"
#import "ViewController.h"
#import "InitialSlidingViewController.h"
#import "BTSlideInteractor.h"

#define CELL_CONTENT_MARGIN 8.0f
#define CELL_CONTENT_WIDTH self.view.frame.size.width-CELL_CONTENT_MARGIN

static const CGFloat KEYBOARD_ANIMATION_DURATION = 0.3;
static const CGFloat MINIMUM_SCROLL_FRACTION = 0.2;
static const CGFloat MAXIMUM_SCROLL_FRACTION = 0.8;
static const CGFloat PORTRAIT_KEYBOARD_HEIGHT = 216;

@interface ContentProductCatPluginViewController ()
@property (strong, nonatomic) BTSlideInteractor *interactor;
@end

@implementation ContentProductCatPluginViewController
UIAlertView *alert;
UIActivityIndicatorView *indicator;
CLLocationManager *locationManager;

@synthesize maincatPopoverController, subcatPopoverController, txt_maincat, txt_subcat, listOfmaincat, listOfsubcat, scrollView, pluginValue1, pluginValue2, pluginValue3;

//__strong NSString *cont_id;
//__strong NSString *cont_title;

InitialSlidingViewController *modalController;

AppDelegate *appdelegate;

- (UIColor *) colorWithHexString:(NSString *)hexString {
    NSString *colorString = [[hexString stringByReplacingOccurrencesOfString: @"#" withString: @""] uppercaseString];
    CGFloat alpha, red, blue, green;
    alpha = 1.0f;
    red   = [self colorComponentFrom: colorString start: 0 length: 2];
    green = [self colorComponentFrom: colorString start: 2 length: 2];
    blue  = [self colorComponentFrom: colorString start: 4 length: 2];
    return [UIColor colorWithRed: red green: green blue: blue alpha: alpha];
}

- (CGFloat) colorComponentFrom:(NSString *)string start:(NSUInteger)start length:(NSUInteger)length {
    NSString *substring = [string substringWithRange: NSMakeRange(start, length)];
    NSString *fullHex = length == 2 ? substring : [NSString stringWithFormat: @"%@%@", substring, substring];
    unsigned hexComponent;
    [[NSScanner scannerWithString: fullHex] scanHexInt: &hexComponent];
    return hexComponent / 255.0;
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    self.title = pluginValue1;
    
    appdelegate.homeclicked = NO;
    
    //self.title = appdelegate.iconname;
    self.view.backgroundColor = [UIColor clearColor];
    self.navigationItem.backBarButtonItem = [[UIBarButtonItem alloc]initWithTitle:@"" style:UIBarButtonItemStylePlain target:nil action:nil];
    
    appdelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    
    scrollview = [[UIScrollView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height)];
    scrollview.backgroundColor = [UIColor clearColor];
    scrollview.bounces = NO;
    [self.view addSubview:scrollview];
    
    CGRect rect = CGRectMake(0,0,20,20);
    UIGraphicsBeginImageContext( rect.size );
    [[UIImage imageNamed:@"home.png"] drawInRect:rect];
    UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    UIBarButtonItem *myHomeButton = [[UIBarButtonItem alloc] initWithImage:image style:UIBarButtonItemStyleDone target:self action:@selector(myHomeAction:)];
    
    
    NSArray *myButtonArray = [[NSArray alloc] initWithObjects:myHomeButton, nil];
    
    self.navigationItem.rightBarButtonItems = myButtonArray;
    
    txt_maincat = [[UITextField alloc] initWithFrame:CGRectMake(0, 0, scrollview.frame.size.width, 50)];
    txt_maincat.textColor = [UIColor blackColor];
    [txt_maincat setBorderStyle:UITextBorderStyleRoundedRect];
    txt_maincat.delegate = self;
    
    listOfmaincat = [[NSMutableArray alloc] init];
    listOfsubcat = [[NSMutableArray alloc] init];
    
    maincatTableView = [[UITableView alloc] init];
    maincatTableView.delegate = self;
    maincatTableView.dataSource = self;
    
    subcatTableView = [[UITableView alloc] init];
    subcatTableView.delegate = self;
    subcatTableView.dataSource = self;
    
    [self getDataFromDb];
    [self displaycontent];
}

-(void) getDataFromDb {
    
    [listOfmaincat removeAllObjects];
    
    sqlite3 *database;
    
    if(sqlite3_open([appdelegate.databasePath UTF8String], &database) == SQLITE_OK) {
        
        //if ([aplugintype isEqualToString:@"content"]) {
        
        NSString *theme_object;
        theme_object = [[NSString alloc] initWithFormat:@"SELECT id, name FROM content_category WHERE parent_id = 0 AND top_id=%@", appdelegate.plugin_id];
        //NSLog(@"theme_object:: %@", theme_object);
        
        NSString *cname;
        NSString *cid;
        
        const char *sqlStatement3 = [theme_object UTF8String];
        sqlite3_stmt *compiledStatement3;
        if(sqlite3_prepare_v2(database, sqlStatement3, -1, &compiledStatement3, NULL) == SQLITE_OK) {
            while(sqlite3_step(compiledStatement3) == SQLITE_ROW) {
                
                cid = [NSString stringWithUTF8String:(char *)sqlite3_column_text(compiledStatement3, 0)];
                cname = [NSString stringWithUTF8String:(char *)sqlite3_column_text(compiledStatement3, 1)];
                
                cname = [cname stringByDecodingHTMLEntities];
                cname = [cname stringByReplacingOccurrencesOfString:@"''" withString:@"'"];
                
                dicmaincat = [NSDictionary dictionaryWithObjectsAndKeys:cid,@"id",cname,@"name", nil];
                [listOfmaincat addObject:dicmaincat];
                
            }
        }
        // Release the compiled statement from memory
        sqlite3_finalize(compiledStatement3);
        
        cname =nil;
        cid =nil;
        
    }
    sqlite3_close(database);
    
}

-(void) displaycontent {
    
    /*
     - (void)fetchedData:(NSData *)responseData {
     //parse out the json data
     NSError* error;
     NSDictionary* json = [NSJSONSerialization
     JSONObjectWithData:responseData //1
     
     options:kNilOptions
     error:&error];
     
     NSArray* latestLoans = [json objectForKey:@"loans"]; //2
     
     NSLog(@"loans: %@", latestLoans); //3
     }
     */
    
    scrollview = [[UIScrollView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height-64)];
    scrollview.backgroundColor = [UIColor clearColor];
    scrollview.bounces = NO;
    [self.view addSubview:scrollview];
    
    UILabel *lblmaincat = [[UILabel alloc] initWithFrame: CGRectMake(scrollview.frame.size.width/2-130, 15, 70, 30)];
    [lblmaincat setFont:[UIFont systemFontOfSize:18]];
    lblmaincat.textColor = [UIColor grayColor];
    lblmaincat.textAlignment = NSTextAlignmentLeft;
    lblmaincat.backgroundColor = [UIColor clearColor];
    lblmaincat.numberOfLines = 0;
    [lblmaincat setText:pluginValue2];
    [scrollview addSubview:lblmaincat];
    
    txt_maincat = [[UITextField alloc] initWithFrame:CGRectMake(scrollview.frame.size.width/2-70, 10, 180, 40)];
    txt_maincat.textColor = [UIColor blackColor];
    [txt_maincat setBorderStyle:UITextBorderStyleRoundedRect];
    //txt_gender.secureTextEntry = YES;
    [scrollview addSubview:txt_maincat];
    txt_maincat.delegate = self;
    
    UILabel *lblsubcat = [[UILabel alloc] initWithFrame: CGRectMake(scrollview.frame.size.width/2-130, 60, 70, 30)];
    [lblsubcat setFont:[UIFont systemFontOfSize:18]];
    lblsubcat.textColor = [UIColor grayColor];
    lblsubcat.textAlignment = NSTextAlignmentLeft;
    lblsubcat.backgroundColor = [UIColor clearColor];
    lblsubcat.numberOfLines = 0;
    [lblsubcat setText:pluginValue3];
    [scrollview addSubview:lblsubcat];
    
    txt_subcat = [[UITextField alloc] initWithFrame:CGRectMake(scrollview.frame.size.width/2-70, 60, 180, 40)];
    txt_subcat.textColor = [UIColor blackColor];
    [txt_subcat setBorderStyle:UITextBorderStyleRoundedRect];
    //txt_language.secureTextEntry = YES;
    [scrollview addSubview:txt_subcat];
    txt_subcat.delegate = self;
    
    UIButton *buttonsubmit = [UIButton buttonWithType:UIButtonTypeRoundedRect];
    buttonsubmit.tag = 1001;
    [buttonsubmit addTarget:self action:@selector(aMethod:) forControlEvents:UIControlEventTouchUpInside];
    [buttonsubmit setTitle:NSLocalizedString(@"submit", nil) forState:UIControlStateNormal];
    [buttonsubmit setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    [buttonsubmit setBackgroundColor:[UIColor colorWithRed:230/255.0 green:230/255.0 blue:230/255.0 alpha:1]];
    buttonsubmit.frame = CGRectMake(scrollview.frame.size.width/2-90, 130, 180, 44.0);
    [scrollview addSubview:buttonsubmit];
    
    scrollview.contentSize = CGSizeMake(scrollview.frame.size.width, 180);
    scrollview =nil;
    
}

- (void)aMethod:(id)sender
{
    UIButton *clicked = (UIButton *) sender;
    
    //Submit button
    if (clicked.tag==1001)
    {
        if ([[txt_maincat text] length] == 0 || [[txt_subcat text] length] == 0) {
            [self displayAlert:[NSString stringWithFormat:@"%@", NSLocalizedString(@"blankfield", nil)]];
        } else {
            appdelegate.plugin_id = subcatcode;
            //NSLog(@"subcatcode: %@", subcatcode);
            if (appdelegate.showloading) {
                alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"loading", nil) message:nil delegate:self cancelButtonTitle:nil otherButtonTitles: nil];
                
                //a simple activity indicator:
                UIActivityIndicatorView *activityIndicator = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
                activityIndicator.frame= CGRectMake(50, 10, 37, 37);
                activityIndicator.autoresizingMask = UIViewAutoresizingFlexibleTopMargin | UIViewAutoresizingFlexibleRightMargin | UIViewAutoresizingFlexibleBottomMargin | UIViewAutoresizingFlexibleLeftMargin;
                [activityIndicator startAnimating];
                
                //the magic line below,
                //we associate the activity indicator to the alert view: (addSubview is not used)
                [alert setValue:activityIndicator forKey:@"accessoryView"];
                
                [alert show];
            
                // Adjust the indicator so it is up a few pixels from the bottom of the alert
                [UIView beginAnimations:nil context:nil];
                [UIView setAnimationDuration:0.0];
                [UIView setAnimationDelay:0.0];
                [UIView setAnimationTransition:UIViewAnimationTransitionNone forView:alert cache:YES];
                [UIView setAnimationDelegate:self];
                [UIView setAnimationDidStopSelector:@selector(startupAnimationDoneIndicator:finished:context:)];
                alert.alpha = 0.5;
                [UIView commitAnimations];
            } else {
                [self goNextPage];
            }
        }
    }
}

-(void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    //[self processdata];
}

//Slide Menu
- (void)willAnimateRotationToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation duration:(NSTimeInterval)duration
{
    //NSLog(@"view frame=%@ bounds=%@", NSStringFromCGRect(self.view.frame), NSStringFromCGRect(self.view.bounds));
}


- (void)showModalButtonWasTouched
{
    self.interactor = [[BTSlideInteractor alloc] init];
    self.interactor.presenting = YES;
    
    //BTModalViewController *modalController = [self.storyboard instantiateViewControllerWithIdentifier:@"ModalViewController"];
    modalController = [[InitialSlidingViewController alloc] init];
    modalController.modalPresentationStyle = UIModalPresentationCustom;
    modalController.transitioningDelegate = self.interactor;
    [self.navigationController presentViewController:modalController animated:YES completion:nil];
    //[self.navigationController popToViewController:modalController animated:NO];
    //[self.navigationController pushViewController:modalController animated:YES];
}
// End Slide Menu

-(void)myHomeAction:(UIBarButtonItem *)sender{
    
    appdelegate.nextPage = @"";
    
    if ([appdelegate.listOfVerMenu count]>0) {
        if (appdelegate.homeclicked==NO) {
            appdelegate.homeclicked = YES;
            [self showModalButtonWasTouched];
        } else {
            appdelegate.homeclicked = NO;
            [modalController dismissViewControllerAnimated:YES completion:nil];
        }
    } else {
        //AppDelegate *appdelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
        //NSLog(@"View Controllers : %@",[appdelegate.navController viewControllers]);
        NSMutableArray *navigationArray = [[NSMutableArray alloc] initWithArray: appdelegate.navController.viewControllers];
        
        for (UIViewController* viewController in appdelegate.navController.viewControllers) {
            
            //This if condition checks whether the viewController's class is MyGroupViewController
            // if true that means its the MyGroupViewController (which has been pushed at some point)
            if ([viewController isKindOfClass:[RootViewController class]] ) {
                
                // Here viewController is a reference of UIViewController base class of MyGroupViewController
                // but viewController holds MyGroupViewController  object so we can type cast it here
                //ThemeViewController *themeViewController = (ThemeViewController*)viewController;
                //[self.navigationController popToViewController:themeViewController animated:YES];
                [navigationArray removeObjectAtIndex: 0];
            }
        }
        appdelegate.navController.viewControllers = navigationArray;
        for (UIViewController* viewController in appdelegate.navController.viewControllers) {
            
            //This if condition checks whether the viewController's class is MyGroupViewController
            // if true that means its the MyGroupViewController (which has been pushed at some point)
            if ([viewController isKindOfClass:[ViewController class]] ) {
                
                // Here viewController is a reference of UIViewController base class of MyGroupViewController
                // but viewController holds MyGroupViewController  object so we can type cast it here
                ViewController *themeViewController = (ViewController*)viewController;
                [appdelegate.navController popToViewController:themeViewController animated:NO];
                //[self.navigationController popToViewController:viewController animated:YES];
            }
        }
        //NSLog(@"View Controllers111 : %@",appdelegate.navController.viewControllers);
    }
    
}

-(void)textFieldDidBeginEditing:(UITextField *)textField
{
    CGRect textFieldRect =
    [self.view.window convertRect:textField.bounds fromView:textField];
    CGRect viewRect =
    [self.view.window convertRect:self.view.bounds fromView:self.view];
    
    CGFloat midline = textFieldRect.origin.y + 0.5 * textFieldRect.size.height;
    CGFloat numerator =
    midline - viewRect.origin.y
    - MINIMUM_SCROLL_FRACTION * viewRect.size.height;
    CGFloat denominator =
    (MAXIMUM_SCROLL_FRACTION - MINIMUM_SCROLL_FRACTION)
    * viewRect.size.height;
    CGFloat heightFraction = numerator / denominator;
    if (heightFraction < 0.0)
    {
        heightFraction = 0.0;
    }
    else if (heightFraction > 1.0)
    {
        heightFraction = 1.0;
    }
    
    
    animatedDistance = floor(PORTRAIT_KEYBOARD_HEIGHT * heightFraction);
    
    CGRect viewFrame = self.view.frame;
    viewFrame.origin.y -= animatedDistance;
    
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationBeginsFromCurrentState:YES];
    [UIView setAnimationDuration:KEYBOARD_ANIMATION_DURATION];
    
    [self.view setFrame:viewFrame];
    
    [UIView commitAnimations];
}

- (void)textFieldDidEndEditing:(UITextField *)textField

{
    CGRect viewFrame = self.view.frame;
    viewFrame.origin.y += animatedDistance;
    
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationBeginsFromCurrentState:YES];
    [UIView setAnimationDuration:KEYBOARD_ANIMATION_DURATION];
    
    [self.view setFrame:viewFrame];
    
    [UIView commitAnimations];
    
}

#pragma mark WEPopoverControllerDelegate implementation

- (void)popoverControllerDidDismissPopover:(WEPopoverController *)thePopoverController {
    //Safe to release the popover here
    self.maincatPopoverController = nil;
    self.subcatPopoverController = nil;
}

- (BOOL)popoverControllerShouldDismissPopover:(WEPopoverController *)thePopoverController {
    //The popover is automatically dismissed if you click outside it, unless you return NO here
    return YES;
}

- (WEPopoverContainerViewProperties *)improvedContainerViewProperties {
    
    WEPopoverContainerViewProperties *props = [WEPopoverContainerViewProperties alloc];
    NSString *bgImageName = nil;
    CGFloat bgMargin = 0.0;
    CGFloat bgCapSize = 0.0;
    CGFloat contentMargin = 4.0;
    
    bgImageName = @"popoverBg.png";
    
    // These constants are determined by the popoverBg.png image file and are image dependent
    bgMargin = 13; // margin width of 13 pixels on all sides popoverBg.png (62 pixels wide - 36 pixel background) / 2 == 26 / 2 == 13
    bgCapSize = 31; // ImageSize/2  == 62 / 2 == 31 pixels
    
    props.leftBgMargin = bgMargin;
    props.rightBgMargin = bgMargin;
    props.topBgMargin = bgMargin;
    props.bottomBgMargin = bgMargin;
    props.leftBgCapSize = bgCapSize;
    props.topBgCapSize = bgCapSize;
    props.bgImageName = bgImageName;
    props.leftContentMargin = contentMargin;
    props.rightContentMargin = contentMargin - 1; // Need to shift one pixel for border to look correct
    props.topContentMargin = contentMargin;
    props.bottomContentMargin = contentMargin;
    
    props.arrowMargin = 4.0;
    
    props.upArrowImageName = @"popoverArrowUp.png";
    props.downArrowImageName = @"popoverArrowDown.png";
    props.leftArrowImageName = @"popoverArrowLeft.png";
    props.rightArrowImageName = @"popoverArrowRight.png";
    return props;
}

- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField {
    
    if(textField == txt_maincat)
    {
        [maincatTableView reloadData];
        
        if (maincatContent == nil )
        {
            maincatContent = [[UIViewController alloc] init];
            maincatContent.view = maincatTableView;
        }
        else
        {
            if (maincatContent.view != maincatTableView) {
                maincatContent.view = maincatTableView;
            }
        }
        
        if (!self.maincatPopoverController)
        {
            self.maincatPopoverController = [[WEPopoverController alloc] initWithContentViewController:maincatContent];
            self.maincatPopoverController.popoverContentSize = CGSizeMake(400.0f, 600.0f);
            
            if ([self.maincatPopoverController respondsToSelector:@selector(setContainerViewProperties:)])
                [self.maincatPopoverController setContainerViewProperties:[self improvedContainerViewProperties]];
            
            self.maincatPopoverController.delegate = self;
            self.maincatPopoverController.passthroughViews = [NSArray arrayWithObject:appdelegate.navController.navigationBar];
            [self.maincatPopoverController presentPopoverFromRect:textField.bounds inView:textField permittedArrowDirections:UIPopoverArrowDirectionUp animated:NO];
        }
        else
        {
            [self.maincatPopoverController dismissPopoverAnimated:NO];
            self.maincatPopoverController = nil;
        }
        return NO;  // Hide both keyboard and blinking cursor.
    }
    
    if(textField == txt_subcat){
        
        [subcatTableView reloadData];
        
        if (subcatContent == nil )
        {
            subcatContent = [[UIViewController alloc] init];
            subcatContent.view = subcatTableView;
        }
        else
        {
            if (subcatContent.view != subcatTableView) {
                subcatContent.view = subcatTableView;
            }
        }
        
        if (!self.subcatPopoverController)
        {
            self.subcatPopoverController = [[WEPopoverController alloc] initWithContentViewController:subcatContent];
            self.subcatPopoverController.popoverContentSize = CGSizeMake(400.0f, 600.0f);
            if ([self.subcatPopoverController respondsToSelector:@selector(setContainerViewProperties:)])
                [self.subcatPopoverController setContainerViewProperties:[self improvedContainerViewProperties]];
            
            self.subcatPopoverController.delegate = self;
            self.subcatPopoverController.passthroughViews = [NSArray arrayWithObject:appdelegate.navController.navigationBar];
            [self.subcatPopoverController presentPopoverFromRect:textField.bounds inView:textField permittedArrowDirections:UIPopoverArrowDirectionUp animated:NO];
        }
        else
        {
            [self.subcatPopoverController dismissPopoverAnimated:NO];
            self.subcatPopoverController = nil;
        }
        
        return NO;  // Hide both keyboard and blinking cursor.
        
    }
    
    return YES;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)viewWillDisappear:(BOOL)animated {
    [maincatPopoverController dismissPopoverAnimated:NO];
    //[branchPopoverController release];
    maincatPopoverController = nil;
    [subcatPopoverController dismissPopoverAnimated:NO];
    //[prodPopoverController release];
    subcatPopoverController = nil;
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{

    if(tableView == maincatTableView){
        return [listOfmaincat count];
    }
    
    if(tableView == subcatTableView){
        return [listOfsubcat count];
    }
    
    return 0;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if(tableView == maincatTableView){
        return 30;
    }
    
    if(tableView == subcatTableView){
        return 30;
    }
    
	return 44;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    //NSLog(@"cellForRowAtIndexPath");
    
    if(tableView == maincatTableView) {
        UITableViewCell *cell;
        static NSString *CellIdentifier = @"CellIdentifier";
        cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        UILabel *cname;
        
        const NSInteger LEFT_TOP_TAG = 2000;
        
        if (cell == nil)
        {
            cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
            
            cname = [[UILabel alloc] initWithFrame: CGRectMake(5, 10, 310, 20)];
            cname.tag = LEFT_TOP_TAG;
            cname.textColor = [UIColor blackColor];
            cname.textAlignment = NSTextAlignmentLeft;
            cname.backgroundColor = [UIColor clearColor];
            cname.numberOfLines = 0;
            [cname setFont:[UIFont systemFontOfSize:14]];
            [cell.contentView addSubview:cname];
        }
        else
            cname = (UILabel *)[cell viewWithTag:LEFT_TOP_TAG];
        
        if ([listOfmaincat count]>0)
        {
            NSDictionary *data = [listOfmaincat objectAtIndex:indexPath.row];
            NSString *sname = [data objectForKey:@"name"];
            
            cname.text = [NSString stringWithFormat:@"%@",sname];
        }
        
        return cell;
    }
    
    if(tableView == subcatTableView) {
        UITableViewCell *cell;
        static NSString *CellIdentifier = @"CellIdentifier";
        cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        UILabel *cname;
        
        const NSInteger LEFT_TOP_TAG = 2000;
        
        if (cell == nil)
        {
            cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
            
            cname = [[UILabel alloc] initWithFrame: CGRectMake(5, 10, 310, 20)];
            cname.tag = LEFT_TOP_TAG;
            cname.textColor = [UIColor blackColor];
            cname.textAlignment = NSTextAlignmentLeft;
            cname.backgroundColor = [UIColor clearColor];
            cname.numberOfLines = 0;
            [cname setFont:[UIFont systemFontOfSize:14]];
            [cell.contentView addSubview:cname];
        }
        else
            cname = (UILabel *)[cell viewWithTag:LEFT_TOP_TAG];
        
        if ([listOfsubcat count]>0)
        {
            NSDictionary *data = [listOfsubcat objectAtIndex:indexPath.row];
            NSString *sname = [data objectForKey:@"name"];
            
            cname.text = [NSString stringWithFormat:@"%@",sname];
        }
        
        return cell;
    }
    
    return nil;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if(tableView == maincatTableView){
        NSDictionary *data = [listOfmaincat objectAtIndex:indexPath.row];
        NSString *cname = [data objectForKey:@"name"];
        NSString *ccode = [data objectForKey:@"id"];
        txt_maincat.text = cname;
        maincatcode = ccode;
        [maincatPopoverController dismissPopoverAnimated:NO];
        self.maincatPopoverController = nil;
        
        [listOfsubcat removeAllObjects];
        
        sqlite3 *database;
        if(sqlite3_open([appdelegate.databasePath UTF8String], &database) == SQLITE_OK) {
            
            //if ([aplugintype isEqualToString:@"content"]) {
            
            NSString *theme_object;
            theme_object = [[NSString alloc] initWithFormat:@"SELECT id, name, top_id FROM content_category WHERE parent_id =%@ ", maincatcode];
            //NSLog(@"theme_objectsub:: %@", theme_object);
            
            NSString *cname;
            NSString *cid;
            NSString *tid;
            
            const char *sqlStatement3 = [theme_object UTF8String];
            sqlite3_stmt *compiledStatement3;
            if(sqlite3_prepare_v2(database, sqlStatement3, -1, &compiledStatement3, NULL) == SQLITE_OK) {
                while(sqlite3_step(compiledStatement3) == SQLITE_ROW) {
                    
                    cid = [NSString stringWithUTF8String:(char *)sqlite3_column_text(compiledStatement3, 0)];
                    cname = [NSString stringWithUTF8String:(char *)sqlite3_column_text(compiledStatement3, 1)];
                    tid = [NSString stringWithUTF8String:(char *)sqlite3_column_text(compiledStatement3, 2)];
                    
                    cname = [cname stringByDecodingHTMLEntities];
                    cname = [cname stringByReplacingOccurrencesOfString:@"''" withString:@"'"];
                    
                    dicsubcat = [NSDictionary dictionaryWithObjectsAndKeys:cid,@"id",cname,@"name",tid,@"topid", nil];
                    [listOfsubcat addObject:dicsubcat];
                    
                }
            }
            // Release the compiled statement from memory
            sqlite3_finalize(compiledStatement3);
            
            cname =nil;
            cid =nil;
            tid =nil;
        }
        sqlite3_close(database);
    }
    
    if(tableView == subcatTableView){
        NSDictionary *data = [listOfsubcat objectAtIndex:indexPath.row];
        NSString *cname = [data objectForKey:@"name"];
        NSString *ccode = [data objectForKey:@"topid"];
        txt_subcat.text = cname;
        subcatcode = ccode;
        [subcatPopoverController dismissPopoverAnimated:NO];
        self.subcatPopoverController = nil;
    }
    
}

-(void) dissmissIndicator {
    [alert dismissWithClickedButtonIndex:0 animated:NO];
}

-(void) goNextPage {
    ContentPluginViewController *contentPluginViewController = [[ContentPluginViewController alloc] init];
    [appdelegate.navController pushViewController:contentPluginViewController animated:NO];
}

- (void)startupAnimationDoneIndicator:(NSString *)animationID finished:(NSNumber *)finished context:(void *)context
{
    [self goNextPage];
    [self dissmissIndicator];
    alert =nil;
}

-(BOOL) textFieldShouldReturn:(UITextField *)textField{
    [textField resignFirstResponder];
    return YES;
}

/*** Push Notification 27Aug2015 ***/
-(void) viewDidDisappear:(BOOL)animated {
    
    appdelegate.notify_actiontype = @"";
    appdelegate.notify_actionvalue = @"";
    appdelegate.notify_contentid = @"";
    
    appdelegate.homeclicked = NO;
    [modalController dismissViewControllerAnimated:YES completion:nil];
    
    [super viewDidDisappear:animated];
    
}

-(void) displayAlert: (NSString *) stralert {
    NSString *str = [[NSString alloc] initWithFormat:@"%@", stralert];
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:appdelegate.iconname
                                                    message:str
                                                   delegate:self
                                          cancelButtonTitle:NSLocalizedString(@"done", nil)
                                          otherButtonTitles:nil];
    
    [alert show];
    str =nil;
    alert =nil;
}

/*** Push Notification 27Aug2015 ***/

@end
