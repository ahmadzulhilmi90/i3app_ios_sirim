//
//  ContentSearchViewController.h
//  standardmobileappsv3
//
//  Created by M3Online on 7/16/14.
//  Copyright (c) 2014 M3Online. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <SDWebImage/UIButton+WebCache.h>
#import <SDWebImage/UIImageView+WebCache.h>

@interface ContentSearchViewController : UIViewController <UITextFieldDelegate, UITableViewDataSource, UITableViewDelegate, UIViewControllerTransitioningDelegate> {
    
    IBOutlet UITableView *GridtableView;
    NSMutableArray *sections;
    
    IBOutlet UITableView *MenutableView;
    
    UIScrollView *scrollview;
}

@property (strong, nonatomic) IBOutlet UIView *view_warrantyList;
@property (strong, nonatomic) NSMutableArray *listOfContents;
@property (nonatomic, strong) UITableView *GridtableView;
@property (nonatomic, strong) NSMutableArray *sections;
@property (nonatomic, strong) UITableView *MenutableView;
@property (strong, nonatomic) NSDictionary *dicContents;

@property (strong, nonatomic) IBOutlet UITextField *txt_search;
@property (strong, nonatomic) IBOutlet UIButton *button_searchDetail;

@property (strong, nonatomic) IBOutlet UIScrollView *scrollView;

@property (strong, nonatomic) NSString *list_template;

@property (strong, nonatomic) NSString *cont_title;

@end
