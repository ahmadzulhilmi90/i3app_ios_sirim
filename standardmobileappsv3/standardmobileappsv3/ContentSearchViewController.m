//
//  ContentSearchViewController.m
//  standardmobileappsv3
//
//  Created by M3Online on 7/16/14.
//  Copyright (c) 2014 M3Online. All rights reserved.
//

#import "ContentSearchViewController.h"
#import "AppDelegate.h"
#import <sqlite3.h>
#import "Item.h"
#import "ContentPluginDetailsViewController.h"
#import "NSString+HTML.h"
#import "RootViewController.h"
#import "ViewController.h"
#import "InitialSlidingViewController.h"
#import "BTSlideInteractor.h"

@interface ContentSearchViewController ()
@property (strong, nonatomic) BTSlideInteractor *interactor;
@end

InitialSlidingViewController *modalController;

AppDelegate *appdelegate;
__strong NSString *cont_id;
//__strong NSString *cont_title;

@implementation ContentSearchViewController
UIAlertView *alert;
UIActivityIndicatorView *indicator;
@synthesize listOfContents, MenutableView, GridtableView, sections, scrollView;
@synthesize txt_search, button_searchDetail, dicContents, list_template, cont_title;

- (UIColor *) colorWithHexString:(NSString *)hexString {
    NSString *colorString = [[hexString stringByReplacingOccurrencesOfString: @"#" withString: @""] uppercaseString];
    CGFloat alpha, red, blue, green;
    alpha = 1.0f;
    red   = [self colorComponentFrom: colorString start: 0 length: 2];
    green = [self colorComponentFrom: colorString start: 2 length: 2];
    blue  = [self colorComponentFrom: colorString start: 4 length: 2];
    return [UIColor colorWithRed: red green: green blue: blue alpha: alpha];
}

- (CGFloat) colorComponentFrom:(NSString *)string start:(NSUInteger)start length:(NSUInteger)length {
    NSString *substring = [string substringWithRange: NSMakeRange(start, length)];
    NSString *fullHex = length == 2 ? substring : [NSString stringWithFormat: @"%@%@", substring, substring];
    unsigned hexComponent;
    [[NSScanner scannerWithString: fullHex] scanHexInt: &hexComponent];
    return hexComponent / 255.0;
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    self.title = cont_title;
    self.view.backgroundColor = [UIColor whiteColor];
    self.navigationItem.backBarButtonItem = [[UIBarButtonItem alloc]initWithTitle:@"" style:UIBarButtonItemStylePlain target:nil action:nil];
    
    appdelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    
    appdelegate.homeclicked = NO;
    
    txt_search = [[UITextField alloc] initWithFrame:CGRectMake(10, 10, self.view.frame.size.width-130, 40)];
    txt_search.textColor = [UIColor blackColor];
    [txt_search setBorderStyle:UITextBorderStyleRoundedRect];
    [self.view addSubview:txt_search];
    txt_search.delegate = self;
    
    button_searchDetail = [UIButton buttonWithType:UIButtonTypeRoundedRect];
    [button_searchDetail addTarget:self
               action:@selector(processdata) forControlEvents:UIControlEventTouchUpInside];
    [button_searchDetail setTitle:NSLocalizedString(@"search", nil) forState:UIControlStateNormal];
    [button_searchDetail setTitleColor:[UIColor blackColor] forState:UIControlStateNormal ];
    button_searchDetail.frame = CGRectMake(self.view.frame.size.width-110, 10.0, 90.0, 40.0);
    button_searchDetail.backgroundColor = [UIColor grayColor];
    button_searchDetail.layer.cornerRadius=6;
    [self.view addSubview:button_searchDetail];
    
    scrollview = [[UIScrollView alloc] initWithFrame:CGRectMake(0, 60, self.view.frame.size.width, self.view.frame.size.height-60)];
    scrollview.backgroundColor = [UIColor whiteColor];
    scrollview.bounces = NO;
    [self.view addSubview:scrollview];
    
    CGRect rect = CGRectMake(0,0,20,20);
    UIGraphicsBeginImageContext( rect.size );
    [[UIImage imageNamed:@"home.png"] drawInRect:rect];
    UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    UIBarButtonItem *myHomeButton = [[UIBarButtonItem alloc] initWithImage:image style:UIBarButtonItemStyleDone target:self action:@selector(myHomeAction:)];
    
    
    NSArray *myButtonArray = [[NSArray alloc] initWithObjects:myHomeButton, nil];
    
    self.navigationItem.rightBarButtonItems = myButtonArray;
    
    listOfContents = [[NSMutableArray alloc] init];
}

-(void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
}

//Slide Menu
- (void)willAnimateRotationToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation duration:(NSTimeInterval)duration
{
    //NSLog(@"view frame=%@ bounds=%@", NSStringFromCGRect(self.view.frame), NSStringFromCGRect(self.view.bounds));
}


- (void)showModalButtonWasTouched
{
    self.interactor = [[BTSlideInteractor alloc] init];
    self.interactor.presenting = YES;
    
    //BTModalViewController *modalController = [self.storyboard instantiateViewControllerWithIdentifier:@"ModalViewController"];
    modalController = [[InitialSlidingViewController alloc] init];
    modalController.modalPresentationStyle = UIModalPresentationCustom;
    modalController.transitioningDelegate = self.interactor;
    [self.navigationController presentViewController:modalController animated:YES completion:nil];
    //[self.navigationController popToViewController:modalController animated:NO];
    //[self.navigationController pushViewController:modalController animated:YES];
}
// End Slide Menu

-(void)myHomeAction:(UIBarButtonItem *)sender{
    
    appdelegate.nextPage = @"";
    
    if ([appdelegate.listOfVerMenu count]>0) {
        if (appdelegate.homeclicked==NO) {
            appdelegate.homeclicked = YES;
            [self showModalButtonWasTouched];
        } else {
            appdelegate.homeclicked = NO;
            [modalController dismissViewControllerAnimated:YES completion:nil];
        }
    } else {
        //AppDelegate *appdelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
        //NSLog(@"View Controllers : %@",[appdelegate.navController viewControllers]);
        NSMutableArray *navigationArray = [[NSMutableArray alloc] initWithArray: appdelegate.navController.viewControllers];
        
        for (UIViewController* viewController in appdelegate.navController.viewControllers) {
            
            //This if condition checks whether the viewController's class is MyGroupViewController
            // if true that means its the MyGroupViewController (which has been pushed at some point)
            if ([viewController isKindOfClass:[RootViewController class]] ) {
                
                // Here viewController is a reference of UIViewController base class of MyGroupViewController
                // but viewController holds MyGroupViewController  object so we can type cast it here
                //ThemeViewController *themeViewController = (ThemeViewController*)viewController;
                //[self.navigationController popToViewController:themeViewController animated:YES];
                [navigationArray removeObjectAtIndex: 0];
            }
        }
        appdelegate.navController.viewControllers = navigationArray;
        for (UIViewController* viewController in appdelegate.navController.viewControllers) {
            
            //This if condition checks whether the viewController's class is MyGroupViewController
            // if true that means its the MyGroupViewController (which has been pushed at some point)
            if ([viewController isKindOfClass:[ViewController class]] ) {
                
                // Here viewController is a reference of UIViewController base class of MyGroupViewController
                // but viewController holds MyGroupViewController  object so we can type cast it here
                ViewController *themeViewController = (ViewController*)viewController;
                [appdelegate.navController popToViewController:themeViewController animated:NO];
                //[self.navigationController popToViewController:viewController animated:YES];
            }
        }
        //NSLog(@"View Controllers111 : %@",appdelegate.navController.viewControllers);
    }
    
}

#pragma mark UITableView Delegation
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    //NSLog(@"numberOfSectionsInTableView");
	if (tableView == GridtableView)
    {
        return [sections count];
        
    }
    
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
	//NSLog(@">>> Entering *** %s <<<", __PRETTY_FUNCTION__);
    
    //NSLog(@"tableView %@", tableView);
    
    if (tableView == MenutableView)
    {
        return [listOfContents count];
        
    }
    
    if (tableView == GridtableView) {
        return 1;
    }
    
    return 0;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (tableView == MenutableView) {
        if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone) {
            // For iPhone
            return 70;
        } else {
            return 110;
        }
    }
    
    if (tableView == GridtableView)
    {
        int count = [listOfContents count];
        
		int numRows = count/2;
        int numRows1 = (count)%2;
        
        if (numRows1>0)
            return (numRows+1) * self.view.frame.size.width/2;
        else
            return numRows * self.view.frame.size.width/2;
    }
	return 44;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (tableView == MenutableView)
    {
        static NSString *CellIdentifier = @"CellIdentifier";
        UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
        
        UIImageView *imgViewMenu;
        const NSInteger LEFT_TAG = 1000;
        UILabel *menutext;
        const NSInteger RIGHT_TAG = 1001;
        UILabel *pricetitle;
        const NSInteger RIGHT_TAG1 = 1002;
        UILabel *price;
        const NSInteger RIGHT_TAG2 = 1003;
        UILabel *var1;
        const NSInteger RIGHT_TAG3 = 1004;
        
        if (cell == nil)
        {
            
            cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
            //cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
            
            //if ([[dictionary objectForKey:@"object_type"] isEqualToString:@"img"]) {
            if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone) {
                // For iPhone
                imgViewMenu = [[UIImageView alloc] initWithFrame:CGRectMake(5, 5, 60, 60)];
            } else {
                imgViewMenu = [[UIImageView alloc] initWithFrame:CGRectMake(5, 5, 100, 100)];
            }
            imgViewMenu.tag = LEFT_TAG;
            [cell.contentView addSubview:imgViewMenu];
            
            //} else if ([[dictionary objectForKey:@"object_type"] isEqualToString:@"text"]) {
            
            if ([list_template isEqualToString:@"4"]) {
                if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone) {
                    // For iPhone
                    menutext = [[UILabel alloc] initWithFrame: CGRectMake(70, 0, self.view.frame.size.width-70, 60)];
                } else {
                    menutext = [[UILabel alloc] initWithFrame: CGRectMake(110, 0, self.view.frame.size.width-110, 100)];
                }
            } else {
                if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone) {
                    // For iPhone
                    menutext = [[UILabel alloc] initWithFrame: CGRectMake(70, 5, self.view.frame.size.width-70, 20)];
                } else {
                    menutext = [[UILabel alloc] initWithFrame: CGRectMake(110, 5, self.view.frame.size.width-110, 33)];
                }
            }
            menutext.tag = RIGHT_TAG;
            [menutext setFont:[UIFont boldSystemFontOfSize:16]];
            menutext.textColor = [UIColor grayColor];
            menutext.textAlignment = NSTextAlignmentLeft;
            menutext.backgroundColor = [UIColor clearColor];
            menutext.numberOfLines = 0;
            [cell.contentView addSubview:menutext];
            
            if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone) {
                // For iPhone
                pricetitle = [[UILabel alloc] initWithFrame: CGRectMake(70, 25, 30, 20)];
            } else {
                pricetitle = [[UILabel alloc] initWithFrame: CGRectMake(110, 38, 30, 33)];
            }
            pricetitle.tag = RIGHT_TAG1;
            [pricetitle setFont:[UIFont boldSystemFontOfSize:16]];
            pricetitle.textColor = [UIColor grayColor];
            pricetitle.textAlignment = NSTextAlignmentLeft;
            pricetitle.backgroundColor = [UIColor clearColor];
            pricetitle.numberOfLines = 0;
            [cell.contentView addSubview:pricetitle];
            
            if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone) {
                // For iPhone
                price = [[UILabel alloc] initWithFrame: CGRectMake(70+30, 25, self.view.frame.size.width-70-30, 20)];
            } else {
                price = [[UILabel alloc] initWithFrame: CGRectMake(110+30, 38, self.view.frame.size.width-110-30, 33)];
            }
            price.tag = RIGHT_TAG2;
            [price setFont:[UIFont boldSystemFontOfSize:16]];
            price.textColor = [UIColor grayColor];
            price.textAlignment = NSTextAlignmentLeft;
            price.backgroundColor = [UIColor clearColor];
            price.numberOfLines = 0;
            [cell.contentView addSubview:price];
            
            if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone) {
                // For iPhone
                var1 = [[UILabel alloc] initWithFrame: CGRectMake(70, 45, self.view.frame.size.width-70, 20)];
            } else {
                var1 = [[UILabel alloc] initWithFrame: CGRectMake(110, 71, self.view.frame.size.width-110, 33)];
            }
            var1.tag = RIGHT_TAG3;
            [var1 setFont:[UIFont systemFontOfSize:14]];
            var1.textColor = [UIColor grayColor];
            var1.textAlignment = NSTextAlignmentLeft;
            var1.backgroundColor = [UIColor clearColor];
            var1.numberOfLines = 0;
            [cell.contentView addSubview:var1];
            //}
            
        }
        else
        {
            //if ([[dictionary objectForKey:@"object_type"] isEqualToString:@"img"]) {
            imgViewMenu = (UIImageView *)[cell viewWithTag:LEFT_TAG];
            //} else if ([[dictionary objectForKey:@"object_type"] isEqualToString:@"text"]) {
            menutext = (UILabel *)[cell viewWithTag:RIGHT_TAG];
            pricetitle = (UILabel *)[cell viewWithTag:RIGHT_TAG1];
            price = (UILabel *)[cell viewWithTag:RIGHT_TAG2];
            var1 = (UILabel *)[cell viewWithTag:RIGHT_TAG3];
            //}
        }
        
        if([listOfContents count]>0)
        {
            NSDictionary *dictionary = [listOfContents objectAtIndex:indexPath.row];
            
            NSString *imgURL = [dictionary objectForKey:@"thumbnail"];
            
            __block UIActivityIndicatorView *activityIndicator;
            __weak UIImageView *weakImageView = imgViewMenu;
            /*[weakImageView setImageWithURL:[NSURL URLWithString:imgURL] placeholderImage:[UIImage imageNamed:@"No-Image-icon.png"] options:SDWebImageProgressiveDownload progress:^(NSInteger receivedSize, NSInteger expectedSize)
             {
                 if (!activityIndicator)
                 {
                     [weakImageView addSubview:activityIndicator = [UIActivityIndicatorView.alloc initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray]];
                     activityIndicator.center = weakImageView.center;
                     [activityIndicator startAnimating];
                 }
             }
                                 completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType)
             {
                 [activityIndicator removeFromSuperview];
                 activityIndicator = nil;
             }]; */
            dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0), ^ {
                [weakImageView sd_setImageWithURL:[NSURL URLWithString:imgURL]
                                 placeholderImage:[UIImage imageNamed:@"No-Image-icon.png"]
                                          options:SDWebImageProgressiveDownload
                                         progress:^(NSInteger receivedSize, NSInteger expectedSize, NSURL *targetURL) {
                                             dispatch_async(dispatch_get_main_queue(), ^ {
                                                 if (!activityIndicator) {
                                                     
                                                     [weakImageView addSubview:activityIndicator = [UIActivityIndicatorView.alloc initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray]];
                                                     activityIndicator.center = weakImageView.center;
                                                     [activityIndicator startAnimating];
                                                 }
                                             });
                                         }
                                        completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
                                            dispatch_async(dispatch_get_main_queue(), ^ {
                                                //[MBProgressHUD hideHUDForView:self.view animated:YES];
                                                [activityIndicator removeFromSuperview];
                                                activityIndicator = nil;
                                            });
                                        }];
            });
            
            imgViewMenu.contentMode  = UIViewContentModeScaleAspectFill;
            [imgViewMenu setClipsToBounds:YES];
            
            if ([list_template isEqualToString:@"4"]) {
                menutext.text = [NSString stringWithFormat:@"%@", [dictionary objectForKey:@"title"]];
                pricetitle.text =nil;
                price.text =nil;
                var1.text =nil;
            } else {
                menutext.text = [NSString stringWithFormat:@"%@", [dictionary objectForKey:@"title"]];
                if ([[dictionary objectForKey:@"price"] floatValue]>0) {
                    pricetitle.text = [NSString stringWithFormat:@"%@", [dictionary objectForKey:@"price_title"]];
                    price.text = [NSString stringWithFormat:@"%@", [dictionary objectForKey:@"price"]];
                } else {
                    pricetitle.text =nil;
                    price.text =nil;
                }
                var1.text = [NSString stringWithFormat:@"%@", [dictionary objectForKey:@"var1"]];
            }
            
        }
        return cell;
        imgViewMenu =nil;
        menutext =nil;
        pricetitle =nil;
        price =nil;
        var1 =nil;
    }
    
    if (tableView == GridtableView)
    {
        static NSString *hlCellID = @"hlCellID";
		
		UITableViewCell *hlcell = [tableView dequeueReusableCellWithIdentifier:hlCellID];
		if(hlcell == nil)
        {
			hlcell =  [[UITableViewCell alloc]
                       initWithStyle:UITableViewCellStyleDefault reuseIdentifier:hlCellID];
			hlcell.accessoryType = UITableViewCellAccessoryNone;
			hlcell.selectionStyle = UITableViewCellSelectionStyleNone;
		}
        
        int section = indexPath.section;
		NSMutableArray *sectionItems = [sections objectAtIndex:section];
        
        int n = [sectionItems count];
		int i=0,i1=0;
		
        while(i<n)
        {
			int yy = 5 +i1*self.view.frame.size.width/2;
			int j=0;
			
            for(j=0; j<2;j++)
            {
				if (i>=n)
                    break;
                
				Item *item = [sectionItems objectAtIndex:i];
                
                if ([item.image length]>0 || [item.title length]>0)
                {
                    CGRect rect = CGRectMake(5+self.view.frame.size.width/2*j, yy, self.view.frame.size.width/2-10, self.view.frame.size.width/2-10);
                    UIButton *button=[[UIButton alloc] initWithFrame:rect];
                    [button setFrame:rect];
                    [[button layer] setBorderWidth:1.0f];
                    [[button layer] setBorderColor:[UIColor grayColor].CGColor];
                    
                    if ([item.image length]>0)
                    {
                        //[button setBackgroundImageWithURL:[NSURL URLWithString:item.image] forState:UIControlStateNormal placeholderImage:[UIImage imageNamed:@"No-Image-icon.png"]];
                        UIImageView *imgV = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, button.frame.size.width, button.frame.size.height)];
                        
                        [button addSubview:imgV];
                        
                        __block UIActivityIndicatorView *activityIndicator;
                        __weak UIImageView *weakImageView = imgV;
                        /*[weakImageView setImageWithURL:[NSURL URLWithString:item.image] placeholderImage:[UIImage imageNamed:@"No-Image-icon.png"] options:SDWebImageProgressiveDownload progress:^(NSInteger receivedSize, NSInteger expectedSize)
                         {
                             if (!activityIndicator)
                             {
                                 [weakImageView addSubview:activityIndicator = [UIActivityIndicatorView.alloc initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray]];
                                 activityIndicator.center = weakImageView.center;
                                 [activityIndicator startAnimating];
                             }
                         }
                                             completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType)
                         {
                             [activityIndicator removeFromSuperview];
                             activityIndicator = nil;
                         }]; */
                        //dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0), ^ {
                            [weakImageView sd_setImageWithURL:[NSURL URLWithString:item.image]
                                             placeholderImage:[UIImage imageNamed:@"No-Image-icon.png"]
                                                      options:SDWebImageProgressiveDownload
                                                     progress:^(NSInteger receivedSize, NSInteger expectedSize, NSURL *targetURL) {
                                                         dispatch_async(dispatch_get_main_queue(), ^ {
                                                             if (!activityIndicator) {
                                                                 
                                                                 [weakImageView addSubview:activityIndicator = [UIActivityIndicatorView.alloc initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray]];
                                                                 activityIndicator.center = weakImageView.center;
                                                                 [activityIndicator startAnimating];
                                                             }
                                                         });
                                                     }
                                                    completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
                                                        dispatch_async(dispatch_get_main_queue(), ^ {
                                                            //[MBProgressHUD hideHUDForView:self.view animated:YES];
                                                            [activityIndicator removeFromSuperview];
                                                            activityIndicator = nil;
                                                        });
                                                    }];
                        //});
                        
                        imgV.contentMode  = UIViewContentModeScaleAspectFill;
                        [imgV setClipsToBounds:YES];
                    }
                    else
                    {
                        [button setTitle:@"No Image" forState:UIControlStateNormal];
                        [button setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
                        [button setBackgroundColor:[UIColor colorWithRed:230/255.0 green:230/255.0 blue:230/255.0 alpha:1]];
                    }
                    item.image =nil;
                    
                    [button setContentMode:UIViewContentModeCenter];
                    
                    NSString *tagValue = [NSString stringWithFormat:@"%d%d", indexPath.section+1, i];
                    button.tag = [tagValue intValue];
                    
                    [button addTarget:self action:@selector(buttonPressed:) forControlEvents:UIControlEventTouchUpInside];
                    [hlcell.contentView addSubview:button];
                    
                    UIImageView *ImageBarView = [[UIImageView alloc] initWithFrame:CGRectMake((self.view.frame.size.width/2-10*j)+5, yy+self.view.frame.size.width/2-10, self.view.frame.size.width/2-10, 25)];
                    
                    UIView *overlay;
                    overlay = [[UIView alloc] initWithFrame:CGRectMake(0, 0, ImageBarView.frame.size.width, ImageBarView.frame.size.height)];
                    overlay.backgroundColor = [self colorWithHexString:appdelegate.themeColorCode];
                    overlay.alpha = 0.6f;
                    //overlay.opaque = YES;
                    
                    [ImageBarView addSubview:overlay];
                    
                    UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(5, 0, self.view.frame.size.width/2-20, 15)];
                    label.numberOfLines = 0;
                    label.lineBreakMode = NSLineBreakByWordWrapping;
                    UIFont *yourFont;
                    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone) {
                        // For iPhone
                        yourFont = [UIFont fontWithName:@"Helvetica-Bold" size:14];
                    } else {
                        yourFont = [UIFont fontWithName:@"Helvetica-Bold" size:20];
                    }
                    label.font = yourFont;
                    label.text = item.title;
                    label.backgroundColor = [UIColor clearColor];
                    label.textColor = [UIColor whiteColor];
                    [label sizeToFit];
                    label.shadowColor = [UIColor blackColor];
                    label.shadowOffset = CGSizeMake(0, 1);
                    [ImageBarView addSubview:label];
                    CGSize constrainedSize = CGSizeMake(label.frame.size.width  , 9999);
                    
                    NSDictionary *attributesDictionary = [NSDictionary dictionaryWithObjectsAndKeys:
                                                          yourFont, NSFontAttributeName,
                                                          nil];
                    
                    NSMutableAttributedString *string = [[NSMutableAttributedString alloc] initWithString:label.text attributes:attributesDictionary];
                    
                    CGRect requiredHeight = [string boundingRectWithSize:constrainedSize options:NSStringDrawingUsesLineFragmentOrigin context:nil];
                    
                    if (requiredHeight.size.width > label.frame.size.width) {
                        requiredHeight = CGRectMake(0,0, label.frame.size.width, requiredHeight.size.height);
                    }
                    CGRect newFrame = label.frame;
                    newFrame.size.height = requiredHeight.size.height;
                    label.frame = newFrame;
                    ImageBarView.frame = CGRectMake(5+self.view.frame.size.width/2*j, yy+self.view.frame.size.width/2-10-newFrame.size.height, self.view.frame.size.width/2-10, newFrame.size.height);
                    overlay.frame = CGRectMake(0, 0, ImageBarView.frame.size.width, ImageBarView.frame.size.height);
                    
                    [hlcell.contentView addSubview:ImageBarView];
                }
				
				i++;
			}
			i1 = i1+1;
		}
		return hlcell;
    }
    return nil;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
	if (tableView == MenutableView)
    {
        
        NSDictionary *dictionary = [listOfContents objectAtIndex:indexPath.row];
        
        cont_id = [dictionary objectForKey:@"id"];
        if (appdelegate.showloading) {
            alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"loading", nil) message:nil delegate:self cancelButtonTitle:nil otherButtonTitles: nil];
            
            //a simple activity indicator:
            UIActivityIndicatorView *activityIndicator = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
            activityIndicator.frame= CGRectMake(50, 10, 37, 37);
            activityIndicator.autoresizingMask = UIViewAutoresizingFlexibleTopMargin | UIViewAutoresizingFlexibleRightMargin | UIViewAutoresizingFlexibleBottomMargin | UIViewAutoresizingFlexibleLeftMargin;
            [activityIndicator startAnimating];
            
            //the magic line below,
            //we associate the activity indicator to the alert view: (addSubview is not used)
            [alert setValue:activityIndicator forKey:@"accessoryView"];
            
            [alert show];
        
            // Adjust the indicator so it is up a few pixels from the bottom of the alert
            [UIView beginAnimations:nil context:nil];
            [UIView setAnimationDuration:0.0];
            [UIView setAnimationDelay:0.0];
            [UIView setAnimationTransition:UIViewAnimationTransitionNone forView:alert cache:YES];
            [UIView setAnimationDelegate:self];
            [UIView setAnimationDidStopSelector:@selector(startupAnimationDoneIndicator:finished:context:)];
            alert.alpha = 0.5;
            [UIView commitAnimations];
            
        } else {
            [self goNextPage];
        }
        
    }
}

-(IBAction)buttonPressed:(id)sender {
    
    int tagId = [sender tag];
    int divNum = 0;
    if(tagId<100)
        divNum=10;
    else
        divNum=100;
    int section = [sender tag]/divNum;
    section -=1; // we had incremented at tag assigning time
    int itemId = [sender tag]%divNum;
    
    NSMutableArray *sectionItems = [sections objectAtIndex:section];
    Item *item = [sectionItems objectAtIndex:itemId];
    
    cont_id = item.contid;
    
    if (appdelegate.showloading) {
        alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"loading", nil) message:nil delegate:self cancelButtonTitle:nil otherButtonTitles: nil];
        
        //a simple activity indicator:
        UIActivityIndicatorView *activityIndicator = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
        activityIndicator.frame= CGRectMake(50, 10, 37, 37);
        activityIndicator.autoresizingMask = UIViewAutoresizingFlexibleTopMargin | UIViewAutoresizingFlexibleRightMargin | UIViewAutoresizingFlexibleBottomMargin | UIViewAutoresizingFlexibleLeftMargin;
        [activityIndicator startAnimating];
        
        //the magic line below,
        //we associate the activity indicator to the alert view: (addSubview is not used)
        [alert setValue:activityIndicator forKey:@"accessoryView"];
        
        [alert show];
    
        // Adjust the indicator so it is up a few pixels from the bottom of the alert
        [UIView beginAnimations:nil context:nil];
        [UIView setAnimationDuration:0.0];
        [UIView setAnimationDelay:0.0];
        [UIView setAnimationTransition:UIViewAnimationTransitionNone forView:alert cache:YES];
        [UIView setAnimationDelegate:self];
        [UIView setAnimationDidStopSelector:@selector(startupAnimationDoneIndicator:finished:context:)];
        alert.alpha = 0.5;
        [UIView commitAnimations];
    } else {
        [self goNextPage];
    }
    
}

-(void) dissmissIndicator {
    [alert dismissWithClickedButtonIndex:0 animated:NO];
}

-(void) goNextPage {
    ContentPluginDetailsViewController *contentPluginDetailsViewController = [[ContentPluginDetailsViewController alloc] init];
    contentPluginDetailsViewController.contId = cont_id;
    contentPluginDetailsViewController.conttitle = cont_title;
    contentPluginDetailsViewController.listtemplate = list_template;
    [appdelegate.navController pushViewController:contentPluginDetailsViewController animated:NO];
}

- (void)startupAnimationDoneIndicator:(NSString *)animationID finished:(NSNumber *)finished context:(void *)context
{
    [self goNextPage];
    [self dissmissIndicator];
    alert =nil;
}

- (void) processdata {
    [txt_search resignFirstResponder];
    //AppDelegate *appdelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    //self.title = appdelegate.iconname;
    
    [listOfContents removeAllObjects];
    [MenutableView removeFromSuperview];
    sqlite3 *database;
    
    if(sqlite3_open([appdelegate.databasePath UTF8String], &database) == SQLITE_OK) {
        
        NSString *strsym = @"%";
        NSString *textsearch = [[NSString alloc] initWithFormat:@"%@%@%@", strsym, txt_search.text, strsym];
        
        NSString *theme_object = [[NSString alloc] initWithFormat:@"SELECT * FROM contents WHERE (title like '%@' OR var1 like '%@' OR var2 like '%@' OR var3 like '%@' OR var4 like '%@' OR var5 like '%@') AND user_id = '%@' ORDER BY orderno", textsearch, textsearch, textsearch, textsearch, textsearch, textsearch, appdelegate.user];
        //NSLog(@"theme_object_plugin:: %@", theme_object);
        
        NSString *cid;
        NSString *ctitle;
        NSString *cimage;
        NSString *cthumbnail;
        NSString *cpricetitle;
        NSString *cprice;
        NSString *cvar1;
        const char *sqlStatement3 = [theme_object UTF8String];
        sqlite3_stmt *compiledStatement3;
        if(sqlite3_prepare_v2(database, sqlStatement3, -1, &compiledStatement3, NULL) == SQLITE_OK) {
            while(sqlite3_step(compiledStatement3) == SQLITE_ROW) {
                
                cid = [NSString stringWithUTF8String:(char *)sqlite3_column_text(compiledStatement3, 0)];
                ctitle = [NSString stringWithUTF8String:(char *)sqlite3_column_text(compiledStatement3, 1)];
                cimage = [NSString stringWithUTF8String:(char *)sqlite3_column_text(compiledStatement3, 3)];
                cthumbnail = [NSString stringWithUTF8String:(char *)sqlite3_column_text(compiledStatement3, 4)];
                cpricetitle = [NSString stringWithUTF8String:(char *)sqlite3_column_text(compiledStatement3, 23)];
                cprice = [NSString stringWithUTF8String:(char *)sqlite3_column_text(compiledStatement3, 24)];
                cvar1 = [NSString stringWithUTF8String:(char *)sqlite3_column_text(compiledStatement3, 26)];
                
                ctitle = [ctitle stringByDecodingHTMLEntities];
                cpricetitle = [cpricetitle stringByDecodingHTMLEntities];
                cvar1 = [cvar1 stringByDecodingHTMLEntities];
                ctitle = [ctitle stringByReplacingOccurrencesOfString:@"''" withString:@"'"];
                cpricetitle = [cpricetitle stringByReplacingOccurrencesOfString:@"''" withString:@"'"];
                cvar1 = [cvar1 stringByReplacingOccurrencesOfString:@"''" withString:@"'"];
                
                dicContents = [NSDictionary dictionaryWithObjectsAndKeys:cid,@"id",ctitle,@"title",cimage,@"image",cthumbnail,@"thumbnail",cpricetitle,@"price_title",cprice,@"price",cvar1,@"var1",nil];
                [listOfContents addObject:dicContents];
                
                //NSLog(@"listOfContents::%@", listOfContents);
                
            }
        }
                
        if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone) {
            // For iPhone
            if ([listOfContents count]*70 > self.view.frame.size.height-60) {
                MenutableView = [[UITableView alloc] initWithFrame:CGRectMake(0, 60, self.view.frame.size.width, self.view.frame.size.height-60)];
            } else {
                MenutableView = [[UITableView alloc] initWithFrame:CGRectMake(0, 60, self.view.frame.size.width, [listOfContents count]*70)];
            }
        } else {
            if ([listOfContents count]*110 > self.view.frame.size.height-60) {
                MenutableView = [[UITableView alloc] initWithFrame:CGRectMake(0, 60, self.view.frame.size.width, self.view.frame.size.height-60)];
            } else {
                MenutableView = [[UITableView alloc] initWithFrame:CGRectMake(0, 60, self.view.frame.size.width, [listOfContents count]*110)];
            }
        }
        MenutableView.autoresizingMask = UIViewAutoresizingFlexibleHeight | UIViewAutoresizingFlexibleWidth;
        MenutableView.delegate = self;
        MenutableView.dataSource = self;
        
        [self.view addSubview:MenutableView];
        
        
        cid =nil;
        ctitle =nil;
        cimage =nil;
        cthumbnail =nil;
    }
    sqlite3_close(database);
}

-(void) checkButtonClick:(id)sender {
    
    UIButton *clicked = (UIButton *) sender;
    
    NSDictionary *dictionary = [listOfContents objectAtIndex:clicked.tag];
    
    cont_id = [dictionary objectForKey:@"id"];
    
    if (appdelegate.showloading) {
        alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"loading", nil) message:nil delegate:self cancelButtonTitle:nil otherButtonTitles: nil];
        
        //a simple activity indicator:
        UIActivityIndicatorView *activityIndicator = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
        activityIndicator.frame= CGRectMake(50, 10, 37, 37);
        activityIndicator.autoresizingMask = UIViewAutoresizingFlexibleTopMargin | UIViewAutoresizingFlexibleRightMargin | UIViewAutoresizingFlexibleBottomMargin | UIViewAutoresizingFlexibleLeftMargin;
        [activityIndicator startAnimating];
        
        //the magic line below,
        //we associate the activity indicator to the alert view: (addSubview is not used)
        [alert setValue:activityIndicator forKey:@"accessoryView"];
        
        [alert show];
    
        // Adjust the indicator so it is up a few pixels from the bottom of the alert
        [UIView beginAnimations:nil context:nil];
        [UIView setAnimationDuration:0.0];
        [UIView setAnimationDelay:0.0];
        [UIView setAnimationTransition:UIViewAnimationTransitionNone forView:alert cache:YES];
        [UIView setAnimationDelegate:self];
        [UIView setAnimationDidStopSelector:@selector(startupAnimationDoneIndicator:finished:context:)];
        alert.alpha = 0.5;
        [UIView commitAnimations];
    } else {
        [self goNextPage];
    }
}

-(BOOL) textFieldShouldReturn:(UITextField *)textField{
    [textField resignFirstResponder];
    return YES;
}

-(void) viewDidDisappear:(BOOL)animated {
    
    [super viewDidDisappear:animated];
    
    appdelegate.homeclicked = NO;
    [modalController dismissViewControllerAnimated:YES completion:nil];
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
