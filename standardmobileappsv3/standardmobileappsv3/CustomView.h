//
//  CustomView.h
//  standardmobileappsv3
//
//  Created by M3Online on 8/5/14.
//  Copyright (c) 2014 M3Online. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MessageUI/MessageUI.h>
#import <SDWebImage/UIButton+WebCache.h>
#import <SDWebImage/UIImageView+WebCache.h>

@protocol CustomViewDelegate <NSObject>

@end

@interface CustomView : UIView <UITableViewDataSource, UITableViewDelegate,UIViewControllerTransitioningDelegate, MFMailComposeViewControllerDelegate, UIWebViewDelegate, MFMessageComposeViewControllerDelegate/*, NSURLConnectionDataDelegate*/> {
    NSMutableArray *galleryImages_;
    
    NSMutableArray *listOfObject;
    NSDictionary *dicImages;
    NSString *page_id;
    
    UIViewController *viewCtrl;
    
    IBOutlet UITableView *GridtableView;
    IBOutlet UITableView *GridVertableView;
    
    NSMutableArray *listOfMenu;
    NSDictionary *dicMenu;
    
    NSMutableArray *listOfVerMenu;
    NSDictionary *dicVerMenu;
    
    UIButton *btnMenuMA;
    UIButton *btnMenuMB;
    UIButton *btnMenuMC;
    UIButton *btnMenuMD;
    UIButton *btnMenuME;
    UIButton *btnMenuMF;
    
    UIWebView *myWebView;
    
    BOOL homeclicked;
    
    UIScrollView *scrollviewMain;
    
    BOOL firstcview;
}

@property (assign) id<CustomViewDelegate> delegate;

@property (nonatomic, strong) NSMutableArray *listOfObject;
@property (nonatomic, strong) NSString *page_id;
@property (nonatomic, strong) UIViewController *viewCtrl;
@property (nonatomic, strong) UITableView *GridtableView;

@property(nonatomic, strong)NSMutableArray *galleryImages; //Array holding the image file paths

@property(nonatomic, strong) UIScrollView *myWebScrollView;

@property(nonatomic, assign) BOOL Menu;

@property (nonatomic, strong) NSMutableArray *listOfMenu;

@property(nonatomic, strong) UIButton *btnMenuMA;
@property(nonatomic, strong) UIButton *btnMenuMB;
@property(nonatomic, strong) UIButton *btnMenuMC;
@property(nonatomic, strong) UIButton *btnMenuMD;
@property(nonatomic, strong) UIButton *btnMenuME;
@property(nonatomic, strong) UIButton *btnMenuMF;

@property(nonatomic, strong) UIView *menuView;
@property(nonatomic, strong) UIScrollView *viewFeed;

@property(nonatomic, strong) UIWebView *myWebView;

@property (nonatomic, strong) NSMutableArray *listOfVerMenu;

@property(nonatomic, assign) BOOL homeclicked;

@property (nonatomic, strong) UIScrollView *scrollviewMain;

@property(nonatomic, assign) BOOL firstcview;

- (void)setupScrollView:(UIScrollView*)scrMain ;
- (id)initWithFrame:(CGRect)frame withArray:(NSMutableArray *)listOfObject withViewController:(UIViewController *)vc withArrayMenu:(NSMutableArray *)listOfVerMenu1 withString:(NSString *)themepagenotification withBool:(BOOL)homeclicked1 withfirst:(BOOL)firstcview1; /*** Push Notification 27Aug2015 ***/

@end
