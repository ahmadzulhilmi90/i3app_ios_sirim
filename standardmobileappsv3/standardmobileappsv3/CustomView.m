//
//  CustomView.m
//  standardmobileappsv3
//
//  Created by M3Online on 8/5/14.
//  Copyright (c) 2014 M3Online. All rights reserved.
//

#import "CustomView.h"
#import "AppDelegate.h"
#import "ViewController.h"
#import "ThemeViewController.h"
#import "BasicPluginViewController.h"
#import "ContentPluginViewController.h"
#import "ContentPluginDetailsViewController.h"
#import "LoyaltyViewController.h"
#import "WarrantyViewController.h"
#import "ContentSearchViewController.h"
#import "NSString+HTML.h"
#import "SettingViewController.h"
#import "BookingListViewController.h"
#import "ScorePointViewController.h"
#import "ContentCatPluginViewController.h"
#import "InboxViewController.h"
#import "ContentProductCatPluginViewController.h"
#import "NotesViewController.h"
#import "EventPluginViewController.h"
#import "RsvpViewController.h"
#import "SIRIMLabelViewController.h"
#import "SIRIMPCIViewController.h"

@implementation CustomView
UIAlertView *alert;
UIActivityIndicatorView *indicator;
UIActivityIndicatorView *processIndicator;
@synthesize listOfObject ,page_id, viewCtrl, GridtableView, viewFeed, myWebView, homeclicked, scrollviewMain, firstcview;
@synthesize galleryImages = galleryImages_;
@synthesize btnMenuMA, btnMenuMB, btnMenuMC, btnMenuMD, btnMenuME, btnMenuMF, Menu, listOfMenu, menuView, listOfVerMenu;

float delaytime;

__strong NSString *ios_link;

__strong NSString *themecode;
__strong NSString *page_id;
__strong NSString *page_name;
__strong NSString *prevpage;
__strong NSString *aplugintype;
__strong NSString *apluginvalue1;
__strong NSString *apluginvalue2;
__strong NSString *apluginvalue3;
__strong NSString *aid;

AppDelegate *appdelegate;

- (id)initWithFrame:(CGRect)frame withArray:(NSMutableArray *)arrayObject withViewController:(UIViewController *)vc withArrayMenu:(NSMutableArray *)listOfVerMenu1 withString:(NSString *)themepagenotification withBool:(BOOL)homeclicked1 withfirst:(BOOL)firstcview1 /*** Push Notification 27Aug2015 ***/
{
    self = [super initWithFrame:frame];
    //NSLog(@"frame:: %f", frame.size.height);
    if (self) {
        // Initialization code
        appdelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
        
        Menu = FALSE;
        btnMenuMA = [UIButton buttonWithType:UIButtonTypeCustom];
        btnMenuMA.hidden = YES;
        btnMenuMB = [UIButton buttonWithType:UIButtonTypeCustom];
        btnMenuMB.hidden = YES;
        btnMenuMC = [UIButton buttonWithType:UIButtonTypeCustom];
        btnMenuMC.hidden = YES;
        btnMenuMD = [UIButton buttonWithType:UIButtonTypeCustom];
        btnMenuMD.hidden = YES;
        btnMenuME = [UIButton buttonWithType:UIButtonTypeCustom];
        btnMenuME.hidden = YES;
        btnMenuMF = [UIButton buttonWithType:UIButtonTypeCustom];
        btnMenuMF.hidden = YES;
        
        listOfObject = arrayObject;
        viewCtrl = vc;
        listOfVerMenu = listOfVerMenu1;
        homeclicked = homeclicked1;
        firstcview = firstcview1;
        
        if ([listOfObject count]>0) {
            NSDictionary *pageid = [listOfObject objectAtIndex:0];
            page_id = [pageid objectForKey:@"page_id"];
        }
        
        //NSLog(@"listOfObject:%@", listOfObject);
        //NSLog(@"listOfVerMenu:%@", listOfVerMenu);
        
        sqlite3 *database;
        NSString *bgurl = @"";
        
        if(sqlite3_open([appdelegate.databasePath UTF8String], &database) == SQLITE_OK) {
            // Setup the SQL Statement and compile it for faster access
            
            NSString *theme_object = [[NSString alloc] initWithFormat:@"Select object_value from theme_object WHERE page_id=%@ and slot_code='BG' and user_id=%@", page_id, appdelegate.user];
            //NSLog(@"theme_object:: %@", theme_object);
            const char *sqlStatement1 = [theme_object UTF8String];
            sqlite3_stmt *compiledStatement1;
            if(sqlite3_prepare_v2(database, sqlStatement1, -1, &compiledStatement1, NULL) == SQLITE_OK) {
                
                while(sqlite3_step(compiledStatement1) == SQLITE_ROW) {
                    
                    bgurl = [NSString stringWithUTF8String:(char *)sqlite3_column_text(compiledStatement1, 0)];
                    
                }
            }
            // Release the compiled statement from memory
            sqlite3_finalize(compiledStatement1);
            
            NSString *menu_object = [[NSString alloc] initWithFormat:@"SELECT * from theme_object WHERE theme_code = 'MENU' and object_type<>'0' Order by slot_code"];
            //NSLog(@"menu_object:: %@", menu_object);
            const char *sqlStatementMenu = [menu_object UTF8String];
            sqlite3_stmt *compiledStatementMenu;
            if(sqlite3_prepare_v2(database, sqlStatementMenu, -1, &compiledStatementMenu, NULL) == SQLITE_OK) {
                
                listOfMenu = [[NSMutableArray alloc] init];
                while(sqlite3_step(compiledStatementMenu) == SQLITE_ROW) {
                    
                    NSString *aidobj = [NSString stringWithUTF8String:(char *)sqlite3_column_text(compiledStatementMenu, 0)];
                    NSString *athemecode = [NSString stringWithUTF8String:(char *)sqlite3_column_text(compiledStatementMenu, 1)];
                    NSString *aslotcode = [NSString stringWithUTF8String:(char *)sqlite3_column_text(compiledStatementMenu, 2)];
                    NSString *apageid = [NSString stringWithUTF8String:(char *)sqlite3_column_text(compiledStatementMenu, 3)];
                    NSString *aobjcttype = [NSString stringWithUTF8String:(char *)sqlite3_column_text(compiledStatementMenu, 4)];
                    NSString *aobjectvalue = [NSString stringWithUTF8String:(char *)sqlite3_column_text(compiledStatementMenu, 5)];
                    NSString *aattribute = [NSString stringWithUTF8String:(char *)sqlite3_column_text(compiledStatementMenu, 6)];
                    NSString *aactiontype = [NSString stringWithUTF8String:(char *)sqlite3_column_text(compiledStatementMenu, 7)];
                    NSString *aactionvalue = [NSString stringWithUTF8String:(char *)sqlite3_column_text(compiledStatementMenu, 8)];
                    NSString *aremark = [NSString stringWithUTF8String:(char *)sqlite3_column_text(compiledStatementMenu, 10)];
                    
                    aobjectvalue = [aobjectvalue stringByDecodingHTMLEntities];
                    aremark = [aremark stringByDecodingHTMLEntities];
                    aobjectvalue = [aobjectvalue stringByReplacingOccurrencesOfString:@"''" withString:@"'"];
                    aremark = [aremark stringByReplacingOccurrencesOfString:@"''" withString:@"'"];
                    
                    /*NSLog(@"aidobj:: %@", aidobj);
                    NSLog(@"athemecode:: %@", athemecode);
                    NSLog(@"aslotcode:: %@", aslotcode);
                    NSLog(@"apageid:: %@", apageid);
                    NSLog(@"aobjcttype:: %@", aobjcttype);
                    NSLog(@"aobjectvalue:: %@", aobjectvalue);
                    NSLog(@"aattribute:: %@", aattribute);
                    NSLog(@"aactiontype:: %@", aactiontype);
                    NSLog(@"aactionvalue:: %@", aactionvalue);
                    NSLog(@"aremark:: %@", aremark); */
                    
                    dicMenu = [NSDictionary dictionaryWithObjectsAndKeys:aidobj,@"id",athemecode,@"theme_code",aslotcode,@"slot_code",apageid,@"page_id",aobjcttype,@"object_type",aobjectvalue,@"object_value",aattribute,@"attribute",aactiontype,@"action_type",aactionvalue,@"action_value",aremark,@"remark",nil];
                    [listOfMenu addObject:dicMenu];
                    
                    aidobj =nil;
                    athemecode =nil;
                    aslotcode =nil;
                    apageid =nil;
                    aobjcttype =nil;
                    aobjectvalue =nil;
                    aattribute =nil;
                    aactiontype =nil;
                    aactionvalue =nil;
                    aremark =nil;
                    
                }
                
                //NSLog(@"listOfMenu:: %@", listOfMenu);
            }
            // Release the compiled statement from memory
            sqlite3_finalize(compiledStatementMenu);
            
            NSString *menuchk_object = [[NSString alloc] initWithFormat:@"Select * from theme_object WHERE page_id=%@ and slot_code='MENU' and user_id=%@ and object_value='1' ", page_id, appdelegate.user];
            //NSLog(@"menuchk_object:: %@", menuchk_object);
            const char *sqlStatementMenuChk = [menuchk_object UTF8String];
            sqlite3_stmt *compiledStatementMenuChk;
            if(sqlite3_prepare_v2(database, sqlStatementMenuChk, -1, &compiledStatementMenuChk, NULL) == SQLITE_OK) {
                
                while(sqlite3_step(compiledStatementMenuChk) == SQLITE_ROW) {
                    //NSLog(@"menuchk11");
                    Menu = TRUE;
                    
                }
            }
            // Release the compiled statement from memory
            sqlite3_finalize(compiledStatementMenuChk);
            
        }
        sqlite3_close(database);
        //NSLog(@"bgurl:: %@", bgurl);
        if ([bgurl isEqualToString:@""]) {
            //self.backgroundColor = [self colorWithHexString:appdelegate.themeColorCode1];
            //NSLog(@"appdelegate.backgroundT : %@", appdelegate.backgroundT);
            if (![appdelegate.backgroundT isEqualToString:@""]) {
                
                NSString *filePath = appdelegate.backgroundT;
                
                NSRange end = [filePath rangeOfString:@"/" options:NSBackwardsSearch];
                NSString *shortString;
                if (end.location != NSNotFound) {
                    shortString =[filePath substringWithRange:NSMakeRange(end.location+1, filePath.length-(end.location+1))];
                } else {
                    shortString = filePath;
                }
                
                BOOL success;
                
                // Create a FileManager object, we will use this to check the status
                // of the database and to copy it over if required
                NSFileManager *fileManager = [NSFileManager defaultManager];
                
                // Check if the database has already been created in the users filesystem
                success = [fileManager fileExistsAtPath:[[[NSBundle mainBundle] resourcePath] stringByAppendingPathComponent:shortString]];
                
                UIImageView *imgV = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, self.frame.size.width, self.frame.size.height)];
                //[self addSubview:imgV];
                
                //__block UIActivityIndicatorView *activityIndicator;
                __weak UIImageView *weakImageView = imgV;
                NSString *imagePath;
                if(success) {
                    imagePath = [[[NSBundle mainBundle] resourcePath] stringByAppendingPathComponent:shortString];
                    [weakImageView setImage:[UIImage imageNamed:shortString]];
                    
                    //NSLog(@"shortStringimagePath : %@", imagePath);
                    imgV.contentMode  = UIViewContentModeScaleAspectFit;
                    [imgV setClipsToBounds:YES];
                    
                    UIGraphicsBeginImageContext(self.viewCtrl.view.frame.size);
                    CGRect rect=CGRectMake(0,-64,self.viewCtrl.view.bounds.size.width,self.viewCtrl.view.bounds.size.height+64);
                    [imgV.image drawInRect:rect];
                    UIImage *image1 = UIGraphicsGetImageFromCurrentImageContext();
                    UIGraphicsEndImageContext();
                    self.backgroundColor = [UIColor colorWithPatternImage:image1];
                } else {
                    imagePath = filePath;
                    NSURL *imageURL = [NSURL URLWithString:imagePath];
                    NSData *imageData = [NSData dataWithContentsOfURL:imageURL];
                    //self.backgroundColor = [UIColor redColor];
                    UIGraphicsBeginImageContext(self.viewCtrl.view.frame.size);
                    CGRect rect=CGRectMake(0,-64,self.viewCtrl.view.bounds.size.width,self.viewCtrl.view.bounds.size.height+64);
                    [[UIImage imageWithData:imageData] drawInRect:rect];
                    UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
                    UIGraphicsEndImageContext();
                    self.backgroundColor = [UIColor colorWithPatternImage:image];
                }
            } else {
                self.backgroundColor = [UIColor clearColor];
            }
        } else {
            //NSURL *imageURL = [NSURL URLWithString:bgurl];
            //NSData *imageData = [NSData dataWithContentsOfURL:imageURL];
            //self.backgroundColor = [UIColor redColor];
            NSRange end = [bgurl rangeOfString:@"/" options:NSBackwardsSearch];
            NSString *shortString;
            if (end.location != NSNotFound) {
                shortString =[bgurl substringWithRange:NSMakeRange(end.location+1, bgurl.length-(end.location+1))];
            } else {
                shortString = bgurl;
            }
            
            BOOL success;
            
            // Create a FileManager object, we will use this to check the status
            // of the database and to copy it over if required
            NSFileManager *fileManager = [NSFileManager defaultManager];
            
            // Check if the database has already been created in the users filesystem
            success = [fileManager fileExistsAtPath:[[[NSBundle mainBundle] resourcePath] stringByAppendingPathComponent:shortString]];
            //NSLog(@"shortString %@", shortString);
            if ([bgurl length]>0)
            {
                //[buttonName setBackgroundImageWithURL:[NSURL URLWithString:imgURL] forState:UIControlStateNormal placeholderImage:[UIImage imageNamed:@"No-Image-icon.png"]];
                UIImageView *imgV = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, self.frame.size.width, self.frame.size.height)];
                
                __block UIActivityIndicatorView *activityIndicator;
                __weak UIImageView *weakImageView = imgV;
                NSString *imagePath;
                if(success) {
                    imagePath = [[[NSBundle mainBundle] resourcePath] stringByAppendingPathComponent:shortString];
                    [weakImageView setImage:[UIImage imageNamed:shortString]];
                } else {
                    imagePath = bgurl;
                    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0), ^ {
                        [weakImageView sd_setImageWithURL:[NSURL URLWithString:imagePath]
                                         placeholderImage:nil
                                                  options:SDWebImageProgressiveDownload
                                                 progress:^(NSInteger receivedSize, NSInteger expectedSize, NSURL *targetURL) {
                                                     dispatch_async(dispatch_get_main_queue(), ^ {
                                                         if (!activityIndicator) {
                                                             
                                                             [weakImageView addSubview:activityIndicator = [UIActivityIndicatorView.alloc initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray]];
                                                             activityIndicator.center = weakImageView.center;
                                                             [activityIndicator startAnimating];
                                                         }
                                                     });
                                                 }
                                                completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
                                                    dispatch_async(dispatch_get_main_queue(), ^ {
                                                        //[MBProgressHUD hideHUDForView:self.view animated:YES];
                                                        [activityIndicator removeFromSuperview];
                                                        activityIndicator = nil;
                                                    });
                                                }];
                    });
                    
                }
                //NSLog(@"shortStringimagePath : %@", imagePath);
                [viewCtrl.view addSubview:weakImageView];
                imgV.contentMode  = UIViewContentModeScaleAspectFill;
                [imgV setClipsToBounds:YES];
            }
            else
            {
                viewCtrl.view.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"No-Image-icon.png"]];
            }
            /*UIGraphicsBeginImageContext(self.viewCtrl.view.frame.size);
            CGRect rect=CGRectMake(0,-64,self.viewCtrl.view.bounds.size.width,self.viewCtrl.view.bounds.size.height+64);
            [[UIImage imageWithData:imageData] drawInRect:rect];
            UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
            UIGraphicsEndImageContext();
            self.backgroundColor = [UIColor colorWithPatternImage:image]; */
        }
        
        bgurl =nil;
        
        if ([listOfObject count]>0) {
            NSDictionary *themecode = [listOfObject objectAtIndex:0];
            
            NSString *slotGallery = @"";
            sqlite3 *database;
            if(sqlite3_open([appdelegate.databasePath UTF8String], &database) == SQLITE_OK) {
                NSString *theme_objectSlot = [[NSString alloc] initWithFormat:@"Select slot_code, count(slot_code) from theme_object where page_id=%@ and theme_code='%@' and user_id=%@ group by slot_code having count(slot_code) > 1 order by slot_code Desc", page_id, [themecode objectForKey:@"theme_code"], appdelegate.user];
                //NSLog(@"theme_objectSlot:: %@", theme_objectSlot);
                const char *sqlStatement2 = [theme_objectSlot UTF8String];
                sqlite3_stmt *compiledStatement2;
                if(sqlite3_prepare_v2(database, sqlStatement2, -1, &compiledStatement2, NULL) == SQLITE_OK) {
                    
                    while(sqlite3_step(compiledStatement2) == SQLITE_ROW) {
                        
                        slotGallery = [NSString stringWithUTF8String:(char *)sqlite3_column_text(compiledStatement2, 0)];
                        
                    }
                }
                // Release the compiled statement from memory
                sqlite3_finalize(compiledStatement2);
            }
            sqlite3_close(database);
            
            if (![slotGallery isEqualToString:@""]) {
                self.galleryImages = [[NSMutableArray alloc] init];
            }
            
            //NSLog(@"slotGallery: %@", slotGallery);
            [scrollviewMain removeFromSuperview];
            if (firstcview==YES) {
                scrollviewMain = [[UIScrollView alloc] initWithFrame:CGRectMake(0, 0, self.frame.size.width, self.frame.size.height-64)];
            } else {
                scrollviewMain = [[UIScrollView alloc] initWithFrame:CGRectMake(0, 0, self.frame.size.width, self.frame.size.height)];
            }
            scrollviewMain.bounces = NO;
            
            UIScrollView *scrollview = [[UIScrollView alloc] initWithFrame:CGRectMake(0, 0, 0, 0)];
            scrollview.bounces = NO;
            
            //scrollview.contentSize = CGSizeMake(scrollview.frame.size.height/4*7, scrollview.frame.size.height);
            //scrollview.pagingEnabled=YES;
            
            [scrollviewMain scrollRectToVisible:self.frame animated:TRUE];
            [scrollviewMain setBackgroundColor:[UIColor clearColor]];
            [self addSubview:scrollviewMain];
            
            [scrollview setBackgroundColor:[UIColor clearColor]];
            [scrollviewMain addSubview:scrollview];
            
            if ([listOfObject count]>0) {
                int i;
                int j=0;
                for (i=0; i<[listOfObject count]; i++) {
                    NSDictionary *dictionary = [listOfObject objectAtIndex:i];
                    
                    //All the themes layout: Theme 1 - 20
                    if (![[dictionary objectForKey:@"theme_code"] isEqualToString:@"0"]) {
                        CGRect slotFrame;
                        if ([[themecode objectForKey:@"theme_code"] isEqualToString:@"1"]) {
                            slotFrame = CGRectMake(0, 0, scrollviewMain.frame.size.width, scrollviewMain.frame.size.width*870/540);
                            
                            scrollviewMain.contentSize = CGSizeMake(scrollviewMain.frame.size.width, scrollviewMain.frame.size.width*870/540);
                            
                        } else if ([[themecode objectForKey:@"theme_code"] isEqualToString:@"2"]) {
                            if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone) {
                                // For iPhone
                                if ([[dictionary objectForKey:@"slot_code"] isEqualToString:@"2A"]) {
                                    slotFrame = CGRectMake(0, 0, scrollviewMain.frame.size.width, scrollviewMain.frame.size.width*762/540);
                                } else {
                                    slotFrame = CGRectMake(scrollviewMain.frame.size.width/5*j, scrollviewMain.frame.size.width*762/540, scrollviewMain.frame.size.width/5, scrollviewMain.frame.size.width/5);
                                    j++;
                                }
                                
                                scrollviewMain.contentSize = CGSizeMake(scrollviewMain.frame.size.width, scrollviewMain.frame.size.width*870/540);
                            } else {
                                if ([[dictionary objectForKey:@"slot_code"] isEqualToString:@"2A"]) {
                                    slotFrame = CGRectMake(0, 0, scrollviewMain.frame.size.width, scrollviewMain.frame.size.height-scrollviewMain.frame.size.width/5);
                                } else {
                                    slotFrame = CGRectMake(scrollviewMain.frame.size.width/5*j, scrollviewMain.frame.size.height-scrollviewMain.frame.size.width/5, scrollviewMain.frame.size.width/5, scrollviewMain.frame.size.width/5);
                                    j++;
                                }
                                scrollviewMain.contentSize = CGSizeMake(scrollviewMain.frame.size.width, scrollviewMain.frame.size.height);
                            }
                            
                        } else if ([[themecode objectForKey:@"theme_code"] isEqualToString:@"3"]) {
                            //if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone) {
                                // For iPhone
                                if ([listOfObject count]*scrollviewMain.frame.size.width*109/540 > viewCtrl.view.frame.size.height) {
                                    GridtableView = [[UITableView alloc] initWithFrame:CGRectMake(0, 0, viewCtrl.view.frame.size.width, viewCtrl.view.frame.size.height)];
                                } else {
                                    GridtableView = [[UITableView alloc] initWithFrame:CGRectMake(0, 0, viewCtrl.view.frame.size.width, [listOfObject count]*scrollviewMain.frame.size.width*109/540)];
                                }
                            //} else {
                                //if ([listOfObject count]*140 > viewCtrl.view.frame.size.height-64) {
                                    //GridtableView = [[UITableView alloc] initWithFrame:CGRectMake(0, 0, viewCtrl.view.frame.size.width, viewCtrl.view.frame.size.height)];
                                //} else {
                                    //GridtableView = [[UITableView alloc] initWithFrame:CGRectMake(0, 0, viewCtrl.view.frame.size.width, [listOfObject count]*140)];
                                //}
                            //}
                            GridtableView.autoresizingMask = UIViewAutoresizingFlexibleHeight | UIViewAutoresizingFlexibleWidth;
                            GridtableView.delegate = self;
                            GridtableView.dataSource = self;
                            
                            [GridtableView setBackgroundColor:[UIColor clearColor]];
                            GridtableView.bounces = NO;
                            GridtableView.separatorStyle = UITableViewCellSeparatorStyleNone;

                            [scrollviewMain addSubview:GridtableView];
                            
                        } else if ([[themecode objectForKey:@"theme_code"] isEqualToString:@"4"]) {
                            scrollview.frame = CGRectMake(0, 0, scrollviewMain.frame.size.width, scrollviewMain.frame.size.height);
                            
                            if ([[dictionary objectForKey:@"slot_code"] isEqualToString:@"4A"]) {
                                slotFrame = CGRectMake(0, 0, scrollview.frame.size.height/4*2, scrollview.frame.size.height/4*2);
                            } else if ([[dictionary objectForKey:@"slot_code"] isEqualToString:@"4I"]) {
                                slotFrame = CGRectMake(0,  scrollview.frame.size.height/4*2, scrollview.frame.size.height/4, scrollview.frame.size.height/4);
                            } else if ([[dictionary objectForKey:@"slot_code"] isEqualToString:@"4J"]) {
                                slotFrame = CGRectMake(scrollview.frame.size.height/4, scrollview.frame.size.height/4*2, scrollview.frame.size.height/4, scrollview.frame.size.height/4);
                            } else if ([[dictionary objectForKey:@"slot_code"] isEqualToString:@"4L"]) {
                                slotFrame = CGRectMake(0, scrollview.frame.size.height/4*3, scrollview.frame.size.height/4*2, scrollview.frame.size.height/4);
                            } else if ([[dictionary objectForKey:@"slot_code"] isEqualToString:@"4B"]) {
                                slotFrame = CGRectMake(scrollview.frame.size.height/4*2, 0, scrollview.frame.size.height/4, scrollview.frame.size.height/4);
                            } else if ([[dictionary objectForKey:@"slot_code"] isEqualToString:@"4F"]) {
                                slotFrame = CGRectMake(scrollview.frame.size.height/4*2, scrollview.frame.size.height/4, scrollview.frame.size.height/4, scrollview.frame.size.height/4);
                            } else if ([[dictionary objectForKey:@"slot_code"] isEqualToString:@"4C"]) {
                                slotFrame = CGRectMake(scrollview.frame.size.height/4*3, 0, scrollview.frame.size.height/4, scrollview.frame.size.height/4*2);
                            } else if ([[dictionary objectForKey:@"slot_code"] isEqualToString:@"4K"]) {
                                slotFrame = CGRectMake(scrollview.frame.size.height/4*2, scrollview.frame.size.height/4*2, scrollview.frame.size.height/4*2, scrollview.frame.size.height/4*2);
                            } else if ([[dictionary objectForKey:@"slot_code"] isEqualToString:@"4D"]) {
                                slotFrame = CGRectMake(scrollview.frame.size.height/4*4, 0, scrollview.frame.size.height/4*2, scrollview.frame.size.height/4);
                            } else if ([[dictionary objectForKey:@"slot_code"] isEqualToString:@"4E"]) {
                                slotFrame = CGRectMake(scrollview.frame.size.height/4*6, 0, scrollview.frame.size.height/4, scrollview.frame.size.height/4);
                            } else if ([[dictionary objectForKey:@"slot_code"] isEqualToString:@"4G"]) {
                                slotFrame = CGRectMake(scrollview.frame.size.height/4*4, scrollview.frame.size.height/4, scrollview.frame.size.height/4, scrollview.frame.size.height/4*2);
                            } else if ([[dictionary objectForKey:@"slot_code"] isEqualToString:@"4H"]) {
                                slotFrame = CGRectMake(scrollview.frame.size.height/4*5, scrollview.frame.size.height/4, scrollview.frame.size.height/4*2, scrollview.frame.size.height/4*2);
                            } else if ([[dictionary objectForKey:@"slot_code"] isEqualToString:@"4M"]) {
                                slotFrame = CGRectMake(scrollview.frame.size.height/4*4, scrollview.frame.size.height/4*3, scrollview.frame.size.height/4*2, scrollview.frame.size.height/4);
                            } else if ([[dictionary objectForKey:@"slot_code"] isEqualToString:@"4N"]) {
                                slotFrame = CGRectMake(scrollview.frame.size.height/4*6, scrollview.frame.size.height/4*3, scrollview.frame.size.height/4, scrollview.frame.size.height/4);
                            }
                            
                            scrollviewMain.contentSize = CGSizeMake(scrollviewMain.frame.size.height/4*7, scrollviewMain.frame.size.height);
                            
                        } else if ([[themecode objectForKey:@"theme_code"] isEqualToString:@"5"]) {
                            if ([[dictionary objectForKey:@"slot_code"] isEqualToString:@"5A"]) {
                                slotFrame = CGRectMake(0, 0, scrollviewMain.frame.size.width, scrollviewMain.frame.size.width*330/540);
                            } else if ([[dictionary objectForKey:@"slot_code"] isEqualToString:@"5B"]) {
                                slotFrame = CGRectMake(0,  scrollviewMain.frame.size.width*330/540, scrollviewMain.frame.size.width/3, scrollviewMain.frame.size.width/3);
                            } else if ([[dictionary objectForKey:@"slot_code"] isEqualToString:@"5C"]) {
                                slotFrame = CGRectMake(scrollviewMain.frame.size.width/3, scrollviewMain.frame.size.width*330/540, scrollviewMain.frame.size.width/3, scrollviewMain.frame.size.width/3);
                            } else if ([[dictionary objectForKey:@"slot_code"] isEqualToString:@"5D"]) {
                                slotFrame = CGRectMake(scrollviewMain.frame.size.width/3*2, scrollviewMain.frame.size.width*330/540, scrollviewMain.frame.size.width/3, scrollviewMain.frame.size.width/3);
                            } else if ([[dictionary objectForKey:@"slot_code"] isEqualToString:@"5E"]) {
                                slotFrame = CGRectMake(0, scrollviewMain.frame.size.width*330/540+scrollviewMain.frame.size.width/3, scrollviewMain.frame.size.width/3, scrollviewMain.frame.size.width/3);
                            } else if ([[dictionary objectForKey:@"slot_code"] isEqualToString:@"5F"]) {
                                slotFrame = CGRectMake(scrollviewMain.frame.size.width/3, scrollviewMain.frame.size.width*330/540+scrollviewMain.frame.size.width/3, scrollviewMain.frame.size.width/3, scrollviewMain.frame.size.width/3);
                            } else if ([[dictionary objectForKey:@"slot_code"] isEqualToString:@"5G"]) {
                                slotFrame = CGRectMake(scrollviewMain.frame.size.width/3*2, scrollviewMain.frame.size.width*330/540+scrollviewMain.frame.size.width/3, scrollviewMain.frame.size.width/3, scrollviewMain.frame.size.width/3);
                            } else if ([[dictionary objectForKey:@"slot_code"] isEqualToString:@"5H"]) {
                                slotFrame = CGRectMake(0, scrollviewMain.frame.size.width*330/540+scrollviewMain.frame.size.width/3*2, scrollviewMain.frame.size.width/3, scrollviewMain.frame.size.width/3);
                            } else if ([[dictionary objectForKey:@"slot_code"] isEqualToString:@"5I"]) {
                                slotFrame = CGRectMake(scrollviewMain.frame.size.width/3, scrollviewMain.frame.size.width*330/540+scrollviewMain.frame.size.width/3*2, scrollviewMain.frame.size.width/3, scrollviewMain.frame.size.width/3);
                            } else if ([[dictionary objectForKey:@"slot_code"] isEqualToString:@"5J"]) {
                                slotFrame = CGRectMake(scrollviewMain.frame.size.width/3*2, scrollviewMain.frame.size.width*330/540+scrollviewMain.frame.size.width/3*2, scrollviewMain.frame.size.width/3, scrollviewMain.frame.size.width/3);
                            }
                            
                            scrollviewMain.contentSize = CGSizeMake(scrollviewMain.frame.size.width, (scrollviewMain.frame.size.width*330/540+scrollviewMain.frame.size.width/3*3));
                            
                        } else if ([[themecode objectForKey:@"theme_code"] isEqualToString:@"6"]) {
                            if ([[dictionary objectForKey:@"slot_code"] isEqualToString:@"6A"]) {
                                slotFrame = CGRectMake(0, 0, scrollviewMain.frame.size.width, scrollviewMain.frame.size.width*330/540);
                            } else if ([[dictionary objectForKey:@"slot_code"] isEqualToString:@"6B"]) {
                                slotFrame = CGRectMake(0, scrollviewMain.frame.size.width*330/540, scrollviewMain.frame.size.width/2, scrollviewMain.frame.size.width/2);
                            } else if ([[dictionary objectForKey:@"slot_code"] isEqualToString:@"6C"]) {
                                slotFrame = CGRectMake(scrollviewMain.frame.size.width/2, scrollviewMain.frame.size.width*330/540, scrollviewMain.frame.size.width/2, scrollviewMain.frame.size.width/2);
                            } else if ([[dictionary objectForKey:@"slot_code"] isEqualToString:@"6D"]) {
                                slotFrame = CGRectMake(0, scrollviewMain.frame.size.width*330/540+scrollviewMain.frame.size.width/2, scrollviewMain.frame.size.width/2, scrollviewMain.frame.size.width/2);
                            } else if ([[dictionary objectForKey:@"slot_code"] isEqualToString:@"6E"]) {
                                slotFrame = CGRectMake(scrollviewMain.frame.size.width/2, scrollviewMain.frame.size.width*330/540+scrollviewMain.frame.size.width/2, scrollviewMain.frame.size.width/2, scrollviewMain.frame.size.width/2);
                            }
                            
                            scrollviewMain.contentSize = CGSizeMake(scrollviewMain.frame.size.width, (scrollviewMain.frame.size.width*330/540+scrollviewMain.frame.size.width/2*2));
                            
                        } else if ([[themecode objectForKey:@"theme_code"] isEqualToString:@"7"]) {
                            if ([[dictionary objectForKey:@"slot_code"] isEqualToString:@"7D"]) {
                                slotFrame = CGRectMake(0, scrollviewMain.frame.size.width/3, scrollviewMain.frame.size.width, scrollviewMain.frame.size.width*510/540);
                            } else if ([[dictionary objectForKey:@"slot_code"] isEqualToString:@"7A"]) {
                                slotFrame = CGRectMake(0, 0, scrollviewMain.frame.size.width/3, scrollviewMain.frame.size.width/3);
                            } else if ([[dictionary objectForKey:@"slot_code"] isEqualToString:@"7B"]) {
                                slotFrame = CGRectMake(scrollviewMain.frame.size.width/3, 0, scrollviewMain.frame.size.width/3, scrollviewMain.frame.size.width/3);
                            } else if ([[dictionary objectForKey:@"slot_code"] isEqualToString:@"7C"]) {
                                slotFrame = CGRectMake(scrollviewMain.frame.size.width/3*2, 0, scrollviewMain.frame.size.width/3, scrollviewMain.frame.size.width/3);
                            } else if ([[dictionary objectForKey:@"slot_code"] isEqualToString:@"7E"]) {
                                slotFrame = CGRectMake(0, scrollviewMain.frame.size.width/3+scrollviewMain.frame.size.width*510/540, scrollviewMain.frame.size.width/3, scrollviewMain.frame.size.width/3);
                            } else if ([[dictionary objectForKey:@"slot_code"] isEqualToString:@"7F"]) {
                                slotFrame = CGRectMake(scrollviewMain.frame.size.width/3, scrollviewMain.frame.size.width/3+scrollviewMain.frame.size.width*510/540, scrollviewMain.frame.size.width/3, scrollviewMain.frame.size.width/3);
                            } else if ([[dictionary objectForKey:@"slot_code"] isEqualToString:@"7G"]) {
                                slotFrame = CGRectMake(scrollviewMain.frame.size.width/3*2, scrollviewMain.frame.size.width/3+scrollviewMain.frame.size.width*510/540, scrollviewMain.frame.size.width/3, scrollviewMain.frame.size.width/3);
                            }
                            
                            scrollviewMain.contentSize = CGSizeMake(scrollviewMain.frame.size.width, (scrollviewMain.frame.size.width/3*2+scrollviewMain.frame.size.width*510/540));
                            
                        } else if ([[themecode objectForKey:@"theme_code"] isEqualToString:@"8"]) {
                            if ([[dictionary objectForKey:@"slot_code"] isEqualToString:@"8A"]) {
                                slotFrame = CGRectMake(0, 0, scrollviewMain.frame.size.width/2, scrollviewMain.frame.size.width*217/540);
                            } else if ([[dictionary objectForKey:@"slot_code"] isEqualToString:@"8B"]) {
                                slotFrame = CGRectMake(scrollviewMain.frame.size.width/2, 0, scrollviewMain.frame.size.width/2, scrollviewMain.frame.size.width*217/540);
                            } else if ([[dictionary objectForKey:@"slot_code"] isEqualToString:@"8C"]) {
                                slotFrame = CGRectMake(0, scrollviewMain.frame.size.width*217/540, scrollviewMain.frame.size.width/2, scrollviewMain.frame.size.width*217/540);
                            } else if ([[dictionary objectForKey:@"slot_code"] isEqualToString:@"8D"]) {
                                slotFrame = CGRectMake(scrollviewMain.frame.size.width/2, scrollviewMain.frame.size.width*217/540, scrollviewMain.frame.size.width/2, scrollviewMain.frame.size.width*217/540);
                            } else if ([[dictionary objectForKey:@"slot_code"] isEqualToString:@"8E"]) {
                                slotFrame = CGRectMake(0, scrollviewMain.frame.size.width*217/540*2, scrollviewMain.frame.size.width/2, scrollviewMain.frame.size.width*217/540);
                            } else if ([[dictionary objectForKey:@"slot_code"] isEqualToString:@"8F"]) {
                                slotFrame = CGRectMake(scrollviewMain.frame.size.width/2, scrollviewMain.frame.size.width*217/540*2, scrollviewMain.frame.size.width/2, scrollviewMain.frame.size.width*217/540);
                            } else if ([[dictionary objectForKey:@"slot_code"] isEqualToString:@"8G"]) {
                                slotFrame = CGRectMake(0, scrollviewMain.frame.size.width*217/540*3, scrollviewMain.frame.size.width/2, scrollviewMain.frame.size.width*217/540);
                            } else if ([[dictionary objectForKey:@"slot_code"] isEqualToString:@"8H"]) {
                                slotFrame = CGRectMake(scrollviewMain.frame.size.width/2, scrollviewMain.frame.size.width*217/540*3, scrollviewMain.frame.size.width/2, scrollviewMain.frame.size.width*217/540);
                            }
                            
                            scrollviewMain.contentSize = CGSizeMake(scrollviewMain.frame.size.width, (scrollviewMain.frame.size.width*217/540*4));
                            
                        } else if ([[themecode objectForKey:@"theme_code"] isEqualToString:@"9"]) {
                            if ([[dictionary objectForKey:@"slot_code"] isEqualToString:@"9A"]) {
                                slotFrame = CGRectMake(0, 0, scrollviewMain.frame.size.width, scrollviewMain.frame.size.width);
                            } else if ([[dictionary objectForKey:@"slot_code"] isEqualToString:@"9B"]) {
                                slotFrame = CGRectMake(0, scrollviewMain.frame.size.width, scrollviewMain.frame.size.width/4*3, scrollviewMain.frame.size.width/9);
                            } else if ([[dictionary objectForKey:@"slot_code"] isEqualToString:@"9C"]) {
                                slotFrame = CGRectMake(scrollviewMain.frame.size.width/4*3, scrollviewMain.frame.size.width, scrollviewMain.frame.size.width/4, scrollviewMain.frame.size.width/9);
                            } else if ([[dictionary objectForKey:@"slot_code"] isEqualToString:@"9D"]) {
                                slotFrame = CGRectMake(0, scrollviewMain.frame.size.width/9*10, scrollviewMain.frame.size.width, scrollviewMain.frame.size.width/9*5);
                            } else if ([[dictionary objectForKey:@"slot_code"] isEqualToString:@"9E"]) {
                                slotFrame = CGRectMake(0, scrollviewMain.frame.size.width/9*15, scrollviewMain.frame.size.width/4*3, scrollviewMain.frame.size.width/9);
                            } else if ([[dictionary objectForKey:@"slot_code"] isEqualToString:@"9F"]) {
                                slotFrame = CGRectMake(scrollviewMain.frame.size.width/4*3, scrollviewMain.frame.size.width/9*15, scrollviewMain.frame.size.width/4, scrollviewMain.frame.size.width/9);
                            } else if ([[dictionary objectForKey:@"slot_code"] isEqualToString:@"9G"]) {
                                slotFrame = CGRectMake(0, scrollviewMain.frame.size.width/9*16, scrollviewMain.frame.size.width, scrollviewMain.frame.size.width/9*5);
                            } else if ([[dictionary objectForKey:@"slot_code"] isEqualToString:@"9H"]) {
                                slotFrame = CGRectMake(0, scrollviewMain.frame.size.width/9*21, scrollviewMain.frame.size.width/4*3, scrollviewMain.frame.size.width/9);
                            } else if ([[dictionary objectForKey:@"slot_code"] isEqualToString:@"9I"]) {
                                slotFrame = CGRectMake(scrollviewMain.frame.size.width/4*3, scrollviewMain.frame.size.width/9*21, scrollviewMain.frame.size.width/4, scrollviewMain.frame.size.width/9);
                            } else if ([[dictionary objectForKey:@"slot_code"] isEqualToString:@"9J"]) {
                                slotFrame = CGRectMake(0, scrollviewMain.frame.size.width/9*22, scrollviewMain.frame.size.width, scrollviewMain.frame.size.width/9*5);
                            } else if ([[dictionary objectForKey:@"slot_code"] isEqualToString:@"9K"]) {
                                slotFrame = CGRectMake(0, scrollviewMain.frame.size.width/9*27, scrollviewMain.frame.size.width/4*3, scrollviewMain.frame.size.width/9);
                            } else if ([[dictionary objectForKey:@"slot_code"] isEqualToString:@"9L"]) {
                                slotFrame = CGRectMake(scrollviewMain.frame.size.width/4*3, scrollviewMain.frame.size.width/9*27, scrollviewMain.frame.size.width/4, scrollviewMain.frame.size.width/9);
                            } else if ([[dictionary objectForKey:@"slot_code"] isEqualToString:@"9M"]) {
                                slotFrame = CGRectMake(0, scrollviewMain.frame.size.width/9*28, scrollviewMain.frame.size.width, scrollviewMain.frame.size.width/9*5);
                            } else if ([[dictionary objectForKey:@"slot_code"] isEqualToString:@"9N"]) {
                                slotFrame = CGRectMake(0, scrollviewMain.frame.size.width/9*33, scrollviewMain.frame.size.width, scrollviewMain.frame.size.width*700/540);
                            } else if ([[dictionary objectForKey:@"slot_code"] isEqualToString:@"9O"]) {
                                slotFrame = CGRectMake(0, scrollviewMain.frame.size.width/9*33+scrollviewMain.frame.size.width*700/540, scrollviewMain.frame.size.width, scrollviewMain.frame.size.width/9);
                            }
                            
                            scrollviewMain.contentSize = CGSizeMake(scrollviewMain.frame.size.width, scrollviewMain.frame.size.width/9*34+scrollviewMain.frame.size.width*700/540);
                            
                        } else if ([[themecode objectForKey:@"theme_code"] isEqualToString:@"10"]) {
                            if ([[dictionary objectForKey:@"slot_code"] isEqualToString:@"10A"]) {
                                slotFrame = CGRectMake(0, 0, scrollviewMain.frame.size.width, scrollviewMain.frame.size.width*100/540);
                            } else if ([[dictionary objectForKey:@"slot_code"] isEqualToString:@"10B"]) {
                                slotFrame = CGRectMake(0, scrollviewMain.frame.size.width*100/540, scrollviewMain.frame.size.width*100/540, scrollviewMain.frame.size.width*100/540);
                            } else if ([[dictionary objectForKey:@"slot_code"] isEqualToString:@"10C"]) {
                                slotFrame = CGRectMake(scrollviewMain.frame.size.width*100/540, scrollviewMain.frame.size.width*100/540, scrollviewMain.frame.size.width*340/540, scrollviewMain.frame.size.width*100/540);
                            } else if ([[dictionary objectForKey:@"slot_code"] isEqualToString:@"10D"]) {
                                slotFrame = CGRectMake(scrollviewMain.frame.size.width*440/540, scrollviewMain.frame.size.width*100/540, scrollviewMain.frame.size.width*100/540, scrollviewMain.frame.size.width*100/540);
                            } else if ([[dictionary objectForKey:@"slot_code"] isEqualToString:@"10E"]) {
                                slotFrame = CGRectMake(0, scrollviewMain.frame.size.width*100/540*2, scrollviewMain.frame.size.width, scrollviewMain.frame.size.width*330/540);
                            } else if ([[dictionary objectForKey:@"slot_code"] isEqualToString:@"10F"]) {
                                slotFrame = CGRectMake(0, scrollviewMain.frame.size.width*530/540, scrollviewMain.frame.size.width, scrollviewMain.frame.size.width*100/540);
                            } else if ([[dictionary objectForKey:@"slot_code"] isEqualToString:@"10G"]) {
                                slotFrame = CGRectMake(0, scrollviewMain.frame.size.width*630/540, scrollviewMain.frame.size.width*180/540, scrollviewMain.frame.size.width*180/540);
                            } else if ([[dictionary objectForKey:@"slot_code"] isEqualToString:@"10H"]) {
                                slotFrame = CGRectMake(scrollviewMain.frame.size.width*180/540, scrollviewMain.frame.size.width*630/540, scrollviewMain.frame.size.width*180/540, scrollviewMain.frame.size.width*180/540);
                            } else if ([[dictionary objectForKey:@"slot_code"] isEqualToString:@"10I"]) {
                                slotFrame = CGRectMake(scrollviewMain.frame.size.width*180/540*2, scrollviewMain.frame.size.width*630/540, scrollviewMain.frame.size.width*180/540, scrollviewMain.frame.size.width*180/540);
                            } else if ([[dictionary objectForKey:@"slot_code"] isEqualToString:@"10J"]) {
                                slotFrame = CGRectMake(0, scrollviewMain.frame.size.width*810/540, scrollviewMain.frame.size.width, scrollviewMain.frame.size.width*60/540);
                            }
                            
                            scrollviewMain.contentSize = CGSizeMake(scrollviewMain.frame.size.width, scrollviewMain.frame.size.width*870/540);
                            
                        } else if ([[themecode objectForKey:@"theme_code"] isEqualToString:@"11"]) {
                            if ([[dictionary objectForKey:@"slot_code"] isEqualToString:@"11A"]) {
                                slotFrame = CGRectMake(0, 0, scrollviewMain.frame.size.width, scrollviewMain.frame.size.width*100/540);
                            } else if ([[dictionary objectForKey:@"slot_code"] isEqualToString:@"11B"]) {
                                slotFrame = CGRectMake(0, scrollviewMain.frame.size.width*100/540, scrollviewMain.frame.size.width*100/540, scrollviewMain.frame.size.width*100/540);
                            } else if ([[dictionary objectForKey:@"slot_code"] isEqualToString:@"11C"]) {
                                slotFrame = CGRectMake(scrollviewMain.frame.size.width*100/540, scrollviewMain.frame.size.width*100/540, scrollviewMain.frame.size.width*340/540, scrollviewMain.frame.size.width*100/540);
                            } else if ([[dictionary objectForKey:@"slot_code"] isEqualToString:@"11D"]) {
                                slotFrame = CGRectMake(scrollviewMain.frame.size.width*440/540, scrollviewMain.frame.size.width*100/540, scrollviewMain.frame.size.width*100/540, scrollviewMain.frame.size.width*100/540);
                            } else if ([[dictionary objectForKey:@"slot_code"] isEqualToString:@"11E"]) {
                                slotFrame = CGRectMake(0, scrollviewMain.frame.size.width*100/540*2, scrollviewMain.frame.size.width, scrollviewMain.frame.size.width*180/540);
                            } else if ([[dictionary objectForKey:@"slot_code"] isEqualToString:@"11F"]) {
                                slotFrame = CGRectMake(0, scrollviewMain.frame.size.width*380/540, scrollviewMain.frame.size.width, scrollviewMain.frame.size.width*100/540);
                            } else if ([[dictionary objectForKey:@"slot_code"] isEqualToString:@"11G"]) {
                                slotFrame = CGRectMake(0, scrollviewMain.frame.size.width*480/540, scrollviewMain.frame.size.width, scrollviewMain.frame.size.width*330/540);
                            } else if ([[dictionary objectForKey:@"slot_code"] isEqualToString:@"11H"]) {
                                slotFrame = CGRectMake(0, scrollviewMain.frame.size.width*810/540, scrollviewMain.frame.size.width, scrollviewMain.frame.size.width*60/540);
                            }
                            
                            scrollviewMain.contentSize = CGSizeMake(scrollviewMain.frame.size.width, scrollviewMain.frame.size.width*870/540);
                            
                        } else if ([[themecode objectForKey:@"theme_code"] isEqualToString:@"12"]) {
                            if ([[dictionary objectForKey:@"slot_code"] isEqualToString:@"12A"]) {
                                slotFrame = CGRectMake(0, 0, scrollviewMain.frame.size.width, scrollviewMain.frame.size.width*100/540);
                            } else if ([[dictionary objectForKey:@"slot_code"] isEqualToString:@"12B"]) {
                                slotFrame = CGRectMake(0, scrollviewMain.frame.size.width*100/540, scrollviewMain.frame.size.width, scrollviewMain.frame.size.width*385/540);
                            } else if ([[dictionary objectForKey:@"slot_code"] isEqualToString:@"12C"]) {
                                slotFrame = CGRectMake(0, scrollviewMain.frame.size.width*485/540, scrollviewMain.frame.size.width, scrollviewMain.frame.size.width*385/540);
                            }
                            
                            scrollviewMain.contentSize = CGSizeMake(scrollviewMain.frame.size.width, scrollviewMain.frame.size.width*870/540);
                            
                        } else if ([[themecode objectForKey:@"theme_code"] isEqualToString:@"13"]) {
                            if ([[dictionary objectForKey:@"slot_code"] isEqualToString:@"13A"]) {
                                slotFrame = CGRectMake(0, 0, scrollviewMain.frame.size.width, scrollviewMain.frame.size.width*435/540);
                            } else if ([[dictionary objectForKey:@"slot_code"] isEqualToString:@"13B"]) {
                                slotFrame = CGRectMake(0, scrollviewMain.frame.size.width*435/540, scrollviewMain.frame.size.width, scrollviewMain.frame.size.width*435/540);
                            }
                            
                            scrollviewMain.contentSize = CGSizeMake(scrollviewMain.frame.size.width, scrollviewMain.frame.size.width*870/540);
                            
                        } else if ([[themecode objectForKey:@"theme_code"] isEqualToString:@"14"]) {
                            if ([[dictionary objectForKey:@"slot_code"] isEqualToString:@"14A"]) {
                                slotFrame = CGRectMake(0, 0, scrollviewMain.frame.size.width, scrollviewMain.frame.size.width*290/540);
                            } else if ([[dictionary objectForKey:@"slot_code"] isEqualToString:@"14B"]) {
                                slotFrame = CGRectMake(0, scrollviewMain.frame.size.width*290/540, scrollviewMain.frame.size.width, scrollviewMain.frame.size.width*290/540);
                            } else if ([[dictionary objectForKey:@"slot_code"] isEqualToString:@"14C"]) {
                                slotFrame = CGRectMake(0, scrollviewMain.frame.size.width*290/540*2, scrollviewMain.frame.size.width, scrollviewMain.frame.size.width*290/540);
                            }
                            
                            scrollviewMain.contentSize = CGSizeMake(scrollviewMain.frame.size.width, scrollviewMain.frame.size.width*290/540*3);
                            
                        } else if ([[themecode objectForKey:@"theme_code"] isEqualToString:@"15"]) {
                            if ([[dictionary objectForKey:@"slot_code"] isEqualToString:@"15A"]) {
                                slotFrame = CGRectMake(0, 0, scrollviewMain.frame.size.width, scrollviewMain.frame.size.width*100/540);
                            } else if ([[dictionary objectForKey:@"slot_code"] isEqualToString:@"15B"]) {
                                slotFrame = CGRectMake(0, scrollviewMain.frame.size.width*100/540, scrollviewMain.frame.size.width, scrollviewMain.frame.size.width*300/540);
                            } else if ([[dictionary objectForKey:@"slot_code"] isEqualToString:@"15C"]) {
                                slotFrame = CGRectMake(0, scrollviewMain.frame.size.width*400/540, scrollviewMain.frame.size.width, scrollviewMain.frame.size.width*300/540);
                            } else if ([[dictionary objectForKey:@"slot_code"] isEqualToString:@"15D"]) {
                                slotFrame = CGRectMake(0, scrollviewMain.frame.size.width*700/540, scrollviewMain.frame.size.width, scrollviewMain.frame.size.width*300/540);
                            } else if ([[dictionary objectForKey:@"slot_code"] isEqualToString:@"15E"]) {
                                slotFrame = CGRectMake(0, scrollviewMain.frame.size.width*1000/540, scrollviewMain.frame.size.width, scrollviewMain.frame.size.width*300/540);
                            } else if ([[dictionary objectForKey:@"slot_code"] isEqualToString:@"15F"]) {
                                slotFrame = CGRectMake(0, scrollviewMain.frame.size.width*1300/540, scrollviewMain.frame.size.width, scrollviewMain.frame.size.width*300/540);
                            }
                            
                            scrollviewMain.contentSize = CGSizeMake(scrollviewMain.frame.size.width, scrollviewMain.frame.size.width*1600/540);
                            
                        } else if ([[themecode objectForKey:@"theme_code"] isEqualToString:@"16"]) {
                            if ([[dictionary objectForKey:@"slot_code"] isEqualToString:@"16A"]) {
                                slotFrame = CGRectMake(0, 0, scrollviewMain.frame.size.width, scrollviewMain.frame.size.width*110/540);
                            } else if ([[dictionary objectForKey:@"slot_code"] isEqualToString:@"16B"]) {
                                slotFrame = CGRectMake(0, scrollviewMain.frame.size.width*110/540, scrollviewMain.frame.size.width, scrollviewMain.frame.size.width*110/540);
                            } else if ([[dictionary objectForKey:@"slot_code"] isEqualToString:@"16C"]) {
                                slotFrame = CGRectMake(0, scrollviewMain.frame.size.width*220/540, scrollviewMain.frame.size.width, scrollviewMain.frame.size.width*110/540);
                            } else if ([[dictionary objectForKey:@"slot_code"] isEqualToString:@"16D"]) {
                                slotFrame = CGRectMake(0, scrollviewMain.frame.size.width*330/540, scrollviewMain.frame.size.width, scrollviewMain.frame.size.width*430/540);
                            } else if ([[dictionary objectForKey:@"slot_code"] isEqualToString:@"16E"]) {
                                slotFrame = CGRectMake(0, scrollviewMain.frame.size.width*760/540, scrollviewMain.frame.size.width, scrollviewMain.frame.size.width*110/540);
                            }
                            
                            scrollviewMain.contentSize = CGSizeMake(scrollviewMain.frame.size.width, scrollviewMain.frame.size.width*870/540);
                            
                        } else if ([[themecode objectForKey:@"theme_code"] isEqualToString:@"17"]) {
                            if ([[dictionary objectForKey:@"slot_code"] isEqualToString:@"17A"]) {
                                slotFrame = CGRectMake(0, 0, scrollviewMain.frame.size.width, scrollviewMain.frame.size.width*255/540);
                            } else if ([[dictionary objectForKey:@"slot_code"] isEqualToString:@"17B"]) {
                                slotFrame = CGRectMake(0, scrollviewMain.frame.size.width*255/540, scrollviewMain.frame.size.width/2, scrollviewMain.frame.size.width*180/540);
                            } else if ([[dictionary objectForKey:@"slot_code"] isEqualToString:@"17C"]) {
                                slotFrame = CGRectMake(scrollviewMain.frame.size.width/2, scrollviewMain.frame.size.width*255/540, scrollviewMain.frame.size.width/2, scrollviewMain.frame.size.width*180/540);
                            } else if ([[dictionary objectForKey:@"slot_code"] isEqualToString:@"17D"]) {
                                slotFrame = CGRectMake(0, scrollviewMain.frame.size.width*435/540, scrollviewMain.frame.size.width, scrollviewMain.frame.size.width*255/540);
                            } else if ([[dictionary objectForKey:@"slot_code"] isEqualToString:@"17E"]) {
                                slotFrame = CGRectMake(0, scrollviewMain.frame.size.width*690/540, scrollviewMain.frame.size.width/2, scrollviewMain.frame.size.width*180/540);
                            } else if ([[dictionary objectForKey:@"slot_code"] isEqualToString:@"17F"]) {
                                slotFrame = CGRectMake(scrollviewMain.frame.size.width/2, scrollviewMain.frame.size.width*690/540, scrollviewMain.frame.size.width/2, scrollviewMain.frame.size.width*180/540);
                            }
                            
                            scrollviewMain.contentSize = CGSizeMake(scrollviewMain.frame.size.width, scrollviewMain.frame.size.width*870/540);
                            
                        } else if ([[themecode objectForKey:@"theme_code"] isEqualToString:@"18"]) {
                            if ([[dictionary objectForKey:@"slot_code"] isEqualToString:@"18A"]) {
                                slotFrame = CGRectMake(0, 0, scrollviewMain.frame.size.width/3, scrollviewMain.frame.size.width*200/540);
                            } else if ([[dictionary objectForKey:@"slot_code"] isEqualToString:@"18B"]) {
                                slotFrame = CGRectMake(scrollviewMain.frame.size.width/3, 0, scrollviewMain.frame.size.width/3, scrollviewMain.frame.size.width*200/540);
                            } else if ([[dictionary objectForKey:@"slot_code"] isEqualToString:@"18C"]) {
                                slotFrame = CGRectMake(scrollviewMain.frame.size.width/3*2, 0, scrollviewMain.frame.size.width/3, scrollviewMain.frame.size.width*200/540);
                            } else if ([[dictionary objectForKey:@"slot_code"] isEqualToString:@"18D"]) {
                                slotFrame = CGRectMake(0, scrollviewMain.frame.size.width*200/540, scrollviewMain.frame.size.width/3, scrollviewMain.frame.size.width*200/540);
                            } else if ([[dictionary objectForKey:@"slot_code"] isEqualToString:@"18E"]) {
                                slotFrame = CGRectMake(scrollviewMain.frame.size.width/3, scrollviewMain.frame.size.width*200/540, scrollviewMain.frame.size.width/3, scrollviewMain.frame.size.width*200/540);
                            } else if ([[dictionary objectForKey:@"slot_code"] isEqualToString:@"18F"]) {
                                slotFrame = CGRectMake(scrollviewMain.frame.size.width/3*2, scrollviewMain.frame.size.width*200/540, scrollviewMain.frame.size.width/3, scrollviewMain.frame.size.width*200/540);
                            } else if ([[dictionary objectForKey:@"slot_code"] isEqualToString:@"18G"]) {
                                slotFrame = CGRectMake(0, scrollviewMain.frame.size.width*400/540, scrollviewMain.frame.size.width/3, scrollviewMain.frame.size.width*200/540);
                            } else if ([[dictionary objectForKey:@"slot_code"] isEqualToString:@"18H"]) {
                                slotFrame = CGRectMake(scrollviewMain.frame.size.width/3, scrollviewMain.frame.size.width*400/540, scrollviewMain.frame.size.width/3, scrollviewMain.frame.size.width*200/540);
                            } else if ([[dictionary objectForKey:@"slot_code"] isEqualToString:@"18I"]) {
                                slotFrame = CGRectMake(scrollviewMain.frame.size.width/3*2, scrollviewMain.frame.size.width*400/540, scrollviewMain.frame.size.width/3, scrollviewMain.frame.size.width*200/540);
                            } else if ([[dictionary objectForKey:@"slot_code"] isEqualToString:@"18J"]) {
                                slotFrame = CGRectMake(0, scrollviewMain.frame.size.width*600/540, scrollviewMain.frame.size.width/3, scrollviewMain.frame.size.width*200/540);
                            } else if ([[dictionary objectForKey:@"slot_code"] isEqualToString:@"18K"]) {
                                slotFrame = CGRectMake(scrollviewMain.frame.size.width/3, scrollviewMain.frame.size.width*600/540, scrollviewMain.frame.size.width/3, scrollviewMain.frame.size.width*200/540);
                            } else if ([[dictionary objectForKey:@"slot_code"] isEqualToString:@"18L"]) {
                                slotFrame = CGRectMake(scrollviewMain.frame.size.width/3*2, scrollviewMain.frame.size.width*600/540, scrollviewMain.frame.size.width/3, scrollviewMain.frame.size.width*200/540);
                            } else if ([[dictionary objectForKey:@"slot_code"] isEqualToString:@"18M"]) {
                                slotFrame = CGRectMake(0, scrollviewMain.frame.size.width*800/540, scrollviewMain.frame.size.width, scrollviewMain.frame.size.width*70/540);
                            }
                            
                            scrollviewMain.contentSize = CGSizeMake(scrollviewMain.frame.size.width, scrollviewMain.frame.size.width*870/540);
                            
                        } else if ([[themecode objectForKey:@"theme_code"] isEqualToString:@"19"]) {
                            if ([[dictionary objectForKey:@"slot_code"] isEqualToString:@"19A"]) {
                                slotFrame = CGRectMake(0, 0, scrollviewMain.frame.size.width, scrollviewMain.frame.size.width*60/540);
                            } else if ([[dictionary objectForKey:@"slot_code"] isEqualToString:@"19B"]) {
                                slotFrame = CGRectMake(0, scrollviewMain.frame.size.width*60/540, scrollviewMain.frame.size.width/3, scrollviewMain.frame.size.width*250/540);
                            } else if ([[dictionary objectForKey:@"slot_code"] isEqualToString:@"19C"]) {
                                slotFrame = CGRectMake(scrollviewMain.frame.size.width/3, scrollviewMain.frame.size.width*60/540, scrollviewMain.frame.size.width/3, scrollviewMain.frame.size.width*250/540);
                            } else if ([[dictionary objectForKey:@"slot_code"] isEqualToString:@"19D"]) {
                                slotFrame = CGRectMake(scrollviewMain.frame.size.width/3*2, scrollviewMain.frame.size.width*60/540, scrollviewMain.frame.size.width/3, scrollviewMain.frame.size.width*250/540);
                            } else if ([[dictionary objectForKey:@"slot_code"] isEqualToString:@"19E"]) {
                                slotFrame = CGRectMake(0, scrollviewMain.frame.size.width*310/540, scrollviewMain.frame.size.width/3, scrollviewMain.frame.size.width*250/540);
                            } else if ([[dictionary objectForKey:@"slot_code"] isEqualToString:@"19F"]) {
                                slotFrame = CGRectMake(scrollviewMain.frame.size.width/3, scrollviewMain.frame.size.width*310/540, scrollviewMain.frame.size.width/3, scrollviewMain.frame.size.width*250/540);
                            } else if ([[dictionary objectForKey:@"slot_code"] isEqualToString:@"19G"]) {
                                slotFrame = CGRectMake(scrollviewMain.frame.size.width/3*2, scrollviewMain.frame.size.width*310/540, scrollviewMain.frame.size.width/3, scrollviewMain.frame.size.width*250/540);
                            } else if ([[dictionary objectForKey:@"slot_code"] isEqualToString:@"19H"]) {
                                slotFrame = CGRectMake(0, scrollviewMain.frame.size.width*560/540, scrollviewMain.frame.size.width/3, scrollviewMain.frame.size.width*250/540);
                            } else if ([[dictionary objectForKey:@"slot_code"] isEqualToString:@"19I"]) {
                                slotFrame = CGRectMake(scrollviewMain.frame.size.width/3, scrollviewMain.frame.size.width*560/540, scrollviewMain.frame.size.width/3, scrollviewMain.frame.size.width*250/540);
                            } else if ([[dictionary objectForKey:@"slot_code"] isEqualToString:@"19J"]) {
                                slotFrame = CGRectMake(scrollviewMain.frame.size.width/3*2, scrollviewMain.frame.size.width*560/540, scrollviewMain.frame.size.width/3, scrollviewMain.frame.size.width*250/540);
                            } else if ([[dictionary objectForKey:@"slot_code"] isEqualToString:@"19K"]) {
                                slotFrame = CGRectMake(0, scrollviewMain.frame.size.width*810/540, scrollviewMain.frame.size.width, scrollviewMain.frame.size.width*60/540);
                            }
                            
                            scrollviewMain.contentSize = CGSizeMake(scrollviewMain.frame.size.width, scrollviewMain.frame.size.width*870/540);
                            
                        } else if ([[themecode objectForKey:@"theme_code"] isEqualToString:@"20"]) {
                            if ([[dictionary objectForKey:@"slot_code"] isEqualToString:@"20A"]) {
                                slotFrame = CGRectMake(0, 0, scrollviewMain.frame.size.width, scrollviewMain.frame.size.width*300/540);
                            } else if ([[dictionary objectForKey:@"slot_code"] isEqualToString:@"20B"]) {
                                slotFrame = CGRectMake(0, scrollviewMain.frame.size.width*300/540, scrollviewMain.frame.size.width/5, scrollviewMain.frame.size.width*110/540);
                            } else if ([[dictionary objectForKey:@"slot_code"] isEqualToString:@"20C"]) {
                                slotFrame = CGRectMake(scrollviewMain.frame.size.width/5, scrollviewMain.frame.size.width*300/540, scrollviewMain.frame.size.width/5, scrollviewMain.frame.size.width*110/540);
                            } else if ([[dictionary objectForKey:@"slot_code"] isEqualToString:@"20D"]) {
                                slotFrame = CGRectMake(scrollviewMain.frame.size.width/5*2, scrollviewMain.frame.size.width*300/540, scrollviewMain.frame.size.width/5, scrollviewMain.frame.size.width*110/540);
                            } else if ([[dictionary objectForKey:@"slot_code"] isEqualToString:@"20E"]) {
                                slotFrame = CGRectMake(scrollviewMain.frame.size.width/5*3, scrollviewMain.frame.size.width*300/540, scrollviewMain.frame.size.width/5, scrollviewMain.frame.size.width*110/540);
                            } else if ([[dictionary objectForKey:@"slot_code"] isEqualToString:@"20F"]) {
                                slotFrame = CGRectMake(scrollviewMain.frame.size.width/5*4, scrollviewMain.frame.size.width*300/540, scrollviewMain.frame.size.width/5, scrollviewMain.frame.size.width*110/540);
                            } else if ([[dictionary objectForKey:@"slot_code"] isEqualToString:@"20G"]) {
                                slotFrame = CGRectMake(0, scrollviewMain.frame.size.width*410/540, scrollviewMain.frame.size.width, scrollviewMain.frame.size.width*230/540);
                            } else if ([[dictionary objectForKey:@"slot_code"] isEqualToString:@"20H"]) {
                                slotFrame = CGRectMake(0, scrollviewMain.frame.size.width*640/540, scrollviewMain.frame.size.width, scrollviewMain.frame.size.width*230/540);
                            }
                            
                            scrollviewMain.contentSize = CGSizeMake(scrollviewMain.frame.size.width, scrollviewMain.frame.size.width*870/540);
                            
                        } else if ([[themecode objectForKey:@"theme_code"] isEqualToString:@"21"]) {
                            if ([[dictionary objectForKey:@"slot_code"] isEqualToString:@"21A"]) {
                                slotFrame = CGRectMake(0, 0, scrollviewMain.frame.size.width*100/540, scrollviewMain.frame.size.width*100/540);
                            } else if ([[dictionary objectForKey:@"slot_code"] isEqualToString:@"21B"]) {
                                slotFrame = CGRectMake(scrollviewMain.frame.size.width*100/540, 0, scrollviewMain.frame.size.width*340/540, scrollviewMain.frame.size.width*100/540);
                            } else if ([[dictionary objectForKey:@"slot_code"] isEqualToString:@"21C"]) {
                                slotFrame = CGRectMake(scrollviewMain.frame.size.width*440/540, 0, scrollviewMain.frame.size.width*100/540, scrollviewMain.frame.size.width*100/540);
                            } else if ([[dictionary objectForKey:@"slot_code"] isEqualToString:@"21D"]) {
                                slotFrame = CGRectMake(0, scrollviewMain.frame.size.width*100/540, scrollviewMain.frame.size.width, scrollviewMain.frame.size.width*330/540);
                            } else if ([[dictionary objectForKey:@"slot_code"] isEqualToString:@"21E"]) {
                                slotFrame = CGRectMake(0, scrollviewMain.frame.size.width*430/540, scrollviewMain.frame.size.width, scrollviewMain.frame.size.width*100/540);
                            } else if ([[dictionary objectForKey:@"slot_code"] isEqualToString:@"21F"]) {
                                slotFrame = CGRectMake(0, scrollviewMain.frame.size.width*530/540, scrollviewMain.frame.size.width, scrollviewMain.frame.size.width*100/540);
                            } else if ([[dictionary objectForKey:@"slot_code"] isEqualToString:@"21G"]) {
                                slotFrame = CGRectMake(0, scrollviewMain.frame.size.width*630/540, scrollviewMain.frame.size.width/3, scrollviewMain.frame.size.width/3);
                            } else if ([[dictionary objectForKey:@"slot_code"] isEqualToString:@"21H"]) {
                                slotFrame = CGRectMake(scrollviewMain.frame.size.width/3, scrollviewMain.frame.size.width*630/540, scrollviewMain.frame.size.width/3, scrollviewMain.frame.size.width/3);
                            } else if ([[dictionary objectForKey:@"slot_code"] isEqualToString:@"21I"]) {
                                slotFrame = CGRectMake(scrollviewMain.frame.size.width/3*2, scrollviewMain.frame.size.width*630/540, scrollviewMain.frame.size.width/3, scrollviewMain.frame.size.width/3);
                            } else if ([[dictionary objectForKey:@"slot_code"] isEqualToString:@"21J"]) {
                                slotFrame = CGRectMake(0, scrollviewMain.frame.size.width*810/540, scrollviewMain.frame.size.width, scrollviewMain.frame.size.width*60/540);
                            }
                            
                            scrollviewMain.contentSize = CGSizeMake(scrollviewMain.frame.size.width, scrollviewMain.frame.size.width*870/540);
                            
                        } else if ([[themecode objectForKey:@"theme_code"] isEqualToString:@"22"]) {
                            if ([[dictionary objectForKey:@"slot_code"] isEqualToString:@"22A"]) {
                                slotFrame = CGRectMake(0, 0, scrollviewMain.frame.size.width, scrollviewMain.frame.size.width*100/540);
                            } else if ([[dictionary objectForKey:@"slot_code"] isEqualToString:@"22B"]) {
                                slotFrame = CGRectMake(0, scrollviewMain.frame.size.width*100/540, scrollviewMain.frame.size.width, scrollviewMain.frame.size.width*300/540);
                            } else if ([[dictionary objectForKey:@"slot_code"] isEqualToString:@"22C"]) {
                                slotFrame = CGRectMake(0, scrollviewMain.frame.size.width*400/540, scrollviewMain.frame.size.width, scrollviewMain.frame.size.width*300/540);
                            } else if ([[dictionary objectForKey:@"slot_code"] isEqualToString:@"22D"]) {
                                slotFrame = CGRectMake(0, scrollviewMain.frame.size.width*700/540, scrollviewMain.frame.size.width, scrollviewMain.frame.size.width*300/540);
                            } else if ([[dictionary objectForKey:@"slot_code"] isEqualToString:@"22E"]) {
                                slotFrame = CGRectMake(0, scrollviewMain.frame.size.width*1000/540, scrollviewMain.frame.size.width, scrollviewMain.frame.size.width*300/540);
                            } else if ([[dictionary objectForKey:@"slot_code"] isEqualToString:@"22F"]) {
                                slotFrame = CGRectMake(0, scrollviewMain.frame.size.width*1300/540, scrollviewMain.frame.size.width, scrollviewMain.frame.size.width*300/540);
                            } else if ([[dictionary objectForKey:@"slot_code"] isEqualToString:@"22G"]) {
                                slotFrame = CGRectMake(0, scrollviewMain.frame.size.width*1600/540, scrollviewMain.frame.size.width, scrollviewMain.frame.size.width*300/540);
                            } else if ([[dictionary objectForKey:@"slot_code"] isEqualToString:@"22H"]) {
                                slotFrame = CGRectMake(0, scrollviewMain.frame.size.width*1900/540, scrollviewMain.frame.size.width, scrollviewMain.frame.size.width*300/540);
                            } else if ([[dictionary objectForKey:@"slot_code"] isEqualToString:@"22I"]) {
                                slotFrame = CGRectMake(0, scrollviewMain.frame.size.width*2200/540, scrollviewMain.frame.size.width, scrollviewMain.frame.size.width*300/540);
                            } else if ([[dictionary objectForKey:@"slot_code"] isEqualToString:@"22J"]) {
                                slotFrame = CGRectMake(0, scrollviewMain.frame.size.width*2500/540, scrollviewMain.frame.size.width, scrollviewMain.frame.size.width*300/540);
                            } else if ([[dictionary objectForKey:@"slot_code"] isEqualToString:@"22K"]) {
                                slotFrame = CGRectMake(0, scrollviewMain.frame.size.width*2800/540, scrollviewMain.frame.size.width, scrollviewMain.frame.size.width*300/540);
                            } else if ([[dictionary objectForKey:@"slot_code"] isEqualToString:@"22L"]) {
                                slotFrame = CGRectMake(0, scrollviewMain.frame.size.width*3100/540, scrollviewMain.frame.size.width, scrollviewMain.frame.size.width*300/540);
                            }
                            
                            scrollviewMain.contentSize = CGSizeMake(scrollviewMain.frame.size.width, scrollviewMain.frame.size.width*3400/540);
                            
                        } else if ([[themecode objectForKey:@"theme_code"] isEqualToString:@"23"]) {
                            if ([[dictionary objectForKey:@"slot_code"] isEqualToString:@"23A"]) {
                                slotFrame = CGRectMake(0, 0, scrollviewMain.frame.size.width, scrollviewMain.frame.size.width*300/540);
                            } else if ([[dictionary objectForKey:@"slot_code"] isEqualToString:@"23B"]) {
                                slotFrame = CGRectMake(0, scrollviewMain.frame.size.width*300/540, scrollviewMain.frame.size.width, scrollviewMain.frame.size.width*300/540);
                            } else if ([[dictionary objectForKey:@"slot_code"] isEqualToString:@"23C"]) {
                                slotFrame = CGRectMake(0, scrollviewMain.frame.size.width*300/540*2, scrollviewMain.frame.size.width, scrollviewMain.frame.size.width*300/540);
                            } else if ([[dictionary objectForKey:@"slot_code"] isEqualToString:@"23D"]) {
                                slotFrame = CGRectMake(0, scrollviewMain.frame.size.width*300/540*3, scrollviewMain.frame.size.width, scrollviewMain.frame.size.width*300/540);
                            } else if ([[dictionary objectForKey:@"slot_code"] isEqualToString:@"23E"]) {
                                slotFrame = CGRectMake(0, scrollviewMain.frame.size.width*300/540*4, scrollviewMain.frame.size.width, scrollviewMain.frame.size.width*300/540);
                            } else if ([[dictionary objectForKey:@"slot_code"] isEqualToString:@"23F"]) {
                                slotFrame = CGRectMake(0, scrollviewMain.frame.size.width*300/540*5, scrollviewMain.frame.size.width, scrollviewMain.frame.size.width*300/540);
                            } else if ([[dictionary objectForKey:@"slot_code"] isEqualToString:@"23G"]) {
                                slotFrame = CGRectMake(0, scrollviewMain.frame.size.width*300/540*6, scrollviewMain.frame.size.width, scrollviewMain.frame.size.width*300/540);
                            } else if ([[dictionary objectForKey:@"slot_code"] isEqualToString:@"23H"]) {
                                slotFrame = CGRectMake(0, scrollviewMain.frame.size.width*300/540*7, scrollviewMain.frame.size.width, scrollviewMain.frame.size.width*300/540);
                            } else if ([[dictionary objectForKey:@"slot_code"] isEqualToString:@"23I"]) {
                                slotFrame = CGRectMake(0, scrollviewMain.frame.size.width*300/540*8, scrollviewMain.frame.size.width, scrollviewMain.frame.size.width*300/540);
                            } else if ([[dictionary objectForKey:@"slot_code"] isEqualToString:@"23J"]) {
                                slotFrame = CGRectMake(0, scrollviewMain.frame.size.width*300/540*9, scrollviewMain.frame.size.width, scrollviewMain.frame.size.width*300/540);
                            }
                            
                            scrollviewMain.contentSize = CGSizeMake(scrollviewMain.frame.size.width, scrollviewMain.frame.size.width*300/540*10);
                            
                        } else if ([[themecode objectForKey:@"theme_code"] isEqualToString:@"24"]) {
                            if ([[dictionary objectForKey:@"slot_code"] isEqualToString:@"24A"]) {
                                slotFrame = CGRectMake(0, 0, scrollviewMain.frame.size.width, scrollviewMain.frame.size.width*90/540);
                                
                            } else if ([[dictionary objectForKey:@"slot_code"] isEqualToString:@"24B"]) {
                                slotFrame = CGRectMake(0, scrollviewMain.frame.size.width*90/540, scrollviewMain.frame.size.width/3, scrollviewMain.frame.size.width*168/540);
                            } else if ([[dictionary objectForKey:@"slot_code"] isEqualToString:@"24C"]) {
                                slotFrame = CGRectMake(scrollviewMain.frame.size.width/3, scrollviewMain.frame.size.width*90/540, scrollviewMain.frame.size.width/3, scrollviewMain.frame.size.width*168/540);
                            } else if ([[dictionary objectForKey:@"slot_code"] isEqualToString:@"24D"]) {
                                slotFrame = CGRectMake(scrollviewMain.frame.size.width/3*2, scrollviewMain.frame.size.width*90/540, scrollviewMain.frame.size.width/3, scrollviewMain.frame.size.width*168/540);
                                
                            } else if ([[dictionary objectForKey:@"slot_code"] isEqualToString:@"24E"]) {
                                slotFrame = CGRectMake(0, scrollviewMain.frame.size.width*90/540+scrollviewMain.frame.size.width*168/540, scrollviewMain.frame.size.width/3, scrollviewMain.frame.size.width*168/540);
                            } else if ([[dictionary objectForKey:@"slot_code"] isEqualToString:@"24F"]) {
                                slotFrame = CGRectMake(scrollviewMain.frame.size.width/3, scrollviewMain.frame.size.width*90/540+scrollviewMain.frame.size.width*168/540, scrollviewMain.frame.size.width/3, scrollviewMain.frame.size.width*168/540);
                            } else if ([[dictionary objectForKey:@"slot_code"] isEqualToString:@"24G"]) {
                                slotFrame = CGRectMake(scrollviewMain.frame.size.width/3*2, scrollviewMain.frame.size.width*90/540+scrollviewMain.frame.size.width*168/540, scrollviewMain.frame.size.width/3, scrollviewMain.frame.size.width*168/540);
                                
                            } else if ([[dictionary objectForKey:@"slot_code"] isEqualToString:@"24H"]) {
                                slotFrame = CGRectMake(0, scrollviewMain.frame.size.width*90/540+scrollviewMain.frame.size.width*168/540*2, scrollviewMain.frame.size.width/3, scrollviewMain.frame.size.width*168/540);
                            } else if ([[dictionary objectForKey:@"slot_code"] isEqualToString:@"24I"]) {
                                slotFrame = CGRectMake(scrollviewMain.frame.size.width/3, scrollviewMain.frame.size.width*90/540+scrollviewMain.frame.size.width*168/540*2, scrollviewMain.frame.size.width/3, scrollviewMain.frame.size.width*168/540);
                            } else if ([[dictionary objectForKey:@"slot_code"] isEqualToString:@"24J"]) {
                                slotFrame = CGRectMake(scrollviewMain.frame.size.width/3*2, scrollviewMain.frame.size.width*90/540+scrollviewMain.frame.size.width*168/540*2, scrollviewMain.frame.size.width/3, scrollviewMain.frame.size.width*168/540);
                                
                            } else if ([[dictionary objectForKey:@"slot_code"] isEqualToString:@"24K"]) {
                                slotFrame = CGRectMake(0, scrollviewMain.frame.size.width*90/540+scrollviewMain.frame.size.width*168/540*3, scrollviewMain.frame.size.width/3, scrollviewMain.frame.size.width*168/540);
                            } else if ([[dictionary objectForKey:@"slot_code"] isEqualToString:@"24L"]) {
                                slotFrame = CGRectMake(scrollviewMain.frame.size.width/3, scrollviewMain.frame.size.width*90/540+scrollviewMain.frame.size.width*168/540*3, scrollviewMain.frame.size.width/3, scrollviewMain.frame.size.width*168/540);
                            } else if ([[dictionary objectForKey:@"slot_code"] isEqualToString:@"24M"]) {
                                slotFrame = CGRectMake(scrollviewMain.frame.size.width/3*2, scrollviewMain.frame.size.width*90/540+scrollviewMain.frame.size.width*168/540*3, scrollviewMain.frame.size.width/3, scrollviewMain.frame.size.width*168/540);
                                
                            } else if ([[dictionary objectForKey:@"slot_code"] isEqualToString:@"24N"]) {
                                slotFrame = CGRectMake(0, scrollviewMain.frame.size.width*90/540+scrollviewMain.frame.size.width*168/540*4, scrollviewMain.frame.size.width/5, scrollviewMain.frame.size.width*108/540);
                            } else if ([[dictionary objectForKey:@"slot_code"] isEqualToString:@"24O"]) {
                                slotFrame = CGRectMake(scrollviewMain.frame.size.width/5, scrollviewMain.frame.size.width*90/540+scrollviewMain.frame.size.width*168/540*4, scrollviewMain.frame.size.width/5, scrollviewMain.frame.size.width*108/540);
                            } else if ([[dictionary objectForKey:@"slot_code"] isEqualToString:@"24P"]) {
                                slotFrame = CGRectMake(scrollviewMain.frame.size.width/5*2, scrollviewMain.frame.size.width*90/540+scrollviewMain.frame.size.width*168/540*4, scrollviewMain.frame.size.width/5, scrollviewMain.frame.size.width*108/540);
                            } else if ([[dictionary objectForKey:@"slot_code"] isEqualToString:@"24Q"]) {
                                slotFrame = CGRectMake(scrollviewMain.frame.size.width/5*3, scrollviewMain.frame.size.width*90/540+scrollviewMain.frame.size.width*168/540*4, scrollviewMain.frame.size.width/5, scrollviewMain.frame.size.width*108/540);
                            } else if ([[dictionary objectForKey:@"slot_code"] isEqualToString:@"24R"]) {
                                slotFrame = CGRectMake(scrollviewMain.frame.size.width/5*4, scrollviewMain.frame.size.width*90/540+scrollviewMain.frame.size.width*168/540*4, scrollviewMain.frame.size.width/5, scrollviewMain.frame.size.width*108/540);
                            }
                            
                            scrollviewMain.contentSize = CGSizeMake(scrollviewMain.frame.size.width, scrollviewMain.frame.size.width*870/540);
                            
                        } else if ([[themecode objectForKey:@"theme_code"] isEqualToString:@"25"]) {
                            if ([[dictionary objectForKey:@"slot_code"] isEqualToString:@"25A"]) {
                                slotFrame = CGRectMake(0, 0, scrollviewMain.frame.size.width, scrollviewMain.frame.size.width*762/540);
                                
                            } else if ([[dictionary objectForKey:@"slot_code"] isEqualToString:@"25B"]) {
                                slotFrame = CGRectMake(0, scrollviewMain.frame.size.width*762/540, scrollviewMain.frame.size.width, scrollviewMain.frame.size.width*108/540);
                            }
                            
                            scrollviewMain.contentSize = CGSizeMake(scrollviewMain.frame.size.width, scrollviewMain.frame.size.width*870/540);
                            
                        } else if ([[themecode objectForKey:@"theme_code"] isEqualToString:@"26"]) {
                            if ([[dictionary objectForKey:@"slot_code"] isEqualToString:@"26A"]) {
                                slotFrame = CGRectMake(0, 0, scrollviewMain.frame.size.width, scrollviewMain.frame.size.width*300/540);
                            } else if ([[dictionary objectForKey:@"slot_code"] isEqualToString:@"26B"]) {
                                slotFrame = CGRectMake(0, scrollviewMain.frame.size.width*300/540, scrollviewMain.frame.size.width, scrollviewMain.frame.size.width*300/540);
                            } else if ([[dictionary objectForKey:@"slot_code"] isEqualToString:@"26C"]) {
                                slotFrame = CGRectMake(0, scrollviewMain.frame.size.width*300/540*2, scrollviewMain.frame.size.width, scrollviewMain.frame.size.width*300/540);
                            } else if ([[dictionary objectForKey:@"slot_code"] isEqualToString:@"26D"]) {
                                slotFrame = CGRectMake(0, scrollviewMain.frame.size.width*300/540*3, scrollviewMain.frame.size.width, scrollviewMain.frame.size.width*300/540);
                            } else if ([[dictionary objectForKey:@"slot_code"] isEqualToString:@"26E"]) {
                                slotFrame = CGRectMake(0, scrollviewMain.frame.size.width*300/540*4, scrollviewMain.frame.size.width, scrollviewMain.frame.size.width*300/540);
                            } else if ([[dictionary objectForKey:@"slot_code"] isEqualToString:@"26F"]) {
                                slotFrame = CGRectMake(0, scrollviewMain.frame.size.width*300/540*5, scrollviewMain.frame.size.width, scrollviewMain.frame.size.width*300/540);
                            } else if ([[dictionary objectForKey:@"slot_code"] isEqualToString:@"26G"]) {
                                slotFrame = CGRectMake(0, scrollviewMain.frame.size.width*300/540*6, scrollviewMain.frame.size.width, scrollviewMain.frame.size.width*300/540);
                            } else if ([[dictionary objectForKey:@"slot_code"] isEqualToString:@"26H"]) {
                                slotFrame = CGRectMake(0, scrollviewMain.frame.size.width*300/540*7, scrollviewMain.frame.size.width, scrollviewMain.frame.size.width*300/540);
                            } else if ([[dictionary objectForKey:@"slot_code"] isEqualToString:@"26I"]) {
                                slotFrame = CGRectMake(0, scrollviewMain.frame.size.width*300/540*8, scrollviewMain.frame.size.width, scrollviewMain.frame.size.width*300/540);
                            } else if ([[dictionary objectForKey:@"slot_code"] isEqualToString:@"26J"]) {
                                slotFrame = CGRectMake(0, scrollviewMain.frame.size.width*300/540*9, scrollviewMain.frame.size.width, scrollviewMain.frame.size.width*50/540);
                            }
                            
                            scrollviewMain.contentSize = CGSizeMake(scrollviewMain.frame.size.width, scrollviewMain.frame.size.width*300/540*9+scrollviewMain.frame.size.width*50/540);
                            
                    /* not scrollable, all fix to a full page
                    if (![[dictionary objectForKey:@"theme_code"] isEqualToString:@"0"]) {
                        CGRect slotFrame;
                        if ([[themecode objectForKey:@"theme_code"] isEqualToString:@"1"]) {
                            slotFrame = CGRectMake(0, 0, self.frame.size.width, self.frame.size.height-64);
                            
                        } else if ([[themecode objectForKey:@"theme_code"] isEqualToString:@"2"]) {
                            if ([[dictionary objectForKey:@"slot_code"] isEqualToString:@"2A"]) {
                                slotFrame = CGRectMake(0, 0, self.frame.size.width, self.frame.size.height-(self.frame.size.width/5)-64);
                            } else {
                                slotFrame = CGRectMake(self.frame.size.width/5*j, self.frame.size.height-(self.frame.size.width/5)-64, self.frame.size.width/5, self.frame.size.width/5);
                                j++;
                            }
                            
                        } else if ([[themecode objectForKey:@"theme_code"] isEqualToString:@"3"]) {
                            if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone) {
                                // For iPhone
                                if ([listOfObject count]*60 > viewCtrl.view.frame.size.height-64) {
                                    GridtableView = [[UITableView alloc] initWithFrame:CGRectMake(0, 0, viewCtrl.view.frame.size.width, viewCtrl.view.frame.size.height-64)];
                                } else {
                                    GridtableView = [[UITableView alloc] initWithFrame:CGRectMake(0, 0, viewCtrl.view.frame.size.width, [listOfObject count]*60)];
                                }
                            } else {
                                if ([listOfObject count]*140 > viewCtrl.view.frame.size.height-64) {
                                    GridtableView = [[UITableView alloc] initWithFrame:CGRectMake(0, 0, viewCtrl.view.frame.size.width, viewCtrl.view.frame.size.height-64)];
                                } else {
                                    GridtableView = [[UITableView alloc] initWithFrame:CGRectMake(0, 0, viewCtrl.view.frame.size.width, [listOfObject count]*140)];
                                }
                            }
                            GridtableView.autoresizingMask = UIViewAutoresizingFlexibleHeight | UIViewAutoresizingFlexibleWidth;
                            GridtableView.delegate = self;
                            GridtableView.dataSource = self;
                            
                            [GridtableView setBackgroundColor:[UIColor clearColor]];
                            [scrollviewMain addSubview:GridtableView];
                            
                        } else if ([[themecode objectForKey:@"theme_code"] isEqualToString:@"4"]) {
                            scrollview.frame = CGRectMake(0, 0, scrollviewMain.frame.size.width, scrollviewMain.frame.size.height);
                            
                            if ([[dictionary objectForKey:@"slot_code"] isEqualToString:@"4A"]) {
                                slotFrame = CGRectMake(0, 0, scrollview.frame.size.height/4*2, scrollview.frame.size.height/4*2);
                            } else if ([[dictionary objectForKey:@"slot_code"] isEqualToString:@"4I"]) {
                                slotFrame = CGRectMake(0,  scrollview.frame.size.height/4*2, scrollview.frame.size.height/4, scrollview.frame.size.height/4);
                            } else if ([[dictionary objectForKey:@"slot_code"] isEqualToString:@"4J"]) {
                                slotFrame = CGRectMake(scrollview.frame.size.height/4, scrollview.frame.size.height/4*2, scrollview.frame.size.height/4, scrollview.frame.size.height/4);
                            } else if ([[dictionary objectForKey:@"slot_code"] isEqualToString:@"4L"]) {
                                slotFrame = CGRectMake(0, scrollview.frame.size.height/4*3, scrollview.frame.size.height/4*2, scrollview.frame.size.height/4);
                            } else if ([[dictionary objectForKey:@"slot_code"] isEqualToString:@"4B"]) {
                                slotFrame = CGRectMake(scrollview.frame.size.height/4*2, 0, scrollview.frame.size.height/4, scrollview.frame.size.height/4);
                            } else if ([[dictionary objectForKey:@"slot_code"] isEqualToString:@"4F"]) {
                                slotFrame = CGRectMake(scrollview.frame.size.height/4*2, scrollview.frame.size.height/4, scrollview.frame.size.height/4, scrollview.frame.size.height/4);
                            } else if ([[dictionary objectForKey:@"slot_code"] isEqualToString:@"4C"]) {
                                slotFrame = CGRectMake(scrollview.frame.size.height/4*3, 0, scrollview.frame.size.height/4, scrollview.frame.size.height/4*2);
                            } else if ([[dictionary objectForKey:@"slot_code"] isEqualToString:@"4K"]) {
                                slotFrame = CGRectMake(scrollview.frame.size.height/4*2, scrollview.frame.size.height/4*2, scrollview.frame.size.height/4*2, scrollview.frame.size.height/4*2);
                            } else if ([[dictionary objectForKey:@"slot_code"] isEqualToString:@"4D"]) {
                                slotFrame = CGRectMake(scrollview.frame.size.height/4*4, 0, scrollview.frame.size.height/4*2, scrollview.frame.size.height/4);
                            } else if ([[dictionary objectForKey:@"slot_code"] isEqualToString:@"4E"]) {
                                slotFrame = CGRectMake(scrollview.frame.size.height/4*6, 0, scrollview.frame.size.height/4, scrollview.frame.size.height/4);
                            } else if ([[dictionary objectForKey:@"slot_code"] isEqualToString:@"4G"]) {
                                slotFrame = CGRectMake(scrollview.frame.size.height/4*4, scrollview.frame.size.height/4, scrollview.frame.size.height/4, scrollview.frame.size.height/4*2);
                            } else if ([[dictionary objectForKey:@"slot_code"] isEqualToString:@"4H"]) {
                                slotFrame = CGRectMake(scrollview.frame.size.height/4*5, scrollview.frame.size.height/4, scrollview.frame.size.height/4*2, scrollview.frame.size.height/4*2);
                            } else if ([[dictionary objectForKey:@"slot_code"] isEqualToString:@"4M"]) {
                                slotFrame = CGRectMake(scrollview.frame.size.height/4*4, scrollview.frame.size.height/4*3, scrollview.frame.size.height/4*2, scrollview.frame.size.height/4);
                            } else if ([[dictionary objectForKey:@"slot_code"] isEqualToString:@"4N"]) {
                                slotFrame = CGRectMake(scrollview.frame.size.height/4*6, scrollview.frame.size.height/4*3, scrollview.frame.size.height/4, scrollview.frame.size.height/4);
                            }
                            
                            scrollviewMain.contentSize = CGSizeMake(scrollviewMain.frame.size.height/4*7, scrollviewMain.frame.size.height);
                            
                        } else if ([[themecode objectForKey:@"theme_code"] isEqualToString:@"5"]) {
                            if ([[dictionary objectForKey:@"slot_code"] isEqualToString:@"5A"]) {
                                slotFrame = CGRectMake(0, 0, scrollviewMain.frame.size.width, scrollviewMain.frame.size.height/87*33);
                            } else if ([[dictionary objectForKey:@"slot_code"] isEqualToString:@"5B"]) {
                                slotFrame = CGRectMake(0,  scrollviewMain.frame.size.height/87*33, scrollviewMain.frame.size.width/3, scrollviewMain.frame.size.height/87*18);
                            } else if ([[dictionary objectForKey:@"slot_code"] isEqualToString:@"5C"]) {
                                slotFrame = CGRectMake(scrollviewMain.frame.size.width/3, scrollviewMain.frame.size.height/87*33, scrollviewMain.frame.size.width/3, scrollviewMain.frame.size.height/87*18);
                            } else if ([[dictionary objectForKey:@"slot_code"] isEqualToString:@"5D"]) {
                                slotFrame = CGRectMake(scrollviewMain.frame.size.width/3*2, scrollviewMain.frame.size.height/87*33, scrollviewMain.frame.size.width/3, scrollviewMain.frame.size.height/87*18);
                            } else if ([[dictionary objectForKey:@"slot_code"] isEqualToString:@"5E"]) {
                                slotFrame = CGRectMake(0, scrollviewMain.frame.size.height/87*51, scrollviewMain.frame.size.width/3, scrollviewMain.frame.size.height/87*18);
                            } else if ([[dictionary objectForKey:@"slot_code"] isEqualToString:@"5F"]) {
                                slotFrame = CGRectMake(scrollviewMain.frame.size.width/3, scrollviewMain.frame.size.height/87*51, scrollviewMain.frame.size.width/3, scrollviewMain.frame.size.height/87*18);
                            } else if ([[dictionary objectForKey:@"slot_code"] isEqualToString:@"5G"]) {
                                slotFrame = CGRectMake(scrollviewMain.frame.size.width/3*2, scrollviewMain.frame.size.height/87*51, scrollviewMain.frame.size.width/3, scrollviewMain.frame.size.height/87*18);
                            } else if ([[dictionary objectForKey:@"slot_code"] isEqualToString:@"5H"]) {
                                slotFrame = CGRectMake(0, scrollviewMain.frame.size.height/87*69, scrollviewMain.frame.size.width/3, scrollviewMain.frame.size.height/87*18);
                            } else if ([[dictionary objectForKey:@"slot_code"] isEqualToString:@"5I"]) {
                                slotFrame = CGRectMake(scrollviewMain.frame.size.width/3, scrollviewMain.frame.size.height/87*69, scrollviewMain.frame.size.width/3, scrollviewMain.frame.size.height/87*18);
                            } else if ([[dictionary objectForKey:@"slot_code"] isEqualToString:@"5J"]) {
                                slotFrame = CGRectMake(scrollviewMain.frame.size.width/3*2, scrollviewMain.frame.size.height/87*69, scrollviewMain.frame.size.width/3, scrollviewMain.frame.size.height/87*18);
                            }
                            
                            //scrollviewMain.contentSize = CGSizeMake(scrollviewMain.frame.size.width, (scrollviewMain.frame.size.width/3*5));
                            
                        } else if ([[themecode objectForKey:@"theme_code"] isEqualToString:@"6"]) {
                            if ([[dictionary objectForKey:@"slot_code"] isEqualToString:@"6A"]) {
                                slotFrame = CGRectMake(0, 0, scrollviewMain.frame.size.width, scrollviewMain.frame.size.height/87*33);
                            } else if ([[dictionary objectForKey:@"slot_code"] isEqualToString:@"6B"]) {
                                slotFrame = CGRectMake(0, scrollviewMain.frame.size.height/87*33, scrollviewMain.frame.size.width/6*3, scrollviewMain.frame.size.height/87*27);
                            } else if ([[dictionary objectForKey:@"slot_code"] isEqualToString:@"6C"]) {
                                slotFrame = CGRectMake(scrollviewMain.frame.size.width/6*3, scrollviewMain.frame.size.height/87*33, scrollviewMain.frame.size.width/6*3, scrollviewMain.frame.size.height/87*27);
                            } else if ([[dictionary objectForKey:@"slot_code"] isEqualToString:@"6D"]) {
                                slotFrame = CGRectMake(0, scrollviewMain.frame.size.height/87*60, scrollviewMain.frame.size.width/6*3, scrollviewMain.frame.size.height/87*27);
                            } else if ([[dictionary objectForKey:@"slot_code"] isEqualToString:@"6E"]) {
                                slotFrame = CGRectMake(scrollviewMain.frame.size.width/6*3, scrollviewMain.frame.size.height/87*60, scrollviewMain.frame.size.width/6*3, scrollviewMain.frame.size.height/87*27);
                            }
                            
                            //scrollviewMain.contentSize = CGSizeMake(scrollviewMain.frame.size.width, (scrollviewMain.frame.size.width/6*10));
                            
                        } else if ([[themecode objectForKey:@"theme_code"] isEqualToString:@"7"]) {
                            if ([[dictionary objectForKey:@"slot_code"] isEqualToString:@"7D"]) {
                                slotFrame = CGRectMake(0, scrollviewMain.frame.size.height/87*18, scrollviewMain.frame.size.width, scrollviewMain.frame.size.height/87*51);
                            } else if ([[dictionary objectForKey:@"slot_code"] isEqualToString:@"7A"]) {
                                slotFrame = CGRectMake(0, 0, scrollviewMain.frame.size.width/3, scrollviewMain.frame.size.height/87*18);
                            } else if ([[dictionary objectForKey:@"slot_code"] isEqualToString:@"7B"]) {
                                slotFrame = CGRectMake(scrollviewMain.frame.size.width/3, 0, scrollviewMain.frame.size.width/3, scrollviewMain.frame.size.height/87*18);
                            } else if ([[dictionary objectForKey:@"slot_code"] isEqualToString:@"7C"]) {
                                slotFrame = CGRectMake(scrollviewMain.frame.size.width/3*2, 0, scrollviewMain.frame.size.width/3, scrollviewMain.frame.size.height/87*18);
                            } else if ([[dictionary objectForKey:@"slot_code"] isEqualToString:@"7E"]) {
                                slotFrame = CGRectMake(0, scrollviewMain.frame.size.height/87*69, scrollviewMain.frame.size.width/3, scrollviewMain.frame.size.height/87*18);
                            } else if ([[dictionary objectForKey:@"slot_code"] isEqualToString:@"7F"]) {
                                slotFrame = CGRectMake(scrollviewMain.frame.size.width/3, scrollviewMain.frame.size.height/87*69, scrollviewMain.frame.size.width/3, scrollviewMain.frame.size.height/87*18);
                            } else if ([[dictionary objectForKey:@"slot_code"] isEqualToString:@"7G"]) {
                                slotFrame = CGRectMake(scrollviewMain.frame.size.width/3*2, scrollviewMain.frame.size.height/87*69, scrollviewMain.frame.size.width/3, scrollviewMain.frame.size.height/87*18);
                            }
                            
                            //scrollviewMain.contentSize = CGSizeMake(scrollviewMain.frame.size.width, (scrollviewMain.frame.size.width/3*5));
                            
                        } else if ([[themecode objectForKey:@"theme_code"] isEqualToString:@"8"]) {
                            if ([[dictionary objectForKey:@"slot_code"] isEqualToString:@"8A"]) {
                                slotFrame = CGRectMake(0, 0, scrollviewMain.frame.size.width/2, scrollviewMain.frame.size.height/4);
                            } else if ([[dictionary objectForKey:@"slot_code"] isEqualToString:@"8B"]) {
                                slotFrame = CGRectMake(scrollviewMain.frame.size.width/2, 0, scrollviewMain.frame.size.width/2, scrollviewMain.frame.size.height/4);
                            } else if ([[dictionary objectForKey:@"slot_code"] isEqualToString:@"8C"]) {
                                slotFrame = CGRectMake(0, scrollviewMain.frame.size.height/4, scrollviewMain.frame.size.width/2, scrollviewMain.frame.size.height/4);
                            } else if ([[dictionary objectForKey:@"slot_code"] isEqualToString:@"8D"]) {
                                slotFrame = CGRectMake(scrollviewMain.frame.size.width/2, scrollviewMain.frame.size.height/4, scrollviewMain.frame.size.width/2, scrollviewMain.frame.size.height/4);
                            } else if ([[dictionary objectForKey:@"slot_code"] isEqualToString:@"8E"]) {
                                slotFrame = CGRectMake(0, scrollviewMain.frame.size.height/4*2, scrollviewMain.frame.size.width/2, scrollviewMain.frame.size.height/4);
                            } else if ([[dictionary objectForKey:@"slot_code"] isEqualToString:@"8F"]) {
                                slotFrame = CGRectMake(scrollviewMain.frame.size.width/2, scrollviewMain.frame.size.height/4*2, scrollviewMain.frame.size.width/2, scrollviewMain.frame.size.height/4);
                            } else if ([[dictionary objectForKey:@"slot_code"] isEqualToString:@"8G"]) {
                                slotFrame = CGRectMake(0, scrollviewMain.frame.size.height/4*3, scrollviewMain.frame.size.width/2, scrollviewMain.frame.size.height/4);
                            } else if ([[dictionary objectForKey:@"slot_code"] isEqualToString:@"8H"]) {
                                slotFrame = CGRectMake(scrollviewMain.frame.size.width/2, scrollviewMain.frame.size.height/4*3, scrollviewMain.frame.size.width/2, scrollviewMain.frame.size.height/4);
                            }
                            
                            //scrollviewMain.contentSize = CGSizeMake(scrollviewMain.frame.size.width, ((scrollviewMain.frame.size.width/2-scrollviewMain.frame.size.width/10)*4));
                            
                        } else if ([[themecode objectForKey:@"theme_code"] isEqualToString:@"9"]) {
                            if ([[dictionary objectForKey:@"slot_code"] isEqualToString:@"9A"]) {
                                slotFrame = CGRectMake(0, 0, scrollviewMain.frame.size.width, scrollviewMain.frame.size.width);
                            } else if ([[dictionary objectForKey:@"slot_code"] isEqualToString:@"9B"]) {
                                slotFrame = CGRectMake(0, scrollviewMain.frame.size.width, scrollviewMain.frame.size.width/4*3, scrollviewMain.frame.size.width/9);
                            } else if ([[dictionary objectForKey:@"slot_code"] isEqualToString:@"9C"]) {
                                slotFrame = CGRectMake(scrollviewMain.frame.size.width/4*3, scrollviewMain.frame.size.width, scrollviewMain.frame.size.width/4, scrollviewMain.frame.size.width/9);
                            } else if ([[dictionary objectForKey:@"slot_code"] isEqualToString:@"9D"]) {
                                slotFrame = CGRectMake(0, scrollviewMain.frame.size.width/9*10, scrollviewMain.frame.size.width, scrollviewMain.frame.size.width/9*5);
                            } else if ([[dictionary objectForKey:@"slot_code"] isEqualToString:@"9E"]) {
                                slotFrame = CGRectMake(0, scrollviewMain.frame.size.width/9*15, scrollviewMain.frame.size.width/4*3, scrollviewMain.frame.size.width/9);
                            } else if ([[dictionary objectForKey:@"slot_code"] isEqualToString:@"9F"]) {
                                slotFrame = CGRectMake(scrollviewMain.frame.size.width/4*3, scrollviewMain.frame.size.width/9*15, scrollviewMain.frame.size.width/4, scrollviewMain.frame.size.width/9);
                            } else if ([[dictionary objectForKey:@"slot_code"] isEqualToString:@"9G"]) {
                                slotFrame = CGRectMake(0, scrollviewMain.frame.size.width/9*16, scrollviewMain.frame.size.width, scrollviewMain.frame.size.width/9*5);
                            } else if ([[dictionary objectForKey:@"slot_code"] isEqualToString:@"9H"]) {
                                slotFrame = CGRectMake(0, scrollviewMain.frame.size.width/9*21, scrollviewMain.frame.size.width/4*3, scrollviewMain.frame.size.width/9);
                            } else if ([[dictionary objectForKey:@"slot_code"] isEqualToString:@"9I"]) {
                                slotFrame = CGRectMake(scrollviewMain.frame.size.width/4*3, scrollviewMain.frame.size.width/9*21, scrollviewMain.frame.size.width/4, scrollviewMain.frame.size.width/9);
                            } else if ([[dictionary objectForKey:@"slot_code"] isEqualToString:@"9J"]) {
                                slotFrame = CGRectMake(0, scrollviewMain.frame.size.width/9*22, scrollviewMain.frame.size.width, scrollviewMain.frame.size.width/9*5);
                            } else if ([[dictionary objectForKey:@"slot_code"] isEqualToString:@"9K"]) {
                                slotFrame = CGRectMake(0, scrollviewMain.frame.size.width/9*27, scrollviewMain.frame.size.width/4*3, scrollviewMain.frame.size.width/9);
                            } else if ([[dictionary objectForKey:@"slot_code"] isEqualToString:@"9L"]) {
                                slotFrame = CGRectMake(scrollviewMain.frame.size.width/4*3, scrollviewMain.frame.size.width/9*27, scrollviewMain.frame.size.width/4, scrollviewMain.frame.size.width/9);
                            } else if ([[dictionary objectForKey:@"slot_code"] isEqualToString:@"9M"]) {
                                slotFrame = CGRectMake(0, scrollviewMain.frame.size.width/9*28, scrollviewMain.frame.size.width, scrollviewMain.frame.size.width/9*5);
                            } else if ([[dictionary objectForKey:@"slot_code"] isEqualToString:@"9N"]) {
                                slotFrame = CGRectMake(0, scrollviewMain.frame.size.width/9*33, scrollviewMain.frame.size.width, scrollviewMain.frame.size.width/9*12);
                            } else if ([[dictionary objectForKey:@"slot_code"] isEqualToString:@"9O"]) {
                                slotFrame = CGRectMake(0, scrollviewMain.frame.size.width/9*45, scrollviewMain.frame.size.width, scrollviewMain.frame.size.width/9);
                            }
                            
                            scrollviewMain.contentSize = CGSizeMake(scrollviewMain.frame.size.width, scrollviewMain.frame.size.width/9*46);
                            
                        } else if ([[themecode objectForKey:@"theme_code"] isEqualToString:@"10"]) {
                            if ([[dictionary objectForKey:@"slot_code"] isEqualToString:@"10A"]) {
                                slotFrame = CGRectMake(0, 0, scrollviewMain.frame.size.width, scrollviewMain.frame.size.height/87*10);
                            } else if ([[dictionary objectForKey:@"slot_code"] isEqualToString:@"10B"]) {
                                slotFrame = CGRectMake(0, scrollviewMain.frame.size.height/87*10, scrollviewMain.frame.size.height/87*10, scrollviewMain.frame.size.height/87*10);
                            } else if ([[dictionary objectForKey:@"slot_code"] isEqualToString:@"10C"]) {
                                slotFrame = CGRectMake(scrollviewMain.frame.size.height/87*10, scrollviewMain.frame.size.height/87*10, scrollviewMain.frame.size.width-(scrollviewMain.frame.size.height/87*10*2), scrollviewMain.frame.size.height/87*10);
                            } else if ([[dictionary objectForKey:@"slot_code"] isEqualToString:@"10D"]) {
                                slotFrame = CGRectMake(scrollviewMain.frame.size.height/87*10+scrollviewMain.frame.size.width-(scrollviewMain.frame.size.height/87*10*2), scrollviewMain.frame.size.height/87*10, scrollviewMain.frame.size.height/87*10, scrollviewMain.frame.size.height/87*10);
                            } else if ([[dictionary objectForKey:@"slot_code"] isEqualToString:@"10E"]) {
                                slotFrame = CGRectMake(0, scrollviewMain.frame.size.height/87*20, scrollviewMain.frame.size.width, scrollviewMain.frame.size.height/87*33);
                            } else if ([[dictionary objectForKey:@"slot_code"] isEqualToString:@"10F"]) {
                                slotFrame = CGRectMake(0, scrollviewMain.frame.size.height/87*53, scrollviewMain.frame.size.width, scrollviewMain.frame.size.height/87*10);
                            } else if ([[dictionary objectForKey:@"slot_code"] isEqualToString:@"10G"]) {
                                slotFrame = CGRectMake(0, scrollviewMain.frame.size.height/87*63, scrollviewMain.frame.size.width/3, scrollviewMain.frame.size.height/87*18);
                            } else if ([[dictionary objectForKey:@"slot_code"] isEqualToString:@"10H"]) {
                                slotFrame = CGRectMake(scrollviewMain.frame.size.width/3, scrollviewMain.frame.size.height/87*63, scrollviewMain.frame.size.width/3, scrollviewMain.frame.size.height/87*18);
                            } else if ([[dictionary objectForKey:@"slot_code"] isEqualToString:@"10I"]) {
                                slotFrame = CGRectMake(scrollviewMain.frame.size.width/3*2, scrollviewMain.frame.size.height/87*63, scrollviewMain.frame.size.width/3, scrollviewMain.frame.size.height/87*18);
                            } else if ([[dictionary objectForKey:@"slot_code"] isEqualToString:@"10J"]) {
                                slotFrame = CGRectMake(0, scrollviewMain.frame.size.height/87*81, scrollviewMain.frame.size.width, scrollviewMain.frame.size.height/87*6);
                            }
                            
                        } else if ([[themecode objectForKey:@"theme_code"] isEqualToString:@"11"]) {
                            if ([[dictionary objectForKey:@"slot_code"] isEqualToString:@"11A"]) {
                                slotFrame = CGRectMake(0, 0, scrollviewMain.frame.size.width, scrollviewMain.frame.size.height/87*10);
                            } else if ([[dictionary objectForKey:@"slot_code"] isEqualToString:@"11B"]) {
                                slotFrame = CGRectMake(0, scrollviewMain.frame.size.height/87*10, scrollviewMain.frame.size.height/87*10, scrollviewMain.frame.size.height/87*10);
                            } else if ([[dictionary objectForKey:@"slot_code"] isEqualToString:@"11C"]) {
                                slotFrame = CGRectMake(scrollviewMain.frame.size.height/87*10, scrollviewMain.frame.size.height/87*10, scrollviewMain.frame.size.width-(scrollviewMain.frame.size.height/87*10*2), scrollviewMain.frame.size.height/87*10);
                            } else if ([[dictionary objectForKey:@"slot_code"] isEqualToString:@"11D"]) {
                                slotFrame = CGRectMake(scrollviewMain.frame.size.height/87*10+scrollviewMain.frame.size.width-(scrollviewMain.frame.size.height/87*10*2), scrollviewMain.frame.size.height/87*10, scrollviewMain.frame.size.height/87*10, scrollviewMain.frame.size.height/87*10);
                            } else if ([[dictionary objectForKey:@"slot_code"] isEqualToString:@"11E"]) {
                                slotFrame = CGRectMake(0, scrollviewMain.frame.size.height/87*20, scrollviewMain.frame.size.width, scrollviewMain.frame.size.height/87*18);
                            } else if ([[dictionary objectForKey:@"slot_code"] isEqualToString:@"11F"]) {
                                slotFrame = CGRectMake(0, scrollviewMain.frame.size.height/87*38, scrollviewMain.frame.size.width, scrollviewMain.frame.size.height/87*10);
                            } else if ([[dictionary objectForKey:@"slot_code"] isEqualToString:@"11G"]) {
                                slotFrame = CGRectMake(0, scrollviewMain.frame.size.height/87*48, scrollviewMain.frame.size.width, scrollviewMain.frame.size.height/87*33);
                            } else if ([[dictionary objectForKey:@"slot_code"] isEqualToString:@"11H"]) {
                                slotFrame = CGRectMake(0, scrollviewMain.frame.size.height/87*81, scrollviewMain.frame.size.width, scrollviewMain.frame.size.height/87*6);
                            }
                            
                        } else if ([[themecode objectForKey:@"theme_code"] isEqualToString:@"12"]) {
                            if ([[dictionary objectForKey:@"slot_code"] isEqualToString:@"12A"]) {
                                slotFrame = CGRectMake(0, 0, scrollviewMain.frame.size.width, scrollviewMain.frame.size.height/87*10);
                            } else if ([[dictionary objectForKey:@"slot_code"] isEqualToString:@"12B"]) {
                                slotFrame = CGRectMake(0, scrollviewMain.frame.size.height/87*10, scrollviewMain.frame.size.width, scrollviewMain.frame.size.height/87*38.5);
                            } else if ([[dictionary objectForKey:@"slot_code"] isEqualToString:@"12C"]) {
                                slotFrame = CGRectMake(0, scrollviewMain.frame.size.height/87*48.5, scrollviewMain.frame.size.width, scrollviewMain.frame.size.height/87*38.5);
                            }
                            
                        } else if ([[themecode objectForKey:@"theme_code"] isEqualToString:@"13"]) {
                            if ([[dictionary objectForKey:@"slot_code"] isEqualToString:@"13A"]) {
                                slotFrame = CGRectMake(0, 0, scrollviewMain.frame.size.width, scrollviewMain.frame.size.height/87*43.5);
                            } else if ([[dictionary objectForKey:@"slot_code"] isEqualToString:@"13B"]) {
                                slotFrame = CGRectMake(0, scrollviewMain.frame.size.height/87*43.5, scrollviewMain.frame.size.width, scrollviewMain.frame.size.height/87*43.5);
                            }
                            
                        } else if ([[themecode objectForKey:@"theme_code"] isEqualToString:@"14"]) {
                            if ([[dictionary objectForKey:@"slot_code"] isEqualToString:@"14A"]) {
                                slotFrame = CGRectMake(0, 0, scrollviewMain.frame.size.width, scrollviewMain.frame.size.height/3);
                            } else if ([[dictionary objectForKey:@"slot_code"] isEqualToString:@"14B"]) {
                                slotFrame = CGRectMake(0, scrollviewMain.frame.size.height/3, scrollviewMain.frame.size.width, scrollviewMain.frame.size.height/3);
                            } else if ([[dictionary objectForKey:@"slot_code"] isEqualToString:@"14C"]) {
                                slotFrame = CGRectMake(0, scrollviewMain.frame.size.height/3*2, scrollviewMain.frame.size.width, scrollviewMain.frame.size.height/3);
                            }
                            
                        } else if ([[themecode objectForKey:@"theme_code"] isEqualToString:@"15"]) {
                            if ([[dictionary objectForKey:@"slot_code"] isEqualToString:@"15A"]) {
                                slotFrame = CGRectMake(0, 0, scrollviewMain.frame.size.width, scrollviewMain.frame.size.width*100/540);
                            } else if ([[dictionary objectForKey:@"slot_code"] isEqualToString:@"15B"]) {
                                slotFrame = CGRectMake(0, scrollviewMain.frame.size.width*100/540, scrollviewMain.frame.size.width, scrollviewMain.frame.size.width*300/540);
                            } else if ([[dictionary objectForKey:@"slot_code"] isEqualToString:@"15C"]) {
                                slotFrame = CGRectMake(0, scrollviewMain.frame.size.width*400/540, scrollviewMain.frame.size.width, scrollviewMain.frame.size.width*300/540);
                            } else if ([[dictionary objectForKey:@"slot_code"] isEqualToString:@"15D"]) {
                                slotFrame = CGRectMake(0, scrollviewMain.frame.size.width*700/540, scrollviewMain.frame.size.width, scrollviewMain.frame.size.width*300/540);
                            } else if ([[dictionary objectForKey:@"slot_code"] isEqualToString:@"15E"]) {
                                slotFrame = CGRectMake(0, scrollviewMain.frame.size.width*1000/540, scrollviewMain.frame.size.width, scrollviewMain.frame.size.width*300/540);
                            } else if ([[dictionary objectForKey:@"slot_code"] isEqualToString:@"15F"]) {
                                slotFrame = CGRectMake(0, scrollviewMain.frame.size.width*1300/540, scrollviewMain.frame.size.width, scrollviewMain.frame.size.width*300/540);
                            }
                            
                            scrollviewMain.contentSize = CGSizeMake(scrollviewMain.frame.size.width, scrollviewMain.frame.size.width*1600/540);
                            
                        } else if ([[themecode objectForKey:@"theme_code"] isEqualToString:@"16"]) {
                            if ([[dictionary objectForKey:@"slot_code"] isEqualToString:@"16A"]) {
                                slotFrame = CGRectMake(0, 0, scrollviewMain.frame.size.width, scrollviewMain.frame.size.height/87*11);
                            } else if ([[dictionary objectForKey:@"slot_code"] isEqualToString:@"16B"]) {
                                slotFrame = CGRectMake(0, scrollviewMain.frame.size.height/87*11, scrollviewMain.frame.size.width, scrollviewMain.frame.size.height/87*11);
                            } else if ([[dictionary objectForKey:@"slot_code"] isEqualToString:@"16C"]) {
                                slotFrame = CGRectMake(0, scrollviewMain.frame.size.height/87*22, scrollviewMain.frame.size.width, scrollviewMain.frame.size.height/87*11);
                            } else if ([[dictionary objectForKey:@"slot_code"] isEqualToString:@"16D"]) {
                                slotFrame = CGRectMake(0, scrollviewMain.frame.size.height/87*33, scrollviewMain.frame.size.width, scrollviewMain.frame.size.height/87*43);
                            } else if ([[dictionary objectForKey:@"slot_code"] isEqualToString:@"16E"]) {
                                slotFrame = CGRectMake(0, scrollviewMain.frame.size.height/87*76, scrollviewMain.frame.size.width, scrollviewMain.frame.size.height/87*11);
                            }
                            
                        } else if ([[themecode objectForKey:@"theme_code"] isEqualToString:@"17"]) {
                            if ([[dictionary objectForKey:@"slot_code"] isEqualToString:@"17A"]) {
                                slotFrame = CGRectMake(0, 0, scrollviewMain.frame.size.width, scrollviewMain.frame.size.height/87*25.5);
                            } else if ([[dictionary objectForKey:@"slot_code"] isEqualToString:@"17B"]) {
                                slotFrame = CGRectMake(0, scrollviewMain.frame.size.height/87*25.5, scrollviewMain.frame.size.width/2, scrollviewMain.frame.size.height/87*18);
                            } else if ([[dictionary objectForKey:@"slot_code"] isEqualToString:@"17C"]) {
                                slotFrame = CGRectMake(scrollviewMain.frame.size.width/2, scrollviewMain.frame.size.height/87*25.5, scrollviewMain.frame.size.width/2, scrollviewMain.frame.size.height/87*18);
                            } else if ([[dictionary objectForKey:@"slot_code"] isEqualToString:@"17D"]) {
                                slotFrame = CGRectMake(0, scrollviewMain.frame.size.height/87*43.5, scrollviewMain.frame.size.width, scrollviewMain.frame.size.height/87*25.5);
                            } else if ([[dictionary objectForKey:@"slot_code"] isEqualToString:@"17E"]) {
                                slotFrame = CGRectMake(0, scrollviewMain.frame.size.height/87*69, scrollviewMain.frame.size.width/2, scrollviewMain.frame.size.height/87*18);
                            } else if ([[dictionary objectForKey:@"slot_code"] isEqualToString:@"17F"]) {
                                slotFrame = CGRectMake(scrollviewMain.frame.size.width/2, scrollviewMain.frame.size.height/87*69, scrollviewMain.frame.size.width/2, scrollviewMain.frame.size.height/87*18);
                            }
                            
                        } else if ([[themecode objectForKey:@"theme_code"] isEqualToString:@"18"]) {
                            if ([[dictionary objectForKey:@"slot_code"] isEqualToString:@"18A"]) {
                                slotFrame = CGRectMake(0, 0, scrollviewMain.frame.size.width/3, scrollviewMain.frame.size.height/87*20);
                            } else if ([[dictionary objectForKey:@"slot_code"] isEqualToString:@"18B"]) {
                                slotFrame = CGRectMake(scrollviewMain.frame.size.width/3, 0, scrollviewMain.frame.size.width/3, scrollviewMain.frame.size.height/87*20);
                            } else if ([[dictionary objectForKey:@"slot_code"] isEqualToString:@"18C"]) {
                                slotFrame = CGRectMake(scrollviewMain.frame.size.width/3*2, 0, scrollviewMain.frame.size.width/3, scrollviewMain.frame.size.height/87*20);
                            } else if ([[dictionary objectForKey:@"slot_code"] isEqualToString:@"18D"]) {
                                slotFrame = CGRectMake(0, scrollviewMain.frame.size.height/87*20, scrollviewMain.frame.size.width/3, scrollviewMain.frame.size.height/87*20);
                            } else if ([[dictionary objectForKey:@"slot_code"] isEqualToString:@"18E"]) {
                                slotFrame = CGRectMake(scrollviewMain.frame.size.width/3, scrollviewMain.frame.size.height/87*20, scrollviewMain.frame.size.width/3, scrollviewMain.frame.size.height/87*20);
                            } else if ([[dictionary objectForKey:@"slot_code"] isEqualToString:@"18F"]) {
                                slotFrame = CGRectMake(scrollviewMain.frame.size.width/3*2, scrollviewMain.frame.size.height/87*20, scrollviewMain.frame.size.width/3, scrollviewMain.frame.size.height/87*20);
                            } else if ([[dictionary objectForKey:@"slot_code"] isEqualToString:@"18G"]) {
                                slotFrame = CGRectMake(0, scrollviewMain.frame.size.height/87*40, scrollviewMain.frame.size.width/3, scrollviewMain.frame.size.height/87*20);
                            } else if ([[dictionary objectForKey:@"slot_code"] isEqualToString:@"18H"]) {
                                slotFrame = CGRectMake(scrollviewMain.frame.size.width/3, scrollviewMain.frame.size.height/87*40, scrollviewMain.frame.size.width/3, scrollviewMain.frame.size.height/87*20);
                            } else if ([[dictionary objectForKey:@"slot_code"] isEqualToString:@"18I"]) {
                                slotFrame = CGRectMake(scrollviewMain.frame.size.width/3*2, scrollviewMain.frame.size.height/87*40, scrollviewMain.frame.size.width/3, scrollviewMain.frame.size.height/87*20);
                            } else if ([[dictionary objectForKey:@"slot_code"] isEqualToString:@"18J"]) {
                                slotFrame = CGRectMake(0, scrollviewMain.frame.size.height/87*60, scrollviewMain.frame.size.width/3, scrollviewMain.frame.size.height/87*20);
                            } else if ([[dictionary objectForKey:@"slot_code"] isEqualToString:@"18K"]) {
                                slotFrame = CGRectMake(scrollviewMain.frame.size.width/3, scrollviewMain.frame.size.height/87*60, scrollviewMain.frame.size.width/3, scrollviewMain.frame.size.height/87*20);
                            } else if ([[dictionary objectForKey:@"slot_code"] isEqualToString:@"18L"]) {
                                slotFrame = CGRectMake(scrollviewMain.frame.size.width/3*2, scrollviewMain.frame.size.height/87*60, scrollviewMain.frame.size.width/3, scrollviewMain.frame.size.height/87*20);
                            } else if ([[dictionary objectForKey:@"slot_code"] isEqualToString:@"18M"]) {
                                slotFrame = CGRectMake(0, scrollviewMain.frame.size.height/87*80, scrollviewMain.frame.size.width, scrollviewMain.frame.size.height/87*7);
                            }
                            
                        } else if ([[themecode objectForKey:@"theme_code"] isEqualToString:@"19"]) {
                            if ([[dictionary objectForKey:@"slot_code"] isEqualToString:@"19A"]) {
                                slotFrame = CGRectMake(0, 0, scrollviewMain.frame.size.width, scrollviewMain.frame.size.height/87*6);
                            } else if ([[dictionary objectForKey:@"slot_code"] isEqualToString:@"19B"]) {
                                slotFrame = CGRectMake(0, scrollviewMain.frame.size.height/87*6, scrollviewMain.frame.size.width/3, scrollviewMain.frame.size.height/87*25);
                            } else if ([[dictionary objectForKey:@"slot_code"] isEqualToString:@"19C"]) {
                                slotFrame = CGRectMake(scrollviewMain.frame.size.width/3, scrollviewMain.frame.size.height/87*6, scrollviewMain.frame.size.width/3, scrollviewMain.frame.size.height/87*25);
                            } else if ([[dictionary objectForKey:@"slot_code"] isEqualToString:@"19D"]) {
                                slotFrame = CGRectMake(scrollviewMain.frame.size.width/3*2, scrollviewMain.frame.size.height/87*6, scrollviewMain.frame.size.width/3, scrollviewMain.frame.size.height/87*25);
                            } else if ([[dictionary objectForKey:@"slot_code"] isEqualToString:@"19E"]) {
                                slotFrame = CGRectMake(0, scrollviewMain.frame.size.height/87*31, scrollviewMain.frame.size.width/3, scrollviewMain.frame.size.height/87*25);
                            } else if ([[dictionary objectForKey:@"slot_code"] isEqualToString:@"19F"]) {
                                slotFrame = CGRectMake(scrollviewMain.frame.size.width/3, scrollviewMain.frame.size.height/87*31, scrollviewMain.frame.size.width/3, scrollviewMain.frame.size.height/87*25);
                            } else if ([[dictionary objectForKey:@"slot_code"] isEqualToString:@"19G"]) {
                                slotFrame = CGRectMake(scrollviewMain.frame.size.width/3*2, scrollviewMain.frame.size.height/87*31, scrollviewMain.frame.size.width/3, scrollviewMain.frame.size.height/87*25);
                            } else if ([[dictionary objectForKey:@"slot_code"] isEqualToString:@"19H"]) {
                                slotFrame = CGRectMake(0, scrollviewMain.frame.size.height/87*56, scrollviewMain.frame.size.width/3, scrollviewMain.frame.size.height/87*25);
                            } else if ([[dictionary objectForKey:@"slot_code"] isEqualToString:@"19I"]) {
                                slotFrame = CGRectMake(scrollviewMain.frame.size.width/3, scrollviewMain.frame.size.height/87*56, scrollviewMain.frame.size.width/3, scrollviewMain.frame.size.height/87*25);
                            } else if ([[dictionary objectForKey:@"slot_code"] isEqualToString:@"19J"]) {
                                slotFrame = CGRectMake(scrollviewMain.frame.size.width/3*2, scrollviewMain.frame.size.height/87*56, scrollviewMain.frame.size.width/3, scrollviewMain.frame.size.height/87*25);
                            } else if ([[dictionary objectForKey:@"slot_code"] isEqualToString:@"19K"]) {
                                slotFrame = CGRectMake(0, scrollviewMain.frame.size.height/87*81, scrollviewMain.frame.size.width, scrollviewMain.frame.size.height/87*6);
                            }
                            
                        } else if ([[themecode objectForKey:@"theme_code"] isEqualToString:@"20"]) {
                            if ([[dictionary objectForKey:@"slot_code"] isEqualToString:@"20A"]) {
                                slotFrame = CGRectMake(0, 0, scrollviewMain.frame.size.width, scrollviewMain.frame.size.height/87*30);
                            } else if ([[dictionary objectForKey:@"slot_code"] isEqualToString:@"20B"]) {
                                slotFrame = CGRectMake(0, scrollviewMain.frame.size.height/87*30, scrollviewMain.frame.size.width/5, scrollviewMain.frame.size.height/87*11);
                            } else if ([[dictionary objectForKey:@"slot_code"] isEqualToString:@"20C"]) {
                                slotFrame = CGRectMake(scrollviewMain.frame.size.width/5, scrollviewMain.frame.size.height/87*30, scrollviewMain.frame.size.width/5, scrollviewMain.frame.size.height/87*11);
                            } else if ([[dictionary objectForKey:@"slot_code"] isEqualToString:@"20D"]) {
                                slotFrame = CGRectMake(scrollviewMain.frame.size.width/5*2, scrollviewMain.frame.size.height/87*30, scrollviewMain.frame.size.width/5, scrollviewMain.frame.size.height/87*11);
                            } else if ([[dictionary objectForKey:@"slot_code"] isEqualToString:@"20E"]) {
                                slotFrame = CGRectMake(scrollviewMain.frame.size.width/5*3, scrollviewMain.frame.size.height/87*30, scrollviewMain.frame.size.width/5, scrollviewMain.frame.size.height/87*11);
                            } else if ([[dictionary objectForKey:@"slot_code"] isEqualToString:@"20F"]) {
                                slotFrame = CGRectMake(scrollviewMain.frame.size.width/5*4, scrollviewMain.frame.size.height/87*30, scrollviewMain.frame.size.width/5, scrollviewMain.frame.size.height/87*11);
                            } else if ([[dictionary objectForKey:@"slot_code"] isEqualToString:@"20G"]) {
                                slotFrame = CGRectMake(0, scrollviewMain.frame.size.height/87*41, scrollviewMain.frame.size.width, scrollviewMain.frame.size.height/87*23);
                            } else if ([[dictionary objectForKey:@"slot_code"] isEqualToString:@"20H"]) {
                                slotFrame = CGRectMake(0, scrollviewMain.frame.size.height/87*64, scrollviewMain.frame.size.width, scrollviewMain.frame.size.height/87*23);
                            } */
                            
                        } else {
                            
                            /*UIScrollView *scrollview = [[UIScrollView alloc] initWithFrame:CGRectMake(0, 0, self.frame.size.width, self.frame.size.height-64)];
                             [scrollview setBackgroundColor:[UIColor clearColor]];
                             [self addSubview:scrollview]; */
                            
                            UILabel *msgTitle;
                            if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone) {
                                // For iPhone
                                msgTitle = [[UILabel alloc] initWithFrame: CGRectMake(10, 10, self.frame.size.width-10, 60)];
                            } else {
                                msgTitle = [[UILabel alloc] initWithFrame: CGRectMake(10, 10, self.frame.size.width-10, 140)];
                            }
                            [msgTitle setFont:[UIFont boldSystemFontOfSize:18]];
                            msgTitle.textColor = [UIColor blackColor];
                            msgTitle.textAlignment = NSTextAlignmentCenter;
                            msgTitle.backgroundColor = [UIColor clearColor];
                            msgTitle.numberOfLines = 0;
                            msgTitle.text = NSLocalizedString(@"msgTitleDownload", nil);
                            [self addSubview:msgTitle];
                            
                            UILabel *msgContent;
                            if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone) {
                                // For iPhone
                                msgContent = [[UILabel alloc] initWithFrame: CGRectMake(10, 80, self.frame.size.width-10, 60)];
                            } else {
                                msgContent = [[UILabel alloc] initWithFrame: CGRectMake(10, 160, self.frame.size.width-10, 140)];
                            }
                            [msgContent setFont:[UIFont systemFontOfSize:16]];
                            msgContent.textColor = [UIColor blackColor];
                            msgContent.textAlignment = NSTextAlignmentCenter;
                            msgContent.backgroundColor = [UIColor clearColor];
                            msgContent.numberOfLines = 0;
                            msgContent.text = NSLocalizedString(@"msgContentDownload", nil);
                            [self addSubview:msgContent];
                            
                            sqlite3 *database;
                            ios_link = @"";
                            
                            if(sqlite3_open([appdelegate.databasePath UTF8String], &database) == SQLITE_OK) {
                                // Setup the SQL Statement and compile it for faster access
                                
                                NSString *theme_object = [[NSString alloc] initWithFormat:@"Select ios_link from app_setup"];
                                //NSLog(@"theme_objecttheme2:: %@", theme_object);
                                const char *sqlStatement1 = [theme_object UTF8String];
                                sqlite3_stmt *compiledStatement1;
                                if(sqlite3_prepare_v2(database, sqlStatement1, -1, &compiledStatement1, NULL) == SQLITE_OK) {
                                    
                                    while(sqlite3_step(compiledStatement1) == SQLITE_ROW) {
                                        
                                        ios_link = [NSString stringWithUTF8String:(char *)sqlite3_column_text(compiledStatement1, 0)];
                                        
                                    }
                                }
                                // Release the compiled statement from memory
                                sqlite3_finalize(compiledStatement1);
                            }
                            sqlite3_close(database);
                            
                            if ([ios_link length]>0) {
                                
                                UIButton *button = [UIButton buttonWithType:UIButtonTypeRoundedRect];
                                [button addTarget:self action:@selector(aMethod:) forControlEvents:UIControlEventTouchUpInside];
                                [button setTitle:NSLocalizedString(@"buttonDownload", nil) forState:UIControlStateNormal];
                                if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone) {
                                    // For iPhone
                                    button.frame = CGRectMake(self.frame.size.width/2-100, 150, 200, 40);
                                } else {
                                    button.frame = CGRectMake(self.frame.size.width/2-100, 310, 200, 60);
                                }
                                
                                [self addSubview:button];
                                
                            }
                        }
                        
                        //NSLog(@"bbb:%@", [dictionary objectForKey:@"object_type"]);
                        if (![[themecode objectForKey:@"theme_code"] isEqualToString:@"3"]) {
                            if ([slotGallery isEqualToString:[dictionary objectForKey:@"slot_code"]]) {
                                scrollview.frame = slotFrame;
                                [self showaction:dictionary :scrollview];
                            } else {
                                if ([[dictionary objectForKey:@"object_type"] isEqualToString:@"feed"]) {
                                    
                                    [self addFeedUrl:i :slotFrame :scrollviewMain :[dictionary objectForKey:@"attribute"] :[themecode objectForKey:@"theme_code"]];
                                } else {
                                    
                                    UIButton *myButton = [UIButton buttonWithType:UIButtonTypeCustom];
                                    myButton.frame = slotFrame;
                                    [self addTextImageButton:i :listOfObject :myButton :scrollviewMain :[dictionary objectForKey:@"attribute"]];
                                    //[[viewCtrl navigationController] setNavigationBarHidden:NO animated:YES];
                                }
                            }
                            //[myButton setBackgroundColor:[self colorWithHexString:appdelegate.themeColorCode1]];
                        }
                        dictionary =nil;
                    }
                    
                }
            }
            
            [self setImagesGallery:scrollview];
            slotGallery = nil;
            
            if (Menu) {
                if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone) {
                    // For iPhone
                    menuView = [[UIView alloc] initWithFrame:CGRectMake(0, scrollviewMain.frame.size.height-70, self.frame.size.width, 70)];
                    menuView.backgroundColor = [UIColor clearColor];
                    [self addSubview: menuView];
                } else {
                    menuView = [[UIView alloc] initWithFrame:CGRectMake(0, scrollviewMain.frame.size.height-100, self.frame.size.width, 100)];
                    menuView.backgroundColor = [UIColor clearColor];
                    [self addSubview: menuView];
                }
                
                [self bringSubviewToFront:menuView];
            }
            
            //Menu
            if ([listOfMenu count]>0) {
                int m;
                for (m=0; m<[listOfMenu count]; m++) {
                    NSDictionary *dictionary = [listOfMenu objectAtIndex:m];
                    //NSLog(@"listOfMenuList : %@", [listOfMenu objectAtIndex:m]);
                    
                    if (Menu) {
                        //NSLog(@"MenuMenu");
                        CGRect slotFrame;
                        if ([[dictionary objectForKey:@"slot_code"] isEqualToString:@"MA"]) {
                            if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone) {
                                slotFrame = CGRectMake(0, 35, self.frame.size.width/3, 35);
                            } else {
                                slotFrame = CGRectMake(0, 50, self.frame.size.width/3, 50);
                            }
                            btnMenuMA.hidden = NO;
                            btnMenuMA.frame = slotFrame;
                            [self addTextImageButton:m :listOfMenu :btnMenuMA :nil :[dictionary objectForKey:@"attribute"]];
                        } else if ([[dictionary objectForKey:@"slot_code"] isEqualToString:@"MB"]) {
                            if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone) {
                                slotFrame = CGRectMake(self.frame.size.width/3,  35, self.frame.size.width/3, 35);
                            } else {
                                slotFrame = CGRectMake(self.frame.size.width/3,  50, self.frame.size.width/3, 50);
                            }
                            btnMenuMB.hidden = NO;
                            btnMenuMB.frame = slotFrame;
                            [self addTextImageButton:m :listOfMenu :btnMenuMB :nil :[dictionary objectForKey:@"attribute"]];
                        } else if ([[dictionary objectForKey:@"slot_code"] isEqualToString:@"MC"]) {
                            if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone) {
                                slotFrame = CGRectMake(self.frame.size.width/3*2,  35, self.frame.size.width/3, 35);
                            } else {
                                slotFrame = CGRectMake(self.frame.size.width/3*2,  50, self.frame.size.width/3, 50);
                            }
                            btnMenuMC.hidden = NO;
                            btnMenuMC.frame = slotFrame;
                            [self addTextImageButton:m :listOfMenu :btnMenuMC :nil :[dictionary objectForKey:@"attribute"]];
                        } else if ([[dictionary objectForKey:@"slot_code"] isEqualToString:@"MD"]) {
                            if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone) {
                                slotFrame = CGRectMake(0, 35, self.frame.size.width/3, 35);
                            } else {
                                slotFrame = CGRectMake(0,  50, self.frame.size.width/3, 50);
                            }
                            btnMenuMD.hidden = NO;
                            btnMenuMD.frame = slotFrame;
                            [self addTextImageButton:m :listOfMenu :btnMenuMD :nil :[dictionary objectForKey:@"attribute"]];
                        } else if ([[dictionary objectForKey:@"slot_code"] isEqualToString:@"ME"]) {
                            if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone) {
                                slotFrame = CGRectMake(self.frame.size.width/3,  35, self.frame.size.width/3, 35);
                            } else {
                                slotFrame = CGRectMake(self.frame.size.width/3,  50, self.frame.size.width/3, 50);
                            }
                            btnMenuME.hidden = NO;
                            btnMenuME.frame = slotFrame;
                            [self addTextImageButton:m :listOfMenu :btnMenuME :nil :[dictionary objectForKey:@"attribute"]];
                        } else if ([[dictionary objectForKey:@"slot_code"] isEqualToString:@"MF"]) {
                            if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone) {
                                slotFrame = CGRectMake(self.frame.size.width/3*2,  35, self.frame.size.width/3, 35);
                            } else {
                                slotFrame = CGRectMake(self.frame.size.width/3*2,  50, self.frame.size.width/3, 50);
                            }
                            btnMenuMF.hidden = NO;
                            btnMenuMF.frame = slotFrame;
                            [self addTextImageButton:m :listOfMenu :btnMenuMF :nil :[dictionary objectForKey:@"attribute"]];
                        }
                    }
                }
            }
                
        }
        
        /*** Push Notification 27Aug2015 ***/
        //NSLog(@"notify_actiontypeviewCtrl: %@", appdelegate.notify_actiontype);
        //NSLog(@"notify_actionvalueviewCtrl: %@", appdelegate.notify_actionvalue);
        //NSLog(@"notify_contentidviewCtrl: %@", appdelegate.notify_contentid);
        if ([appdelegate.notify_actiontype isEqual:[NSNull null]]) {
            appdelegate.notify_actiontype = @"";
        }
        if ([appdelegate.notify_actionvalue isEqual:[NSNull null]]) {
            appdelegate.notify_actionvalue = @"";
        }
        if ([appdelegate.notify_contentid isEqual:[NSNull null]]) {
            appdelegate.notify_contentid = @"";
        }
        if (![appdelegate.notify_actiontype isEqualToString:@""] && ![appdelegate.notify_actionvalue isEqualToString:@""]) {
            if ([appdelegate.notify_actiontype isEqualToString:@"plugin"]) {
                [self goPluginPageNotification];
            } else if ([appdelegate.notify_actiontype isEqualToString:@"page"]) {
                [self goNextPageNotification];
            }
            
            //return false;
        }
        /*** Push Notification 27Aug2015 ***/
    }
    
    return self;
}

- (void) aMethod:(UIButton*)button {
    
    NSString *iTunesLink = ios_link;
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:iTunesLink]];
    
    //[[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"itms-apps://yourAppLinkHere"]];
    
}

- (void) setImagesGallery:(UIScrollView *)scrollview {

    if ([self.galleryImages count]>0) {
        scrollview.tag = 1;
        scrollview.autoresizingMask=UIViewAutoresizingNone;
        [self setupScrollView:scrollview];
    }
}

- (UIColor *) colorWithHexString:(NSString *)hexString {
    NSString *colorString = [[hexString stringByReplacingOccurrencesOfString: @"#" withString: @""] uppercaseString];
    CGFloat alpha, red, blue, green;
    alpha = 1.0f;
    red   = [self colorComponentFrom: colorString start: 0 length: 2];
    green = [self colorComponentFrom: colorString start: 2 length: 2];
    blue  = [self colorComponentFrom: colorString start: 4 length: 2];
    return [UIColor colorWithRed: red green: green blue: blue alpha: alpha];
}

- (CGFloat) colorComponentFrom:(NSString *)string start:(NSUInteger)start length:(NSUInteger)length {
    NSString *substring = [string substringWithRange: NSMakeRange(start, length)];
    NSString *fullHex = length == 2 ? substring : [NSString stringWithFormat: @"%@%@", substring, substring];
    unsigned hexComponent;
    [[NSScanner scannerWithString: fullHex] scanHexInt: &hexComponent];
    return hexComponent / 255.0;
}

- (void)setupScrollView:(UIScrollView*)scrMain {
    // we have 10 images here.
    // we will add all images into a scrollView & set the appropriate size.
    
    for (int i=1; i<=[self.galleryImages count]; i++) {
        
        UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
        button.frame = CGRectMake((i-1)*scrMain.frame.size.width, 0, scrMain.frame.size.width, scrMain.frame.size.height);
        //button.tag = i;
        NSDictionary *dictionary = [self.galleryImages objectAtIndex:i-1];
        //NSString *filePath = [self.galleryImages objectAtIndex:i-1];
        NSString *filePath = [dictionary objectForKey:@"image"];
        
        NSRange end = [filePath rangeOfString:@"/" options:NSBackwardsSearch];
        NSString *shortString;
        if (end.location != NSNotFound) {
            shortString =[filePath substringWithRange:NSMakeRange(end.location+1, filePath.length-(end.location+1))];
        } else {
            shortString = filePath;
        }
        
        BOOL success;
        
        // Create a FileManager object, we will use this to check the status
        // of the database and to copy it over if required
        NSFileManager *fileManager = [NSFileManager defaultManager];
        
        // Check if the database has already been created in the users filesystem
        success = [fileManager fileExistsAtPath:[[[NSBundle mainBundle] resourcePath] stringByAppendingPathComponent:shortString]];
        
        if ([[dictionary objectForKey:@"action_value"] intValue] > 0) {
            if ([[dictionary objectForKey:@"action_type"] isEqualToString:@"page"] || [[dictionary objectForKey:@"action_type"] isEqualToString:@"plugin"]) {
                button.tag = [[dictionary objectForKey:@"action_value"] intValue];
                
                if ([[dictionary objectForKey:@"action_type"] isEqualToString:@"page"]) {
                    [button addTarget:self
                               action:@selector(checkButtonClick:)
                     forControlEvents:UIControlEventTouchUpInside];
                } else {
                    [button addTarget:self
                               action:@selector(checkButtonPluginClick:)
                     forControlEvents:UIControlEventTouchUpInside];
                }
            }
        }
        //UIImageView *imgV = [[UIImageView alloc] initWithFrame:CGRectMake((i-1)*scrMain.frame.size.width, 0, scrMain.frame.size.width, scrMain.frame.size.height)];
        UIImageView *imgV = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, button.frame.size.width, button.frame.size.height)];
        //UIImageView *imgV = [[UIImageView alloc] initWithFrame:CGRectMake((button.frame.size.width/4)/2, 0, button.frame.size.width-(button.frame.size.width/4), button.frame.size.height)];
        // set scale to fill
        [scrMain addSubview:button];
        
        [button addSubview:imgV];
        
        __block UIActivityIndicatorView *activityIndicator;
        __weak UIImageView *weakImageView = imgV;
        NSString *imagePath;
        if(success) {
            imagePath = [[[NSBundle mainBundle] resourcePath] stringByAppendingPathComponent:shortString];
            [weakImageView setImage:[UIImage imageNamed:shortString]];
        } else {
            imagePath = filePath;
            /*[weakImageView setImageWithURL:[NSURL URLWithString:imagePath] placeholderImage:[UIImage imageNamed:@"No-Image-icon.png"] options:SDWebImageProgressiveDownload progress:^(NSInteger receivedSize, NSInteger expectedSize)
             {
                 if (!activityIndicator)
                 {
                     [weakImageView addSubview:activityIndicator = [UIActivityIndicatorView.alloc initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray]];
                     activityIndicator.center = weakImageView.center;
                     [activityIndicator startAnimating];
                 }
             }
                                 completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType)
             {
                 [activityIndicator removeFromSuperview];
                 activityIndicator = nil;
             }]; */
            dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0), ^ {
                [weakImageView sd_setImageWithURL:[NSURL URLWithString:imagePath]
                                 placeholderImage:[UIImage imageNamed:@"No-Image-icon.png"]
                                          options:SDWebImageProgressiveDownload
                                         progress:^(NSInteger receivedSize, NSInteger expectedSize, NSURL *targetURL) {
                                             dispatch_async(dispatch_get_main_queue(), ^ {
                                                 if (!activityIndicator) {
                                                     
                                                     [weakImageView addSubview:activityIndicator = [UIActivityIndicatorView.alloc initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray]];
                                                     activityIndicator.center = weakImageView.center;
                                                     [activityIndicator startAnimating];
                                                 }
                                             });
                                         }
                                        completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
                                            dispatch_async(dispatch_get_main_queue(), ^ {
                                                //[MBProgressHUD hideHUDForView:self.view animated:YES];
                                                [activityIndicator removeFromSuperview];
                                                activityIndicator = nil;
                                            });
                                        }];
            });
        }
        //NSLog(@"shortStringimagePath : %@", imagePath);
        imgV.contentMode  = UIViewContentModeScaleAspectFit;
        [imgV setClipsToBounds:YES];
    }
    // set the content size to 10 image width
    [scrMain setContentSize:CGSizeMake(scrMain.frame.size.width*[self.galleryImages count], scrMain.frame.size.height)];
    // enable timer after each 2 seconds for scrolling.
    if ([self.galleryImages count]>1) {
        [NSTimer scheduledTimerWithTimeInterval:delaytime target:self selector:@selector(scrollingTimer) userInfo:nil repeats:YES];
    }

}

- (void)scrollingTimer {
    // access the scroll view with the tag
    UIScrollView *scrMain = (UIScrollView*) [self viewWithTag:1];
    // same way, access pagecontroll access
    UIPageControl *pgCtr = (UIPageControl*) [self viewWithTag:12];
    // get the current offset ( which page is being displayed )
    CGFloat contentOffset = scrMain.contentOffset.x;
    // calculate next page to display
    int nextPage = (int)(contentOffset/scrMain.frame.size.width) + 1 ;
    // if page is not 10, display it
    if( nextPage!=[self.galleryImages count] )  {
        [scrMain scrollRectToVisible:CGRectMake(nextPage*scrMain.frame.size.width, 0, scrMain.frame.size.width, scrMain.frame.size.height) animated:NO];
        pgCtr.currentPage=nextPage;
        // else start sliding form 1 :)
    } else {
        [scrMain scrollRectToVisible:CGRectMake(0, 0, scrMain.frame.size.width, scrMain.frame.size.height) animated:NO];
        pgCtr.currentPage=0;
    }
}

-(void) addFeedUrl :(int)index :(CGRect)slotFrame :(UIScrollView *)scrollview :(NSString *)attribute :themecode {
    
    viewFeed = [[UIScrollView alloc] initWithFrame:slotFrame];
    viewFeed.backgroundColor = [UIColor clearColor];
    
    myWebView = [[UIWebView alloc] init];
    //[webView setFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height)];
    myWebView.delegate = self;
    myWebView.opaque = NO;
    myWebView.backgroundColor = [UIColor clearColor];
    
    //initialize the activity indicator
    
    processIndicator=[[UIActivityIndicatorView alloc]initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhiteLarge];
    
    //Change the color of the indicator, this override the color set by UIActivityIndicatorViewStyleWhiteLarge
    processIndicator.color=[UIColor blackColor];
    
    //Put the indicator on the center of the webview
    [processIndicator setCenter:scrollview.center];
    
    //Add the indicator to the webView to make it visible
    [myWebView addSubview:processIndicator];
    //[webView setScalesPageToFit:YES];
    
    [processIndicator startAnimating];
    
    sqlite3 *database;
    NSString *feedlink;
    if(sqlite3_open([appdelegate.databasePath UTF8String], &database) == SQLITE_OK) {
        NSString *theme_objectSlot = [[NSString alloc] initWithFormat:@"Select id, plugin_value3 from theme_object_plugin where id=%@", attribute];
        //NSLog(@"theme_objectSlot:: %@", theme_objectSlot);
        const char *sqlStatement2 = [theme_objectSlot UTF8String];
        sqlite3_stmt *compiledStatement2;
        if(sqlite3_prepare_v2(database, sqlStatement2, -1, &compiledStatement2, NULL) == SQLITE_OK) {
            
            while(sqlite3_step(compiledStatement2) == SQLITE_ROW) {
                
                feedlink = [NSString stringWithUTF8String:(char *)sqlite3_column_text(compiledStatement2, 1)];
                
            }
        }
        // Release the compiled statement from memory
        sqlite3_finalize(compiledStatement2);
    }
    sqlite3_close(database);
    
    //NSLog(@"feedlink:: %@", feedlink);
    
    //feedlink = @"http://www.getsnapps.com/";
   /* myWebView.delegate = self;
    
    //initialize the activity indicator
    
    activityIndicator=[[UIActivityIndicatorView alloc]initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhiteLarge];
    
    //Change the color of the indicator, this override the color set by UIActivityIndicatorViewStyleWhiteLarge
    activityIndicator.color=[UIColor blackColor];
    
    //Put the indicator on the center of the webview
    [activityIndicator setCenter:scrollview.center];
    
    //Add the indicator to the webView to make it visible
    [myWebView addSubview:activityIndicator];
    //[myWebView setScalesPageToFit:YES];
    
    [activityIndicator startAnimating];
    
    [myWebView loadRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:feedlink]]];
    //NSArray *docPaths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    //NSString *docPath = [docPaths objectAtIndex:0];
    //[myWebView loadRequest:[NSURLRequest requestWithURL:[NSURL fileURLWithPath:[docPath stringByAppendingPathComponent:@"page.html"]]]];
    //NSURL *htmlFile = [NSURL fileURLWithPath:[[NSBundle mainBundle] pathForResource:@"index" ofType:@"html"]];
    //[_viewWeb loadRequest:[NSURLRequest requestWithURL:htmlFile]]; */
    /*myWebView = [[UIWebView alloc] init];
    //myWebView.frame = slotFrame;
    myWebView.frame = CGRectMake(0, 0, slotFrame.size.width, slotFrame.size.height);
    //[myWebView setScalesPageToFit:YES];
    //webView.scalesPageToFit = YES;
    //[self.view addSubview:webView];
    
    //initialize the activity indicator
    
    processIndicator=[[UIActivityIndicatorView alloc]initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhiteLarge];
    
    //Change the color of the indicator, this override the color set by UIActivityIndicatorViewStyleWhiteLarge
    processIndicator.color=[UIColor blackColor];
    
    //Put the indicator on the center of the webview
    [processIndicator setCenter:CGPointMake(myWebView.center.x, slotFrame.size.height/2)];
    
    //Add the indicator to the webView to make it visible
    [myWebView addSubview:processIndicator];
    myWebView.scrollView.scrollEnabled = YES;
    //myWebView.contentMode = UIViewContentModeScaleAspectFit;
    myWebView.scalesPageToFit = YES;
    myWebView.scrollView.bounces = NO;
    myWebView.opaque = NO;
    myWebView.backgroundColor = [UIColor clearColor];
    
    [processIndicator startAnimating];
    
    //NSURLRequest *request = [NSURLRequest requestWithURL:[NSURL URLWithString:feedlink]
                                           //  cachePolicy:NSURLRequestUseProtocolCachePolicy
                                         //timeoutInterval:60.0];
    
    //[myWebView loadRequest:request];
    
    NSURL *url = [NSURL URLWithString:feedlink];
    //NSLog(@"feedlink::%@", feedlink);
    NSString *replacedfeedlink;
    NSCharacterSet *doNotWant = [NSCharacterSet characterSetWithCharactersInString:@"/:.?=&"];
    replacedfeedlink = [[feedlink componentsSeparatedByCharactersInSet: doNotWant] componentsJoinedByString: @""];
    //NSLog(@"%@", replacedfeedlink);
    
    // Determile cache file path
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *filePath = [NSString stringWithFormat:@"%@/%@%@", [paths objectAtIndex:0], replacedfeedlink, @".html"];
    //NSLog(@"filePath::%@", filePath);
    // Download and write to file
    
    NSData *urlData = [NSData dataWithContentsOfURL:url];
    [urlData writeToFile:filePath atomically:YES];
    
    // Load file in UIWebView
    [myWebView loadRequest:[NSURLRequest requestWithURL:[NSURL fileURLWithPath:filePath]]];
    //[myWebView loadRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:feedlink]]];
    //NSURLConnection *conn =
    //[NSURLConnection connectionWithRequest:request delegate:self];
    feedlink = nil;
    
    //scrollview.frame = myWebView.frame;
    
    //if (scrollview == nil) {
        //[self addSubview:myWebView];
    //} else { */
    
    NSString *urlAddress = feedlink;
    NSRange rangeurl = [urlAddress rangeOfString:@"?"];
    if(rangeurl.location != NSNotFound) {
        //found
        urlAddress = [NSString stringWithFormat: @"%@&f=1", urlAddress];
    } else {
        urlAddress = [NSString stringWithFormat: @"%@?f=1", urlAddress];
    }
    //NSLog(@"urlAddresshtml5 %@", urlAddress);
    NSURL *url = [NSURL URLWithString:urlAddress];
    
    NSString *html = [NSString stringWithContentsOfURL:url encoding:NSUTF8StringEncoding error:nil];
    NSRange range = [html rangeOfString:@"<body"];
    //NSLog(@"html %@", html);
    if(range.location != NSNotFound) {
        // Adjust style for mobile
        //float inset = 40;
        NSString *style = [NSString stringWithFormat:@"<style>div {max-width: %d%%; width: %fpx;height: %fpx;}</style>", 100, viewFeed.frame.size.width, viewFeed.frame.size.height];
        html = [NSString stringWithFormat:@"%@%@%@", [html substringToIndex:range.location], style, [html substringFromIndex:range.location]];
    }
    
    [myWebView loadHTMLString:html baseURL:url]; //change to loadRequest
    //NSURL *url = [NSURL URLWithString:[urlAddress stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]];
    //NSLog(@"url %@", url);
    //[myWebView loadRequest:[NSURLRequest requestWithURL:url]];
    
    float h;
    h = [[myWebView stringByEvaluatingJavaScriptFromString: @"document.document.height;"] integerValue];
    NSString *heightString = [myWebView stringByEvaluatingJavaScriptFromString:@"document.body.offsetHeight;"];
    //NSLog(@"web content is %@ high",heightString);
    h = [heightString floatValue]; // convert from string to float plus some extra points because calculation is sometimes one line short
    h = h + 7700;
    myWebView.frame = CGRectMake(-10, 0, viewFeed.frame.size.width, viewFeed.frame.size.height);
    //viewFeed.frame = CGRectMake(0, 0, viewFeed.frame.size.width, viewFeed.frame.size.height);
    viewFeed.contentSize = CGSizeMake(viewFeed.frame.size.width, h);
    viewFeed.bounces = NO;
    scrollview.bounces = NO;
    //NSLog(@"web view is %f width", myWebView.frame.size.width);
    [viewFeed addSubview:myWebView];
    //myWebView.delegate = self;
    [scrollview addSubview:viewFeed];
    
    //}
    //myWebView = nil;
}

/*- (NSCachedURLResponse *)connection:(NSURLConnection *)connection willCacheResponse:(NSCachedURLResponse *)cachedResponse
{
    NSURLResponse *response = cachedResponse.response;
    if ([response isKindOfClass:NSHTTPURLResponse.class]) return cachedResponse;
    
    NSHTTPURLResponse *HTTPResponse = (NSHTTPURLResponse*)response;
    NSDictionary *headers = HTTPResponse.allHeaderFields;
    if (headers[@"Cache-Control"]) return cachedResponse;
    
    NSMutableDictionary *modifiedHeaders = headers.mutableCopy;
    modifiedHeaders[@"Cache-Control"] = @"max-age=60";
    NSHTTPURLResponse *modifiedResponse = [[NSHTTPURLResponse alloc]
                                           initWithURL:HTTPResponse.URL
                                           statusCode:HTTPResponse.statusCode
                                           HTTPVersion:@"HTTP/1.1"
                                           headerFields:modifiedHeaders];
    
    cachedResponse = [[NSCachedURLResponse alloc]
                      initWithResponse:modifiedResponse
                      data:cachedResponse.data
                      userInfo:cachedResponse.userInfo
                      storagePolicy:cachedResponse.storagePolicy];
    return cachedResponse;
} */

-(void) addTextImageButton:(int)index :(NSMutableArray *)arrayList :(UIButton *)buttonName :(UIScrollView *)scrollview :(NSString *)attribute {
    
    buttonName.backgroundColor = [UIColor clearColor];
    NSDictionary *dictionary = [arrayList objectAtIndex:index];
    //NSLog(@"addTextImageButtonobject_type : %@", [dictionary objectForKey:@"object_type"]);
    if ([[dictionary objectForKey:@"object_type"] isEqualToString:@"img"]) {
        NSString *imgURL = [dictionary objectForKey:@"object_value"];
        
        NSRange end = [imgURL rangeOfString:@"/" options:NSBackwardsSearch];
        NSString *shortString;
        if (end.location != NSNotFound) {
            shortString =[imgURL substringWithRange:NSMakeRange(end.location+1, imgURL.length-(end.location+1))];
        } else {
            shortString = imgURL;
        }
        
        BOOL success;
        
        // Create a FileManager object, we will use this to check the status
        // of the database and to copy it over if required
        NSFileManager *fileManager = [NSFileManager defaultManager];
        
        // Check if the database has already been created in the users filesystem
        success = [fileManager fileExistsAtPath:[[[NSBundle mainBundle] resourcePath] stringByAppendingPathComponent:shortString]];
        //NSLog(@"shortString %@", shortString);
        if ([imgURL length]>0)
        {
            //[buttonName setBackgroundImageWithURL:[NSURL URLWithString:imgURL] forState:UIControlStateNormal placeholderImage:[UIImage imageNamed:@"No-Image-icon.png"]];
            UIImageView *imgV = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, buttonName.frame.size.width, buttonName.frame.size.height)];
            
            [buttonName addSubview:imgV];
            
            __block UIActivityIndicatorView *activityIndicator;
            __weak UIImageView *weakImageView = imgV;
            NSString *imagePath;
            if(success) {
                imagePath = [[[NSBundle mainBundle] resourcePath] stringByAppendingPathComponent:shortString];
                [weakImageView setImage:[UIImage imageNamed:shortString]];
            } else {
                imagePath = imgURL;
                /*[weakImageView setImageWithURL:[NSURL URLWithString:imagePath] placeholderImage:nil options:SDWebImageProgressiveDownload progress:^(NSInteger receivedSize, NSInteger expectedSize)
                 {
                     if (!activityIndicator)
                     {
                         [weakImageView addSubview:activityIndicator = [UIActivityIndicatorView.alloc initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray]];
                         activityIndicator.center = weakImageView.center;
                         [activityIndicator startAnimating];
                     }
                 }
                                     completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType)
                 {
                     [activityIndicator removeFromSuperview];
                     activityIndicator = nil;
                 }]; */
                dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0), ^ {
                    [weakImageView sd_setImageWithURL:[NSURL URLWithString:imagePath]
                                     placeholderImage:nil
                                              options:SDWebImageProgressiveDownload
                                             progress:^(NSInteger receivedSize, NSInteger expectedSize, NSURL *targetURL) {
                                                 dispatch_async(dispatch_get_main_queue(), ^ {
                                                     if (!activityIndicator) {
                                                         
                                                         [weakImageView addSubview:activityIndicator = [UIActivityIndicatorView.alloc initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray]];
                                                         activityIndicator.center = weakImageView.center;
                                                         [activityIndicator startAnimating];
                                                     }
                                                 });
                                             }
                                            completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
                                                dispatch_async(dispatch_get_main_queue(), ^ {
                                                    //[MBProgressHUD hideHUDForView:self.view animated:YES];
                                                    [activityIndicator removeFromSuperview];
                                                    activityIndicator = nil;
                                                });
                                            }];
                });

            }
            //NSLog(@"shortStringimagePath : %@", imagePath);
            imgV.contentMode  = UIViewContentModeScaleAspectFit;
            [imgV setClipsToBounds:YES];
        }
        else
        {
            [buttonName setTitle:@"No Image" forState:UIControlStateNormal];
            [buttonName setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
            [buttonName setBackgroundColor:[UIColor clearColor]];
        }
        imgURL =nil;
    } else if ([[dictionary objectForKey:@"object_type"] isEqualToString:@"text"]) {
        NSArray *myArray = [attribute componentsSeparatedByCharactersInSet:[NSCharacterSet characterSetWithCharactersInString:@"|"]];
        //NSLog(@"myArray: %@", myArray);
        [buttonName setTitle:[dictionary objectForKey:@"object_value"] forState:UIControlStateNormal];
        [buttonName setTitleColor:[self colorWithHexString:[myArray objectAtIndex:1]] forState:UIControlStateNormal];
        if ([[myArray objectAtIndex:0] length]==6) {
            [buttonName setBackgroundColor:[self colorWithHexString:[myArray objectAtIndex:0]]];
        } else {
            [buttonName setBackgroundColor:[UIColor clearColor]];
        }
        buttonName.titleLabel.lineBreakMode = NSLineBreakByWordWrapping;
        [buttonName.titleLabel setFont:[UIFont fontWithName:@"Helvetica" size:[[myArray objectAtIndex:2] floatValue]]];
        if ([[myArray objectAtIndex:3] isEqualToString:@"center"]) {
            //[buttonName.titleLabel setTextAlignment: NSTextAlignmentCenter];
            buttonName.contentHorizontalAlignment = UIControlContentHorizontalAlignmentCenter;
        } else if ([[myArray objectAtIndex:3] isEqualToString:@"left"]) {
            //[buttonName.titleLabel setTextAlignment: NSTextAlignmentLeft];
            buttonName.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
        } else if ([[myArray objectAtIndex:3] isEqualToString:@"right"]) {
            //[buttonName.titleLabel setTextAlignment: NSTextAlignmentRight];
            buttonName.contentHorizontalAlignment = UIControlContentHorizontalAlignmentRight;
        }
        
        if (buttonName == btnMenuMA || buttonName == btnMenuMB || buttonName == btnMenuMC || buttonName == btnMenuMD || buttonName == btnMenuME || buttonName == btnMenuMF) {
            if ([[myArray objectAtIndex:0] length]==0) {
                [buttonName setBackgroundColor:[UIColor clearColor]];
            }
            buttonName.layer.borderColor = [self colorWithHexString:[myArray objectAtIndex:1]].CGColor;
            buttonName.layer.borderWidth = 0.5f;
            buttonName.alpha = 0.8;
            if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone) {
                if (index > 2) {
                    buttonName.frame = CGRectMake(self.frame.size.width/3*(index-3),  35, self.frame.size.width/3, 35);
                } else {
                    buttonName.frame = CGRectMake(self.frame.size.width/3*index,  0, self.frame.size.width/3, 35);
                }
            } else {
                if (index > 2) {
                    buttonName.frame = CGRectMake(self.frame.size.width/3*(index-3),  50, self.frame.size.width/3, 50);
                } else {
                    buttonName.frame = CGRectMake(self.frame.size.width/3*index,  0, self.frame.size.width/3, 50);
                }
            }
        }
    }
    
    /*NSDictionary *dictionary1 = [listOfObject objectAtIndex:index];
    if ([[dictionary1 objectForKey:@"theme_code"] isEqualToString:@"MENU"]) {
        NSArray *myArray = [attribute componentsSeparatedByCharactersInSet:[NSCharacterSet characterSetWithCharactersInString:@"|"]];
        //NSLog(@"myArray: %@", myArray);
        [buttonName setTitle:[dictionary objectForKey:@"object_value"] forState:UIControlStateNormal];
        [buttonName setTitleColor:[self colorWithHexString:[myArray objectAtIndex:1]] forState:UIControlStateNormal];
        if ([[myArray objectAtIndex:0] length]==6) {
            [buttonName setBackgroundColor:[self colorWithHexString:[myArray objectAtIndex:0]]];
        } else {
            [buttonName setBackgroundColor:[UIColor clearColor]];
        }
        buttonName.titleLabel.lineBreakMode = NSLineBreakByWordWrapping;
        [buttonName.titleLabel setFont:[UIFont fontWithName:@"Helvetica" size:[[myArray objectAtIndex:2] floatValue]]];
        if ([[myArray objectAtIndex:3] isEqualToString:@"center"]) {
            //[buttonName.titleLabel setTextAlignment: NSTextAlignmentCenter];
            buttonName.contentHorizontalAlignment = UIControlContentHorizontalAlignmentCenter;
        } else if ([[myArray objectAtIndex:3] isEqualToString:@"left"]) {
            //[buttonName.titleLabel setTextAlignment: NSTextAlignmentLeft];
            buttonName.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
        } else if ([[myArray objectAtIndex:3] isEqualToString:@"right"]) {
            //[buttonName.titleLabel setTextAlignment: NSTextAlignmentRight];
            buttonName.contentHorizontalAlignment = UIControlContentHorizontalAlignmentRight;
        }
    } */
    
    if ([[dictionary objectForKey:@"action_value"] intValue] > 0) {
        if ([[dictionary objectForKey:@"action_type"] isEqualToString:@"page"] || [[dictionary objectForKey:@"action_type"] isEqualToString:@"plugin"]) {
            buttonName.tag = [[dictionary objectForKey:@"action_value"] intValue];
            
            if ([[dictionary objectForKey:@"action_type"] isEqualToString:@"page"]) {
                [buttonName addTarget:self
                               action:@selector(checkButtonClick:)
                     forControlEvents:UIControlEventTouchUpInside];
            } else {
                [buttonName addTarget:self
                               action:@selector(checkButtonPluginClick:)
                     forControlEvents:UIControlEventTouchUpInside];
            }
        }
    }
    
    if (scrollview == nil) {
        //NSLog(@"scrollview == nil");
        if (Menu) {
            [menuView addSubview:buttonName];
        } else {
            [self addSubview:buttonName];
        }
    } else {
        [scrollview addSubview:buttonName];
    }
}

- (void) showaction:(NSDictionary *)dictionary :(UIScrollView *)scrollview {
    if ([[dictionary objectForKey:@"object_type"] isEqualToString:@"img"]) {
        NSString *content_img = [dictionary objectForKey:@"object_value"];
        
        dicImages = [NSDictionary dictionaryWithObjectsAndKeys:[dictionary objectForKey:@"id"],@"id",content_img,@"image",[dictionary objectForKey:@"object_type"],@"object_type",[dictionary objectForKey:@"action_value"],@"action_value",[dictionary objectForKey:@"action_type"],@"action_type",nil];
        [self.galleryImages addObject:dicImages];
        //[self.galleryImages addObject:content_img];
        content_img =nil;
    } else if ([[dictionary objectForKey:@"object_type"] isEqualToString:@"text"]) {
        CGRect textViewFrame = CGRectMake(0, 0, scrollview.frame.size.width, scrollview.frame.size.height);
        UITextView *textView = [[UITextView alloc] initWithFrame:textViewFrame];
        textView.returnKeyType = UIReturnKeyDone;
        textView.text = [dictionary objectForKey:@"object_value"];
        textView.userInteractionEnabled = NO;
        NSAttributedString *attributedString = [[NSAttributedString alloc] initWithData:[textView.text dataUsingEncoding:NSUnicodeStringEncoding] options:@{ NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType } documentAttributes:nil error:nil];
        textView.attributedText = attributedString;
        [scrollview addSubview:textView];
        textView =nil;
    } else if ([[dictionary objectForKey:@"object_type"] isEqualToString:@"video"]) {
        UIWebView *webView = [[UIWebView alloc] init];
        webView.backgroundColor = [UIColor clearColor];
        [webView setFrame:CGRectMake(0, 0, scrollview.frame.size.width, scrollview.frame.size.height)];
        //[[UIApplication sharedApplication] openURL:[NSURL URLWithString:[dictionary objectForKey:@"object_value"]]];
        //[webView loadRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:[dictionary objectForKey:@"object_value"]]]];
        //webView.autoresizesSubviews = YES;
        //webView.autoresizingMask=(UIViewAutoresizingFlexibleHeight | UIViewAutoresizingFlexibleWidth);
        
        NSString *videoUrl = [dictionary objectForKey:@"object_value"];
        
        // if your url is not in embed format or it is dynamic then you have to convert it in embed format.
        NSString *string = @"watch?v=";
        if ([videoUrl rangeOfString:string].location == NSNotFound) {
            //NSLog(@"string does not contain substring");
            
            NSString *string1 = @"v/";
            if ([videoUrl rangeOfString:string1].location == NSNotFound) {
                [webView loadRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:[dictionary objectForKey:@"object_value"]]]];
            } else {
                videoUrl = [videoUrl stringByReplacingOccurrencesOfString:@"v/" withString:@"embed/"];
                
                NSString *youTubeVideoHTML = [NSString stringWithFormat:@"<html><body style=\"margin:0\"><iframe class=\"youtube-player\" type=\"text/html\" width=\"%f\" height=\"%f\" style=\"max-width:100%%\" src=\"%@\" frameborder=\"0\"></iframe></body></html>", scrollview.frame.size.width, scrollview.frame.size.height, videoUrl];
                
                // Populate HTML with the URL and requested frame size
                //NSString *html = [NSString stringWithFormat:youTubeVideoHTML];
                
                // Load the html into the webview
                [webView loadHTMLString:youTubeVideoHTML baseURL:nil];
            }
            
        } else {
            //NSLog(@"string contains substring!");
            
            videoUrl = [videoUrl stringByReplacingOccurrencesOfString:@"watch?v=" withString:@"embed/"];
            
            NSRange range = [videoUrl rangeOfString:@"&"];
            @try {
                videoUrl = [videoUrl substringToIndex:range.location];
            }
            @catch (NSException *exception) {
                
            }
            
            NSString *youTubeVideoHTML = [NSString stringWithFormat:@"<html><body style=\"margin:0\"><iframe class=\"youtube-player\" type=\"text/html\" width=\"%f\" height=\"%f\" style=\"max-width:100%%\" src=\"%@\" frameborder=\"0\"></iframe></body></html>", scrollview.frame.size.width, scrollview.frame.size.height, videoUrl];
            
            // Populate HTML with the URL and requested frame size
            //NSString *html = [NSString stringWithFormat:youTubeVideoHTML];
            
            // Load the html into the webview
            [webView loadHTMLString:youTubeVideoHTML baseURL:nil];
        }
    
        [scrollview addSubview:webView];
        webView =nil;
    }
    if ([[dictionary objectForKey:@"attribute"] floatValue] > 0) {
        delaytime = [[dictionary objectForKey:@"attribute"] floatValue];
    } else {
        delaytime = 10;
    }
}

-(void) checkButtonClick:(id)sender {
    
    UIButton *clicked = (UIButton *) sender;
    //NSLog(@"checkButtonClick %d",clicked.tag);
    //AppDelegate *appdelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    appdelegate.nextPage = [NSString stringWithFormat:@"%d", clicked.tag];
    
    if (appdelegate.showloading) {
        alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"loading", nil) message:nil delegate:self cancelButtonTitle:nil otherButtonTitles: nil];
        
        //a simple activity indicator:
        UIActivityIndicatorView *activityIndicator = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
        activityIndicator.frame= CGRectMake(50, 10, 37, 37);
        activityIndicator.autoresizingMask = UIViewAutoresizingFlexibleTopMargin | UIViewAutoresizingFlexibleRightMargin | UIViewAutoresizingFlexibleBottomMargin | UIViewAutoresizingFlexibleLeftMargin;
        [activityIndicator startAnimating];
        
        //the magic line below,
        //we associate the activity indicator to the alert view: (addSubview is not used)
        [alert setValue:activityIndicator forKey:@"accessoryView"];
        
        [alert show];
    
        // Adjust the indicator so it is up a few pixels from the bottom of the alert
        [UIView beginAnimations:nil context:nil];
        [UIView setAnimationDuration:0.0];
        [UIView setAnimationDelay:0.0];
        [UIView setAnimationTransition:UIViewAnimationTransitionNone forView:alert cache:YES];
        [UIView setAnimationDelegate:self];
        [UIView setAnimationDidStopSelector:@selector(startupAnimationDoneIndicator:finished:context:)];
        alert.alpha = 0.5;
        [UIView commitAnimations];
    } else {
        [self goNextPage];
    }
}

-(void) checkButtonPluginClick:(id)sender {
    
    //NSString *aid;
    NSString *apageid;
    NSString *aobjctid;
    NSString *apluginid;
    //NSString *apluginvalue1;
    //NSString *apluginvalue2;
    //NSString *apluginvalue3;
    NSString *aremark;
    
    UIButton *clicked = (UIButton *) sender;
    //NSLog(@"checkButtonClick %d",clicked.tag);
    //AppDelegate *appdelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    appdelegate.plugin_id = [NSString stringWithFormat:@"%d", clicked.tag];
    sqlite3 *database;
    
    if(sqlite3_open([appdelegate.databasePath UTF8String], &database) == SQLITE_OK) {
        NSString *theme_object = [[NSString alloc] initWithFormat:@"SELECT * from theme_object_plugin WHERE id = %@", appdelegate.plugin_id];
        //NSLog(@"theme_object_plugin:: %@", theme_object);
        
        const char *sqlStatement1 = [theme_object UTF8String];
        sqlite3_stmt *compiledStatement1;
        if(sqlite3_prepare_v2(database, sqlStatement1, -1, &compiledStatement1, NULL) == SQLITE_OK) {
            while(sqlite3_step(compiledStatement1) == SQLITE_ROW) {
                
                aid = [NSString stringWithUTF8String:(char *)sqlite3_column_text(compiledStatement1, 0)];
                apageid = [NSString stringWithUTF8String:(char *)sqlite3_column_text(compiledStatement1, 1)];
                aobjctid = [NSString stringWithUTF8String:(char *)sqlite3_column_text(compiledStatement1, 2)];
                apluginid = [NSString stringWithUTF8String:(char *)sqlite3_column_text(compiledStatement1, 11)];
                aplugintype = [NSString stringWithUTF8String:(char *)sqlite3_column_text(compiledStatement1, 3)];
                apluginvalue1 = [NSString stringWithUTF8String:(char *)sqlite3_column_text(compiledStatement1, 4)];
                apluginvalue2 = [NSString stringWithUTF8String:(char *)sqlite3_column_text(compiledStatement1, 5)];
                apluginvalue3 = [NSString stringWithUTF8String:(char *)sqlite3_column_text(compiledStatement1, 6)];
                aremark = [NSString stringWithUTF8String:(char *)sqlite3_column_text(compiledStatement1, 8)];
                
            }
        }
        // Release the compiled statement from memory
        sqlite3_finalize(compiledStatement1);
    }
    sqlite3_close(database);
    
    if ([aplugintype isEqualToString:@"email"]) {
        if ([apluginvalue1 length]>0) {
            // Email Subject
            //NSString *emailTitle = @"Test Email";
            // Email Content
            //NSString *messageBody = @"<h1>Learning iOS Programming!</h1>"; // Change the message body to HTML
            // To address
            NSArray *toRecipents = [NSArray arrayWithObject:apluginvalue1];
            
            MFMailComposeViewController *mc = [[MFMailComposeViewController alloc] init];
            mc.mailComposeDelegate = self;
            //[mc setSubject:emailTitle];
            //[mc setMessageBody:messageBody isHTML:YES];
            [mc setToRecipients:toRecipents];
            
            // Present mail view controller on screen
            //[viewCtrl presentViewController:mc animated:YES completion:NULL];
            if([MFMailComposeViewController canSendMail])
            {
                [viewCtrl presentViewController:mc animated:NO completion:NULL];
                //[viewCtrl.navigationController pushViewController:mc animated:YES];
            }
        }
    } else if ([aplugintype isEqualToString:@"sms"]) {
        if ([apluginvalue1 length]>0) {
            if(![MFMessageComposeViewController canSendText]) {
                UIAlertView *warningAlert = [[UIAlertView alloc] initWithTitle:@"Error" message:@"Your device doesn't support SMS!" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
                [warningAlert show];
                return;
            }
            
            NSArray *recipents = [NSArray arrayWithObject:apluginvalue1];
            NSString *message = apluginvalue2;
            
            MFMessageComposeViewController *messageController = [[MFMessageComposeViewController alloc] init];
            messageController.messageComposeDelegate = self;
            [messageController setRecipients:recipents];
            [messageController setBody:message];
            
            // Present message view controller on screen
            [viewCtrl presentViewController:messageController animated:NO completion:nil];
        }
    } else if ([aplugintype isEqualToString:@"call"]) {
        //NSLog(@"apluginvalue1:: %@", apluginvalue1);
        if ([apluginvalue1 length]>0) {
            //NSString *strPhone = [[NSString alloc] initWithFormat:@"tel://%@", apluginvalue1];
            NSString *strPhone = [[NSString alloc] initWithFormat:@"telprompt://%@", apluginvalue1];
            [[UIApplication sharedApplication] openURL:[NSURL URLWithString:strPhone]];
        }
    } else {
        
        if (appdelegate.showloading) {
            alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"loading", nil) message:nil delegate:self cancelButtonTitle:nil otherButtonTitles: nil];
            
            //a simple activity indicator:
            UIActivityIndicatorView *activityIndicator = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
            activityIndicator.frame= CGRectMake(50, 10, 37, 37);
            activityIndicator.autoresizingMask = UIViewAutoresizingFlexibleTopMargin | UIViewAutoresizingFlexibleRightMargin | UIViewAutoresizingFlexibleBottomMargin | UIViewAutoresizingFlexibleLeftMargin;
            [activityIndicator startAnimating];
            
            //the magic line below,
            //we associate the activity indicator to the alert view: (addSubview is not used)
            [alert setValue:activityIndicator forKey:@"accessoryView"];
            
            [alert show];
        
            // Adjust the indicator so it is up a few pixels from the bottom of the alert
            [UIView beginAnimations:nil context:nil];
            [UIView setAnimationDuration:0.0];
            [UIView setAnimationDelay:0.0];
            [UIView setAnimationTransition:UIViewAnimationTransitionNone forView:alert cache:YES];
            [UIView setAnimationDelegate:self];
            [UIView setAnimationDidStopSelector:@selector(startupAnimationDoneIndicatorPlugin:finished:context:)];
            alert.alpha = 0.5;
            [UIView commitAnimations];
        } else {
            [self goPluginPage];
        }
    }
}

-(void)mailComposeController:(MFMailComposeViewController *)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError *)error {
    
    switch (result)
    {
        case MFMailComposeResultCancelled:
            NSLog(@"Mail cancelled");
            break;
        case MFMailComposeResultSaved:
            NSLog(@"Mail saved");
            break;
        case MFMailComposeResultSent:
            NSLog(@"Mail sent");
            break;
        case MFMailComposeResultFailed:
            NSLog(@"Mail sent failure: %@", [error localizedDescription]);
            break;
        default:
            break;
    }
    
    [viewCtrl dismissViewControllerAnimated:NO completion:nil];
}

- (void)messageComposeViewController:(MFMessageComposeViewController *)controller didFinishWithResult:(MessageComposeResult) result
{
    switch (result) {
        case MessageComposeResultCancelled:
            break;
            
        case MessageComposeResultFailed:
        {
            UIAlertView *warningAlert = [[UIAlertView alloc] initWithTitle:@"Error" message:@"Failed to send SMS!" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
            [warningAlert show];
            break;
        }
            
        case MessageComposeResultSent:
            break;
            
        default:
            break;
    }
    
    [viewCtrl dismissViewControllerAnimated:NO completion:nil];
}

-(void) dissmissIndicator {
    [alert dismissWithClickedButtonIndex:0 animated:NO];
}

-(void) goNextPage {
    //[self performSegueWithIdentifier:@"View1Segue" sender:self] ;
    sqlite3 *database;
    NSString *themecodenextpage;
    NSString *page_idnextpage;
    NSString *page_namenextpage;
    
    if(sqlite3_open([appdelegate.databasePath UTF8String], &database) == SQLITE_OK) {
        // Setup the SQL Statement and compile it for faster access
        NSString *temp = [[NSString alloc] initWithFormat:@"SELECT * from theme_page WHERE id = %@", appdelegate.nextPage];
        //NSLog(@"temp:: %@", temp);
        const char *sqlStatement = [temp UTF8String];
        sqlite3_stmt *compiledStatement;
        if(sqlite3_prepare_v2(database, sqlStatement, -1, &compiledStatement, NULL) == SQLITE_OK) {
            while(sqlite3_step(compiledStatement) == SQLITE_ROW) {
                page_idnextpage = [NSString stringWithUTF8String:(char *)sqlite3_column_text(compiledStatement, 0)];
                themecodenextpage = [NSString stringWithUTF8String:(char *)sqlite3_column_text(compiledStatement, 1)];
                page_namenextpage = [NSString stringWithUTF8String:(char *)sqlite3_column_text(compiledStatement, 2)];
                //prevpage = [NSString stringWithUTF8String:(char *)sqlite3_column_text(compiledStatement, 3)];
                
            }
        }
        // Release the compiled statement from memory
        sqlite3_finalize(compiledStatement);
    }
    sqlite3_close(database);
    
    ThemeViewController *themeViewController = [[ThemeViewController alloc] init];
    themeViewController.page_id = page_idnextpage;
    themeViewController.themepagenotification = @"Normal"; /*** Push Notification 27Aug2015 ***/
    [viewCtrl.navigationController pushViewController:themeViewController animated:NO];
}

-(void) goPluginPage {
    
    //NSLog(@"aplugintype:%@", aplugintype);
    
    if ([aplugintype isEqualToString:@"content"]) {
        sqlite3 *database;
        NSString *listtemplate;
        NSString *cont_id;
        NSString *cont_title;
        
        if(sqlite3_open([appdelegate.databasePath UTF8String], &database) == SQLITE_OK) {
            NSString *theme_object = [[NSString alloc] initWithFormat:@"SELECT cc.list_template, c.id, c.title from content_category cc JOIN contents c ON cc.id = c.category_id WHERE cc.top_id = %@", aid];
            //NSLog(@"theme_object_Details:: %@", theme_object);
            
            const char *sqlStatement1 = [theme_object UTF8String];
            sqlite3_stmt *compiledStatement1;
            if(sqlite3_prepare_v2(database, sqlStatement1, -1, &compiledStatement1, NULL) == SQLITE_OK) {
                while(sqlite3_step(compiledStatement1) == SQLITE_ROW) {
                    
                    listtemplate = [NSString stringWithUTF8String:(char *)sqlite3_column_text(compiledStatement1, 0)];
                    cont_id = [NSString stringWithUTF8String:(char *)sqlite3_column_text(compiledStatement1, 1)];
                    cont_title = [NSString stringWithUTF8String:(char *)sqlite3_column_text(compiledStatement1, 2)];
                    
                }
            }
            // Release the compiled statement from memory
            sqlite3_finalize(compiledStatement1);
        }
        sqlite3_close(database);
        
        if ([listtemplate isEqualToString:@"1"]) {
            ContentPluginDetailsViewController *contentPluginDetailsViewController = [[ContentPluginDetailsViewController alloc] init];
            contentPluginDetailsViewController.contId = cont_id;
            contentPluginDetailsViewController.conttitle = apluginvalue1;
            contentPluginDetailsViewController.listtemplate = listtemplate;
            [viewCtrl.navigationController pushViewController:contentPluginDetailsViewController animated:NO];
        } else {
            ContentPluginViewController *contentPluginViewController = [[ContentPluginViewController alloc] init];
            [viewCtrl.navigationController pushViewController:contentPluginViewController animated:NO];
        }
        listtemplate = nil;
        cont_id = nil;
        cont_title = nil;
        
    } else if ([aplugintype isEqualToString:@"productcat"]) {
        
        ContentProductCatPluginViewController *contentProductCatPluginViewController = [[ContentProductCatPluginViewController alloc] init];
        contentProductCatPluginViewController.pluginValue1 = apluginvalue1;
        contentProductCatPluginViewController.pluginValue2 = apluginvalue2;
        contentProductCatPluginViewController.pluginValue3 = apluginvalue3;
        [viewCtrl.navigationController pushViewController:contentProductCatPluginViewController animated:NO];
        
    } else if ([aplugintype isEqualToString:@"contentcat"]) {
        
        ContentCatPluginViewController *contentCatPluginViewController = [[ContentCatPluginViewController alloc] init];
        contentCatPluginViewController.pluginValue1 = apluginvalue1;
        contentCatPluginViewController.pluginValue2 = apluginvalue2;
        contentCatPluginViewController.pluginValue3 = apluginvalue3;
        [viewCtrl.navigationController pushViewController:contentCatPluginViewController animated:NO];
        
    } else if ([aplugintype isEqualToString:@"customer"]) {
        NSString *nibFileName;
        if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone) {
            nibFileName = @"SettingViewController";
        } else {
            nibFileName = @"SettingViewController_ipad";
        }
        SettingViewController *settingViewController = [[SettingViewController alloc] initWithNibName:nibFileName bundle:nil];
        settingViewController.cont_title = apluginvalue1;
        appdelegate.shouldReturnToPrevPage = NO;
        [viewCtrl.navigationController pushViewController:settingViewController animated:NO];
    } else if ([aplugintype isEqualToString:@"loyalty"]) {
        NSString *nibFileName;
        if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone) {
            nibFileName = @"LoyaltyViewController";
        } else {
            nibFileName = @"LoyaltyViewController_ipad";
        }
        LoyaltyViewController *loyaltyViewController = [[LoyaltyViewController alloc] initWithNibName:nibFileName bundle:nil];
        loyaltyViewController.cont_title = apluginvalue1;
        [viewCtrl.navigationController pushViewController:loyaltyViewController animated:NO];
    } else if ([aplugintype isEqualToString:@"ibeacon"]) {
        appdelegate.ibeaconfrommenu = YES;
        //[appdelegate.locationManager stopUpdatingLocation];
        //[appdelegate.locationManager startUpdatingLocation];
        appdelegate.beaconFoundTimer = appdelegate.intervalNotification-2;
    } else if ([aplugintype isEqualToString:@"warranty"]) {
        NSString *nibFileName;
        if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone) {
            nibFileName = @"WarrantyViewController";
        } else {
            nibFileName = @"WarrantyViewController_ipad";
        }
        WarrantyViewController *warrantyViewController = [[WarrantyViewController alloc] initWithNibName:nibFileName bundle:nil];
        warrantyViewController.cont_title = apluginvalue1;
        [viewCtrl.navigationController pushViewController:warrantyViewController animated:NO];
    } else if ([aplugintype isEqualToString:@"search"]) {
        ContentSearchViewController *contentSearchViewController = [[ContentSearchViewController alloc] init];
        contentSearchViewController.list_template = @"5";
        contentSearchViewController.cont_title = apluginvalue1;
        [viewCtrl.navigationController pushViewController:contentSearchViewController animated:NO];
    } else if ([aplugintype isEqualToString:@"favorite"]) {
        ContentPluginViewController *contentPluginViewController = [[ContentPluginViewController alloc] init];
        contentPluginViewController.list_favorite = @"ListOut";
        [viewCtrl.navigationController pushViewController:contentPluginViewController animated:NO];
    } else if ([aplugintype isEqualToString:@"booking"]) {
        NSString *nibFileName;
        if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone) {
            nibFileName = @"BookingListViewController";
        } else {
            nibFileName = @"BookingListViewController_ipad";
        }
        BookingListViewController *bookingListViewController = [[BookingListViewController alloc] initWithNibName:nibFileName bundle:nil];
        bookingListViewController.cont_title = apluginvalue1;
        [viewCtrl.navigationController pushViewController:bookingListViewController animated:NO];
    } else if ([aplugintype isEqualToString:@"points"]) {
        NSString *nibFileName;
        if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone) {
            nibFileName = @"ScorePointViewController";
        } else {
            nibFileName = @"ScorePointViewController_ipad";
        }
        ScorePointViewController *scorePointViewController = [[ScorePointViewController alloc] initWithNibName:nibFileName bundle:nil];
        scorePointViewController.cont_title = apluginvalue1;
        [viewCtrl.navigationController pushViewController:scorePointViewController animated:NO];
    } else if ([aplugintype isEqualToString:@"inbox"]) {
        
        InboxViewController *inboxViewController = [[InboxViewController alloc] init];
        inboxViewController.topId = aid;
        inboxViewController.conttitle = apluginvalue1;
        [viewCtrl.navigationController pushViewController:inboxViewController animated:NO];
    } else if ([aplugintype isEqualToString:@"point_slot"]) {
        
        //
    } else if ([aplugintype isEqualToString:@"notes"]) {
        NSString *nibFileName;
        if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone) {
            nibFileName = @"NotesViewController";
        } else {
            nibFileName = @"NotesViewController_ipad";
        }
        NotesViewController *notesViewController = [[NotesViewController alloc] initWithNibName:nibFileName bundle:nil];
        notesViewController.cont_title = apluginvalue1;
        [viewCtrl.navigationController pushViewController:notesViewController animated:NO];
    } else if ([aplugintype isEqualToString:@"event_invite"]) {
        NSString *nibFileName;
        if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone) {
            nibFileName = @"EventPluginViewController";
        } else {
            nibFileName = @"EventPluginViewController_ipad";
        }
        EventPluginViewController *eventPluginViewController = [[EventPluginViewController alloc] initWithNibName:nibFileName bundle:nil];
        eventPluginViewController.topId = aid;
        eventPluginViewController.cont_title = apluginvalue1;
        [viewCtrl.navigationController pushViewController:eventPluginViewController animated:NO];
    
    } else if ([aplugintype isEqualToString:@"event_reservation"]) {
        NSString *nibFileName;
        if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone) {
            nibFileName = @"RsvpViewController";
        } else {
            nibFileName = @"RsvpViewController_ipad";
        }
        RsvpViewController *rsvpViewController = [[RsvpViewController alloc] initWithNibName:nibFileName bundle:nil];
        //rsvpViewController.topId = aid;
        rsvpViewController.cont_title = apluginvalue1;
        [viewCtrl.navigationController pushViewController:rsvpViewController animated:NO];
    
    } else if ([aplugintype isEqualToString:@"sirim_label"]) {
        SIRIMLabelViewController *sirimLabelViewController = [[SIRIMLabelViewController alloc] init];
        [viewCtrl.navigationController pushViewController:sirimLabelViewController animated:NO];
        
    } else if ([aplugintype isEqualToString:@"sirim_pci"]) {
        SIRIMPCIViewController *sirimLabelViewController = [[SIRIMPCIViewController alloc] init];
        [viewCtrl.navigationController pushViewController:sirimLabelViewController animated:NO];
        
    } else {
        BasicPluginViewController *basicPluginViewController = [[BasicPluginViewController alloc] init];
        [viewCtrl.navigationController pushViewController:basicPluginViewController animated:NO];
    }
}

/*** Push Notification 27Aug2015 ***/
-(void) goNextPageNotification {
    //[self performSegueWithIdentifier:@"View1Segue" sender:self] ;
    sqlite3 *databaseNotification;
    NSString *themecodenextpage;
    NSString *page_idnextpage;
    NSString *page_namenextpage;
    
    if(sqlite3_open([appdelegate.databasePath UTF8String], &databaseNotification) == SQLITE_OK) {
        // Setup the SQL Statement and compile it for faster access
        NSString *tempNotification = [[NSString alloc] initWithFormat:@"SELECT * from theme_page WHERE id = %@", appdelegate.notify_actionvalue];
        //NSLog(@"temp:: %@", tempNotification);
        const char *sqlStatementNotification = [tempNotification UTF8String];
        sqlite3_stmt *compiledStatementNotification;
        if(sqlite3_prepare_v2(databaseNotification, sqlStatementNotification, -1, &compiledStatementNotification, NULL) == SQLITE_OK) {
            while(sqlite3_step(compiledStatementNotification) == SQLITE_ROW) {
                page_idnextpage = [NSString stringWithUTF8String:(char *)sqlite3_column_text(compiledStatementNotification, 0)];
                themecodenextpage = [NSString stringWithUTF8String:(char *)sqlite3_column_text(compiledStatementNotification, 1)];
                page_namenextpage = [NSString stringWithUTF8String:(char *)sqlite3_column_text(compiledStatementNotification, 2)];
                //prevpage = [NSString stringWithUTF8String:(char *)sqlite3_column_text(compiledStatement, 3)];
                
            }
        }
        // Release the compiled statement from memory
        sqlite3_finalize(compiledStatementNotification);
    }
    sqlite3_close(databaseNotification);
    
    //NSLog(@"temppage_idnextpage:: %@", page_idnextpage);
    
    ThemeViewController *themeViewController = [[ThemeViewController alloc] init];
    themeViewController.page_id = page_idnextpage;
    themeViewController.themepagenotification = @"Notification"; /*** Push Notification 27Aug2015 ***/
    [viewCtrl.navigationController pushViewController:themeViewController animated:NO];
}

-(void) goPluginPageNotification {
    sqlite3 *database;
    if(sqlite3_open([appdelegate.databasePath UTF8String], &database) == SQLITE_OK) {
        
        NSString *plugintype;
        NSString *pluginvalue1;
        
        NSString *theme_objectplugin = [[NSString alloc] initWithFormat:@"SELECT plugin_type, plugin_value1 from theme_object_plugin WHERE id = %@", appdelegate.notify_actionvalue];
        //NSLog(@"theme_objectplugin:: %@", theme_objectplugin);
        
        const char *sqlStatement1plugin = [theme_objectplugin UTF8String];
        sqlite3_stmt *compiledStatement1plugin;
        if(sqlite3_prepare_v2(database, sqlStatement1plugin, -1, &compiledStatement1plugin, NULL) == SQLITE_OK) {
            while(sqlite3_step(compiledStatement1plugin) == SQLITE_ROW) {
                
                plugintype = [NSString stringWithUTF8String:(char *)sqlite3_column_text(compiledStatement1plugin, 0)];
                pluginvalue1 = [NSString stringWithUTF8String:(char *)sqlite3_column_text(compiledStatement1plugin, 1)];
            }
        }
        // Release the compiled statement from memory
        sqlite3_finalize(compiledStatement1plugin);
        //NSLog(@"plugintype:: %@", plugintype);
        if ([plugintype isEqualToString:@"content"]) {
            
            NSString *listtemplate;
            NSString *cont_id;
            NSString *cont_title;
        
        
            NSString *theme_object = [[NSString alloc] initWithFormat:@"SELECT cc.list_template, c.id, c.title from content_category cc JOIN contents c ON cc.id = c.category_id WHERE cc.top_id = %@", appdelegate.notify_actionvalue];
            //NSLog(@"theme_object_Details:: %@", theme_object);
            
            const char *sqlStatement1 = [theme_object UTF8String];
            sqlite3_stmt *compiledStatement1;
            if(sqlite3_prepare_v2(database, sqlStatement1, -1, &compiledStatement1, NULL) == SQLITE_OK) {
                while(sqlite3_step(compiledStatement1) == SQLITE_ROW) {
                    
                    listtemplate = [NSString stringWithUTF8String:(char *)sqlite3_column_text(compiledStatement1, 0)];
                    cont_id = [NSString stringWithUTF8String:(char *)sqlite3_column_text(compiledStatement1, 1)];
                    cont_title = [NSString stringWithUTF8String:(char *)sqlite3_column_text(compiledStatement1, 2)];
                }
            }
            // Release the compiled statement from memory
            sqlite3_finalize(compiledStatement1);
            
            //appdelegate.notify_contentid = @"";
            if (![appdelegate.notify_contentid isEqualToString:@""]) {
                //NSLog(@"NotificationContentPlugin");
                ContentPluginDetailsViewController *contentPluginDetailsViewController = [[ContentPluginDetailsViewController alloc] init];
                contentPluginDetailsViewController.contId = appdelegate.notify_contentid;
                contentPluginDetailsViewController.conttitle = pluginvalue1;
                contentPluginDetailsViewController.listtemplate = listtemplate;
                //NSLog(@"appdelegate.notify_contentid: %@", appdelegate.notify_contentid);
                //NSLog(@"pluginvalue1: %@", pluginvalue1);
                //NSLog(@"listtemplate: %@", listtemplate);
                [viewCtrl.navigationController pushViewController:contentPluginDetailsViewController animated:NO];
            } else {
                ContentPluginViewController *contentPluginViewController = [[ContentPluginViewController alloc] init];
                [viewCtrl.navigationController pushViewController:contentPluginViewController animated:NO];
            }
            listtemplate = nil;
            cont_id = nil;
            cont_title = nil;
        }
    }
    sqlite3_close(database);
}
/*** Push Notification 27Aug2015 ***/

- (void)startupAnimationDoneIndicator:(NSString *)animationID finished:(NSNumber *)finished context:(void *)context
{
    [self goNextPage];
    [self dissmissIndicator];
    alert =nil;
}

- (void)startupAnimationDoneIndicatorPlugin:(NSString *)animationID finished:(NSNumber *)finished context:(void *)context
{
    [self goPluginPage];
    [self dissmissIndicator];
    alert =nil;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    
    if (tableView == GridtableView) {
        //NSLog(@"numberOfRowsInSectionGridtableView %d", [listOfObject count]);
        return 8;
    }
    
    return 0;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (tableView == GridtableView) {
        //if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone) {
            // For iPhone
        return scrollviewMain.frame.size.width*109/540;
        //} else {
            //return 140;
        //}
    }
    
   	return 44;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (tableView == GridtableView)
    {
        static NSString *CellIdentifier = @"CellIdentifier";
        UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
        
        UIImageView *imgViewMenu;
        const NSInteger RIGHT_TAG = 1001;
        UILabel *menutext;
        const NSInteger LEFT_TAG = 1000;
        
        if (cell == nil)
        {
            
            cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
            //cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
            
            //if ([[dictionary objectForKey:@"object_type"] isEqualToString:@"img"]) {
            
            //if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone) {
                // For iPhone
            imgViewMenu = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, scrollviewMain.frame.size.width, scrollviewMain.frame.size.width*109/540)];
            //} else {
                //imgViewMenu = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, self.frame.size.width, 140)];
            //}
            imgViewMenu.tag = LEFT_TAG;
            [cell.contentView addSubview:imgViewMenu];
            
            //} else if ([[dictionary objectForKey:@"object_type"] isEqualToString:@"text"]) {
            
            //if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone) {
                // For iPhone
            menutext = [[UILabel alloc] initWithFrame: CGRectMake(0, 0, scrollviewMain.frame.size.width, scrollviewMain.frame.size.width*109/540)];
            //} else {
                //menutext = [[UILabel alloc] initWithFrame: CGRectMake(0, 0, self.frame.size.width, 140)];
            //}
            menutext.tag = RIGHT_TAG;
            [menutext setFont:[UIFont boldSystemFontOfSize:16]];
            menutext.textColor = [UIColor blackColor];
            menutext.textAlignment = NSTextAlignmentLeft;
            menutext.backgroundColor = [UIColor clearColor];
            menutext.numberOfLines = 0;
            [cell.contentView addSubview:menutext];
            //}
            
        }
        else
        {
            //if ([[dictionary objectForKey:@"object_type"] isEqualToString:@"img"]) {
            imgViewMenu = (UIImageView *)[cell viewWithTag:LEFT_TAG];
            //} else if ([[dictionary objectForKey:@"object_type"] isEqualToString:@"text"]) {
            menutext = (UILabel *)[cell viewWithTag:RIGHT_TAG];
            //}
        }
        
        if([listOfObject count]>0)
        {
            NSDictionary *dictionary = [listOfObject objectAtIndex:indexPath.row];
            if ([[dictionary objectForKey:@"object_type"] isEqualToString:@"img"]) {
                NSString *imgURL = [dictionary objectForKey:@"object_value"];
                
                NSRange end = [imgURL rangeOfString:@"/" options:NSBackwardsSearch];
                NSString *shortString;
                if (end.location != NSNotFound) {
                    shortString =[imgURL substringWithRange:NSMakeRange(end.location+1, imgURL.length-(end.location+1))];
                } else {
                    shortString = imgURL;
                }
                
                BOOL success;
                
                // Create a FileManager object, we will use this to check the status
                // of the database and to copy it over if required
                NSFileManager *fileManager = [NSFileManager defaultManager];
                
                // Check if the database has already been created in the users filesystem
                success = [fileManager fileExistsAtPath:[[[NSBundle mainBundle] resourcePath] stringByAppendingPathComponent:shortString]];
                
                __block UIActivityIndicatorView *activityIndicator;
                __weak UIImageView *weakImageView = imgViewMenu;
                NSString *imagePath;
                if(success) {
                    imagePath = [[[NSBundle mainBundle] resourcePath] stringByAppendingPathComponent:shortString];
                    [weakImageView setImage:[UIImage imageNamed:shortString]];
                } else {
                    imagePath = imgURL;
                    /*[weakImageView setImageWithURL:[NSURL URLWithString:imagePath] placeholderImage:nil options:SDWebImageProgressiveDownload progress:^(NSInteger receivedSize, NSInteger expectedSize)
                     {
                         if (!activityIndicator)
                         {
                             [weakImageView addSubview:activityIndicator = [UIActivityIndicatorView.alloc initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray]];
                             activityIndicator.center = weakImageView.center;
                             [activityIndicator startAnimating];
                         }
                     }
                                         completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType)
                     {
                         [activityIndicator removeFromSuperview];
                         activityIndicator = nil;
                     }]; */
                    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0), ^ {
                        [weakImageView sd_setImageWithURL:[NSURL URLWithString:imagePath]
                                     placeholderImage:nil
                                              options:SDWebImageProgressiveDownload
                                             progress:^(NSInteger receivedSize, NSInteger expectedSize, NSURL *targetURL) {
                                                 dispatch_async(dispatch_get_main_queue(), ^ {
                                                     if (!activityIndicator) {
                                                         
                                                         [weakImageView addSubview:activityIndicator = [UIActivityIndicatorView.alloc initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray]];
                                                         activityIndicator.center = weakImageView.center;
                                                         [activityIndicator startAnimating];
                                                     }
                                                 });
                                             }
                                            completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
                                                dispatch_async(dispatch_get_main_queue(), ^ {
                                                    //[MBProgressHUD hideHUDForView:self.view animated:YES];
                                                    [activityIndicator removeFromSuperview];
                                                    activityIndicator = nil;
                                                });
                                            }];
                    });
                }
                //NSLog(@"shortStringimagePath : %@", imagePath);
                imgViewMenu.contentMode  = UIViewContentModeScaleAspectFit;
                [imgViewMenu setClipsToBounds:YES];
                menutext.text = @"";
                // NSString *myImage = [NSString stringWithFormat:@"%@%@", appdelegate.themecolor, [arr objectForKey:@"img"]];
                // UIImage *iconproduct = [UIImage imageNamed:imgURL];
                
            } else if ([[dictionary objectForKey:@"object_type"] isEqualToString:@"text"]) {
                
                NSArray *myArray = [[dictionary objectForKey:@"attribute"] componentsSeparatedByCharactersInSet:[NSCharacterSet characterSetWithCharactersInString:@"|"]];
                //NSLog(@"myArray: %@", myArray);
                menutext.textColor = [self colorWithHexString:[myArray objectAtIndex:1]];
                if ([[myArray objectAtIndex:0] length]==6) {
                    [menutext setBackgroundColor:[self colorWithHexString:[myArray objectAtIndex:0]]];
                } else {
                    [menutext setBackgroundColor:[UIColor clearColor]];
                }
                if ([[myArray objectAtIndex:3] isEqualToString:@"center"]) {
                    //[buttonName.titleLabel setTextAlignment: NSTextAlignmentCenter];
                    [menutext setTextAlignment:NSTextAlignmentCenter];
                } else if ([[myArray objectAtIndex:3] isEqualToString:@"left"]) {
                    //[buttonName.titleLabel setTextAlignment: NSTextAlignmentLeft];
                    [menutext setTextAlignment:NSTextAlignmentLeft];
                } else if ([[myArray objectAtIndex:3] isEqualToString:@"right"]) {
                    //[buttonName.titleLabel setTextAlignment: NSTextAlignmentRight];
                    [menutext setTextAlignment:NSTextAlignmentRight];
                }
                
                menutext.text = [NSString stringWithFormat:@"%@", [dictionary objectForKey:@"object_value"]];
                [imgViewMenu setImage:nil];
                
            }
            
        }
        return cell;
        imgViewMenu =nil;
        menutext =nil;
    }
    
    return nil;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
	if (tableView == GridtableView)
    {
        NSDictionary *dictionary = [listOfObject objectAtIndex:indexPath.row];
        if ([[dictionary objectForKey:@"action_value"] intValue] > 0) {
            //if ([[dictionary objectForKey:@"action_type"] isEqualToString:@"page"]) {
            if ([[dictionary objectForKey:@"action_type"] isEqualToString:@"page"] || [[dictionary objectForKey:@"action_type"] isEqualToString:@"plugin"]) {
                //appdelegate.nextPageType = [dictionary objectForKey:@"action_type"];
                if ([[dictionary objectForKey:@"action_type"] isEqualToString:@"page"]) {
                    //AppDelegate *appdelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
                    appdelegate.nextPage = [dictionary objectForKey:@"action_value"];
                    if (appdelegate.showloading) {
                        alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"loading", nil) message:nil delegate:self cancelButtonTitle:nil otherButtonTitles: nil];
                        
                        //a simple activity indicator:
                        UIActivityIndicatorView *activityIndicator = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
                        activityIndicator.frame= CGRectMake(50, 10, 37, 37);
                        activityIndicator.autoresizingMask = UIViewAutoresizingFlexibleTopMargin | UIViewAutoresizingFlexibleRightMargin | UIViewAutoresizingFlexibleBottomMargin | UIViewAutoresizingFlexibleLeftMargin;
                        [activityIndicator startAnimating];
                        
                        //the magic line below,
                        //we associate the activity indicator to the alert view: (addSubview is not used)
                        [alert setValue:activityIndicator forKey:@"accessoryView"];
                        
                        [alert show];
                    
                        // Adjust the indicator so it is up a few pixels from the bottom of the alert
                        [UIView beginAnimations:nil context:nil];
                        [UIView setAnimationDuration:0.0];
                        [UIView setAnimationDelay:0.0];
                        [UIView setAnimationTransition:UIViewAnimationTransitionNone forView:alert cache:YES];
                        [UIView setAnimationDelegate:self];
                        [UIView setAnimationDidStopSelector:@selector(startupAnimationDoneIndicator:finished:context:)];
                        alert.alpha = 0.5;
                        [UIView commitAnimations];
                    } else {
                        [self goNextPage];
                    }
                } else {
                    //AppDelegate *appdelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
                    appdelegate.plugin_id = [dictionary objectForKey:@"action_value"];
                    sqlite3 *database;
                    NSString *apluginid;
                   // NSString *aplugintype;
                    //NSString *apluginvalue1;
                    //NSString *apluginvalue2;
                    //NSString *apluginvalue3;
                    NSString *aremark;
                    
                    if(sqlite3_open([appdelegate.databasePath UTF8String], &database) == SQLITE_OK) {
                        NSString *theme_object = [[NSString alloc] initWithFormat:@"SELECT * from theme_object_plugin WHERE id = %@", appdelegate.plugin_id];
                        //NSLog(@"theme_object_plugin:: %@", theme_object);
                        
                        const char *sqlStatement1 = [theme_object UTF8String];
                        sqlite3_stmt *compiledStatement1;
                        if(sqlite3_prepare_v2(database, sqlStatement1, -1, &compiledStatement1, NULL) == SQLITE_OK) {
                            while(sqlite3_step(compiledStatement1) == SQLITE_ROW) {
                                
                                aid = [NSString stringWithUTF8String:(char *)sqlite3_column_text(compiledStatement1, 0)];
                                apluginid = [NSString stringWithUTF8String:(char *)sqlite3_column_text(compiledStatement1, 11)];
                                aplugintype = [NSString stringWithUTF8String:(char *)sqlite3_column_text(compiledStatement1, 3)];
                                apluginvalue1 = [NSString stringWithUTF8String:(char *)sqlite3_column_text(compiledStatement1, 4)];
                                apluginvalue2 = [NSString stringWithUTF8String:(char *)sqlite3_column_text(compiledStatement1, 5)];
                                apluginvalue3 = [NSString stringWithUTF8String:(char *)sqlite3_column_text(compiledStatement1, 6)];
                                aremark = [NSString stringWithUTF8String:(char *)sqlite3_column_text(compiledStatement1, 8)];
                                
                            }
                        }
                        // Release the compiled statement from memory
                        sqlite3_finalize(compiledStatement1);
                    }
                    sqlite3_close(database);
                    
                    if ([aplugintype isEqualToString:@"email"]) {
                        if ([apluginvalue1 length]>0) {
                            // Email Subject
                            //NSString *emailTitle = @"Test Email";
                            // Email Content
                            //NSString *messageBody = @"<h1>Learning iOS Programming!</h1>"; // Change the message body to HTML
                            // To address
                            NSArray *toRecipents = [NSArray arrayWithObject:apluginvalue1];
                            
                            MFMailComposeViewController *mc = [[MFMailComposeViewController alloc] init];
                            mc.mailComposeDelegate = self;
                            //[mc setSubject:emailTitle];
                            //[mc setMessageBody:messageBody isHTML:YES];
                            [mc setToRecipients:toRecipents];
                            
                            // Present mail view controller on screen
                            //[viewCtrl presentViewController:mc animated:YES completion:NULL];
                            //[viewCtrl.navigationController pushViewController:mc animated:YES];
                            if([MFMailComposeViewController canSendMail])
                            {
                                [viewCtrl presentViewController:mc animated:NO completion:NULL];
                                //[viewCtrl.navigationController pushViewController:mc animated:YES];
                            }
                        }
                        
                    } else if ([aplugintype isEqualToString:@"call"]) {
                        //NSLog(@"apluginvalue1:: %@", apluginvalue1);
                        if ([apluginvalue1 length]>0) {
                            //NSString *strPhone = [[NSString alloc] initWithFormat:@"tel://%@", apluginvalue1];
                            NSString *strPhone = [[NSString alloc] initWithFormat:@"telprompt://%@", apluginvalue1];
                            [[UIApplication sharedApplication] openURL:[NSURL URLWithString:strPhone]];
                        }
                    } else {
                        
                        if (appdelegate.showloading) {
                            alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"loading", nil) message:nil delegate:self cancelButtonTitle:nil otherButtonTitles: nil];
                            
                            //a simple activity indicator:
                            UIActivityIndicatorView *activityIndicator = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
                            activityIndicator.frame= CGRectMake(50, 10, 37, 37);
                            activityIndicator.autoresizingMask = UIViewAutoresizingFlexibleTopMargin | UIViewAutoresizingFlexibleRightMargin | UIViewAutoresizingFlexibleBottomMargin | UIViewAutoresizingFlexibleLeftMargin;
                            [activityIndicator startAnimating];
                            
                            //the magic line below,
                            //we associate the activity indicator to the alert view: (addSubview is not used)
                            [alert setValue:activityIndicator forKey:@"accessoryView"];
                            
                            [alert show];
                        
                            // Adjust the indicator so it is up a few pixels from the bottom of the alert
                            [UIView beginAnimations:nil context:nil];
                            [UIView setAnimationDuration:0.0];
                            [UIView setAnimationDelay:0.0];
                            [UIView setAnimationTransition:UIViewAnimationTransitionNone forView:alert cache:YES];
                            [UIView setAnimationDelegate:self];
                            [UIView setAnimationDidStopSelector:@selector(startupAnimationDoneIndicatorPlugin:finished:context:)];
                            alert.alpha = 0.5;
                            [UIView commitAnimations];
                            
                        } else {
                            [self goPluginPage];
                        }
                    }
                    //[UIView setAnimationDidStopSelector:@selector(startupAnimationDoneIndicatorPlugin:finished:context:)];
                }
            }
        }
        //
    }
}

- (void)webViewDidFinishLoad:(UIWebView *)aWebView
{
    //NSLog(@"webViewDidFinishLoad");
    /*CGSize contentSize = aWebView.scrollView.contentSize;
    CGSize viewSize = self.bounds.size;
    
    float rw = viewSize.width / contentSize.width;
    
    aWebView.scrollView.minimumZoomScale = rw;
    aWebView.scrollView.maximumZoomScale = rw;
    aWebView.scrollView.zoomScale = rw; */
    //CGRect newBounds = aWebView.bounds;
    //newBounds.size.height = aWebView.scrollView.contentSize.height;
    //aWebView.bounds = newBounds;
    [aWebView stringByEvaluatingJavaScriptFromString:@"iframeLoaded()"];
    
    [aWebView sizeToFit];
    //NSLog(@"width: %f", aWebView.frame.size.width);
    //[aWebView setFrame:CGRectMake(aWebView.frame.origin.x, aWebView.frame.origin.y, aWebView.frame.size.width, aWebView.frame.size.height)];
    //viewFeed.contentSize = CGSizeMake(aWebView.frame.size.width, aWebView.frame.size.height);
    [processIndicator stopAnimating];
    //webView =nil;
    
    /*CGRect frame = aWebView.frame;
    frame.size.height = 1;
    aWebView.frame = frame;
    CGSize fittingSize = [aWebView sizeThatFits:CGSizeZero];
    frame.size = fittingSize;
    //aWebView.frame = frame;
    
    scrollviewFeed.contentSize = CGSizeMake(fittingSize.width, fittingSize.height); */
    //NSLog(@"size: %f, %f", fittingSize.width, fittingSize.height);
    
    /*UIButton *button = [UIButton buttonWithType:UIButtonTypeRoundedRect];
    [button setFrame:CGRectMake(aWebView.frame.origin.x, aWebView.frame.origin.y, 100, 10)];
    [button addTarget:self action:@selector(goBack:) forControlEvents:UIControlEventTouchUpInside];
    [aWebView addSubview:button]; */
    
    [viewCtrl.navigationItem setHidesBackButton:NO];
    
}

//No Internet Connection error code
-(void)webView:(UIWebView *)webView didFailLoadWithError:(NSError *)error {
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"error", nil) message:NSLocalizedString(@"MSG_CONNECTION_ERROR", nil) delegate:self cancelButtonTitle:NSLocalizedString(@"close", nil) otherButtonTitles:nil, nil];
    [alert show];
    [processIndicator stopAnimating];
}

@end
