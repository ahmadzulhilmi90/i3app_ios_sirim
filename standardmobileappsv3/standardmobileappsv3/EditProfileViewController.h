//
//  EditProfileViewController.h
//  standardmobileappsv3
//
//  Created by M3Online on 12/21/14.
//  Copyright (c) 2014 M3Online. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "WEPopoverController.h"
#import "LoadingView.h"

@interface EditProfileViewController : UIViewController<UITextFieldDelegate, WEPopoverControllerDelegate, UITableViewDelegate, UITableViewDataSource, UIViewControllerTransitioningDelegate> {
    CGFloat animatedDistance;
    NSString *contId;
    NSString *conttitle;
    
    //NSMutableData* receivedData;
    NSString *currentRequest;
    UIScrollView *scrollview;
    
    LoadingView *loadingView;
    
    NSString *alertmsgstatus;
    
    WEPopoverController *genderPopoverController;
    WEPopoverController *langPopoverController;
    WEPopoverController *countryPopoverController;
    WEPopoverController *statePopoverController;
    
    UITableView *genderTableView;
    UITableView *langTableView;
    UITableView *countryTableView;
    UITableView *stateTableView;
    
    UIViewController *genderContent;
    UIViewController *langContent;
    UIViewController *countryContent;
    UIViewController *stateContent;
    
    NSMutableArray *listOfgender;
    NSMutableArray *listOflang;
    NSMutableArray *listOfcountry;
    NSMutableArray *listOfstate;
    NSMutableArray *listOfstatetemp;
    
    NSString *gendercode;
    NSString *langcode;
    NSString *countrycode;
    NSString *statecode;
}

@property (nonatomic, strong) NSString *contId;
@property (nonatomic, strong) NSString *conttitle;

//@property (nonatomic, strong) NSMutableData *receivedData;
@property (nonatomic, strong) NSString *currentRequest;
@property (nonatomic, strong) UIScrollView *scrollview;

@property (strong, nonatomic) IBOutlet UITextField *txt_fullname;
@property (strong, nonatomic) IBOutlet UITextField *txt_telephone;
@property (strong, nonatomic) IBOutlet UITextField *txt_gender;
@property (strong, nonatomic) IBOutlet UITextField *txt_language;
@property (strong, nonatomic) IBOutlet UITextField *txt_country;
@property (strong, nonatomic) IBOutlet UITextField *txt_state;

@property (nonatomic, strong) NSString *alertmsgstatus;

@property (nonatomic, strong) WEPopoverController *genderPopoverController;
@property (nonatomic, strong) WEPopoverController *langPopoverController;
@property (nonatomic, strong) WEPopoverController *countryPopoverController;
@property (nonatomic, strong) WEPopoverController *statePopoverController;

@property (nonatomic, retain) NSMutableArray *listOfgender;
@property (nonatomic, retain) NSMutableArray *listOflang;
@property (nonatomic, retain) NSMutableArray *listOfcountry;
@property (nonatomic, retain) NSMutableArray *listOfstate;
@property (nonatomic, retain) NSMutableArray *listOfstatetemp;

@end
