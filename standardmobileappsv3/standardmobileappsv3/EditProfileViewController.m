//
//  EditProfileViewController.m
//  standardmobileappsv3
//
//  Created by M3Online on 12/21/14.
//  Copyright (c) 2014 M3Online. All rights reserved.
//

#import "EditProfileViewController.h"
#import "AppDelegate.h"
#import "LoadingView.h"
#import "SBJSON.h"
#import "RootViewController.h"
#import "ViewController.h"
#import "SettingViewController.h"
#import "InitialSlidingViewController.h"
#import "BTSlideInteractor.h"

#define CELL_CONTENT_MARGIN 8.0f
#define CELL_CONTENT_WIDTH self.view.frame.size.width-CELL_CONTENT_MARGIN

static const CGFloat KEYBOARD_ANIMATION_DURATION = 0.3;
static const CGFloat MINIMUM_SCROLL_FRACTION = 0.2;
static const CGFloat MAXIMUM_SCROLL_FRACTION = 0.8;
static const CGFloat PORTRAIT_KEYBOARD_HEIGHT = 216;

@interface EditProfileViewController ()
@property (strong, nonatomic) BTSlideInteractor *interactor;
@end

@implementation EditProfileViewController
@synthesize contId, scrollview;
@synthesize /*receivedData, */currentRequest, conttitle;
@synthesize txt_telephone, txt_gender, txt_fullname, alertmsgstatus, txt_language, txt_country, txt_state;
@synthesize genderPopoverController, langPopoverController, countryPopoverController, statePopoverController, listOfgender, listOflang, listOfcountry, listOfstate, listOfstatetemp;

InitialSlidingViewController *modalController;

AppDelegate *appdelegate;
__strong LoadingView *loadingView;

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
    //////NSLog(@">>> Entering *** %s <<<", __PRETTY_FUNCTION__);
    [self hideKeyPad];
}

- (IBAction) callHideKeyBoard:(id)sender{
    //////NSLog(@"testing...");
    [self hideKeyPad];
}

-(void) hideKeyPad {
    //////NSLog(@"#### %s",__PRETTY_FUNCTION__);
    [txt_fullname resignFirstResponder];
    [txt_telephone resignFirstResponder];
    [txt_gender resignFirstResponder];
    [txt_language resignFirstResponder];
    [txt_country resignFirstResponder];
    [txt_state resignFirstResponder];
}


- (void)viewDidLoad {
    
    gendercode = @"";
    langcode = @"";
    countrycode = @"";
    statecode = @"";
    
    appdelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    
    if (![appdelegate.backgroundT isEqualToString:@""]) {
        
        NSString *filePath = appdelegate.backgroundT;
        
        NSRange end = [filePath rangeOfString:@"/" options:NSBackwardsSearch];
        NSString *shortString;
        if (end.location != NSNotFound) {
            shortString =[filePath substringWithRange:NSMakeRange(end.location+1, filePath.length-(end.location+1))];
        } else {
            shortString = filePath;
        }
        
        BOOL success;
        
        // Create a FileManager object, we will use this to check the status
        // of the database and to copy it over if required
        NSFileManager *fileManager = [NSFileManager defaultManager];
        
        // Check if the database has already been created in the users filesystem
        success = [fileManager fileExistsAtPath:[[[NSBundle mainBundle] resourcePath] stringByAppendingPathComponent:shortString]];
        
        UIImageView *imgV = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height)];
        //[self addSubview:imgV];
        
        //__block UIActivityIndicatorView *activityIndicator;
        __weak UIImageView *weakImageView = imgV;
        NSString *imagePath;
        if(success) {
            imagePath = [[[NSBundle mainBundle] resourcePath] stringByAppendingPathComponent:shortString];
            [weakImageView setImage:[UIImage imageNamed:shortString]];
            
            //NSLog(@"shortStringimagePath : %@", imagePath);
            imgV.contentMode  = UIViewContentModeScaleAspectFit;
            [imgV setClipsToBounds:YES];
            
            UIGraphicsBeginImageContext(self.view.frame.size);
            CGRect rect=CGRectMake(0,-64,self.view.bounds.size.width,self.view.bounds.size.height+64);
            [imgV.image drawInRect:rect];
            UIImage *image1 = UIGraphicsGetImageFromCurrentImageContext();
            UIGraphicsEndImageContext();
            self.view.backgroundColor = [UIColor colorWithPatternImage:image1];
        } else {
            imagePath = filePath;
            NSURL *imageURL = [NSURL URLWithString:imagePath];
            NSData *imageData = [NSData dataWithContentsOfURL:imageURL];
            //self.backgroundColor = [UIColor redColor];
            UIGraphicsBeginImageContext(self.view.frame.size);
            CGRect rect=CGRectMake(0,-64,self.view.bounds.size.width,self.view.bounds.size.height+64);
            [[UIImage imageWithData:imageData] drawInRect:rect];
            UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
            UIGraphicsEndImageContext();
            self.view.backgroundColor = [UIColor colorWithPatternImage:image];
        }
    } else {
        self.view.backgroundColor = [UIColor clearColor];
    }
    
    self.title = conttitle;
    appdelegate.homeclicked = NO;
    
    genderTableView = [[UITableView alloc] init];
    genderTableView.delegate = self;
    genderTableView.dataSource = self;
    
    langTableView = [[UITableView alloc] init];
    langTableView.delegate = self;
    langTableView.dataSource = self;
    
    countryTableView = [[UITableView alloc] init];
    countryTableView.delegate = self;
    countryTableView.dataSource = self;
    
    stateTableView = [[UITableView alloc] init];
    stateTableView.delegate = self;
    stateTableView.dataSource = self;
    
    self.navigationItem.backBarButtonItem = [[UIBarButtonItem alloc]initWithTitle:@"" style:UIBarButtonItemStylePlain target:nil action:nil];
    
    CGRect rect = CGRectMake(0,0,20,20);
    UIGraphicsBeginImageContext( rect.size );
    [[UIImage imageNamed:@"home.png"] drawInRect:rect];
    UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    UIBarButtonItem *myHomeButton = [[UIBarButtonItem alloc] initWithImage:image style:UIBarButtonItemStyleDone target:self action:@selector(myHomeAction:)];
    
    
    NSArray *myButtonArray = [[NSArray alloc] initWithObjects:myHomeButton, nil];
    
    self.navigationItem.rightBarButtonItems = myButtonArray;
    
    [self getDataFromJson];
    
    [self displaycontent];
    
    NSString *urlAddress = [[NSString alloc] initWithFormat:@"%@cust_details.php?db=%@&userid=%@&udid=%@&cuid=%@", appdelegate.user_apipath, appdelegate.db, appdelegate.user, appdelegate.uuid, appdelegate.customer_id];
    //NSLog(@"urlAddresscust : %@", urlAddress);
    currentRequest = @"ViewDetails";
    [self connectToServer:urlAddress];
    
}

-(void) getDataFromJson {
    
    [listOfgender removeAllObjects];
    [listOflang removeAllObjects];
    [listOfcountry removeAllObjects];
    
    NSString *filePath = [[NSBundle mainBundle] pathForResource:@"app_gender" ofType:@"json"];
    NSData *data = [NSData dataWithContentsOfFile:filePath];
    listOfgender = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:nil];
    //NSLog(@"listOfgender %@", listOfgender);
    
    NSString *filePath1 = [[NSBundle mainBundle] pathForResource:@"app_language" ofType:@"json"];
    NSData *data1 = [NSData dataWithContentsOfFile:filePath1];
    listOflang = [NSJSONSerialization JSONObjectWithData:data1 options:kNilOptions error:nil];
    //NSLog(@"listOfgender %@", listOfgender);
    
    NSString *filePath2 = [[NSBundle mainBundle] pathForResource:@"app_countries" ofType:@"json"];
    NSData *data2 = [NSData dataWithContentsOfFile:filePath2];
    listOfcountry = [NSJSONSerialization JSONObjectWithData:data2 options:kNilOptions error:nil];
    //NSLog(@"listOfgender %@", listOfgender);
    
    NSString *filePath3 = [[NSBundle mainBundle] pathForResource:@"app_states" ofType:@"json"];
    NSData *data3 = [NSData dataWithContentsOfFile:filePath3];
    listOfstatetemp = [NSJSONSerialization JSONObjectWithData:data3 options:kNilOptions error:nil];
    
}

-(void) displaycontent {
    
    /*
     - (void)fetchedData:(NSData *)responseData {
     //parse out the json data
     NSError* error;
     NSDictionary* json = [NSJSONSerialization
     JSONObjectWithData:responseData //1
     
     options:kNilOptions
     error:&error];
     
     NSArray* latestLoans = [json objectForKey:@"loans"]; //2
     
     NSLog(@"loans: %@", latestLoans); //3
     }
    */
    
    scrollview = [[UIScrollView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height-64)];
    scrollview.backgroundColor = [UIColor clearColor];
    scrollview.bounces = NO;
    [self.view addSubview:scrollview];
    
    UILabel *lblfullname = [[UILabel alloc] initWithFrame: CGRectMake(10, 20, scrollview.frame.size.width-20, 30)];
    [lblfullname setFont:[UIFont systemFontOfSize:18]];
    lblfullname.textColor = [UIColor grayColor];
    lblfullname.textAlignment = NSTextAlignmentLeft;
    lblfullname.backgroundColor = [UIColor clearColor];
    lblfullname.numberOfLines = 0;
    [lblfullname setText:NSLocalizedString(@"full_name", nil)];
    [scrollview addSubview:lblfullname];
    
    txt_fullname = [[UITextField alloc] initWithFrame:CGRectMake(10, 50, scrollview.frame.size.width-20, 40)];
    txt_fullname.textColor = [UIColor blackColor];
    [txt_fullname setBorderStyle:UITextBorderStyleRoundedRect];
    //txt_fullname.secureTextEntry = YES;
    [scrollview addSubview:txt_fullname];
    txt_fullname.delegate = self;
    
    UILabel *lbltelephone = [[UILabel alloc] initWithFrame: CGRectMake(10, 100, scrollview.frame.size.width-20, 30)];
    [lbltelephone setFont:[UIFont systemFontOfSize:18]];
    lbltelephone.textColor = [UIColor grayColor];
    lbltelephone.textAlignment = NSTextAlignmentLeft;
    lbltelephone.backgroundColor = [UIColor clearColor];
    lbltelephone.numberOfLines = 0;
    [lbltelephone setText:NSLocalizedString(@"telephone", nil)];
    [scrollview addSubview:lbltelephone];
    
    txt_telephone = [[UITextField alloc] initWithFrame:CGRectMake(10, 130, scrollview.frame.size.width-20, 40)];
    txt_telephone.textColor = [UIColor blackColor];
    [txt_telephone setBorderStyle:UITextBorderStyleRoundedRect];
    //txt_telephone.secureTextEntry = YES;
    [scrollview addSubview:txt_telephone];
    txt_telephone.delegate = self;
    
    UILabel *lblgender = [[UILabel alloc] initWithFrame: CGRectMake(10, 180, scrollview.frame.size.width-20, 30)];
    [lblgender setFont:[UIFont systemFontOfSize:18]];
    lblgender.textColor = [UIColor grayColor];
    lblgender.textAlignment = NSTextAlignmentLeft;
    lblgender.backgroundColor = [UIColor clearColor];
    lblgender.numberOfLines = 0;
    [lblgender setText:NSLocalizedString(@"gender", nil)];
    [scrollview addSubview:lblgender];
    
    txt_gender = [[UITextField alloc] initWithFrame:CGRectMake(10, 210, scrollview.frame.size.width-20, 40)];
    txt_gender.textColor = [UIColor blackColor];
    [txt_gender setBorderStyle:UITextBorderStyleRoundedRect];
    //txt_gender.secureTextEntry = YES;
    [scrollview addSubview:txt_gender];
    txt_gender.delegate = self;
    
    UILabel *lbllanguage = [[UILabel alloc] initWithFrame: CGRectMake(10, 260, scrollview.frame.size.width-20, 30)];
    [lbllanguage setFont:[UIFont systemFontOfSize:18]];
    lbllanguage.textColor = [UIColor grayColor];
    lbllanguage.textAlignment = NSTextAlignmentLeft;
    lbllanguage.backgroundColor = [UIColor clearColor];
    lbllanguage.numberOfLines = 0;
    [lbllanguage setText:NSLocalizedString(@"language", nil)];
    [scrollview addSubview:lbllanguage];
    
    txt_language = [[UITextField alloc] initWithFrame:CGRectMake(10, 290, scrollview.frame.size.width-20, 40)];
    txt_language.textColor = [UIColor blackColor];
    [txt_language setBorderStyle:UITextBorderStyleRoundedRect];
    //txt_language.secureTextEntry = YES;
    [scrollview addSubview:txt_language];
    txt_language.delegate = self;
    
    UILabel *lblcountry = [[UILabel alloc] initWithFrame: CGRectMake(10, 340, scrollview.frame.size.width-20, 30)];
    [lblcountry setFont:[UIFont systemFontOfSize:18]];
    lblcountry.textColor = [UIColor grayColor];
    lblcountry.textAlignment = NSTextAlignmentLeft;
    lblcountry.backgroundColor = [UIColor clearColor];
    lblcountry.numberOfLines = 0;
    [lblcountry setText:NSLocalizedString(@"country", nil)];
    [scrollview addSubview:lblcountry];
    
    txt_country = [[UITextField alloc] initWithFrame:CGRectMake(10, 370, scrollview.frame.size.width-20, 40)];
    txt_country.textColor = [UIColor blackColor];
    [txt_country setBorderStyle:UITextBorderStyleRoundedRect];
    //txt_country.secureTextEntry = YES;
    [scrollview addSubview:txt_country];
    txt_country.delegate = self;
    
    UILabel *lblstate = [[UILabel alloc] initWithFrame: CGRectMake(10, 420, scrollview.frame.size.width-20, 30)];
    [lblstate setFont:[UIFont systemFontOfSize:18]];
    lblstate.textColor = [UIColor grayColor];
    lblstate.textAlignment = NSTextAlignmentLeft;
    lblstate.backgroundColor = [UIColor clearColor];
    lblstate.numberOfLines = 0;
    [lblstate setText:NSLocalizedString(@"state", nil)];
    [scrollview addSubview:lblstate];
    
    txt_state = [[UITextField alloc] initWithFrame:CGRectMake(10, 450, scrollview.frame.size.width-20, 40)];
    txt_state.textColor = [UIColor blackColor];
    [txt_state setBorderStyle:UITextBorderStyleRoundedRect];
    //txt_state.secureTextEntry = YES;
    [scrollview addSubview:txt_state];
    txt_state.delegate = self;
    
    UIButton *buttonsubmit = [UIButton buttonWithType:UIButtonTypeRoundedRect];
    buttonsubmit.tag = 1001;
    [buttonsubmit addTarget:self action:@selector(aMethod:) forControlEvents:UIControlEventTouchUpInside];
    [buttonsubmit setTitle:NSLocalizedString(@"save", nil) forState:UIControlStateNormal];
    [buttonsubmit setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    [buttonsubmit setBackgroundColor:[UIColor colorWithRed:230/255.0 green:230/255.0 blue:230/255.0 alpha:1]];
    buttonsubmit.frame = CGRectMake(10, 520, 210, 44.0);
    [scrollview addSubview:buttonsubmit];
    
    scrollview.contentSize = CGSizeMake(scrollview.frame.size.width, 580);
    scrollview =nil;
    
}

- (void)aMethod:(id)sender
{
    UIButton *clicked = (UIButton *) sender;
    
    //Submit button
    if (clicked.tag==1001)
    {
        NSString *urlAddress = [[NSString alloc] initWithFormat:@"%@cust_update.php?db=%@&userid=%@&udid=%@&cuid=%@&cs=%@&name=%@&mobile=%@&gender=%@&language=%@&country=%@&state=%@", appdelegate.user_apipath, appdelegate.db, appdelegate.user, appdelegate.uuid, appdelegate.customer_id, appdelegate.cs, [self urlEncode:[txt_fullname text]], [self urlEncode:[txt_telephone text]], gendercode, langcode, countrycode, statecode];
        currentRequest = @"UpdateDetails";
        NSLog(@"urlAddress : %@",urlAddress);
        [self connectToServer:urlAddress];
        
    }
}

//Slide Menu
- (void)willAnimateRotationToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation duration:(NSTimeInterval)duration
{
    //NSLog(@"view frame=%@ bounds=%@", NSStringFromCGRect(self.view.frame), NSStringFromCGRect(self.view.bounds));
}


- (void)showModalButtonWasTouched
{
    self.interactor = [[BTSlideInteractor alloc] init];
    self.interactor.presenting = YES;
    
    //BTModalViewController *modalController = [self.storyboard instantiateViewControllerWithIdentifier:@"ModalViewController"];
    modalController = [[InitialSlidingViewController alloc] init];
    modalController.modalPresentationStyle = UIModalPresentationCustom;
    modalController.transitioningDelegate = self.interactor;
    [self.navigationController presentViewController:modalController animated:YES completion:nil];
    //[self.navigationController popToViewController:modalController animated:NO];
    //[self.navigationController pushViewController:modalController animated:YES];
}
// End Slide Menu

-(void)myHomeAction:(UIBarButtonItem *)sender{
    
    appdelegate.nextPage = @"";
    
    if ([appdelegate.listOfVerMenu count]>0) {
        if (appdelegate.homeclicked==NO) {
            appdelegate.homeclicked = YES;
            [self showModalButtonWasTouched];
        } else {
            appdelegate.homeclicked = NO;
            [modalController dismissViewControllerAnimated:YES completion:nil];
        }
    } else {
        //AppDelegate *appdelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
        //NSLog(@"View Controllers : %@",[appdelegate.navController viewControllers]);
        NSMutableArray *navigationArray = [[NSMutableArray alloc] initWithArray: appdelegate.navController.viewControllers];
        
        for (UIViewController* viewController in appdelegate.navController.viewControllers) {
            
            //This if condition checks whether the viewController's class is MyGroupViewController
            // if true that means its the MyGroupViewController (which has been pushed at some point)
            if ([viewController isKindOfClass:[RootViewController class]] ) {
                
                // Here viewController is a reference of UIViewController base class of MyGroupViewController
                // but viewController holds MyGroupViewController  object so we can type cast it here
                //ThemeViewController *themeViewController = (ThemeViewController*)viewController;
                //[self.navigationController popToViewController:themeViewController animated:YES];
                [navigationArray removeObjectAtIndex: 0];
            }
        }
        appdelegate.navController.viewControllers = navigationArray;
        for (UIViewController* viewController in appdelegate.navController.viewControllers) {
            
            //This if condition checks whether the viewController's class is MyGroupViewController
            // if true that means its the MyGroupViewController (which has been pushed at some point)
            if ([viewController isKindOfClass:[ViewController class]] ) {
                
                // Here viewController is a reference of UIViewController base class of MyGroupViewController
                // but viewController holds MyGroupViewController  object so we can type cast it here
                ViewController *themeViewController = (ViewController*)viewController;
                [appdelegate.navController popToViewController:themeViewController animated:NO];
                //[self.navigationController popToViewController:viewController animated:YES];
            }
        }
        //NSLog(@"View Controllers111 : %@",appdelegate.navController.viewControllers);
    }
    
}

#pragma mark CONNECT TO SERVER
-(void) connectToServer:(NSString *) urlPath {
    [self addLoadingView];
    NSString *urlAddress =[NSString stringWithFormat:@"%@", urlPath];
    
    /*NSURL *url = [NSURL URLWithString:urlAddress];
    
    NSURLRequest *requestObj = [NSURLRequest requestWithURL:url
                                                cachePolicy:NSURLRequestReloadIgnoringLocalAndRemoteCacheData
                                            timeoutInterval:30];
    
    NSURLConnection *theConnection=[[NSURLConnection alloc] initWithRequest:requestObj delegate:self];
    if (theConnection) {
        // Create the NSMutableData to hold the received data.
        // receivedData is an instance variable declared elsewhere.
        receivedData = [NSMutableData data];
    } else {
        // Inform the user that the connection failed.
        //[self displayAlert:@"Connection Error. Please try again later."];
        [self displayAlert:@"Connection error. Please try again later."];
        [self removeLoadingView];
    } */
    [self getJsonResponse:urlAddress success:^(NSDictionary *responseDict) {
        
        //NSLog(@"%@",responseDict);
        [self successfunction:responseDict];
        
    } failure:^(NSError *error) {
        // release the connection, and the data object
        
        [self removeLoadingView];
        [self displayAlert:@"No internet connection available. Standard Mobile Apps requires internet connection to function properly."];
    }];
}

-(void)getJsonResponse : (NSString *)urlStr success : (void (^)(NSDictionary *responseDict))success failure:(void(^)(NSError* error))failure
{
    
    
    NSURLSession * session = [NSURLSession sharedSession];
    NSURL * url = [NSURL URLWithString: urlStr];
    
    
    // Asynchronously Api is hit here
    NSURLSessionDataTask * dataTask = [session dataTaskWithURL:url completionHandler:^(NSData *data, NSURLResponse *response, NSError *error)
                                       {
                                           
                                           //NSLog(@"%@",data);
                                           NSDictionary * json;
                                           if ([data length]>0) {
                                               json  = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
                                               //NSLog(@"%@",json);
                                               success(json);
                                           } else {
                                               json = nil;
                                               dispatch_async(dispatch_get_main_queue(), ^ {
                                                   [self removeLoadingView];
                                                   [self displayAlert:NSLocalizedString(@"MSG_CONNECTION_ERROR", nil)];
                                               });
                                           }
                                           
                                           
                                       }];
    
    [dataTask resume] ; // Executed First
    
    
}

-(void) successfunction:(NSDictionary *)responseDict {
    dispatch_async(dispatch_get_main_queue(), ^{
        //NSString *jsonString = [responseDict JSONRepresentation];
        [self removeLoadingView];
        
        if ([currentRequest isEqualToString:@"UpdateDetails"])
        {
            NSDictionary *dictionary = responseDict;
            
            for(NSDictionary *row in dictionary)
            {
                alertmsgstatus = [row objectForKey:@"status"];
                [self displayAlert:[row objectForKey:@"desc"]];
            }
        }
        
        if ([currentRequest isEqualToString:@"ViewDetails"])
        {
            NSDictionary *dictionary = responseDict;
            
            for(NSDictionary *row in dictionary)
            {
                
                txt_fullname.text = [row objectForKey:@"name"];
                
                NSPredicate *thePredicate = [NSPredicate predicateWithFormat:@"%K LIKE %@", @"gender_code", [row objectForKey:@"gender"]];
                NSArray *filteredList = [listOfgender filteredArrayUsingPredicate:thePredicate];
                NSDictionary *selectedDict = [[NSDictionary alloc]init];
                selectedDict = [filteredList lastObject];
                if ([selectedDict count]) {
                    txt_gender.text = [NSString stringWithFormat:@"%@",[selectedDict objectForKey:@"gender_name"]];
                    gendercode = [selectedDict objectForKey:@"gender_code"];
                } else {
                    txt_gender.text = @"";
                }
                
                
                txt_telephone.text = [row objectForKey:@"mobile"];
                
                NSPredicate *thePredicate1 = [NSPredicate predicateWithFormat:@"%K LIKE %@", @"language_code", [row objectForKey:@"language"]];
                NSArray *filteredList1 = [listOflang filteredArrayUsingPredicate:thePredicate1];
                NSDictionary *selectedDict1 = [[NSDictionary alloc]init];
                selectedDict1 = [filteredList1 lastObject];
                if ([selectedDict1 count]) {
                    txt_language.text = [NSString stringWithFormat:@"%@",[selectedDict1 objectForKey:@"language_name"]];
                    langcode = [selectedDict1 objectForKey:@"language_code"];
                } else {
                    txt_language.text = @"";
                }
                
                NSPredicate *thePredicate2 = [NSPredicate predicateWithFormat:@"%K LIKE %@", @"country_code", [row objectForKey:@"country"]];
                NSArray *filteredList2 = [listOfcountry filteredArrayUsingPredicate:thePredicate2];
                NSDictionary *selectedDict2 = [[NSDictionary alloc]init];
                selectedDict2 = [filteredList2 lastObject];
                if ([selectedDict2 count]) {
                    txt_country.text = [NSString stringWithFormat:@"%@",[selectedDict2 objectForKey:@"country_name"]];
                    countrycode = [selectedDict2 objectForKey:@"country_code"];
                } else {
                    txt_country.text = @"";
                }
                
                NSPredicate *thePredicate3 = [NSPredicate predicateWithFormat:@"%K LIKE %@", @"state_code", [row objectForKey:@"state"]];
                NSArray *filteredList3 = [listOfstatetemp filteredArrayUsingPredicate:thePredicate3];
                NSDictionary *selectedDict3 = [[NSDictionary alloc]init];
                selectedDict3 = [filteredList3 lastObject];
                //NSLog(@"listOfstate :: %@", listOfstate);
                if ([selectedDict3 count]) {
                    txt_state.text = [NSString stringWithFormat:@"%@",[selectedDict3 objectForKey:@"state_name"]];
                    statecode = [selectedDict3 objectForKey:@"state_code"];
                } else {
                    txt_state.text = @"";
                }
                //[self displayAlert:[row objectForKey:@"desc"]];
                
            }
        }
    });
    
}

/*- (void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response
{
    [receivedData setLength:0];
}

- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data
{
    [receivedData appendData:data];
}

- (void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error {
    // release the connection, and the data object
    
    connection =nil;
    receivedData =nil;
    
    [self removeLoadingView];
    [self displayAlert:@"No internet connection available. Standard Mobile Apps requires internet connection to function properly."];
}

- (void)connectionDidFinishLoading:(NSURLConnection *)connection
{
    //NSLog(@"Succeed! Received %d bytes of data",[receivedData length]);
    [self removeLoadingView];
    
    NSString *response = [[NSString alloc] initWithData:receivedData encoding:NSUTF8StringEncoding];
    //NSLog(@"response :: %@", response);
    if ([currentRequest isEqualToString:@"UpdateDetails"])
    {
        NSDictionary *dictionary = [response JSONValue];
        
        for(NSDictionary *row in dictionary)
        {
            alertmsgstatus = [row objectForKey:@"status"];
            [self displayAlert:[row objectForKey:@"desc"]];
        }
    }
    
    if ([currentRequest isEqualToString:@"ViewDetails"])
    {
        NSDictionary *dictionary = [response JSONValue];
        
        for(NSDictionary *row in dictionary)
        {
            txt_fullname.text = [row objectForKey:@"name"];
            
            NSPredicate *thePredicate = [NSPredicate predicateWithFormat:@"%K LIKE %@", @"gender_code", [row objectForKey:@"gender"]];
            NSArray *filteredList = [listOfgender filteredArrayUsingPredicate:thePredicate];
            NSDictionary *selectedDict = [[NSDictionary alloc]init];
            selectedDict = [filteredList lastObject];
            if ([selectedDict count]) {
                txt_gender.text = [NSString stringWithFormat:@"%@",[selectedDict objectForKey:@"gender_name"]];
                gendercode = [selectedDict objectForKey:@"gender_code"];
            } else {
                txt_gender.text = @"";
            }
            
            
            txt_telephone.text = [row objectForKey:@"mobile"];
            
            NSPredicate *thePredicate1 = [NSPredicate predicateWithFormat:@"%K LIKE %@", @"language_code", [row objectForKey:@"language"]];
            NSArray *filteredList1 = [listOflang filteredArrayUsingPredicate:thePredicate1];
            NSDictionary *selectedDict1 = [[NSDictionary alloc]init];
            selectedDict1 = [filteredList1 lastObject];
            if ([selectedDict1 count]) {
                txt_language.text = [NSString stringWithFormat:@"%@",[selectedDict1 objectForKey:@"language_name"]];
                langcode = [selectedDict1 objectForKey:@"language_code"];
            } else {
                txt_language.text = @"";
            }
            
            NSPredicate *thePredicate2 = [NSPredicate predicateWithFormat:@"%K LIKE %@", @"country_code", [row objectForKey:@"country"]];
            NSArray *filteredList2 = [listOfcountry filteredArrayUsingPredicate:thePredicate2];
            NSDictionary *selectedDict2 = [[NSDictionary alloc]init];
            selectedDict2 = [filteredList2 lastObject];
            if ([selectedDict2 count]) {
                txt_country.text = [NSString stringWithFormat:@"%@",[selectedDict2 objectForKey:@"country_name"]];
                countrycode = [selectedDict2 objectForKey:@"country_code"];
            } else {
                txt_country.text = @"";
            }
            
            NSPredicate *thePredicate3 = [NSPredicate predicateWithFormat:@"%K LIKE %@", @"state_code", [row objectForKey:@"state"]];
            NSArray *filteredList3 = [listOfstatetemp filteredArrayUsingPredicate:thePredicate3];
            NSDictionary *selectedDict3 = [[NSDictionary alloc]init];
            selectedDict3 = [filteredList3 lastObject];
            //NSLog(@"listOfstate :: %@", listOfstate);
            if ([selectedDict3 count]) {
                txt_state.text = [NSString stringWithFormat:@"%@",[selectedDict3 objectForKey:@"state_name"]];
                statecode = [selectedDict3 objectForKey:@"state_code"];
            } else {
                txt_state.text = @"";
            }
            //[self displayAlert:[row objectForKey:@"desc"]];
        }
    }
    
    connection =nil;
    response =nil;
    receivedData =nil;
} */

#pragma mark LoadingView
-(void) addLoadingView
{
    loadingView = [LoadingView loadingViewInView:self.view];
}

-(void) removeLoadingView
{
    [loadingView removeView];
}

#pragma mark displayAlert
-(void) displayAlert: (NSString *) stralert {
    NSString *str = [[NSString alloc] initWithFormat:@"%@", stralert];
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:appdelegate.iconname
                                                    message:str
                                                   delegate:self
                                          cancelButtonTitle:NSLocalizedString(@"done", nil)
                                          otherButtonTitles:nil];
    
    [alert show];
    str =nil;
    alert =nil;
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    //NSLog(@"buttonIndex%d", buttonIndex);
    if (buttonIndex == 0) {
        //cancel clicked ...do your action
        if ([alertmsgstatus isEqualToString:@"1"]) {
            NSString *nibFileName;
            if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone) {
                nibFileName = @"SettingViewController";
            } else {
                nibFileName = @"SettingViewController_ipad";
            }
            SettingViewController *settingViewController = [[SettingViewController alloc] initWithNibName:nibFileName bundle:nil];
            settingViewController.cont_title = conttitle;
            [appdelegate.navController pushViewController:settingViewController animated:NO];
        }
    }else{
        //reset clicked
    }
    alertmsgstatus = @"";
}

-(void)textFieldDidBeginEditing:(UITextField *)textField
{
    CGRect textFieldRect =
    [self.view.window convertRect:textField.bounds fromView:textField];
    CGRect viewRect =
    [self.view.window convertRect:self.view.bounds fromView:self.view];
    
    CGFloat midline = textFieldRect.origin.y + 0.5 * textFieldRect.size.height;
    CGFloat numerator =
    midline - viewRect.origin.y
    - MINIMUM_SCROLL_FRACTION * viewRect.size.height;
    CGFloat denominator =
    (MAXIMUM_SCROLL_FRACTION - MINIMUM_SCROLL_FRACTION)
    * viewRect.size.height;
    CGFloat heightFraction = numerator / denominator;
    if (heightFraction < 0.0)
    {
        heightFraction = 0.0;
    }
    else if (heightFraction > 1.0)
    {
        heightFraction = 1.0;
    }
    
    
    animatedDistance = floor(PORTRAIT_KEYBOARD_HEIGHT * heightFraction);
    
    CGRect viewFrame = self.view.frame;
    viewFrame.origin.y -= animatedDistance;
    
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationBeginsFromCurrentState:YES];
    [UIView setAnimationDuration:KEYBOARD_ANIMATION_DURATION];
    
    [self.view setFrame:viewFrame];
    
    [UIView commitAnimations];
}

- (void)textFieldDidEndEditing:(UITextField *)textField

{
    CGRect viewFrame = self.view.frame;
    viewFrame.origin.y += animatedDistance;
    
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationBeginsFromCurrentState:YES];
    [UIView setAnimationDuration:KEYBOARD_ANIMATION_DURATION];
    
    [self.view setFrame:viewFrame];
    
    [UIView commitAnimations];
    
}

#pragma mark WEPopoverControllerDelegate implementation

- (void)popoverControllerDidDismissPopover:(WEPopoverController *)thePopoverController {
    //Safe to release the popover here
    self.genderPopoverController = nil;
    self.langPopoverController = nil;
    self.countryPopoverController = nil;
    self.statePopoverController = nil;
}

- (BOOL)popoverControllerShouldDismissPopover:(WEPopoverController *)thePopoverController {
    //The popover is automatically dismissed if you click outside it, unless you return NO here
    return YES;
}

- (WEPopoverContainerViewProperties *)improvedContainerViewProperties {
    
    WEPopoverContainerViewProperties *props = [WEPopoverContainerViewProperties alloc];
    NSString *bgImageName = nil;
    CGFloat bgMargin = 0.0;
    CGFloat bgCapSize = 0.0;
    CGFloat contentMargin = 4.0;
    
    bgImageName = @"popoverBg.png";
    
    // These constants are determined by the popoverBg.png image file and are image dependent
    bgMargin = 13; // margin width of 13 pixels on all sides popoverBg.png (62 pixels wide - 36 pixel background) / 2 == 26 / 2 == 13
    bgCapSize = 31; // ImageSize/2  == 62 / 2 == 31 pixels
    
    props.leftBgMargin = bgMargin;
    props.rightBgMargin = bgMargin;
    props.topBgMargin = bgMargin;
    props.bottomBgMargin = bgMargin;
    props.leftBgCapSize = bgCapSize;
    props.topBgCapSize = bgCapSize;
    props.bgImageName = bgImageName;
    props.leftContentMargin = contentMargin;
    props.rightContentMargin = contentMargin - 1; // Need to shift one pixel for border to look correct
    props.topContentMargin = contentMargin;
    props.bottomContentMargin = contentMargin;
    
    props.arrowMargin = 4.0;
    
    props.upArrowImageName = @"popoverArrowUp.png";
    props.downArrowImageName = @"popoverArrowDown.png";
    props.leftArrowImageName = @"popoverArrowLeft.png";
    props.rightArrowImageName = @"popoverArrowRight.png";
    return props;
}

- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField {
    
    if(textField == txt_gender)
    {
        [genderTableView reloadData];
        
        if (genderContent == nil )
        {
            genderContent = [[UIViewController alloc] init];
            genderContent.view = genderTableView;
        }
        else
        {
            if (genderContent.view != genderTableView) {
                genderContent.view = genderTableView;
            }
        }
        
        if (!self.genderPopoverController)
        {
            self.genderPopoverController = [[WEPopoverController alloc] initWithContentViewController:genderContent];
            self.genderPopoverController.popoverContentSize = CGSizeMake(400.0f, 600.0f);
            
            if ([self.genderPopoverController respondsToSelector:@selector(setContainerViewProperties:)])
                [self.genderPopoverController setContainerViewProperties:[self improvedContainerViewProperties]];
            
            self.genderPopoverController.delegate = self;
            self.genderPopoverController.passthroughViews = [NSArray arrayWithObject:appdelegate.navController.navigationBar];
            [self.genderPopoverController presentPopoverFromRect:textField.bounds inView:textField permittedArrowDirections:UIPopoverArrowDirectionUp animated:NO];
        }
        else
        {
            [self.genderPopoverController dismissPopoverAnimated:NO];
            self.genderPopoverController = nil;
        }
        return NO;  // Hide both keyboard and blinking cursor.
    }
    
    if(textField == txt_language){
        
        [langTableView reloadData];
        
        if (langContent == nil )
        {
            langContent = [[UIViewController alloc] init];
            langContent.view = langTableView;
        }
        else
        {
            if (langContent.view != langTableView) {
                langContent.view = langTableView;
            }
        }
        
        if (!self.langPopoverController)
        {
            self.langPopoverController = [[WEPopoverController alloc] initWithContentViewController:langContent];
            self.langPopoverController.popoverContentSize = CGSizeMake(400.0f, 600.0f);
            if ([self.langPopoverController respondsToSelector:@selector(setContainerViewProperties:)])
                [self.langPopoverController setContainerViewProperties:[self improvedContainerViewProperties]];
            
            self.langPopoverController.delegate = self;
            self.langPopoverController.passthroughViews = [NSArray arrayWithObject:appdelegate.navController.navigationBar];
            [self.langPopoverController presentPopoverFromRect:textField.bounds inView:textField permittedArrowDirections:UIPopoverArrowDirectionUp animated:NO];
        }
        else
        {
            [self.langPopoverController dismissPopoverAnimated:NO];
            self.langPopoverController = nil;
        }
        
        return NO;  // Hide both keyboard and blinking cursor.
        
    }
    
    if(textField == txt_country){
        
        [countryTableView reloadData];
        
        if (countryContent == nil )
        {
            countryContent = [[UIViewController alloc] init];
            countryContent.view = countryTableView;
        }
        else
        {
            if (countryContent.view != countryTableView) {
                countryContent.view = countryTableView;
            }
        }
        
        if (!self.countryPopoverController) {
            self.countryPopoverController = [[WEPopoverController alloc] initWithContentViewController:countryContent];
            self.countryPopoverController.popoverContentSize = CGSizeMake(400.0f, 600.0f);
            if ([self.countryPopoverController respondsToSelector:@selector(setContainerViewProperties:)]) {
                [self.countryPopoverController setContainerViewProperties:[self improvedContainerViewProperties]];
            }
            self.countryPopoverController.delegate = self;
            self.countryPopoverController.passthroughViews = [NSArray arrayWithObject:appdelegate.navController.navigationBar];
            [self.countryPopoverController presentPopoverFromRect:textField.bounds inView:textField permittedArrowDirections:UIPopoverArrowDirectionUp animated:NO];
            
        } else {
            [self.countryPopoverController dismissPopoverAnimated:NO];
            self.countryPopoverController = nil;
        }
        
        return NO;  // Hide both keyboard and blinking cursor.
        
    }
    
    if(textField == txt_state){
        
        [listOfstate removeAllObjects];
        
        listOfstate = [[NSMutableArray alloc] init];
        
        for (int i=0;i<[listOfstatetemp count];i++)
        {
            NSDictionary* item=[listOfstatetemp objectAtIndex:i];
            
            //NSLog(@"item %@", item);
            //NSLog(@"countrycode %@", [item objectForKey:@"country_code"]);
            if ([countrycode isEqualToString:[item objectForKey:@"country_code"] ])
            {
                [listOfstate addObject:item];
            }
        }
        
        //NSLog(@"listOfstate %@", listOfstate);
        
        [stateTableView reloadData];
        
        if (stateContent == nil )
        {
            stateContent = [[UIViewController alloc] init];
            stateContent.view = stateTableView;
        }
        else
        {
            if (stateContent.view != stateTableView) {
                stateContent.view = stateTableView;
            }
        }
        
        if (!self.statePopoverController) {
            self.statePopoverController = [[WEPopoverController alloc] initWithContentViewController:stateContent];
            self.statePopoverController.popoverContentSize = CGSizeMake(400.0f, 600.0f);
            if ([self.statePopoverController respondsToSelector:@selector(setContainerViewProperties:)]) {
                [self.statePopoverController setContainerViewProperties:[self improvedContainerViewProperties]];
            }
            self.statePopoverController.delegate = self;
            self.statePopoverController.passthroughViews = [NSArray arrayWithObject:appdelegate.navController.navigationBar];
            [self.statePopoverController presentPopoverFromRect:textField.bounds inView:textField permittedArrowDirections:UIPopoverArrowDirectionUp animated:NO];
            
        } else {
            [self.statePopoverController dismissPopoverAnimated:NO];
            self.statePopoverController = nil;
        }
        
        return NO;  // Hide both keyboard and blinking cursor.
        
    }
    
    return YES;
}

-(void) viewDidDisappear:(BOOL)animated {
    
    [super viewDidDisappear:animated];
    
    appdelegate.homeclicked = NO;
    [modalController dismissViewControllerAnimated:YES completion:nil];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)viewWillDisappear:(BOOL)animated {
    [genderPopoverController dismissPopoverAnimated:NO];
    //[branchPopoverController release];
    genderPopoverController = nil;
    [langPopoverController dismissPopoverAnimated:NO];
    //[prodPopoverController release];
    langPopoverController = nil;
    [countryPopoverController dismissPopoverAnimated:NO];
    //[dselectPopoverController release];
    countryPopoverController = nil;
    [statePopoverController dismissPopoverAnimated:NO];
    //[dselectPopoverController release];
    statePopoverController = nil;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    
    if(tableView == genderTableView){
        return [listOfgender count];
    }
    
    if(tableView == langTableView){
        return [listOflang count];
    }
    
    if(tableView == countryTableView){
        return [listOfcountry count];
    }
    
    if(tableView == stateTableView){
        return [listOfstate count];
    }
    
    return 1;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if(tableView == genderTableView){
        return 30;
    } else if(tableView == langTableView){
        return 30;
    } else if(tableView == countryTableView){
        return 30;
    } else if(tableView == stateTableView){
        return 30;
    }
    
    return 44;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell;
    
    if(tableView == genderTableView) {
        static NSString *CellIdentifier = @"CellIdentifier";
        cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        UILabel *cname;
        
        const NSInteger LEFT_TOP_TAG = 2000;
        
        if (cell == nil)
        {
            cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
            
            cname = [[UILabel alloc] initWithFrame: CGRectMake(5, 10, 310, 20)];
            cname.tag = LEFT_TOP_TAG;
            cname.textColor = [UIColor blackColor];
            cname.textAlignment = NSTextAlignmentLeft;
            cname.backgroundColor = [UIColor clearColor];
            cname.numberOfLines = 0;
            [cname setFont:[UIFont systemFontOfSize:14]];
            [cell.contentView addSubview:cname];
        }
        else
            cname = (UILabel *)[cell viewWithTag:LEFT_TOP_TAG];
        
        if ([listOfgender count]>0)
        {
            NSDictionary *data = [listOfgender objectAtIndex:indexPath.row];
            NSString *sname = [data objectForKey:@"gender_name"];
            
            cname.text = [NSString stringWithFormat:@"%@",sname];
        }
    }
    
    if(tableView == langTableView) {
        static NSString *CellIdentifier = @"CellIdentifier";
        cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        UILabel *cname;
        
        const NSInteger LEFT_TOP_TAG = 2000;
        
        if (cell == nil)
        {
            cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
            
            cname = [[UILabel alloc] initWithFrame: CGRectMake(5, 10, 310, 20)];
            cname.tag = LEFT_TOP_TAG;
            cname.textColor = [UIColor blackColor];
            cname.textAlignment = NSTextAlignmentLeft;
            cname.backgroundColor = [UIColor clearColor];
            cname.numberOfLines = 0;
            [cname setFont:[UIFont systemFontOfSize:14]];
            [cell.contentView addSubview:cname];
        }
        else
            cname = (UILabel *)[cell viewWithTag:LEFT_TOP_TAG];
        
        if ([listOflang count]>0)
        {
            NSDictionary *data = [listOflang objectAtIndex:indexPath.row];
            NSString *sname = [data objectForKey:@"language_name"];
            
            cname.text = [NSString stringWithFormat:@"%@",sname];
        }
    }
    
    if(tableView == countryTableView) {
        static NSString *CellIdentifier = @"CellIdentifier";
        cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        UILabel *cname;
        
        const NSInteger LEFT_TOP_TAG = 2000;
        
        if (cell == nil)
        {
            cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
            
            cname = [[UILabel alloc] initWithFrame: CGRectMake(5, 10, 310, 20)];
            cname.tag = LEFT_TOP_TAG;
            cname.textColor = [UIColor blackColor];
            cname.textAlignment = NSTextAlignmentLeft;
            cname.backgroundColor = [UIColor clearColor];
            cname.numberOfLines = 0;
            [cname setFont:[UIFont systemFontOfSize:14]];
            [cell.contentView addSubview:cname];
        }
        else
            cname = (UILabel *)[cell viewWithTag:LEFT_TOP_TAG];
        
        if ([listOfcountry count]>0)
        {
            NSDictionary *data = [listOfcountry objectAtIndex:indexPath.row];
            NSString *sname = [data objectForKey:@"country_name"];
            
            cname.text = [NSString stringWithFormat:@"%@",sname];
        }
    }
    
    if(tableView == stateTableView) {
        static NSString *CellIdentifier = @"CellIdentifier";
        cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        UILabel *cname;
        
        const NSInteger LEFT_TOP_TAG = 2000;
        
        if (cell == nil)
        {
            cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
            
            cname = [[UILabel alloc] initWithFrame: CGRectMake(5, 10, 310, 20)];
            cname.tag = LEFT_TOP_TAG;
            cname.textColor = [UIColor blackColor];
            cname.textAlignment = NSTextAlignmentLeft;
            cname.backgroundColor = [UIColor clearColor];
            cname.numberOfLines = 0;
            [cname setFont:[UIFont systemFontOfSize:14]];
            [cell.contentView addSubview:cname];
        }
        else
            cname = (UILabel *)[cell viewWithTag:LEFT_TOP_TAG];
        
        if ([listOfstate count]>0)
        {
            NSDictionary *data = [listOfstate objectAtIndex:indexPath.row];
            NSString *sname = [data objectForKey:@"state_name"];
            
            cname.text = [NSString stringWithFormat:@"%@",sname];
        }
    }
    
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if(tableView == genderTableView){
        NSDictionary *data = [listOfgender objectAtIndex:indexPath.row];
        NSString *cname = [data objectForKey:@"gender_name"];
        NSString *ccode = [data objectForKey:@"gender_code"];
        txt_gender.text = cname;
        gendercode = ccode;
        [genderPopoverController dismissPopoverAnimated:NO];
        self.genderPopoverController = nil;
    }
    
    if(tableView == langTableView){
        NSDictionary *data = [listOflang objectAtIndex:indexPath.row];
        NSString *cname = [data objectForKey:@"language_name"];
        NSString *ccode = [data objectForKey:@"language_code"];
        txt_language.text = cname;
        langcode = ccode;
        [langPopoverController dismissPopoverAnimated:NO];
        self.langPopoverController = nil;
    }
    
    if(tableView == countryTableView){
        NSDictionary *data = [listOfcountry objectAtIndex:indexPath.row];
        NSString *cname = [data objectForKey:@"country_name"];
        NSString *ccode = [data objectForKey:@"country_code"];
        txt_country.text = cname;
        countrycode = ccode;
        [countryPopoverController dismissPopoverAnimated:NO];
        self.countryPopoverController = nil;
    }
    
    if(tableView == stateTableView){
        NSDictionary *data = [listOfstate objectAtIndex:indexPath.row];
        NSString *cname = [data objectForKey:@"state_name"];
        NSString *ccode = [data objectForKey:@"state_code"];
        txt_state.text = cname;
        statecode = ccode;
        [statePopoverController dismissPopoverAnimated:NO];
        self.statePopoverController = nil;
    }
    
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    [textField resignFirstResponder];
    return YES;
}

-(NSString *) urlEncode:(NSString *) theStr {
    /*NSString *encoded = (NSString *)CFURLCreateStringByAddingPercentEscapes(
     NULL,
     (CFStringRef)theStr,
     NULL,
     (CFStringRef)@"!*'\"();:@&=+$,/?%#[]% ",
     //																		NSUnicodeStringEncoding );
     kCFStringEncodingUTF8 ); */
    NSString *encoded = (NSString *)CFBridgingRelease(CFURLCreateStringByAddingPercentEscapes(
                                                                                              NULL,
                                                                                              (__bridge CFStringRef) theStr,
                                                                                              NULL,
                                                                                              CFSTR("!*'();:@&=+$,/?%#[]\" "),
                                                                                              kCFStringEncodingUTF8));
    return encoded;
    //return nil;
}

- (void)dealloc {
    currentRequest =nil;
    //receivedData =nil;
    alertmsgstatus = nil;
    scrollview =nil;
    //[super dealloc];
    [genderPopoverController dismissPopoverAnimated:NO];
    //[branchPopoverController release];
    genderPopoverController = nil;
    [langPopoverController dismissPopoverAnimated:NO];
    //[prodPopoverController release];
    langPopoverController = nil;
    [countryPopoverController dismissPopoverAnimated:NO];
    //[prodPopoverController release];
    countryPopoverController = nil;
    [statePopoverController dismissPopoverAnimated:NO];
    //[prodPopoverController release];
    statePopoverController = nil;
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
