//
//  ContentPluginViewController.h
//  standardmobileappsv3
//
//  Created by M3Online on 5/30/14.
//  Copyright (c) 2014 M3Online. All rights reserved.
//

#import <UIKit/UIKit.h>
//#import <SDWebImage/UIButton+WebCache.h>
//#import <SDWebImage/UIImageView+WebCache.h>
#import "LoadingView.h"

@interface EventPluginViewController : UIViewController <UITableViewDataSource, UITableViewDelegate, UITextFieldDelegate, UIViewControllerTransitioningDelegate> {
    NSMutableArray *listOfContents;
    NSDictionary *dicContents;
    NSMutableArray *listOfSettings;
    NSDictionary *dicSettings;

    
    IBOutlet UITableView *MenutableView;
    
    //NSMutableData* receivedData;
    NSString *currentRequestMenu;
    
    UIView *view_popoverMain;
    UIView *view_plain;
    UIButton *buttonclose;
    UIButton *buttonlogin;
    UILabel *lbllogin;
    
    NSString *topId;

}

@property (nonatomic, strong) NSMutableArray *listOfContents;
@property (nonatomic, strong) NSMutableArray *listOfSettings;

@property (nonatomic, strong) UITableView *MenutableView;

@property (strong, nonatomic) IBOutlet UIScrollView *scrollView;

//@property (nonatomic, retain) NSMutableData *receivedData;
@property (nonatomic, retain) NSString *currentRequestMenu;

@property (retain, nonatomic) IBOutlet UIView *view_popoverMain;

@property (nonatomic, strong) NSString *topId;

@property (strong, nonatomic) NSString *cont_title;

- (IBAction)logIn:(id)sender;
- (IBAction)closeLoginPopup:(id)sender;

@end
