//
//  ContentPluginViewController.m
//  standardmobileappsv3
//
//  Created by M3Online on 5/30/14.
//  Copyright (c) 2014 M3Online. All rights reserved.
//

#import "EventPluginViewController.h"
#import "AppDelegate.h"
#import <sqlite3.h>
#import "Item.h"
#import "NSString+HTML.h"
#import <CoreLocation/CoreLocation.h>
#import "RootViewController.h"
#import "ViewController.h"
#import "SBJson.h"
#import "InitialSlidingViewController.h"
#import "BTSlideInteractor.h"
#import "ThemeViewController.h"
#import "GpsNavigateViewController.h"

@interface EventPluginViewController ()
@property (strong, nonatomic) BTSlideInteractor *interactor;
@end

@implementation EventPluginViewController
LoadingView *loadingView;
UIAlertView *alert;
UIActivityIndicatorView *indicator;
CLLocationManager *locationManager;

@synthesize listOfContents, topId, cont_title, listOfSettings;
@synthesize MenutableView, currentRequestMenu, view_popoverMain;

InitialSlidingViewController *modalController;

AppDelegate *appdelegate;

- (UIColor *) colorWithHexString:(NSString *)hexString {
    NSString *colorString = [[hexString stringByReplacingOccurrencesOfString: @"#" withString: @""] uppercaseString];
    CGFloat alpha, red, blue, green;
    alpha = 1.0f;
    red   = [self colorComponentFrom: colorString start: 0 length: 2];
    green = [self colorComponentFrom: colorString start: 2 length: 2];
    blue  = [self colorComponentFrom: colorString start: 4 length: 2];
    return [UIColor colorWithRed: red green: green blue: blue alpha: alpha];
}

- (CGFloat) colorComponentFrom:(NSString *)string start:(NSUInteger)start length:(NSUInteger)length {
    NSString *substring = [string substringWithRange: NSMakeRange(start, length)];
    NSString *fullHex = length == 2 ? substring : [NSString stringWithFormat: @"%@%@", substring, substring];
    unsigned hexComponent;
    [[NSScanner scannerWithString: fullHex] scanHexInt: &hexComponent];
    return hexComponent / 255.0;
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (IBAction)closePopover:(id)sender
{
    [view_popoverMain removeFromSuperview];
    //[view_popoverMain setHidden:NO];
}

- (IBAction)logIn:(id)sender
{
    NSString *nibFileName;
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone) {
        nibFileName = @"SettingViewController";
    } else {
        nibFileName = @"SettingViewController_ipad";
    }
    SettingViewController *s = [[SettingViewController alloc] initWithNibName:nibFileName bundle:nil];
    appdelegate.shouldReturnToPrevPage = YES;
    //LoyaltyViewController *l = [[LoyaltyViewController alloc] initWithNibName:@"LoyaltyViewController" bundle:nil];
    appdelegate.originVC = self;
    [appdelegate.navController pushViewController:s animated:NO];
    
    [view_popoverMain removeFromSuperview];
}

- (IBAction)closeLoginPopup:(id)sender
{
    [view_popoverMain removeFromSuperview];
}


- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.title = cont_title;
    self.view.backgroundColor = [UIColor clearColor];
    self.navigationItem.backBarButtonItem = [[UIBarButtonItem alloc]initWithTitle:@"" style:UIBarButtonItemStylePlain target:nil action:nil];
    
    appdelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    
    CGRect rect = CGRectMake(0,0,20,20);
    UIGraphicsBeginImageContext( rect.size );
    [[UIImage imageNamed:@"home.png"] drawInRect:rect];
    UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    UIBarButtonItem *myHomeButton = [[UIBarButtonItem alloc] initWithImage:image style:UIBarButtonItemStyleDone target:self action:@selector(myHomeAction:)];
    
    
    NSArray *myButtonArray = [[NSArray alloc] initWithObjects:myHomeButton, nil];
    
    self.navigationItem.rightBarButtonItems = myButtonArray;
    
    listOfContents = [[NSMutableArray alloc] init];
    listOfSettings = [[NSMutableArray alloc] init];
    
    [self.view addSubview:view_popoverMain];
    [view_popoverMain setHidden:YES];

}

/*- (NSString *)deviceLocation {
    return [NSString stringWithFormat:@"latitude: %f longitude: %f", locationManager.location.coordinate.latitude, locationManager.location.coordinate.longitude];
} */

-(void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSString *hasLoggedInFB = [defaults objectForKey:@"hasLoggedInFB"];
    NSString *hasLoggedInMyApp = [defaults objectForKey:@"hasLoggedInMyApp"];
    
    if ([hasLoggedInFB isEqualToString:@"YES"] || [hasLoggedInMyApp isEqualToString:@"YES"])
    {
        [view_popoverMain setHidden:YES];
        NSString *urlAddress = [[NSString alloc] initWithFormat:@"%@event_general_setting.php?db=%@&userid=%@&udid=%@&cuid=%@&top=%@",appdelegate.user_apipath,appdelegate.db,appdelegate.user,appdelegate.uuid,appdelegate.customer_id,topId];
        currentRequestMenu = @"GetSettings";
        //NSLog(@"GetSettingsUrlAddress:: %@", urlAddress);
        [self connectToServer:urlAddress];
    }
    else {
        [view_popoverMain setHidden:NO];
    }
}

//Slide Menu
- (void)willAnimateRotationToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation duration:(NSTimeInterval)duration
{
    //NSLog(@"view frame=%@ bounds=%@", NSStringFromCGRect(self.view.frame), NSStringFromCGRect(self.view.bounds));
}


- (void)showModalButtonWasTouched
{
    self.interactor = [[BTSlideInteractor alloc] init];
    self.interactor.presenting = YES;
    
    //BTModalViewController *modalController = [self.storyboard instantiateViewControllerWithIdentifier:@"ModalViewController"];
    modalController = [[InitialSlidingViewController alloc] init];
    modalController.modalPresentationStyle = UIModalPresentationCustom;
    modalController.transitioningDelegate = self.interactor;
    [self.navigationController presentViewController:modalController animated:YES completion:nil];
    //[self.navigationController popToViewController:modalController animated:NO];
    //[self.navigationController pushViewController:modalController animated:YES];
}
// End Slide Menu

-(void)myHomeAction:(UIBarButtonItem *)sender{
    
    appdelegate.nextPage = @"";
    
    if ([appdelegate.listOfVerMenu count]>0) {
        if (appdelegate.homeclicked==NO) {
            appdelegate.homeclicked = YES;
            [self showModalButtonWasTouched];
        } else {
            appdelegate.homeclicked = NO;
            [modalController dismissViewControllerAnimated:YES completion:nil];
        }
    } else {
        //AppDelegate *appdelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
        //NSLog(@"View Controllers : %@",[appdelegate.navController viewControllers]);
        NSMutableArray *navigationArray = [[NSMutableArray alloc] initWithArray: appdelegate.navController.viewControllers];
        
        for (UIViewController* viewController in appdelegate.navController.viewControllers) {
            
            //This if condition checks whether the viewController's class is MyGroupViewController
            // if true that means its the MyGroupViewController (which has been pushed at some point)
            if ([viewController isKindOfClass:[RootViewController class]] ) {
                
                // Here viewController is a reference of UIViewController base class of MyGroupViewController
                // but viewController holds MyGroupViewController  object so we can type cast it here
                //ThemeViewController *themeViewController = (ThemeViewController*)viewController;
                //[self.navigationController popToViewController:themeViewController animated:YES];
                [navigationArray removeObjectAtIndex: 0];
            }
        }
        appdelegate.navController.viewControllers = navigationArray;
        for (UIViewController* viewController in appdelegate.navController.viewControllers) {
            
            //This if condition checks whether the viewController's class is MyGroupViewController
            // if true that means its the MyGroupViewController (which has been pushed at some point)
            if ([viewController isKindOfClass:[ViewController class]] ) {
                
                // Here viewController is a reference of UIViewController base class of MyGroupViewController
                // but viewController holds MyGroupViewController  object so we can type cast it here
                ViewController *themeViewController = (ViewController*)viewController;
                [appdelegate.navController popToViewController:themeViewController animated:NO];
                //[self.navigationController popToViewController:viewController animated:YES];
            }
        }
        //NSLog(@"View Controllers111 : %@",appdelegate.navController.viewControllers);
    }
    
}

- (void) processdata {
    //NSLog(@"processdata");
    
    NSDictionary *dictionary;
    NSString *bgurl=@"";
    NSString *bgcolor=@"";
    if([listOfSettings count]>0)
    {
        dictionary = [listOfSettings objectAtIndex:0];
        
        
        bgurl = [dictionary objectForKey:@"bg_img"];
        bgcolor = [dictionary objectForKey:@"bgcolor"];
        
        if ([bgurl isEqual:[NSNull null]]) {
            bgurl = @"";
        }
        if ([bgcolor isEqual:[NSNull null]]) {
            bgcolor = @"";
        }
        
        if ([bgcolor length]>0) {
            self.view.backgroundColor = [self colorWithHexString:bgcolor];
        }
        //NSLog(@"bgurl : %@",bgurl);
        if ([bgurl length]>0) {
            NSURL *imageURL = [NSURL URLWithString:bgurl];
            NSData *imageData = [NSData dataWithContentsOfURL:imageURL];
            //self.backgroundColor = [UIColor redColor];
            UIGraphicsBeginImageContext(self.view.frame.size);
            CGRect rect=CGRectMake(0,0,self.view.bounds.size.width,self.view.bounds.size.height);
            [[UIImage imageWithData:imageData] drawInRect:rect];
            UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
            UIGraphicsEndImageContext();
            self.view.backgroundColor = [UIColor colorWithPatternImage:image];
        }
        
        bgurl = nil;
        bgcolor = nil;
    }
    
    [MenutableView removeFromSuperview];
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone) {
        // For iPhone
        if ([listOfContents count]*50 > self.view.frame.size.height-10) {
            MenutableView = [[UITableView alloc] initWithFrame:CGRectMake(5, 5, self.view.frame.size.width-10, self.view.frame.size.height-10)];
        } else {
            MenutableView = [[UITableView alloc] initWithFrame:CGRectMake(5, 5, self.view.frame.size.width-10, [listOfContents count]*50)];
        }
    } else {
         if ([listOfContents count]*70 > self.view.frame.size.height-10) {
         MenutableView = [[UITableView alloc] initWithFrame:CGRectMake(5, 5, self.view.frame.size.width-10, self.view.frame.size.height-10)];
         } else {
         MenutableView = [[UITableView alloc] initWithFrame:CGRectMake(5, 5, self.view.frame.size.width-10, [listOfContents count]*70)];
         }
    }
    MenutableView.autoresizingMask = UIViewAutoresizingFlexibleHeight | UIViewAutoresizingFlexibleWidth;
    MenutableView.delegate = self;
    MenutableView.dataSource = self;
    
    [self.view addSubview:MenutableView];
    
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
	//NSLog(@">>> Entering *** %s <<<", __PRETTY_FUNCTION__);
    
    //NSLog(@"tableView %@", tableView);
    
    if (tableView == MenutableView)
    {
        //NSLog(@"listOfContents count");
        return [listOfContents count];
        
    }
    
    return 0;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (tableView == MenutableView) {
        if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone) {
            return 50;
        } else {
            return 70;
        }
    }
    
	return 44;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (tableView == MenutableView)
    {
        //NSLog(@"cellForRowAtIndexPath");
        NSString *cellIdentifier = [NSString stringWithFormat:@"cell%ld",(long)indexPath.row];
        UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
        
        UIButton *btndate;
        const NSInteger TAG1 = 1000;
        UIButton *btndateEnd;
        const NSInteger TAG4 = 1003;
        UIButton *btnevent;
        const NSInteger TAG2 = 1001;
        UIButton *btngps;
        const NSInteger TAG3 = 1002;
        
        NSDictionary *dictionary;
        if([listOfContents count]>0)
        {
            dictionary = [listOfContents objectAtIndex:indexPath.row];
            
        }
        
        if(!cell) {
            cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier];
            //cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
            
            
            btndate = [UIButton buttonWithType:UIButtonTypeCustom];
            btndate = [[UIButton alloc] init];
            btndate.tag = TAG1;
            
            btndateEnd = [UIButton buttonWithType:UIButtonTypeCustom];
            btndateEnd = [[UIButton alloc] init];
            btndateEnd.tag = TAG4;
            
            btnevent = [UIButton buttonWithType:UIButtonTypeCustom];
            btnevent = [[UIButton alloc] init];
            btnevent.tag = TAG2;
            
            btngps = [UIButton buttonWithType:UIButtonTypeCustom];
            btngps = [[UIButton alloc] init];
            btngps.tag = TAG3;
            
            //if ([list_template isEqualToString:@"6"]) {
            [cell.contentView setBackgroundColor:[UIColor whiteColor]];
            
            if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone) {
                    // For iPhone
                [btndate setFrame:CGRectMake(5, 13, self.view.frame.size.width/5*2-5, 24)];
            } else {
                [btndate setFrame: CGRectMake(5, 18, self.view.frame.size.width/5*2-5, 34)];
            }
            
            /*button.imageEdgeInsets = UIEdgeInsetsMake(0, 10, 0, 0);
            [button setTitle:@"Hello" forState:UIControlStateNormal];
            button.titleEdgeInsets = UIEdgeInsetsMake(0, 20, 0, 0);
            button.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft; */
            

            [btndate setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
            [btndate setBackgroundColor:[UIColor clearColor]];
            btndate.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
            [btndate.titleLabel setLineBreakMode:NSLineBreakByTruncatingTail];
            btndate.titleLabel.font = [UIFont systemFontOfSize:14];
            [cell.contentView addSubview:btndate];
            
            /*if ([[dictionary objectForKey:@"date_end"] length]==10) {
                if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone) {
                    // For iPhone
                    [btndate setFrame:CGRectMake(5, 2, self.view.frame.size.width/5*2-5-20, 20)];
                    [btndateEnd setFrame:CGRectMake(5, 24, self.view.frame.size.width/5*2-5, 20)];
                } else {
                    [btndate setFrame: CGRectMake(5, 2, self.view.frame.size.width/5*2-5-20, 30)];
                    [btndateEnd setFrame: CGRectMake(5, 34, self.view.frame.size.width/5*2-5, 30)];
                }
            }
            [btndateEnd setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
            [btndateEnd setBackgroundColor:[UIColor clearColor]];
            btndateEnd.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
            [btndateEnd.titleLabel setLineBreakMode:NSLineBreakByTruncatingTail];
            [cell.contentView addSubview:btndateEnd]; */
            
            if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone) {
                // For iPhone
                [btnevent setFrame:CGRectMake(self.view.frame.size.width/5*2-20, 13, self.view.frame.size.width/5*2+20, 24)];
            } else {
                [btnevent setFrame: CGRectMake(self.view.frame.size.width/5*2-20, 18, self.view.frame.size.width/5*2+20, 34)];
            }
            
            [btnevent setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
            [btnevent setBackgroundColor:[UIColor clearColor]];
            btnevent.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
            [btnevent.titleLabel setLineBreakMode:NSLineBreakByTruncatingTail];
            btnevent.titleLabel.font = [UIFont systemFontOfSize:14];
            [btnevent addTarget:self action:@selector(checkEventTapped:event:) forControlEvents:UIControlEventTouchUpInside];
            [cell.contentView addSubview:btnevent];
            
            if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone) {
                // For iPhone
                [btngps setFrame:CGRectMake(self.view.frame.size.width/5*4+(self.view.frame.size.width/5)/2-15, 7, 36, 36)];
            } else {
                [btngps setFrame: CGRectMake(self.view.frame.size.width/5*4+(self.view.frame.size.width/5)/2-15, 17, 36, 36)];
            }
            
            //[btngps setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
            [btngps setBackgroundColor:[UIColor clearColor]];
            btngps.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
            //[btngps.titleLabel setLineBreakMode:NSLineBreakByTruncatingTail];
            [btngps setImage:[UIImage imageNamed:@"location.png"] forState:UIControlStateNormal];
            [btngps addTarget:self action:@selector(checkButtonGPSTapped:event:) forControlEvents:UIControlEventTouchUpInside];
            [cell.contentView addSubview:btngps];
            
        } else
        {
            //if ([[dictionary objectForKey:@"object_type"] isEqualToString:@"img"]) {
            btndate = (UIButton *)[cell viewWithTag:TAG1];
            btndate = (UIButton *)[cell viewWithTag:TAG4];
            //} else if ([[dictionary objectForKey:@"object_type"] isEqualToString:@"text"]) {
            btnevent = (UIButton *)[cell viewWithTag:TAG2];
            btngps = (UIButton *)[cell viewWithTag:TAG3];
            //}
        }
        
        if([listOfContents count]>0)
        {
            
            [btndate setTitle:[NSString stringWithFormat:@"%@", [dictionary objectForKey:@"date_start"]] forState:UIControlStateNormal];
            [btndateEnd setTitle:[NSString stringWithFormat:@"%@", [dictionary objectForKey:@"date_end"]] forState:UIControlStateNormal];
            [btnevent setTitle:[NSString stringWithFormat:@"%@", [dictionary objectForKey:@"event_name"]] forState:UIControlStateNormal];
            [btngps setTitle:[NSString stringWithFormat:@"%@", NSLocalizedString(@"gps", nil)] forState:UIControlStateNormal];
            
            /*NSLog(@"btndate %@", btndate.titleLabel.text);
            NSLog(@"btndateEnd %@", btndateEnd.titleLabel.text);
            NSLog(@"btnevent %@", btndate.titleLabel.text);
            NSLog(@"btngps %@", btndate.titleLabel.text); */
        }
        
        NSString *fontcolor = @"";
        NSString *alternatefontcolor = @"";
        NSString *rowbgcolor = @"";
        NSString *alternaterowbgcolor = @"";
        NSString *gps_img = @"";
        if([listOfSettings count]>0)
        {
            
            NSDictionary *dictionarySetting = [listOfSettings objectAtIndex:0];
            fontcolor = [dictionarySetting objectForKey:@"fontcolor"];
            alternatefontcolor = [dictionarySetting objectForKey:@"alternatefontcolor"];
            rowbgcolor = [dictionarySetting objectForKey:@"rowbgcolor"];
            alternaterowbgcolor = [dictionarySetting objectForKey:@"alternaterowbgcolor"];
            gps_img = [dictionarySetting objectForKey:@"gps_img"];
            
            if ([fontcolor isEqual:[NSNull null]]) {
                fontcolor = @"";
            }
            if ([alternatefontcolor isEqual:[NSNull null]]) {
                alternatefontcolor = @"";
            }
            if ([rowbgcolor isEqual:[NSNull null]]) {
                rowbgcolor = @"";
            }
            if ([alternaterowbgcolor isEqual:[NSNull null]]) {
                alternaterowbgcolor = @"";
            }
            if ([gps_img isEqual:[NSNull null]]) {
                gps_img = @"";
            }
            
            if ([rowbgcolor length]>0 || [alternaterowbgcolor length]>0) {
                UIColor *bgcontentview;
                if(indexPath.row%2 == 0){
                    if ([rowbgcolor length]>0) {
                        bgcontentview = [self colorWithHexString:rowbgcolor];
                    } else {
                        bgcontentview = [UIColor whiteColor];
                    }
                }else{
                    if ([alternaterowbgcolor length]>0) {
                        bgcontentview = [self colorWithHexString:alternaterowbgcolor];
                    } else {
                        bgcontentview = [UIColor whiteColor];
                    }
                }
                [cell.contentView setBackgroundColor:bgcontentview];
                
            }
            
            if ([fontcolor length]>0 || [alternatefontcolor length]>0) {
                if(indexPath.row%2 == 0){
                    if ([fontcolor length]>0) {
                        [btndate setTitleColor:[self colorWithHexString:fontcolor] forState:UIControlStateNormal];
                        [btnevent setTitleColor:[self colorWithHexString:fontcolor] forState:UIControlStateNormal];
                    } else {
                        [btndate setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
                        [btnevent setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
                    }
                } else {
                    if ([alternatefontcolor length]>0) {
                        [btndate setTitleColor:[self colorWithHexString:alternatefontcolor] forState:UIControlStateNormal];
                        [btnevent setTitleColor:[self colorWithHexString:alternatefontcolor] forState:UIControlStateNormal];
                    } else {
                        [btndate setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
                        [btnevent setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
                    }
                }
            }
            
            if ([gps_img length]>0) {
                NSURL *imageURL = [NSURL URLWithString:gps_img];
                NSData *imageData = [NSData dataWithContentsOfURL:imageURL];
                //self.backgroundColor = [UIColor redColor];
                UIGraphicsBeginImageContext(btngps.frame.size);
                CGRect rect=btngps.bounds;
                [[UIImage imageWithData:imageData] drawInRect:rect];
                UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
                UIGraphicsEndImageContext();
                [btngps setImage:image forState:UIControlStateNormal];
            }
        }
        
        return cell;
    }

    return nil;
}

- (void)checkEventTapped:(id)sender event:(id)event
{
    
    NSSet *touches = [event allTouches];
    UITouch *touch = [touches anyObject];
    CGPoint currentTouchPosition = [touch locationInView:MenutableView];
    NSIndexPath *indexPath = [MenutableView indexPathForRowAtPoint: currentTouchPosition];
    if (indexPath != nil)
    {
        
        NSDictionary *dictionary;
        if([listOfContents count]>0)
        {
            dictionary = [listOfContents objectAtIndex:indexPath.row];
            appdelegate.event_id = [dictionary objectForKey:@"id"];
            //NSLog(@"event_id:: %@", appdelegate.event_id);
            [self linkToPage:[dictionary objectForKey:@"page_id"]];
        }
        
    }
}

- (void)checkButtonGPSTapped:(id)sender event:(id)event
{
    NSSet *touches = [event allTouches];
    UITouch *touch = [touches anyObject];
    CGPoint currentTouchPosition = [touch locationInView:MenutableView];
    NSIndexPath *indexPath = [MenutableView indexPathForRowAtPoint: currentTouchPosition];
    if (indexPath != nil)
    {
        
        NSDictionary *dictionary;
        if([listOfContents count]>0)
        {
            dictionary = [listOfContents objectAtIndex:indexPath.row];
            
            NSString *latitude = [dictionary objectForKey:@"latitude"];
            NSString *longitude = [dictionary objectForKey:@"longitude"];
            
            if ([latitude floatValue]>0 && [longitude floatValue]>0) {
                GpsNavigateViewController *gpsNavigateViewController = [[GpsNavigateViewController alloc] init];
                gpsNavigateViewController.latitude = latitude;
                gpsNavigateViewController.longitude = longitude;
                [appdelegate.navController pushViewController:gpsNavigateViewController animated:NO];
            }
            
        }
        
    }
    
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
	if (tableView == MenutableView)
    {
       
        NSDictionary *dictionary;
        if([listOfContents count]>0)
        {
            dictionary = [listOfContents objectAtIndex:indexPath.row];
            appdelegate.event_id = [dictionary objectForKey:@"id"];
            //NSLog(@"event_id:: %@", appdelegate.event_id);
            [self linkToPage:[dictionary objectForKey:@"page_id"]];
        }
        
    }
    
}

-(void) linkToPage:(NSString *)pageId {

    ThemeViewController *themeViewController = [[ThemeViewController alloc] init];
    themeViewController.page_id = pageId;
    themeViewController.themepagenotification = @"Normal"; /*** Push Notification 27Aug2015 ***/
    [appdelegate.navController pushViewController:themeViewController animated:NO];
    appdelegate.homeclicked = NO;
}

-(void) dissmissIndicator {
    [alert dismissWithClickedButtonIndex:0 animated:NO];
}

/*** Push Notification 27Aug2015 ***/
-(void) viewDidDisappear:(BOOL)animated {
    
    appdelegate.homeclicked = NO;
    [modalController dismissViewControllerAnimated:YES completion:nil];
    
    [super viewDidDisappear:animated];
    
}
/*** Push Notification 27Aug2015 ***/

-(NSString *) urlEncode:(NSString *) theStr {

    NSString *encoded = (NSString *)CFBridgingRelease(CFURLCreateStringByAddingPercentEscapes(
                                                                                              NULL,
                                                                                              (__bridge CFStringRef) theStr,
                                                                                              NULL,
                                                                                              CFSTR("!*'();:@&=+$,/?%#[]\" "),
                                                                                              kCFStringEncodingUTF8));
    return encoded;
    //return nil;
}

#pragma mark CONNECT TO SERVER
-(void) connectToServer:(NSString *) urlPath {
    
    //[self addLoadingView];
    NSString *urlAddress =[NSString stringWithFormat:@"%@", urlPath];
    
    //NSLog(@"URL: %@", urlAddress);
    //Create a URL object.
    /*NSURL *url = [NSURL URLWithString:urlAddress];
    
    //URL Requst Object
    //NSURLRequest *requestObj = [NSURLRequest requestWithURL:url];
    NSURLRequest *requestObj = [NSURLRequest requestWithURL:url
                                                cachePolicy:NSURLRequestReloadIgnoringLocalAndRemoteCacheData
                                            timeoutInterval:30];
    
    NSURLConnection *theConnection=[[NSURLConnection alloc] initWithRequest:requestObj delegate:self];
    if (theConnection)
    {
        // Create the NSMutableData to hold the received data.
            receivedData = [NSMutableData data];
    }
    else
    {
        // Inform the user that the connection failed.
        //[self displayAlert:@"Connection Error. Please try again later."];
        [self displayAlert:NSLocalizedString(@"MSG_CONNECTION_ERROR", nil)];
        [self removeLoadingView];
    } */
    [self getJsonResponse:urlAddress success:^(NSDictionary *responseDict) {
        
        //NSLog(@"%@",responseDict);
        [self successfunction:responseDict];
        
    } failure:^(NSError *error) {
        // release the connection, and the data object
        
        [self removeLoadingView];
        [self displayAlert:NSLocalizedString(@"MSG_CONNECTION_ERROR", nil)];
    }];
}

-(void)getJsonResponse : (NSString *)urlStr success : (void (^)(NSDictionary *responseDict))success failure:(void(^)(NSError* error))failure
{
    
    
    NSURLSession * session = [NSURLSession sharedSession];
    NSURL * url = [NSURL URLWithString: urlStr];
    
    
    // Asynchronously Api is hit here
    NSURLSessionDataTask * dataTask = [session dataTaskWithURL:url completionHandler:^(NSData *data, NSURLResponse *response, NSError *error)
                                       {
                                           
                                           //NSLog(@"%@",data);
                                           NSDictionary * json;
                                           if ([data length]>0) {
                                               json  = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
                                               //NSLog(@"%@",json);
                                               success(json);
                                           } else {
                                               json = nil;
                                               dispatch_async(dispatch_get_main_queue(), ^ {
                                                   [self removeLoadingView];
                                                   [self displayAlert:NSLocalizedString(@"MSG_CONNECTION_ERROR", nil)];
                                               });
                                           }
                                           
                                           
                                       }];
    
    [dataTask resume] ; // Executed First
    
    
}

-(void) successfunction:(NSDictionary *)responseDict {
    dispatch_async(dispatch_get_main_queue(), ^{
        //NSString *jsonString = [responseDict JSONRepresentation];
        
        //[self removeLoadingView];
        
        if ([currentRequestMenu isEqualToString:@"GetSettings"]) {
            
            NSDictionary *dictionary = responseDict;
            //NSLog(@"FavoritelisDictionary %@",dictionary);
            [listOfSettings removeAllObjects];
            for(NSDictionary *row in dictionary)
            {
                dicSettings = [NSDictionary dictionaryWithObjectsAndKeys:[row objectForKey:@"bg_img"],@"bg_img", [row objectForKey:@"bgcolor"],@"bgcolor", [row objectForKey:@"fontcolor"],@"fontcolor", [row objectForKey:@"alternatefontcolor"],@"alternatefontcolor", [row objectForKey:@"rowbgcolor"],@"rowbgcolor", [row objectForKey:@"alternaterowbgcolor"],@"alternaterowbgcolor", [row objectForKey:@"gps_img"],@"gps_img",nil];
                [listOfSettings addObject:dicSettings];
            }
            //NSLog(@"listOfSettings %@",listOfSettings);
            
            NSString *urlAddress = [[NSString alloc] initWithFormat:@"%@get_guest_event.php?db=%@&userid=%@&udid=%@&cuid=%@&top=%@",appdelegate.user_apipath,appdelegate.db,appdelegate.user,appdelegate.uuid,appdelegate.customer_id,topId];
            currentRequestMenu = @"GetEvents";
            //NSLog(@"EventInviteurlAddress:: %@", urlAddress);
            [self connectToServer:urlAddress];
            
        } else if ([currentRequestMenu isEqualToString:@"GetEvents"]) {
            
            NSString *status,*desc,*eventids;
            
            NSDictionary *dictionary = responseDict;
            
            for(NSDictionary *row in dictionary)
            {
                status = [row objectForKey:@"status"];
                desc = [row objectForKey:@"desc"];
                eventids = [row objectForKey:@"event_id"];
            }
            
            if ([status isEqualToString:@"1"])
            {
                NSString *urlAddress = [[NSString alloc] initWithFormat:@"%@event_invitation.php?db=%@&userid=%@&udid=%@&cuid=%@&top=%@&eventlist=%@",appdelegate.user_apipath,appdelegate.db,appdelegate.user,appdelegate.uuid,appdelegate.customer_id,topId,eventids];
                currentRequestMenu = @"InviteList";
                //NSLog(@"EventInviteurlAddress:: %@", urlAddress);
                [self connectToServer:urlAddress];
            }
            else
                [self displayAlert:NSLocalizedString(@"no_events", nil)];
            
            
        } else if ([currentRequestMenu isEqualToString:@"InviteList"]) {
            
            NSDictionary *dictionary = responseDict;
            //NSLog(@"FavoritelisDictionary %@",dictionary);
            [listOfContents removeAllObjects];
            for(NSDictionary *row in dictionary)
            {
                dicContents = [NSDictionary dictionaryWithObjectsAndKeys:[row objectForKey:@"id"],@"id", [row objectForKey:@"event_name"],@"event_name", [row objectForKey:@"date_start"],@"date_start", [row objectForKey:@"date_end"],@"date_end", [row objectForKey:@"time_start"],@"time_start", [row objectForKey:@"time_end"],@"time_end", [row objectForKey:@"pax_limit"],@"pax_limit", [row objectForKey:@"latitude"],@"latitude", [row objectForKey:@"longitude"],@"longitude", [row objectForKey:@"cutoff"],@"cutoff", [row objectForKey:@"status"],@"status", [row objectForKey:@"schedule_date"],@"schedule_date", [row objectForKey:@"page_id"],@"page_id",nil];
                [listOfContents addObject:dicContents];
            }
            //NSLog(@"listOfContents %@",listOfContents);
            [self processdata];
            
        }
    });
    
}

/*- (void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response
{
        [receivedData setLength:0];
}

- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data
{
        [receivedData appendData:data];
}

- (void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error {
    // release the connection, and the data object

    connection =nil;
    receivedData =nil;
    
    //NSLog(@"%@",error.description);
    
    [self removeLoadingView];
    [self displayAlert:NSLocalizedString(@"MSG_CONNECTION_ERROR", nil)];
}

- (void)connectionDidFinishLoading:(NSURLConnection *)connection
{
        
    NSString *response = [[NSString alloc] initWithData:receivedData encoding:NSUTF8StringEncoding];
    
    [self removeLoadingView];
    
    if ([currentRequestMenu isEqualToString:@"GetSettings"]) {
        
        NSDictionary *dictionary = [response JSONValue];
        //NSLog(@"FavoritelisDictionary %@",dictionary);
        [listOfSettings removeAllObjects];
        for(NSDictionary *row in dictionary)
        {
            dicSettings = [NSDictionary dictionaryWithObjectsAndKeys:[row objectForKey:@"bg_img"],@"bg_img", [row objectForKey:@"bgcolor"],@"bgcolor", [row objectForKey:@"fontcolor"],@"fontcolor", [row objectForKey:@"alternatefontcolor"],@"alternatefontcolor", [row objectForKey:@"rowbgcolor"],@"rowbgcolor", [row objectForKey:@"alternaterowbgcolor"],@"alternaterowbgcolor", [row objectForKey:@"gps_img"],@"gps_img",nil];
            [listOfSettings addObject:dicSettings];
        }
        //NSLog(@"listOfSettings %@",listOfSettings);
        
        NSString *urlAddress = [[NSString alloc] initWithFormat:@"%@get_guest_event.php?db=%@&userid=%@&udid=%@&cuid=%@&top=%@",appdelegate.user_apipath,appdelegate.db,appdelegate.user,appdelegate.uuid,appdelegate.customer_id,topId];
        currentRequestMenu = @"GetEvents";
        //NSLog(@"EventInviteurlAddress:: %@", urlAddress);
        [self connectToServer:urlAddress];
        
    } else if ([currentRequestMenu isEqualToString:@"GetEvents"]) {
        
        NSString *response = [[NSString alloc] initWithData:receivedData encoding:NSUTF8StringEncoding];
        NSString *status,*desc,*eventids;
        if([receivedData length]>10)
        {
            NSDictionary *dictionary = [response JSONValue];
            
            for(NSDictionary *row in dictionary)
            {
                status = [row objectForKey:@"status"];
                desc = [row objectForKey:@"desc"];
                eventids = [row objectForKey:@"event_id"];
            }
            
            if ([status isEqualToString:@"1"])
            {
                NSString *urlAddress = [[NSString alloc] initWithFormat:@"%@event_invitation.php?db=%@&userid=%@&udid=%@&cuid=%@&top=%@&eventlist=%@",appdelegate.user_apipath,appdelegate.db,appdelegate.user,appdelegate.uuid,appdelegate.customer_id,topId,eventids];
                currentRequestMenu = @"InviteList";
                //NSLog(@"EventInviteurlAddress:: %@", urlAddress);
                [self connectToServer:urlAddress];
            }
            else
                [self displayAlert:desc];
        }
        response =nil;
        
    } else if ([currentRequestMenu isEqualToString:@"InviteList"]) {
        
        NSDictionary *dictionary = [response JSONValue];
        //NSLog(@"FavoritelisDictionary %@",dictionary);
        [listOfContents removeAllObjects];
        for(NSDictionary *row in dictionary)
        {
            dicContents = [NSDictionary dictionaryWithObjectsAndKeys:[row objectForKey:@"id"],@"id", [row objectForKey:@"event_name"],@"event_name", [row objectForKey:@"date_start"],@"date_start", [row objectForKey:@"date_end"],@"date_end", [row objectForKey:@"time_start"],@"time_start", [row objectForKey:@"time_end"],@"time_end", [row objectForKey:@"pax_limit"],@"pax_limit", [row objectForKey:@"latitude"],@"latitude", [row objectForKey:@"longitude"],@"longitude", [row objectForKey:@"cutoff"],@"cutoff", [row objectForKey:@"status"],@"status", [row objectForKey:@"schedule_date"],@"schedule_date", [row objectForKey:@"page_id"],@"page_id",nil];
            [listOfContents addObject:dicContents];
        }
        //NSLog(@"listOfContents %@",listOfContents);
        [self processdata];

    }
    
} */

-(void) addLoadingView {
    loadingView = [LoadingView loadingViewInView:appdelegate.window];
}

-(void) removeLoadingView
{
    [loadingView removeView];
}

-(void) displayAlert: (NSString *) stralert {
    NSString *str = [[NSString alloc] initWithFormat:@"%@", stralert];
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:appdelegate.iconname
                                                    message:str
                                                   delegate:self
                                          cancelButtonTitle:NSLocalizedString(@"done", nil)
                                          otherButtonTitles:nil];
    
    [alert show];
    str =nil;
    alert =nil;
}


@end
