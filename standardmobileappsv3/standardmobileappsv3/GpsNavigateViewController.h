//
//  ContentPluginDetailsViewController1.h
//  standardmobileappsv3
//
//  Created by M3Online on 5/27/14.
//  Copyright (c) 2014 M3Online. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MapKit/MapKit.h>

@interface GpsNavigateViewController : UIViewController <MKMapViewDelegate, UIWebViewDelegate, UIViewControllerTransitioningDelegate>{
    UIWebView *webView;
    
    NSString *title;
    NSString *latitude;
    NSString *longitude;
}

@property (strong, nonatomic) IBOutlet MKMapView *mapView;
@property (nonatomic, strong) UIWebView *webView;
@property (nonatomic, strong) NSString *title;
@property (nonatomic, strong) NSString *latitude;
@property (nonatomic, strong) NSString *longitude;

@end
