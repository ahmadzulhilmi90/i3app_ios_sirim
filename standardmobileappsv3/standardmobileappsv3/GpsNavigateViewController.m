//
//  ViewControllerPlugin2.m
//  standardmobileappsv3
//
//  Created by M3Online on 5/27/14.
//  Copyright (c) 2014 M3Online. All rights reserved.
//

#import "GpsNavigateViewController.h"
#import "AppDelegate.h"
#import "myAnnotation.h"
#import <CoreLocation/CoreLocation.h>
#import "RootViewController.h"
#import "ViewController.h"
#import "InitialSlidingViewController.h"
#import "BTSlideInteractor.h"

#define METERS_PER_MILE 1609.344

@interface GpsNavigateViewController ()
@property (strong, nonatomic) BTSlideInteractor *interactor;
@end

@implementation GpsNavigateViewController
UIAlertView *alert;
UIActivityIndicatorView *indicator;
UIActivityIndicatorView *activityIndicator;
CLLocationManager *locationManager;
@synthesize webView;
@synthesize title, longitude, latitude;

InitialSlidingViewController *modalController;

AppDelegate *appdelegate;

- (UIColor *) colorWithHexString:(NSString *)hexString {
    NSString *colorString = [[hexString stringByReplacingOccurrencesOfString: @"#" withString: @""] uppercaseString];
    CGFloat alpha, red, blue, green;
    alpha = 1.0f;
    red   = [self colorComponentFrom: colorString start: 0 length: 2];
    green = [self colorComponentFrom: colorString start: 2 length: 2];
    blue  = [self colorComponentFrom: colorString start: 4 length: 2];
    return [UIColor colorWithRed: red green: green blue: blue alpha: alpha];
}

- (CGFloat) colorComponentFrom:(NSString *)string start:(NSUInteger)start length:(NSUInteger)length {
    NSString *substring = [string substringWithRange: NSMakeRange(start, length)];
    NSString *fullHex = length == 2 ? substring : [NSString stringWithFormat: @"%@%@", substring, substring];
    unsigned hexComponent;
    [[NSScanner scannerWithString: fullHex] scanHexInt: &hexComponent];
    return hexComponent / 255.0;
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    appdelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    
    appdelegate.homeclicked = NO;
    
    self.title = appdelegate.iconname;
    self.view.backgroundColor = [UIColor whiteColor];
    self.navigationItem.backBarButtonItem = [[UIBarButtonItem alloc]initWithTitle:@"" style:UIBarButtonItemStylePlain target:nil action:nil];
    
    [self processdata];
    
    //1
    CLLocationCoordinate2D zoomLocation;
    zoomLocation.latitude = [latitude floatValue];
    zoomLocation.longitude= [longitude floatValue];
    // 2
    MKCoordinateRegion viewRegion = MKCoordinateRegionMakeWithDistance(zoomLocation, 0.3*METERS_PER_MILE, 0.3*METERS_PER_MILE);
    [self.mapView setRegion:viewRegion animated:NO];
    
    CGRect rect = CGRectMake(0,0,20,20);
    UIGraphicsBeginImageContext( rect.size );
    [[UIImage imageNamed:@"home.png"] drawInRect:rect];
    UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    UIBarButtonItem *myHomeButton = [[UIBarButtonItem alloc] initWithImage:image style:UIBarButtonItemStyleDone target:self action:@selector(myHomeAction:)];
    
    
    NSArray *myButtonArray = [[NSArray alloc] initWithObjects:myHomeButton, nil];
    
    self.navigationItem.rightBarButtonItems = myButtonArray;
    
    locationManager = [[CLLocationManager alloc] init];
    locationManager.distanceFilter = kCLDistanceFilterNone; // whenever we move
    locationManager.desiredAccuracy = kCLLocationAccuracyHundredMeters; // 100 m
    [locationManager startUpdatingLocation];
    
}

//Slide Menu
- (void)willAnimateRotationToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation duration:(NSTimeInterval)duration
{
    //NSLog(@"view frame=%@ bounds=%@", NSStringFromCGRect(self.view.frame), NSStringFromCGRect(self.view.bounds));
}


- (void)showModalButtonWasTouched
{
    self.interactor = [[BTSlideInteractor alloc] init];
    self.interactor.presenting = YES;
    
    //BTModalViewController *modalController = [self.storyboard instantiateViewControllerWithIdentifier:@"ModalViewController"];
    modalController = [[InitialSlidingViewController alloc] init];
    modalController.modalPresentationStyle = UIModalPresentationCustom;
    modalController.transitioningDelegate = self.interactor;
    [self.navigationController presentViewController:modalController animated:YES completion:nil];
    //[self.navigationController popToViewController:modalController animated:NO];
    //[self.navigationController pushViewController:modalController animated:YES];
}
// End Slide Menu

-(void)myHomeAction:(UIBarButtonItem *)sender{
    
    appdelegate.nextPage = @"";
    
    if ([appdelegate.listOfVerMenu count]>0) {
        if (appdelegate.homeclicked==NO) {
            appdelegate.homeclicked = YES;
            [self showModalButtonWasTouched];
        } else {
            appdelegate.homeclicked = NO;
            [modalController dismissViewControllerAnimated:YES completion:nil];
        }
    } else {
        //AppDelegate *appdelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
        //NSLog(@"View Controllers : %@",[appdelegate.navController viewControllers]);
        NSMutableArray *navigationArray = [[NSMutableArray alloc] initWithArray: appdelegate.navController.viewControllers];
        
        for (UIViewController* viewController in appdelegate.navController.viewControllers) {
            
            //This if condition checks whether the viewController's class is MyGroupViewController
            // if true that means its the MyGroupViewController (which has been pushed at some point)
            if ([viewController isKindOfClass:[RootViewController class]] ) {
                
                // Here viewController is a reference of UIViewController base class of MyGroupViewController
                // but viewController holds MyGroupViewController  object so we can type cast it here
                //ThemeViewController *themeViewController = (ThemeViewController*)viewController;
                //[self.navigationController popToViewController:themeViewController animated:YES];
                [navigationArray removeObjectAtIndex: 0];
            }
        }
        appdelegate.navController.viewControllers = navigationArray;
        for (UIViewController* viewController in appdelegate.navController.viewControllers) {
            
            //This if condition checks whether the viewController's class is MyGroupViewController
            // if true that means its the MyGroupViewController (which has been pushed at some point)
            if ([viewController isKindOfClass:[ViewController class]] ) {
                
                // Here viewController is a reference of UIViewController base class of MyGroupViewController
                // but viewController holds MyGroupViewController  object so we can type cast it here
                ViewController *themeViewController = (ViewController*)viewController;
                [appdelegate.navController popToViewController:themeViewController animated:NO];
                //[self.navigationController popToViewController:viewController animated:YES];
            }
        }
        //NSLog(@"View Controllers111 : %@",appdelegate.navController.viewControllers);
    }
    
}

/*- (NSString *)deviceLocation {
    return [NSString stringWithFormat:@"latitude: %f longitude: %f", locationManager.location.coordinate.latitude, locationManager.location.coordinate.longitude];
} */

- (void)viewDidAppear:(BOOL)animated {
    
}

- (void) processdata {
    //AppDelegate *appdelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    //self.title = appdelegate.email;
    
    self.mapView = [[MKMapView alloc] initWithFrame:CGRectMake(0,0,self.view.frame.size.width,self.view.frame.size.height-64)];
    [self.view addSubview:self.mapView];
    self.mapView.delegate = self;
    //5
    //NSLog(@"cont_value1 :: %@", cont_value1);
    CLLocationCoordinate2D coordinate1;
    coordinate1.latitude = [latitude floatValue];
    coordinate1.longitude = [longitude floatValue];
    myAnnotation *annotation = [[myAnnotation alloc] initWithCoordinate:coordinate1 title:@""];
    [self.mapView addAnnotation:annotation];
    //[self.view addSubview:self.mapView];
    
    //Top Bar
    __strong UIView* view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, 70)];
    [view setBackgroundColor:[UIColor whiteColor]];
    
    CLLocation *location1 = [[CLLocation alloc] initWithLatitude:[latitude floatValue] longitude:[longitude floatValue]];
    CLLocation *location2 = [[CLLocation alloc] initWithLatitude:locationManager.location.coordinate.latitude longitude:locationManager.location.coordinate.longitude];
    //NSLog(@"Distance i meters: %f", [location1 distanceFromLocation:location2]);
    
    //[self.view addSubview: view];
    
    //Bottom Bar
    __strong UIView* viewBot = [[UIView alloc] initWithFrame:CGRectMake(0, self.view.frame.size.height-70-64, self.view.frame.size.width, 70)];
    [viewBot setBackgroundColor: [UIColor whiteColor]];
    
    UIButton *buttonBot = [UIButton buttonWithType:UIButtonTypeRoundedRect];
    buttonBot.frame = CGRectMake(self.view.frame.size.width-70, 15.0, 60.0, 40.0);
    [buttonBot setTitle:NSLocalizedString(@"go", nil) forState:UIControlStateNormal];
    [buttonBot setTitleColor: [UIColor colorWithRed:114/255.0f green:114/255.0f blue:114/255.0f alpha:1.0f] forState: UIControlStateNormal];
    //[button setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    [buttonBot setBackgroundColor:[UIColor colorWithRed:229.0f/255.0f green:229.0f/255.0f blue:229.0f/255.0f alpha:1.0f]];
    //[synbutton setImage:[UIImage imageNamed:@"sync.png"] forState:UIControlStateNormal];
    buttonBot.tag = 3000;
    [buttonBot addTarget:self
               action:@selector(aMethod:)
     forControlEvents:UIControlEventTouchUpInside];
    buttonBot.layer.cornerRadius = 5;
    [viewBot addSubview: buttonBot];
    
    UIButton *buttonImgBot = [UIButton buttonWithType:UIButtonTypeRoundedRect];
    buttonImgBot.frame = CGRectMake(self.view.frame.size.width-100, 15.0, 40.0, 40.0);
    buttonImgBot.backgroundColor = [UIColor colorWithRed:229.0f/255.0f green:229.0f/255.0f blue:229.0f/255.0f alpha:1.0f];
    [buttonImgBot setBackgroundImage:[UIImage imageNamed:@"direction.png"] forState:UIControlStateNormal];
    buttonImgBot.tag = 3001;
    [buttonImgBot addTarget:self action:@selector(aMethod:) forControlEvents:UIControlEventTouchUpInside];
    buttonImgBot.layer.cornerRadius = 5;
    [viewBot addSubview:buttonImgBot];
    
    __strong NSString *strdistance;
    //if (locationManager.location.coordinate.latitude > 0 && locationManager.location.coordinate.longitude > 0 && [cont_value1 floatValue] > 0 && [cont_value2 floatValue] > 0) {
    if (locationManager.location.coordinate.latitude > 0 && locationManager.location.coordinate.longitude > 0) {
        float fdistance = [location1 distanceFromLocation:location2];
        fdistance = fdistance / 1000;
        if (fdistance > 9) {
            NSNumberFormatter* numberFormatter = [[NSNumberFormatter alloc] init];
            [numberFormatter setFormatterBehavior: NSNumberFormatterBehavior10_4];
            [numberFormatter setNumberStyle: NSNumberFormatterDecimalStyle];
            NSString *numberString = [numberFormatter stringFromNumber: [NSNumber numberWithInteger: fdistance]];
            strdistance = [NSString stringWithFormat:@"%@ %@", numberString, NSLocalizedString(@"km", nil)];
        } else if (fdistance > 1) {
            strdistance = [NSString stringWithFormat:@"%.1f %@", fdistance, NSLocalizedString(@"km", nil)];
        } else if (fdistance >= 0) {
            strdistance = [NSString stringWithFormat:@"%.2f", fdistance];
            float f = [strdistance floatValue] * 1000;
            strdistance = [NSString stringWithFormat:@"%.0f %@", f, NSLocalizedString(@"m", nil)];
        }
        strdistance = [NSString stringWithFormat:@"%@ %@", strdistance, NSLocalizedString(@"away", nil)];
    } else {
        strdistance = @"";
    }
    UILabel *label_dis = [[UILabel alloc] initWithFrame:CGRectMake(10,25,self.view.frame.size.width-110,25)];
    [label_dis setNumberOfLines:0];
    label_dis.adjustsFontSizeToFitWidth = YES;
    [label_dis setText:strdistance];
    label_dis.autoresizingMask = UIViewAutoresizingFlexibleHeight | UIViewAutoresizingFlexibleTopMargin | UIViewAutoresizingFlexibleBottomMargin;
    label_dis.textColor = [UIColor colorWithRed:114/255.0f green:114/255.0f blue:114/255.0f alpha:1.0f];
    label_dis.backgroundColor = [UIColor clearColor];
    UIFont *yourFont2 = [UIFont fontWithName:@"Helvetica" size:16];
    label_dis.font = yourFont2;
    [viewBot addSubview: label_dis];
    
    [self.view addSubview: viewBot];
}

- (void)aMethod:(NSString *)sender {
    UIButton *clicked = (UIButton *) sender;
    
    if (clicked.tag == 3000 || clicked.tag == 3001) {

        CLLocationCoordinate2D rdOfficeLocation = CLLocationCoordinate2DMake([appdelegate.latitude floatValue],[appdelegate.longitude floatValue]);
        //Apple Maps, using the MKMapItem class
        MKPlacemark *placemark = [[MKPlacemark alloc] initWithCoordinate:rdOfficeLocation addressDictionary:nil];
        MKMapItem *item = [[MKMapItem alloc] initWithPlacemark:placemark];
        item.name = title;
        [item openInMapsWithLaunchOptions:nil];
    }
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
}

- (void) viewWillDisappear:(BOOL)animated {
    title =nil;
    latitude =nil;
    longitude =nil;
}

#pragma mark -MapView Delegate Methods
//6
- (MKAnnotationView *)mapView:(MKMapView *)mapView viewForAnnotation:(id<MKAnnotation>)annotation {
    //7
    if([annotation isKindOfClass:[MKUserLocation class]])
        return nil;
    
    //8
    static NSString *identifier = @"myAnnotation";
    MKPinAnnotationView * annotationView = (MKPinAnnotationView*)[self.mapView dequeueReusableAnnotationViewWithIdentifier:identifier];
    if (!annotationView)
    {
        //9
        annotationView = [[MKPinAnnotationView alloc] initWithAnnotation:annotation reuseIdentifier:identifier];
        annotationView.pinColor = MKPinAnnotationColorPurple;
        annotationView.animatesDrop = YES;
        annotationView.canShowCallout = YES;
    }else {
        annotationView.annotation = annotation;
    }
    annotationView.rightCalloutAccessoryView = [UIButton buttonWithType:UIButtonTypeDetailDisclosure];
    return annotationView;
}

-(void) viewDidDisappear:(BOOL)animated {
    
    [super viewDidDisappear:animated];
    
    //[self.viewWeb loadHTMLString:@"<html></html>" baseURL:nil];
    //[self.viewWeb loadHTMLString:@"" baseURL:nil];
    [activityIndicator removeFromSuperview];
    [[NSURLCache sharedURLCache] removeAllCachedResponses];
    
    appdelegate.homeclicked = NO;
    [modalController dismissViewControllerAnimated:YES completion:nil];
    
    //[self dismissViewControllerAnimated:YES completion:NULL];
    
}

- (void)webViewDidFinishLoad:(UIWebView *)webView
{
    //NSLog(@"webViewDidFinishLoad");
    [activityIndicator stopAnimating];
    //webView =nil;
    
}

//No Internet Connection error code
-(void)webView:(UIWebView *)webView didFailLoadWithError:(NSError *)error {
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"error", nil) message:NSLocalizedString(@"MSG_CONNECTION_ERROR", nil) delegate:self cancelButtonTitle:NSLocalizedString(@"close", nil) otherButtonTitles:nil, nil];
    [alert show];
    [activityIndicator stopAnimating];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
