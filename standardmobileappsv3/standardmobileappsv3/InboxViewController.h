//
//  InboxViewController.h
//  standardmobileappsv3
//
//  Created by M3Online on 9/4/14.
//  Copyright (c) 2014 M3Online. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "LoadingView.h"

@interface InboxViewController : UIViewController <UITextViewDelegate, UIImagePickerControllerDelegate, UINavigationControllerDelegate, UIViewControllerTransitioningDelegate>{
    CGFloat animatedDistance;
    NSString *contId;
    NSString *conttitle;
    NSString *topId;
    
    //NSMutableData* receivedData;
    NSString *currentRequest;
    NSMutableArray *listOfContent;
    NSDictionary *dicContent;
    UIScrollView *scrollview;
    
    LoadingView *loadingView;
    
    UIImageView * imageViewPhoto;
    UIButton * choosePhotoBtn;
    UIButton * takePhotoBtn;
    UIButton * buttonsubmit;
    
    int heightPhotoView;
    int height;
}

@property (nonatomic, strong) NSString *contId;
@property (nonatomic, strong) NSString *conttitle;

//@property (nonatomic, strong) NSMutableData *receivedData;
@property (nonatomic, strong) NSString *currentRequest;
@property (nonatomic, strong) NSMutableArray *listOfContent;
@property (nonatomic, strong) UIScrollView *scrollview;
@property (nonatomic, strong) NSString *topId;

@property (nonatomic, retain) IBOutlet UIImageView * imageViewPhoto;
@property (nonatomic, retain) IBOutlet UIButton * choosePhotoBtn;
@property (nonatomic, retain) IBOutlet UIButton * takePhotoBtn;
@property (nonatomic, retain) IBOutlet UIButton * buttonsubmit;

@property (nonatomic, assign) int heightPhotoView;
@property (nonatomic, assign) int height;

-(IBAction) getPhoto:(id) sender;

@end
