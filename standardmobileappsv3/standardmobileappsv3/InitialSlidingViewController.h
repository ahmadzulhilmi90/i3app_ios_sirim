//
//  InitialSlidingViewController.h
//  SlideToDo
//
//  Created by Brandon King on 4/18/13.
//  Copyright (c) 2013 King's Cocoa. All rights reserved.
//
#import <UIKit/UIKit.h>
#import <MessageUI/MessageUI.h>
#import <SDWebImage/UIButton+WebCache.h>
#import <SDWebImage/UIImageView+WebCache.h>

@interface InitialSlidingViewController : UIViewController<UITableViewDataSource, UITableViewDelegate, MFMailComposeViewControllerDelegate, UIViewControllerTransitioningDelegate> {

    IBOutlet UITableView *GridVertableView;
    NSMutableArray *listOfVerMenu;
    NSDictionary *dicVerMenu;
}

@property (nonatomic, strong) UITableView *GridVertableView;
@property (nonatomic, strong) NSMutableArray *listOfVerMenu;

@end
