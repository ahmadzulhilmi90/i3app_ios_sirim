//
//  InitialSlidingViewController.m
//  SlideToDo
//
//  Created by Brandon King on 4/18/13.
//  Copyright (c) 2013 King's Cocoa. All rights reserved.
//

#import "InitialSlidingViewController.h"
#import "AppDelegate.h"
#import "NSString+HTML.h"
#import "ContentPluginViewController.h"
#import "ThemeViewController.h"
#import "BasicPluginViewController.h"
#import "ContentPluginViewController.h"
#import "ContentPluginDetailsViewController.h"
#import "LoyaltyViewController.h"
#import "WarrantyViewController.h"
#import "ContentSearchViewController.h"
#import "SettingViewController.h"
#import "BookingListViewController.h"
#import "ScorePointViewController.h"
#import "ContentCatPluginViewController.h"
#import "InboxViewController.h"
#import "ContentProductCatPluginViewController.h"
#import "NotesViewController.h"
#import "SIRIMLabelViewController.h"
#import "SIRIMPCIViewController.h"

@interface InitialSlidingViewController ()
@end


@implementation InitialSlidingViewController
@synthesize GridVertableView, listOfVerMenu;

AppDelegate *appdelegate;
UIAlertView *alert;

__strong NSString *aplugintype;
__strong NSString *apluginvalue1;
__strong NSString *apluginvalue2;
__strong NSString *apluginvalue3;
__strong NSString *aid;

- (UIColor *) colorWithHexString:(NSString *)hexString {
    NSString *colorString = [[hexString stringByReplacingOccurrencesOfString: @"#" withString: @""] uppercaseString];
    CGFloat alpha, red, blue, green;
    alpha = 1.0f;
    red   = [self colorComponentFrom: colorString start: 0 length: 2];
    green = [self colorComponentFrom: colorString start: 2 length: 2];
    blue  = [self colorComponentFrom: colorString start: 4 length: 2];
    return [UIColor colorWithRed: red green: green blue: blue alpha: alpha];
}

- (CGFloat) colorComponentFrom:(NSString *)string start:(NSUInteger)start length:(NSUInteger)length {
    NSString *substring = [string substringWithRange: NSMakeRange(start, length)];
    NSString *fullHex = length == 2 ? substring : [NSString stringWithFormat: @"%@%@", substring, substring];
    unsigned hexComponent;
    [[NSScanner scannerWithString: fullHex] scanHexInt: &hexComponent];
    return hexComponent / 255.0;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	//self.topViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"ToDoView"];
    [self.view setBackgroundColor:[UIColor clearColor]];
    
    appdelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    
    sqlite3 *database;
    
    if(sqlite3_open([appdelegate.databasePath UTF8String], &database) == SQLITE_OK) {
    
        NSString *vermenu_object = [[NSString alloc] initWithFormat:@"SELECT * from theme_object WHERE theme_code = 'MENU-VER' and object_type<>'0' Order by slot_code"];
        //NSLog(@"menu_object:: %@", menu_object);
        const char *sqlStatementVerMenu = [vermenu_object UTF8String];
        sqlite3_stmt *compiledStatementVerMenu;
        if(sqlite3_prepare_v2(database, sqlStatementVerMenu, -1, &compiledStatementVerMenu, NULL) == SQLITE_OK) {
            
            listOfVerMenu = [[NSMutableArray alloc] init];
            while(sqlite3_step(compiledStatementVerMenu) == SQLITE_ROW) {
                
                NSString *aidobj = [NSString stringWithUTF8String:(char *)sqlite3_column_text(compiledStatementVerMenu, 0)];
                NSString *athemecode = [NSString stringWithUTF8String:(char *)sqlite3_column_text(compiledStatementVerMenu, 1)];
                NSString *aslotcode = [NSString stringWithUTF8String:(char *)sqlite3_column_text(compiledStatementVerMenu, 2)];
                NSString *apageid = [NSString stringWithUTF8String:(char *)sqlite3_column_text(compiledStatementVerMenu, 3)];
                NSString *aobjcttype = [NSString stringWithUTF8String:(char *)sqlite3_column_text(compiledStatementVerMenu, 4)];
                NSString *aobjectvalue = [NSString stringWithUTF8String:(char *)sqlite3_column_text(compiledStatementVerMenu, 5)];
                NSString *aattribute = [NSString stringWithUTF8String:(char *)sqlite3_column_text(compiledStatementVerMenu, 6)];
                NSString *aactiontype = [NSString stringWithUTF8String:(char *)sqlite3_column_text(compiledStatementVerMenu, 7)];
                NSString *aactionvalue = [NSString stringWithUTF8String:(char *)sqlite3_column_text(compiledStatementVerMenu, 8)];
                NSString *aremark = [NSString stringWithUTF8String:(char *)sqlite3_column_text(compiledStatementVerMenu, 10)];
                
                aobjectvalue = [aobjectvalue stringByDecodingHTMLEntities];
                aremark = [aremark stringByDecodingHTMLEntities];
                aobjectvalue = [aobjectvalue stringByReplacingOccurrencesOfString:@"''" withString:@"'"];
                aremark = [aremark stringByReplacingOccurrencesOfString:@"''" withString:@"'"];
                
                dicVerMenu = [NSDictionary dictionaryWithObjectsAndKeys:aidobj,@"id",athemecode,@"theme_code",aslotcode,@"slot_code",apageid,@"page_id",aobjcttype,@"object_type",aobjectvalue,@"object_value",aattribute,@"attribute",aactiontype,@"action_type",aactionvalue,@"action_value",aremark,@"remark",nil];
                [listOfVerMenu addObject:dicVerMenu];
                
                aidobj =nil;
                athemecode =nil;
                aslotcode =nil;
                apageid =nil;
                aobjcttype =nil;
                aobjectvalue =nil;
                aattribute =nil;
                aactiontype =nil;
                aactionvalue =nil;
                aremark =nil;
                
            }
            
            //NSLog(@"listOfMenu:: %@", listOfMenu);
        }
        // Release the compiled statement from memory
        sqlite3_finalize(compiledStatementVerMenu);
        
    }
    sqlite3_close(database);
    
    float yheight;
#if TARGET_IPHONE_SIMULATOR
    yheight = 0;
#else
    yheight = 0;
#endif
    
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone) {
        // For iPhone
        if ([listOfVerMenu count]*50 > self.view.frame.size.height-yheight) {
            GridVertableView = [[UITableView alloc] initWithFrame:CGRectMake(0, yheight, 250, self.view.frame.size.height-yheight)];
        } else {
            GridVertableView = [[UITableView alloc] initWithFrame:CGRectMake(0, yheight, 250, [listOfVerMenu count]*50)];
        }
    } else {
        if ([listOfVerMenu count]*100 > self.view.frame.size.height-yheight) {
            GridVertableView = [[UITableView alloc] initWithFrame:CGRectMake(0, yheight, 500, self.view.frame.size.height-yheight)];
        } else {
            GridVertableView = [[UITableView alloc] initWithFrame:CGRectMake(0, yheight, 500, [listOfVerMenu count]*100)];
        }
    }
    //GridVertableView.autoresizingMask = UIViewAutoresizingFlexibleHeight | UIViewAutoresizingFlexibleWidth;
    GridVertableView.delegate = self;
    GridVertableView.dataSource = self;
    GridVertableView.bounces = NO;
    
    [GridVertableView setBackgroundColor:[UIColor clearColor]];
    [self.view addSubview:GridVertableView];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    
    if (tableView == GridVertableView) {
        return [listOfVerMenu count];
    }
    
    return 0;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    if (tableView == GridVertableView) {
        if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone) {
            // For iPhone
            return 50;
        } else {
            return 100;
        }
    }
    
    return 44;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    //NSLog(@"cellForRowAtIndexPath");
    if (tableView == GridVertableView)
    {
        static NSString *CellIdentifier = @"CellIdentifier";
        UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
        
        UIImageView *imgViewMenu;
        const NSInteger RIGHT_TAG = 1001;
        UILabel *menutext;
        const NSInteger LEFT_TAG = 1000;
        
        if (cell == nil)
        {
            
            cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
            //cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
            
            //if ([[dictionary objectForKey:@"object_type"] isEqualToString:@"img"]) {
            
            if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone) {
                // For iPhone
                imgViewMenu = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 250, 50)];
            } else {
                imgViewMenu = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 500, 100)];
            }
            imgViewMenu.tag = LEFT_TAG;
            [cell.contentView addSubview:imgViewMenu];
            
            //} else if ([[dictionary objectForKey:@"object_type"] isEqualToString:@"text"]) {
            
            if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone) {
                // For iPhone
                menutext = [[UILabel alloc] initWithFrame: CGRectMake(0, 0, 250, 50)];
            } else {
                menutext = [[UILabel alloc] initWithFrame: CGRectMake(0, 0, 500, 100)];
            }
            menutext.tag = RIGHT_TAG;
            //[menutext setFont:[UIFont boldSystemFontOfSize:16]];
            //menutext.textColor = [UIColor blackColor];
            //menutext.textAlignment = NSTextAlignmentLeft;
            //menutext.backgroundColor = [UIColor clearColor];
            menutext.numberOfLines = 0;
            [cell.contentView addSubview:menutext];
            //}
            
        }
        else
        {
            //if ([[dictionary objectForKey:@"object_type"] isEqualToString:@"img"]) {
            imgViewMenu = (UIImageView *)[cell viewWithTag:LEFT_TAG];
            //} else if ([[dictionary objectForKey:@"object_type"] isEqualToString:@"text"]) {
            menutext = (UILabel *)[cell viewWithTag:RIGHT_TAG];
            //}
        }
        
        if([listOfVerMenu count]>0)
        {
            NSDictionary *dictionary = [listOfVerMenu objectAtIndex:indexPath.row];
            
            // NSString *myImage = [NSString stringWithFormat:@"%@%@", appdelegate.themecolor, [arr objectForKey:@"img"]];
            // UIImage *iconproduct = [UIImage imageNamed:imgURL];
            
            NSArray *myArray = [[dictionary objectForKey:@"attribute"] componentsSeparatedByCharactersInSet:[NSCharacterSet characterSetWithCharactersInString:@"|"]];
            //NSLog(@"myArray: %@", myArray);
            
            NSString *imgURL = [myArray objectAtIndex:4];
            
            NSRange end = [imgURL rangeOfString:@"/" options:NSBackwardsSearch];
            NSString *shortString;
            if (end.location != NSNotFound) {
                shortString =[imgURL substringWithRange:NSMakeRange(end.location+1, imgURL.length-(end.location+1))];
            } else {
                shortString = imgURL;
            }
            
            menutext.textColor = [self colorWithHexString:[myArray objectAtIndex:1]];
            if ([[myArray objectAtIndex:0] length]==6) {
                [cell setBackgroundColor:[self colorWithHexString:[myArray objectAtIndex:0]]];
            } else {
                [cell setBackgroundColor:[UIColor whiteColor]];
            }
            menutext.lineBreakMode = NSLineBreakByWordWrapping;
            float sizefont;
            if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone) {
                // For iPhone
                sizefont = [[myArray objectAtIndex:2] floatValue];
            } else {
                sizefont = [[myArray objectAtIndex:2] floatValue] + 10.0f;
            }
            [menutext setFont:[UIFont systemFontOfSize:sizefont]];
            //[menutext setFont:[UIFont fontWithName:@"Helvetica" size:[[myArray objectAtIndex:2] floatValue]]];
            if ([[myArray objectAtIndex:3] isEqualToString:@"center"]) {
                //[buttonName.titleLabel setTextAlignment: NSTextAlignmentCenter];
                menutext.textAlignment = NSTextAlignmentCenter;
            } else if ([[myArray objectAtIndex:3] isEqualToString:@"left"]) {
                //[buttonName.titleLabel setTextAlignment: NSTextAlignmentLeft];
                menutext.textAlignment = NSTextAlignmentLeft;
            } else if ([[myArray objectAtIndex:3] isEqualToString:@"right"]) {
                //[buttonName.titleLabel setTextAlignment: NSTextAlignmentRight];
                menutext.textAlignment = NSTextAlignmentRight;
            }
            menutext.text = [NSString stringWithFormat:@"%@", [dictionary objectForKey:@"object_value"]];
            //buttonName.frame = CGRectMake(self.frame.size.width/3*(index-3),  35, self.frame.size.width/3, 35);
            
            if ([[myArray objectAtIndex:4] length]>5 && [[dictionary objectForKey:@"object_value"] length]>0) {
                if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone) {
                    // For iPhone
                    [imgViewMenu setFrame:CGRectMake(0, 0, 50, 50)];
                    [menutext setFrame:CGRectMake(60, 0, 250-60, 50)];
                } else {
                    [imgViewMenu setFrame:CGRectMake(0, 0, 100, 100)];
                    [menutext setFrame:CGRectMake(110, 0, 500-110, 100)];
                }
            } else if ([[myArray objectAtIndex:4] length]<=5) {
                if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone) {
                    // For iPhone
                    [menutext setFrame:CGRectMake(0, 0, 250, 50)];
                } else {
                    [menutext setFrame:CGRectMake(0, 0, 500, 100)];
                }
                [imgViewMenu setImage:nil];
            } else if ([[dictionary objectForKey:@"object_value"] length]==0) {
                //NSData *imageData = [NSData dataWithContentsOfURL:[NSURL URLWithString:imgURL]];
                //UIImage *image = [UIImage imageWithData:imageData];
                //NSLog(@"image height: %f",image.size.height);
                //NSLog(@"image width: %f",image.size.width);
                if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone) {
                    // For iPhone
                    [imgViewMenu setFrame:CGRectMake(0, 0, 250, 50)];
                } else {
                    [imgViewMenu setFrame:CGRectMake(0, 0, 500, 100)];
                }
                menutext.text = @"";
            } else {
                menutext.text = @"";
                [imgViewMenu setImage:nil];
            }
            
            BOOL success;
            
            // Create a FileManager object, we will use this to check the status
            // of the database and to copy it over if required
            NSFileManager *fileManager = [NSFileManager defaultManager];
            
            // Check if the database has already been created in the users filesystem
            success = [fileManager fileExistsAtPath:[[[NSBundle mainBundle] resourcePath] stringByAppendingPathComponent:shortString]];
            
            __block UIActivityIndicatorView *activityIndicator;
            __weak UIImageView *weakImageView = imgViewMenu;
            NSString *imagePath;
            if(success) {
                imagePath = [[[NSBundle mainBundle] resourcePath] stringByAppendingPathComponent:shortString];
                [weakImageView setImage:[UIImage imageNamed:shortString]];
            } else {
                imagePath = imgURL;
                dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0), ^ {
                    [weakImageView sd_setImageWithURL:[NSURL URLWithString:imagePath]
                                     placeholderImage:nil
                                              options:SDWebImageProgressiveDownload
                                             progress:^(NSInteger receivedSize, NSInteger expectedSize, NSURL *targetURL) {
                                                 dispatch_async(dispatch_get_main_queue(), ^ {
                                                     if (!activityIndicator) {
                                                         
                                                         [weakImageView addSubview:activityIndicator = [UIActivityIndicatorView.alloc initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray]];
                                                         activityIndicator.center = weakImageView.center;
                                                         [activityIndicator startAnimating];
                                                      }
                                                  });
                                                 
                                             }
                                            completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
                                                dispatch_async(dispatch_get_main_queue(), ^ {
                                                    //[MBProgressHUD hideHUDForView:self.view animated:YES];
                                                    [activityIndicator removeFromSuperview];
                                                    activityIndicator = nil;
                                                });
                                            }];
                });
            }
            //NSLog(@"shortStringimagePath : %@", imagePath);
            imgViewMenu.contentMode  = UIViewContentModeScaleAspectFill;
            [imgViewMenu setClipsToBounds:YES];
        }
        return cell;
        imgViewMenu =nil;
        menutext =nil;
    }
    
    return nil;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    if (tableView == GridVertableView)
    {
        NSDictionary *dictionary = [listOfVerMenu objectAtIndex:indexPath.row];
        if ([[dictionary objectForKey:@"action_value"] intValue] > 0) {
            //if ([[dictionary objectForKey:@"action_type"] isEqualToString:@"page"]) {
            if ([[dictionary objectForKey:@"action_type"] isEqualToString:@"page"] || [[dictionary objectForKey:@"action_type"] isEqualToString:@"plugin"]) {
                //appdelegate.nextPageType = [dictionary objectForKey:@"action_type"];
                if ([[dictionary objectForKey:@"action_type"] isEqualToString:@"page"]) {
                    //AppDelegate *appdelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
                    appdelegate.nextPage = [dictionary objectForKey:@"action_value"];
                    if (appdelegate.showloading) {
                        alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"loading", nil) message:nil delegate:self cancelButtonTitle:nil otherButtonTitles: nil];
                        
                        //a simple activity indicator:
                        UIActivityIndicatorView *activityIndicator = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
                        activityIndicator.frame= CGRectMake(50, 10, 37, 37);
                        activityIndicator.autoresizingMask = UIViewAutoresizingFlexibleTopMargin | UIViewAutoresizingFlexibleRightMargin | UIViewAutoresizingFlexibleBottomMargin | UIViewAutoresizingFlexibleLeftMargin;
                        [activityIndicator startAnimating];
                        
                        //the magic line below,
                        //we associate the activity indicator to the alert view: (addSubview is not used)
                        [alert setValue:activityIndicator forKey:@"accessoryView"];
                        
                        [alert show];
                    
                        // Adjust the indicator so it is up a few pixels from the bottom of the alert
                        [UIView beginAnimations:nil context:nil];
                        [UIView setAnimationDuration:0.0];
                        [UIView setAnimationDelay:0.0];
                        [UIView setAnimationTransition:UIViewAnimationTransitionNone forView:alert cache:YES];
                        [UIView setAnimationDelegate:self];
                        [UIView setAnimationDidStopSelector:@selector(startupAnimationDoneIndicator:finished:context:)];
                        alert.alpha = 0.5;
                        [UIView commitAnimations];
                        
                    } else {
                        [self goNextPage];
                    }
                    
                } else {
                    //AppDelegate *appdelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
                    appdelegate.plugin_id = [dictionary objectForKey:@"action_value"];
                    sqlite3 *database;
                    NSString *apluginid;
                    // NSString *aplugintype;
                    //NSString *apluginvalue1;
                    //NSString *apluginvalue2;
                    //NSString *apluginvalue3;
                    NSString *aremark;
                    
                    if(sqlite3_open([appdelegate.databasePath UTF8String], &database) == SQLITE_OK) {
                        NSString *theme_object = [[NSString alloc] initWithFormat:@"SELECT * from theme_object_plugin WHERE id = %@", appdelegate.plugin_id];
                        //NSLog(@"theme_object_plugin:: %@", theme_object);
                        
                        const char *sqlStatement1 = [theme_object UTF8String];
                        sqlite3_stmt *compiledStatement1;
                        if(sqlite3_prepare_v2(database, sqlStatement1, -1, &compiledStatement1, NULL) == SQLITE_OK) {
                            while(sqlite3_step(compiledStatement1) == SQLITE_ROW) {
                                
                                aid = [NSString stringWithUTF8String:(char *)sqlite3_column_text(compiledStatement1, 0)];
                                apluginid = [NSString stringWithUTF8String:(char *)sqlite3_column_text(compiledStatement1, 11)];
                                aplugintype = [NSString stringWithUTF8String:(char *)sqlite3_column_text(compiledStatement1, 3)];
                                apluginvalue1 = [NSString stringWithUTF8String:(char *)sqlite3_column_text(compiledStatement1, 4)];
                                apluginvalue2 = [NSString stringWithUTF8String:(char *)sqlite3_column_text(compiledStatement1, 5)];
                                apluginvalue3 = [NSString stringWithUTF8String:(char *)sqlite3_column_text(compiledStatement1, 6)];
                                aremark = [NSString stringWithUTF8String:(char *)sqlite3_column_text(compiledStatement1, 8)];
                                
                            }
                        }
                        // Release the compiled statement from memory
                        sqlite3_finalize(compiledStatement1);
                    }
                    sqlite3_close(database);
                    //NSLog(@"aplugintype:: %@", aplugintype);
                    if ([aplugintype isEqualToString:@"email"]) {
                        if ([apluginvalue1 length]>0) {
                            // Email Subject
                            //NSString *emailTitle = @"Test Email";
                            // Email Content
                            //NSString *messageBody = @"<h1>Learning iOS Programming!</h1>"; // Change the message body to HTML
                            // To address
                            NSArray *toRecipents = [NSArray arrayWithObject:apluginvalue1];
                            
                            MFMailComposeViewController *mc = [[MFMailComposeViewController alloc] init];
                            mc.mailComposeDelegate = self;
                            //[mc setSubject:emailTitle];
                            //[mc setMessageBody:messageBody isHTML:YES];
                            [mc setToRecipients:toRecipents];
                            
                            // Present mail view controller on screen
                            //[viewCtrl presentViewController:mc animated:YES completion:NULL];
                            //[viewCtrl.navigationController pushViewController:mc animated:YES];
                            if([MFMailComposeViewController canSendMail])
                            {
                                [self presentViewController:mc animated:NO completion:NULL];
                                //[viewCtrl.navigationController pushViewController:mc animated:YES];
                            }
                        }
                        
                    } else if ([aplugintype isEqualToString:@"call"]) {
                        //NSLog(@"apluginvalue1:: %@", apluginvalue1);
                        if ([apluginvalue1 length]>0) {
                            //NSString *strPhone = [[NSString alloc] initWithFormat:@"tel://%@", apluginvalue1];
                            NSString *strPhone = [[NSString alloc] initWithFormat:@"telprompt://%@", apluginvalue1];
                            [[UIApplication sharedApplication] openURL:[NSURL URLWithString:strPhone]];
                        }
                    } else {
                        if (appdelegate.showloading) {
                            
                            alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"loading", nil) message:nil delegate:self cancelButtonTitle:nil otherButtonTitles: nil];
                            
                            //a simple activity indicator:
                            UIActivityIndicatorView *activityIndicator = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
                            activityIndicator.frame= CGRectMake(50, 10, 37, 37);
                            activityIndicator.autoresizingMask = UIViewAutoresizingFlexibleTopMargin | UIViewAutoresizingFlexibleRightMargin | UIViewAutoresizingFlexibleBottomMargin | UIViewAutoresizingFlexibleLeftMargin;
                            [activityIndicator startAnimating];
                            
                            //the magic line below,
                            //we associate the activity indicator to the alert view: (addSubview is not used)
                            [alert setValue:activityIndicator forKey:@"accessoryView"];
                            
                            [alert show];
                        
                            // Adjust the indicator so it is up a few pixels from the bottom of the alert
                            [UIView beginAnimations:nil context:nil];
                            [UIView setAnimationDuration:0.0];
                            [UIView setAnimationDelay:0.0];
                            [UIView setAnimationTransition:UIViewAnimationTransitionNone forView:alert cache:YES];
                            [UIView setAnimationDelegate:self];
                            [UIView setAnimationDidStopSelector:@selector(startupAnimationDoneIndicatorPlugin:finished:context:)];
                            alert.alpha = 0.5;
                            [UIView commitAnimations];
                        } else {
                            [self goPluginPage];
                        }
                    }
                    //[UIView setAnimationDidStopSelector:@selector(startupAnimationDoneIndicatorPlugin:finished:context:)];
                }
            }
        }
        //
    }
}

-(void) dissmissIndicator {
    [alert dismissWithClickedButtonIndex:0 animated:YES];
}

- (void)startupAnimationDoneIndicator:(NSString *)animationID finished:(NSNumber *)finished context:(void *)context
{
    [self goNextPage];
    [self dissmissIndicator];
    alert =nil;
}

- (void)startupAnimationDoneIndicatorPlugin:(NSString *)animationID finished:(NSNumber *)finished context:(void *)context
{
    [self goPluginPage];
    [self dissmissIndicator];
    alert =nil;
}

-(void) goNextPage {
    //[self performSegueWithIdentifier:@"View1Segue" sender:self] ;
    sqlite3 *database;
    NSString *themecodenextpage;
    NSString *page_idnextpage;
    NSString *page_namenextpage;
    
    if(sqlite3_open([appdelegate.databasePath UTF8String], &database) == SQLITE_OK) {
        // Setup the SQL Statement and compile it for faster access
        NSString *temp = [[NSString alloc] initWithFormat:@"SELECT * from theme_page WHERE id = %@", appdelegate.nextPage];
        //NSLog(@"temp:: %@", temp);
        const char *sqlStatement = [temp UTF8String];
        sqlite3_stmt *compiledStatement;
        if(sqlite3_prepare_v2(database, sqlStatement, -1, &compiledStatement, NULL) == SQLITE_OK) {
            while(sqlite3_step(compiledStatement) == SQLITE_ROW) {
                page_idnextpage = [NSString stringWithUTF8String:(char *)sqlite3_column_text(compiledStatement, 0)];
                themecodenextpage = [NSString stringWithUTF8String:(char *)sqlite3_column_text(compiledStatement, 1)];
                page_namenextpage = [NSString stringWithUTF8String:(char *)sqlite3_column_text(compiledStatement, 2)];
                //prevpage = [NSString stringWithUTF8String:(char *)sqlite3_column_text(compiledStatement, 3)];
                
            }
        }
        // Release the compiled statement from memory
        sqlite3_finalize(compiledStatement);
    }
    sqlite3_close(database);
    
    ThemeViewController *themeViewController = [[ThemeViewController alloc] init];
    themeViewController.page_id = page_idnextpage;
    themeViewController.themepagenotification = @"Normal"; /*** Push Notification 27Aug2015 ***/
    [appdelegate.navController pushViewController:themeViewController animated:NO];
    appdelegate.homeclicked = NO;
    [self dismissViewControllerAnimated:YES completion:nil];
}

-(void) goPluginPage {
    
    if ([aplugintype isEqualToString:@"content"]) {
        sqlite3 *database;
        NSString *listtemplate;
        NSString *cont_id;
        NSString *cont_title;
        
        if(sqlite3_open([appdelegate.databasePath UTF8String], &database) == SQLITE_OK) {
            NSString *theme_object = [[NSString alloc] initWithFormat:@"SELECT cc.list_template, c.id, c.title from content_category cc JOIN contents c ON cc.id = c.category_id WHERE cc.top_id = %@", aid];
            //NSLog(@"theme_object_Details:: %@", theme_object);
            
            const char *sqlStatement1 = [theme_object UTF8String];
            sqlite3_stmt *compiledStatement1;
            if(sqlite3_prepare_v2(database, sqlStatement1, -1, &compiledStatement1, NULL) == SQLITE_OK) {
                while(sqlite3_step(compiledStatement1) == SQLITE_ROW) {
                    
                    listtemplate = [NSString stringWithUTF8String:(char *)sqlite3_column_text(compiledStatement1, 0)];
                    cont_id = [NSString stringWithUTF8String:(char *)sqlite3_column_text(compiledStatement1, 1)];
                    cont_title = [NSString stringWithUTF8String:(char *)sqlite3_column_text(compiledStatement1, 2)];
                    
                }
            }
            // Release the compiled statement from memory
            sqlite3_finalize(compiledStatement1);
        }
        sqlite3_close(database);
        //NSLog(@"listtemplate:: %@", listtemplate);
        if ([listtemplate isEqualToString:@"1"]) {
            ContentPluginDetailsViewController *contentPluginDetailsViewController = [[ContentPluginDetailsViewController alloc] init];
            contentPluginDetailsViewController.contId = cont_id;
            contentPluginDetailsViewController.conttitle = apluginvalue1;
            contentPluginDetailsViewController.listtemplate = listtemplate;
            [appdelegate.navController pushViewController:contentPluginDetailsViewController animated:NO];
        } else {
            ContentPluginViewController *contentPluginViewController = [[ContentPluginViewController alloc] init];
            [appdelegate.navController pushViewController:contentPluginViewController animated:NO];
        }
        listtemplate = nil;
        cont_id = nil;
        cont_title = nil;
        
    } else if ([aplugintype isEqualToString:@"productcat"]) {
        
        ContentProductCatPluginViewController *contentProductCatPluginViewController = [[ContentProductCatPluginViewController alloc] init];
        contentProductCatPluginViewController.pluginValue1 = apluginvalue1;
        contentProductCatPluginViewController.pluginValue2 = apluginvalue2;
        contentProductCatPluginViewController.pluginValue3 = apluginvalue3;
        [appdelegate.navController pushViewController:contentProductCatPluginViewController animated:NO];
        
    } else if ([aplugintype isEqualToString:@"contentcat"]) {
        
        ContentCatPluginViewController *contentCatPluginViewController = [[ContentCatPluginViewController alloc] init];
        contentCatPluginViewController.pluginValue1 = apluginvalue1;
        contentCatPluginViewController.pluginValue2 = apluginvalue2;
        contentCatPluginViewController.pluginValue3 = apluginvalue3;
        [appdelegate.navController pushViewController:contentCatPluginViewController animated:NO];
        
    } else if ([aplugintype isEqualToString:@"customer"]) {
        NSString *nibFileName;
        if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone) {
            nibFileName = @"SettingViewController";
        } else {
            nibFileName = @"SettingViewController_ipad";
        }
        SettingViewController *settingViewController = [[SettingViewController alloc] initWithNibName:nibFileName bundle:nil];
        settingViewController.cont_title = apluginvalue1;
        appdelegate.shouldReturnToPrevPage = NO;
        [appdelegate.navController pushViewController:settingViewController animated:NO];
    } else if ([aplugintype isEqualToString:@"loyalty"]) {
        NSString *nibFileName;
        if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone) {
            nibFileName = @"LoyaltyViewController";
        } else {
            nibFileName = @"LoyaltyViewController_ipad";
        }
        LoyaltyViewController *loyaltyViewController = [[LoyaltyViewController alloc] initWithNibName:nibFileName bundle:nil];
        loyaltyViewController.cont_title = apluginvalue1;
        [appdelegate.navController pushViewController:loyaltyViewController animated:NO];
    } else if ([aplugintype isEqualToString:@"ibeacon"]) {
        appdelegate.ibeaconfrommenu = YES;
        //[appdelegate.locationManager stopUpdatingLocation];
        //[appdelegate.locationManager startUpdatingLocation];
        appdelegate.beaconFoundTimer = appdelegate.intervalNotification-2;
    } else if ([aplugintype isEqualToString:@"warranty"]) {
        NSString *nibFileName;
        if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone) {
            nibFileName = @"WarrantyViewController";
        } else {
            nibFileName = @"WarrantyViewController_ipad";
        }
        WarrantyViewController *warrantyViewController = [[WarrantyViewController alloc] initWithNibName:nibFileName bundle:nil];
        warrantyViewController.cont_title = apluginvalue1;
        [appdelegate.navController pushViewController:warrantyViewController animated:NO];
    } else if ([aplugintype isEqualToString:@"search"]) {
        ContentSearchViewController *contentSearchViewController = [[ContentSearchViewController alloc] init];
        contentSearchViewController.list_template = @"5";
        contentSearchViewController.cont_title = apluginvalue1;
        [appdelegate.navController pushViewController:contentSearchViewController animated:NO];
    } else if ([aplugintype isEqualToString:@"favorite"]) {
        ContentPluginViewController *contentPluginViewController = [[ContentPluginViewController alloc] init];
        contentPluginViewController.list_favorite = @"ListOut";
        [appdelegate.navController pushViewController:contentPluginViewController animated:NO];
    } else if ([aplugintype isEqualToString:@"booking"]) {
        NSString *nibFileName;
        if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone) {
            nibFileName = @"BookingListViewController";
        } else {
            nibFileName = @"BookingListViewController_ipad";
        }
        BookingListViewController *bookingListViewController = [[BookingListViewController alloc] initWithNibName:nibFileName bundle:nil];
        bookingListViewController.cont_title = apluginvalue1;
        [appdelegate.navController pushViewController:bookingListViewController animated:NO];
    } else if ([aplugintype isEqualToString:@"points"]) {
        NSString *nibFileName;
        if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone) {
            nibFileName = @"ScorePointViewController";
        } else {
            nibFileName = @"ScorePointViewController_ipad";
        }
        ScorePointViewController *scorePointViewController = [[ScorePointViewController alloc] initWithNibName:nibFileName bundle:nil];
        scorePointViewController.cont_title = apluginvalue1;
        [appdelegate.navController pushViewController:scorePointViewController animated:NO];
    } else if ([aplugintype isEqualToString:@"inbox"]) {
        
        InboxViewController *inboxViewController = [[InboxViewController alloc] init];
        inboxViewController.topId = aid;
        inboxViewController.conttitle = apluginvalue1;
        [appdelegate.navController pushViewController:inboxViewController animated:NO];
    } else if ([aplugintype isEqualToString:@"point_slot"]) {
        
        //
    } else if ([aplugintype isEqualToString:@"notes"]) {
        NSString *nibFileName;
        if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone) {
            nibFileName = @"NotesViewController";
        } else {
            nibFileName = @"NotesViewController_ipad";
        }
        NotesViewController *notesViewController = [[NotesViewController alloc] initWithNibName:nibFileName bundle:nil];
        [appdelegate.navController pushViewController:notesViewController animated:NO];
        
    } else if ([aplugintype isEqualToString:@"sirim_label"]) {
        SIRIMLabelViewController *sirimLabelViewController = [[SIRIMLabelViewController alloc] init];
        [appdelegate.navController pushViewController:sirimLabelViewController animated:NO];
    
    } else if ([aplugintype isEqualToString:@"sirim_pci"]) {
        SIRIMPCIViewController *sirimLabelViewController = [[SIRIMPCIViewController alloc] init];
        [appdelegate.navController pushViewController:sirimLabelViewController animated:NO];
        
    } else {
        BasicPluginViewController *basicPluginViewController = [[BasicPluginViewController alloc] init];
        [appdelegate.navController pushViewController:basicPluginViewController animated:NO];
    }
    appdelegate.homeclicked = NO;
    [self dismissViewControllerAnimated:YES completion:nil];
}

@end
