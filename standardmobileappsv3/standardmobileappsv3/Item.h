//
//  Item.h
//  Button_Example
//
//  Created by Chakra on 08/04/11.
//  Copyright 2011 Chakra Interactive Pvt Ltd. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Item : NSObject {
	
	NSString *title;
	NSString *contid;
	NSString *image;
    NSString *contname;
    NSString *price;
    NSString *var1;
    NSString *pricetitle;
    NSString *topid;
    NSString *linkurl;
}

@property (nonatomic, copy) NSString *title;
@property (nonatomic, copy) NSString *contid;
@property (nonatomic, copy) NSString *image;
@property (nonatomic, copy) NSString *contname;
@property (nonatomic, copy) NSString *price;
@property (nonatomic, copy) NSString *var1;
@property (nonatomic, copy) NSString *pricetitle;
@property (nonatomic, copy) NSString *topid;
@property (nonatomic, copy) NSString *linkurl;

@end
