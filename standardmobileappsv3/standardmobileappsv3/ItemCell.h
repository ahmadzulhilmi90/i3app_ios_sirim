//
//  MSSItemCell.h
//
//  Created by Maggie on 9/13/13.
//  Copyright (c) 2013 M3Online . All rights reserved.
//
//
#import <UIKit/UIKit.h>

@interface ItemCell : UITableViewCell

@property (nonatomic, strong) UILabel *labelname;
@property (nonatomic, strong) UILabel *labelvalue;
@property (nonatomic, strong) UIImageView *imageicon;

@end
