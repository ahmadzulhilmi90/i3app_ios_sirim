//
//  MSSItemCell.m
//
//  Created by Maggie on 9/13/13.
//  Copyright (c) 2013 M3Online . All rights reserved.
//
//

#import "ItemCell.h"

@implementation ItemCell

@synthesize labelname = _labelname;
@synthesize labelvalue = _labelvalue;
@synthesize imageicon = _imageicon;

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // configure control(s)
        //self.imageicon = [[UIImageView alloc] initWithFrame:CGRectMake(15, 5, 30, 30)];
        //[self addSubview:self.imageicon];
        
        self.labelname = [[UILabel alloc] initWithFrame:CGRectMake(5, 5, 110, 30)];
        self.labelname.textColor = [UIColor blackColor];
        self.labelname.font = [UIFont fontWithName:@"Arial" size:12.0f];
        self.labelname.backgroundColor = [UIColor clearColor];
        self.labelname.opaque = NO;
        [self addSubview:self.labelname];
        
        if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone) {
            // For iPhone
            self.labelvalue = [[UILabel alloc] initWithFrame:CGRectMake(120, 5, 180, 30)];
        } else {
            self.labelvalue = [[UILabel alloc] initWithFrame:CGRectMake(120, 5, 1004-120, 30)];
        }
        self.labelvalue.textColor = [UIColor blackColor];
        self.labelvalue.font = [UIFont fontWithName:@"Arial" size:12.0f];
        self.labelvalue.backgroundColor = [UIColor clearColor];
        self.labelvalue.lineBreakMode = NSLineBreakByWordWrapping;
        self.labelvalue.numberOfLines = 0;
        //self.labelvalue.textAlignment= NSTextAlignmentRight;
        self.labelvalue.opaque = NO;
        [self addSubview:self.labelvalue];
        
        
    }
    return self;
}

- (void)dealloc {
    _labelname =nil;
    _labelvalue =nil;
    _imageicon =nil;
    //[super dealloc];
}
@end
