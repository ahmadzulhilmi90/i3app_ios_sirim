//
//  LoyaltyDetailViewController.h
//  standardmobileapps
//
//  Created by Hazwan on 11/28/13.
//  Copyright (c) 2013 Maggie. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface LoyaltyDetailViewController : UIViewController <UITextFieldDelegate, UICollectionViewDelegate, UICollectionViewDataSource,UICollectionViewDelegateFlowLayout, UIViewControllerTransitioningDelegate>

@property (assign) int total_stamp;
@property (assign) int total_gotStamped;
@property (nonatomic,strong) NSString *secret_code;
@property (nonatomic,strong) NSString *loyalty_title;
@property (nonatomic,strong) NSString *loyalty_desc;
@property (nonatomic,strong) NSString *loyalty_id;

@property (strong, nonatomic) NSString *stamp_icon_type;
//@property (strong, nonatomic) NSString *stamp_icon_name;
//@property (strong, nonatomic) NSString *stamp_icon_bg_name;
@property (strong, nonatomic) NSString *stamp_icon_url;
@property (strong, nonatomic) NSString *stamp_icon_bg_url;
@property (strong, nonatomic) NSString *bg_img;

@property (strong, nonatomic) IBOutlet UILabel *label_loyaltyTitle;
@property (strong, nonatomic) IBOutlet UILabel *label_loyaltyDesc;
@property (strong, nonatomic) IBOutlet UIScrollView *scrollView;
@property (strong, nonatomic) IBOutlet UIView *view_stampRedeemCard;
@property (strong, nonatomic) IBOutlet UIView *view_stampRedeemCardInside;
@property (strong, nonatomic) IBOutlet UIScrollView *scrollView_popover;
@property (strong, nonatomic) IBOutlet UICollectionView *myCollectionView;
@property (strong, nonatomic) IBOutlet UIButton *button_stampAndRedeem;
@property (strong, nonatomic) IBOutlet UIButton *button_stampAndRedeemPopover;
@property (strong, nonatomic) IBOutlet UITextField *txt_secretCode;
@property (strong, nonatomic) IBOutlet UITextField *txt_repCode;

@property (nonatomic, strong) NSString *conttitle;

- (IBAction)onStampAndRedeemCardClicked:(id)sender;
- (IBAction)onCancelStampAndRedeem:(id)sender;
- (IBAction)onConfirmStampAndRedeemClicked:(id)sender;

@end
