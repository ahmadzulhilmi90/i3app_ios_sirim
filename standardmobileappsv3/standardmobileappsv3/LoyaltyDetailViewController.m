//
//  LoyaltyDetailViewController.m
//  standardmobileapps
//
//  Created by Hazwan on 11/28/13.
//  Copyright (c) 2013 Maggie. All rights reserved.
//

#import "LoyaltyDetailViewController.h"
#import "AppDelegate.h"
#import <QuartzCore/QuartzCore.h>
#import "LoadingView.h"
#import "SBJson.h"
//#import "JSON.h"
#import "RootViewController.h"
#import "ViewController.h"
#import "InitialSlidingViewController.h"
#import "BTSlideInteractor.h"

@interface LoyaltyDetailViewController()
@property (strong, nonatomic) BTSlideInteractor *interactor;
@end

InitialSlidingViewController *modalController;

AppDelegate *appdelegate;
__strong LoadingView *loadingView;
__strong NSString *currentRequest;
//__strong NSMutableData *receivedData;
int scrollViewHeight;

@implementation LoyaltyDetailViewController

@synthesize scrollView, scrollView_popover;
@synthesize total_stamp, total_gotStamped, secret_code,label_loyaltyDesc,loyalty_desc, loyalty_id, label_loyaltyTitle, loyalty_title;
@synthesize view_stampRedeemCardInside,view_stampRedeemCard, myCollectionView, button_stampAndRedeem, button_stampAndRedeemPopover;
@synthesize txt_repCode, txt_secretCode;
@synthesize stamp_icon_type, stamp_icon_url, stamp_icon_bg_url, conttitle, bg_img;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self)
    {
        appdelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    }
    return self;
}


- (UIColor *) colorWithHexString:(NSString *)hexString {
    NSString *colorString = [[hexString stringByReplacingOccurrencesOfString: @"#" withString: @""] uppercaseString];
    CGFloat alpha, red, blue, green;
    alpha = 1.0f;
    red   = [self colorComponentFrom: colorString start: 0 length: 2];
    green = [self colorComponentFrom: colorString start: 2 length: 2];
    blue  = [self colorComponentFrom: colorString start: 4 length: 2];
    return [UIColor colorWithRed: red green: green blue: blue alpha: alpha];
}

- (CGFloat) colorComponentFrom:(NSString *)string start:(NSUInteger)start length:(NSUInteger)length {
    NSString *substring = [string substringWithRange: NSMakeRange(start, length)];
    NSString *fullHex = length == 2 ? substring : [NSString stringWithFormat: @"%@%@", substring, substring];
    unsigned hexComponent;
    [[NSScanner scannerWithString: fullHex] scanHexInt: &hexComponent];
    return hexComponent / 255.0;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    currentRequest = @"countGotStamped";
    NSString *urlAddress2 = [[NSString alloc] initWithFormat:@"%@loyalty_stamp.php?db=%@&userid=%@&udid=%@&cuid=%@&task=get&loyalty_id=%@&rep_code=&device=ios&sc=%@",appdelegate.user_apipath,appdelegate.db, appdelegate.user, appdelegate.uuid, appdelegate.customer_id,loyalty_id, appdelegate.cs];
    [self connectToServer:urlAddress2];
    
    self.navigationItem.backBarButtonItem = [[UIBarButtonItem alloc]initWithTitle:@"" style:UIBarButtonItemStylePlain target:nil action:nil];
    
    appdelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    
    self.title = conttitle;
    appdelegate.homeclicked = NO;
    CGRect rect = CGRectMake(0,0,20,20);
    UIGraphicsBeginImageContext( rect.size );
    [[UIImage imageNamed:@"home.png"] drawInRect:rect];
    UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    UIBarButtonItem *myHomeButton = [[UIBarButtonItem alloc] initWithImage:image style:UIBarButtonItemStyleDone target:self action:@selector(myHomeAction:)];
    
    
    NSArray *myButtonArray = [[NSArray alloc] initWithObjects:myHomeButton, nil];
    
    self.navigationItem.rightBarButtonItems = myButtonArray;
    
    if ([bg_img isEqualToString:@""]) {
        //self.backgroundColor = [self colorWithHexString:appdelegate.themeColorCode1];
        self.view.backgroundColor = [UIColor whiteColor];
    } else {
        NSURL *imageURL = [NSURL URLWithString:bg_img];
        NSData *imageData = [NSData dataWithContentsOfURL:imageURL];
        //self.backgroundColor = [UIColor redColor];
        UIGraphicsBeginImageContext(self.view.frame.size);
        [[UIImage imageWithData:imageData] drawInRect:self.view.bounds];
        UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
        UIGraphicsEndImageContext();
        self.view.backgroundColor = [UIColor colorWithPatternImage:image];
        //self.backgroundColor = [[UIColor alloc] initWithPatternImage:[UIImage imageWithData:imageData]];
    }
    
    //--Displaying Loyalty Title
    CGRect labelFrameTitle = CGRectMake(20,0,280,30);
    label_loyaltyTitle = [[UILabel alloc] initWithFrame:labelFrameTitle];
    [label_loyaltyTitle setNumberOfLines:0];
    label_loyaltyTitle.adjustsFontSizeToFitWidth = YES;
    label_loyaltyTitle.font = [UIFont boldSystemFontOfSize:20];
    [label_loyaltyTitle setText:loyalty_title];
    label_loyaltyTitle.autoresizingMask = UIViewAutoresizingFlexibleHeight | UIViewAutoresizingFlexibleTopMargin | UIViewAutoresizingFlexibleBottomMargin;
    label_loyaltyTitle.textColor = [self colorWithHexString:appdelegate.themeColorCode];
    label_loyaltyTitle.backgroundColor = [UIColor clearColor];
    
    //Calculate the expected size based on the font and linebreak mode of your label
    CGSize maximumLabelSizeTitle = CGSizeMake(296,9999);
    
    NSDictionary *attributesDictionaryTitle = [NSDictionary dictionaryWithObjectsAndKeys:
                                          label_loyaltyTitle.font, NSFontAttributeName,
                                          nil];
    
    NSMutableAttributedString *stringTitle = [[NSMutableAttributedString alloc] initWithString:label_loyaltyTitle.text attributes:attributesDictionaryTitle];
    
    CGRect expectedLabelSizeTitle = [stringTitle boundingRectWithSize:maximumLabelSizeTitle options:NSStringDrawingUsesLineFragmentOrigin context:nil];
    
    //adjust the label the the new height.
    CGRect newFrameTitle = label_loyaltyTitle.frame;
    newFrameTitle.size.height = expectedLabelSizeTitle.size.height + 10;
    label_loyaltyTitle.frame = newFrameTitle;
    [scrollView addSubview:label_loyaltyTitle];
    
    //--Displaying Loyalty description
    CGRect labelFrame = CGRectMake(20,(label_loyaltyTitle.frame.origin.y + label_loyaltyTitle.frame.size.height),280,70);
    label_loyaltyDesc = [[UILabel alloc] initWithFrame:labelFrame];
    [label_loyaltyDesc setNumberOfLines:0];
    label_loyaltyDesc.adjustsFontSizeToFitWidth = YES;
    [label_loyaltyDesc setText:loyalty_desc];
    label_loyaltyDesc.autoresizingMask = UIViewAutoresizingFlexibleHeight | UIViewAutoresizingFlexibleTopMargin | UIViewAutoresizingFlexibleBottomMargin;
    label_loyaltyDesc.textColor = [UIColor blackColor];
    label_loyaltyDesc.backgroundColor = [UIColor clearColor];
    
    //Calculate the expected size based on the font and linebreak mode of your label
    CGSize maximumLabelSize = CGSizeMake(296,9999);
    
    NSDictionary *attributesDictionary = [NSDictionary dictionaryWithObjectsAndKeys:
                                          label_loyaltyDesc.font, NSFontAttributeName,
                                          nil];
    
    NSMutableAttributedString *string = [[NSMutableAttributedString alloc] initWithString:label_loyaltyDesc.text attributes:attributesDictionary];
    
    CGRect expectedLabelSize = [string boundingRectWithSize:maximumLabelSize options:NSStringDrawingUsesLineFragmentOrigin context:nil];
    
    //adjust the label the the new height.
    CGRect newFrame = label_loyaltyDesc.frame;
    newFrame.size.height = expectedLabelSize.size.height + 10;
    label_loyaltyDesc.frame = newFrame;
    [scrollView addSubview:label_loyaltyDesc];
    
    //--Displaying Collection View
    CGRect cvFrame = CGRectMake(20,(label_loyaltyDesc.frame.origin.y + label_loyaltyDesc.frame.size.height),280,70);
    UICollectionViewFlowLayout *aFlowLayout = [[UICollectionViewFlowLayout alloc] init];
    [aFlowLayout setScrollDirection:UICollectionViewScrollDirectionVertical];
    myCollectionView = [[UICollectionView alloc]initWithFrame:cvFrame collectionViewLayout:aFlowLayout];
    
    myCollectionView.delegate = self;
    myCollectionView.dataSource = self;
    
    [myCollectionView registerClass:[UICollectionViewCell class] forCellWithReuseIdentifier:@"ID"];
    [myCollectionView setBackgroundColor:[UIColor colorWithRed:0.241 green:0.241 blue:0.241 alpha:1.0]];
    
    myCollectionView.layer.cornerRadius = 5;
    
    cvFrame = CGRectMake(20,(label_loyaltyDesc.frame.origin.y + label_loyaltyDesc.frame.size.height + 10),280,myCollectionView.collectionViewLayout.collectionViewContentSize.height);
    [myCollectionView setFrame:cvFrame];
    
    [scrollView addSubview:myCollectionView];
    
    //--Displaying Instruction Label
    CGRect labelFrame2 = CGRectMake(20,(myCollectionView.frame.origin.y + myCollectionView.collectionViewLayout.collectionViewContentSize.height + 10),280,70);
    UILabel *label_loyaltyInst = [[UILabel alloc] initWithFrame:labelFrame2];
    [label_loyaltyInst setNumberOfLines:0];
    [label_loyaltyInst setTextAlignment:NSTextAlignmentLeft];
    label_loyaltyInst.adjustsFontSizeToFitWidth = YES;
    [label_loyaltyInst setText:NSLocalizedString(@"order_text", nil)];
    label_loyaltyInst.autoresizingMask = UIViewAutoresizingFlexibleHeight | UIViewAutoresizingFlexibleTopMargin | UIViewAutoresizingFlexibleBottomMargin;
    label_loyaltyInst.textColor = [UIColor blackColor];
    label_loyaltyInst.backgroundColor = [UIColor clearColor];
    
    //Calculate the expected size based on the font and linebreak mode of your label
    CGSize maximumLabelSize2 = CGSizeMake(296,9999);
    /*CGSize expectedLabelSize2 = [label_loyaltyInst.text sizeWithFont:label_loyaltyInst.font
                                                  constrainedToSize:maximumLabelSize2
                                                      lineBreakMode:label_loyaltyInst.lineBreakMode]; */
    NSDictionary *attributesDictionary2 = [NSDictionary dictionaryWithObjectsAndKeys:
                                          label_loyaltyInst.font, NSFontAttributeName,
                                          nil];
    
    NSMutableAttributedString *string2 = [[NSMutableAttributedString alloc] initWithString:label_loyaltyInst.text attributes:attributesDictionary2];
    
    CGRect expectedLabelSize2 = [string2 boundingRectWithSize:maximumLabelSize2 options:NSStringDrawingUsesLineFragmentOrigin context:nil];
    
    //adjust the label the the new height.
    CGRect newFrame2 = label_loyaltyInst.frame;
    newFrame2.size.height = expectedLabelSize2.size.height + 10;
    label_loyaltyInst.frame = newFrame2;
    [scrollView addSubview:label_loyaltyInst];
    
    //--Displaying Stamp Button
    button_stampAndRedeem = [UIButton buttonWithType:UIButtonTypeCustom];
    [button_stampAndRedeem addTarget:self action:@selector(onStampAndRedeemCardClicked:) forControlEvents:UIControlEventTouchUpInside];
    [button_stampAndRedeem setTitle:NSLocalizedString(@"stamp", nil) forState:UIControlStateNormal];
    CGRect btnFrame;
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone) {
        btnFrame = CGRectMake(20, (label_loyaltyInst.frame.origin.y + label_loyaltyInst.frame.size.height + 10), 136, 44);
    } else {
        btnFrame = CGRectMake(20, (label_loyaltyInst.frame.origin.y + label_loyaltyInst.frame.size.height + 10), 136, 44);
    }
    button_stampAndRedeem.frame = btnFrame;
    [button_stampAndRedeem setBackgroundColor:[UIColor grayColor]];
    [button_stampAndRedeem setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    button_stampAndRedeem.layer.borderWidth = 0.5f;
    button_stampAndRedeem.layer.borderColor = [UIColor grayColor].CGColor;
    button_stampAndRedeem.layer.cornerRadius = 7;
    [scrollView addSubview:button_stampAndRedeem];
    
    //--Setting scrollView contentSize
    scrollViewHeight = 0.0f;
    
    for (UIView* view in scrollView.subviews)
        scrollViewHeight += view.frame.size.height + 5;
    
    //scrollView.contentSize = CGSizeMake(scrollView.frame.size.width, scrollViewHeight);
    scrollView.contentSize = CGSizeMake(scrollView.frame.size.width, (label_loyaltyInst.frame.origin.y + label_loyaltyInst.frame.size.height + 10) + 44 +10);
}

//Slide Menu
- (void)willAnimateRotationToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation duration:(NSTimeInterval)duration
{
    //NSLog(@"view frame=%@ bounds=%@", NSStringFromCGRect(self.view.frame), NSStringFromCGRect(self.view.bounds));
}


- (void)showModalButtonWasTouched
{
    self.interactor = [[BTSlideInteractor alloc] init];
    self.interactor.presenting = YES;
    
    //BTModalViewController *modalController = [self.storyboard instantiateViewControllerWithIdentifier:@"ModalViewController"];
    modalController = [[InitialSlidingViewController alloc] init];
    modalController.modalPresentationStyle = UIModalPresentationCustom;
    modalController.transitioningDelegate = self.interactor;
    [self.navigationController presentViewController:modalController animated:YES completion:nil];
    //[self.navigationController popToViewController:modalController animated:NO];
    //[self.navigationController pushViewController:modalController animated:YES];
}
// End Slide Menu

-(void)myHomeAction:(UIBarButtonItem *)sender{
    
    appdelegate.nextPage = @"";
    
    if ([appdelegate.listOfVerMenu count]>0) {
        if (appdelegate.homeclicked==NO) {
            appdelegate.homeclicked = YES;
            [self showModalButtonWasTouched];
        } else {
            appdelegate.homeclicked = NO;
            [modalController dismissViewControllerAnimated:YES completion:nil];
        }
    } else {
        //AppDelegate *appdelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
        //NSLog(@"View Controllers : %@",[appdelegate.navController viewControllers]);
        NSMutableArray *navigationArray = [[NSMutableArray alloc] initWithArray: appdelegate.navController.viewControllers];
        
        for (UIViewController* viewController in appdelegate.navController.viewControllers) {
            
            //This if condition checks whether the viewController's class is MyGroupViewController
            // if true that means its the MyGroupViewController (which has been pushed at some point)
            if ([viewController isKindOfClass:[RootViewController class]] ) {
                
                // Here viewController is a reference of UIViewController base class of MyGroupViewController
                // but viewController holds MyGroupViewController  object so we can type cast it here
                //ThemeViewController *themeViewController = (ThemeViewController*)viewController;
                //[self.navigationController popToViewController:themeViewController animated:YES];
                [navigationArray removeObjectAtIndex: 0];
            }
        }
        appdelegate.navController.viewControllers = navigationArray;
        for (UIViewController* viewController in appdelegate.navController.viewControllers) {
            
            //This if condition checks whether the viewController's class is MyGroupViewController
            // if true that means its the MyGroupViewController (which has been pushed at some point)
            if ([viewController isKindOfClass:[ViewController class]] ) {
                
                // Here viewController is a reference of UIViewController base class of MyGroupViewController
                // but viewController holds MyGroupViewController  object so we can type cast it here
                ViewController *themeViewController = (ViewController*)viewController;
                [appdelegate.navController popToViewController:themeViewController animated:NO];
                //[self.navigationController popToViewController:viewController animated:YES];
            }
        }
        //NSLog(@"View Controllers111 : %@",appdelegate.navController.viewControllers);
    }
    
}

-(void) viewDidDisappear:(BOOL)animated {
    
    [super viewDidDisappear:animated];
    
    appdelegate.homeclicked = NO;
    [modalController dismissViewControllerAnimated:YES completion:nil];
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)dealloc {
    scrollView =nil;
    secret_code =nil;
    label_loyaltyDesc =nil;
    view_stampRedeemCard =nil;
    view_stampRedeemCardInside =nil;
    scrollView_popover =nil;
    myCollectionView =nil;
    button_stampAndRedeem =nil;
    button_stampAndRedeemPopover =nil;
    txt_secretCode =nil;
    txt_repCode =nil;
    //[super dealloc];
}

- (void)textFieldDidBeginEditing:(UITextField *)textField
{
    CGPoint scrollPoint = CGPointMake(0, textField.frame.origin.y/3.0);
    [scrollView_popover setContentOffset:scrollPoint animated:NO];
}

- (void)textFieldDidEndEditing:(UITextField *)textField
{
    [scrollView_popover setContentOffset:CGPointZero animated:NO];
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    return YES;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return total_stamp;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    UICollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"ID" forIndexPath:indexPath];
    
    //NSLog(@"stamp_icon_bg_url :: %@", stamp_icon_bg_url);
    NSURL* url_bgImg = [NSURL URLWithString:stamp_icon_bg_url];
    NSURLRequest* request2 = [NSURLRequest requestWithURL:url_bgImg];
    
    /*[NSURLConnection sendAsynchronousRequest:request2
                                       queue:[NSOperationQueue mainQueue]
                           completionHandler:^(NSURLResponse * response,
                                               NSData * data,
                                               NSError * error) {
                               if (!error)
                               {
                                   UIImage* image = [[UIImage alloc] initWithData:data];
                                   UIImageView *imgView = [[UIImageView alloc]init];
                                   [imgView setImage:image];
                                   imgView.contentMode  = UIViewContentModeScaleAspectFill;
                                   [imgView setClipsToBounds:YES];
                                   // do whatever you want with image
                                   [cell setBackgroundView:imgView];
                               }
                           }]; */
    NSURLSession *session = [NSURLSession sharedSession];
    
    [[session dataTaskWithRequest:request2 completionHandler:^(NSData * _Nullable data, NSURLResponse * _Nullable response, NSError * _Nullable error) {
        
        if (!error)
        {
            UIImage* image = [[UIImage alloc] initWithData:data];
            UIImageView *imgView = [[UIImageView alloc]init];
            [imgView setImage:image];
            imgView.contentMode  = UIViewContentModeScaleAspectFill;
            [imgView setClipsToBounds:YES];
            // do whatever you want with image
            [cell setBackgroundView:imgView];
        }
        
    }]resume];
    
    if (indexPath.row < total_gotStamped)
    {
        //NSLog(@"Stamped at row: %d",indexPath.row);
        NSURL* url_frontImg = [NSURL URLWithString:stamp_icon_url];
        NSURLRequest* request = [NSURLRequest requestWithURL:url_frontImg];
        
        /*[NSURLConnection sendAsynchronousRequest:request
                                           queue:[NSOperationQueue mainQueue]
                               completionHandler:^(NSURLResponse * response,
                                                   NSData * data,
                                                   NSError * error) {
                                   if (!error)
                                   {
                                       UIImage* image = [[UIImage alloc] initWithData:data];
                                       UIImageView *imgView = [[UIImageView alloc]init];
                                       [imgView setImage:image];
                                       imgView.contentMode  = UIViewContentModeScaleAspectFill;
                                       [imgView setClipsToBounds:YES];
                                       // do whatever you want with image
                                       [cell setBackgroundView:imgView];
                                   }
                               }]; */
        NSURLSession *session = [NSURLSession sharedSession];
        
        [[session dataTaskWithRequest:request completionHandler:^(NSData * _Nullable data, NSURLResponse * _Nullable response, NSError * _Nullable error) {
            
            if (!error)
            {
                UIImage* image = [[UIImage alloc] initWithData:data];
                UIImageView *imgView = [[UIImageView alloc]init];
                [imgView setImage:image];
                imgView.contentMode  = UIViewContentModeScaleAspectFill;
                [imgView setClipsToBounds:YES];
                // do whatever you want with image
                [cell setBackgroundView:imgView];
            }
            
        }]resume];
    }
    
    return cell;
}

- (UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout insetForSectionAtIndex:(NSInteger)section
{
    return UIEdgeInsetsMake(10, 10, 10, 10);
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    return CGSizeMake(40, 40);
}

- (IBAction)onStampAndRedeemCardClicked:(id)sender
{
    view_stampRedeemCardInside.layer.cornerRadius = 5;
    
    [txt_repCode setText:@""];
    [txt_secretCode setText:@""];
    
    [self.view addSubview:view_stampRedeemCard];
}

- (IBAction)onCancelStampAndRedeem:(id)sender
{
    [view_stampRedeemCard removeFromSuperview];
}

- (IBAction)onConfirmStampAndRedeemClicked:(id)sender
{
    if (![txt_secretCode.text isEqualToString:secret_code])
        [self displayAlert:NSLocalizedString(@"invalid_secret_code", nil)];
    else
    {
        if (total_gotStamped == total_stamp)
        {//Redeem
            currentRequest = @"redeem";
            NSString *urlAddress2 = [[NSString alloc] initWithFormat:@"%@loyalty_stamp.php?db=%@&userid=%@&udid=%@&cuid=%@&task=redeem&loyalty_id=%@&rep_code=%@&device=ios&sc=%@",appdelegate.user_apipath,appdelegate.db,appdelegate.user,appdelegate.uuid,appdelegate.customer_id,loyalty_id,[txt_repCode text], appdelegate.cs];
            [self connectToServer:urlAddress2];
        }
        else
        {//Stamp
            currentRequest = @"addStamp";
            NSString *urlAddress2 = [[NSString alloc] initWithFormat:@"%@loyalty_stamp.php?db=%@&userid=%@&udid=%@&cuid=%@&task=add&loyalty_id=%@&rep_code=%@&device=ios&sc=%@",appdelegate.user_apipath,appdelegate.db,appdelegate.user,appdelegate.uuid,appdelegate.customer_id,loyalty_id,[txt_repCode text],appdelegate.cs];
            [self connectToServer:urlAddress2];
            //NSLog(@"urlAddress2 : %@", urlAddress2);
        }
        
        [view_stampRedeemCard removeFromSuperview];
    }
}

#pragma mark Connect To API
-(void) connectToServer:(NSString *) urlPath {
    [self addLoadingView];
	NSString *urlAddress =[NSString stringWithFormat:@"%@", urlPath];
	
	/*NSURL *url = [NSURL URLWithString:urlAddress];
	
	NSURLRequest *requestObj = [NSURLRequest requestWithURL:url
												cachePolicy:NSURLRequestReloadIgnoringLocalAndRemoteCacheData
											timeoutInterval:30];
	
	NSURLConnection *theConnection=[[NSURLConnection alloc] initWithRequest:requestObj delegate:self];
	if (theConnection) {
		// Create the NSMutableData to hold the received data.
		// receivedData is an instance variable declared elsewhere.
		receivedData = [NSMutableData data];
	} else {
		// Inform the user that the connection failed.
		//[self displayAlert:@"Connection Error. Please try again later."];
		[self displayAlert:NSLocalizedString(@"MSG_CONNECTION_ERROR", nil)];
        [self removeLoadingView];
	} */
    [self getJsonResponse:urlAddress success:^(NSDictionary *responseDict) {
        
        //NSLog(@"%@",responseDict);
        [self successfunction:responseDict];
        
    } failure:^(NSError *error) {
        // release the connection, and the data object
        [self removeLoadingView];
        [self displayAlert:NSLocalizedString(@"MSG_CONNECTION_ERROR", nil)];
    }];
}

-(void)getJsonResponse : (NSString *)urlStr success : (void (^)(NSDictionary *responseDict))success failure:(void(^)(NSError* error))failure
{
    
    
    NSURLSession * session = [NSURLSession sharedSession];
    NSURL * url = [NSURL URLWithString: urlStr];
    
    
    // Asynchronously Api is hit here
    NSURLSessionDataTask * dataTask = [session dataTaskWithURL:url completionHandler:^(NSData *data, NSURLResponse *response, NSError *error)
                                       {
                                           
                                           //NSLog(@"%@",data);
                                           NSDictionary * json;
                                           if ([data length]>0) {
                                               json  = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
                                               //NSLog(@"%@",json);
                                               success(json);
                                           } else {
                                               json = nil;
                                               dispatch_async(dispatch_get_main_queue(), ^ {
                                                   [self removeLoadingView];
                                                   [self displayAlert:NSLocalizedString(@"MSG_CONNECTION_ERROR", nil)];
                                               });
                                           }
                                           
                                           
                                       }];
    
    [dataTask resume] ; // Executed First
    
    
}

-(void) successfunction:(NSDictionary *)responseDict {
    dispatch_async(dispatch_get_main_queue(), ^{
        //NSString *jsonString = [responseDict JSONRepresentation];
        [self removeLoadingView];
        
        if ([currentRequest isEqualToString:@"countGotStamped"])
        {
            NSDictionary *dictionary = responseDict;
            
            for(NSDictionary *row in dictionary)
            {
                total_gotStamped = [[row objectForKey:@"total_gotStamped"] integerValue];
            }
            [myCollectionView reloadData];
        }
        
        if ([currentRequest isEqualToString:@"addStamp"])
        {
            NSDictionary *dictionary = responseDict;
            
            for(NSDictionary *row in dictionary)
            {
                total_gotStamped = [[row objectForKey:@"total_gotStamped"] integerValue];
            }
            [myCollectionView reloadData];
        }
        
        if ([currentRequest isEqualToString:@"redeem"])
        {
            NSDictionary *dictionary = responseDict;
            
            for(NSDictionary *row in dictionary)
            {
                total_gotStamped = [[row objectForKey:@"total_gotStamped"] integerValue];
            }
            [myCollectionView reloadData];
        }
        
        if (total_gotStamped == total_stamp)
        {
            [button_stampAndRedeem setTitle:NSLocalizedString(@"redeem", nil) forState:UIControlStateNormal];
            [button_stampAndRedeemPopover setTitle:NSLocalizedString(@"redeem", nil) forState:UIControlStateNormal];
        }
        else
        {
            [button_stampAndRedeem setTitle:NSLocalizedString(@"stamp", nil) forState:UIControlStateNormal];
            [button_stampAndRedeemPopover setTitle:NSLocalizedString(@"stamp", nil) forState:UIControlStateNormal];
        }
    });
}

/*- (void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response
{
    [receivedData setLength:0];
}

- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data
{
    [receivedData appendData:data];
}

- (void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error {
    // release the connection, and the data object
	
    connection =nil;
	receivedData =nil;
	
    [self removeLoadingView];
	[self displayAlert:NSLocalizedString(@"MSG_CONNECTION_ERROR", nil)];
}

- (void)connectionDidFinishLoading:(NSURLConnection *)connection
{
	NSLog(@"Succeed! Received %d bytes of data",[receivedData length]);
    [self removeLoadingView];
    
	NSString *response = [[NSString alloc] initWithData:receivedData encoding:NSUTF8StringEncoding];
	
	if ([currentRequest isEqualToString:@"countGotStamped"])
    {
		NSDictionary *dictionary = [response JSONValue];
        
		for(NSDictionary *row in dictionary)
        {
            total_gotStamped = [[row objectForKey:@"total_gotStamped"] integerValue];
        }
        [myCollectionView reloadData];
    }
    
    if ([currentRequest isEqualToString:@"addStamp"])
    {
		NSDictionary *dictionary = [response JSONValue];
        
		for(NSDictionary *row in dictionary)
        {
            total_gotStamped = [[row objectForKey:@"total_gotStamped"] integerValue];
        }
        [myCollectionView reloadData];
	}
    
    if ([currentRequest isEqualToString:@"redeem"])
    {
		NSDictionary *dictionary = [response JSONValue];
        
		for(NSDictionary *row in dictionary)
        {
            total_gotStamped = [[row objectForKey:@"total_gotStamped"] integerValue];
        }
        [myCollectionView reloadData];
	}
    
    if (total_gotStamped == total_stamp)
    {
        [button_stampAndRedeem setTitle:NSLocalizedString(@"redeem", nil) forState:UIControlStateNormal];
        [button_stampAndRedeemPopover setTitle:NSLocalizedString(@"redeem", nil) forState:UIControlStateNormal];
    }
    else
    {
        [button_stampAndRedeem setTitle:NSLocalizedString(@"stamp", nil) forState:UIControlStateNormal];
        [button_stampAndRedeemPopover setTitle:NSLocalizedString(@"stamp", nil) forState:UIControlStateNormal];
    }
    
	connection =nil;
	response =nil;
	receivedData =nil;
} */

#pragma mark LoadingView
-(void) addLoadingView
{
	loadingView = [LoadingView loadingViewInView:self.view];
}

-(void) removeLoadingView
{
	[loadingView removeView];
}

#pragma mark displayAlert
-(void) displayAlert: (NSString *) stralert {
	NSString *str = [[NSString alloc] initWithFormat:@"%@", stralert];
	UIAlertView *alert = [[UIAlertView alloc] initWithTitle:appdelegate.iconname
													message:str
												   delegate:self
										  cancelButtonTitle:NSLocalizedString(@"done", nil)
										  otherButtonTitles:nil];
	
	[alert show];
	str =nil;
	alert =nil;
}

@end
