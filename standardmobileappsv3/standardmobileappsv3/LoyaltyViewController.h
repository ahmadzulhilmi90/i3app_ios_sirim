//
//  LoyaltyViewController.h
//  standardmobileapps
//
//  Created by Hazwan on 11/25/13.
//  Copyright (c) 2013 Maggie. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface LoyaltyViewController : UIViewController <UITableViewDelegate, UITableViewDataSource, UIViewControllerTransitioningDelegate>

@property (strong, nonatomic) NSMutableArray *arrayLoyaltyList;

@property (strong, nonatomic) IBOutlet UITableView *table_loyaltyList;
@property (strong, nonatomic) IBOutlet UIView *view_popoverMain;
@property (strong, nonatomic) IBOutlet UIView *view_popoverInside;

@property (strong, nonatomic) NSString *cont_title;

- (IBAction)closePopover:(id)sender;
- (IBAction)logIn:(id)sender;

@end
