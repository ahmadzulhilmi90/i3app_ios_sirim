//
//  LoyaltyViewController.m
//  standardmobileapps
//
//  Created by Hazwan on 11/25/13.
//  Copyright (c) 2013 Maggie. All rights reserved.
//

#import <QuartzCore/QuartzCore.h>
#import "LoyaltyViewController.h"
#import "AppDelegate.h"
#import "M3CItemCell.h"
#import "M3CItemCell.h"
#import "LoadingView.h"
#import "SBJSON.h"
#import "SettingViewController.h"
#import "LoyaltyDetailViewController.h"
#import "RootViewController.h"
#import "ViewController.h"
#import "InitialSlidingViewController.h"
#import "BTSlideInteractor.h"

@interface LoyaltyViewController ()
@property (strong, nonatomic) BTSlideInteractor *interactor;
@end

InitialSlidingViewController *modalController;

AppDelegate *appdelegate;
__strong LoadingView *loadingView;
__strong NSString *currentRequest;
//__strong NSMutableData *receivedData;

@implementation LoyaltyViewController

@synthesize arrayLoyaltyList, table_loyaltyList;
@synthesize view_popoverInside, view_popoverMain, cont_title;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        appdelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
        
        currentRequest = @"loyaltyList";
        NSString *urlAddress2 = [[NSString alloc] initWithFormat:@"%@loyalty_list.php?db=%@&userid=%@&udid=%@&cuid=%@&device=ios&sc=%@",appdelegate.user_apipath,appdelegate.db,appdelegate.user,appdelegate.uuid,appdelegate.customer_id,appdelegate.cs];
        //NSLog(@"urlAddress2 loyalty :: %@",urlAddress2);
        [self connectToServer:urlAddress2];
        
        UINib *nib = [UINib nibWithNibName:@"M3CItemCell" bundle:nil];
        
        // Register this NIB which contains the cell
        [table_loyaltyList registerNib:nib forCellReuseIdentifier:@"M3CItemCell"];
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    //self.title = appdelegate.loyaltytitle;
    
    self.navigationItem.backBarButtonItem = [[UIBarButtonItem alloc]initWithTitle:@"" style:UIBarButtonItemStylePlain target:nil action:nil];
    
    appdelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    
    self.title = cont_title;
    appdelegate.homeclicked = NO;
    
    CGRect rect = CGRectMake(0,0,20,20);
    UIGraphicsBeginImageContext( rect.size );
    [[UIImage imageNamed:@"home.png"] drawInRect:rect];
    UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    UIBarButtonItem *myHomeButton = [[UIBarButtonItem alloc] initWithImage:image style:UIBarButtonItemStyleDone target:self action:@selector(myHomeAction:)];
    
    
    NSArray *myButtonArray = [[NSArray alloc] initWithObjects:myHomeButton, nil];
    
    self.navigationItem.rightBarButtonItems = myButtonArray;
    
    arrayLoyaltyList = [[NSMutableArray alloc]init];
    
    [[view_popoverInside layer] setCornerRadius:5];
}

//Slide Menu
- (void)willAnimateRotationToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation duration:(NSTimeInterval)duration
{
    //NSLog(@"view frame=%@ bounds=%@", NSStringFromCGRect(self.view.frame), NSStringFromCGRect(self.view.bounds));
}


- (void)showModalButtonWasTouched
{
    self.interactor = [[BTSlideInteractor alloc] init];
    self.interactor.presenting = YES;
    
    //BTModalViewController *modalController = [self.storyboard instantiateViewControllerWithIdentifier:@"ModalViewController"];
    modalController = [[InitialSlidingViewController alloc] init];
    modalController.modalPresentationStyle = UIModalPresentationCustom;
    modalController.transitioningDelegate = self.interactor;
    [self.navigationController presentViewController:modalController animated:YES completion:nil];
    //[self.navigationController popToViewController:modalController animated:NO];
    //[self.navigationController pushViewController:modalController animated:YES];
}
// End Slide Menu

-(void)myHomeAction:(UIBarButtonItem *)sender{
    
    appdelegate.nextPage = @"";
    
    if ([appdelegate.listOfVerMenu count]>0) {
        if (appdelegate.homeclicked==NO) {
            appdelegate.homeclicked = YES;
            [self showModalButtonWasTouched];
        } else {
            appdelegate.homeclicked = NO;
            [modalController dismissViewControllerAnimated:YES completion:nil];
        }
    } else {
        //AppDelegate *appdelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
        //NSLog(@"View Controllers : %@",[appdelegate.navController viewControllers]);
        NSMutableArray *navigationArray = [[NSMutableArray alloc] initWithArray: appdelegate.navController.viewControllers];
        
        for (UIViewController* viewController in appdelegate.navController.viewControllers) {
            
            //This if condition checks whether the viewController's class is MyGroupViewController
            // if true that means its the MyGroupViewController (which has been pushed at some point)
            if ([viewController isKindOfClass:[RootViewController class]] ) {
                
                // Here viewController is a reference of UIViewController base class of MyGroupViewController
                // but viewController holds MyGroupViewController  object so we can type cast it here
                //ThemeViewController *themeViewController = (ThemeViewController*)viewController;
                //[self.navigationController popToViewController:themeViewController animated:YES];
                [navigationArray removeObjectAtIndex: 0];
            }
        }
        appdelegate.navController.viewControllers = navigationArray;
        for (UIViewController* viewController in appdelegate.navController.viewControllers) {
            
            //This if condition checks whether the viewController's class is MyGroupViewController
            // if true that means its the MyGroupViewController (which has been pushed at some point)
            if ([viewController isKindOfClass:[ViewController class]] ) {
                
                // Here viewController is a reference of UIViewController base class of MyGroupViewController
                // but viewController holds MyGroupViewController  object so we can type cast it here
                ViewController *themeViewController = (ViewController*)viewController;
                [appdelegate.navController popToViewController:themeViewController animated:NO];
                //[self.navigationController popToViewController:viewController animated:YES];
            }
        }
        //NSLog(@"View Controllers111 : %@",appdelegate.navController.viewControllers);
    }
    
}

-(void) viewDidDisappear:(BOOL)animated {
    
    [super viewDidDisappear:animated];
    
    appdelegate.homeclicked = NO;
    [modalController dismissViewControllerAnimated:YES completion:nil];
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark UITableView Delegation
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [arrayLoyaltyList count];
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    if(tableView == table_loyaltyList)
        return 73;
    
    return 50;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    M3CItemCell *cell = [tableView dequeueReusableCellWithIdentifier:@"M3CItemCell"];
    
    if (!cell)
        cell = [[M3CItemCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"M3CItemCell"];
    
    NSMutableDictionary *mutableDict=[[NSMutableDictionary alloc]initWithDictionary:[arrayLoyaltyList objectAtIndex:indexPath.row]];
    
    NSURL* url = [NSURL URLWithString:[mutableDict objectForKey:@"icon"]];
    NSURLRequest* request = [NSURLRequest requestWithURL:url];
    
    
    /*[NSURLConnection sendAsynchronousRequest:request
                                       queue:[NSOperationQueue mainQueue]
                           completionHandler:^(NSURLResponse * response,
                                               NSData * data,
                                               NSError * error) {
                               if (!error)
                               {
                                   UIImage* image = [[UIImage alloc] initWithData:data];
                                   // do whatever you want with image
                                   [[cell img] setImage:image];
                               }
                           }]; */
    NSURLSession *session = [NSURLSession sharedSession];
    
    [[session dataTaskWithRequest:request completionHandler:^(NSData * _Nullable data, NSURLResponse * _Nullable response, NSError * _Nullable error) {
        
        if (!error)
        {
            UIImage* image = [[UIImage alloc] initWithData:data];
            // do whatever you want with image
            [[cell img] setImage:image];
        }
        
    }]resume];
    
    [cell setLoyalty_id         :[mutableDict objectForKey:@"id"]];
    [cell setUser_id            :[mutableDict objectForKey:@"user_id"]];
    [cell setTotal_stamp        :[mutableDict objectForKey:@"total_stamp"]];
    [cell setSecret_code        :[mutableDict objectForKey:@"secret_code"]];
    //[cell setStamp_icon_bg_name :[mutableDict objectForKey:@"stamp_icon_bg_name"]];
    [cell setStamp_icon_bg_url  :[mutableDict objectForKey:@"stamp_icon_bg"]];
    //[cell setStamp_icon_name    :[mutableDict objectForKey:@"stamp_icon_name"]];
    [cell setStamp_icon_type    :[mutableDict objectForKey:@"stamp_icon_type"]];
    [cell setStamp_icon_url     :[mutableDict objectForKey:@"stamp_icon_url"]];
    [cell setBg_img    :[mutableDict objectForKey:@"bg_img"]];
    
    [cell setTableView:table_loyaltyList];
    [cell setController:self];
    [[cell titleLabel] setText:[mutableDict objectForKey:(@"title")]];
    [[cell descLabel] setText:[mutableDict objectForKey:(@"short_description")]];
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSString *hasLoggedInFB = [defaults objectForKey:@"hasLoggedInFB"];
    NSString *hasLoggedInMyApp = [defaults objectForKey:@"hasLoggedInMyApp"];
    
    if ([hasLoggedInFB isEqualToString:@"YES"] || [hasLoggedInMyApp isEqualToString:@"YES"])
    {
        [tableView deselectRowAtIndexPath:[tableView indexPathForSelectedRow] animated:NO];
        
        M3CItemCell *cell = (M3CItemCell *)[tableView cellForRowAtIndexPath:indexPath];
        
        NSString *nibFileName;
        if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone) {
            nibFileName = @"LoyaltyDetailViewController";
        } else {
            nibFileName = @"LoyaltyDetailViewController_ipad";
        }
        LoyaltyDetailViewController *s = [[LoyaltyDetailViewController alloc] initWithNibName:nibFileName bundle:[NSBundle mainBundle]];
        [s setLoyalty_id:[cell loyalty_id]];
        [s setLoyalty_title:[[cell titleLabel] text]];
        [s setTotal_stamp:[[cell total_stamp] integerValue]];
        [s setSecret_code:[cell secret_code]];
        [s setLoyalty_desc: [[cell descLabel] text]];
        
        //[s setStamp_icon_bg_name:[cell stamp_icon_bg_name]];
        [s setStamp_icon_bg_url:[cell stamp_icon_bg_url]];
        //[s setStamp_icon_name:[cell stamp_icon_name]];
        [s setStamp_icon_type:[cell stamp_icon_type]];
        [s setStamp_icon_url:[cell stamp_icon_url]];
        [s setBg_img:[cell bg_img]];
       
        
        s.conttitle = cont_title;
        [appdelegate.navController pushViewController:s animated:NO];
    }
    else
        [self.view addSubview:view_popoverMain];
}

#pragma mark Connect To API
-(void) connectToServer:(NSString *) urlPath {
    [self addLoadingView];
	NSString *urlAddress =[NSString stringWithFormat:@"%@", urlPath];
	
	/*NSURL *url = [NSURL URLWithString:urlAddress];
	
	NSURLRequest *requestObj = [NSURLRequest requestWithURL:url
												cachePolicy:NSURLRequestReloadIgnoringLocalAndRemoteCacheData
											timeoutInterval:30];
	
	NSURLConnection *theConnection=[[NSURLConnection alloc] initWithRequest:requestObj delegate:self];
	if (theConnection) {
		// Create the NSMutableData to hold the received data.
		// receivedData is an instance variable declared elsewhere.
		receivedData = [NSMutableData data];
	} else {
		// Inform the user that the connection failed.
		//[self displayAlert:@"Connection Error. Please try again later."];
		[self displayAlert:NSLocalizedString(@"MSG_CONNECTION_ERROR", nil)];
        [self removeLoadingView];
	} */
    [self getJsonResponse:urlAddress success:^(NSDictionary *responseDict) {
        
        //NSLog(@"%@",responseDict);
        [self successfunction:responseDict];
        
    } failure:^(NSError *error) {
        // release the connection, and the data object
        
        [self removeLoadingView];
        [self displayAlert:NSLocalizedString(@"MSG_CONNECTION_ERROR", nil)];
    }];
}

-(void)getJsonResponse : (NSString *)urlStr success : (void (^)(NSDictionary *responseDict))success failure:(void(^)(NSError* error))failure
{
    
    
    NSURLSession * session = [NSURLSession sharedSession];
    NSURL * url = [NSURL URLWithString: urlStr];
    
    
    // Asynchronously Api is hit here
    NSURLSessionDataTask * dataTask = [session dataTaskWithURL:url completionHandler:^(NSData *data, NSURLResponse *response, NSError *error)
                                       {
                                           
                                           //NSLog(@"%@",data);
                                           NSDictionary * json;
                                           if ([data length]>0) {
                                               json  = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
                                               //NSLog(@"%@",json);
                                               success(json);
                                           } else {
                                               json = nil;
                                               dispatch_async(dispatch_get_main_queue(), ^ {
                                                   [self removeLoadingView];
                                                   [self displayAlert:NSLocalizedString(@"MSG_CONNECTION_ERROR", nil)];
                                               });
                                           }
                                           
                                           
                                       }];
    
    [dataTask resume] ; // Executed First
    
    
}

-(void) successfunction:(NSDictionary *)responseDict {
    dispatch_async(dispatch_get_main_queue(), ^{
        //NSString *jsonString = [responseDict JSONRepresentation];
        [self removeLoadingView];
        
        if ([currentRequest isEqualToString:@"loyaltyList"])
        {
            NSDictionary *dictionary = responseDict;
            
            arrayLoyaltyList = [[NSMutableArray alloc] init];
            
            for(NSDictionary *row in dictionary)
            {
                NSMutableDictionary *mutableDic = [NSMutableDictionary dictionaryWithObjectsAndKeys:
                                                   [row objectForKey:@"id"],@"id",
                                                   [row objectForKey:@"user_id"],@"user_id",
                                                   [row objectForKey:@"title"],@"title",
                                                   [row objectForKey:@"short_description"],@"short_description",
                                                   [row objectForKey:@"secret_code"],@"secret_code",
                                                   [row objectForKey:@"icon"],@"icon",
                                                   [row objectForKey:@"start_date"],@"start_date",
                                                   [row objectForKey:@"end_date"],@"end_date",
                                                   [row objectForKey:@"total_stamp"],@"total_stamp",
                                                   [row objectForKey:@"status"],@"status",
                                                   [row objectForKey:@"date_updated"],@"last_updated",
                                                   [row objectForKey:@"stamp_icon_type"],@"stamp_icon_type",
                                                   [row objectForKey:@"stamp_icon"],@"stamp_icon_url",
                                                   [row objectForKey:@"stamp_icon_bg"],@"stamp_icon_bg",
                                                   //[row objectForKey:@"stamp_icon_name"],@"stamp_icon_name",
                                                   //[row objectForKey:@"stamp_icon_bg_name"],@"stamp_icon_bg_name",
                                                   [row objectForKey:@"bg_img"],@"bg_img",
                                                   nil];
                
                [arrayLoyaltyList addObject:mutableDic];
            }
            [table_loyaltyList reloadData];
            //NSLog(@"arrayLoyaltyList :%@", arrayLoyaltyList);
            
            if ([arrayLoyaltyList count]==0)
                [self displayAlert:NSLocalizedString(@"record_not_found", nil)];
        }
    });
    
}

/*- (void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response
{
    [receivedData setLength:0];
}

- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data
{
    [receivedData appendData:data];
}

- (void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error {
    // release the connection, and the data object
	
    connection =nil;
	receivedData =nil;
	
    [self removeLoadingView];
	[self displayAlert:NSLocalizedString(@"MSG_CONNECTION_ERROR", nil)];
}

- (void)connectionDidFinishLoading:(NSURLConnection *)connection
{
	//NSLog(@"Succeed! Received %d bytes of data",[receivedData length]);
    [self removeLoadingView];
    
	NSString *response = [[NSString alloc] initWithData:receivedData encoding:NSUTF8StringEncoding];
	
	if ([currentRequest isEqualToString:@"loyaltyList"])
    {
		NSDictionary *dictionary = [response JSONValue];

		arrayLoyaltyList = [[NSMutableArray alloc] init];
        
		for(NSDictionary *row in dictionary)
        {
            NSMutableDictionary *mutableDic = [NSMutableDictionary dictionaryWithObjectsAndKeys:
                                               [row objectForKey:@"id"],@"id",
                                               [row objectForKey:@"user_id"],@"user_id",
                                               [row objectForKey:@"title"],@"title",
                                               [row objectForKey:@"short_description"],@"short_description",
                                               [row objectForKey:@"secret_code"],@"secret_code",
                                               [row objectForKey:@"icon"],@"icon",
                                               [row objectForKey:@"start_date"],@"start_date",
                                               [row objectForKey:@"end_date"],@"end_date",
                                               [row objectForKey:@"total_stamp"],@"total_stamp",
                                               [row objectForKey:@"status"],@"status",
                                               [row objectForKey:@"date_updated"],@"last_updated",
                                               [row objectForKey:@"stamp_icon_type"],@"stamp_icon_type",
                                               [row objectForKey:@"stamp_icon"],@"stamp_icon_url",
                                               [row objectForKey:@"stamp_icon_bg"],@"stamp_icon_bg",
                                               //[row objectForKey:@"stamp_icon_name"],@"stamp_icon_name",
                                               //[row objectForKey:@"stamp_icon_bg_name"],@"stamp_icon_bg_name",
                                               [row objectForKey:@"bg_img"],@"bg_img",
                                               nil];

			[arrayLoyaltyList addObject:mutableDic];
        }
        [table_loyaltyList reloadData];
        //NSLog(@"arrayLoyaltyList :%@", arrayLoyaltyList);
        
        if ([arrayLoyaltyList count]==0)
            [self displayAlert:NSLocalizedString(@"record_not_found", nil)];
	}
	connection =nil;
	response =nil;
	receivedData =nil;
} */

#pragma mark LoadingView
-(void) addLoadingView
{
	loadingView = [LoadingView loadingViewInView:self.view];
}

-(void) removeLoadingView
{
	[loadingView removeView];
}

#pragma mark displayAlert
-(void) displayAlert: (NSString *) stralert {
	NSString *str = [[NSString alloc] initWithFormat:@"%@", stralert];
	UIAlertView *alert = [[UIAlertView alloc] initWithTitle:appdelegate.iconname
													message:str
												   delegate:self
										  cancelButtonTitle:NSLocalizedString(@"done", nil)
										  otherButtonTitles:nil];
	
	[alert show];
	str =nil;
	alert =nil;
}

- (void)dealloc {
    table_loyaltyList =nil;
    view_popoverMain =nil;
    view_popoverInside =nil;
    //[super dealloc];
}
- (IBAction)closePopover:(id)sender
{
    [view_popoverMain removeFromSuperview];
}

- (IBAction)logIn:(id)sender
{
    NSString *nibFileName;
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone) {
        nibFileName = @"SettingViewController";
    } else {
        nibFileName = @"SettingViewController_ipad";
    }
    SettingViewController *s = [[SettingViewController alloc] initWithNibName:nibFileName bundle:nil];
    appdelegate.shouldReturnToPrevPage = YES;
    //LoyaltyViewController *l = [[LoyaltyViewController alloc] initWithNibName:@"LoyaltyViewController" bundle:nil];
    appdelegate.originVC = self;
    [appdelegate.navController pushViewController:s animated:NO];
    
    [view_popoverMain removeFromSuperview];
}
@end
