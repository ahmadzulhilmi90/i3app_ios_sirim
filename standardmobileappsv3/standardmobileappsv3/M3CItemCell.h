//
//  MSSItemCell.h
//  echoplus
//
//  Created by Hazwan on 7/17/13.
//
//
#import <UIKit/UIKit.h>

@interface M3CItemCell : UITableViewCell

@property (unsafe_unretained, nonatomic) id controller;
@property (unsafe_unretained, nonatomic) UITableView *tableView;

@property (unsafe_unretained, nonatomic) IBOutlet UILabel *titleLabel;
@property (unsafe_unretained, nonatomic) IBOutlet UILabel *descLabel;
@property (unsafe_unretained, nonatomic) IBOutlet UIImageView *img;

@property (strong, nonatomic) NSString *user_id;
@property (strong, nonatomic) NSString *loyalty_id;
@property (strong, nonatomic) NSString *secret_code;
@property (strong, nonatomic) NSString *total_stamp;

@property (strong, nonatomic) NSString *stamp_icon_type;
@property (strong, nonatomic) NSString *stamp_icon_name;
@property (strong, nonatomic) NSString *stamp_icon_bg_name;
@property (strong, nonatomic) NSString *stamp_icon_url;
@property (strong, nonatomic) NSString *stamp_icon_bg_url;
@property (strong, nonatomic) NSString *bg_img;

@end
