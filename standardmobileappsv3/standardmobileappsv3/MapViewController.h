//
//  MapViewController.h
//  standardmobileapps
//
//  Created by Maggie on 9/23/13.
//  Copyright (c) 2013 M3Online. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MapKit/MapKit.h>
#import "LoadingView.h"

@interface MapViewController : UIViewController<UIViewControllerTransitioningDelegate>{
    
    MKMapView *mapView;
    float latitude;
    float longitude;
    
    LoadingView *loadingView;
    
}

@property (nonatomic, assign) float latitude;
@property (nonatomic, assign) float longitude;

@property (strong, nonatomic) NSString *cont_title;

-(void) addLoadingView;
-(void) removeLoadingView;

@end
