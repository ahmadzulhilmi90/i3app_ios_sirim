//
//  ChangePasswordViewController.h
//  standardmobileappsv3
//
//  Created by M3Online on 10/24/14.
//  Copyright (c) 2014 M3Online. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "WEPopoverController.h"
#import "LoadingView.h"

@interface NotesViewController : UIViewController <UITextViewDelegate, UIViewControllerTransitioningDelegate> {
    CGFloat animatedDistance;
    
    UITextView *textview;
    
    LoadingView *loadingView;
}

@property (nonatomic, strong) IBOutlet UITextView *textview;

@property (retain, nonatomic) IBOutlet UIView *view_popoverMain;
@property (retain, nonatomic) IBOutlet UIView *view_notes;

@property (strong, nonatomic) NSString *cont_title;

- (IBAction)logIn:(id)sender;
- (IBAction)closeLoginPopup:(id)sender;

@end
