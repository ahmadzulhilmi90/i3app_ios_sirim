//
//  ChangePasswordViewController.m
//  standardmobileappsv3
//
//  Created by M3Online on 10/24/14.
//  Copyright (c) 2014 M3Online. All rights reserved.
//

#import "NotesViewController.h"
#import "AppDelegate.h"
#import "LoadingView.h"
#import "InitialSlidingViewController.h"
#import "BTSlideInteractor.h"
#import "RootViewController.h"
#import "ViewController.h"

@interface NotesViewController ()
@property (strong, nonatomic) BTSlideInteractor *interactor;
@end

@implementation NotesViewController

@synthesize textview, view_popoverMain, view_notes, cont_title;

InitialSlidingViewController *modalController;

AppDelegate *appdelegate;
__strong LoadingView *loadingView;

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
    //////NSLog(@">>> Entering *** %s <<<", __PRETTY_FUNCTION__);
    [self hideKeyPad];
}

- (IBAction) callHideKeyBoard:(id)sender{
    //////NSLog(@"testing...");
    [self hideKeyPad];
}

-(void) hideKeyPad {
    //////NSLog(@"#### %s",__PRETTY_FUNCTION__);
    [textview resignFirstResponder];
    [textview setFrame:CGRectMake(textview.frame.origin.x, textview.frame.origin.y, textview.frame.size.width, textview.frame.size.height+250)];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    self.title = cont_title;
    
    [self.view addSubview:view_notes];
    [self.view addSubview:view_popoverMain];
    [view_popoverMain setHidden:YES];
    [view_notes setHidden:YES];
    
    appdelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    
    if (![appdelegate.backgroundT isEqualToString:@""]) {
        
        NSString *filePath = appdelegate.backgroundT;
        
        NSRange end = [filePath rangeOfString:@"/" options:NSBackwardsSearch];
        NSString *shortString;
        if (end.location != NSNotFound) {
            shortString =[filePath substringWithRange:NSMakeRange(end.location+1, filePath.length-(end.location+1))];
        } else {
            shortString = filePath;
        }
        
        BOOL success;
        
        // Create a FileManager object, we will use this to check the status
        // of the database and to copy it over if required
        NSFileManager *fileManager = [NSFileManager defaultManager];
        
        // Check if the database has already been created in the users filesystem
        success = [fileManager fileExistsAtPath:[[[NSBundle mainBundle] resourcePath] stringByAppendingPathComponent:shortString]];
        
        UIImageView *imgV = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height)];
        //[self addSubview:imgV];
        
        //__block UIActivityIndicatorView *activityIndicator;
        __weak UIImageView *weakImageView = imgV;
        NSString *imagePath;
        if(success) {
            imagePath = [[[NSBundle mainBundle] resourcePath] stringByAppendingPathComponent:shortString];
            [weakImageView setImage:[UIImage imageNamed:shortString]];
            
            //NSLog(@"shortStringimagePath : %@", imagePath);
            imgV.contentMode  = UIViewContentModeScaleAspectFit;
            [imgV setClipsToBounds:YES];
            
            UIGraphicsBeginImageContext(self.view.frame.size);
            CGRect rect=CGRectMake(0,-64,self.view.bounds.size.width,self.view.bounds.size.height+64);
            [imgV.image drawInRect:rect];
            UIImage *image1 = UIGraphicsGetImageFromCurrentImageContext();
            UIGraphicsEndImageContext();
            self.view.backgroundColor = [UIColor colorWithPatternImage:image1];
        } else {
            imagePath = filePath;
            NSURL *imageURL = [NSURL URLWithString:imagePath];
            NSData *imageData = [NSData dataWithContentsOfURL:imageURL];
            //self.backgroundColor = [UIColor redColor];
            UIGraphicsBeginImageContext(self.view.frame.size);
            CGRect rect=CGRectMake(0,-64,self.view.bounds.size.width,self.view.bounds.size.height+64);
            [[UIImage imageWithData:imageData] drawInRect:rect];
            UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
            UIGraphicsEndImageContext();
            self.view.backgroundColor = [UIColor colorWithPatternImage:image];
        }
    } else {
        self.view.backgroundColor = [UIColor clearColor];
    }
    
    appdelegate.homeclicked = NO;
    
    self.navigationItem.backBarButtonItem = [[UIBarButtonItem alloc]initWithTitle:@"" style:UIBarButtonItemStylePlain target:nil action:nil];
    
    CGRect rect = CGRectMake(0,0,20,20);
    UIGraphicsBeginImageContext( rect.size );
    [[UIImage imageNamed:@"home.png"] drawInRect:rect];
    UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    UIBarButtonItem *myHomeButton = [[UIBarButtonItem alloc] initWithImage:image style:UIBarButtonItemStyleDone target:self action:@selector(myHomeAction:)];
    
    
    NSArray *myButtonArray = [[NSArray alloc] initWithObjects:myHomeButton, nil];
    
    self.navigationItem.rightBarButtonItems = myButtonArray;
    
    [self addDoneToolBarToKeyboard:textview];
    
    //textview.layer.cornerRadius = 8.0f;
    //textview.layer.masksToBounds = YES;
    textview.layer.borderColor = [[UIColor blackColor] CGColor];
    textview.layer.borderWidth = 0.5f;
}

-(void) viewDidAppear:(BOOL)animated {
    
    [super viewDidAppear:animated];
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSString *hasLoggedInFB = [defaults objectForKey:@"hasLoggedInFB"];
    NSString *hasLoggedInMyApp = [defaults objectForKey:@"hasLoggedInMyApp"];
    
    if ([hasLoggedInFB isEqualToString:@"YES"] || [hasLoggedInMyApp isEqualToString:@"YES"])
    {
        [view_notes setHidden:NO];
        [view_popoverMain setHidden:YES];
        [self displaycontent];
    }
    else {
        [view_popoverMain setHidden:NO];
        [view_notes setHidden:YES];
    }
    
}

-(void) displaycontent {
    
    sqlite3 *database;
    
    if(sqlite3_open([appdelegate.databasePath UTF8String], &database) == SQLITE_OK) {
        // Setup the SQL Statement and compile it for faster access
        
        NSString *query=[NSString stringWithFormat:@"create table IF NOT EXISTS notes(customer_id integer, notes text)"];
        
        sqlite3_stmt *createStmt;
        if (sqlite3_prepare_v2(database, [query UTF8String], -1, &createStmt, NULL) != SQLITE_OK) {
            //return NO;
        }
        sqlite3_exec(database, [query UTF8String], NULL, NULL, NULL);
        
        NSString *sqlItem;
        NSString *notes=@"";
        
        sqlItem = [[NSString alloc] initWithFormat:@"SELECT * FROM notes WHERE customer_id ='%@' ", appdelegate.customer_id];
        
        const char *sqlStatement = [sqlItem UTF8String];
        sqlite3_stmt *compiledStatement;
        if(sqlite3_prepare_v2(database, sqlStatement, -1, &compiledStatement, NULL) == SQLITE_OK) {
            
            while(sqlite3_step(compiledStatement) == SQLITE_ROW) {
                
                notes = [NSString stringWithUTF8String:(char *)sqlite3_column_text(compiledStatement, 1)];

            }
        }
        sqlite3_finalize(compiledStatement);
        
        [textview setText:notes];
        
    }
    sqlite3_close(database);

}

//Slide Menu
- (void)willAnimateRotationToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation duration:(NSTimeInterval)duration
{
    //NSLog(@"view frame=%@ bounds=%@", NSStringFromCGRect(self.view.frame), NSStringFromCGRect(self.view.bounds));
}


- (void)showModalButtonWasTouched
{
    self.interactor = [[BTSlideInteractor alloc] init];
    self.interactor.presenting = YES;
    
    //BTModalViewController *modalController = [self.storyboard instantiateViewControllerWithIdentifier:@"ModalViewController"];
    modalController = [[InitialSlidingViewController alloc] init];
    modalController.modalPresentationStyle = UIModalPresentationCustom;
    modalController.transitioningDelegate = self.interactor;
    [self.navigationController presentViewController:modalController animated:YES completion:nil];
    //[self.navigationController popToViewController:modalController animated:NO];
    //[self.navigationController pushViewController:modalController animated:YES];
}
// End Slide Menu

-(void)myHomeAction:(UIBarButtonItem *)sender{
    
    appdelegate.nextPage = @"";
    
    if ([appdelegate.listOfVerMenu count]>0) {
        if (appdelegate.homeclicked==NO) {
            appdelegate.homeclicked = YES;
            [self showModalButtonWasTouched];
        } else {
            appdelegate.homeclicked = NO;
            [modalController dismissViewControllerAnimated:YES completion:nil];
        }
    } else {
        //AppDelegate *appdelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
        //NSLog(@"View Controllers : %@",[appdelegate.navController viewControllers]);
        NSMutableArray *navigationArray = [[NSMutableArray alloc] initWithArray: appdelegate.navController.viewControllers];
        
        for (UIViewController* viewController in appdelegate.navController.viewControllers) {
            
            //This if condition checks whether the viewController's class is MyGroupViewController
            // if true that means its the MyGroupViewController (which has been pushed at some point)
            if ([viewController isKindOfClass:[RootViewController class]] ) {
                
                // Here viewController is a reference of UIViewController base class of MyGroupViewController
                // but viewController holds MyGroupViewController  object so we can type cast it here
                //ThemeViewController *themeViewController = (ThemeViewController*)viewController;
                //[self.navigationController popToViewController:themeViewController animated:YES];
                [navigationArray removeObjectAtIndex: 0];
            }
        }
        appdelegate.navController.viewControllers = navigationArray;
        for (UIViewController* viewController in appdelegate.navController.viewControllers) {
            
            //This if condition checks whether the viewController's class is MyGroupViewController
            // if true that means its the MyGroupViewController (which has been pushed at some point)
            if ([viewController isKindOfClass:[ViewController class]] ) {
                
                // Here viewController is a reference of UIViewController base class of MyGroupViewController
                // but viewController holds MyGroupViewController  object so we can type cast it here
                ViewController *themeViewController = (ViewController*)viewController;
                [appdelegate.navController popToViewController:themeViewController animated:NO];
                //[self.navigationController popToViewController:viewController animated:YES];
            }
        }
        //NSLog(@"View Controllers111 : %@",appdelegate.navController.viewControllers);
    }
    
}

- (IBAction)logIn:(id)sender
{
    NSString *nibFileName;
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone) {
        nibFileName = @"SettingViewController";
    } else {
        nibFileName = @"SettingViewController_ipad";
    }
    SettingViewController *settingViewController = [[SettingViewController alloc] initWithNibName:nibFileName bundle:nil];
    appdelegate.shouldReturnToPrevPage = YES;
    appdelegate.originVC = self;
    [appdelegate.navController pushViewController:settingViewController animated:NO];
    
    [view_popoverMain removeFromSuperview];
}

- (IBAction)closeLoginPopup:(id)sender
{
    [view_popoverMain removeFromSuperview];
}

-(NSString *) urlEncode:(NSString *) theStr {
    NSString *encoded = (NSString *)CFBridgingRelease(CFURLCreateStringByAddingPercentEscapes(
                                                                                              NULL,
                                                                                              (CFStringRef)theStr,
                                                                                              NULL,
                                                                                              (CFStringRef)@"!*'\"();:@&=+$,/?%#[]% ",
                                                                                              //																		NSUnicodeStringEncoding );
                                                                                              kCFStringEncodingUTF8 ));
    return encoded;
}

- (IBAction)save:(id)sender
{
    //NSLog(@"save");
    sqlite3 *database;
    
    if(sqlite3_open([appdelegate.databasePath UTF8String], &database) == SQLITE_OK) {
        // Setup the SQL Statement and compile it for faster access
        
        int rows=0;
        NSString *theme_object;
        theme_object = [[NSString alloc] initWithFormat:@"SELECT COUNT(*) rec_count FROM notes WHERE customer_id='%@' ", appdelegate.customer_id];
        
        const char *sqlStatement1 = [theme_object UTF8String];
        sqlite3_stmt *compiledStatement;
        if(sqlite3_prepare_v2(database, sqlStatement1, -1, &compiledStatement, NULL) == SQLITE_OK) {
            
            if (sqlite3_step(compiledStatement) == SQLITE_ERROR) {
                NSAssert1(0,@"Error when counting rows  %s",sqlite3_errmsg(database));
            } else {
                rows = sqlite3_column_int(compiledStatement, 0);
                //NSLog(@"SQLite Rows: %i", rows);
            }
            
            if (rows > 0) {
                //NSLog(@"database:::::::update conferences");
                static sqlite3_stmt *compiledupdateStatement;
                sqlite3_exec(database, [[NSString stringWithFormat:@"UPDATE notes SET notes='%@' WHERE customer_id='%@' ", textview.text, appdelegate.customer_id] UTF8String], NULL, NULL, NULL);
                //NSLog(@"compiledupdateStatement: %@", compiledupdateStatement);
                sqlite3_finalize(compiledupdateStatement);
            } else {
                //NSLog(@"database:::::::insert into conferences");
                static sqlite3_stmt *compiledinsertStatement;
                sqlite3_exec(database, [[NSString stringWithFormat:@"INSERT INTO notes (customer_id, notes) VALUES ('%@', '%@') ", appdelegate.customer_id, textview.text] UTF8String], NULL, NULL, NULL);
                //NSLog(@"compiledinsertStatement: %@", compiledinsertStatement);
                sqlite3_finalize(compiledinsertStatement);
            }
            
            [self displayAlert:NSLocalizedString(@"notes_saved", nil)];
            
        }
        // Release the compiled statement from memory
        sqlite3_finalize(compiledStatement);
    }
    sqlite3_close(database);
}

#pragma mark LoadingView
-(void) addLoadingView
{
    loadingView = [LoadingView loadingViewInView:self.view];
}

-(void) removeLoadingView
{
    [loadingView removeView];
}

#pragma mark displayAlert
-(void) displayAlert: (NSString *) stralert {
    NSString *str = [[NSString alloc] initWithFormat:@"%@", stralert];
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:appdelegate.iconname
                                                    message:str
                                                   delegate:self
                                          cancelButtonTitle:NSLocalizedString(@"done", nil)
                                          otherButtonTitles:nil];
    
    [alert show];
    str =nil;
    alert =nil;
}

-(void)addDoneToolBarToKeyboard:(UITextView *)textView
{
    UIToolbar* doneToolbar = [[UIToolbar alloc]initWithFrame:CGRectMake(0, 0, 320, 50)];
    doneToolbar.barStyle = UIBarStyleDefault;
    doneToolbar.items = [NSArray arrayWithObjects:
                         [[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil],
                         [[UIBarButtonItem alloc]initWithTitle:NSLocalizedString(@"done", nil) style:UIBarButtonItemStyleDone target:self action:@selector(hideKeyPad)],
                         nil];
    [doneToolbar sizeToFit];
    textView.inputAccessoryView = doneToolbar;
}

-(void)textViewDidBeginEditing:(UITextView *)textView
{
    [textView setFrame:CGRectMake(textView.frame.origin.x, textView.frame.origin.y, textView.frame.size.width, textView.frame.size.height-250)];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)dealloc {
    //[super dealloc];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
