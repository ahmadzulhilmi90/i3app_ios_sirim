//
//  EventListViewController.h
//  standardmobileapps
//
//  Created by Hazwan on 12/17/13.
//  Copyright (c) 2013 Maggie. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "WEPopoverController.h"
#import <MessageUI/MessageUI.h>

@interface RsvpViewController : UIViewController <UITableViewDelegate, UITableViewDataSource, UIScrollViewDelegate, UITextFieldDelegate, UITextViewDelegate, UITableViewDelegate, WEPopoverControllerDelegate, UIPopoverControllerDelegate, UIAlertViewDelegate, UIGestureRecognizerDelegate, UIViewControllerTransitioningDelegate> {
    
    int labelheight;
    
    UIScrollView *scrollviewAdd;
    UIScrollView *scrollviewAdd1;

    NSMutableArray *listOfContent;
    NSMutableArray *listOfpax;
    UITableView *paxTableView;
    
    UIViewController *paxContent;
    
    WEPopoverController *paxPopoverController;
    
    UITextField *tf;
    
}

@property (retain, nonatomic) IBOutlet UIScrollView *scrollviewAdd;
@property (retain, nonatomic) IBOutlet UIScrollView *scrollviewAdd1;

@property (nonatomic, retain) NSMutableArray *listOfContent;
@property (nonatomic, retain) NSMutableArray *listOfpax;

@property (nonatomic, retain) WEPopoverController *paxPopoverController;

@property (retain, nonatomic) IBOutlet UIView *view_eventAdd;
@property (retain, nonatomic) IBOutlet UIView *view_eventAdd1;

@property (retain, nonatomic) IBOutlet UIView *view_popoverMain;

@property (strong, nonatomic) UIButton *btnYes;
@property (strong, nonatomic) UIButton *btnNo;
@property (strong, nonatomic) UIButton *btnMaybe;

@property (strong, nonatomic) UIButton *btnYesConf;
@property (strong, nonatomic) UIButton *btnLaterConf;

@property (strong, nonatomic) UIButton *btnYesRem;
@property (strong, nonatomic) UIButton *btnNoRem;

@property (strong, nonatomic) UIButton *buttonsubmit;

@property (strong, nonatomic) NSString *cont_title;

- (IBAction)logIn:(id)sender;
- (IBAction)closeLoginPopup:(id)sender;

-(void) connectToServer:(NSString *) urlPath;

@end
