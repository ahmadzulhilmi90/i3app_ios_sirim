//
//  EventListViewController.m
//  standardmobileapps
//
//  Created by Hazwan on 12/17/13.
//  Copyright (c) 2013 Maggie. All rights reserved.
//

#import "RsvpViewController.h"
#import "AppDelegate.h"
#import "LoadingView.h"
#import "SBJSON.h"
#import "MapViewController.h"
#import <QuartzCore/QuartzCore.h>
#import <SystemConfiguration/SystemConfiguration.h>
#import "NSString+HTML.h"
#import "RootViewController.h"
#import "ViewController.h"
#import "InitialSlidingViewController.h"
#import "BTSlideInteractor.h"
#import "Reachability.h"

@interface RsvpViewController () {
    NSMutableArray      *sectionTitleArray;
    NSMutableDictionary *sectionContentDict;
    NSMutableArray      *arrayForBool;
    NSMutableArray *arSelectedRows, *textfieldSelectedRows, *prodSelectedRows;
    NSArray *strings;
}

@property (strong, nonatomic) BTSlideInteractor *interactor;
@end

@implementation RsvpViewController
LoadingView *loadingView;

InitialSlidingViewController *modalController;

AppDelegate *appdelegate;
//NSMutableData *receivedData;
CGFloat scrollViewHeight, scrollViewHeight1;

NSString *currentRequest;
NSString *max_guesstotal;
NSString *confirmStatus;
NSString *reminderStatus;

//@synthesize eventdet,lblevent,lbldate,lbldateval,lbltime,lbltimeval,lbladdress,lbladdressval,lblcutoffdate, lblcutoffdateval, lblconfirmation;
@synthesize scrollviewAdd, view_eventAdd, view_eventAdd1, scrollviewAdd1;
@synthesize listOfContent, listOfpax, paxPopoverController;
@synthesize view_popoverMain, btnYes, btnNo, btnMaybe, btnYesConf, btnLaterConf, btnYesRem, btnNoRem, buttonsubmit, cont_title;
//@synthesize dqty;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        appdelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    }
    return self;
}

- (UIColor *) colorWithHexString:(NSString *)hexString {
    NSString *colorString = [[hexString stringByReplacingOccurrencesOfString: @"#" withString: @""] uppercaseString];
    CGFloat alpha, red, blue, green;
    alpha = 1.0f;
    red   = [self colorComponentFrom: colorString start: 0 length: 2];
    green = [self colorComponentFrom: colorString start: 2 length: 2];
    blue  = [self colorComponentFrom: colorString start: 4 length: 2];
    return [UIColor colorWithRed: red green: green blue: blue alpha: alpha];
}

- (CGFloat) colorComponentFrom:(NSString *)string start:(NSUInteger)start length:(NSUInteger)length {
    NSString *substring = [string substringWithRange: NSMakeRange(start, length)];
    NSString *fullHex = length == 2 ? substring : [NSString stringWithFormat: @"%@%@", substring, substring];
    unsigned hexComponent;
    [[NSScanner scannerWithString: fullHex] scanHexInt: &hexComponent];
    return hexComponent / 255.0;
}

-(void)viewWillDisappear:(BOOL)animated {
    [paxPopoverController dismissPopoverAnimated:NO];
    //[branchPopoverController release];
    paxPopoverController = nil;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.

    scrollviewAdd = [[UIScrollView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height)];
    scrollviewAdd.backgroundColor = [UIColor clearColor];
    scrollviewAdd.scrollEnabled = YES;
    scrollviewAdd1 = [[UIScrollView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height)];
    scrollviewAdd1.backgroundColor = [UIColor clearColor];
    scrollviewAdd1.scrollEnabled = YES;
    
    self.title = cont_title;
    appdelegate.homeclicked = NO;
    
    self.navigationItem.backBarButtonItem = [[UIBarButtonItem alloc]initWithTitle:@"" style:UIBarButtonItemStylePlain target:nil action:nil];
    CGRect rect = CGRectMake(0,0,20,20);
    UIGraphicsBeginImageContext( rect.size );
    [[UIImage imageNamed:@"home.png"] drawInRect:rect];
    UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    UIBarButtonItem *myHomeButton = [[UIBarButtonItem alloc] initWithImage:image style:UIBarButtonItemStyleDone target:self action:@selector(myHomeAction:)];
    
    
    NSArray *myButtonArray = [[NSArray alloc] initWithObjects:myHomeButton, nil];
    
    self.navigationItem.rightBarButtonItems = myButtonArray;
    
    paxTableView = [[UITableView alloc] init];
	paxTableView.delegate = self;
	paxTableView.dataSource = self;
    
    tf = [[UITextField alloc] init];
    
    [view_eventAdd setAutoresizingMask:UIViewAutoresizingFlexibleWidth|
     UIViewAutoresizingFlexibleHeight];
    
    [view_eventAdd setFrame:CGRectMake(0, 10, self.view.frame.size.width, self.view.frame.size.height-10)];
    view_eventAdd.backgroundColor = [UIColor clearColor];
    [self.view addSubview:view_eventAdd];
    [view_eventAdd addSubview:scrollviewAdd];
    [view_eventAdd setHidden:YES];
    
    [view_eventAdd1 setAutoresizingMask:UIViewAutoresizingFlexibleWidth|
     UIViewAutoresizingFlexibleHeight];
    
    [view_eventAdd1 setFrame:CGRectMake(0, 10, self.view.frame.size.width, self.view.frame.size.height-10)];
    view_eventAdd1.backgroundColor = [UIColor clearColor];
    [self.view addSubview:view_eventAdd1];
    [view_eventAdd1 addSubview:scrollviewAdd1];
    [view_eventAdd1 setHidden:YES];
    
}

//Slide Menu
- (void)willAnimateRotationToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation duration:(NSTimeInterval)duration
{
    //NSLog(@"view frame=%@ bounds=%@", NSStringFromCGRect(self.view.frame), NSStringFromCGRect(self.view.bounds));
}


- (void)showModalButtonWasTouched
{
    self.interactor = [[BTSlideInteractor alloc] init];
    self.interactor.presenting = YES;
    
    //BTModalViewController *modalController = [self.storyboard instantiateViewControllerWithIdentifier:@"ModalViewController"];
    modalController = [[InitialSlidingViewController alloc] init];
    modalController.modalPresentationStyle = UIModalPresentationCustom;
    modalController.transitioningDelegate = self.interactor;
    [self.navigationController presentViewController:modalController animated:YES completion:nil];
    //[self.navigationController popToViewController:modalController animated:NO];
    //[self.navigationController pushViewController:modalController animated:YES];
}
// End Slide Menu

-(void)myHomeAction:(UIBarButtonItem *)sender{
    
    appdelegate.nextPage = @"";
    
    if ([appdelegate.listOfVerMenu count]>0) {
        if (appdelegate.homeclicked==NO) {
            appdelegate.homeclicked = YES;
            [self showModalButtonWasTouched];
        } else {
            appdelegate.homeclicked = NO;
            [modalController dismissViewControllerAnimated:YES completion:nil];
        }
    } else {
        //AppDelegate *appdelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
        //NSLog(@"View Controllers : %@",[appdelegate.navController viewControllers]);
        NSMutableArray *navigationArray = [[NSMutableArray alloc] initWithArray: appdelegate.navController.viewControllers];
        
        for (UIViewController* viewController in appdelegate.navController.viewControllers) {
            
            //This if condition checks whether the viewController's class is MyGroupViewController
            // if true that means its the MyGroupViewController (which has been pushed at some point)
            if ([viewController isKindOfClass:[RootViewController class]] ) {
                
                // Here viewController is a reference of UIViewController base class of MyGroupViewController
                // but viewController holds MyGroupViewController  object so we can type cast it here
                //ThemeViewController *themeViewController = (ThemeViewController*)viewController;
                //[self.navigationController popToViewController:themeViewController animated:YES];
                [navigationArray removeObjectAtIndex: 0];
            }
        }
        appdelegate.navController.viewControllers = navigationArray;
        for (UIViewController* viewController in appdelegate.navController.viewControllers) {
            
            //This if condition checks whether the viewController's class is MyGroupViewController
            // if true that means its the MyGroupViewController (which has been pushed at some point)
            if ([viewController isKindOfClass:[ViewController class]] ) {
                
                // Here viewController is a reference of UIViewController base class of MyGroupViewController
                // but viewController holds MyGroupViewController  object so we can type cast it here
                ViewController *themeViewController = (ViewController*)viewController;
                [appdelegate.navController popToViewController:themeViewController animated:NO];
                //[self.navigationController popToViewController:viewController animated:YES];
            }
        }
        //NSLog(@"View Controllers111 : %@",appdelegate.navController.viewControllers);
    }
    
}

- (void)viewDidAppear:(BOOL)animated
{
    [self aMethod:nil];

}

-(void)viewWillAppear:(BOOL)animated
{
    
}

-(void) viewDidDisappear:(BOOL)animated {
    
    [super viewDidDisappear:animated];
    
    //appdelegate.event_id = @"";
    appdelegate.homeclicked = NO;
    [modalController dismissViewControllerAnimated:YES completion:nil];
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)goback
{
    [appdelegate.navController popViewControllerAnimated:NO];
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    
    return 1;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    
    return nil;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    
    return 0;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    
    if(tableView == paxTableView){
        return [max_guesstotal intValue];
    }
    
    return 1;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if(tableView == paxTableView){
		return 30;
    }
    
    return 1;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell;
    
    if(tableView == paxTableView) {
        static NSString *CellIdentifier = @"CellIdentifier";
        cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        UILabel *cname;
        
        const NSInteger LEFT_TOP_TAG = 2000;
        
        if (cell == nil)
        {
            cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
            
            cname = [[UILabel alloc] initWithFrame: CGRectMake(5, 10, 310, 20)];
            cname.tag = LEFT_TOP_TAG;
            cname.textColor = [UIColor blackColor];
            cname.textAlignment = NSTextAlignmentLeft;
            cname.backgroundColor = [UIColor clearColor];
            cname.numberOfLines = 0;
            [cname setFont:[UIFont systemFontOfSize:14]];
            [cell.contentView addSubview:cname];
        }
        else
            cname = (UILabel *)[cell viewWithTag:LEFT_TOP_TAG];
        
        if (indexPath.row < [max_guesstotal intValue] )
            cname.text = [NSString stringWithFormat:@"%i",indexPath.row + 1];
	}
    
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    if(tableView == paxTableView){
		//NSDictionary *data = [listOfbranch objectAtIndex:indexPath.row];
		//NSString *cname = [data objectForKey:@"branch_title"];
		//NSString *ccode = [data objectForKey:@"branch_id"];
		//tf1.text = cname;
		//branchid = ccode;
        //branchTel = ccode;
        tf.text = [NSString stringWithFormat:@"%ld", (long)indexPath.row + 1];
        [paxPopoverController dismissPopoverAnimated:NO];
        self.paxPopoverController = nil;
	}
    
}

- (NSMutableArray *)createMutableArray:(NSArray *)array
{
    return [array mutableCopy];
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    [textField resignFirstResponder];
    return YES;
}

- (void)textViewDidBeginEditing:(UITextView *)textField
{
    CGPoint scrollPoint = CGPointMake(0, textField.frame.origin.y - 100);
    [scrollviewAdd setContentOffset:scrollPoint animated:NO];
}

- (void)textViewDidEndEditing:(UITextView *)textView
{
    [scrollviewAdd setContentOffset:CGPointZero animated:NO];
}

- (void)textFieldDidBeginEditing:(UITextField *)textField
{
    CGPoint scrollPoint = CGPointMake(0, textField.frame.origin.y/2.0);
    [scrollviewAdd setContentOffset:scrollPoint animated:NO];
}

- (void)textFieldDidEndEditing:(UITextField *)textView
{
    [scrollviewAdd setContentOffset:CGPointZero animated:NO];
}

- (BOOL) textView: (UITextView *)textView shouldChangeTextInRange:(NSRange )range replacementText:(NSString *)text
{
    if ([text isEqualToString:@"\n"])
    {
        [textView resignFirstResponder];
        return NO;
    }
    return YES;
}

- (void) touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
    [view_eventAdd endEditing:YES];
}

#pragma mark Connect To API
-(void) connectToServer:(NSString *) urlPath
{
    [self addLoadingView];
    
	NSString *urlAddress =[NSString stringWithFormat:@"%@", urlPath];
	
	/*NSURL *url = [NSURL URLWithString:urlAddress];
	
	NSURLRequest *requestObj = [NSURLRequest requestWithURL:url
												cachePolicy:NSURLRequestReloadIgnoringLocalAndRemoteCacheData
											timeoutInterval:30];
	
	NSURLConnection *theConnection=[[NSURLConnection alloc] initWithRequest:requestObj delegate:self];
	if (theConnection) {
		// Create the NSMutableData to hold the received data.
		// receivedData is an instance variable declared elsewhere.
		receivedData = [NSMutableData data];
	} else {
		// Inform the user that the connection failed.
		//[self displayAlert:@"Connection Error. Please try again later."];
		[self displayAlert:NSLocalizedString(@"MSG_CONNECTION_ERROR", nil)];
        [self removeLoadingView];
	} */
    [self getJsonResponse:urlAddress success:^(NSDictionary *responseDict) {
        
        //NSLog(@"%@",responseDict);
        [self successfunction:responseDict];
        
    } failure:^(NSError *error) {
        // release the connection, and the data object
        
        [self removeLoadingView];
        [self displayAlert:NSLocalizedString(@"MSG_CONNECTION_ERROR", nil)];
    }];
}

-(void)getJsonResponse : (NSString *)urlStr success : (void (^)(NSDictionary *responseDict))success failure:(void(^)(NSError* error))failure
{
    
    
    NSURLSession * session = [NSURLSession sharedSession];
    NSURL * url = [NSURL URLWithString: urlStr];
    
    
    // Asynchronously Api is hit here
    NSURLSessionDataTask * dataTask = [session dataTaskWithURL:url completionHandler:^(NSData *data, NSURLResponse *response, NSError *error)
                                       {
                                           
                                           //NSLog(@"%@",data);
                                           NSDictionary * json;
                                           if ([data length]>0) {
                                               json  = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
                                               //NSLog(@"%@",json);
                                               success(json);
                                           } else {
                                               json = nil;
                                               dispatch_async(dispatch_get_main_queue(), ^ {
                                                   [self removeLoadingView];
                                                   [self displayAlert:NSLocalizedString(@"MSG_CONNECTION_ERROR", nil)];
                                               });
                                           }
                                           
                                           
                                       }];
    
    [dataTask resume] ; // Executed First
    
    
}

-(void) successfunction:(NSDictionary *)responseDict {
    dispatch_async(dispatch_get_main_queue(), ^{
        //NSString *jsonString = [responseDict JSONRepresentation];
        [self removeLoadingView];
        
        if ([currentRequest isEqualToString:@"getEventList"])
        {
            listOfContent = [[NSMutableArray alloc]init];
            
            NSDictionary *dictionary = responseDict;
            
            for(NSDictionary *row in dictionary)
            {
                NSMutableDictionary *mutableDic = [NSMutableDictionary dictionaryWithObjectsAndKeys:
                                                   [row objectForKey:@"id"],@"id",
                                                   [row objectForKey:@"name"],@"name",
                                                   [row objectForKey:@"email"],@"email",
                                                   [row objectForKey:@"mobile"],@"mobile",
                                                   [row objectForKey:@"event_id"],@"event_id",
                                                   
                                                   [row objectForKey:@"note"],@"note",
                                                   [row objectForKey:@"pax"],@"pax",
                                                   [row objectForKey:@"notification"],@"notification",
                                                   [row objectForKey:@"top_id"],@"top_id",
                                                   [row objectForKey:@"status"],@"status",
                                                   [row objectForKey:@"is_imported"],@"is_imported",
                                                   
                                                   [row objectForKey:@"user_id"],@"user_id",
                                                   [row objectForKey:@"cdate"],@"cdate",
                                                   [row objectForKey:@"mdate"],@"mdate",
                                                   [row objectForKey:@"event_name"],@"event_name",
                                                   [row objectForKey:@"event_description"],@"event_description",
                                                   [row objectForKey:@"date_start"],@"date_start",
                                                   [row objectForKey:@"date_end"],@"date_end",
                                                   [row objectForKey:@"time_start"],@"time_start",
                                                   [row objectForKey:@"time_end"],@"time_end",
                                                   [row objectForKey:@"venue"],@"venue",
                                                   [row objectForKey:@"cutoff"],@"cutoff",
                                                   [row objectForKey:@"pax_limit"],@"pax_limit",
                                                   [row objectForKey:@"bgcolor"],@"bgcolor",
                                                   [row objectForKey:@"fontcolor"],@"fontcolor",
                                                   [row objectForKey:@"btncolor"],@"btncolor",
                                                   [row objectForKey:@"btnfontcolor"],@"btnfontcolor",
                                                   [row objectForKey:@"headercolor"],@"headercolor",
                                                   [row objectForKey:@"headerfontcolor"],@"headerfontcolor",
                                                   [row objectForKey:@"bg_img"],@"bg_img",
                                                   [row objectForKey:@"bg_going_img"],@"bg_going_img",
                                                   [row objectForKey:@"bg_maybe_img"],@"bg_maybe_img",
                                                   [row objectForKey:@"bg_not_going_img"],@"bg_not_going_img",
                                                   nil];
                [listOfContent addObject:mutableDic];
            }
            
            [self displayContent];
            
        }
        
        if ([currentRequest isEqualToString:@"submit"])
        {
            
            NSDictionary *dictionary = responseDict;
            
            for(NSDictionary *row in dictionary)
            {
                if ([[row objectForKey:@"status"] isEqualToString:@"1"])
                {
                    
                    [self displayAlert:NSLocalizedString(@"thankyou", nil)];
                    [self geteventlist];
                }
                else
                    [self displayAlert:[row objectForKey:@"desc"]];
            }
        }
    });
    
}

/*- (void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response
{
    [receivedData setLength:0];
}

- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data
{
    [receivedData appendData:data];
}

- (void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error {
    // release the connection, and the data object
	
    //[connection release];
	//[receivedData release];
	
    [self removeLoadingView];
	[self displayAlert:NSLocalizedString(@"MSG_CONNECTION_ERROR", nil)];
}

- (void)connectionDidFinishLoading:(NSURLConnection *)connection
{
	//NSLog(@"Succeed! Received %ld bytes of data",(unsigned long)[receivedData length]);
    [self removeLoadingView];
    
	NSString *response = [[NSString alloc] initWithData:receivedData encoding:NSUTF8StringEncoding];
    //response = [response stringByDecodingHTMLEntities];
    response = [response stringByReplacingOccurrencesOfString:@"'" withString:@"''"];
    //NSLog(@"response: %@", response);
	
	if ([currentRequest isEqualToString:@"getEventList"])
    {
        listOfContent = [[NSMutableArray alloc]init];
        
		NSDictionary *dictionary = [response JSONValue];
        
		for(NSDictionary *row in dictionary)
        {
            NSMutableDictionary *mutableDic = [NSMutableDictionary dictionaryWithObjectsAndKeys:
                                               [row objectForKey:@"id"],@"id",
                                               [row objectForKey:@"name"],@"name",
                                               [row objectForKey:@"email"],@"email",
                                               [row objectForKey:@"mobile"],@"mobile",
                                               [row objectForKey:@"event_id"],@"event_id",
                                               
                                               [row objectForKey:@"note"],@"note",
                                               [row objectForKey:@"pax"],@"pax",
                                               [row objectForKey:@"notification"],@"notification",
                                               [row objectForKey:@"top_id"],@"top_id",
                                               [row objectForKey:@"status"],@"status",
                                               [row objectForKey:@"is_imported"],@"is_imported",
                                               
                                               [row objectForKey:@"user_id"],@"user_id",
                                               [row objectForKey:@"cdate"],@"cdate",
                                               [row objectForKey:@"mdate"],@"mdate",
                                               [row objectForKey:@"event_name"],@"event_name",
                                               [row objectForKey:@"event_description"],@"event_description",
                                               [row objectForKey:@"date_start"],@"date_start",
                                               [row objectForKey:@"date_end"],@"date_end",
                                               [row objectForKey:@"time_start"],@"time_start",
                                               [row objectForKey:@"time_end"],@"time_end",
                                               [row objectForKey:@"venue"],@"venue",
                                               [row objectForKey:@"cutoff"],@"cutoff",
                                               [row objectForKey:@"pax_limit"],@"pax_limit",
                                               [row objectForKey:@"bgcolor"],@"bgcolor",
                                               [row objectForKey:@"fontcolor"],@"fontcolor",
                                               [row objectForKey:@"btncolor"],@"btncolor",
                                               [row objectForKey:@"btnfontcolor"],@"btnfontcolor",
                                               [row objectForKey:@"headercolor"],@"headercolor",
                                               [row objectForKey:@"headerfontcolor"],@"headerfontcolor",
                                               [row objectForKey:@"bg_img"],@"bg_img",
                                               [row objectForKey:@"bg_going_img"],@"bg_going_img",
                                               [row objectForKey:@"bg_maybe_img"],@"bg_maybe_img",
                                               [row objectForKey:@"bg_not_going_img"],@"bg_not_going_img",
                                               nil];
            [listOfContent addObject:mutableDic];
        }
        
        [self displayContent];
        
    }
    
    if ([currentRequest isEqualToString:@"submit"])
    {
        
        NSDictionary *dictionary = [response JSONValue];
        
        for(NSDictionary *row in dictionary)
        {
            if ([[row objectForKey:@"status"] isEqualToString:@"1"])
            {
                
                [self displayAlert:NSLocalizedString(@"thankyou", nil)];
                [self geteventlist];
            }
            else
                [self displayAlert:[row objectForKey:@"desc"]];
        }
    }
    
    connection = nil;
    response = nil;
} */

#pragma mark LoadingView
-(void) addLoadingView
{
    loadingView = [LoadingView loadingViewInView:self.view];
	
}

-(void) removeLoadingView
{
	[loadingView removeView];
}

#pragma mark displayAlert
-(void) displayAlert: (NSString *) stralert {
	NSString *str = [[NSString alloc] initWithFormat:@"%@", stralert];
	UIAlertView *alert = [[UIAlertView alloc] initWithTitle:appdelegate.iconname
													message:str
												   delegate:self
										  cancelButtonTitle:NSLocalizedString(@"done", nil)
										  otherButtonTitles:nil];
	
	[alert show];
    str = nil;
	//[str release];
	//[alert release];
}

- (void)alertView:(UIAlertView *)alertView willDismissWithButtonIndex:(NSInteger)buttonIndex
{
	if (buttonIndex == 0)
    {
		//Clicked Cancel Button
	}
	else
    {
		//Clicked Yes Button
		/*if ([currentRequest isEqualToString:@"cancelBooking"]) {
			
			currentRequest = @"cancelBooking";
            NSString *urlAddress = [[NSString alloc] initWithFormat:@"%@booking_cancel.php?db=%@&userid=%@&udid=%@&cuid=%@&bookid=%@&device=ios&sc=%@",appdelegate.user_apipath,appdelegate.db,appdelegate.user,appdelegate.uuid,appdelegate.customer_id,reservationId,appdelegate.cs];
            [self connectToServer:urlAddress];
		} */
	}
}

- (void) displayContent
{
    [view_eventAdd setHidden:NO];
    [view_eventAdd1 setHidden:YES];
    
    [[scrollviewAdd subviews] makeObjectsPerformSelector:@selector(removeFromSuperview)];
    labelheight = 5;
    
    NSString *guest_id = @"";
    NSString *name = @"";
    NSString *email = @"";
    NSString *mobile = @"";
    NSString *note = @"";
    NSString *pax = @"";
    NSString *notification = @"";
    NSString *status = @"";
    NSString *event_name = @"";
    NSString *event_description = @"";
    NSString *date_start = @"";
    NSString *date_end = @"";
    NSString *time_start = @"";
    NSString *time_end = @"";
    NSString *venue = @"";
    NSString *cutoff = @"";
    NSString *pax_limit = @"";
    NSString *bgcolor = @"";
    NSString *fontcolor = @"";
    NSString *btncolor = @"";
    NSString *btnfontcolor = @"";
    NSString *bg_img = @"";
    NSString *bg_going_img = @"";
    NSString *bg_maybe_img = @"";
    NSString *bg_not_going_img = @"";

    //NSLog(@"listOfContent :%@", listOfContent);
    if([listOfContent count]>0)
    {
        for (int i=0;i<[listOfContent count]; i++)
        {
            NSDictionary *dictionary = [listOfContent objectAtIndex:i];
     
            guest_id = [dictionary objectForKey:@"id"];
            name = [dictionary objectForKey:@"name"];
            email = [dictionary objectForKey:@"email"];
            mobile = [dictionary objectForKey:@"mobile"];
            note = [dictionary objectForKey:@"note"];
            pax = [dictionary objectForKey:@"pax"];
            notification = [dictionary objectForKey:@"notification"];
            status = [dictionary objectForKey:@"status"];
            event_name = [dictionary objectForKey:@"event_name"];
            event_description = [dictionary objectForKey:@"event_description"];
            date_start = [dictionary objectForKey:@"date_start"];
            date_end = [dictionary objectForKey:@"date_end"];
            time_start = [dictionary objectForKey:@"time_start"];
            time_end = [dictionary objectForKey:@"time_end"];
            venue = [dictionary objectForKey:@"venue"];
            cutoff = [dictionary objectForKey:@"cutoff"];
            pax_limit = [dictionary objectForKey:@"pax_limit"];
            bgcolor = [dictionary objectForKey:@"bgcolor"];
            fontcolor = [dictionary objectForKey:@"fontcolor"];
            btncolor = [dictionary objectForKey:@"btncolor"];
            btnfontcolor = [dictionary objectForKey:@"btnfontcolor"];
            bg_img = [dictionary objectForKey:@"bg_img"];
            bg_going_img = [dictionary objectForKey:@"bg_going_img"];
            bg_maybe_img = [dictionary objectForKey:@"bg_maybe_img"];
            bg_not_going_img = [dictionary objectForKey:@"bg_not_going_img"];
            
            if ([name isEqual:[NSNull null]]) {
                name = @"";
            }
            if ([email isEqual:[NSNull null]]) {
                email = @"";
            }
            if ([mobile isEqual:[NSNull null]]) {
                mobile = @"";
            }
            if ([note isEqual:[NSNull null]]) {
                note = @"";
            }
            if ([pax isEqual:[NSNull null]]) {
                pax = @"";
            }
            if ([notification isEqual:[NSNull null]]) {
                notification = @"";
            }
            if ([status isEqual:[NSNull null]]) {
                status = @"";
            }
            if ([event_name isEqual:[NSNull null]]) {
                event_name = @"";
            }
            if ([event_description isEqual:[NSNull null]]) {
                event_description = @"";
            }
            if ([date_start isEqual:[NSNull null]]) {
                date_start = @"";
            }
            if ([date_end isEqual:[NSNull null]]) {
                date_end = @"";
            }
            if ([time_start isEqual:[NSNull null]]) {
                time_start = @"";
            }
            if ([time_end isEqual:[NSNull null]]) {
                time_end = @"";
            }
            if ([venue isEqual:[NSNull null]]) {
                venue = @"";
            }
            if ([cutoff isEqual:[NSNull null]]) {
                cutoff = @"";
            }
            if ([pax_limit isEqual:[NSNull null]]) {
                pax_limit = @"";
            }
            if ([bgcolor isEqual:[NSNull null]]) {
                bgcolor = @"";
            }
            if ([fontcolor isEqual:[NSNull null]]) {
                fontcolor = @"";
            }
            if ([btncolor isEqual:[NSNull null]]) {
                btncolor = @"";
            }
            if ([btnfontcolor isEqual:[NSNull null]]) {
                btnfontcolor = @"";
            }
            if ([bg_img isEqual:[NSNull null]]) {
                bg_img = @"";
            }
            if ([bg_going_img isEqual:[NSNull null]]) {
                bg_going_img = @"";
            }
            if ([bg_maybe_img isEqual:[NSNull null]]) {
                bg_maybe_img = @"";
            }
            if ([bg_not_going_img isEqual:[NSNull null]]) {
                bg_not_going_img = @"";
            }
        }
    }
    
    if ([bgcolor length]>0) {
        self.view.backgroundColor = [self colorWithHexString:bgcolor];
    }
    
    if ([bg_img length]>0) {
        NSURL *imageURL = [NSURL URLWithString:bg_img];
        NSData *imageData = [NSData dataWithContentsOfURL:imageURL];
        //self.backgroundColor = [UIColor redColor];
        UIGraphicsBeginImageContext(self.view.frame.size);
        CGRect rect=CGRectMake(0,0,self.view.bounds.size.width,self.view.bounds.size.height);
        [[UIImage imageWithData:imageData] drawInRect:rect];
        UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
        UIGraphicsEndImageContext();
        self.view.backgroundColor = [UIColor colorWithPatternImage:image];
    }
    
    max_guesstotal = pax_limit;
    
    UILabel *lbleventTitle = [[UILabel alloc] initWithFrame:CGRectMake(5, labelheight, self.view.frame.size.width-10, 20)];
    lbleventTitle.numberOfLines = 0;
    lbleventTitle.lineBreakMode = NSLineBreakByWordWrapping;
    UIFont *Font = [UIFont fontWithName:@"Helvetica-Bold" size:16];
    lbleventTitle.font = Font;
    lbleventTitle.text = [NSString stringWithFormat:@"%@", NSLocalizedString(@"event_details", nil)];
    lbleventTitle.backgroundColor = [UIColor clearColor];
    if ([fontcolor length]>0) {
        lbleventTitle.textColor = [self colorWithHexString:fontcolor];
    }
    [lbleventTitle sizeToFit];
    //lbladdr.shadowColor = [UIColor blackColor];
    //lbladdr.shadowOffset = CGSizeMake(0, 1);
    [scrollviewAdd addSubview:lbleventTitle];
    //[lblbranch release];
    lbleventTitle = nil;
    labelheight = labelheight + 25;
    //NSLog(@"labelheight: %d", labelheight);
    
    UITextView *eventdet = [[UITextView alloc] initWithFrame:CGRectMake(5, labelheight, self.view.frame.size.width-10, 100)];
    eventdet.delegate = self ;
    [eventdet setFont:[UIFont systemFontOfSize:16]];
    eventdet.text = [NSString stringWithFormat:@"%@\n%@", event_name, event_description];
    if ([fontcolor length]>0) {
        eventdet.textColor = [self colorWithHexString:fontcolor];
    }
    //To make the border look very close to a UITextField
    [eventdet.layer setBorderColor:[[self colorWithHexString:@"#999999"] CGColor]];
    [eventdet.layer setBorderWidth:1.0];
    
    //The rounded corner part, where you specify your view's corner radius:
    eventdet.userInteractionEnabled = YES;
    [scrollviewAdd addSubview:eventdet];
    labelheight = labelheight + 120;
        
    UILabel *lbldate = [[UILabel alloc] initWithFrame:CGRectMake(5, labelheight, (self.view.frame.size.width-20)/3, 20)];
    lbldate.numberOfLines = 0;
    lbldate.lineBreakMode = NSLineBreakByWordWrapping;
    UIFont *Font2 = [UIFont fontWithName:@"Helvetica-Bold" size:16];
    lbldate.font = Font2;
    lbldate.text = [NSString stringWithFormat:@"%@", NSLocalizedString(@"date", nil)];
    lbldate.backgroundColor = [UIColor clearColor];
    if ([fontcolor length]>0) {
        lbldate.textColor = [self colorWithHexString:fontcolor];
    }
    [lbldate sizeToFit];
    //lblpostcode.shadowColor = [UIColor blackColor];
    //lbladdr.shadowOffset = CGSizeMake(0, 1);
    [scrollviewAdd addSubview:lbldate];
    //[lbldatestart release];
    lbldate = nil;
    
    UILabel *lbldateval = [[UILabel alloc] initWithFrame:CGRectMake((self.view.frame.size.width-20)/3+5, labelheight, (self.view.frame.size.width-20)/3*2, 20)];
    lbldateval.numberOfLines = 0;
    lbldateval.lineBreakMode = NSLineBreakByWordWrapping;
    UIFont *Font3 = [UIFont fontWithName:@"Helvetica" size:16];
    lbldateval.font = Font3;
    lbldateval.text = [NSString stringWithFormat:@"%@ - %@", date_start, date_end];
    lbldateval.backgroundColor = [UIColor clearColor];
    if ([fontcolor length]>0) {
        lbldateval.textColor = [self colorWithHexString:fontcolor];
    }
    [lbldateval sizeToFit];
    //lblcity.shadowColor = [UIColor blackColor];
    //lbladdr.shadowOffset = CGSizeMake(0, 1);
    [scrollviewAdd addSubview:lbldateval];
    //[lbltimestart release];
    lbldateval = nil;
    labelheight = labelheight + 30;
    //NSLog(@"labelheight: %d", labelheight);
    
    UILabel *lbltime = [[UILabel alloc] initWithFrame:CGRectMake(5, labelheight, (self.view.frame.size.width-20)/3, 20)];
    lbltime.numberOfLines = 0;
    lbltime.lineBreakMode = NSLineBreakByWordWrapping;
    UIFont *Font4 = [UIFont fontWithName:@"Helvetica-Bold" size:16];
    lbltime.font = Font4;
    lbltime.text = [NSString stringWithFormat:@"%@", NSLocalizedString(@"time", nil)];
    lbltime.backgroundColor = [UIColor clearColor];
    if ([fontcolor length]>0) {
        lbltime.textColor = [self colorWithHexString:fontcolor];
    }
    [lbltime sizeToFit];
    //lblstreet.shadowColor = [UIColor blackColor];
    //lbladdr.shadowOffset = CGSizeMake(0, 1);
    [scrollviewAdd addSubview:lbltime];
    //[lbldateend release];
    lbltime = nil;
    
    UILabel *lbltimeval = [[UILabel alloc] initWithFrame:CGRectMake((self.view.frame.size.width-20)/3+5, labelheight, (self.view.frame.size.width-20)/3*2, 20)];
    lbltimeval.numberOfLines = 0;
    lbltimeval.lineBreakMode = NSLineBreakByWordWrapping;
    UIFont *Font7 = [UIFont fontWithName:@"Helvetica" size:16];
    lbltimeval.font = Font7;
    lbltimeval.text = [NSString stringWithFormat:@"%@ - %@", time_start, time_end];
    lbltimeval.backgroundColor = [UIColor clearColor];
    if ([fontcolor length]>0) {
        lbltimeval.textColor = [self colorWithHexString:fontcolor];
    }
    [lbltimeval sizeToFit];
    //lblstreet.shadowColor = [UIColor blackColor];
    //lbladdr.shadowOffset = CGSizeMake(0, 1);
    [scrollviewAdd addSubview:lbltimeval];
    //[lbltimeend release];
    lbltimeval = nil;
    
    labelheight = labelheight + 30;
    //NSLog(@"labelheight: %d", labelheight);
    
    UILabel *lbladdress = [[UILabel alloc] initWithFrame:CGRectMake(5, labelheight, (self.view.frame.size.width-20)/3, 20)];
    lbladdress.numberOfLines = 0;
    lbladdress.lineBreakMode = NSLineBreakByWordWrapping;
    UIFont *Font8 = [UIFont fontWithName:@"Helvetica-Bold" size:16];
    lbladdress.font = Font8;
    lbladdress.text = [NSString stringWithFormat:@"%@", NSLocalizedString(@"address", nil)];
    lbladdress.backgroundColor = [UIColor clearColor];
    if ([fontcolor length]>0) {
        lbladdress.textColor = [self colorWithHexString:fontcolor];
    }
    [lbladdress sizeToFit];
    //lblstreet.shadowColor = [UIColor blackColor];
    //lbladdr.shadowOffset = CGSizeMake(0, 1);
    [scrollviewAdd addSubview:lbladdress];
    //[lbldateend release];
    lbladdress = nil;
    
    UILabel *lbladdressval = [[UILabel alloc] initWithFrame:CGRectMake((self.view.frame.size.width-20)/3+5, labelheight, (self.view.frame.size.width-20)/3*2, 80)];
    lbladdressval.numberOfLines = 0;
    lbladdressval.lineBreakMode = NSLineBreakByWordWrapping;
    UIFont *Font9 = [UIFont fontWithName:@"Helvetica" size:16];
    lbladdressval.font = Font9;
    lbladdressval.text = [NSString stringWithFormat:@"%@", venue];
    lbladdressval.backgroundColor = [UIColor clearColor];
    if ([fontcolor length]>0) {
        lbladdressval.textColor = [self colorWithHexString:fontcolor];
    }
    [lbladdressval sizeToFit];
    //lbladdressval.shadowColor = [UIColor blackColor];
    //lbladdressval.shadowOffset = CGSizeMake(0, 1);
    CGSize constrainedSize = CGSizeMake(lbladdressval.frame.size.width  , 9999);
    
    NSDictionary *attributesDictionary = [NSDictionary dictionaryWithObjectsAndKeys:
                                          Font9, NSFontAttributeName,
                                          nil];
    
    NSMutableAttributedString *string = [[NSMutableAttributedString alloc] initWithString:lbladdressval.text attributes:attributesDictionary];
    
    CGRect requiredHeight = [string boundingRectWithSize:constrainedSize options:NSStringDrawingUsesLineFragmentOrigin context:nil];
    
    if (requiredHeight.size.width > lbladdressval.frame.size.width) {
        requiredHeight = CGRectMake(0,0, lbladdressval.frame.size.width, requiredHeight.size.height);
    }
    CGRect newFrame = lbladdressval.frame;
    newFrame.size.height = requiredHeight.size.height;
    lbladdressval.frame = newFrame;
    
    //lbladdressval.frame = CGRectMake(0, labelheight, self.view.frame.size.width, newFrame.size.height+10);
    [scrollviewAdd addSubview:lbladdressval];
    //[lbltimeend release];
    lbladdressval = nil;
    
    labelheight = labelheight + newFrame.size.height +10;
    
    UILabel *lblcutoffdate = [[UILabel alloc] initWithFrame:CGRectMake(5, labelheight, (self.view.frame.size.width-20)/3, 20)];
    lblcutoffdate.numberOfLines = 0;
    lblcutoffdate.lineBreakMode = NSLineBreakByWordWrapping;
    UIFont *Font10 = [UIFont fontWithName:@"Helvetica-Bold" size:16];
    lblcutoffdate.font = Font10;
    lblcutoffdate.text = [NSString stringWithFormat:@"%@", NSLocalizedString(@"cutoff", nil)];
    lblcutoffdate.backgroundColor = [UIColor clearColor];
    if ([fontcolor length]>0) {
        lblcutoffdate.textColor = [self colorWithHexString:fontcolor];
    }
    [lblcutoffdate sizeToFit];
    //lblstreet.shadowColor = [UIColor blackColor];
    //lbladdr.shadowOffset = CGSizeMake(0, 1);
    [scrollviewAdd addSubview:lblcutoffdate];
    //[lbldateend release];
    lblcutoffdate = nil;
    
    UILabel *lblcutoffdateval = [[UILabel alloc] initWithFrame:CGRectMake((self.view.frame.size.width-20)/3+5, labelheight, (self.view.frame.size.width-20)/3*2, 20)];
    lblcutoffdateval.numberOfLines = 0;
    lblcutoffdateval.lineBreakMode = NSLineBreakByWordWrapping;
    UIFont *Font11 = [UIFont fontWithName:@"Helvetica" size:16];
    lblcutoffdateval.font = Font11;
    lblcutoffdateval.text = [NSString stringWithFormat:@"%@", cutoff];
    lblcutoffdateval.backgroundColor = [UIColor clearColor];
    if ([fontcolor length]>0) {
        lblcutoffdateval.textColor = [self colorWithHexString:fontcolor];
    }
    [lblcutoffdateval sizeToFit];
    //lblstreet.shadowColor = [UIColor blackColor];
    //lbladdr.shadowOffset = CGSizeMake(0, 1);
    [scrollviewAdd addSubview:lblcutoffdateval];
    //[lbltimeend release];
    lblcutoffdateval = nil;
    
    labelheight = labelheight + 40;
    
    /*UILabel *lblconfirmation = [[UILabel alloc] initWithFrame:CGRectMake(5, labelheight, self.view.frame.size.width-10, 20)];
    lblconfirmation.numberOfLines = 0;
    lblconfirmation.lineBreakMode = NSLineBreakByWordWrapping;
    UIFont *Font5 = [UIFont fontWithName:@"Helvetica" size:16];
    lblconfirmation.font = Font5;
    lblconfirmation.text = [NSString stringWithFormat:@"%@", NSLocalizedString(@"confirmation", nil)];
    lblconfirmation.backgroundColor = [UIColor clearColor];
    //lbladdr.textColor = [UIColor blackColor];
    [lblconfirmation sizeToFit];
    //lbladdr.shadowColor = [UIColor blackColor];
    //lbladdr.shadowOffset = CGSizeMake(0, 1);
    [scrollviewAdd addSubview:lblconfirmation];
    //[lbltotperson release];
    lblconfirmation = nil;
    labelheight = labelheight + 30;
    //NSLog(@"labelheight: %d", labelheight); */
    
    float buttonWidthConfirm = 540.0f;
    float buttonHeightConfirm = 100.0f;
    if (self.view.frame.size.width < buttonWidthConfirm) {
        buttonWidthConfirm = 270.0f;
        buttonHeightConfirm = 50.0f;
    }
    
    btnYes = [UIButton buttonWithType:UIButtonTypeCustom];
    btnYes.tag = 1001;
    [btnYes addTarget:self action:@selector(actionConfirm:) forControlEvents:UIControlEventTouchUpInside];
    btnYes.titleLabel.font = [UIFont boldSystemFontOfSize:18];
    [btnYes setTitle:NSLocalizedString(@"yes", nil) forState:UIControlStateNormal];
    if ([btnfontcolor length]>0) {
        [btnYes setTitleColor:[self colorWithHexString:btnfontcolor] forState:UIControlStateNormal];
    } else {
        [btnYes setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    }
    if ([btncolor length]>0) {
        [btnYes setBackgroundColor:[self colorWithHexString:btncolor]];
    } else {
        [btnYes setBackgroundColor:[self colorWithHexString:@"#78CFBF"]];
    }
    //float buttonWidth = 80.0f;
    btnYes.frame = CGRectMake(CGRectGetMidX(self.view.frame)-(buttonWidthConfirm/2), labelheight, buttonWidthConfirm, buttonHeightConfirm);
    [scrollviewAdd addSubview:btnYes];
    if ([bg_going_img length]>0) {
        NSURL *imageURL = [NSURL URLWithString:bg_going_img];
        NSData *imageData = [NSData dataWithContentsOfURL:imageURL];
        [btnYes setBackgroundImage:[UIImage imageWithData:imageData] forState:UIControlStateNormal];
        [btnYes setTitleColor:[UIColor clearColor] forState:UIControlStateNormal];
    }
    labelheight = labelheight + buttonHeightConfirm + 20;
    
    btnMaybe = [UIButton buttonWithType:UIButtonTypeCustom];
    btnMaybe.tag = 1002;
    [btnMaybe addTarget:self action:@selector(actionConfirm:) forControlEvents:UIControlEventTouchUpInside];
    btnMaybe.titleLabel.font = [UIFont boldSystemFontOfSize:18];
    [btnMaybe setTitle:NSLocalizedString(@"maybe", nil) forState:UIControlStateNormal];
    if ([btnfontcolor length]>0) {
        [btnMaybe setTitleColor:[self colorWithHexString:btnfontcolor] forState:UIControlStateNormal];
    } else {
        [btnMaybe setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    }
    if ([btncolor length]>0) {
        [btnMaybe setBackgroundColor:[self colorWithHexString:btncolor]];
    } else {
        [btnMaybe setBackgroundColor:[self colorWithHexString:@"#78CFBF"]];
    }
    btnMaybe.frame = CGRectMake(CGRectGetMidX(self.view.frame)-(buttonWidthConfirm/2), labelheight, buttonWidthConfirm, buttonHeightConfirm);
    [scrollviewAdd addSubview:btnMaybe];
    if ([bg_maybe_img length]>0) {
        NSURL *imageURL = [NSURL URLWithString:bg_maybe_img];
        NSData *imageData = [NSData dataWithContentsOfURL:imageURL];
        [btnMaybe setBackgroundImage:[UIImage imageWithData:imageData] forState:UIControlStateNormal];
        [btnMaybe setTitleColor:[UIColor clearColor] forState:UIControlStateNormal];
    }
    labelheight = labelheight + buttonHeightConfirm + 20;
    
    btnNo = [UIButton buttonWithType:UIButtonTypeCustom];
    btnNo.tag = 1003;
    [btnNo addTarget:self action:@selector(actionConfirm:) forControlEvents:UIControlEventTouchUpInside];
    btnNo.titleLabel.font = [UIFont boldSystemFontOfSize:18];
    [btnNo setTitle:NSLocalizedString(@"no", nil) forState:UIControlStateNormal];
    if ([btnfontcolor length]>0) {
        [btnNo setTitleColor:[self colorWithHexString:btnfontcolor] forState:UIControlStateNormal];
    } else {
        [btnNo setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    }
    if ([btncolor length]>0) {
        [btnNo setBackgroundColor:[self colorWithHexString:btncolor]];
    } else {
        [btnNo setBackgroundColor:[self colorWithHexString:@"#78CFBF"]];
    }
    btnNo.frame = CGRectMake(CGRectGetMidX(self.view.frame)-(buttonWidthConfirm/2), labelheight, buttonWidthConfirm, buttonHeightConfirm);
    [scrollviewAdd addSubview:btnNo];
    if ([bg_not_going_img length]>0) {
        NSURL *imageURL = [NSURL URLWithString:bg_not_going_img];
        NSData *imageData = [NSData dataWithContentsOfURL:imageURL];
        [btnNo setBackgroundImage:[UIImage imageWithData:imageData] forState:UIControlStateNormal];
        [btnNo setTitleColor:[UIColor clearColor] forState:UIControlStateNormal];
    }
    labelheight = labelheight + buttonHeightConfirm + 20;
    
    scrollViewHeight = 0.0f;
    
    for (UIView* view in scrollviewAdd.subviews)
        scrollViewHeight += view.frame.size.height;
    
    scrollviewAdd.contentSize = CGSizeMake(scrollviewAdd.frame.size.width, labelheight + 500);
    
    UITapGestureRecognizer *gestureRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:[self scrollviewAdd] action:@selector(touchesBegan:withEvent:)];
    gestureRecognizer.delegate = self;
    [scrollviewAdd addGestureRecognizer:gestureRecognizer];
}

- (void) displayContent1
{
    [view_eventAdd setHidden:YES];
    [view_eventAdd1 setHidden:NO];
    confirmStatus = @"";
    reminderStatus = @"";
    
    [[scrollviewAdd1 subviews] makeObjectsPerformSelector:@selector(removeFromSuperview)];
    labelheight = 5;
    
    NSString *guest_id = @"";
    NSString *name = @"";
    NSString *email = @"";
    NSString *mobile = @"";
    NSString *note = @"";
    NSString *pax = @"";
    NSString *notification = @"";
    NSString *status = @"";
    
    NSString *headercolor = @"";
    NSString *headerfontcolor = @"";
    if([listOfContent count]>0)
    {
        for (int i=0;i<[listOfContent count]; i++)
        {
            NSDictionary *dictionary = [listOfContent objectAtIndex:i];
            
            guest_id = [dictionary objectForKey:@"id"];
            name = [dictionary objectForKey:@"name"];
            email = [dictionary objectForKey:@"email"];
            mobile = [dictionary objectForKey:@"mobile"];
            note = [dictionary objectForKey:@"note"];
            pax = [dictionary objectForKey:@"pax"];
            notification = [dictionary objectForKey:@"notification"];
            status = [dictionary objectForKey:@"status"];
            headercolor = [dictionary objectForKey:@"headercolor"];
            headerfontcolor = [dictionary objectForKey:@"headerfontcolor"];
        }
    }
    //NSLog(@"listOfContent:%@", listOfContent);
    UIScrollView *viewconfirm = [[UIScrollView alloc] initWithFrame:CGRectMake(5, labelheight, self.view.frame.size.width-10, 100)];
    [viewconfirm.layer setBorderColor:[[[UIColor grayColor] colorWithAlphaComponent:0.5] CGColor]];
    [viewconfirm.layer setBorderWidth:1.0];
    
    //The rounded corner part, where you specify your view's corner radius:
    viewconfirm.layer.cornerRadius = 5;
    viewconfirm.clipsToBounds = YES;
    viewconfirm.scrollEnabled = NO;
    [scrollviewAdd1 addSubview:viewconfirm];
    
    UILabel *lblconf = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width-10, 40)];
    //lblconf.numberOfLines = 0;
    //lblconf.lineBreakMode = NSLineBreakByWordWrapping;
    UIFont *Font = [UIFont fontWithName:@"Helvetica" size:16];
    lblconf.font = Font;
    if ([headerfontcolor length]>0) {
        lblconf.textColor = [self colorWithHexString:headerfontcolor];
    } else {
        lblconf.textColor = [UIColor whiteColor];
    }
    lblconf.text = NSLocalizedString(@"confirmation", nil);
    lblconf.textAlignment = NSTextAlignmentCenter;
    if ([headercolor length]>0) {
        lblconf.backgroundColor = [self colorWithHexString:headercolor];
    } else {
        lblconf.backgroundColor = [self colorWithHexString:@"#78CFBF"];
    }
    //lbladdr.textColor = [UIColor blackColor];
    //[lblconf sizeToFit];
    //lbladdr.shadowColor = [UIColor blackColor];
    //lbladdr.shadowOffset = CGSizeMake(0, 1);
    [viewconfirm addSubview:lblconf];
    CALayer* layer = [lblconf layer];
    CALayer *bottomBorder = [CALayer layer];
    bottomBorder.borderColor = [[[UIColor grayColor] colorWithAlphaComponent:0.5] CGColor];
    bottomBorder.borderWidth = 1;
    bottomBorder.frame = CGRectMake(-1, layer.frame.size.height-1, layer.frame.size.width, 1);
    [layer addSublayer:bottomBorder];
    //[lblbranch release];
    lblconf = nil;
    
    btnYesConf = [UIButton buttonWithType:UIButtonTypeRoundedRect];
    btnYesConf.tag = 2001;
    [btnYesConf addTarget:self action:@selector(actionConfirm1:) forControlEvents:UIControlEventTouchUpInside];
    [btnYesConf setTitle:NSLocalizedString(@"yes", nil) forState:UIControlStateNormal];
    if ([headercolor length]>0) {
        [btnYesConf setTitleColor:[self colorWithHexString:headercolor] forState:UIControlStateNormal];
    } else {
        [btnYesConf setTitleColor:[self colorWithHexString:@"#78CFBF"] forState:UIControlStateNormal];
    }
    [btnYesConf setBackgroundColor:[UIColor whiteColor]];
    float buttonWidth = 80.0f;
    btnYesConf.frame = CGRectMake(CGRectGetMidX(self.view.frame)-(buttonWidth)-25, 55, buttonWidth, 30.0);
    [viewconfirm addSubview:btnYesConf];
    if ([headercolor length]>0) {
        [btnYesConf.layer setBorderColor:[self colorWithHexString:headercolor].CGColor];
    } else {
        [btnYesConf.layer setBorderColor:[self colorWithHexString:@"#78CFBF"].CGColor];
    }
    [btnYesConf.layer setBorderWidth:1.0];
    //btnYesConf.layer.cornerRadius = 5;
    
    btnLaterConf = [UIButton buttonWithType:UIButtonTypeRoundedRect];
    btnLaterConf.tag = 2002;
    [btnLaterConf addTarget:self action:@selector(actionConfirm1:) forControlEvents:UIControlEventTouchUpInside];
    [btnLaterConf setTitle:NSLocalizedString(@"later", nil) forState:UIControlStateNormal];
    if ([headercolor length]>0) {
        [btnLaterConf setTitleColor:[self colorWithHexString:headercolor] forState:UIControlStateNormal];
    } else {
        [btnLaterConf setTitleColor:[self colorWithHexString:@"#78CFBF"] forState:UIControlStateNormal];
    }
    [btnLaterConf setBackgroundColor:[UIColor whiteColor]];
    btnLaterConf.frame = CGRectMake(CGRectGetMidX(self.view.frame)+25, 55, buttonWidth, 30.0);
    [viewconfirm addSubview:btnLaterConf];
    if ([headercolor length]>0) {
        [btnLaterConf.layer setBorderColor:[self colorWithHexString:headercolor].CGColor];
    } else {
        [btnLaterConf.layer setBorderColor:[self colorWithHexString:@"#78CFBF"].CGColor];
    }
    [btnLaterConf.layer setBorderWidth:1.0];
    //btnLaterConf.layer.cornerRadius = 5;
    
    if ([status intValue]==1) {
        [self actionConfirm1:btnYesConf];
    } else if ([status intValue]==2) {
        [self actionConfirm1:btnLaterConf];
    }
    
    labelheight = labelheight + 115;
    //NSLog(@"labelheight: %d", labelheight);
    
    UIScrollView *viewpax = [[UIScrollView alloc] initWithFrame:CGRectMake(5, labelheight, self.view.frame.size.width-10, 100)];
    [viewpax.layer setBorderColor:[[[UIColor grayColor] colorWithAlphaComponent:0.5] CGColor]];
    [viewpax.layer setBorderWidth:1.0];
    
    //The rounded corner part, where you specify your view's corner radius:
    viewpax.layer.cornerRadius = 5;
    viewpax.clipsToBounds = YES;
    viewpax.scrollEnabled = NO;
    [scrollviewAdd1 addSubview:viewpax];
    
    UILabel *lblconfPax = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width-10, 40)];
    //lblconf.numberOfLines = 0;
    //lblconf.lineBreakMode = NSLineBreakByWordWrapping;
    UIFont *Font1 = [UIFont fontWithName:@"Helvetica" size:16];
    lblconfPax.font = Font1;
    lblconfPax.textColor = [UIColor whiteColor];
    lblconfPax.text = NSLocalizedString(@"pax", nil);
    lblconfPax.textAlignment = NSTextAlignmentCenter;
    if ([headercolor length]>0) {
        lblconfPax.backgroundColor = [self colorWithHexString:headercolor];
    } else {
        lblconfPax.backgroundColor = [self colorWithHexString:@"#78CFBF"];
    }
    //lbladdr.textColor = [UIColor blackColor];
    //[lblconf sizeToFit];
    //lbladdr.shadowColor = [UIColor blackColor];
    //lbladdr.shadowOffset = CGSizeMake(0, 1);
    [viewpax addSubview:lblconfPax];
    CALayer* layerPax = [lblconfPax layer];
    CALayer *bottomBorderPax = [CALayer layer];
    bottomBorderPax.borderColor = [[[UIColor grayColor] colorWithAlphaComponent:0.5] CGColor];
    bottomBorderPax.borderWidth = 1;
    bottomBorderPax.frame = CGRectMake(-1, layerPax.frame.size.height-1, layerPax.frame.size.width, 1);
    [layerPax addSublayer:bottomBorderPax];
    //[lblbranch release];
    lblconfPax = nil;
    
    [tf setFrame:CGRectMake(CGRectGetMidX(self.view.frame)-(buttonWidth)-15, 55, buttonWidth*2+30, 30)];
    //[tf5 setKeyboardType:UIKeyboardTypeNumberPad];
    tf.delegate = self ;
    tf.borderStyle = UITextBorderStyleRoundedRect;
    //if ([headercolor length]>0) {
        //tf.backgroundColor=[self colorWithHexString:headercolor];
    //} else {
        tf.backgroundColor=[UIColor whiteColor];
    //}
    if ([max_guesstotal intValue]>0) {
        NSString *strSelectPax = [NSLocalizedString(@"select_pax", nil) stringByReplacingOccurrencesOfString:@"<max>" withString:max_guesstotal];
        tf.placeholder = [NSString stringWithFormat:strSelectPax, max_guesstotal];
    }
    tf.keyboardType = UIKeyboardTypeNumbersAndPunctuation;
    [viewpax addSubview:tf];
    tf.text = pax;
    labelheight = labelheight + 115;
    //NSLog(@"labelheight: %d", labelheight);
    
    UIScrollView *viewreminder = [[UIScrollView alloc] initWithFrame:CGRectMake(5, labelheight, self.view.frame.size.width-10, 100)];
    [viewreminder.layer setBorderColor:[[[UIColor grayColor] colorWithAlphaComponent:0.5] CGColor]];
    [viewreminder.layer setBorderWidth:1.0];
    
    //The rounded corner part, where you specify your view's corner radius:
    viewreminder.layer.cornerRadius = 5;
    viewreminder.clipsToBounds = YES;
    viewreminder.scrollEnabled = NO;
    [scrollviewAdd1 addSubview:viewreminder];
    
    UILabel *lblreminder = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width-10, 40)];
    //lblconf.numberOfLines = 0;
    //lblconf.lineBreakMode = NSLineBreakByWordWrapping;
    UIFont *Font2 = [UIFont fontWithName:@"Helvetica" size:16];
    lblreminder.font = Font2;
    if ([headerfontcolor length]>0) {
        lblreminder.textColor = [self colorWithHexString:headerfontcolor];
    } else {
        lblreminder.textColor = [UIColor whiteColor];
    }
    lblreminder.text = NSLocalizedString(@"reminder", nil);
    lblreminder.textAlignment = NSTextAlignmentCenter;
    if ([headercolor length]>0) {
        lblreminder.backgroundColor = [self colorWithHexString:headercolor];
    } else {
        lblreminder.backgroundColor = [self colorWithHexString:@"#78CFBF"];
    }
    //lbladdr.textColor = [UIColor blackColor];
    //[lblconf sizeToFit];
    //lbladdr.shadowColor = [UIColor blackColor];
    //lbladdr.shadowOffset = CGSizeMake(0, 1);
    [viewreminder addSubview:lblreminder];
    CALayer* layerRem = [lblreminder layer];
    CALayer *bottomBorderRem = [CALayer layer];
    bottomBorderRem.borderColor = [[[UIColor grayColor] colorWithAlphaComponent:0.5] CGColor];
    bottomBorderRem.borderWidth = 1;
    bottomBorderRem.frame = CGRectMake(-1, layerRem.frame.size.height-1, layerRem.frame.size.width, 1);
    [layerRem addSublayer:bottomBorderRem];
    //[lblbranch release];
    lblreminder = nil;
    
    btnYesRem = [UIButton buttonWithType:UIButtonTypeRoundedRect];
    btnYesRem.tag = 3001;
    [btnYesRem addTarget:self action:@selector(actionReminder:) forControlEvents:UIControlEventTouchUpInside];
    [btnYesRem setTitle:NSLocalizedString(@"yes", nil) forState:UIControlStateNormal];
    if ([headercolor length]>0) {
        [btnYesRem setTitleColor:[self colorWithHexString:headercolor] forState:UIControlStateNormal];
    } else {
        [btnYesRem setTitleColor:[self colorWithHexString:@"#78CFBF"] forState:UIControlStateNormal];
    }
    [btnYesRem setBackgroundColor:[UIColor whiteColor]];
    btnYesRem.frame = CGRectMake(CGRectGetMidX(self.view.frame)-(buttonWidth)-25, 55, buttonWidth, 30.0);
    [viewreminder addSubview:btnYesRem];
    if ([headercolor length]>0) {
        [btnYesRem.layer setBorderColor:[self colorWithHexString:headercolor].CGColor];
    } else {
        [btnYesRem.layer setBorderColor:[self colorWithHexString:@"#78CFBF"].CGColor];
    }
    [btnYesRem.layer setBorderWidth:1.0];
    //btnYesRem.layer.cornerRadius = 5;
    
    btnNoRem = [UIButton buttonWithType:UIButtonTypeRoundedRect];
    btnNoRem.tag = 3002;
    [btnNoRem addTarget:self action:@selector(actionReminder:) forControlEvents:UIControlEventTouchUpInside];
    [btnNoRem setTitle:NSLocalizedString(@"no", nil) forState:UIControlStateNormal];
    if ([headercolor length]>0) {
        [btnNoRem setTitleColor:[self colorWithHexString:headercolor] forState:UIControlStateNormal];
    } else {
        [btnNoRem setTitleColor:[self colorWithHexString:@"#78CFBF"] forState:UIControlStateNormal];
    }
    [btnNoRem setBackgroundColor:[UIColor whiteColor]];
    btnNoRem.frame = CGRectMake(CGRectGetMidX(self.view.frame)+25, 55, buttonWidth, 30.0);
    [viewreminder addSubview:btnNoRem];
    if ([headercolor length]>0) {
        [btnNoRem.layer setBorderColor:[self colorWithHexString:headercolor].CGColor];
    } else {
        [btnNoRem.layer setBorderColor:[self colorWithHexString:@"#78CFBF"].CGColor];
    }
    [btnNoRem.layer setBorderWidth:1.0];
    //btnNoRem.layer.cornerRadius = 5;
    
    if ([notification intValue]==1) {
        [self actionReminder:btnYesRem];
    } else if ([notification intValue]==0) {
        [self actionReminder:btnNoRem];
    }
    
    //labelheight = labelheight + 120 + 10;
    
    buttonsubmit = [UIButton buttonWithType:UIButtonTypeRoundedRect];
    [buttonsubmit addTarget:self action:@selector(submitForm:) forControlEvents:UIControlEventTouchUpInside];
    buttonsubmit.titleLabel.font = [UIFont boldSystemFontOfSize:18];
    [buttonsubmit setTitle:NSLocalizedString(@"confirm", nil) forState:UIControlStateNormal];
    if ([headerfontcolor length]>0) {
        [buttonsubmit setTitleColor:[self colorWithHexString:headerfontcolor] forState:UIControlStateNormal];
    } else {
        [buttonsubmit setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    }
    if ([headercolor length]>0) {
        [buttonsubmit setBackgroundColor:[self colorWithHexString:headercolor]];
    } else {
        [buttonsubmit setBackgroundColor:[self colorWithHexString:@"#78CFBF"]];
    }
    //float buttonSubmitWidth = 190.0f;
    buttonsubmit.frame = CGRectMake(5, self.view.frame.size.height-64, self.view.frame.size.width-10, 44.0);
    [scrollviewAdd1 addSubview:buttonsubmit];
    //buttonsubmit.layer.cornerRadius = 5;
    
    //labelheight = labelheight + 44;
    
    scrollViewHeight1 = 0.0f;
    
    for (UIView* view in scrollviewAdd1.subviews)
        scrollViewHeight1 += view.frame.size.height;
    
    //scrollviewAdd1.contentSize = CGSizeMake(scrollviewAdd1.frame.size.width, labelheight + 20);
    
    /*UITapGestureRecognizer *gestureRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:[self scrollviewAdd] action:@selector(touchesBegan:withEvent:)];
    gestureRecognizer.delegate = self;
    [scrollviewAdd addGestureRecognizer:gestureRecognizer]; */
}

- (void)aMethod:(id)sender
{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSString *hasLoggedInFB = [defaults objectForKey:@"hasLoggedInFB"];
    NSString *hasLoggedInMyApp = [defaults objectForKey:@"hasLoggedInMyApp"];
    
    if ([hasLoggedInFB isEqualToString:@"YES"] || [hasLoggedInMyApp isEqualToString:@"YES"])
    {
        [self geteventlist];
    }
    else
        [self.view addSubview:view_popoverMain];
}

- (void)actionConfirm:(id)sender
{
    UIButton *clicked = (UIButton *) sender;
    
    //Submit button
    if (clicked.tag==1001 || clicked.tag==1002)
    {
        [self displayContent1];
    } else {
        confirmStatus = @"3";
        reminderStatus = @"";
        [self submitAction];
    }

}

- (void)actionConfirm1:(id)sender
{

    UIButton *clicked = (UIButton *) sender;
    
    NSString *headercolor = @"";
    NSString *headerfontcolor = @"";
    if([listOfContent count]>0)
    {
        for (int i=0;i<[listOfContent count]; i++)
        {
            NSDictionary *dictionary = [listOfContent objectAtIndex:i];
            
            headercolor = [dictionary objectForKey:@"headercolor"];
            headerfontcolor = [dictionary objectForKey:@"headerfontcolor"];
        }
    }
    
    if (clicked.tag==2001) {
        confirmStatus = @"1";
        if ([headercolor length]>0) {
            [btnYesConf setBackgroundColor:[self colorWithHexString:headercolor]];
            [btnLaterConf setTitleColor:[self colorWithHexString:headercolor] forState:UIControlStateNormal];
        } else {
            [btnYesConf setBackgroundColor:[self colorWithHexString:@"#78CFBF"]];
            [btnLaterConf setTitleColor:[self colorWithHexString:@"#78CFBF"] forState:UIControlStateNormal];
        }
        if ([headerfontcolor length]>0) {
            [btnYesConf setTitleColor:[self colorWithHexString:headerfontcolor] forState:UIControlStateNormal];
            [btnLaterConf setBackgroundColor:[UIColor whiteColor]];
        } else {
            [btnYesConf setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
            [btnLaterConf setBackgroundColor:[UIColor whiteColor]];
        }
    } else {
        confirmStatus = @"2";
        if ([headerfontcolor length]>0) {
            [btnLaterConf setTitleColor:[self colorWithHexString:headerfontcolor] forState:UIControlStateNormal];
            [btnYesConf setBackgroundColor:[UIColor whiteColor]];
        } else {
            [btnLaterConf setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
            [btnYesConf setBackgroundColor:[UIColor whiteColor]];
        }
        if ([headercolor length]>0) {
            [btnYesConf setTitleColor:[self colorWithHexString:headercolor] forState:UIControlStateNormal];
            [btnLaterConf setBackgroundColor:[self colorWithHexString:headercolor]];
        } else {
            [btnYesConf setTitleColor:[self colorWithHexString:@"#78CFBF"] forState:UIControlStateNormal];
            [btnLaterConf setBackgroundColor:[self colorWithHexString:@"#78CFBF"]];
        }
    }
    
}

- (void)actionReminder:(id)sender
{
    UIButton *clicked = (UIButton *) sender;
    
    NSString *headercolor = @"";
    NSString *headerfontcolor = @"";
    if([listOfContent count]>0)
    {
        for (int i=0;i<[listOfContent count]; i++)
        {
            NSDictionary *dictionary = [listOfContent objectAtIndex:i];
            
            headercolor = [dictionary objectForKey:@"headercolor"];
            headerfontcolor = [dictionary objectForKey:@"headerfontcolor"];
        }
    }
    
    //Submit button
    if (clicked.tag==3001) {
        reminderStatus = @"1";
        if ([headercolor length]>0) {
            [btnNoRem setTitleColor:[self colorWithHexString:headercolor] forState:UIControlStateNormal];
            [btnYesRem setBackgroundColor:[self colorWithHexString:headercolor]];
        } else {
            [btnNoRem setTitleColor:[self colorWithHexString:@"#78CFBF"] forState:UIControlStateNormal];
            [btnYesRem setBackgroundColor:[self colorWithHexString:@"#78CFBF"]];
        }
        if ([headerfontcolor length]>0) {
            [btnYesRem setTitleColor:[self colorWithHexString:headerfontcolor] forState:UIControlStateNormal];
            [btnNoRem setBackgroundColor:[UIColor whiteColor]];
        } else {
            [btnYesRem setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
            [btnNoRem setBackgroundColor:[UIColor whiteColor]];
        }
    } else {
        reminderStatus = @"0";
        if ([headerfontcolor length]>0) {
            [btnNoRem setTitleColor:[self colorWithHexString:headerfontcolor] forState:UIControlStateNormal];
            [btnYesRem setBackgroundColor:[UIColor whiteColor]];
        } else {
            [btnNoRem setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
            [btnYesRem setBackgroundColor:[UIColor whiteColor]];
        }
        if ([headercolor length]>0) {
            [btnYesRem setTitleColor:[self colorWithHexString:headercolor] forState:UIControlStateNormal];
            [btnNoRem setBackgroundColor:[self colorWithHexString:headercolor]];
        } else {
            [btnYesRem setTitleColor:[self colorWithHexString:@"#78CFBF"] forState:UIControlStateNormal];
            [btnNoRem setBackgroundColor:[self colorWithHexString:@"#78CFBF"]];
        }
    }
    
}

- (void)submitForm:(id)sender
{
    NSString *cutoff;
    if([listOfContent count]>0)
    {
        for (int i=0;i<[listOfContent count]; i++)
        {
            NSDictionary *dictionary = [listOfContent objectAtIndex:i];
            
            cutoff = [dictionary objectForKey:@"cutoff"];

        }
    }
    
    NSDateFormatter *dateFormatter=[NSDateFormatter new];
    [dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    [dateFormatter setTimeZone:[NSTimeZone timeZoneWithName:@"UTC"]];
    
    NSDate *dateOfCutoff=[dateFormatter dateFromString:cutoff];
    
    if ([[NSDate date] compare:dateOfCutoff] == NSOrderedDescending) {
        
        [self displayAlert:NSLocalizedString(@"cutoff_passed", nil)];
        [self displayContent];
        
    } else {
        NSCharacterSet* notDigits = [[NSCharacterSet decimalDigitCharacterSet] invertedSet];
        
        if ([confirmStatus isEqualToString:@""]) {
            NSString *msg = [NSString stringWithFormat:@"%@ %@!", NSLocalizedString(@"confirmation", nil), NSLocalizedString(@"is_required", nil)];
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Information" message:msg delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
            [alert show];
            //[alert release];
            alert = nil;
        } else if ([tf.text isEqualToString:@""]) {
            NSString *msg = [NSString stringWithFormat:@"%@ %@!", NSLocalizedString(@"pax", nil), NSLocalizedString(@"is_required", nil)];
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Information" message:msg delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
            [alert show];
            //[alert release];
            alert = nil;
        } else if ([tf.text rangeOfCharacterFromSet:notDigits].location != NSNotFound) {
            // newString not consists of the digits 0 through 9
            NSString *msg = [NSString stringWithFormat:NSLocalizedString(@"pax_numbers_only", nil)];
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Information" message:msg delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
            [alert show];
            //[alert release];
            alert = nil;
        } else if ([reminderStatus isEqualToString:@""]) {
            NSString *msg = [NSString stringWithFormat:@"%@ %@!", NSLocalizedString(@"reminder", nil), NSLocalizedString(@"is_required", nil)];
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Information" message:msg delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
            [alert show];
            //[alert release];
            alert = nil;
        }
        else
        {
            if ([max_guesstotal intValue]>0) {
                if ([tf.text intValue] > [max_guesstotal intValue])
                {
                    NSString *strPaxlimit = [NSLocalizedString(@"pax_exceed_limit", nil) stringByReplacingOccurrencesOfString:@"<limit>" withString:max_guesstotal];
                    NSString *msg = [NSString stringWithFormat:@"%@", strPaxlimit];
                    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Information" message:msg delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
                    [alert show];
                    //[alert release];
                    alert = nil;
                }
            }
            
            [self submitAction];
            
        }
    }
}

-(void) submitAction {
    NSString *urlAddressSubmit = [[NSString alloc] initWithFormat:@"%@event_rsvp_update.php?db=%@&userid=%@&udid=%@&cuid=%@&event_id=%@&confirmation=%@&reminder=%@&pax=%@", appdelegate.user_apipath, appdelegate.db, appdelegate.user, appdelegate.uuid, appdelegate.customer_id, appdelegate.event_id, confirmStatus, reminderStatus, [tf text]];
    currentRequest = @"submit";
    [self connectToServer:urlAddressSubmit];
    //NSLog(@"urlAddressSubmit:: %@", urlAddressSubmit);
}


- (IBAction)logIn:(id)sender
{
    NSString *nibFileName;
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone) {
        nibFileName = @"SettingViewController";
    } else {
        nibFileName = @"SettingViewController_ipad";
    }
    SettingViewController *settingViewController = [[SettingViewController alloc] initWithNibName:nibFileName bundle:nil];
    appdelegate.shouldReturnToPrevPage = YES;
    appdelegate.originVC = self;
    [appdelegate.navController pushViewController:settingViewController animated:NO];
    
    [view_popoverMain removeFromSuperview];
}

- (IBAction)closeLoginPopup:(id)sender
{
    [view_popoverMain removeFromSuperview];
}

-(NSString *) urlEncode:(NSString *) theStr {
    NSString *encoded = (NSString *)CFBridgingRelease(CFURLCreateStringByAddingPercentEscapes(
                                                                                              NULL,
                                                                                              (CFStringRef)theStr,
                                                                                              NULL,
                                                                                              (CFStringRef)@"!*'\"();:@&=+$,/?%#[]% ",
                                                                                              //																		NSUnicodeStringEncoding );
                                                                                              kCFStringEncodingUTF8 ));
    return encoded;
}

#pragma mark -
#pragma mark WEPopoverControllerDelegate implementation

- (void)popoverControllerDidDismissPopover:(WEPopoverController *)thePopoverController {
	//Safe to release the popover here
	self.paxPopoverController = nil;
}

- (BOOL)popoverControllerShouldDismissPopover:(WEPopoverController *)thePopoverController {
	//The popover is automatically dismissed if you click outside it, unless you return NO here
	return YES;
}

- (WEPopoverContainerViewProperties *)improvedContainerViewProperties {
	
	WEPopoverContainerViewProperties *props = [WEPopoverContainerViewProperties alloc];
	NSString *bgImageName = nil;
	CGFloat bgMargin = 0.0;
	CGFloat bgCapSize = 0.0;
	CGFloat contentMargin = 4.0;
	
	bgImageName = @"popoverBg.png";
	
	// These constants are determined by the popoverBg.png image file and are image dependent
	bgMargin = 13; // margin width of 13 pixels on all sides popoverBg.png (62 pixels wide - 36 pixel background) / 2 == 26 / 2 == 13
	bgCapSize = 31; // ImageSize/2  == 62 / 2 == 31 pixels
	
	props.leftBgMargin = bgMargin;
	props.rightBgMargin = bgMargin;
	props.topBgMargin = bgMargin;
	props.bottomBgMargin = bgMargin;
	props.leftBgCapSize = bgCapSize;
	props.topBgCapSize = bgCapSize;
	props.bgImageName = bgImageName;
	props.leftContentMargin = contentMargin;
	props.rightContentMargin = contentMargin - 1; // Need to shift one pixel for border to look correct
	props.topContentMargin = contentMargin;
	props.bottomContentMargin = contentMargin;
	
	props.arrowMargin = 4.0;
	
	props.upArrowImageName = @"popoverArrowUp.png";
	props.downArrowImageName = @"popoverArrowDown.png";
	props.leftArrowImageName = @"popoverArrowLeft.png";
	props.rightArrowImageName = @"popoverArrowRight.png";
	return props;
}

- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField {
    
    //NSLog(@"max_guesstotal:%@", max_guesstotal);
    if ([max_guesstotal intValue]>0) {
        if(textField == tf)
        {
            
            [paxTableView reloadData];
            
            if (paxContent == nil )
            {
                paxContent = [[UIViewController alloc] init];
                paxContent.view = paxTableView;
            }
            else
            {
                if (paxContent.view != paxTableView) {
                    paxContent.view = paxTableView;
                }
            }
            
            if (!self.paxPopoverController)
            {
                self.paxPopoverController = [[WEPopoverController alloc] initWithContentViewController:paxContent];
                self.paxPopoverController.popoverContentSize = CGSizeMake(400.0f, 600.0f);
                
                if ([self.paxPopoverController respondsToSelector:@selector(setContainerViewProperties:)])
                    [self.paxPopoverController setContainerViewProperties:[self improvedContainerViewProperties]];
                
                self.paxPopoverController.delegate = self;
                self.paxPopoverController.passthroughViews = [NSArray arrayWithObject:appdelegate.navController.navigationBar];
                [self.paxPopoverController presentPopoverFromRect:textField.bounds inView:textField permittedArrowDirections:UIPopoverArrowDirectionUp animated:NO];
            }
            else
            {
                [self.paxPopoverController dismissPopoverAnimated:NO];
                self.paxPopoverController = nil;
            }
            return NO;  // Hide both keyboard and blinking cursor.
        }
    }
    
    return YES;
}

- (void)separateDateTime:(id)sender
{
    /*[listAvaiDateTime removeAllObjects];
    [sender reloadData];
    
    //Find the time available for the particular date selected
    // NSPredicate *predicate = [NSPredicate predicateWithFormat:@"dateReserv CONTAINS %@", tf2.text];
    NSMutableArray *listDateTime = [self createMutableArray:listAvaiDateTimeOri];
    
    NSDictionary *dicDatetime;
    if ([listDateTime count]>0) {
        for (int k=0; k<[listDateTime count]; k++) {
            NSDictionary *data = [listDateTime objectAtIndex:k];
            NSString *dateReserv = [data objectForKey:@"dateReserv"];
            
            //Format it to time format
            NSDateFormatter *df = [[NSDateFormatter alloc] init];
            [df setDateFormat:@"HH:mm"];
            NSDate *d = [df dateFromString:dateReserv];
            [df setDateFormat:@"HH:mm"];
            NSString *DateString = [df stringFromDate:d];
            
            dicDatetime = [NSDictionary dictionaryWithObjectsAndKeys:DateString,@"dateReserv",nil];
            [self.listAvaiDateTime addObject:dicDatetime];
        }
    } */
}

- (BOOL) isConnectionAvailable
{
    Reachability *networkReachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus networkStatus = [networkReachability currentReachabilityStatus];
    if (networkStatus == NotReachable) {
        //NSLog(@"There IS NO internet connection");
        return FALSE;
    } else {
        //NSLog(@"There IS internet connection");
        return TRUE;
    }
}

- (void) geteventlist {
    
    [view_eventAdd setHidden:NO];
    [view_eventAdd1 setHidden:YES];

    NSString *urlAddress = [[NSString alloc] initWithFormat:@"%@event_rsvp.php?db=%@&userid=%@&udid=%@&cuid=%@&event_id=%@",appdelegate.user_apipath,appdelegate.db,appdelegate.user,appdelegate.uuid,appdelegate.customer_id,appdelegate.event_id];
    currentRequest = @"getEventList";
    //NSLog(@"urlAddressEventRsvp:: %@", urlAddress);
    [self connectToServer:urlAddress];
    
}

- (void)dealloc {
    view_eventAdd = nil;
    scrollviewAdd = nil;
    paxPopoverController = nil;
    listOfpax = nil;
    tf = nil;
    view_popoverMain = nil;
    //[super dealloc];
}
@end
