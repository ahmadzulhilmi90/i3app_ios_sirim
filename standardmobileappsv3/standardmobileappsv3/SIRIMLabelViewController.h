//
//  LoyaltyViewController.h
//  standardmobileapps
//
//  Created by Hazwan on 11/25/13.
//  Copyright (c) 2013 Maggie. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MessageUI/MessageUI.h>

@interface SIRIMLabelViewController : UIViewController <UITextFieldDelegate, MFMailComposeViewControllerDelegate>

@property (strong, nonatomic) IBOutlet UITextField *searchTextField;
@property (strong, nonatomic) IBOutlet UIButton *searchBtn;
@property (strong, nonatomic) IBOutlet UILabel *LabelMainTitle;
@property (strong, nonatomic) IBOutlet UILabel *Label1Title;
@property (strong, nonatomic) IBOutlet UILabel *Label1Value;
@property (strong, nonatomic) IBOutlet UILabel *Label1aValue;
@property (strong, nonatomic) IBOutlet UILabel *Label1bValue;
@property (strong, nonatomic) IBOutlet UILabel *Label1cValue;
@property (strong, nonatomic) IBOutlet UILabel *Label2Title;
@property (strong, nonatomic) IBOutlet UILabel *Label2Value;
@property (strong, nonatomic) IBOutlet UILabel *Label3Title;
@property (strong, nonatomic) IBOutlet UILabel *Label3Value;
@property (strong, nonatomic) IBOutlet UILabel *LabelError;
@property (strong, nonatomic) IBOutlet UITextView *TextView1Value;

@property (strong, nonatomic) IBOutlet UIButton *qrBtn;

@end
