//
//  LoyaltyViewController.m
//  standardmobileapps
//
//  Created by Hazwan on 11/25/13.
//  Copyright (c) 2013 Maggie. All rights reserved.
//

#import <QuartzCore/QuartzCore.h>
#import "SIRIMLabelViewController.h"
#import "AppDelegate.h"
#import "LoadingView.h"
#import "SBJSON.h"
#import "RootViewController.h"
#import "ViewController.h"
#import "InitialSlidingViewController.h"
#import "BTSlideInteractor.h"
#import "QRCodeViewController.h"
#import "NSString+HTML.h"

@interface SIRIMLabelViewController ()
@property (strong, nonatomic) BTSlideInteractor *interactor;
@end

InitialSlidingViewController *modalController;

AppDelegate *appdelegate;
__strong LoadingView *loadingView;
__strong NSString *currentRequest;
//__strong NSMutableData *receivedData;

@implementation SIRIMLabelViewController
@synthesize searchBtn, searchTextField, LabelError, Label1Title, Label1Value, Label2Title, Label2Value, Label3Title, Label3Value, LabelMainTitle, Label1aValue, Label1bValue,Label1cValue, qrBtn, TextView1Value;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    //self.title = appdelegate.loyaltytitle;
    
    self.navigationItem.backBarButtonItem = [[UIBarButtonItem alloc]initWithTitle:@"" style:UIBarButtonItemStylePlain target:nil action:nil];
    
    appdelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    
    //self.title = cont_title;
    appdelegate.homeclicked = NO;
    
    CGRect rect = CGRectMake(0,0,20,20);
    UIGraphicsBeginImageContext( rect.size );
    [[UIImage imageNamed:@"home.png"] drawInRect:rect];
    UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    UIBarButtonItem *myHomeButton = [[UIBarButtonItem alloc] initWithImage:image style:UIBarButtonItemStyleDone target:self action:@selector(myHomeAction:)];
    
    
    NSArray *myButtonArray = [[NSArray alloc] initWithObjects:myHomeButton, nil];
    
    self.navigationItem.rightBarButtonItems = myButtonArray;
    
}

- (void)viewDidAppear:(BOOL)animated {
    [LabelMainTitle setFrame:CGRectMake(5, self.view.frame.size.height/8, self.view.frame.size.width-10, 50)];
    
    [searchTextField setFrame:CGRectMake(10,self.view.frame.size.height/8+60,self.view.frame.size.width-20,30)];
    [searchBtn setFrame:CGRectMake(self.view.frame.size.width/2-45,self.view.frame.size.height/8+100,90,30)];
    [[searchBtn layer] setBorderWidth:1.0];
    [[searchBtn layer] setBorderColor:[UIColor grayColor].CGColor];
    [[searchBtn layer] setCornerRadius:6.0];
    
    //QR Scan Button layout
    //self.view.frame.size.width/5
    [qrBtn setFrame:CGRectMake(0, self.view.frame.size.height-self.view.frame.size.width/5, self.view.frame.size.width, self.view.frame.size.width/5)];
    
    if (![appdelegate.backgroundT isEqualToString:@""]) {
        
        NSString *filePath = appdelegate.backgroundT;
        
        NSRange end = [filePath rangeOfString:@"/" options:NSBackwardsSearch];
        NSString *shortString;
        if (end.location != NSNotFound) {
            shortString =[filePath substringWithRange:NSMakeRange(end.location+1, filePath.length-(end.location+1))];
        } else {
            shortString = filePath;
        }
        
        BOOL success;
        
        // Create a FileManager object, we will use this to check the status
        // of the database and to copy it over if required
        NSFileManager *fileManager = [NSFileManager defaultManager];
        
        // Check if the database has already been created in the users filesystem
        success = [fileManager fileExistsAtPath:[[[NSBundle mainBundle] resourcePath] stringByAppendingPathComponent:shortString]];
        
        UIImageView *imgV = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height)];
        //[self addSubview:imgV];
        
        //__block UIActivityIndicatorView *activityIndicator;
        __weak UIImageView *weakImageView = imgV;
        NSString *imagePath;
        if(success) {
            imagePath = [[[NSBundle mainBundle] resourcePath] stringByAppendingPathComponent:shortString];
            [weakImageView setImage:[UIImage imageNamed:shortString]];
            
            //NSLog(@"shortStringimagePath : %@", imagePath);
            imgV.contentMode  = UIViewContentModeScaleAspectFit;
            [imgV setClipsToBounds:YES];
            
            UIGraphicsBeginImageContext(self.view.frame.size);
            //CGRect rect=CGRectMake(0,-64,self.view.bounds.size.width,self.view.bounds.size.height+64);
            [imgV.image drawInRect:self.view.bounds];
            //[imgV.image drawInRect:rect];
            UIImage *image1 = UIGraphicsGetImageFromCurrentImageContext();
            UIGraphicsEndImageContext();
            self.view.backgroundColor = [UIColor colorWithPatternImage:image1];
        } else {
            imagePath = filePath;
            NSURL *imageURL = [NSURL URLWithString:imagePath];
            NSData *imageData = [NSData dataWithContentsOfURL:imageURL];
            //self.backgroundColor = [UIColor redColor];
            UIGraphicsBeginImageContext(self.view.frame.size);
            //CGRect rect=CGRectMake(0,-64,self.view.bounds.size.width,self.view.bounds.size.height+64);
            //[[UIImage imageWithData:imageData] drawInRect:rect];
            [[UIImage imageWithData:imageData] drawInRect:self.view.bounds];
            UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
            UIGraphicsEndImageContext();
            self.view.backgroundColor = [UIColor colorWithPatternImage:image];
        }
    } else {
        self.view.backgroundColor = [UIColor clearColor];
    }
    
    //NSLog(@"QRUrl:%@",appdelegate.QRUrl);
    if (appdelegate.QRUrl.length>0) {
        //NSString *codeEncrypt = @"";
        currentRequest = @"QRCode";
        //QRUrl = [QRUrl stringByReplacingOccurrencesOfString:@" " withString:@"+"];
        //QRUrl = [[NSString alloc] initWithFormat:@"https://sqasiapps.sirim.my/index.html?+nMscirDRBrDU/XJMlzPcQ=="];
        //if ([QRUrl rangeOfString:@"?"].location == NSNotFound) {
            //NSLog(@"string does not contain bla");
        //} else {
            //NSArray *items = [QRUrl componentsSeparatedByString:@"?"];   //take the one array for split the string
            //codeEncrypt = [items objectAtIndex:1];
            //codeEncrypt = [codeEncrypt stringByReplacingOccurrencesOfString:@"+" withString:@"111"];
            //codeEncrypt = [codeEncrypt stringByReplacingOccurrencesOfString:@"=" withString:@"110"];
            //NSLog(@"codeEncrypt :: %@",codeEncrypt);
            NSString *urlAddress3 = [[NSString alloc] initWithFormat:@"http://app.getsnapps.com/api/api_sirim.php?devicetype=IOS&key=%@", [self urlEncode:appdelegate.QRUrl]];
            //urlAddress3 = [self urlEncode:urlAddress3];
            //NSString *urlAddress3 = [[NSString alloc] initWithFormat:@"%@", QRUrl];
            //NSLog(@"urlAddress3:%@",urlAddress3);
            
            [self connectToServer:urlAddress3];
            //NSLog(@"string contains bla!");
        //}
    }
}

//Slide Menu
- (void)willAnimateRotationToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation duration:(NSTimeInterval)duration
{
    //NSLog(@"view frame=%@ bounds=%@", NSStringFromCGRect(self.view.frame), NSStringFromCGRect(self.view.bounds));
}


- (void)showModalButtonWasTouched
{
    self.interactor = [[BTSlideInteractor alloc] init];
    self.interactor.presenting = YES;
    
    //BTModalViewController *modalController = [self.storyboard instantiateViewControllerWithIdentifier:@"ModalViewController"];
    modalController = [[InitialSlidingViewController alloc] init];
    modalController.modalPresentationStyle = UIModalPresentationCustom;
    modalController.transitioningDelegate = self.interactor;
    [self.navigationController presentViewController:modalController animated:YES completion:nil];
    //[self.navigationController popToViewController:modalController animated:NO];
    //[self.navigationController pushViewController:modalController animated:YES];
}
// End Slide Menu

-(void)myHomeAction:(UIBarButtonItem *)sender{
    
    appdelegate.nextPage = @"";
    
    if ([appdelegate.listOfVerMenu count]>0) {
        if (appdelegate.homeclicked==NO) {
            appdelegate.homeclicked = YES;
            [self showModalButtonWasTouched];
        } else {
            appdelegate.homeclicked = NO;
            [modalController dismissViewControllerAnimated:YES completion:nil];
        }
    } else {
        //AppDelegate *appdelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
        //NSLog(@"View Controllers : %@",[appdelegate.navController viewControllers]);
        NSMutableArray *navigationArray = [[NSMutableArray alloc] initWithArray: appdelegate.navController.viewControllers];
        
        for (UIViewController* viewController in appdelegate.navController.viewControllers) {
            
            //This if condition checks whether the viewController's class is MyGroupViewController
            // if true that means its the MyGroupViewController (which has been pushed at some point)
            if ([viewController isKindOfClass:[RootViewController class]] ) {
                
                // Here viewController is a reference of UIViewController base class of MyGroupViewController
                // but viewController holds MyGroupViewController  object so we can type cast it here
                //ThemeViewController *themeViewController = (ThemeViewController*)viewController;
                //[self.navigationController popToViewController:themeViewController animated:YES];
                [navigationArray removeObjectAtIndex: 0];
            }
        }
        appdelegate.navController.viewControllers = navigationArray;
        for (UIViewController* viewController in appdelegate.navController.viewControllers) {
            
            //This if condition checks whether the viewController's class is MyGroupViewController
            // if true that means its the MyGroupViewController (which has been pushed at some point)
            if ([viewController isKindOfClass:[ViewController class]] ) {
                
                // Here viewController is a reference of UIViewController base class of MyGroupViewController
                // but viewController holds MyGroupViewController  object so we can type cast it here
                ViewController *themeViewController = (ViewController*)viewController;
                [appdelegate.navController popToViewController:themeViewController animated:NO];
                //[self.navigationController popToViewController:viewController animated:YES];
            }
        }
        //NSLog(@"View Controllers111 : %@",appdelegate.navController.viewControllers);
    }
    
}

-(void) viewDidDisappear:(BOOL)animated {
    
    [super viewDidDisappear:animated];
    
    appdelegate.homeclicked = NO;
    appdelegate.QRUrl = @"";
    [modalController dismissViewControllerAnimated:YES completion:nil];
    
}

-(IBAction)search:(id)sender {
    appdelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    
    currentRequest = @"SearchList";
    NSString *urlAddress2 = [[NSString alloc] initWithFormat:@"http://app.getsnapps.com/api/sirimlabel.php?key=%@",[self urlEncode:searchTextField.text] ];
    //NSLog(@"urlAddress2 loyalty :: %@",urlAddress2);
    [self connectToServer:urlAddress2];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(IBAction)qrcodeScan:(id)sender {
    NSString *nibFileName;
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone) {
        nibFileName = @"QRCodeViewController";
    } else {
        nibFileName = @"QRCodeViewController_ipad";
    }
    QRCodeViewController *s = [[QRCodeViewController alloc] initWithNibName:nibFileName bundle:[NSBundle mainBundle]];
    //[s setLoyalty_id:[cell loyalty_id]];
    [appdelegate.navController pushViewController:s animated:NO];
}

#pragma mark Connect To API
-(void) connectToServer:(NSString *) urlPath {
    [self addLoadingView];
	NSString *urlAddress =[NSString stringWithFormat:@"%@", urlPath];
	
    [self getJsonResponse:urlAddress success:^(NSDictionary *responseDict) {
        
        //NSLog(@"%@",responseDict);
        [self successfunction:responseDict];
        
    } failure:^(NSError *error) {
        // release the connection, and the data object
        
        [self removeLoadingView];
        [self displayAlert:NSLocalizedString(@"MSG_CONNECTION_ERROR", nil)];
    }];
}

-(void)getJsonResponse : (NSString *)urlStr success : (void (^)(NSDictionary *responseDict))success failure:(void(^)(NSError* error))failure
{
    
    NSURLSession * session = [NSURLSession sharedSession];
    NSURL * url = [NSURL URLWithString: urlStr];
    //NSLog(@"urlgetJsonResponse: %@",url);
    
    // Asynchronously Api is hit here
    NSURLSessionDataTask * dataTask = [session dataTaskWithURL:url completionHandler:^(NSData *data, NSURLResponse *response, NSError *error)
                                       {
                                           
                                           //NSLog(@"%@",data);
                                           NSDictionary * json;
                                           if ([data length]>0) {
                                           json  = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
                                           //NSLog(@"json:: %@",json); //"Url request failed" = "Url request failed";
                                           success(json);
                                           } else {
                                               json = nil;
                                               //NSString *errorMsg = [[NSString alloc] initWithFormat:@"%@ %@", NSLocalizedString(@"Your request failed", nil), NSLocalizedString(@"MSG_CONNECTION_ERROR", nil)];
                                               dispatch_async(dispatch_get_main_queue(), ^ {
                                                   [self removeLoadingView];
                                                   //[self displayAlert:errorMsg];
                                               });
                                           }
                                           
                                           
                                       }];
    
    [dataTask resume] ; // Executed First
    
    
}

-(void) successfunction:(NSDictionary *)responseDict {
    dispatch_async(dispatch_get_main_queue(), ^{
        //NSString *jsonString = [responseDict JSONRepresentation];
        [self removeLoadingView];
        
        if ([currentRequest isEqualToString:@"SearchList"])
        {
            [LabelError setHidden:YES];
            [Label1Title setHidden:YES];
            [Label1Value setHidden:YES];
            [TextView1Value setHidden:YES];
            [Label1aValue setHidden:YES];
            [Label1bValue setHidden:YES];
            [Label1cValue setHidden:YES];
            [Label2Title setHidden:YES];
            [Label2Value setHidden:YES];
            [Label3Title setHidden:YES];
            [Label3Value setHidden:YES];
            
            if ([searchTextField.text length]>0) {
                
                NSDictionary *dictionary = responseDict;
                //NSLog(@"dictionary:%@", dictionary);
                //NSLog(@"Error:%@", [dictionary objectForKey:@"Error"]);
                //NSLog(@"Response:%@", [dictionary objectForKey:@"Response"]);
                
                //NSDictionary *dicErr = [dictionary objectForKey:@"Error"];
                NSMutableArray *arrResponse = [dictionary objectForKey:@"Response"];
                //NSLog(@"arrResponse:%@", arrResponse);
                int height = self.view.frame.size.height/8+150;
                
                NSDictionary *dicResponse = [arrResponse objectAtIndex:0];
                //NSLog(@"dicrResponse111:%@", dicResponse);
                
                [Label1Title setHidden:NO];
                [Label1Title setFrame:CGRectMake(5, height, self.view.frame.size.width-10, 30)];
                //Label1Title.text = NSLocalizedString(@"Product Status", nil);
                Label1Title.text = [[NSString alloc] initWithFormat:@" %@", NSLocalizedString(@"Product Status", nil)];
                Label1Title.numberOfLines = 0;
                Label1Title.lineBreakMode = NSLineBreakByWordWrapping;
                Label1Title.clipsToBounds = YES;
                Label1Title.backgroundColor = [UIColor colorWithRed:255/255.0 green:195/255.0 blue:0/255.0 alpha:1];
                Label1Title.textColor = [UIColor blackColor];
                Label1Title.textAlignment = NSTextAlignmentLeft;
                CGRect newFrame1Title = Label1Title.frame;
                newFrame1Title.size.height = [self getLabelHeight:Label1Title];
                Label1Title.frame = newFrame1Title;
                //[self.view addSubview:Label1Title];
                height = height + Label1Title.frame.size.height+5;
                
                //[Label1Value setHidden:YES];
                [TextView1Value setHidden:NO];
                /*[Label1aValue setHidden:YES];
                [Label1bValue setHidden:YES];
                [Label1cValue setHidden:YES];
                [Label1Value setFrame:CGRectMake(5, height, self.view.frame.size.width-10, 30)];
                [Label1aValue setFrame:CGRectMake(5, height, self.view.frame.size.width-10, 30)];
                [Label1bValue setFrame:CGRectMake(5, height, self.view.frame.size.width-10, 30)];
                [Label1cValue setFrame:CGRectMake(5, height, self.view.frame.size.width-10, 30)]; */
                if ([[dicResponse objectForKey:@"Serial No."] length]>0) {
                    //Label1Value.text = NSLocalizedString(@"Certified", nil);
                    TextView1Value.text = NSLocalizedString(@"Certified", nil);
                    /*Label1aValue.text = @"";
                    Label1bValue.text = @"";
                    Label1cValue.text = @""; */
                } else {
                    //Label1Value.text = NSLocalizedString(@"For this serial number, please contact us at 03-55446400 or email us at cserviceqas@sirim.my", nil);
                    //Label1Value.text = NSLocalizedString(@"For this serial number, please contact us at ", nil);
                    TextView1Value.text = NSLocalizedString(@"For this serial number, please contact us at 03-55446400 or email us at cserviceqas@sirim.my", nil);
                    TextView1Value.editable = NO;
                    TextView1Value.dataDetectorTypes = UIDataDetectorTypeAll;
                    /*Label1aValue.text = NSLocalizedString(@"03-55446400", nil);
                    Label1bValue.text = NSLocalizedString(@" or email us at ", nil);
                    Label1cValue.text = NSLocalizedString(@"cserviceqas@sirim.my", nil); */
                    
                    //Label1Value.text = [NSString stringWithFormat: @"%@ %@ %@ %@", NSLocalizedString(@"For this serial number, please contact us at", nil), strPhone, NSLocalizedString(@"or email us at", nil), strEmail];
                }
                /*Label1Value.numberOfLines = 0;
                Label1Value.lineBreakMode = NSLineBreakByWordWrapping;
                Label1Value.clipsToBounds = YES;
                Label1Value.backgroundColor = [UIColor clearColor];
                if ([appdelegate.backgroundT isEqualToString:@""] || [appdelegate.backgroundT isEqualToString:@"<null>"]) {
                    Label1Value.textColor = [UIColor blackColor];
                } else {
                    Label1Value.textColor = [UIColor whiteColor];
                }
                Label1Value.textAlignment = NSTextAlignmentLeft; */
                
                if ([appdelegate.backgroundT isEqualToString:@""] || [appdelegate.backgroundT isEqualToString:@"<null>"]) {
                    TextView1Value.textColor = [UIColor blackColor];
                } else {
                    TextView1Value.textColor = [UIColor whiteColor];
                }
                //NSAttributedString *attributedString = [[NSAttributedString alloc] initWithString:@"cserviceqas@sirim.my" attributes:@{ NSLinkAttributeName: [NSURL URLWithString:@"mailto:cserviceqas@sirim.my"] }];
                //TextView1Value.attributedText = attributedString;
                CGFloat heightTextView = ceilf([TextView1Value sizeThatFits:TextView1Value.frame.size].height);
                [TextView1Value setFrame:CGRectMake(5, height, self.view.frame.size.width-10, heightTextView)];
                height = height + heightTextView+10;
                
                /*Label1aValue.numberOfLines = 0;
                Label1aValue.lineBreakMode = NSLineBreakByWordWrapping;
                Label1aValue.clipsToBounds = YES;
                Label1aValue.backgroundColor = [UIColor clearColor];
                //if ([appdelegate.backgroundT isEqualToString:@""] || [appdelegate.backgroundT isEqualToString:@"<null>"]) {
                Label1aValue.textColor = [UIColor blueColor];
                //} else {
                //Label1aValue.textColor = [UIColor whiteColor];
                //}
                NSMutableAttributedString *Label1aString = [[NSMutableAttributedString alloc] initWithString:Label1aValue.text];
                UIColor* textColor = [UIColor blueColor];
                [Label1aString setAttributes:@{NSForegroundColorAttributeName:textColor,NSUnderlineStyleAttributeName:[NSNumber numberWithInteger:NSUnderlineStyleSingle]} range:NSMakeRange(0,[Label1aString length])];
                [Label1aValue setAttributedText:Label1aString];
                Label1aValue.textAlignment = NSTextAlignmentLeft;
                
                Label1bValue.numberOfLines = 0;
                Label1bValue.lineBreakMode = NSLineBreakByWordWrapping;
                Label1bValue.clipsToBounds = YES;
                Label1bValue.backgroundColor = [UIColor clearColor];
                if ([appdelegate.backgroundT isEqualToString:@""] || [appdelegate.backgroundT isEqualToString:@"<null>"]) {
                    Label1bValue.textColor = [UIColor blackColor];
                } else {
                    Label1bValue.textColor = [UIColor whiteColor];
                }
                Label1bValue.textAlignment = NSTextAlignmentLeft;
                
                Label1cValue.numberOfLines = 0;
                Label1cValue.lineBreakMode = NSLineBreakByWordWrapping;
                Label1cValue.clipsToBounds = YES;
                Label1cValue.backgroundColor = [UIColor clearColor];
                //if ([appdelegate.backgroundT isEqualToString:@""] || [appdelegate.backgroundT isEqualToString:@"<null>"]) {
                Label1cValue.textColor = [UIColor blueColor];
                //} else {
                //Label1cValue.textColor = [UIColor whiteColor];
                //}
                NSMutableAttributedString *Label1cString = [[NSMutableAttributedString alloc] initWithString:Label1cValue.text];
                [Label1cString setAttributes:@{NSForegroundColorAttributeName:textColor,NSUnderlineStyleAttributeName:[NSNumber numberWithInteger:NSUnderlineStyleSingle]} range:NSMakeRange(0,[Label1cString length])];
                [Label1cValue setAttributedText:Label1cString];
                Label1cValue.textAlignment = NSTextAlignmentLeft;
                
                float widthIs =
                [Label1Value.text
                 boundingRectWithSize:Label1Value.frame.size
                 options:NSStringDrawingUsesLineFragmentOrigin
                 attributes:@{ NSFontAttributeName:Label1Value.font }
                 context:nil]
                .size.width;
                float widthIs1a = [Label1aValue.text
                                   boundingRectWithSize:Label1aValue.frame.size
                                   options:NSStringDrawingUsesLineFragmentOrigin
                                   attributes:@{ NSFontAttributeName:Label1aValue.font }
                                   context:nil]
                .size.width;
                float widthIs1b = [Label1bValue.text
                                   boundingRectWithSize:Label1bValue.frame.size
                                   options:NSStringDrawingUsesLineFragmentOrigin
                                   attributes:@{ NSFontAttributeName:Label1bValue.font }
                                   context:nil]
                .size.width;
                float widthIs1c = [Label1cValue.text
                                   boundingRectWithSize:Label1cValue.frame.size
                                   options:NSStringDrawingUsesLineFragmentOrigin
                                   attributes:@{ NSFontAttributeName:Label1cValue.font }
                                   context:nil]
                .size.width;
                float width = 5;
                if (width+widthIs<self.view.frame.size.width) {
                    [Label1Value setFrame:CGRectMake(width, height, widthIs, 18)];
                    width = width+widthIs;
                    //height = height + Label1Value.frame.size.height;
                } else {
                    [Label1Value setFrame:CGRectMake(5, height, self.view.frame.size.width, 18)];
                    CGRect newFrame1Value = Label1Value.frame;
                    newFrame1Value.size.height = [self getLabelHeight:Label1Value];
                    Label1Value.frame = newFrame1Value;
                    height = height + Label1Value.frame.size.height;
                    width = width+widthIs-self.view.frame.size.width;
                }
                if (width+widthIs1a<self.view.frame.size.width) {
                    [Label1aValue setFrame:CGRectMake(width, height, widthIs1a, 18)];
                    width = width+widthIs1a;
                } else {
                    height = height + Label1aValue.frame.size.height;
                    [Label1aValue setFrame:CGRectMake(5, height, widthIs1a, 18)];
                    width = 5+widthIs1a;
                }
                if (width+widthIs1b<self.view.frame.size.width) {
                    [Label1bValue setFrame:CGRectMake(width, height, widthIs1b, 18)];
                    width = width+widthIs1b;
                } else {
                    height = height + Label1bValue.frame.size.height;
                    [Label1bValue setFrame:CGRectMake(5, height, widthIs1b, 18)];
                    width = 5+widthIs1b;
                }
                if (width+widthIs1c<self.view.frame.size.width) {
                    [Label1cValue setFrame:CGRectMake(width, height, widthIs1c, 18)];
                    width = width+widthIs1c;
                } else {
                    height = height + Label1cValue.frame.size.height;
                    [Label1cValue setFrame:CGRectMake(5, height, widthIs1c, 18)];
                    width = 5+widthIs1c;
                }
                
                height = height + Label1cValue.frame.size.height+10;
                
                Label1aValue.userInteractionEnabled = YES;
                UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc]
                                                      initWithTarget:self action:@selector(didTapLabelWithGesture:)];
                [Label1aValue addGestureRecognizer:tapGesture];
                
                Label1cValue.userInteractionEnabled = YES;
                UITapGestureRecognizer *tapGesture1c = [[UITapGestureRecognizer alloc]
                                                        initWithTarget:self action:@selector(didTapLabelWithGesture1c:)];
                [Label1cValue addGestureRecognizer:tapGesture1c]; */
                /*CGRect newFrame1Value = Label1Value.frame;
                 newFrame1Value.size.height = [self getLabelHeight:Label1Value];
                 Label1Value.frame = newFrame1Value;
                 CGRect newFrame1aValue = Label1aValue.frame;
                 newFrame1aValue.size.height = [self getLabelHeight:Label1aValue];
                 Label1aValue.frame = newFrame1aValue;
                 CGRect newFrame1bValue = Label1bValue.frame;
                 newFrame1bValue.size.height = [self getLabelHeight:Label1bValue];
                 Label1bValue.frame = newFrame1bValue;
                 CGRect newFrame1cValue = Label1cValue.frame;
                 newFrame1cValue.size.height = [self getLabelHeight:Label1cValue];
                 Label1cValue.frame = newFrame1cValue; */
                
                [Label3Title setHidden:NO];
                [Label3Title setFrame:CGRectMake(5, height, self.view.frame.size.width-10, 30)];
                //Label3Title.text = NSLocalizedString(@"Type of Product", nil);
                Label3Title.text = [[NSString alloc] initWithFormat:@" %@", NSLocalizedString(@"Type of Product", nil)];
                Label3Title.numberOfLines = 0;
                Label3Title.lineBreakMode = NSLineBreakByWordWrapping;
                Label3Title.clipsToBounds = YES;
                Label3Title.backgroundColor = [UIColor colorWithRed:255/255.0 green:195/255.0 blue:0/255.0 alpha:1];
                Label3Title.textColor = [UIColor blackColor];
                Label3Title.textAlignment = NSTextAlignmentLeft;
                CGRect newFrame3Title = Label3Title.frame;
                newFrame3Title.size.height = [self getLabelHeight:Label3Title];
                Label3Title.frame = newFrame3Title;
                //[self.view addSubview:Label3Title];
                height = height + Label3Title.frame.size.height+5;
                
                [Label3Value setHidden:NO];
                [Label3Value setFrame:CGRectMake(10, height, self.view.frame.size.width-20, 30)];
                if ([[dicResponse objectForKey:@"Product"] length]>0) {
                    Label3Value.text = [dicResponse objectForKey:@"Product"];
                } else {
                    Label3Value.text = @"";
                }
                Label3Value.numberOfLines = 0;
                Label3Value.lineBreakMode = NSLineBreakByWordWrapping;
                Label3Value.clipsToBounds = YES;
                Label3Value.backgroundColor = [UIColor clearColor];
                if ([appdelegate.backgroundT isEqualToString:@""] || [appdelegate.backgroundT isEqualToString:@"<null>"]) {
                    Label3Value.textColor = [UIColor blackColor];
                } else {
                    Label3Value.textColor = [UIColor whiteColor];
                }
                Label3Value.textAlignment = NSTextAlignmentLeft;
                CGRect newFrame3Value = Label3Value.frame;
                newFrame3Value.size.height = [self getLabelHeight:Label3Value];
                Label3Value.frame = newFrame3Value;
                //[self.view addSubview:Label3Value];
                height = height + Label3Value.frame.size.height+10;
                
                [Label2Title setHidden:NO];
                [Label2Title setFrame:CGRectMake(5, height, self.view.frame.size.width-10, 30)];
                //Label2Title.text = NSLocalizedString(@"This Label Assigned To", nil);
                Label2Title.text = [[NSString alloc] initWithFormat:@" %@", NSLocalizedString(@"This Label Assigned To", nil)];
                Label2Title.numberOfLines = 0;
                Label2Title.lineBreakMode = NSLineBreakByWordWrapping;
                Label2Title.clipsToBounds = YES;
                Label2Title.backgroundColor = [UIColor colorWithRed:255/255.0 green:195/255.0 blue:0/255.0 alpha:1];
                Label2Title.textColor = [UIColor blackColor];
                Label2Title.textAlignment = NSTextAlignmentLeft;
                CGRect newFrame2Title = Label2Title.frame;
                newFrame2Title.size.height = [self getLabelHeight:Label2Title];
                Label2Title.frame = newFrame2Title;
                //[self.view addSubview:Label2Title];
                height = height + Label2Title.frame.size.height+5;
                
                [Label2Value setHidden:NO];
                [Label2Value setFrame:CGRectMake(10, height, self.view.frame.size.width-20, 30)];
                if ([[dicResponse objectForKey:@"Company Name"] length]>0) {
                    Label2Value.text = [dicResponse objectForKey:@"Company Name"];;
                } else {
                    Label2Value.text = @"";
                }
                Label2Value.numberOfLines = 0;
                Label2Value.lineBreakMode = NSLineBreakByWordWrapping;
                Label2Value.clipsToBounds = YES;
                Label2Value.backgroundColor = [UIColor clearColor];
                if ([appdelegate.backgroundT isEqualToString:@""] || [appdelegate.backgroundT isEqualToString:@"<null>"]) {
                    Label2Value.textColor = [UIColor blackColor];
                } else {
                    Label2Value.textColor = [UIColor whiteColor];
                }
                Label2Value.textAlignment = NSTextAlignmentLeft;
                CGRect newFrame2Value = Label2Value.frame;
                newFrame2Value.size.height = [self getLabelHeight:Label2Value];
                Label2Value.frame = newFrame2Value;
                //[self.view addSubview:Label2Value];
                height = height + Label2Value.frame.size.height+10;
                
                [searchTextField resignFirstResponder];
                
                /*if ([[dicErr objectForKey:@"status_desc"] length]>0) {
                 [LabelError setHidden:NO];
                 [LabelError setFrame:CGRectMake(20, height, self.view.frame.size.width-40, 30)];
                 LabelError.text = NSLocalizedString(@"For this serial number, please contact us at 03-55446400 or email us at cserviceqas@sirim.my", nil);
                 LabelError.numberOfLines = 0;
                 LabelError.lineBreakMode = NSLineBreakByWordWrapping;
                 LabelError.clipsToBounds = YES;
                 LabelError.backgroundColor = [UIColor clearColor];
                 if ([appdelegate.backgroundT isEqualToString:@""] || [appdelegate.backgroundT isEqualToString:@"<null>"]) {
                 LabelError.textColor = [UIColor blackColor];
                 } else {
                 LabelError.textColor = [UIColor whiteColor];
                 }
                 LabelError.textAlignment = NSTextAlignmentLeft;
                 CGRect newFrameError = LabelError.frame;
                 newFrameError.size.height = [self getLabelHeight:LabelError];
                 LabelError.frame = newFrameError;
                 //[self.view addSubview:LabelError];
                 height = height + LabelError.frame.size.height+10;
                 } */
            }
           
        }
        
        if ([currentRequest isEqualToString:@"QRCode"])
        {
            searchTextField.text = @"";
            [LabelError setHidden:YES];
            [Label1Title setHidden:YES];
            [Label1Value setHidden:YES];
            [TextView1Value setHidden:YES];
            [Label1aValue setHidden:YES];
            [Label1bValue setHidden:YES];
            [Label1cValue setHidden:YES];
            [Label2Title setHidden:YES];
            [Label2Value setHidden:YES];
            [Label3Title setHidden:YES];
            [Label3Value setHidden:YES];
            
                
            NSDictionary *dictionary = responseDict;
            //NSLog(@"dictionary:%@", dictionary);
            //NSLog(@"Error:%@", [dictionary objectForKey:@"Error"]);
            //NSLog(@"Response:%@", [dictionary objectForKey:@"Response"]);
            
            //NSDictionary *dicErr = [dictionary objectForKey:@"Error"];
            //NSMutableArray *arrResponse = [dictionary objectForKey:@"Response"];
            //NSLog(@"arrResponse:%@", arrResponse);
            int height = self.view.frame.size.height/8+150;
            NSString *serialNum = @"";
            NSString *productS = @"";
            NSString *applicantName = @"";
            NSString *desc = @"";
            for(NSDictionary *row in dictionary)
            {
                //NSLog(@"row:%@", row);
                if (![[row objectForKey:@"serialNo"] isEqual:[NSNull null]]) {
                    serialNum = [row objectForKey:@"serialNo"];
                }
                if (![[row objectForKey:@"serialNo"] isEqual:[NSNull null]]) {
                    productS = [row objectForKey:@"Product"];
                }
                if (![[row objectForKey:@"ApplicantName"] isEqual:[NSNull null]]) {
                    applicantName = [row objectForKey:@"ApplicantName"];
                }
                if (![[row objectForKey:@"desc"] isEqual:[NSNull null]]) {
                    desc = [row objectForKey:@"desc"];
                }
            }
            
            //NSDictionary *dicResponse = [arrResponse objectAtIndex:0];
            //NSLog(@"dicrResponse111:%@", dicResponse);
            
            [Label1Title setHidden:NO];
            [Label1Title setFrame:CGRectMake(5, height, self.view.frame.size.width-10, 30)];
            //Label1Title.text = NSLocalizedString(@"Product Status", nil);
            Label1Title.text = [[NSString alloc] initWithFormat:@" %@", NSLocalizedString(@"Product Status", nil)];
            Label1Title.numberOfLines = 0;
            Label1Title.lineBreakMode = NSLineBreakByWordWrapping;
            Label1Title.clipsToBounds = YES;
            Label1Title.backgroundColor = [UIColor colorWithRed:255/255.0 green:195/255.0 blue:0/255.0 alpha:1];
            Label1Title.textColor = [UIColor blackColor];
            Label1Title.textAlignment = NSTextAlignmentLeft;
            CGRect newFrame1Title = Label1Title.frame;
            newFrame1Title.size.height = [self getLabelHeight:Label1Title];
            Label1Title.frame = newFrame1Title;
            //[self.view addSubview:Label1Title];
            height = height + Label1Title.frame.size.height+5;
            
            //[Label1Value setHidden:YES];
            [TextView1Value setHidden:NO];
            /*[Label1aValue setHidden:YES];
            [Label1bValue setHidden:YES];
            [Label1cValue setHidden:YES];
            [Label1Value setFrame:CGRectMake(5, height, self.view.frame.size.width-10, 30)];
            [Label1aValue setFrame:CGRectMake(5, height, self.view.frame.size.width-10, 30)];
            [Label1bValue setFrame:CGRectMake(5, height, self.view.frame.size.width-10, 30)];
            [Label1cValue setFrame:CGRectMake(5, height, self.view.frame.size.width-10, 30)]; */
            if ([serialNum length]>0) {
                searchTextField.text = serialNum;
                //Label1Value.text = NSLocalizedString(@"Certified", nil);
                TextView1Value.text = NSLocalizedString(@"Certified", nil);
                /*Label1aValue.text = @"";
                Label1bValue.text = @"";
                Label1cValue.text = @""; */
            } else {
               
                desc = [desc stringByDecodingHTMLEntities];
                NSString *html = desc;
                
                // Apply some inline CSS
                NSString *styledHtml = [self styledHTMLwithHTML:html];
                
                // Generate an attributed string from the HTML
                NSAttributedString *attributedText = [self attributedStringWithHTML:styledHtml];
                
                // Set the attributedText property of the UILabel
                //Label1Value.attributedText = attributedText;
                TextView1Value.attributedText = attributedText;
                /*Label1aValue.text = @"";
                Label1bValue.text = @"";
                Label1cValue.text = @""; */
                
                TextView1Value.editable = NO;
                TextView1Value.dataDetectorTypes = UIDataDetectorTypeAll;
                
            }
            
            if ([appdelegate.backgroundT isEqualToString:@""] || [appdelegate.backgroundT isEqualToString:@"<null>"]) {
                TextView1Value.textColor = [UIColor blackColor];
            } else {
                TextView1Value.textColor = [UIColor whiteColor];
            }
            
            CGFloat heightTextView1 = ceilf([TextView1Value sizeThatFits:TextView1Value.frame.size].height);
            [TextView1Value setFrame:CGRectMake(5, height, self.view.frame.size.width-10, heightTextView1)];
            height = height + heightTextView1+10;
            
            /*Label1Value.numberOfLines = 0;
            Label1Value.lineBreakMode = NSLineBreakByWordWrapping;
            Label1Value.clipsToBounds = YES;
            Label1Value.backgroundColor = [UIColor clearColor];
            if ([appdelegate.backgroundT isEqualToString:@""] || [appdelegate.backgroundT isEqualToString:@"<null>"]) {
                Label1Value.textColor = [UIColor blackColor];
            } else {
                Label1Value.textColor = [UIColor whiteColor];
            }
            Label1Value.textAlignment = NSTextAlignmentLeft;
            CGRect newFrame1Value = Label1Value.frame;
            newFrame1Value.size.height = [self getLabelHeight:Label1Value]+20;
            Label1Value.frame = newFrame1Value;
            //[self.view addSubview:Label2Value];
            //height = height + Label1Value.frame.size.height+10; */
            
            [Label3Title setHidden:NO];
            [Label3Title setFrame:CGRectMake(5, height, self.view.frame.size.width-10, 30)];
            //Label3Title.text = NSLocalizedString(@"Type of Product", nil);
            Label3Title.text = [[NSString alloc] initWithFormat:@" %@", NSLocalizedString(@"Type of Product", nil)];
            Label3Title.numberOfLines = 0;
            Label3Title.lineBreakMode = NSLineBreakByWordWrapping;
            Label3Title.clipsToBounds = YES;
            Label3Title.backgroundColor = [UIColor colorWithRed:255/255.0 green:195/255.0 blue:0/255.0 alpha:1];
            Label3Title.textColor = [UIColor blackColor];
            Label3Title.textAlignment = NSTextAlignmentLeft;
            CGRect newFrame3Title = Label3Title.frame;
            newFrame3Title.size.height = [self getLabelHeight:Label3Title];
            Label3Title.frame = newFrame3Title;
            //[self.view addSubview:Label3Title];
            height = height + Label3Title.frame.size.height+5;
            
            [Label3Value setHidden:NO];
            [Label3Value setFrame:CGRectMake(10, height, self.view.frame.size.width-20, 30)];
            if ([productS length]>0 && ![productS isEqualToString:@"0"]) {
                Label3Value.text = productS;
            } else {
                Label3Value.text = @"";
            }
            Label3Value.numberOfLines = 0;
            Label3Value.lineBreakMode = NSLineBreakByWordWrapping;
            Label3Value.clipsToBounds = YES;
            Label3Value.backgroundColor = [UIColor clearColor];
            if ([appdelegate.backgroundT isEqualToString:@""] || [appdelegate.backgroundT isEqualToString:@"<null>"]) {
                Label3Value.textColor = [UIColor blackColor];
            } else {
                Label3Value.textColor = [UIColor whiteColor];
            }
            Label3Value.textAlignment = NSTextAlignmentLeft;
            CGRect newFrame3Value = Label3Value.frame;
            newFrame3Value.size.height = [self getLabelHeight:Label3Value];
            Label3Value.frame = newFrame3Value;
            //[self.view addSubview:Label3Value];
            height = height + Label3Value.frame.size.height+10;
            
            [Label2Title setHidden:NO];
            [Label2Title setFrame:CGRectMake(5, height, self.view.frame.size.width-10, 30)];
            //Label2Title.text = NSLocalizedString(@"This Label Assigned To", nil);
            Label2Title.text = [[NSString alloc] initWithFormat:@" %@", NSLocalizedString(@"This Label Assigned To", nil)];
            Label2Title.numberOfLines = 0;
            Label2Title.lineBreakMode = NSLineBreakByWordWrapping;
            Label2Title.clipsToBounds = YES;
            Label2Title.backgroundColor = [UIColor colorWithRed:255/255.0 green:195/255.0 blue:0/255.0 alpha:1];
            Label2Title.textColor = [UIColor blackColor];
            Label2Title.textAlignment = NSTextAlignmentLeft;
            CGRect newFrame2Title = Label2Title.frame;
            newFrame2Title.size.height = [self getLabelHeight:Label2Title];
            Label2Title.frame = newFrame2Title;
            //[self.view addSubview:Label2Title];
            height = height + Label2Title.frame.size.height+5;
            
            [Label2Value setHidden:NO];
            [Label2Value setFrame:CGRectMake(10, height, self.view.frame.size.width-20, 30)];
            if ([applicantName length]>0) {
                Label2Value.text = applicantName;
            } else {
                Label2Value.text = @"";
            }
            Label2Value.numberOfLines = 0;
            Label2Value.lineBreakMode = NSLineBreakByWordWrapping;
            Label2Value.clipsToBounds = YES;
            Label2Value.backgroundColor = [UIColor clearColor];
            if ([appdelegate.backgroundT isEqualToString:@""] || [appdelegate.backgroundT isEqualToString:@"<null>"]) {
                Label2Value.textColor = [UIColor blackColor];
            } else {
                Label2Value.textColor = [UIColor whiteColor];
            }
            Label2Value.textAlignment = NSTextAlignmentLeft;
            CGRect newFrame2Value = Label2Value.frame;
            newFrame2Value.size.height = [self getLabelHeight:Label2Value];
            Label2Value.frame = newFrame2Value;
            //[self.view addSubview:Label2Value];
            height = height + Label2Value.frame.size.height+10;
            
        }
    });
    
}

- (NSString *)styledHTMLwithHTML:(NSString *)HTML {
    NSString *style = @"<meta charset=\"UTF-8\"><style> body { font-family: 'Helvetica'; font-size: 17px; }</style>";
    
    return [NSString stringWithFormat:@"%@%@", style, HTML];
}

- (NSAttributedString *)attributedStringWithHTML:(NSString *)HTML {
    //NSLog(@"attributedStringWithHTML:: %@", HTML);
    NSDictionary *options = @{ NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType };
    return [[NSAttributedString alloc] initWithData:[HTML dataUsingEncoding:NSUTF8StringEncoding] options:options documentAttributes:NULL error:NULL];
}

- (void)didTapLabelWithGesture:(UITapGestureRecognizer *)tapGesture {
    NSString *strPhone = [[NSString alloc] initWithFormat:@"telprompt://%@", Label1aValue.text];
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:strPhone]];
}

- (void)didTapLabelWithGesture1c:(UITapGestureRecognizer *)tapGesture {
    /* can try to use, may b work also
    NSString *stringURL = @"mailto:foo@example.com";
    NSURL *url = [NSURL URLWithString:stringURL];
    [[UIApplication sharedApplication] openURL:url];
    */
    /*NSString *strEmail = Label1cValue.text;
     
     NSArray *toRecipents = [NSArray arrayWithObject:strEmail];
     
     MFMailComposeViewController *mc = [[MFMailComposeViewController alloc] init];
     mc.mailComposeDelegate = self;
     //[mc setSubject:emailTitle];
     //[mc setMessageBody:messageBody isHTML:YES];
     [mc setToRecipients:toRecipents];
     
     // Present mail view controller on screen
     //[viewCtrl presentViewController:mc animated:YES completion:NULL];
     if([MFMailComposeViewController canSendMail])
     {
     [self presentViewController:mc animated:NO completion:NULL];
     //[viewCtrl.navigationController pushViewController:mc animated:YES];
     } */
    // From within your active view controller
    if ([MFMailComposeViewController canSendMail])
    {
        NSString *strEmail = Label1cValue.text;
        NSArray *toRecipents = [NSArray arrayWithObject:strEmail];
        MFMailComposeViewController *mail = [[MFMailComposeViewController alloc] init];
        mail.mailComposeDelegate = self;
        [mail setToRecipients:toRecipents];
        //[mail setSubject:@"Sample Subject"];
        //[mail setMessageBody:@"Here is some main text in the email!" isHTML:NO];
        //[mail setToRecipients:@[@"testingEmail@example.com"]];
        
        [self presentViewController:mail animated:YES completion:NULL];
    }
    else
    {
        NSLog(@"This device cannot send email");
    }
    
}

- (void)mailComposeController:(MFMailComposeViewController *)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError *)error
{
    switch (result) {
        case MFMailComposeResultSent:
            NSLog(@"You sent the email.");
            break;
        case MFMailComposeResultSaved:
            NSLog(@"You saved a draft of this email");
            break;
        case MFMailComposeResultCancelled:
            NSLog(@"You cancelled sending this email.");
            break;
        case MFMailComposeResultFailed:
            NSLog(@"Mail failed:  An error occurred when trying to compose this email");
            break;
        default:
            NSLog(@"An error occurred when trying to compose this email");
            break;
    }
    
    [self dismissViewControllerAnimated:YES completion:NULL];
}

- (CGFloat)getLabelHeight:(UILabel*)label
{
    CGSize constraint = CGSizeMake(label.frame.size.width, CGFLOAT_MAX);
    CGSize size;
    
    NSStringDrawingContext *context = [[NSStringDrawingContext alloc] init];
    CGSize boundingBox = [label.text boundingRectWithSize:constraint
                                                  options:NSStringDrawingUsesLineFragmentOrigin
                                               attributes:@{NSFontAttributeName:label.font}
                                                  context:context].size;
    
    size = CGSizeMake(ceil(boundingBox.width), ceil(boundingBox.height));
    
    return size.height;
}

#pragma mark LoadingView
-(void) addLoadingView
{
	loadingView = [LoadingView loadingViewInView:self.view];
}

-(void) removeLoadingView
{
	[loadingView removeView];
}

#pragma mark displayAlert
-(void) displayAlert: (NSString *) stralert {
	NSString *str = [[NSString alloc] initWithFormat:@"%@", stralert];
	UIAlertView *alert = [[UIAlertView alloc] initWithTitle:appdelegate.iconname
													message:str
												   delegate:self
										  cancelButtonTitle:NSLocalizedString(@"done", nil)
										  otherButtonTitles:nil];
	
	[alert show];
	str =nil;
	alert =nil;
}

-(NSString *) urlEncode:(NSString *) theStr {
    /*NSString *encoded = (NSString *)CFURLCreateStringByAddingPercentEscapes(
     NULL,
     (CFStringRef)theStr,
     NULL,
     (CFStringRef)@"!*'\"();:@&=+$,/?%#[]% ",
     //																		NSUnicodeStringEncoding );
     kCFStringEncodingUTF8 ); */
    NSString *encoded = (NSString *)CFBridgingRelease(CFURLCreateStringByAddingPercentEscapes(
                                                                                              NULL,
                                                                                              (__bridge CFStringRef) theStr,
                                                                                              NULL,
                                                                                              CFSTR("!*'();:@&=+$,/?%#[]\" "),
                                                                                              kCFStringEncodingUTF8));
    return encoded;
    //return nil;
}

-(BOOL) textFieldShouldReturn:(UITextField *)textField{
    
    [textField resignFirstResponder];
    return YES;
}

- (void)dealloc {
    
    //[super dealloc];
}

@end
