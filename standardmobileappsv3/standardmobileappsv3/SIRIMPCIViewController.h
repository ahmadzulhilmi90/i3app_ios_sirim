//
//  LoyaltyViewController.h
//  standardmobileapps
//
//  Created by Hazwan on 11/25/13.
//  Copyright (c) 2013 Maggie. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SIRIMPCIViewController : UIViewController <UITextFieldDelegate>

@property (strong, nonatomic) IBOutlet UITextField *rocTextField;
@property (strong, nonatomic) IBOutlet UITextField *companyTextField;
@property (strong, nonatomic) IBOutlet UITextField *licenseTextField;
@property (strong, nonatomic) IBOutlet UIButton *searchBtn;
@property (strong, nonatomic) IBOutlet UILabel *LabelMainTitle;
@property (strong, nonatomic) IBOutlet UILabel *Label1Title;
@property (strong, nonatomic) IBOutlet UILabel *Label1Value;
@property (strong, nonatomic) IBOutlet UILabel *Label2Title;
@property (strong, nonatomic) IBOutlet UILabel *Label2Value;
@property (strong, nonatomic) IBOutlet UILabel *Label3Title;
@property (strong, nonatomic) IBOutlet UILabel *Label3Value;
@property (strong, nonatomic) IBOutlet UILabel *Label4Title;
@property (strong, nonatomic) IBOutlet UILabel *Label4Value;
@property (strong, nonatomic) IBOutlet UILabel *Label5Title;
@property (strong, nonatomic) IBOutlet UILabel *Label5Value;
@property (strong, nonatomic) IBOutlet UILabel *LabelError;

@end
