//
//  LoyaltyViewController.m
//  standardmobileapps
//
//  Created by Hazwan on 11/25/13.
//  Copyright (c) 2013 Maggie. All rights reserved.
//

#import <QuartzCore/QuartzCore.h>
#import "SIRIMPCIViewController.h"
#import "AppDelegate.h"
#import "LoadingView.h"
#import "SBJSON.h"
#import "RootViewController.h"
#import "ViewController.h"
#import "InitialSlidingViewController.h"
#import "BTSlideInteractor.h"

@interface SIRIMPCIViewController ()
@property (strong, nonatomic) BTSlideInteractor *interactor;
@end

InitialSlidingViewController *modalController;

AppDelegate *appdelegate;
__strong LoadingView *loadingView;
__strong NSString *currentRequest;
//__strong NSMutableData *receivedData;

@implementation SIRIMPCIViewController
@synthesize searchBtn, rocTextField, companyTextField, licenseTextField, LabelError, Label1Title, Label1Value, Label2Title, Label2Value, Label3Title, Label3Value, Label4Title, Label4Value, Label5Title, Label5Value, LabelMainTitle;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    //self.title = appdelegate.loyaltytitle;
    
    self.navigationItem.backBarButtonItem = [[UIBarButtonItem alloc]initWithTitle:@"" style:UIBarButtonItemStylePlain target:nil action:nil];
    
    appdelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    
    //self.title = cont_title;
    appdelegate.homeclicked = NO;
    
    CGRect rect = CGRectMake(0,0,20,20);
    UIGraphicsBeginImageContext( rect.size );
    [[UIImage imageNamed:@"home.png"] drawInRect:rect];
    UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    UIBarButtonItem *myHomeButton = [[UIBarButtonItem alloc] initWithImage:image style:UIBarButtonItemStyleDone target:self action:@selector(myHomeAction:)];
    
    
    NSArray *myButtonArray = [[NSArray alloc] initWithObjects:myHomeButton, nil];
    
    self.navigationItem.rightBarButtonItems = myButtonArray;
    
}

- (void)viewDidAppear:(BOOL)animated {
    [LabelMainTitle setFrame:CGRectMake(5, self.view.frame.size.height/8, self.view.frame.size.width-10, 50)];
    
    [rocTextField setFrame:CGRectMake(10,self.view.frame.size.height/8+100,self.view.frame.size.width-20,30)];
    [companyTextField setFrame:CGRectMake(10,self.view.frame.size.height/8+60,self.view.frame.size.width-20,30)];
    [licenseTextField setFrame:CGRectMake(10,self.view.frame.size.height/8+140,self.view.frame.size.width-20,30)];
    [searchBtn setFrame:CGRectMake(self.view.frame.size.width/2-45,self.view.frame.size.height/8+180,90,30)];
    [[searchBtn layer] setBorderWidth:1.0];
    [[searchBtn layer] setBorderColor:[UIColor grayColor].CGColor];
    [[searchBtn layer] setCornerRadius:6.0];
    
    if (![appdelegate.backgroundT isEqualToString:@""]) {
        
        NSString *filePath = appdelegate.backgroundT;
        
        NSRange end = [filePath rangeOfString:@"/" options:NSBackwardsSearch];
        NSString *shortString;
        if (end.location != NSNotFound) {
            shortString =[filePath substringWithRange:NSMakeRange(end.location+1, filePath.length-(end.location+1))];
        } else {
            shortString = filePath;
        }
        
        BOOL success;
        
        // Create a FileManager object, we will use this to check the status
        // of the database and to copy it over if required
        NSFileManager *fileManager = [NSFileManager defaultManager];
        
        // Check if the database has already been created in the users filesystem
        success = [fileManager fileExistsAtPath:[[[NSBundle mainBundle] resourcePath] stringByAppendingPathComponent:shortString]];
        
        UIImageView *imgV = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height)];
        //[self addSubview:imgV];
        
        //__block UIActivityIndicatorView *activityIndicator;
        __weak UIImageView *weakImageView = imgV;
        NSString *imagePath;
        if(success) {
            imagePath = [[[NSBundle mainBundle] resourcePath] stringByAppendingPathComponent:shortString];
            [weakImageView setImage:[UIImage imageNamed:shortString]];
            
            //NSLog(@"shortStringimagePath : %@", imagePath);
            imgV.contentMode  = UIViewContentModeScaleAspectFit;
            [imgV setClipsToBounds:YES];
            
            UIGraphicsBeginImageContext(self.view.frame.size);
            //CGRect rect=CGRectMake(0,-64,self.view.bounds.size.width,self.view.bounds.size.height+64);
            [imgV.image drawInRect:self.view.bounds];
            //[imgV.image drawInRect:rect];
            UIImage *image1 = UIGraphicsGetImageFromCurrentImageContext();
            UIGraphicsEndImageContext();
            self.view.backgroundColor = [UIColor colorWithPatternImage:image1];
        } else {
            imagePath = filePath;
            NSURL *imageURL = [NSURL URLWithString:imagePath];
            NSData *imageData = [NSData dataWithContentsOfURL:imageURL];
            //self.backgroundColor = [UIColor redColor];
            UIGraphicsBeginImageContext(self.view.frame.size);
            //CGRect rect=CGRectMake(0,-64,self.view.bounds.size.width,self.view.bounds.size.height+64);
            //[[UIImage imageWithData:imageData] drawInRect:rect];
            [[UIImage imageWithData:imageData] drawInRect:self.view.bounds];
            UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
            UIGraphicsEndImageContext();
            self.view.backgroundColor = [UIColor colorWithPatternImage:image];
        }
    } else {
        self.view.backgroundColor = [UIColor clearColor];
    }
}

//Slide Menu
- (void)willAnimateRotationToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation duration:(NSTimeInterval)duration
{
    //NSLog(@"view frame=%@ bounds=%@", NSStringFromCGRect(self.view.frame), NSStringFromCGRect(self.view.bounds));
}


- (void)showModalButtonWasTouched
{
    self.interactor = [[BTSlideInteractor alloc] init];
    self.interactor.presenting = YES;
    
    //BTModalViewController *modalController = [self.storyboard instantiateViewControllerWithIdentifier:@"ModalViewController"];
    modalController = [[InitialSlidingViewController alloc] init];
    modalController.modalPresentationStyle = UIModalPresentationCustom;
    modalController.transitioningDelegate = self.interactor;
    [self.navigationController presentViewController:modalController animated:YES completion:nil];
    //[self.navigationController popToViewController:modalController animated:NO];
    //[self.navigationController pushViewController:modalController animated:YES];
}
// End Slide Menu

-(void)myHomeAction:(UIBarButtonItem *)sender{
    
    appdelegate.nextPage = @"";
    
    if ([appdelegate.listOfVerMenu count]>0) {
        if (appdelegate.homeclicked==NO) {
            appdelegate.homeclicked = YES;
            [self showModalButtonWasTouched];
        } else {
            appdelegate.homeclicked = NO;
            [modalController dismissViewControllerAnimated:YES completion:nil];
        }
    } else {
        //AppDelegate *appdelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
        //NSLog(@"View Controllers : %@",[appdelegate.navController viewControllers]);
        NSMutableArray *navigationArray = [[NSMutableArray alloc] initWithArray: appdelegate.navController.viewControllers];
        
        for (UIViewController* viewController in appdelegate.navController.viewControllers) {
            
            //This if condition checks whether the viewController's class is MyGroupViewController
            // if true that means its the MyGroupViewController (which has been pushed at some point)
            if ([viewController isKindOfClass:[RootViewController class]] ) {
                
                // Here viewController is a reference of UIViewController base class of MyGroupViewController
                // but viewController holds MyGroupViewController  object so we can type cast it here
                //ThemeViewController *themeViewController = (ThemeViewController*)viewController;
                //[self.navigationController popToViewController:themeViewController animated:YES];
                [navigationArray removeObjectAtIndex: 0];
            }
        }
        appdelegate.navController.viewControllers = navigationArray;
        for (UIViewController* viewController in appdelegate.navController.viewControllers) {
            
            //This if condition checks whether the viewController's class is MyGroupViewController
            // if true that means its the MyGroupViewController (which has been pushed at some point)
            if ([viewController isKindOfClass:[ViewController class]] ) {
                
                // Here viewController is a reference of UIViewController base class of MyGroupViewController
                // but viewController holds MyGroupViewController  object so we can type cast it here
                ViewController *themeViewController = (ViewController*)viewController;
                [appdelegate.navController popToViewController:themeViewController animated:NO];
                //[self.navigationController popToViewController:viewController animated:YES];
            }
        }
        //NSLog(@"View Controllers111 : %@",appdelegate.navController.viewControllers);
    }
    
}

-(void) viewDidDisappear:(BOOL)animated {
    
    [super viewDidDisappear:animated];
    
    appdelegate.homeclicked = NO;
    [modalController dismissViewControllerAnimated:YES completion:nil];
    
}

-(IBAction)search:(id)sender {
    appdelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    
    currentRequest = @"SearchList";
    NSString *urlAddress2 = [[NSString alloc] initWithFormat:@"https://sqasapp.sirim.my/api/pci.php?roc=%@&company_name=%@&license_no=%@",[self urlEncode:rocTextField.text], [self urlEncode:companyTextField.text], [self urlEncode:licenseTextField.text]];
    //NSLog(@"urlAddress2 loyalty :: %@",urlAddress2);
    [self connectToServer:urlAddress2];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark Connect To API
-(void) connectToServer:(NSString *) urlPath {
    [self addLoadingView];
	NSString *urlAddress =[NSString stringWithFormat:@"%@", urlPath];
	
    [self getJsonResponse:urlAddress success:^(NSDictionary *responseDict) {
        
        //NSLog(@"%@",responseDict);
        [self successfunction:responseDict];
        
    } failure:^(NSError *error) {
        // release the connection, and the data object
        
        [self removeLoadingView];
        [self displayAlert:NSLocalizedString(@"MSG_CONNECTION_ERROR", nil)];
    }];
}

-(void)getJsonResponse : (NSString *)urlStr success : (void (^)(NSDictionary *responseDict))success failure:(void(^)(NSError* error))failure
{
    
    
    NSURLSession * session = [NSURLSession sharedSession];
    NSURL * url = [NSURL URLWithString: urlStr];
    
    
    // Asynchronously Api is hit here
    NSURLSessionDataTask * dataTask = [session dataTaskWithURL:url completionHandler:^(NSData *data, NSURLResponse *response, NSError *error)
                                       {
                                           
                                           //NSLog(@"%@",data);
                                           NSDictionary * json;
                                           if ([data length]>0) {
                                               json  = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
                                               //NSLog(@"%@",json);
                                               success(json);
                                           } else {
                                               json = nil;
                                               dispatch_async(dispatch_get_main_queue(), ^ {
                                                   [self removeLoadingView];
                                                   [self displayAlert:NSLocalizedString(@"MSG_CONNECTION_ERROR", nil)];
                                               });
                                           }
                                           
                                           
                                       }];
    
    [dataTask resume] ; // Executed First
    
    
}

-(void) successfunction:(NSDictionary *)responseDict {
    dispatch_async(dispatch_get_main_queue(), ^{
        //NSString *jsonString = [responseDict JSONRepresentation];
        [self removeLoadingView];
        
        if ([currentRequest isEqualToString:@"SearchList"])
        {
            [LabelError setHidden:YES];
            [Label1Title setHidden:YES];
            [Label1Value setHidden:YES];
            [Label2Title setHidden:YES];
            [Label2Value setHidden:YES];
            [Label3Title setHidden:YES];
            [Label3Value setHidden:YES];
            [Label4Title setHidden:YES];
            [Label4Value setHidden:YES];
            [Label5Title setHidden:YES];
            [Label5Value setHidden:YES];
            
            NSDictionary *dictionary = responseDict;
            //NSLog(@"dictionary:%@", dictionary);
            //NSLog(@"Error:%@", [dictionary objectForKey:@"Error"]);
            //NSLog(@"Response:%@", [dictionary objectForKey:@"Response"]);
            
            NSDictionary *dicErr = [dictionary objectForKey:@"Error"];
            NSMutableArray *arrResponse = [dictionary objectForKey:@"Response"];
            //NSLog(@"arrResponse:%@", arrResponse);
            int height = self.view.frame.size.height/8+230;
            if ([[dicErr objectForKey:@"status_desc"] length]>0) {
                [LabelError setHidden:NO];
                [LabelError setFrame:CGRectMake(5, height, self.view.frame.size.width-10, 30)];
                LabelError.text = [dicErr objectForKey:@"status_desc"];
                LabelError.numberOfLines = 0;
                LabelError.lineBreakMode = NSLineBreakByWordWrapping;
                LabelError.clipsToBounds = YES;
                LabelError.backgroundColor = [UIColor clearColor];
                if ([appdelegate.backgroundT isEqualToString:@""] || [appdelegate.backgroundT isEqualToString:@"<null>"]) {
                    LabelError.textColor = [UIColor blackColor];
                } else {
                    LabelError.textColor = [UIColor whiteColor];
                }
                LabelError.textAlignment = NSTextAlignmentLeft;
                CGRect newFrameError = LabelError.frame;
                newFrameError.size.height = [self getLabelHeight:LabelError];
                LabelError.frame = newFrameError;
                //[self.view addSubview:LabelError];
                height = height + LabelError.frame.size.height+10;
            }
            
            NSDictionary *dicResponse = [arrResponse objectAtIndex:0];
            //NSLog(@"dicrResponse111:%@", dicResponse);
            if ([[dicResponse objectForKey:@"Company"] length]>0 || [[dicResponse objectForKey:@"ROC No."] length]>0) {
                [Label1Title setHidden:NO];
                [Label1Title setFrame:CGRectMake(5, height, self.view.frame.size.width-10, 30)];
                Label1Title.text = [NSString stringWithFormat:@"%@ / %@", NSLocalizedString(@"Company Name", nil), NSLocalizedString(@"ROC", nil)];
                Label1Title.numberOfLines = 0;
                Label1Title.lineBreakMode = NSLineBreakByWordWrapping;
                Label1Title.clipsToBounds = YES;
                Label1Title.backgroundColor = [UIColor colorWithRed:255/255.0 green:195/255.0 blue:0/255.0 alpha:1];
                Label1Title.textColor = [UIColor blackColor];
                Label1Title.textAlignment = NSTextAlignmentLeft;
                CGRect newFrame1Title = Label1Title.frame;
                newFrame1Title.size.height = [self getLabelHeight:Label1Title];
                Label1Title.frame = newFrame1Title;
                //[self.view addSubview:Label1Title];
                height = height + Label1Title.frame.size.height+5;
                
                [Label1Value setHidden:NO];
                [Label1Value setFrame:CGRectMake(5, height, self.view.frame.size.width-10, 30)];
                if ([[dicResponse objectForKey:@"Company"] length]>0) {
                    Label1Value.text = [NSString stringWithFormat:@"%@", [dicResponse objectForKey:@"Company"]];
                } else {
                    Label1Value.text = [NSString stringWithFormat:@"%@", [dicResponse objectForKey:@"ROC No."]];
                }
                Label1Value.numberOfLines = 0;
                Label1Value.lineBreakMode = NSLineBreakByWordWrapping;
                Label1Value.clipsToBounds = YES;
                Label1Value.backgroundColor = [UIColor clearColor];
                if ([appdelegate.backgroundT isEqualToString:@""] || [appdelegate.backgroundT isEqualToString:@"<null>"]) {
                    Label1Value.textColor = [UIColor blackColor];
                } else {
                    Label1Value.textColor = [UIColor whiteColor];
                }
                Label1Value.textAlignment = NSTextAlignmentLeft;
                CGRect newFrame1Value = Label1Value.frame;
                newFrame1Value.size.height = [self getLabelHeight:Label1Value];
                Label1Value.frame = newFrame1Value;
                //[self.view addSubview:Label1Value];
                height = height + Label1Value.frame.size.height+10;
            }
            
            if ([[dicResponse objectForKey:@"License No."] length]>0) {
                [Label2Title setHidden:NO];
                [Label2Title setFrame:CGRectMake(5, height, self.view.frame.size.width-10, 30)];
                Label2Title.text = NSLocalizedString(@"License Number", nil);
                Label2Title.numberOfLines = 0;
                Label2Title.lineBreakMode = NSLineBreakByWordWrapping;
                Label2Title.clipsToBounds = YES;
                Label2Title.backgroundColor = [UIColor colorWithRed:255/255.0 green:195/255.0 blue:0/255.0 alpha:1];
                Label2Title.textColor = [UIColor blackColor];
                Label2Title.textAlignment = NSTextAlignmentLeft;
                CGRect newFrame2Title = Label2Title.frame;
                newFrame2Title.size.height = [self getLabelHeight:Label2Title];
                Label2Title.frame = newFrame2Title;
                //[self.view addSubview:Label2Title];
                height = height + Label2Title.frame.size.height+5;
                
                [Label2Value setHidden:NO];
                [Label2Value setFrame:CGRectMake(5, height, self.view.frame.size.width-10, 30)];
                Label2Value.text = [dicResponse objectForKey:@"License No."];
                Label2Value.numberOfLines = 0;
                Label2Value.lineBreakMode = NSLineBreakByWordWrapping;
                Label2Value.clipsToBounds = YES;
                Label2Value.backgroundColor = [UIColor clearColor];
                if ([appdelegate.backgroundT isEqualToString:@""] || [appdelegate.backgroundT isEqualToString:@"<null>"]) {
                    Label2Value.textColor = [UIColor blackColor];
                } else {
                    Label2Value.textColor = [UIColor whiteColor];
                }
                Label2Value.textAlignment = NSTextAlignmentLeft;
                CGRect newFrame2Value = Label2Value.frame;
                newFrame2Value.size.height = [self getLabelHeight:Label2Value];
                Label2Value.frame = newFrame2Value;
                //[self.view addSubview:Label2Value];
                height = height + Label2Value.frame.size.height+10;
            }
            
            if ([[dicResponse objectForKey:@"Standard"] length]>0) {
                [Label3Title setHidden:NO];
                [Label3Title setFrame:CGRectMake(5, height, self.view.frame.size.width-10, 30)];
                Label3Title.text = NSLocalizedString(@"Standard", nil);
                Label3Title.numberOfLines = 0;
                Label3Title.lineBreakMode = NSLineBreakByWordWrapping;
                Label3Title.clipsToBounds = YES;
                Label3Title.backgroundColor = [UIColor colorWithRed:255/255.0 green:195/255.0 blue:0/255.0 alpha:1];
                Label3Title.textColor = [UIColor blackColor];
                Label3Title.textAlignment = NSTextAlignmentLeft;
                CGRect newFrame3Title = Label3Title.frame;
                newFrame3Title.size.height = [self getLabelHeight:Label3Title];
                Label3Title.frame = newFrame3Title;
                //[self.view addSubview:Label3Title];
                height = height + Label3Title.frame.size.height+5;
                
                [Label3Value setHidden:NO];
                [Label3Value setFrame:CGRectMake(5, height, self.view.frame.size.width-10, 30)];
                Label3Value.text = [dicResponse objectForKey:@"Standard"];
                Label3Value.numberOfLines = 0;
                Label3Value.lineBreakMode = NSLineBreakByWordWrapping;
                Label3Value.clipsToBounds = YES;
                Label3Value.backgroundColor = [UIColor clearColor];
                if ([appdelegate.backgroundT isEqualToString:@""] || [appdelegate.backgroundT isEqualToString:@"<null>"]) {
                    Label3Value.textColor = [UIColor blackColor];
                } else {
                    Label3Value.textColor = [UIColor whiteColor];
                }
                Label3Value.textAlignment = NSTextAlignmentLeft;
                CGRect newFrame3Value = Label3Value.frame;
                newFrame3Value.size.height = [self getLabelHeight:Label3Value];
                Label3Value.frame = newFrame3Value;
                //[self.view addSubview:Label3Value];
                height = height + Label3Value.frame.size.height+10;
            }
            
            if ([[dicResponse objectForKey:@"Product"] length]>0) {
                [Label4Title setHidden:NO];
                [Label4Title setFrame:CGRectMake(5, height, self.view.frame.size.width-10, 30)];
                Label4Title.text = NSLocalizedString(@"Product Name", nil);
                Label4Title.numberOfLines = 0;
                Label4Title.lineBreakMode = NSLineBreakByWordWrapping;
                Label4Title.clipsToBounds = YES;
                Label4Title.backgroundColor = [UIColor colorWithRed:255/255.0 green:195/255.0 blue:0/255.0 alpha:1];
                Label4Title.textColor = [UIColor blackColor];
                Label4Title.textAlignment = NSTextAlignmentLeft;
                CGRect newFrame3Title = Label4Title.frame;
                newFrame3Title.size.height = [self getLabelHeight:Label4Title];
                Label4Title.frame = newFrame3Title;
                //[self.view addSubview:Label4Title];
                height = height + Label4Title.frame.size.height+5;
                
                [Label4Value setHidden:NO];
                [Label4Value setFrame:CGRectMake(5, height, self.view.frame.size.width-10, 30)];
                Label4Value.text = [dicResponse objectForKey:@"Product"];
                Label4Value.numberOfLines = 0;
                Label4Value.lineBreakMode = NSLineBreakByWordWrapping;
                Label4Value.clipsToBounds = YES;
                Label4Value.backgroundColor = [UIColor clearColor];
                if ([appdelegate.backgroundT isEqualToString:@""] || [appdelegate.backgroundT isEqualToString:@"<null>"]) {
                    Label4Value.textColor = [UIColor blackColor];
                } else {
                    Label4Value.textColor = [UIColor whiteColor];
                }
                Label4Value.textAlignment = NSTextAlignmentLeft;
                CGRect newFrame3Value = Label4Value.frame;
                newFrame3Value.size.height = [self getLabelHeight:Label4Value];
                Label4Value.frame = newFrame3Value;
                //[self.view addSubview:Label4Value];
                height = height + Label4Value.frame.size.height+10;
            }
            
            if ([[dicResponse objectForKey:@"Brand"] length]>0 || [[dicResponse objectForKey:@"Model"] length]>0) {
                [Label5Title setHidden:NO];
                [Label5Title setFrame:CGRectMake(5, height, self.view.frame.size.width-10, 30)];
                Label5Title.text = [NSString stringWithFormat:@"%@ / %@", NSLocalizedString(@"Brand", nil), NSLocalizedString(@"Model", nil)];
                Label5Title.numberOfLines = 0;
                Label5Title.lineBreakMode = NSLineBreakByWordWrapping;
                Label5Title.clipsToBounds = YES;
                Label5Title.backgroundColor = [UIColor colorWithRed:255/255.0 green:195/255.0 blue:0/255.0 alpha:1];
                Label5Title.textColor = [UIColor blackColor];
                Label5Title.textAlignment = NSTextAlignmentLeft;
                CGRect newFrame3Title = Label5Title.frame;
                newFrame3Title.size.height = [self getLabelHeight:Label5Title];
                Label5Title.frame = newFrame3Title;
                //[self.view addSubview:Label5Title];
                height = height + Label5Title.frame.size.height+5;
                
                [Label5Value setHidden:NO];
                [Label5Value setFrame:CGRectMake(5, height, self.view.frame.size.width-10, 30)];
                if ([[dicResponse objectForKey:@"Brand"] length]>0) {
                    Label5Value.text = [NSString stringWithFormat:@"%@", [dicResponse objectForKey:@"Brand"]];
                } else {
                    Label5Value.text = [NSString stringWithFormat:@"%@", [dicResponse objectForKey:@"Model"]];
                }
                Label5Value.numberOfLines = 0;
                Label5Value.lineBreakMode = NSLineBreakByWordWrapping;
                Label5Value.clipsToBounds = YES;
                Label5Value.backgroundColor = [UIColor clearColor];
                if ([appdelegate.backgroundT isEqualToString:@""] || [appdelegate.backgroundT isEqualToString:@"<null>"]) {
                    Label5Value.textColor = [UIColor blackColor];
                } else {
                    Label5Value.textColor = [UIColor whiteColor];
                }
                Label5Value.textAlignment = NSTextAlignmentLeft;
                CGRect newFrame3Value = Label5Value.frame;
                newFrame3Value.size.height = [self getLabelHeight:Label5Value];
                Label5Value.frame = newFrame3Value;
                //[self.view addSubview:Label5Value];
                height = height + Label5Value.frame.size.height+10;
            }
           
        }
    });
    
}

- (CGFloat)getLabelHeight:(UILabel*)label
{
    CGSize constraint = CGSizeMake(label.frame.size.width, CGFLOAT_MAX);
    CGSize size;
    
    NSStringDrawingContext *context = [[NSStringDrawingContext alloc] init];
    CGSize boundingBox = [label.text boundingRectWithSize:constraint
                                                  options:NSStringDrawingUsesLineFragmentOrigin
                                               attributes:@{NSFontAttributeName:label.font}
                                                  context:context].size;
    
    size = CGSizeMake(ceil(boundingBox.width), ceil(boundingBox.height));
    
    return size.height;
}

#pragma mark LoadingView
-(void) addLoadingView
{
	loadingView = [LoadingView loadingViewInView:self.view];
}

-(void) removeLoadingView
{
	[loadingView removeView];
}

#pragma mark displayAlert
-(void) displayAlert: (NSString *) stralert {
	NSString *str = [[NSString alloc] initWithFormat:@"%@", stralert];
	UIAlertView *alert = [[UIAlertView alloc] initWithTitle:appdelegate.iconname
													message:str
												   delegate:self
										  cancelButtonTitle:NSLocalizedString(@"done", nil)
										  otherButtonTitles:nil];
	
	[alert show];
	str =nil;
	alert =nil;
}

-(NSString *) urlEncode:(NSString *) theStr {
    /*NSString *encoded = (NSString *)CFURLCreateStringByAddingPercentEscapes(
     NULL,
     (CFStringRef)theStr,
     NULL,
     (CFStringRef)@"!*'\"();:@&=+$,/?%#[]% ",
     //																		NSUnicodeStringEncoding );
     kCFStringEncodingUTF8 ); */
    NSString *encoded = (NSString *)CFBridgingRelease(CFURLCreateStringByAddingPercentEscapes(
                                                                                              NULL,
                                                                                              (__bridge CFStringRef) theStr,
                                                                                              NULL,
                                                                                              CFSTR("!*'();:@&=+$,/?%#[]\" "),
                                                                                              kCFStringEncodingUTF8));
    return encoded;
    //return nil;
}

-(BOOL) textFieldShouldReturn:(UITextField *)textField{
    
    [textField resignFirstResponder];
    return YES;
}

- (void)dealloc {
    
    //[super dealloc];
}

@end
