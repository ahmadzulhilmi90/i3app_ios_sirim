//
//  ScorePointViewController.h
//  standardmobileappsv3
//
//  Created by M3Online on 12/26/14.
//  Copyright (c) 2014 M3Online. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ScorePointViewController : UIViewController <UITableViewDelegate, UITableViewDataSource, UIViewControllerTransitioningDelegate>

@property (strong, nonatomic) IBOutlet UIView *view_login;

@property (strong, nonatomic) IBOutlet UIView *view_scorepointList;
@property (strong, nonatomic) NSMutableArray *arrayScorePointList;
@property (strong, nonatomic) IBOutlet UITableView *table_scorepointList;
@property (strong, nonatomic) NSDictionary *dicObject;

@property (strong, nonatomic) NSString *cont_title;

@end
