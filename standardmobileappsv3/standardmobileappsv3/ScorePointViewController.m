//
//  ScorePointViewController.m
//  standardmobileappsv3
//
//  Created by M3Online on 12/26/14.
//  Copyright (c) 2014 M3Online. All rights reserved.
//

#import "ScorePointViewController.h"
#import "SettingViewController.h"
#import "AppDelegate.h"
#import "LoadingView.h"
#import "NSString+HTML.h"
#import <sqlite3.h>
#import "TxtFiles.h"
#import "SBJson.h"
#import <SystemConfiguration/SystemConfiguration.h>
#import "RootViewController.h"
#import "ViewController.h"
#import "InitialSlidingViewController.h"
#import "BTSlideInteractor.h"
#import "Reachability.h"

@interface ScorePointViewController ()
@property (strong, nonatomic) BTSlideInteractor *interactor;
@end

//float tabButtonHeight_iphone = 45.0f;
//float tabButtonHeight_ipad = 65.0f;
InitialSlidingViewController *modalController;

AppDelegate *appdelegate;
__strong LoadingView *loadingView;
__strong NSString *currentRequest;
__strong NSMutableData *receivedData;
__strong NSString *cont_id, *level_icon, *level_name, *points;
__strong UIImageView *imgViewMenu;
__strong UILabel *lblpoint;
__strong UILabel *lblpoint1;
__strong UILabel *lbllevel;
__strong UILabel *lbllevel1;
__strong NSString *apluginvalue2;
__strong NSString *apluginvalue3;

@implementation ScorePointViewController
UIAlertView *alert;
UIActivityIndicatorView *indicator;
@synthesize view_login, view_scorepointList, arrayScorePointList, table_scorepointList, cont_title;
@synthesize dicObject;

- (UIColor *) colorWithHexString:(NSString *)hexString {
    NSString *colorString = [[hexString stringByReplacingOccurrencesOfString: @"#" withString: @""] uppercaseString];
    CGFloat alpha, red, blue, green;
    alpha = 1.0f;
    red   = [self colorComponentFrom: colorString start: 0 length: 2];
    green = [self colorComponentFrom: colorString start: 2 length: 2];
    blue  = [self colorComponentFrom: colorString start: 4 length: 2];
    return [UIColor colorWithRed: red green: green blue: blue alpha: alpha];
}

- (CGFloat) colorComponentFrom:(NSString *)string start:(NSUInteger)start length:(NSUInteger)length {
    NSString *substring = [string substringWithRange: NSMakeRange(start, length)];
    NSString *fullHex = length == 2 ? substring : [NSString stringWithFormat: @"%@%@", substring, substring];
    unsigned hexComponent;
    [[NSScanner scannerWithString: fullHex] scanHexInt: &hexComponent];
    return hexComponent / 255.0;
}

-(id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super initWithCoder:aDecoder];
    if(self) {
        //_navDelegate = [SCNavControllerDelegate new];
        //self.delegate = _navDelegate;
    }
    return self;
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        self.view.backgroundColor = [self colorWithHexString:appdelegate.themeColorCode1];
    }
    return self;
}

- (BOOL) isConnectionAvailable
{
    Reachability *networkReachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus networkStatus = [networkReachability currentReachabilityStatus];
    if (networkStatus == NotReachable) {
        //NSLog(@"There IS NO internet connection");
        return FALSE;
    } else {
        //NSLog(@"There IS internet connection");
        return TRUE;
    }
}

- (void)startupAnimationDoneIndicatorLoadPage:(NSString *)animationID finished:(NSNumber *)finished context:(void *)context
{
    [self loadAllData];
    //[self processdata:@""];
    [self dissmissIndicator];
    alert =nil;
}

- (void) loadAllData {
    
    if ([self isConnectionAvailable])
    {
        
        //NSString * timeStampValueSync = [NSString stringWithFormat:@"%ld", (long)[[NSDate date] timeIntervalSince1970]];
        
        //sqlite3 *database;
        // Open the database from the users filessytem
        //if(sqlite3_open([appdelegate.databasePath UTF8String], &database) == SQLITE_OK) {
            
        NSString *urlAddress11 = [[NSString alloc] initWithFormat:@"%@point_customer.php?db=%@&userid=%@&udid=%@&cuid=%@", appdelegate.user_apipath, appdelegate.db, appdelegate.user, appdelegate.uuid, appdelegate.customer_id];
        currentRequest = @"scorepoint";
        //NSLog(@"urlAddress11: %@", urlAddress11);
        [self connectToServer:urlAddress11];
        
            //TxtFiles *readFile11 = [[TxtFiles alloc] init];
            //NSString *ReadData11 = [readFile11 readFromFile:readFile11.setFilenamewarranty];
            
            //SBJsonParser *jsonParser11 = [SBJsonParser new];
            //NSDictionary *jsonData11 = (NSDictionary *) [jsonParser11 objectWithString:ReadData11 error:nil];
            
            //NSLog(@"jsonData6: %@", jsonData6);
            
            /*NSMutableDictionary *dictionary12 = [jsonData11 mutableCopy];
            //NSLog(@"warranty:: %@", dictionary12);
            for(NSMutableDictionary *row in dictionary12){
                NSString *war_id = [row objectForKey:@"id"];
                NSString *war_user_id = [row objectForKey:@"user_id"];
                NSString *war_customer_id = [row objectForKey:@"customer_id"];
                NSString *war_unregister_id = [row objectForKey:@"unregister_id"];
                
                NSString *war_title;
                if([row objectForKey:@"title"] == (NSString*)[NSNull null]) {
                    war_title = @"";
                } else {
                    war_title = [[row objectForKey:@"title"] stringByReplacingOccurrencesOfString:@"'" withString:@"''"];
                }
                
                NSString *war_dealer;
                if([row objectForKey:@"dealer"] == (NSString*)[NSNull null]) {
                    war_dealer = @"";
                } else {
                    war_dealer = [[row objectForKey:@"dealer"] stringByReplacingOccurrencesOfString:@"'" withString:@"''"];
                }
                
                NSString *war_serialnum;
                if([row objectForKey:@"serial_number"] == (NSString*)[NSNull null]) {
                    war_serialnum = @"";
                } else {
                    war_serialnum = [[row objectForKey:@"serial_number"] stringByReplacingOccurrencesOfString:@"'" withString:@"''"];
                }
                
                NSString *war_modelnum;
                if([row objectForKey:@"model_number"] == (NSString*)[NSNull null]) {
                    war_modelnum = @"";
                } else {
                    war_modelnum = [[row objectForKey:@"model_number"] stringByReplacingOccurrencesOfString:@"'" withString:@"''"];
                }
                
                NSString *war_purchasedate;
                if([row objectForKey:@"purchase_date"] == (NSString*)[NSNull null]) {
                    war_purchasedate = @"";
                } else {
                    war_purchasedate = [[row objectForKey:@"purchase_date"] stringByReplacingOccurrencesOfString:@"'" withString:@"''"];
                }
                
                NSString *war_period;
                if([row objectForKey:@"warranty_period"] == (NSString*)[NSNull null]) {
                    war_period = @"";
                } else {
                    war_period = [[row objectForKey:@"warranty_period"] stringByReplacingOccurrencesOfString:@"'" withString:@"''"];
                }
                
                NSString *war_icon;
                if([row objectForKey:@"icon"] == (NSString*)[NSNull null]) {
                    war_icon = @"";
                } else {
                    war_icon = [[row objectForKey:@"icon"] stringByReplacingOccurrencesOfString:@"'" withString:@"''"];
                }
                
                NSString *war_imagproduct;
                if([row objectForKey:@"img_product"] == (NSString*)[NSNull null]) {
                    war_imagproduct = @"";
                } else {
                    war_imagproduct = [[row objectForKey:@"img_product"] stringByReplacingOccurrencesOfString:@"'" withString:@"''"];
                }
                
                NSString *war_imgreceipt;
                if([row objectForKey:@"img_receipt"] == (NSString*)[NSNull null]) {
                    war_imgreceipt = @"";
                } else {
                    war_imgreceipt = [[row objectForKey:@"img_receipt"] stringByReplacingOccurrencesOfString:@"'" withString:@"''"];
                }
                
                NSString *war_imgwar;
                if([row objectForKey:@"img_warranty"] == (NSString*)[NSNull null]) {
                    war_imgwar = @"";
                } else {
                    war_imgwar = [[row objectForKey:@"img_warranty"] stringByReplacingOccurrencesOfString:@"'" withString:@"''"];
                }
                
                NSString *war_comments;
                if([row objectForKey:@"comments"] == (NSString*)[NSNull null]) {
                    war_comments = @"";
                } else {
                    war_comments = [[row objectForKey:@"comments"] stringByReplacingOccurrencesOfString:@"'" withString:@"''"];
                }
                
                NSString *war_dateinserted;
                if([row objectForKey:@"date_inserted"] == (NSString*)[NSNull null]) {
                    war_dateinserted = @"";
                } else {
                    war_dateinserted = [[row objectForKey:@"date_inserted"] stringByReplacingOccurrencesOfString:@"'" withString:@"''"];
                }
                
                NSString *war_dateupdated;
                if([row objectForKey:@"date_updated"] == (NSString*)[NSNull null]) {
                    war_dateupdated = @"";
                } else {
                    war_dateupdated = [[row objectForKey:@"date_updated"] stringByReplacingOccurrencesOfString:@"'" withString:@"''"];
                }
                
                NSString *war_status;
                if([row objectForKey:@"status"] == (NSString*)[NSNull null]) {
                    war_status = @"";
                } else {
                    war_status = [[row objectForKey:@"status"] stringByReplacingOccurrencesOfString:@"'" withString:@"''"];
                }
                
                NSString *war_brand;
                if([row objectForKey:@"brand"] == (NSString*)[NSNull null]) {
                    war_brand = @"";
                } else {
                    war_brand = [[row objectForKey:@"brand"] stringByReplacingOccurrencesOfString:@"'" withString:@"''"];
                }
                
                NSString *war_category;
                if([row objectForKey:@"category"] == (NSString*)[NSNull null]) {
                    war_category = @"";
                } else {
                    war_category = [[row objectForKey:@"category"] stringByReplacingOccurrencesOfString:@"'" withString:@"''"];
                }
                
                int rows=0;
                NSString *temp = [[NSString alloc] initWithFormat:@"SELECT * FROM warranty WHERE id=%@ ",war_id];
                //NSLog(@"temp:: %@", temp);
                const char *sqlStatement = [temp UTF8String];
                sqlite3_stmt *compiledStatement;
                if(sqlite3_prepare_v2(database, sqlStatement, -1, &compiledStatement, NULL) == SQLITE_OK) {
                    // Loop through the results and add them to the feeds array
                    
                    if (sqlite3_step(compiledStatement) == SQLITE_ERROR) {
                        NSAssert1(0,@"Error when counting rows  %s",sqlite3_errmsg(database));
                    } else {
                        rows = sqlite3_column_int(compiledStatement, 0);
                        //NSLog(@"SQLite Rows: %i", rows);
                    }
                    
                    //while(sqlite3_step(compiledStatement) == SQLITE_ROW) {
                    if (rows > 0) {
                        //NSLog(@"database:::::::update conferences");
                        static sqlite3_stmt *compiledupdateStatementwar;
                        sqlite3_exec(database, [[NSString stringWithFormat:@"update warranty SET user_id='%@', customer_id='%@', unregister_id='%@', title='%@', dealer='%@', serial_number='%@', model_number='%@', purchase_date='%@', warranty_period='%@', icon='%@', img_product='%@', img_receipt='%@', img_warranty='%@', comments='%@', date_inserted='%@', date_updated='%@', status='%@', brand='%@', category='%@', lastsync_date='%@' where id='%@' ", war_user_id, war_customer_id, war_unregister_id, war_title, war_dealer, war_serialnum, war_modelnum, war_purchasedate, war_period, war_icon, war_imagproduct, war_imgreceipt, war_imgwar, war_comments, war_dateinserted, war_dateupdated, war_status, war_brand, war_category, timeStampValueSync, war_id] UTF8String], NULL, NULL, NULL);
                        sqlite3_finalize(compiledupdateStatementwar);
                    } else {
                        //NSLog(@"database:::::::insert into ibeacon_device");
                        static sqlite3_stmt *compiledinsertStatementwar;
                        sqlite3_exec(database, [[NSString stringWithFormat:@"insert into warranty (id, user_id, customer_id, unregister_id, title, dealer, serial_number, model_number, purchase_date, warranty_period, icon, img_product, img_receipt, img_warranty, comments, date_inserted, date_updated, status, brand, category, lastsync_date) values ('%@', '%@', '%@', '%@', '%@', '%@', '%@', '%@', '%@', '%@', '%@', '%@', '%@', '%@', '%@', '%@', '%@', '%@', '%@', '%@', '%@')", war_id,war_user_id, war_customer_id, war_unregister_id, war_title, war_dealer, war_serialnum, war_modelnum, war_purchasedate, war_period, war_icon, war_imagproduct, war_imgreceipt, war_imgwar, war_comments, war_dateinserted, war_dateupdated, war_status, war_brand, war_category, timeStampValueSync] UTF8String], NULL, NULL, NULL);
                        sqlite3_finalize(compiledinsertStatementwar);
                    }
                }
                // Release the compiled statement from memory
                sqlite3_finalize(compiledStatement);
                
            }
            //NSLog(@"database:::::::Delete not synced posters");
            static sqlite3_stmt *compiledinsertStatement3k;
            sqlite3_exec(database, [[NSString stringWithFormat:@"Delete from warranty where lastsync_date<>%@", timeStampValueSync] UTF8String], NULL, NULL, NULL);
            sqlite3_finalize(compiledinsertStatement3k); */
            
        //}
    } else {
        [self displayAlert:NSLocalizedString(@"MSG_NO_INTERNET", nil)];
    }
}

- (void)viewDidAppear:(BOOL)animated {
    
    sqlite3 *database;
    
    if(sqlite3_open([appdelegate.databasePath UTF8String], &database) == SQLITE_OK) {
        NSString *theme_object = [[NSString alloc] initWithFormat:@"SELECT * from theme_object_plugin WHERE id = %@", appdelegate.plugin_id];
        //NSLog(@"theme_object_plugin:: %@", theme_object);
        
        const char *sqlStatement1 = [theme_object UTF8String];
        sqlite3_stmt *compiledStatement1;
        if(sqlite3_prepare_v2(database, sqlStatement1, -1, &compiledStatement1, NULL) == SQLITE_OK) {
            while(sqlite3_step(compiledStatement1) == SQLITE_ROW) {
                
                apluginvalue2 = [NSString stringWithUTF8String:(char *)sqlite3_column_text(compiledStatement1, 5)];
                apluginvalue3 = [NSString stringWithUTF8String:(char *)sqlite3_column_text(compiledStatement1, 6)];
                
            }
        }
        // Release the compiled statement from memory
        sqlite3_finalize(compiledStatement1);
        
    }
    sqlite3_close(database);
    
    if (appdelegate.showloading) {
        alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"loading", nil) message:nil delegate:self cancelButtonTitle:nil otherButtonTitles: nil];
        
        //a simple activity indicator:
        UIActivityIndicatorView *activityIndicator = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
        activityIndicator.frame= CGRectMake(50, 10, 37, 37);
        activityIndicator.autoresizingMask = UIViewAutoresizingFlexibleTopMargin | UIViewAutoresizingFlexibleRightMargin | UIViewAutoresizingFlexibleBottomMargin | UIViewAutoresizingFlexibleLeftMargin;
        [activityIndicator startAnimating];
        
        //the magic line below,
        //we associate the activity indicator to the alert view: (addSubview is not used)
        [alert setValue:activityIndicator forKey:@"accessoryView"];
        
        [alert show];
    
        // Adjust the indicator so it is up a few pixels from the bottom of the alert
        [UIView beginAnimations:nil context:nil];
        [UIView setAnimationDuration:0.0];
        [UIView setAnimationDelay:0.0];
        [UIView setAnimationTransition:UIViewAnimationTransitionNone forView:alert cache:YES];
        [UIView setAnimationDelegate:self];
        [UIView setAnimationDidStopSelector:@selector(startupAnimationDoneIndicatorLoadPage:finished:context:)];
        alert.alpha = 0.5;
        [UIView commitAnimations];
    } else {
        [self loadAllData];
    }
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSString *hasLoggedInFB = [defaults objectForKey:@"hasLoggedInFB"];
    NSString *hasLoggedInMyApp = [defaults objectForKey:@"hasLoggedInMyApp"];
    
    if ([hasLoggedInFB isEqualToString:@"YES"] || [hasLoggedInMyApp isEqualToString:@"YES"])
    {
        if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone) {
            // For iPhone
            view_scorepointList.frame = CGRectMake(0, 150, self.view.frame.size.width, self.view.frame.size.height-150);
        } else {
            view_scorepointList.frame = CGRectMake(0, 200, self.view.frame.size.width, self.view.frame.size.height-200);
        }
        //[self.view addSubview:view_scorepointList];
    }
    else
        [self.view addSubview:view_login];
    
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    self.navigationItem.backBarButtonItem = [[UIBarButtonItem alloc]initWithTitle:@"" style:UIBarButtonItemStylePlain target:nil action:nil];
    
    appdelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    
    if (![appdelegate.backgroundT isEqualToString:@""]) {
        
        NSString *filePath = appdelegate.backgroundT;
        
        NSRange end = [filePath rangeOfString:@"/" options:NSBackwardsSearch];
        NSString *shortString;
        if (end.location != NSNotFound) {
            shortString =[filePath substringWithRange:NSMakeRange(end.location+1, filePath.length-(end.location+1))];
        } else {
            shortString = filePath;
        }
        
        BOOL success;
        
        // Create a FileManager object, we will use this to check the status
        // of the database and to copy it over if required
        NSFileManager *fileManager = [NSFileManager defaultManager];
        
        // Check if the database has already been created in the users filesystem
        success = [fileManager fileExistsAtPath:[[[NSBundle mainBundle] resourcePath] stringByAppendingPathComponent:shortString]];
        
        UIImageView *imgV = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height)];
        //[self addSubview:imgV];
        
        //__block UIActivityIndicatorView *activityIndicator;
        __weak UIImageView *weakImageView = imgV;
        NSString *imagePath;
        if(success) {
            imagePath = [[[NSBundle mainBundle] resourcePath] stringByAppendingPathComponent:shortString];
            [weakImageView setImage:[UIImage imageNamed:shortString]];
            
            //NSLog(@"shortStringimagePath : %@", imagePath);
            imgV.contentMode  = UIViewContentModeScaleAspectFit;
            [imgV setClipsToBounds:YES];
            
            UIGraphicsBeginImageContext(self.view.frame.size);
            CGRect rect=CGRectMake(0,-64,self.view.bounds.size.width,self.view.bounds.size.height+64);
            [imgV.image drawInRect:rect];
            UIImage *image1 = UIGraphicsGetImageFromCurrentImageContext();
            UIGraphicsEndImageContext();
            self.view.backgroundColor = [UIColor colorWithPatternImage:image1];
        } else {
            imagePath = filePath;
            NSURL *imageURL = [NSURL URLWithString:imagePath];
            NSData *imageData = [NSData dataWithContentsOfURL:imageURL];
            //self.backgroundColor = [UIColor redColor];
            UIGraphicsBeginImageContext(self.view.frame.size);
            CGRect rect=CGRectMake(0,-64,self.view.bounds.size.width,self.view.bounds.size.height+64);
            [[UIImage imageWithData:imageData] drawInRect:rect];
            UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
            UIGraphicsEndImageContext();
            self.view.backgroundColor = [UIColor colorWithPatternImage:image];
        }
    } else {
        self.view.backgroundColor = [UIColor clearColor];
    }
    
    self.title = cont_title;
    appdelegate.homeclicked = NO;
    
    CGRect rect = CGRectMake(0,0,20,20);
    UIGraphicsBeginImageContext( rect.size );
    [[UIImage imageNamed:@"home.png"] drawInRect:rect];
    UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    UIBarButtonItem *myHomeButton = [[UIBarButtonItem alloc] initWithImage:image style:UIBarButtonItemStyleDone target:self action:@selector(myHomeAction:)];
    
    
    NSArray *myButtonArray = [[NSArray alloc] initWithObjects:myHomeButton, nil];
    
    self.navigationItem.rightBarButtonItems = myButtonArray;
    
    //arrayWarrantyList = [[NSMutableArray alloc]init];
    
    arrayScorePointList = [[NSMutableArray alloc] init];
    //[table_warrantyList setFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height-200)];
    [self.view addSubview:view_scorepointList];
    
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone) {
        // For iPhone
        imgViewMenu = [[UIImageView alloc] initWithFrame:CGRectMake((self.view.frame.size.width/4)-30, 10, 80, 80)];
    } else {
        imgViewMenu = [[UIImageView alloc] initWithFrame:CGRectMake((self.view.frame.size.width/2), 10, 100, 100)];
    }
    
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone) {
        lblpoint = [[UILabel alloc] initWithFrame: CGRectMake(5, 100, self.view.frame.size.width/4, 20)];
    } else {
        lblpoint = [[UILabel alloc] initWithFrame: CGRectMake(5, 120, self.view.frame.size.width/2+50, 30)];
    }
    
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone) {
        lblpoint1 = [[UILabel alloc] initWithFrame: CGRectMake((self.view.frame.size.width/4)+10, 100, self.view.frame.size.width/4, 20)];
    } else {
        lblpoint1 = [[UILabel alloc] initWithFrame: CGRectMake((self.view.frame.size.width/2)+10+50, 120, self.view.frame.size.width/2, 30)];
    }
    
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone) {
        lbllevel = [[UILabel alloc] initWithFrame: CGRectMake(5, 120, self.view.frame.size.width/4, 20)];
    } else {
        lbllevel = [[UILabel alloc] initWithFrame: CGRectMake(5, 150, self.view.frame.size.width/2+50, 30)];
    }
    
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone) {
        lbllevel1 = [[UILabel alloc] initWithFrame: CGRectMake((self.view.frame.size.width/4)+10, 120, self.view.frame.size.width/4, 20)];
    } else {
        lbllevel1 = [[UILabel alloc] initWithFrame: CGRectMake((self.view.frame.size.width/2)+10+50, 150, self.view.frame.size.width/2, 30)];
    }
    //[self processdata:@""];
}

//Slide Menu
- (void)willAnimateRotationToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation duration:(NSTimeInterval)duration
{
    //NSLog(@"view frame=%@ bounds=%@", NSStringFromCGRect(self.view.frame), NSStringFromCGRect(self.view.bounds));
}


- (void)showModalButtonWasTouched
{
    self.interactor = [[BTSlideInteractor alloc] init];
    self.interactor.presenting = YES;
    
    //BTModalViewController *modalController = [self.storyboard instantiateViewControllerWithIdentifier:@"ModalViewController"];
    modalController = [[InitialSlidingViewController alloc] init];
    modalController.modalPresentationStyle = UIModalPresentationCustom;
    modalController.transitioningDelegate = self.interactor;
    [self.navigationController presentViewController:modalController animated:YES completion:nil];
    //[self.navigationController popToViewController:modalController animated:NO];
    //[self.navigationController pushViewController:modalController animated:YES];
}
// End Slide Menu

-(void)myHomeAction:(UIBarButtonItem *)sender{
    
    appdelegate.nextPage = @"";
    
    if ([appdelegate.listOfVerMenu count]>0) {
        if (appdelegate.homeclicked==NO) {
            appdelegate.homeclicked = YES;
            [self showModalButtonWasTouched];
        } else {
            appdelegate.homeclicked = NO;
            [modalController dismissViewControllerAnimated:YES completion:nil];
        }
    } else {
        //AppDelegate *appdelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
        //NSLog(@"View Controllers : %@",[appdelegate.navController viewControllers]);
        NSMutableArray *navigationArray = [[NSMutableArray alloc] initWithArray: appdelegate.navController.viewControllers];
        
        for (UIViewController* viewController in appdelegate.navController.viewControllers) {
            
            //This if condition checks whether the viewController's class is MyGroupViewController
            // if true that means its the MyGroupViewController (which has been pushed at some point)
            if ([viewController isKindOfClass:[RootViewController class]] ) {
                
                // Here viewController is a reference of UIViewController base class of MyGroupViewController
                // but viewController holds MyGroupViewController  object so we can type cast it here
                //ThemeViewController *themeViewController = (ThemeViewController*)viewController;
                //[self.navigationController popToViewController:themeViewController animated:YES];
                [navigationArray removeObjectAtIndex: 0];
            }
        }
        appdelegate.navController.viewControllers = navigationArray;
        for (UIViewController* viewController in appdelegate.navController.viewControllers) {
            
            //This if condition checks whether the viewController's class is MyGroupViewController
            // if true that means its the MyGroupViewController (which has been pushed at some point)
            if ([viewController isKindOfClass:[ViewController class]] ) {
                
                // Here viewController is a reference of UIViewController base class of MyGroupViewController
                // but viewController holds MyGroupViewController  object so we can type cast it here
                ViewController *themeViewController = (ViewController*)viewController;
                [appdelegate.navController popToViewController:themeViewController animated:NO];
                //[self.navigationController popToViewController:viewController animated:YES];
            }
        }
        //NSLog(@"View Controllers111 : %@",appdelegate.navController.viewControllers);
    }
    
}

#pragma mark UITableView Delegation
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [arrayScorePointList count];
    //return 1;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    if(tableView == table_scorepointList) {
        if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone) {
            // For iPhone
            return 70;
        } else {
            return 110;
        }
    }
    
    return 50;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    //if (tableView == table_warrantyList) {
    static NSString *CellIdentifier = @"CellIdentifier";
    UITableViewCell *cell = [table_scorepointList dequeueReusableCellWithIdentifier:CellIdentifier];
    
    UIImageView *imgViewMenu;
    const NSInteger LEFT_TAG = 2000;
    UILabel *menutext;
    const NSInteger RIGHT_TAG = 2001;
    UILabel *details;
    const NSInteger RIGHT_TAG1 = 2002;
    
    if (cell == nil)
    {
        
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
        //cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        
        //if ([[dictionary objectForKey:@"object_type"] isEqualToString:@"img"]) {
        if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone) {
            // For iPhone
            imgViewMenu = [[UIImageView alloc] initWithFrame:CGRectMake(5, 5, 60, 60)];
        } else {
            imgViewMenu = [[UIImageView alloc] initWithFrame:CGRectMake(5, 5, 100, 100)];
        }
        imgViewMenu.tag = LEFT_TAG;
        [cell.contentView addSubview:imgViewMenu];
        
        //} else if ([[dictionary objectForKey:@"object_type"] isEqualToString:@"text"]) {
        
        if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone) {
            // For iPhone
            menutext = [[UILabel alloc] initWithFrame: CGRectMake(70, 5, table_scorepointList.frame.size.width-70-65, 20)];
        } else {
            menutext = [[UILabel alloc] initWithFrame: CGRectMake(110, 5, table_scorepointList.frame.size.width-110-65, 33)];
        }
        menutext.tag = RIGHT_TAG;
        [menutext setFont:[UIFont systemFontOfSize:14]];
        menutext.textColor = [self colorWithHexString:appdelegate.themeColorCode1];
        menutext.textAlignment = NSTextAlignmentLeft;
        menutext.backgroundColor = [UIColor clearColor];
        menutext.numberOfLines = 0;
        [cell.contentView addSubview:menutext];
        
        if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone) {
            // For iPhone
            details = [[UILabel alloc] initWithFrame: CGRectMake(70, 25, table_scorepointList.frame.size.width-70-65, 40)];
        } else {
            details = [[UILabel alloc] initWithFrame: CGRectMake(110, 38, table_scorepointList.frame.size.width-110-65, 66)];
        }
        details.tag = RIGHT_TAG1;
        [details setFont:[UIFont systemFontOfSize:14]];
        details.textColor = [UIColor grayColor];
        details.textAlignment = NSTextAlignmentLeft;
        details.backgroundColor = [UIColor clearColor];
        details.numberOfLines = 0;
        details.lineBreakMode = NSLineBreakByWordWrapping;
        //[details sizeToFit];
        //[details sizeThatFits:CGSizeMake(table_scorepointList.frame.size.width-70-65, 20)];
        //[details setAdjustsFontSizeToFitWidth:YES];
        [cell.contentView addSubview:details];
        
        /*if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone) {
         // For iPhone
         expiring = [[UILabel alloc] initWithFrame: CGRectMake(table_warrantyList.frame.size.width-65, 5, 60, 20)];
         } else {
         expiring = [[UILabel alloc] initWithFrame: CGRectMake(table_warrantyList.frame.size.width-65, 5, 60, 33)];
         }
         expiring.tag = RIGHT_TAG3;
         [expiring setFont:[UIFont systemFontOfSize:14]];
         expiring.textColor = [UIColor grayColor];
         expiring.textAlignment = NSTextAlignmentLeft;
         expiring.backgroundColor = [UIColor clearColor];
         expiring.numberOfLines = 0;
         [cell.contentView addSubview:expiring];
         //} */
        
    }
    else
    {
        //if ([[dictionary objectForKey:@"object_type"] isEqualToString:@"img"]) {
        imgViewMenu = (UIImageView *)[cell viewWithTag:LEFT_TAG];
        //} else if ([[dictionary objectForKey:@"object_type"] isEqualToString:@"text"]) {
        menutext = (UILabel *)[cell viewWithTag:RIGHT_TAG];
        details = (UILabel *)[cell viewWithTag:RIGHT_TAG1];
    }
    
    if([arrayScorePointList count]>0)
    {
        NSDictionary *dictionary = [arrayScorePointList objectAtIndex:indexPath.row];
        
        NSString *imgURL = [dictionary objectForKey:@"level_icon"];
        
        __block UIActivityIndicatorView *activityIndicator;
        __weak UIImageView *weakImageView = imgViewMenu;
        /*[weakImageView setImageWithURL:[NSURL URLWithString:imgURL] placeholderImage:[UIImage imageNamed:@"No-Image-icon.png"] options:SDWebImageProgressiveDownload progress:^(NSInteger receivedSize, NSInteger expectedSize)
         {
             if (!activityIndicator)
             {
                 [weakImageView addSubview:activityIndicator = [UIActivityIndicatorView.alloc initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray]];
                 activityIndicator.center = weakImageView.center;
                 [activityIndicator startAnimating];
             }
         }
                             completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType)
         {
             [activityIndicator removeFromSuperview];
             activityIndicator = nil;
         }]; */
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0), ^ {
            [weakImageView sd_setImageWithURL:[NSURL URLWithString:imgURL]
                             placeholderImage:[UIImage imageNamed:@"No-Image-icon.png"]
                                      options:SDWebImageProgressiveDownload
                                     progress:^(NSInteger receivedSize, NSInteger expectedSize, NSURL *targetURL) {
                                         dispatch_async(dispatch_get_main_queue(), ^ {
                                             if (!activityIndicator) {
                                                 
                                                 [weakImageView addSubview:activityIndicator = [UIActivityIndicatorView.alloc initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray]];
                                                 activityIndicator.center = weakImageView.center;
                                                 [activityIndicator startAnimating];
                                             }
                                         });
                                     }
                                    completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
                                        dispatch_async(dispatch_get_main_queue(), ^ {
                                            //[MBProgressHUD hideHUDForView:self.view animated:YES];
                                            [activityIndicator removeFromSuperview];
                                            activityIndicator = nil;
                                        });
                                    }];
        });
        
        imgViewMenu.contentMode  = UIViewContentModeScaleAspectFill;
        [imgViewMenu setClipsToBounds:YES];
        
        menutext.text = [NSString stringWithFormat:@"%@", [dictionary objectForKey:@"level_name"]];
        details.text = [NSString stringWithFormat:@"%@", [dictionary objectForKey:@"level_details"]];
        
    }
    
    imgViewMenu =nil;
    menutext =nil;
    details =nil;
    CellIdentifier =nil;
    
    return cell;
    //}
    
    //return nil;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    //if (tableView == view_warrantyList)
    //{
    
    /*NSDictionary *dictionary = [arrayScorePointList objectAtIndex:indexPath.row];
    
    cont_id = [dictionary objectForKey:@"id"];
    //cont_title = [dictionary objectForKey:@"title"];
    
    //ContentListViewController *f = [[ContentListViewController alloc] initWithNibName:@"ContentListViewController" bundle:[NSBundle mainBundle]];
    //[self.navigationController pushViewController:f animated:YES];
    alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"loading", nil) message:nil delegate:self cancelButtonTitle:nil otherButtonTitles: nil];
    
    //a simple activity indicator:
    UIActivityIndicatorView *activityIndicator = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
    activityIndicator.frame= CGRectMake(50, 10, 37, 37);
    activityIndicator.autoresizingMask = UIViewAutoresizingFlexibleTopMargin | UIViewAutoresizingFlexibleRightMargin | UIViewAutoresizingFlexibleBottomMargin | UIViewAutoresizingFlexibleLeftMargin;
    [activityIndicator startAnimating];
    
    //the magic line below,
    //we associate the activity indicator to the alert view: (addSubview is not used)
    [alert setValue:activityIndicator forKey:@"accessoryView"];
    
    [alert show];
    
    // Adjust the indicator so it is up a few pixels from the bottom of the alert
    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationDuration:0.1];
    [UIView setAnimationDelay:0.0];
    [UIView setAnimationTransition:UIViewAnimationTransitionNone forView:alert cache:YES];
    [UIView setAnimationDelegate:self];
    [UIView setAnimationDidStopSelector:@selector(startupAnimationDoneIndicator:finished:context:)];
    alert.alpha = 0.5;
    [UIView commitAnimations]; */
    
    //}
}

- (IBAction)login:(id)sender
{
    NSString *nibFileName;
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone) {
        nibFileName = @"SettingViewController";
    } else {
        nibFileName = @"SettingViewController_ipad";
    }
    SettingViewController *s = [[SettingViewController alloc] initWithNibName:nibFileName bundle:nil];
    appdelegate.shouldReturnToPrevPage = YES;
    //LoyaltyViewController *l = [[LoyaltyViewController alloc] initWithNibName:@"LoyaltyViewController" bundle:nil];
    appdelegate.originVC = self;
    [appdelegate.navController pushViewController:s animated:NO];
    
    [view_login removeFromSuperview];
}

-(void) processdata:(NSString *)type {
    
    //AppDelegate *appdelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    sqlite3 *database;
    
    if(sqlite3_open([appdelegate.databasePath UTF8String], &database) == SQLITE_OK) {
        // Setup the SQL Statement and compile it for faster access
        NSString *theme_object;
        theme_object = [[NSString alloc] initWithFormat:@"SELECT * from warranty where status > 0 order by title"];
        
        [arrayScorePointList removeAllObjects];
        //NSLog(@"theme_objectwarranty:: %@", theme_object);
        const char *sqlStatement1 = [theme_object UTF8String];
        sqlite3_stmt *compiledStatement1;
        if(sqlite3_prepare_v2(database, sqlStatement1, -1, &compiledStatement1, NULL) == SQLITE_OK) {
            
            while(sqlite3_step(compiledStatement1) == SQLITE_ROW) {
                
                NSString *aidobj = [NSString stringWithUTF8String:(char *)sqlite3_column_text(compiledStatement1, 0)];
                NSString *acustid = [NSString stringWithUTF8String:(char *)sqlite3_column_text(compiledStatement1, 2)];
                NSString *aunregid = [NSString stringWithUTF8String:(char *)sqlite3_column_text(compiledStatement1, 3)];
                NSString *atitle = [NSString stringWithUTF8String:(char *)sqlite3_column_text(compiledStatement1, 4)];
                NSString *adealer = [NSString stringWithUTF8String:(char *)sqlite3_column_text(compiledStatement1, 5)];
                NSString *aserialnum = [NSString stringWithUTF8String:(char *)sqlite3_column_text(compiledStatement1, 6)];
                NSString *amodelnum = [NSString stringWithUTF8String:(char *)sqlite3_column_text(compiledStatement1, 7)];
                NSString *apurchdate = [NSString stringWithUTF8String:(char *)sqlite3_column_text(compiledStatement1, 8)];
                NSString *awarranty_period = [NSString stringWithUTF8String:(char *)sqlite3_column_text(compiledStatement1, 9)];
                NSString *aicon = [NSString stringWithUTF8String:(char *)sqlite3_column_text(compiledStatement1, 10)];
                NSString *aimgprod = [NSString stringWithUTF8String:(char *)sqlite3_column_text(compiledStatement1, 11)];
                NSString *aimgreceipt = [NSString stringWithUTF8String:(char *)sqlite3_column_text(compiledStatement1, 12)];
                NSString *aimgwarranty = [NSString stringWithUTF8String:(char *)sqlite3_column_text(compiledStatement1, 13)];
                NSString *acomments = [NSString stringWithUTF8String:(char *)sqlite3_column_text(compiledStatement1, 14)];
                NSString *adateinsert = [NSString stringWithUTF8String:(char *)sqlite3_column_text(compiledStatement1, 15)];
                NSString *adateupd = [NSString stringWithUTF8String:(char *)sqlite3_column_text(compiledStatement1, 16)];
                NSString *astatus = [NSString stringWithUTF8String:(char *)sqlite3_column_text(compiledStatement1, 17)];
                NSString *abrand = [NSString stringWithUTF8String:(char *)sqlite3_column_text(compiledStatement1, 18)];
                NSString *acategory = [NSString stringWithUTF8String:(char *)sqlite3_column_text(compiledStatement1, 19)];
                
                atitle = [atitle stringByDecodingHTMLEntities];
                aserialnum = [aserialnum stringByDecodingHTMLEntities];
                amodelnum = [amodelnum stringByDecodingHTMLEntities];
                acomments = [acomments stringByDecodingHTMLEntities];
                abrand = [abrand stringByDecodingHTMLEntities];
                acategory = [acategory stringByDecodingHTMLEntities];
                atitle = [atitle stringByReplacingOccurrencesOfString:@"''" withString:@"'"];
                aserialnum = [aserialnum stringByReplacingOccurrencesOfString:@"''" withString:@"'"];
                amodelnum = [amodelnum stringByReplacingOccurrencesOfString:@"''" withString:@"'"];
                acomments = [acomments stringByReplacingOccurrencesOfString:@"''" withString:@"'"];
                abrand = [abrand stringByReplacingOccurrencesOfString:@"''" withString:@"'"];
                acategory = [acategory stringByReplacingOccurrencesOfString:@"''" withString:@"'"];
                
                dicObject = [NSDictionary dictionaryWithObjectsAndKeys:aidobj,@"id",acustid,@"customer_id",aunregid,@"unregister_id",atitle,@"title",adealer,@"dealer",aserialnum,@"serial_number",amodelnum,@"model_number",apurchdate,@"purchase_date",awarranty_period,@"warranty_period",aicon,@"icon",aimgprod,@"img_product",aimgreceipt,@"img_receipt",aimgwarranty,@"img_warranty",acomments,@"comments",adateinsert,@"date_inserted",adateupd,@"date_updated",astatus,@"status",abrand,@"brand",acategory,@"category",nil];
                [arrayScorePointList addObject:dicObject];
                //NSLog(@"arrayWarrantyList: %@", arrayWarrantyList);
                
                aidobj =nil;
                acustid =nil;
                aunregid =nil;
                atitle =nil;
                adealer =nil;
                aserialnum =nil;
                amodelnum =nil;
                apurchdate =nil;
                awarranty_period =nil;
                aicon =nil;
                aimgprod =nil;
                aimgreceipt =nil;
                aimgwarranty =nil;
                acomments =nil;
                adateinsert =nil;
                adateupd =nil;
                astatus =nil;
                abrand =nil;
                acategory =nil;
                
            }
            
            [table_scorepointList reloadData];
        }
        // Release the compiled statement from memory
        sqlite3_finalize(compiledStatement1);
    }
    sqlite3_close(database);
    
}

-(void) dissmissIndicator {
    [alert dismissWithClickedButtonIndex:0 animated:NO];
}

#pragma mark Start Connection

#pragma mark Connect To API
-(void) connectToServer:(NSString *) urlPath
{
    [self addLoadingView];
    
    NSString *urlAddress =[NSString stringWithFormat:@"%@", urlPath];
    
    /*NSURL *url = [NSURL URLWithString:urlAddress];
    
    NSURLRequest *requestObj = [NSURLRequest requestWithURL:url
                                                cachePolicy:NSURLRequestReloadIgnoringLocalAndRemoteCacheData
                                            timeoutInterval:30];
    
    NSURLConnection *theConnection=[[NSURLConnection alloc] initWithRequest:requestObj delegate:self];
    if (theConnection) {
        // Create the NSMutableData to hold the received data.
        // receivedData is an instance variable declared elsewhere.
        receivedData = [NSMutableData data];
    } else {
        // Inform the user that the connection failed.
        //[self displayAlert:@"Connection Error. Please try again later."];
        [self displayAlert:NSLocalizedString(@"MSG_CONNECTION_ERROR", nil)];
        [self removeLoadingView];
    } */
    [self getJsonResponse:urlAddress success:^(NSDictionary *responseDict) {
        
        //NSLog(@"%@",responseDict);
        [self successfunction:responseDict];
        
    } failure:^(NSError *error) {
        // release the connection, and the data object
        
        [self removeLoadingView];
        [self displayAlert:NSLocalizedString(@"MSG_CONNECTION_ERROR", nil)];
    }];
}

-(void)getJsonResponse : (NSString *)urlStr success : (void (^)(NSDictionary *responseDict))success failure:(void(^)(NSError* error))failure
{
    
    
    NSURLSession * session = [NSURLSession sharedSession];
    NSURL * url = [NSURL URLWithString: urlStr];
    
    
    // Asynchronously Api is hit here
    NSURLSessionDataTask * dataTask = [session dataTaskWithURL:url completionHandler:^(NSData *data, NSURLResponse *response, NSError *error)
                                       {
                                           
                                           //NSLog(@"%@",data);
                                           NSDictionary * json;
                                           if ([data length]>0) {
                                               json  = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
                                               //NSLog(@"%@",json);
                                               success(json);
                                           } else {
                                               json = nil;
                                               dispatch_async(dispatch_get_main_queue(), ^ {
                                                   [self removeLoadingView];
                                                   [self displayAlert:NSLocalizedString(@"MSG_CONNECTION_ERROR", nil)];
                                               });
                                           }
                                           
                                           
                                       }];
    
    [dataTask resume] ; // Executed First
    
    
}

-(void) successfunction:(NSDictionary *)responseDict {
    dispatch_async(dispatch_get_main_queue(), ^{
        //NSString *jsonString = [responseDict JSONRepresentation];
        [self removeLoadingView];
        
        //NSLog(@"currentRequest : %@", currentRequest);
        if ([currentRequest isEqualToString:@"scorepoint"])
        {
            arrayScorePointList = [[NSMutableArray alloc]init];
            
            NSDictionary *dictionary = responseDict;
            //NSLog(@"dictionary111 : %@", dictionary);
            
            for(NSDictionary *row in dictionary)
            {
                
                level_icon = [row objectForKey:@"level_icon"];
                level_name = [row objectForKey:@"level_name"];
                points = [row objectForKey:@"score_total"];
                
            }
            //[table_scorepointList reloadData];
            
            NSString *urlAddress22 = [[NSString alloc] initWithFormat:@"%@point_level.php?db=%@&userid=%@&udid=%@&cuid=%@", appdelegate.user_apipath, appdelegate.db, appdelegate.user, appdelegate.uuid, appdelegate.customer_id];
            currentRequest = @"pointlevel";
            //NSLog(@"urlAddress22: %@", urlAddress22);
            [self connectToServer:urlAddress22];
            
        } else if ([currentRequest isEqualToString:@"pointlevel"])
        {
            arrayScorePointList = [[NSMutableArray alloc]init];
            
            NSDictionary *dictionary = responseDict;
            //NSLog(@"dictionary222 : %@", dictionary);
            for(NSDictionary *row in dictionary)
            {
                NSMutableDictionary *mutableDic = [NSMutableDictionary dictionaryWithObjectsAndKeys:
                                                   [row objectForKey:@"id"],@"id",
                                                   [row objectForKey:@"user_id"],@"user_id",
                                                   [row objectForKey:@"m_date"],@"m_date",
                                                   [row objectForKey:@"level_name"],@"level_name",
                                                   
                                                   [row objectForKey:@"level_min"],@"level_min",
                                                   [row objectForKey:@"level_details"],@"level_details",
                                                   [row objectForKey:@"level_icon"],@"level_icon",
                                                   [row objectForKey:@"status"],@"status",
                                                   nil];
                [arrayScorePointList addObject:mutableDic];
            }
            
            
            NSString *imgURL = level_icon;
            
            __block UIActivityIndicatorView *activityIndicator1;
            __weak UIImageView *weakImageView = imgViewMenu;
            
            dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0), ^ {
                [weakImageView sd_setImageWithURL:[NSURL URLWithString:imgURL]
                                 placeholderImage:[UIImage imageNamed:@"No-Image-icon.png"]
                                          options:SDWebImageProgressiveDownload
                                         progress:^(NSInteger receivedSize, NSInteger expectedSize, NSURL *targetURL) {
                                             dispatch_async(dispatch_get_main_queue(), ^ {
                                                 if (!activityIndicator1) {
                                                     
                                                     [weakImageView addSubview:activityIndicator1 = [UIActivityIndicatorView.alloc initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray]];
                                                     activityIndicator1.center = weakImageView.center;
                                                     [activityIndicator1 startAnimating];
                                                 }
                                             });
                                         }
                                        completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
                                            dispatch_async(dispatch_get_main_queue(), ^ {
                                                //[MBProgressHUD hideHUDForView:self.view animated:YES];
                                                [activityIndicator1 removeFromSuperview];
                                                activityIndicator1 = nil;
                                            });
                                        }];
            });
            
            imgViewMenu.contentMode  = UIViewContentModeScaleAspectFill;
            [imgViewMenu setClipsToBounds:YES];
            [self.view addSubview:imgViewMenu];
            
            __strong NSString *yourpoint;
            if ([apluginvalue2 length] > 0) {
                yourpoint = apluginvalue2;
            } else {
                yourpoint = NSLocalizedString(@"yourpoint", nil);
            }
            lblpoint.font = [UIFont boldSystemFontOfSize:16.0f];
            lblpoint.lineBreakMode = NSLineBreakByWordWrapping;
            lblpoint.numberOfLines = 0;
            lblpoint.text = [NSString stringWithFormat:@"%@: ", yourpoint];
            lblpoint.backgroundColor = [UIColor clearColor];
            lblpoint.textColor = [UIColor blackColor];
            lblpoint.textAlignment = NSTextAlignmentRight;
            [self.view addSubview:lblpoint];
            
            lblpoint1.font = [UIFont boldSystemFontOfSize:16.0f];
            lblpoint1.lineBreakMode = NSLineBreakByWordWrapping;
            lblpoint1.numberOfLines = 0;
            lblpoint1.text = points;
            lblpoint1.backgroundColor = [UIColor clearColor];
            lblpoint1.textColor = [UIColor blackColor];
            lblpoint1.textAlignment = NSTextAlignmentLeft;
            [self.view addSubview:lblpoint1];
            
            __strong NSString *yourlevel;
            if ([apluginvalue3 length] > 0) {
                yourlevel = apluginvalue3;
            } else {
                yourlevel = NSLocalizedString(@"yourlevel", nil);
            }
            lbllevel.font = [UIFont boldSystemFontOfSize:16.0f];
            lbllevel.lineBreakMode = NSLineBreakByWordWrapping;
            lbllevel.numberOfLines = 0;
            lbllevel.text = [NSString stringWithFormat:@"%@: ", yourlevel];
            lbllevel.backgroundColor = [UIColor clearColor];
            lbllevel.textColor = [UIColor blackColor];
            lbllevel.textAlignment = NSTextAlignmentRight;
            [self.view addSubview:lbllevel];
            
            lbllevel1.font = [UIFont boldSystemFontOfSize:16.0f];
            lbllevel1.lineBreakMode = NSLineBreakByWordWrapping;
            lbllevel1.numberOfLines = 0;
            lbllevel1.text = level_name;
            lbllevel1.backgroundColor = [UIColor clearColor];
            lbllevel1.textColor = [UIColor blackColor];
            lbllevel1.textAlignment = NSTextAlignmentLeft;
            [self.view addSubview:lbllevel1];
            [table_scorepointList reloadData];
            
        }

    });
}

/*- (void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response
{
    [receivedData setLength:0];
}

- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data
{
    [receivedData appendData:data];
}

- (void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error {
    // release the connection, and the data object
    
    //[connection release];
    //[receivedData release];
    
    [self removeLoadingView];
    [self displayAlert:NSLocalizedString(@"MSG_CONNECTION_ERROR", nil)];
}

- (void)connectionDidFinishLoading:(NSURLConnection *)connection
{
    NSLog(@"Succeed! Received %ld bytes of data",(unsigned long)[receivedData length]);
    [self removeLoadingView];
    
    NSString *response = [[NSString alloc] initWithData:receivedData encoding:NSUTF8StringEncoding];
    //NSLog(@"response : %@", response);
    
    NSLog(@"currentRequest : %@", currentRequest);
    if ([currentRequest isEqualToString:@"scorepoint"])
    {
        arrayScorePointList = [[NSMutableArray alloc]init];
        
        NSDictionary *dictionary = [response JSONValue];
        //NSLog(@"dictionary111 : %@", dictionary);
        
        for(NSDictionary *row in dictionary)
        {
            
            level_icon = [row objectForKey:@"level_icon"];
            level_name = [row objectForKey:@"level_name"];
            points = [row objectForKey:@"score_total"];
 
        }
        //[table_scorepointList reloadData];
        
        NSString *urlAddress22 = [[NSString alloc] initWithFormat:@"%@point_level.php?db=%@&userid=%@&udid=%@&cuid=%@", appdelegate.user_apipath, appdelegate.db, appdelegate.user, appdelegate.uuid, appdelegate.customer_id];
        currentRequest = @"pointlevel";
        //NSLog(@"urlAddress22: %@", urlAddress22);
        [self connectToServer:urlAddress22];
        
    } else if ([currentRequest isEqualToString:@"pointlevel"])
    {
        arrayScorePointList = [[NSMutableArray alloc]init];
        
        NSDictionary *dictionary = [response JSONValue];
        //NSLog(@"dictionary222 : %@", dictionary);
        for(NSDictionary *row in dictionary)
        {
            NSMutableDictionary *mutableDic = [NSMutableDictionary dictionaryWithObjectsAndKeys:
                                               [row objectForKey:@"id"],@"id",
                                               [row objectForKey:@"user_id"],@"user_id",
                                               [row objectForKey:@"m_date"],@"m_date",
                                               [row objectForKey:@"level_name"],@"level_name",
                                               
                                               [row objectForKey:@"level_min"],@"level_min",
                                               [row objectForKey:@"level_details"],@"level_details",
                                               [row objectForKey:@"level_icon"],@"level_icon",
                                               [row objectForKey:@"status"],@"status",
                                               nil];
            [arrayScorePointList addObject:mutableDic];
        }
        
        NSString *imgURL = level_icon;
        
        __block UIActivityIndicatorView *activityIndicator1;
        __weak UIImageView *weakImageView = imgViewMenu;
 
        [weakImageView sd_setImageWithURL:[NSURL URLWithString:imgURL]
                         placeholderImage:[UIImage imageNamed:@"No-Image-icon.png"]
                                  options:SDWebImageProgressiveDownload
                                 progress:^(NSInteger receivedSize, NSInteger expectedSize, NSURL *targetURL) {
                                     if (!activityIndicator1) {
                                         [weakImageView addSubview:activityIndicator1 = [UIActivityIndicatorView.alloc initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray]];
                                         activityIndicator1.center = weakImageView.center;
                                         [activityIndicator1 startAnimating];
                                     }
                                 }
                                completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
                                    [activityIndicator1 removeFromSuperview];
                                    activityIndicator1 = nil;
                                }];
        
        imgViewMenu.contentMode  = UIViewContentModeScaleAspectFill;
        [imgViewMenu setClipsToBounds:YES];
        [self.view addSubview:imgViewMenu];
        
        __strong NSString *yourpoint;
        if ([apluginvalue2 length] > 0) {
            yourpoint = apluginvalue2;
        } else {
            yourpoint = NSLocalizedString(@"yourpoint", nil);
        }
        lblpoint.font = [UIFont boldSystemFontOfSize:16.0f];
        lblpoint.lineBreakMode = NSLineBreakByWordWrapping;
        lblpoint.numberOfLines = 0;
        lblpoint.text = [NSString stringWithFormat:@"%@: ", yourpoint];
        lblpoint.backgroundColor = [UIColor clearColor];
        lblpoint.textColor = [UIColor blackColor];
        lblpoint.textAlignment = NSTextAlignmentRight;
        [self.view addSubview:lblpoint];
        
        lblpoint1.font = [UIFont boldSystemFontOfSize:16.0f];
        lblpoint1.lineBreakMode = NSLineBreakByWordWrapping;
        lblpoint1.numberOfLines = 0;
        lblpoint1.text = points;
        lblpoint1.backgroundColor = [UIColor clearColor];
        lblpoint1.textColor = [UIColor blackColor];
        lblpoint1.textAlignment = NSTextAlignmentLeft;
        [self.view addSubview:lblpoint1];
        
        __strong NSString *yourlevel;
        if ([apluginvalue3 length] > 0) {
            yourlevel = apluginvalue3;
        } else {
            yourlevel = NSLocalizedString(@"yourlevel", nil);
        }
        lbllevel.font = [UIFont boldSystemFontOfSize:16.0f];
        lbllevel.lineBreakMode = NSLineBreakByWordWrapping;
        lbllevel.numberOfLines = 0;
        lbllevel.text = [NSString stringWithFormat:@"%@: ", yourlevel];
        lbllevel.backgroundColor = [UIColor clearColor];
        lbllevel.textColor = [UIColor blackColor];
        lbllevel.textAlignment = NSTextAlignmentRight;
        [self.view addSubview:lbllevel];
        
        lbllevel1.font = [UIFont boldSystemFontOfSize:16.0f];
        lbllevel1.lineBreakMode = NSLineBreakByWordWrapping;
        lbllevel1.numberOfLines = 0;
        lbllevel1.text = level_name;
        lbllevel1.backgroundColor = [UIColor clearColor];
        lbllevel1.textColor = [UIColor blackColor];
        lbllevel1.textAlignment = NSTextAlignmentLeft;
        [self.view addSubview:lbllevel1];
        [table_scorepointList reloadData];
    }
    
    //[connection release];
    connection = nil;
    //[response release];
    response = nil;
} */

#pragma mark LoadingView
-(void) addLoadingView
{
    loadingView = [LoadingView loadingViewInView:self.view];
}

-(void) removeLoadingView
{
    [loadingView removeView];
}

#pragma mark displayAlert
-(void) displayAlert: (NSString *) stralert {
    NSString *str = [[NSString alloc] initWithFormat:@"%@", stralert];
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:appdelegate.iconname
                                                    message:str
                                                   delegate:self
                                          cancelButtonTitle:NSLocalizedString(@"done", nil)
                                          otherButtonTitles:nil];
    
    [alert show];
    str =nil;
    alert =nil;
}

-(BOOL) textFieldShouldReturn:(UITextField *)textField{
    [textField resignFirstResponder];
    return YES;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void) viewDidDisappear:(BOOL)animated {
    lblpoint =nil;
    lblpoint1 =nil;
    lbllevel =nil;
    lbllevel1 =nil;
    
    appdelegate.homeclicked = NO;
    [modalController dismissViewControllerAnimated:YES completion:nil];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
