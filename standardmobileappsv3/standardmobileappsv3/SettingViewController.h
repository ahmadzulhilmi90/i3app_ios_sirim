//
//  SettingViewController.h
//  standardmobileapps
//
//  Created by Maggie on 10/14/13.
//  Copyright (c) 2013 M3Online. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <FacebookSDK/FacebookSDK.h>
#import "LoadingView.h"

@interface SettingViewController : UIViewController </*NSURLConnectionDelegate, */UITextFieldDelegate, FBLoginViewDelegate,UIGestureRecognizerDelegate, UIViewControllerTransitioningDelegate> {
    LoadingView *loadingView;
    
    //NSMutableData* receivedData;
    //NSMutableData* receivedData2;
    //NSMutableData* receivedData1;
    NSString *currentRequestMenu;
    
    NSMutableArray *listOfmenu;
    NSMutableArray *listOfcategory;
    NSMutableArray *listOfappsetup;
    
    NSMutableArray *listOfContent;
    //NSMutableData* receivedData3;
    
    NSMutableArray *listOfContentDet;
    //NSMutableData* receivedData4;
    
    NSMutableArray *listOfCompProfile;
    //NSMutableData* receivedData5;
    
    NSMutableArray *listOfProductCat;
    //NSMutableData* receivedData6;
    
    NSMutableArray *listOfProductImg;
    //NSMutableData* receivedData7;
    
    NSMutableArray *listOfProductDet;
    //NSMutableData* receivedData8;
    
    //NSMutableData* receivedData9;
    
    //NSMutableData* receivedData10;
    
    //NSMutableData* receivedData11;
    
    NSMutableArray *listOfProductHome;
    //NSMutableData* receivedData12;
    
    NSString *strFilePath;
    
    IBOutlet UITableView *MenuTableView;
    NSMutableArray *listOfsetting;
    
    NSMutableArray *selectedBtnarr;
    
    IBOutlet UIButton *btnNewRegis;
    
    NSString *allowReg;
    
}

//@property (nonatomic, retain) NSMutableData *receivedData2;
//@property (nonatomic, retain) NSMutableData *receivedData1;
@property (nonatomic, retain) NSString *currentRequestMenu;

@property (nonatomic, retain) NSMutableArray *listOfmenu;
@property (nonatomic, retain) NSMutableArray *listOfcategory;
@property (nonatomic, retain) NSMutableArray *listOfappsetup;
//Menu end

//@property (nonatomic, retain) NSMutableData *receivedData3;
@property (nonatomic, retain) NSMutableArray *listOfContent;

//@property (nonatomic, retain) NSMutableData *receivedData4;
@property (nonatomic, retain) NSMutableArray *listOfContentDet;

//@property (nonatomic, retain) NSMutableData *receivedData5;
@property (nonatomic, retain) NSMutableArray *listOfCompProfile;

//@property (nonatomic, retain) NSMutableData *receivedData6;
@property (nonatomic, retain) NSMutableArray *listOfProductCat;

//@property (nonatomic, retain) NSMutableData *receivedData7;
@property (nonatomic, retain) NSMutableArray *listOfProductImg;

//@property (nonatomic, retain) NSMutableData *receivedData8;
@property (nonatomic, retain) NSMutableArray *listOfProductDet;

//@property (nonatomic, retain) NSMutableData *receivedData9;

//@property (nonatomic, retain) NSMutableData *receivedData10;

//@property (nonatomic, retain) NSMutableData *receivedData11;

//@property (nonatomic, retain) NSMutableData *receivedData12;
@property (nonatomic, retain) NSMutableArray *listOfProductHome;

@property (retain, nonatomic) IBOutlet UIView *view_myAppLoginScreen;
@property (retain, nonatomic) IBOutlet UIView *view_whatIsMyApp;
@property (retain, nonatomic) IBOutlet UIView *view_resetPassword;
@property (retain, nonatomic) IBOutlet UIView *view_resetPasswordResult;
@property (retain, nonatomic) IBOutlet UIView *view_registration;
@property (retain, nonatomic) IBOutlet UIView *view_registrationResult;
@property (retain, nonatomic) IBOutlet UIScrollView *scrollView;
@property (retain, nonatomic) IBOutlet FBLoginView *loginView;
@property (retain, nonatomic) IBOutlet UIView *view_loginView;
@property (retain, nonatomic) IBOutlet UIView *view_profileView;
@property (retain, nonatomic) IBOutlet FBProfilePictureView *userProfileImage;
@property (retain, nonatomic) IBOutlet UILabel *lbl_userName;
@property (retain, nonatomic) IBOutlet UITextField *txt_name;
@property (retain, nonatomic) IBOutlet UITextField *txt_email;
@property (retain, nonatomic) IBOutlet UITextField *txt_telNo;
@property (retain, nonatomic) IBOutlet UITextField *txt_password;
@property (retain, nonatomic) IBOutlet UITextField *txt_resetPassword;
@property (retain, nonatomic) IBOutlet UITextField *txt_myAppPassword;
@property (retain, nonatomic) IBOutlet UITextField *txt_myAppEmail;
@property (retain, nonatomic) IBOutlet UIButton *btn_logOutFBMyApp;

@property (retain, nonatomic) IBOutlet UIView *view_popUpRegisterFailed;
@property (retain, nonatomic) IBOutlet UIView *view_popUpMain;
@property (retain, nonatomic) IBOutlet UILabel *label_errorDesc;
@property (retain, nonatomic) IBOutlet UITextField *txt_emailForgotPassword;

@property (strong, nonatomic) NSString *cont_title;

@property (nonatomic, retain) IBOutlet UITableView *MenuTableView;
@property (nonatomic, retain) NSMutableArray *listOfsetting;

@property (retain, nonatomic) IBOutlet UIButton *btnchk;
@property (retain, nonatomic) IBOutlet UIButton *btntermscond;

@property (strong, nonatomic) NSMutableArray *selectedBtnarr;

@property (retain, nonatomic) IBOutlet UIButton *btn_submitRegist;

@property (strong, nonatomic) IBOutlet UIButton *btnNewRegis;

@property (strong, nonatomic) IBOutlet UILabel *lblOr;
@property (strong, nonatomic) NSString *allowReg;

@property (strong, nonatomic) IBOutlet UIView *viewhideFBLogin;

@property (strong, nonatomic) NSString *loginpluginname;

@property (strong, nonatomic) IBOutlet UILabel *lbl1stTimeRegist;

@property (strong, nonatomic) IBOutlet UILabel *lblLogin;
@property (strong, nonatomic) IBOutlet UILabel *lblNote;
@property (strong, nonatomic) IBOutlet UIButton *btnForgotPass;
@property (strong, nonatomic) IBOutlet UILabel *lbllogintext;
@property (strong, nonatomic) IBOutlet UILabel *lblloginsignedinas;
@property (strong, nonatomic) IBOutlet UIButton *btnLoginEmail;

-(void) connectToServer:(NSString *) urlPath;
-(void) addLoadingView;
-(void) removeLoadingView;
-(void) displayAlert: (NSString *) stralert;
- (IBAction)signInWithMyApp:(id)sender;
//- (IBAction)onWhatIsMyAppClicked:(id)sender;
- (IBAction)onForgotPasswordClicked:(id)sender;
- (IBAction)onForgotPasswordSubmitClicked:(id)sender;
- (IBAction)onRegisterClicked:(id)sender;
- (IBAction)onRegisterSubmitClicked:(id)sender;
- (IBAction)logOut:(id)sender;
- (IBAction)loginMyApp:(id)sender;
- (IBAction)onRegisterUsingOtherEmail:(id)sender;
- (IBAction)termscondlink:(id)sender;


@end
