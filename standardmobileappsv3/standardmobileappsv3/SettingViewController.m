//
//  SettingViewController.m
//  standardmobileapps
//
//  Created by Maggie on 10/14/13.
//  Copyright (c) 2013 M3Online. All rights reserved.
//

#import "SettingViewController.h"
#import "AppDelegate.h"
#import "SBJson.h"
#import <sqlite3.h>
#import <QuartzCore/QuartzCore.h>
#import "InboxViewController.h"
#import "RootViewController.h"
#import "ViewController.h"
#import "ChangePasswordViewController.h"
#import "EditProfileViewController.h"
#import "ScorePointViewController.h"
#import "InitialSlidingViewController.h"
#import "BTSlideInteractor.h"

@interface SettingViewController ()
@property (strong, nonatomic) BTSlideInteractor *interactor;
@end

@implementation SettingViewController

@synthesize MenuTableView, listOfsetting;
@synthesize listOfmenu,/*receivedData2, receivedData1, */currentRequestMenu, listOfcategory, listOfContent, /*receivedData3, */listOfCompProfile, listOfContentDet, /*receivedData4, receivedData5, receivedData6, receivedData7, receivedData8, receivedData9, receivedData10, receivedData11, receivedData12, */listOfappsetup, listOfProductCat, listOfProductDet, listOfProductHome, listOfProductImg, view_whatIsMyApp, view_myAppLoginScreen, view_resetPassword, view_resetPasswordResult, view_registration, view_registrationResult, scrollView, view_loginView, view_profileView, lbl_userName, userProfileImage, txt_email, txt_name, txt_password, txt_telNo, txt_resetPassword, loginView, txt_myAppEmail, txt_myAppPassword, btn_logOutFBMyApp, view_popUpMain, view_popUpRegisterFailed, label_errorDesc, txt_emailForgotPassword, cont_title, btnchk, btntermscond, selectedBtnarr, btn_submitRegist, btnNewRegis, lblOr, allowReg, viewhideFBLogin, loginpluginname, lblNote, btnForgotPass, lblLogin, lbl1stTimeRegist, lbllogintext, lblloginsignedinas, btnLoginEmail;

InitialSlidingViewController *modalController;

AppDelegate *appdelegate;

__strong NSString *termsconds;
__strong NSString *pluginValue1;

BOOL chkunchk = NO;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self checkmbflogin];
    
    if ([loginpluginname isEqualToString:@"53"]) {
        [lblLogin setText:@"Card Number"];
        [btnLoginEmail setImage:nil forState:UIControlStateNormal];
        [btnLoginEmail setTitle:NSLocalizedString(@"login", nil) forState:UIControlStateNormal];
        [btnLoginEmail setBackgroundColor:[UIColor lightGrayColor]];
        txt_myAppEmail.keyboardType = UIKeyboardTypeNumberPad;
        [lblNote setHidden:NO];
        [btnForgotPass setTitle:NSLocalizedString(@"change_password", nil) forState:UIControlStateNormal];
        [btnNewRegis setHidden:YES];
    } else {
        [lblLogin setText:@"Email / Mobile"];
        txt_myAppEmail.keyboardType = UIKeyboardTypeDefault;
        [lblNote setHidden:YES];
        [btnForgotPass setTitle:NSLocalizedString(@"forgot_pass", nil) forState:UIControlStateNormal];
        [btnNewRegis setHidden:NO];
    }
    //self.backgroundColor = [self colorWithHexString:appdelegate.themeColorCode1];
    //NSLog(@"appdelegate.backgroundT : %@", appdelegate.backgroundT);
    if (![appdelegate.backgroundT isEqualToString:@""]) {
        
        NSString *filePath = appdelegate.backgroundT;
        
        NSRange end = [filePath rangeOfString:@"/" options:NSBackwardsSearch];
        NSString *shortString;
        if (end.location != NSNotFound) {
            shortString =[filePath substringWithRange:NSMakeRange(end.location+1, filePath.length-(end.location+1))];
        } else {
            shortString = filePath;
        }
        
        BOOL success;
        
        // Create a FileManager object, we will use this to check the status
        // of the database and to copy it over if required
        NSFileManager *fileManager = [NSFileManager defaultManager];
        
        // Check if the database has already been created in the users filesystem
        success = [fileManager fileExistsAtPath:[[[NSBundle mainBundle] resourcePath] stringByAppendingPathComponent:shortString]];
        
        UIImageView *imgV = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height)];
        //[self addSubview:imgV];
        
        //__block UIActivityIndicatorView *activityIndicator;
        __weak UIImageView *weakImageView = imgV;
        NSString *imagePath;
        if(success) {
            imagePath = [[[NSBundle mainBundle] resourcePath] stringByAppendingPathComponent:shortString];
            [weakImageView setImage:[UIImage imageNamed:shortString]];
            
            //NSLog(@"shortStringimagePath : %@", imagePath);
            imgV.contentMode  = UIViewContentModeScaleAspectFit;
            [imgV setClipsToBounds:YES];
            
            UIGraphicsBeginImageContext(self.view.frame.size);
            //CGRect rect=CGRectMake(0,-64,self.view.bounds.size.width,self.view.bounds.size.height+64);
            [imgV.image drawInRect:self.view.bounds];
            //[imgV.image drawInRect:rect];
            UIImage *image1 = UIGraphicsGetImageFromCurrentImageContext();
            UIGraphicsEndImageContext();
            self.view.backgroundColor = [UIColor colorWithPatternImage:image1];
        } else {
            imagePath = filePath;
            NSURL *imageURL = [NSURL URLWithString:imagePath];
            NSData *imageData = [NSData dataWithContentsOfURL:imageURL];
            //self.backgroundColor = [UIColor redColor];
            UIGraphicsBeginImageContext(self.view.frame.size);
            //CGRect rect=CGRectMake(0,-64,self.view.bounds.size.width,self.view.bounds.size.height+64);
            //[[UIImage imageWithData:imageData] drawInRect:rect];
            [[UIImage imageWithData:imageData] drawInRect:self.view.bounds];
            UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
            UIGraphicsEndImageContext();
            self.view.backgroundColor = [UIColor colorWithPatternImage:image];
        }
    } else {
        self.view.backgroundColor = [UIColor clearColor];
    }
    
    //NSLog(@"self.view.backgroundColor %@", self.view.backgroundColor);
    
    self.navigationItem.backBarButtonItem = [[UIBarButtonItem alloc]initWithTitle:@"" style:UIBarButtonItemStylePlain target:nil action:nil];
 
    appdelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    
    //self.title = cont_title;
    self.title = NSLocalizedString(@"login", nil);
    appdelegate.homeclicked = NO;
    txt_telNo.text = appdelegate.prefixphone;
    
    CGRect rect = CGRectMake(0,0,20,20);
    UIGraphicsBeginImageContext( rect.size );
    [[UIImage imageNamed:@"home.png"] drawInRect:rect];
    UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    UIBarButtonItem *myHomeButton = [[UIBarButtonItem alloc] initWithImage:image style:UIBarButtonItemStyleDone target:self action:@selector(myHomeAction:)];
    
    
    NSArray *myButtonArray = [[NSArray alloc] initWithObjects:myHomeButton, nil];
    
    self.navigationItem.rightBarButtonItems = myButtonArray;
    
    // Do any additional setup after loading the view from its nib.
    if ([loginpluginname isEqualToString:@"53"]) {
        //loginView = nil;
    } else {
        loginView = [[FBLoginView alloc] init];
    }
    
    NSUserDefaults *defaults =[NSUserDefaults standardUserDefaults];
    NSString *hasLoggedInFB = [defaults objectForKey:@"hasLoggedInFB"];
    NSString *hasLoggedInMyApp = [defaults objectForKey:@"hasLoggedInMyApp"];

    if ([hasLoggedInFB isEqualToString:@"YES"] || [hasLoggedInMyApp isEqualToString:@"YES"])
    {
        [view_profileView setHidden:NO];
        [view_loginView setHidden:YES];
        [MenuTableView setHidden:NO];
    } else {
        if ([loginpluginname isEqualToString:@"53"]) {
            //[self signInWithMyApp:nil];
        }
    }
    
    [listOfsetting removeAllObjects];
    listOfsetting = [[NSMutableArray alloc] init];
    //[MenuItems addObject:@"Customer Details"];  //Maggie 28Feb2013
    if ([loginpluginname isEqualToString:@"53"]) {
        [listOfsetting addObject:@"Change Password"];
    } else {
        [listOfsetting addObject:@"Inbox"];
        [listOfsetting addObject:@"Scoreboard"];
        [listOfsetting addObject:@"Update Details"];
        [listOfsetting addObject:@"Change Password"];
    }
    //[listOfsetting addObject:@"Login/Logout Twitter"];
    
    if ([loginpluginname isEqualToString:@"53"]) {
        //
    } else {
        [self checkscorepoint];
    }
    [self.view addSubview:MenuTableView];
    [MenuTableView reloadData];
    
    [[btnchk layer] setBorderWidth:0.5f];
    [[btnchk layer] setBorderColor:[UIColor blackColor].CGColor];
    [btnchk addTarget:self action:@selector(ChkUnChk:) forControlEvents:UIControlEventTouchUpInside];
    
    selectedBtnarr=[[NSMutableArray alloc]init];
    //Link is from column termscons_url at app_setup table
    
    //[self ChkUnChk:nil];
    btn_submitRegist.enabled = NO;
    [btnNewRegis setHidden:YES];
    [lblOr setHidden:YES];
    if ([loginpluginname isEqualToString:@"53"]) {
        [lbl1stTimeRegist setHidden:YES];
        [viewhideFBLogin setHidden:YES];
        [loginView setHidden:YES];
    } else {
        [viewhideFBLogin setHidden:NO];
        [loginView setHidden:NO];
        [lbl1stTimeRegist setHidden:NO];
    }
    [view_resetPasswordResult setBackgroundColor:[UIColor clearColor]];
    [view_loginView setBackgroundColor:[UIColor clearColor]];
    [view_popUpRegisterFailed setBackgroundColor:[UIColor clearColor]];
    [view_profileView setBackgroundColor:[UIColor clearColor]];
    [view_registrationResult setBackgroundColor:[UIColor clearColor]];
    [view_myAppLoginScreen setBackgroundColor:[UIColor clearColor]];
    [view_resetPasswordResult setBackgroundColor:[UIColor clearColor]];
    [view_whatIsMyApp setBackgroundColor:[UIColor clearColor]];
    [viewhideFBLogin setBackgroundColor:[UIColor clearColor]];
    
    if ([loginpluginname isEqualToString:@"53"]) {
        //
    } else {
        NSString *urlAddress = [[NSString alloc] initWithFormat:@"%@allow_registration.php?db=%@&userid=%@&udid=%@&cuid=%@", appdelegate.user_apipath, [self urlEncode:appdelegate.db], [self urlEncode:appdelegate.user], [self urlEncode:appdelegate.uuid], [self urlEncode:appdelegate.customer_id]];
        currentRequestMenu = @"AllowRegistration";
        //NSLog(@"appdelegate.user_apipath:: %@", appdelegate.user_apipath);
        //NSLog(@"urlAddress:: %@", urlAddress);
        [self connectToServer:urlAddress];
    }
    
}

//Slide Menu
- (void)willAnimateRotationToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation duration:(NSTimeInterval)duration
{
    //NSLog(@"view frame=%@ bounds=%@", NSStringFromCGRect(self.view.frame), NSStringFromCGRect(self.view.bounds));
}


- (void)showModalButtonWasTouched
{
    self.interactor = [[BTSlideInteractor alloc] init];
    self.interactor.presenting = YES;
    
    //BTModalViewController *modalController = [self.storyboard instantiateViewControllerWithIdentifier:@"ModalViewController"];
    modalController = [[InitialSlidingViewController alloc] init];
    modalController.modalPresentationStyle = UIModalPresentationCustom;
    modalController.transitioningDelegate = self.interactor;
    [self.navigationController presentViewController:modalController animated:YES completion:nil];
    //[self.navigationController popToViewController:modalController animated:NO];
    //[self.navigationController pushViewController:modalController animated:YES];
}
// End Slide Menu

-(void)myHomeAction:(UIBarButtonItem *)sender{
    
    appdelegate.nextPage = @"";
    
    if ([appdelegate.listOfVerMenu count]>0) {
        if (appdelegate.homeclicked==NO) {
            appdelegate.homeclicked = YES;
            [self showModalButtonWasTouched];
        } else {
            appdelegate.homeclicked = NO;
            [modalController dismissViewControllerAnimated:YES completion:nil];
        }
    } else {
        //AppDelegate *appdelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
        //NSLog(@"View Controllers : %@",[appdelegate.navController viewControllers]);
        NSMutableArray *navigationArray = [[NSMutableArray alloc] initWithArray: appdelegate.navController.viewControllers];
        
        for (UIViewController* viewController in appdelegate.navController.viewControllers) {
            
            //This if condition checks whether the viewController's class is MyGroupViewController
            // if true that means its the MyGroupViewController (which has been pushed at some point)
            if ([viewController isKindOfClass:[RootViewController class]] ) {
                
                // Here viewController is a reference of UIViewController base class of MyGroupViewController
                // but viewController holds MyGroupViewController  object so we can type cast it here
                //ThemeViewController *themeViewController = (ThemeViewController*)viewController;
                //[self.navigationController popToViewController:themeViewController animated:YES];
                [navigationArray removeObjectAtIndex: 0];
            }
        }
        appdelegate.navController.viewControllers = navigationArray;
        for (UIViewController* viewController in appdelegate.navController.viewControllers) {
            
            //This if condition checks whether the viewController's class is MyGroupViewController
            // if true that means its the MyGroupViewController (which has been pushed at some point)
            if ([viewController isKindOfClass:[ViewController class]] ) {
                
                // Here viewController is a reference of UIViewController base class of MyGroupViewController
                // but viewController holds MyGroupViewController  object so we can type cast it here
                ViewController *themeViewController = (ViewController*)viewController;
                [appdelegate.navController popToViewController:themeViewController animated:NO];
                //[self.navigationController popToViewController:viewController animated:YES];
            }
        }
        //NSLog(@"View Controllers111 : %@",appdelegate.navController.viewControllers);
    }
    
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    [self checkmbflogin];
    if (self.shouldSkipLogIn) {
        [self performSelector:@selector(transitionToMainViewController) withObject:nil afterDelay:.5];
    }
}

- (void) checkmbflogin {
    sqlite3 *database;
    
    if(sqlite3_open([appdelegate.databasePath UTF8String], &database) == SQLITE_OK) {
        NSString *theme_object = [[NSString alloc] initWithFormat:@"SELECT plugin_id FROM theme_object_plugin WHERE plugin_type='customer' AND user_id='%@' AND plugin_id='53' ", appdelegate.user];
        //NSLog(@"theme_object_plugin:: %@", theme_object);
        loginpluginname = @"";
        //int rows=0;
        const char *sqlStatement = [theme_object UTF8String];
        sqlite3_stmt *compiledStatement;
        if(sqlite3_prepare_v2(database, sqlStatement, -1, &compiledStatement, NULL) == SQLITE_OK) {
            // Loop through the results and add them to the feeds array
            
            /*if (sqlite3_step(compiledStatement) == SQLITE_ERROR) {
             NSAssert1(0,@"Error when counting rows  %s",sqlite3_errmsg(database));
             } else {
             rows = sqlite3_column_int(compiledStatement, 0);
             //NSLog(@"SQLite Rows: %i", rows);
             } */
            
            //while(sqlite3_step(compiledStatement) == SQLITE_ROW) {
            
            //NSLog(@"testtest111 ::");
            while(sqlite3_step(compiledStatement) == SQLITE_ROW) {
                loginpluginname = [NSString stringWithUTF8String:(char *)sqlite3_column_text(compiledStatement, 0)];
                //NSLog(@"loginpluginname :: %@", loginpluginname);
            }
            
        }
        // Release the compiled statement from memory
        sqlite3_finalize(compiledStatement);
        
    }
    sqlite3_close(database);
    
    //[MenuTableView reloadData];
}

- (void) checkscorepoint {
    sqlite3 *database;
    
    if(sqlite3_open([appdelegate.databasePath UTF8String], &database) == SQLITE_OK) {
        NSString *theme_object = [[NSString alloc] initWithFormat:@"SELECT * from theme_object_plugin WHERE plugin_type='points' and plugin_id=24"];
        //NSLog(@"theme_object_plugin:: %@", theme_object);
        
        //int rows=0;
        const char *sqlStatement = [theme_object UTF8String];
        sqlite3_stmt *compiledStatement;
        if(sqlite3_prepare_v2(database, sqlStatement, -1, &compiledStatement, NULL) == SQLITE_OK) {
            // Loop through the results and add them to the feeds array
            
            /*if (sqlite3_step(compiledStatement) == SQLITE_ERROR) {
                NSAssert1(0,@"Error when counting rows  %s",sqlite3_errmsg(database));
            } else {
                rows = sqlite3_column_int(compiledStatement, 0);
                //NSLog(@"SQLite Rows: %i", rows);
            } */
            
            //while(sqlite3_step(compiledStatement) == SQLITE_ROW) {
            
            int count = 0;
            //NSLog(@"testtest111 ::");
            while(sqlite3_step(compiledStatement) == SQLITE_ROW) {
                pluginValue1 = [NSString stringWithUTF8String:(char *)sqlite3_column_text(compiledStatement, 4)];
                count = 1;
                //NSLog(@"testtest :: %@", pluginValue1);
            }
            
            if (count == 0) {
               [listOfsetting removeObjectAtIndex:1];
            }
            
        }
        // Release the compiled statement from memory
        sqlite3_finalize(compiledStatement);
        
    }
    sqlite3_close(database);
    
    [MenuTableView reloadData];
}

- (void)setShouldSkipLogIn:(BOOL)skip {
    [[NSUserDefaults standardUserDefaults] setBool:skip forKey:@"StandardAppsSkipLogIn"];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

- (BOOL)shouldSkipLogIn {
    return [[NSUserDefaults standardUserDefaults] boolForKey:@"StandardAppsSkipLogIn"];
}

- (void)viewDidUnload {
    [self setLoginView:nil];
    [super viewDidUnload];
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
	//NSLog(@">>> Entering *** %s <<<", __PRETTY_FUNCTION__);
    
    if (tableView == MenuTableView) {
        return [listOfsetting count];
    }
    
    return 0;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    
	return 44;
    
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
	
    if (tableView == MenuTableView) {
        static NSString *CellIdentifier = @"CellIdentifier";
        UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
        
        if (cell == nil) {
            cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
            //cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
            
            if ([[listOfsetting objectAtIndex:[indexPath row]] isEqualToString:@"Scoreboard"]) {
                cell.textLabel.text = pluginValue1;
            } else {
                if ([[listOfsetting objectAtIndex:[indexPath row]] isEqualToString:@"Inbox"]) {
                    cell.textLabel.text = NSLocalizedString(@"inbox", nil);
                } else if ([[listOfsetting objectAtIndex:[indexPath row]] isEqualToString:@"Update Details"]) {
                    cell.textLabel.text = NSLocalizedString(@"updatedetails", nil);
                } else if ([[listOfsetting objectAtIndex:[indexPath row]] isEqualToString:@"Change Password"]) {
                    cell.textLabel.text = NSLocalizedString(@"change_password", nil);
                }
            }
            
        }
        
        return cell;
        
        /*cell = [[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:nil] autorelease];
         cell.textLabel.font = [UIFont boldSystemFontOfSize:14.0];
         cell.textLabel.numberOfLines = 0;
         cell.textLabel.textColor = [UIColor blackColor];
         cell.selectionStyle = UITableViewCellSelectionStyleNone;
         cell.textLabel.text = [listOfsetting objectAtIndex:[indexPath row]]; */
        
    }
    
    return nil;
    
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
	//NSLog(@">>> Entering *** %s <<<", __PRETTY_FUNCTION__);
    if (tableView == MenuTableView) {
        if ([[listOfsetting objectAtIndex:indexPath.row] isEqualToString:@"Inbox"]) {
            InboxViewController *inboxViewController = [[InboxViewController alloc] init];
            inboxViewController.conttitle = cont_title;
            inboxViewController.topId = @"0";
            [appdelegate.navController pushViewController:inboxViewController animated:NO];
        } else if ([[listOfsetting objectAtIndex:indexPath.row] isEqualToString:@"Change Password"]) {
            ChangePasswordViewController *changePasswordViewController = [[ChangePasswordViewController alloc] init];
            changePasswordViewController.conttitle = cont_title;
            [appdelegate.navController pushViewController:changePasswordViewController animated:NO];
        } else if ([[listOfsetting objectAtIndex:indexPath.row] isEqualToString:@"Update Details"]) {
            EditProfileViewController *editProfileViewController = [[EditProfileViewController alloc] init];
            editProfileViewController.conttitle = cont_title;
            [appdelegate.navController pushViewController:editProfileViewController animated:NO];
        } else if ([[listOfsetting objectAtIndex:indexPath.row] isEqualToString:@"Scoreboard"]) {
            NSString *nibFileName;
            if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone) {
                nibFileName = @"ScorePointViewController";
            } else {
                nibFileName = @"ScorePointViewController_ipad";
            }
            ScorePointViewController *scorePointViewController = [[ScorePointViewController alloc] initWithNibName:nibFileName bundle:nil];
            scorePointViewController.cont_title = cont_title;
            [appdelegate.navController pushViewController:scorePointViewController animated:NO];
        }
    } 
}

/*- (void)goback
{
    [self.navigationController popViewControllerAnimated:YES];
} */

-(BOOL) stringIsValidEmail:(NSString *)checkString
{
    BOOL stricterFilter = YES; // Discussion http://blog.logichigh.com/2010/09/02/validating-an-e-mail-address/
    NSString *stricterFilterString = @"[A-Z0-9a-z\\._%+-]+@([A-Za-z0-9-]+\\.)+[A-Za-z]{2,4}";
    NSString *laxString = @".+@([A-Za-z0-9]+\\.)+[A-Za-z]{2}[A-Za-z]*";
    NSString *emailRegex = stricterFilter ? stricterFilterString : laxString;
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
    return [emailTest evaluateWithObject:checkString];
}

-(NSString *) urlEncode:(NSString *) theStr {
    /*NSString *encoded = (NSString *)CFURLCreateStringByAddingPercentEscapes(
																			NULL,
																			(CFStringRef)theStr,
																			NULL,
																			(CFStringRef)@"!*'\"();:@&=+$,/?%#[]% ",
																			//																		NSUnicodeStringEncoding );
																			kCFStringEncodingUTF8 ); */
    NSString *encoded = (NSString *)CFBridgingRelease(CFURLCreateStringByAddingPercentEscapes(
                                                                                                    NULL,
                                                                                                    (__bridge CFStringRef) theStr,
                                                                                                    NULL,
                                                                                                    CFSTR("!*'();:@&=+$,/?%#[]\" "),
                                                                                                    kCFStringEncodingUTF8));
    return encoded;
    //return nil;
}

#pragma mark CONNECT TO SERVER
-(void) connectToServer:(NSString *) urlPath {
    
    if (![currentRequestMenu isEqualToString:@"registerUserFromFB"])
        [self addLoadingView];
    
    NSFileManager *fileManager = [NSFileManager defaultManager];
    NSError * error;
    NSArray * cacheFiles = [fileManager contentsOfDirectoryAtPath:NSTemporaryDirectory() error:&error];
    
    for(NSString * file in cacheFiles)
    {
        error=nil;
        NSString * filePath = [NSTemporaryDirectory() stringByAppendingPathComponent:file ];
        //NSLog(@"filePath to remove = %@",filePath);
        
        [fileManager removeItemAtPath:filePath error:&error];
    }
    
	NSString *urlAddress =[NSString stringWithFormat:@"%@", urlPath];
	
	//NSLog(@"URL: %@", urlAddress);
	//Create a URL object.
	/*NSURL *url = [NSURL URLWithString:urlAddress];
	
	//URL Requst Object
	//NSURLRequest *requestObj = [NSURLRequest requestWithURL:url];
	NSURLRequest *requestObj = [NSURLRequest requestWithURL:url
												cachePolicy:NSURLRequestReloadIgnoringLocalAndRemoteCacheData
											timeoutInterval:30];
	
	NSURLConnection *theConnection=[[NSURLConnection alloc] initWithRequest:requestObj delegate:self];
	if (theConnection)
    {
		// Create the NSMutableData to hold the received data.
		if ([currentRequestMenu isEqualToString:@"registerUser"] || [currentRequestMenu isEqualToString:@"registerUserFromFB"] || [currentRequestMenu isEqualToString:@"loginMyApp"] || [currentRequestMenu isEqualToString:@"forgotPassword"] || [currentRequestMenu isEqualToString:@"AllowRegistration"])
            receivedData = [NSMutableData data];
	}
    else
    {
		// Inform the user that the connection failed.
		//[self displayAlert:@"Connection Error. Please try again later."];
		[self displayAlert:NSLocalizedString(@"MSG_CONNECTION_ERROR", nil)];
		[self removeLoadingView];
	} */
    [self getJsonResponse:urlAddress success:^(NSDictionary *responseDict) {
        
        //NSLog(@"%@",responseDict);
        [self successfunction:responseDict];
        
    } failure:^(NSError *error) {
        // release the connection, and the data object
        
        if (![currentRequestMenu isEqualToString:@"registerUserFromFB"])
            [self removeLoadingView];
        [self displayAlert:NSLocalizedString(@"MSG_CONNECTION_ERROR", nil)];
    }];
}

-(void)getJsonResponse : (NSString *)urlStr success : (void (^)(NSDictionary *responseDict))success failure:(void(^)(NSError* error))failure
{
    
    
    NSURLSession * session = [NSURLSession sharedSession];
    NSURL * url = [NSURL URLWithString: urlStr];
    
    
    // Asynchronously Api is hit here
    NSURLSessionDataTask * dataTask = [session dataTaskWithURL:url completionHandler:^(NSData *data, NSURLResponse *response, NSError *error)
                                       {
                                           
                                           //NSLog(@"%@",data);
                                           NSDictionary * json;
                                           if ([data length]>0) {
                                               json  = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
                                               //NSLog(@"%@",json);
                                               success(json);
                                           } else {
                                               json = nil;
                                               dispatch_async(dispatch_get_main_queue(), ^ {
                                                   if (![currentRequestMenu isEqualToString:@"registerUserFromFB"])
                                                       [self removeLoadingView];
                                                   [self displayAlert:NSLocalizedString(@"MSG_CONNECTION_ERROR", nil)];
                                               });
                                           }
                                           
                                           
                                       }];
    
    [dataTask resume] ; // Executed First
    
    
}

-(void) successfunction:(NSDictionary *)responseDict {
    dispatch_async(dispatch_get_main_queue(), ^{
        //NSString *jsonString = [responseDict JSONRepresentation];
        if ([currentRequestMenu isEqualToString:@"registerUser"] || [currentRequestMenu isEqualToString:@"registerUserFromFB"])
        {
            if (![currentRequestMenu isEqualToString:@"registerUserFromFB"])
                [self removeLoadingView];
            
            NSString *status,*desc;
            
            NSDictionary *dictionary = responseDict;
            NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
            for(NSDictionary *row in dictionary)
            {
                status = [row objectForKey:@"status"];
                desc = [row objectForKey:@"desc"];
                appdelegate.customer_id = [row objectForKey:@"customer_id"];
                [appdelegate checksum];
                
                [defaults setObject:[row objectForKey:@"customer_id"] forKey:@"customer_id"];
                [defaults setObject:[row objectForKey:@"customer_phone"] forKey:@"customer_phone"];
                [[NSUserDefaults standardUserDefaults] synchronize];
            }
            
            if ([status isEqualToString:@"1"])
            {
                if (appdelegate.shouldReturnToPrevPage)
                {
                    if ([currentRequestMenu isEqualToString:@"registerUser"])
                    {
                        [defaults setObject:@"YES" forKey:@"hasLoggedInMyApp"];
                        [defaults setObject:[txt_name text] forKey:@"customer_name"];
                        [[NSUserDefaults standardUserDefaults] synchronize];
                        
                        self.shouldSkipLogIn = NO;
                        
                        [view_profileView setHidden:NO];
                        [view_loginView setHidden:YES];
                        [MenuTableView setHidden:NO];
                        
                        [btn_logOutFBMyApp setTitle:NSLocalizedString(@"logout", nil) forState:UIControlStateNormal];
                        [btn_logOutFBMyApp setBackgroundColor:[UIColor redColor]];
                        
                        userProfileImage.profileID = nil;
                        lbl_userName.text = [defaults objectForKey:@"customer_name"];
                        appdelegate.hasLoggedIn = YES;
                        
                        [self displayAlert:[NSString stringWithFormat:@"%@ & %@", NSLocalizedString(@"register_success", nil), NSLocalizedString(@"login_success", nil)]];
                    }
                    else if ([currentRequestMenu isEqualToString:@"registerUserFromFB"])
                        [self displayAlert:NSLocalizedString(@"login_success", nil)];
                    
                    //[appdelegate.navController popToViewController:appdelegate.originVC animated:YES];
                    //[appdelegate.navController popViewControllerAnimated:NO];
                }
                else
                {
                    [view_registrationResult setHidden:NO];
                    [scrollView setHidden:YES];
                    
                    //Clear all TextField
                    [txt_email setText:@""];
                    [txt_name setText:@""];
                    [txt_password setText:@""];
                    [txt_telNo setText:@""];
                }
            }
            else
            {
                //Display alert only when user register using MyApp and don't show when registering via FB
                if ([currentRequestMenu isEqualToString:@"registerUser"])
                {
                    //[self displayAlert:desc];
                    [label_errorDesc setText:desc];
                    view_popUpRegisterFailed.layer.cornerRadius = 10;
                    [view_registration addSubview:view_popUpMain];
                }
                
                if (appdelegate.shouldReturnToPrevPage && [currentRequestMenu isEqualToString:@"registerUserFromFB"])
                {
                    [self displayAlert:NSLocalizedString(@"login_success", nil)];
                    //NSLog(@"%@",appdelegate.navController);
                    //[appdelegate.navController popToViewController:appdelegate.originVC animated:YES];
                    //[appdelegate.navController popViewControllerAnimated:NO];
                }
            }
            
        }
        else if ([currentRequestMenu isEqualToString:@"loginMyApp"])
        {
            [self removeLoadingView];
            NSString *status,*desc;
            
            NSDictionary *dictionary = responseDict;
            NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
            for(NSDictionary *row in dictionary)
            {
                status = [row objectForKey:@"status"];
                desc = [row objectForKey:@"desc"];
                
                appdelegate.customer_id = [row objectForKey:@"customer_id"];
                
                [appdelegate checksum];
                
                [defaults setObject:[row objectForKey:@"login_id"] forKey:@"login_id"];
                [defaults setObject:[row objectForKey:@"customer_id"] forKey:@"customer_id"];
                [defaults setObject:[row objectForKey:@"name"] forKey:@"customer_name"];
                [defaults setObject:[row objectForKey:@"phone"] forKey:@"customer_phone"];
            }
            
            if ([status isEqualToString:@"1"])
            {
                self.shouldSkipLogIn = NO;
                
                [view_profileView setHidden:NO];
                [view_loginView setHidden:YES];
                [MenuTableView setHidden:NO];
                
                [btn_logOutFBMyApp setTitle:NSLocalizedString(@"logout", nil) forState:UIControlStateNormal];
                [btn_logOutFBMyApp setBackgroundColor:[UIColor redColor]];
                
                userProfileImage.profileID = nil;
                lbl_userName.text = [defaults objectForKey:@"customer_name"];
                appdelegate.hasLoggedIn = YES;
                
                [defaults setObject:@"YES" forKey:@"hasLoggedInMyApp"];
                
                if (appdelegate.shouldReturnToPrevPage)
                {
                    if ([loginpluginname isEqualToString:@"53"]) {
                        [self displayAlert:desc];
                    } else {
                        [self displayAlert:NSLocalizedString(@"login_success", nil)];
                    }
                    
                    //[appdelegate.navController popToViewController:appdelegate.originVC animated:YES];
                    //[appdelegate.navController popViewControllerAnimated:NO];
                }
                else
                    [appdelegate.navController popViewControllerAnimated:NO];
            }
            else
                [self displayAlert:desc];
            
            [[NSUserDefaults standardUserDefaults] synchronize];
            
        }
        else if ([currentRequestMenu isEqualToString:@"forgotPassword"])
        {
            [self removeLoadingView];
            NSString *status,*desc;
            
            NSDictionary *dictionary = responseDict;
            
            for(NSDictionary *row in dictionary)
            {
                status = [row objectForKey:@"status"];
                desc = [row objectForKey:@"desc"];
            }
            
            if ([status isEqualToString:@"1"])
            {
                [view_resetPasswordResult setHidden:NO];
                [txt_emailForgotPassword setText:@""];
            }
            else
                [self displayAlert:desc];
            
        }
        else if ([currentRequestMenu isEqualToString:@"AllowRegistration"])
        {
            [self removeLoadingView];
            
            NSDictionary *dictionary = responseDict;
            for(NSDictionary *row in dictionary)
            {
                allowReg = [row objectForKey:@"allow_registration"];
            }
            //NSLog(@"allowReg:%@", allowReg);
            if ([loginpluginname isEqualToString:@"53"]) {
                [btnNewRegis setHidden:YES];
                //[lblOr setHidden:NO];
                [viewhideFBLogin setHidden:YES];
                [loginView setHidden:YES];
            } else {
                //NSLog(@"allowReg%@", allowReg);
                if ([allowReg isEqualToString:@"1"])
                {
                    [btnNewRegis setHidden:NO];
                    //[lblOr setHidden:NO];
                    [viewhideFBLogin setHidden:NO];
                }
                else {
                    [btnNewRegis setHidden:YES];
                    //[lblOr setHidden:NO];
                    [viewhideFBLogin setHidden:YES];
                    [self signInWithMyApp:nil];
                }
                [loginView setHidden:NO];
            }
            
        }
        else
        {
            [self removeLoadingView];
        }
    });
    
}

/*- (void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response
{
    if ([currentRequestMenu isEqualToString:@"registerUser"] || [currentRequestMenu isEqualToString:@"registerUserFromFB"] || [currentRequestMenu isEqualToString:@"loginMyApp"] || [currentRequestMenu isEqualToString:@"forgotPassword"] || [currentRequestMenu isEqualToString:@"AllowRegistration"])
        [receivedData setLength:0];
}

- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data
{
    if ([currentRequestMenu isEqualToString:@"registerUser"] || [currentRequestMenu isEqualToString:@"registerUserFromFB"] || [currentRequestMenu isEqualToString:@"loginMyApp"] || [currentRequestMenu isEqualToString:@"forgotPassword"] || [currentRequestMenu isEqualToString:@"AllowRegistration"])
        [receivedData appendData:data];
}

- (void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error {
    // release the connection, and the data object
	
    if ([currentRequestMenu isEqualToString:@"registerUser"] || [currentRequestMenu isEqualToString:@"registerUserFromFB"] || [currentRequestMenu isEqualToString:@"loginMyApp"] || [currentRequestMenu isEqualToString:@"forgotPassword"] || [currentRequestMenu isEqualToString:@"AllowRegistration"]) {
        connection =nil;
        receivedData2 =nil;
    }

    //NSLog(@"%@",error.description);
    
	[self removeLoadingView];
	[self displayAlert:NSLocalizedString(@"MSG_CONNECTION_ERROR", nil)];
}

- (void)connectionDidFinishLoading:(NSURLConnection *)connection
{
    if ([currentRequestMenu isEqualToString:@"registerUser"] || [currentRequestMenu isEqualToString:@"registerUserFromFB"])
    {
        if (![currentRequestMenu isEqualToString:@"registerUserFromFB"])
            [self removeLoadingView];
        
        NSString *response = [[NSString alloc] initWithData:receivedData encoding:NSUTF8StringEncoding];
        NSString *status,*desc;
        if([receivedData length]>10)
        {
            NSDictionary *dictionary = [response JSONValue];
            NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
            for(NSDictionary *row in dictionary)
            {
                status = [row objectForKey:@"status"];
                desc = [row objectForKey:@"desc"];
                appdelegate.customer_id = [row objectForKey:@"customer_id"];
                [appdelegate checksum];
                
                [defaults setObject:[row objectForKey:@"customer_id"] forKey:@"customer_id"];
                [defaults setObject:[row objectForKey:@"customer_phone"] forKey:@"customer_phone"];
                [[NSUserDefaults standardUserDefaults] synchronize];
            }
            
            if ([status isEqualToString:@"1"])
            {
                if (appdelegate.shouldReturnToPrevPage)
                {
                    if ([currentRequestMenu isEqualToString:@"registerUser"])
                    {
                        [defaults setObject:@"YES" forKey:@"hasLoggedInMyApp"];
                        [defaults setObject:[txt_name text] forKey:@"customer_name"];
                        [[NSUserDefaults standardUserDefaults] synchronize];
                        
                        self.shouldSkipLogIn = NO;
                        
                        [view_profileView setHidden:NO];
                        [view_loginView setHidden:YES];
                        [MenuTableView setHidden:NO];
                        
                        [btn_logOutFBMyApp setTitle:NSLocalizedString(@"logout", nil) forState:UIControlStateNormal];
                        [btn_logOutFBMyApp setBackgroundColor:[UIColor redColor]];
                        
                        userProfileImage.profileID = nil;
                        lbl_userName.text = [defaults objectForKey:@"customer_name"];
                        appdelegate.hasLoggedIn = YES;
                        
                        [self displayAlert:[NSString stringWithFormat:@"%@ & %@", NSLocalizedString(@"register_success", nil), NSLocalizedString(@"login_success", nil)]];
                    }
                    else if ([currentRequestMenu isEqualToString:@"registerUserFromFB"])
                        [self displayAlert:NSLocalizedString(@"login_success", nil)];
                    
                    //[appdelegate.navController popToViewController:appdelegate.originVC animated:YES];
                    //[appdelegate.navController popViewControllerAnimated:NO];
                }
                else
                {
                    [view_registrationResult setHidden:NO];
                    [scrollView setHidden:YES];
                    
                    //Clear all TextField
                    [txt_email setText:@""];
                    [txt_name setText:@""];
                    [txt_password setText:@""];
                    [txt_telNo setText:@""];
                }
            }
            else
            {
                //Display alert only when user register using MyApp and don't show when registering via FB
                if ([currentRequestMenu isEqualToString:@"registerUser"])
                {
                    //[self displayAlert:desc];
                    [label_errorDesc setText:desc];
                    view_popUpRegisterFailed.layer.cornerRadius = 10;
                    [view_registration addSubview:view_popUpMain];
                }
                
                if (appdelegate.shouldReturnToPrevPage && [currentRequestMenu isEqualToString:@"registerUserFromFB"])
                {
                    [self displayAlert:NSLocalizedString(@"login_success", nil)];
                    //NSLog(@"%@",appdelegate.navController);
                    //[appdelegate.navController popToViewController:appdelegate.originVC animated:YES];
                    //[appdelegate.navController popViewControllerAnimated:NO];
                }
            }
        }
        response =nil;
    }
    else if ([currentRequestMenu isEqualToString:@"loginMyApp"])
    {
        [self removeLoadingView];
        NSString *response = [[NSString alloc] initWithData:receivedData encoding:NSUTF8StringEncoding];
        NSString *status,*desc;
        if([receivedData length]>10)
        {
            NSDictionary *dictionary = [response JSONValue];
            NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
            
            for(NSDictionary *row in dictionary)
            {
                status = [row objectForKey:@"status"];
                desc = [row objectForKey:@"desc"];
                appdelegate.customer_id = [row objectForKey:@"customer_id"];
                [appdelegate checksum];
                
                [defaults setObject:[row objectForKey:@"customer_id"] forKey:@"customer_id"];
                [defaults setObject:[row objectForKey:@"name"] forKey:@"customer_name"];
                [defaults setObject:[row objectForKey:@"phone"] forKey:@"customer_phone"];
            }
            
            if ([status isEqualToString:@"1"])
            {
                self.shouldSkipLogIn = NO;

                [view_profileView setHidden:NO];
                [view_loginView setHidden:YES];
                [MenuTableView setHidden:NO];
                
                [btn_logOutFBMyApp setTitle:NSLocalizedString(@"logout", nil) forState:UIControlStateNormal];
                [btn_logOutFBMyApp setBackgroundColor:[UIColor redColor]];
                
                userProfileImage.profileID = nil;
                lbl_userName.text = [defaults objectForKey:@"customer_name"];
                appdelegate.hasLoggedIn = YES;
                
                [defaults setObject:@"YES" forKey:@"hasLoggedInMyApp"];
                
                if (appdelegate.shouldReturnToPrevPage)
                {
                    [self displayAlert:NSLocalizedString(@"login_success", nil)];
                    
                    //[appdelegate.navController popToViewController:appdelegate.originVC animated:YES];
                    //[appdelegate.navController popViewControllerAnimated:NO];
                }
                else
                    [appdelegate.navController popViewControllerAnimated:NO];
            }
            else
                [self displayAlert:desc];
            
            [[NSUserDefaults standardUserDefaults] synchronize];
        }
        response =nil;
        
    }
    else if ([currentRequestMenu isEqualToString:@"forgotPassword"])
    {
        [self removeLoadingView];
        NSString *response = [[NSString alloc] initWithData:receivedData encoding:NSUTF8StringEncoding];
        NSString *status,*desc;
        if([receivedData length]>10)
        {
            NSDictionary *dictionary = [response JSONValue];
            
            for(NSDictionary *row in dictionary)
            {
                status = [row objectForKey:@"status"];
                desc = [row objectForKey:@"desc"];
            }
            
            if ([status isEqualToString:@"1"])
            {
                [view_resetPasswordResult setHidden:NO];
                [txt_emailForgotPassword setText:@""];
            }
            else
                [self displayAlert:desc];
        }
        response =nil;
    }
    else if ([currentRequestMenu isEqualToString:@"AllowRegistration"])
    {
        [self removeLoadingView];
        NSString *response = [[NSString alloc] initWithData:receivedData encoding:NSUTF8StringEncoding];
        //NSLog(@"response %@", response);
        if([receivedData length]>10)
        {
            NSDictionary *dictionary = [response JSONValue];
            for(NSDictionary *row in dictionary)
            {
                allowReg = [row objectForKey:@"allow_registration"];
            }
            //NSLog(@"allowReg:%@", allowReg);
            if ([allowReg isEqualToString:@"1"])
            {
                [btnNewRegis setHidden:NO];
                [lblOr setHidden:NO];
                [viewhideFBLogin setHidden:YES];
            }
            else {
                [btnNewRegis setHidden:YES];
                [lblOr setHidden:YES];
                [viewhideFBLogin setHidden:NO];
            }
        }
        response =nil;
    }
    else
    {
        [self removeLoadingView];
    }
    
} */

-(void) addLoadingView {
	loadingView = [LoadingView loadingViewInView:appdelegate.window];
}

-(void) removeLoadingView
{
	[loadingView removeView];
}

#pragma mark End LoadingView

-(void) displayAlert: (NSString *) stralert {
	NSString *str = [[NSString alloc] initWithFormat:@"%@", stralert];
	UIAlertView *alert = [[UIAlertView alloc] initWithTitle:appdelegate.iconname
													message:str
												   delegate:self
										  cancelButtonTitle:NSLocalizedString(@"done", nil)
										  otherButtonTitles:nil];
	
	[alert show];
	str =nil;
	alert =nil;
}

- (void)alertView:(UIAlertView *)alertView willDismissWithButtonIndex:(NSInteger)buttonIndex
{
    if (buttonIndex == 0)
    {
        if (appdelegate.shouldReturnToPrevPage) {
            [appdelegate.navController popToViewController:appdelegate.originVC animated:NO];
        }
    }
}



- (IBAction)signInWithMyApp:(id)sender
{
    UIViewController *vc = [[UIViewController alloc] init];
    [vc setTitle:NSLocalizedString(@"login", nil)];
    [vc setView:view_myAppLoginScreen];
    [vc.view setBackgroundColor:self.view.backgroundColor];
    vc.navigationItem.backBarButtonItem = [[UIBarButtonItem alloc]initWithTitle:@"" style:UIBarButtonItemStylePlain target:nil action:nil];
    appdelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    
    //self.title = appdelegate.iconname;
    self.title = NSLocalizedString(@"login", nil);
    
    CGRect rect = CGRectMake(0,0,20,20);
    UIGraphicsBeginImageContext( rect.size );
    [[UIImage imageNamed:@"home.png"] drawInRect:rect];
    UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    UIBarButtonItem *myHomeButton = [[UIBarButtonItem alloc] initWithImage:image style:UIBarButtonItemStyleDone target:self action:@selector(myHomeAction:)];
    
    
    NSArray *myButtonArray = [[NSArray alloc] initWithObjects:myHomeButton, nil];
    
    vc.navigationItem.rightBarButtonItems = myButtonArray;
    
    [[self navigationController]pushViewController:vc animated:NO];
    //[[self navigationController] presentViewController:vc animated:NO completion:nil];
    vc =nil;
    
    /*if ([loginpluginname isEqualToString:@"53"]) {
        //NSLog(@"%@",[self.navigationController.viewControllers objectAtIndex:self.navigationController.viewControllers.count-2]);
        NSMutableArray *allViewControllers = [NSMutableArray arrayWithArray: self.navigationController.viewControllers];
        [allViewControllers removeObjectIdenticalTo: [self.navigationController.viewControllers objectAtIndex:self.navigationController.viewControllers.count-2]];
        self.navigationController.viewControllers = allViewControllers;
    } */
}

- (IBAction)loginMyApp:(id)sender
{
    if ([[txt_myAppEmail text] length] == 0 || [[txt_myAppPassword text] length] == 0) {
        if ([loginpluginname isEqualToString:@"53"]) {
            [self displayAlert:[NSString stringWithFormat:@"%@ & %@", NSLocalizedString(@"err_cardnumber", nil), NSLocalizedString(@"err_password", nil)]];
        } else {
            [self displayAlert:[NSString stringWithFormat:@"%@ & %@", NSLocalizedString(@"err_email", nil), NSLocalizedString(@"err_password", nil)]];
        }
    } else
    {
        NSString *urlAddress;
        if ([loginpluginname isEqualToString:@"53"]) {
            BOOL isDigit = FALSE;
            NSCharacterSet* notDigits = [[NSCharacterSet decimalDigitCharacterSet] invertedSet];
            if ([[txt_myAppEmail text] rangeOfCharacterFromSet:notDigits].location == NSNotFound)
            {
                // newString consists only of the digits 0 through 9
                isDigit = TRUE;
            }
            if ([[txt_myAppEmail text] length] != 15 || isDigit  == FALSE) {
                [self displayAlert:[NSString stringWithFormat:@"%@", NSLocalizedString(@"login_error_not_15_digit", nil)]];
            } else {
                urlAddress = [[NSString alloc] initWithFormat:@"%@mbf_cust_login.php?db=%@&userid=%@&udid=%@&cuid=%@&device=ios&sc=%@&login_id=%@&password=%@", appdelegate.user_apipath, [self urlEncode:appdelegate.db], [self urlEncode:appdelegate.user], [self urlEncode:appdelegate.uuid], [self urlEncode:appdelegate.customer_id], appdelegate.cs, [self urlEncode:[txt_myAppEmail text]], [self urlEncode:[txt_myAppPassword text]]];
                currentRequestMenu = @"loginMyApp";
                //NSLog(@"appdelegate.user_apipath:: %@", appdelegate.user_apipath);
                //NSLog(@"urlAddress:: %@", urlAddress);
                [self connectToServer:urlAddress];
                
                [txt_myAppPassword resignFirstResponder];
                [txt_myAppEmail resignFirstResponder];
            }
        } else {
            urlAddress = [[NSString alloc] initWithFormat:@"%@cust_login.php?db=%@&userid=%@&udid=%@&cuid=%@&email=%@&password=%@&device=ios&sc=%@", appdelegate.user_apipath, [self urlEncode:appdelegate.db], [self urlEncode:appdelegate.user], [self urlEncode:appdelegate.uuid], [self urlEncode:appdelegate.customer_id], [self urlEncode:[txt_myAppEmail text]], [self urlEncode:[txt_myAppPassword text]], appdelegate.cs];
            currentRequestMenu = @"loginMyApp";
            //NSLog(@"appdelegate.user_apipath:: %@", appdelegate.user_apipath);
            //NSLog(@"urlAddress:: %@", urlAddress);
            [self connectToServer:urlAddress];
            
            [txt_myAppPassword resignFirstResponder];
            [txt_myAppEmail resignFirstResponder];
        }
    }
}

- (IBAction)onForgotPasswordClicked:(id)sender
{
    if ([loginpluginname isEqualToString:@"53"]) {
        ChangePasswordViewController *changePasswordViewController = [[ChangePasswordViewController alloc] init];
        changePasswordViewController.conttitle = cont_title;
        [appdelegate.navController pushViewController:changePasswordViewController animated:NO];
    } else {
        [view_popUpMain removeFromSuperview];
        
        UIViewController *vc = [[UIViewController alloc] init];
        [vc setTitle:NSLocalizedString(@"forgot_pass", nil)];
        [vc setView:view_resetPassword];
        [vc.view setBackgroundColor:self.view.backgroundColor];
        
        [view_resetPasswordResult.layer setHidden:YES];
        [txt_resetPassword setText:@""];
        
        vc.navigationItem.backBarButtonItem = [[UIBarButtonItem alloc]initWithTitle:@"" style:UIBarButtonItemStylePlain target:nil action:nil];
        
        appdelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
        
        self.title = appdelegate.iconname;
        
        CGRect rect = CGRectMake(0,0,20,20);
        UIGraphicsBeginImageContext( rect.size );
        [[UIImage imageNamed:@"home.png"] drawInRect:rect];
        UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
        UIGraphicsEndImageContext();
        UIBarButtonItem *myHomeButton = [[UIBarButtonItem alloc] initWithImage:image style:UIBarButtonItemStyleDone target:self action:@selector(myHomeAction:)];
        
        
        NSArray *myButtonArray = [[NSArray alloc] initWithObjects:myHomeButton, nil];
        
        vc.navigationItem.rightBarButtonItems = myButtonArray;
        [[self navigationController]pushViewController:vc animated:NO];
        vc =nil;
    }
    
}

- (IBAction)onForgotPasswordSubmitClicked:(id)sender
{
    if ([self stringIsValidEmail:[txt_emailForgotPassword text]] == NO)
    {
        [self displayAlert:NSLocalizedString(@"err_invalid_email", nil)];
        
        txt_emailForgotPassword.layer.cornerRadius = 8.0f;
        txt_emailForgotPassword.layer.masksToBounds = YES;
        txt_emailForgotPassword.layer.borderColor = [[UIColor redColor] CGColor];
        txt_emailForgotPassword.layer.borderWidth = 1.0f;
        
        txt_name.layer.borderColor = [[UIColor clearColor] CGColor];
        txt_password.layer.borderColor = [[UIColor clearColor] CGColor];
    }
    else
    {
        txt_emailForgotPassword.layer.cornerRadius = 8.0f;
        txt_emailForgotPassword.layer.masksToBounds = YES;
        txt_emailForgotPassword.layer.borderColor = [[UIColor clearColor] CGColor];
        txt_emailForgotPassword.layer.borderWidth = 1.0f;
        
        NSString *urlAddress = [[NSString alloc] initWithFormat:@"%@forgot.php?db=%@&userid=%@&udid=%@&cuid=%@&email=%@&device=ios&sc=%@", appdelegate.user_apipath, appdelegate.db, appdelegate.user, appdelegate.uuid, appdelegate.customer_id, [self urlEncode:[txt_emailForgotPassword text]], appdelegate.cs];
        currentRequestMenu = @"forgotPassword";
        [self connectToServer:urlAddress];
    }
}

- (IBAction)onRegisterUsingOtherEmail:(id)sender
{
    [view_popUpMain removeFromSuperview];
    [txt_email setText:@""];
    [txt_email becomeFirstResponder];
}


#pragma mark REGISTRATION
- (IBAction)onRegisterClicked:(id)sender
{
    [view_registrationResult setHidden:YES];
    [scrollView setHidden:NO];
    
    UIViewController *vc = [[UIViewController alloc] init];
    [vc setTitle:NSLocalizedString(@"registration", nil)];
    [vc setView:view_registration];
    [vc.view setBackgroundColor:self.view.backgroundColor];
    
    vc.navigationItem.backBarButtonItem = [[UIBarButtonItem alloc]initWithTitle:@"" style:UIBarButtonItemStylePlain target:nil action:nil];
    
    appdelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    
    self.title = appdelegate.iconname;
    
    CGRect rect = CGRectMake(0,0,20,20);
    UIGraphicsBeginImageContext( rect.size );
    [[UIImage imageNamed:@"home.png"] drawInRect:rect];
    UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    UIBarButtonItem *myHomeButton = [[UIBarButtonItem alloc] initWithImage:image style:UIBarButtonItemStyleDone target:self action:@selector(myHomeAction:)];
    
    
    NSArray *myButtonArray = [[NSArray alloc] initWithObjects:myHomeButton, nil];
    
    vc.navigationItem.rightBarButtonItems = myButtonArray;
    
    /*UITapGestureRecognizer *gestureRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(touchesBegan:withEvent:)];
    gestureRecognizer.delegate = self;
    [scrollView addGestureRecognizer:gestureRecognizer]; */
    
    [[self navigationController]pushViewController:vc animated:NO];
    vc =nil;
}

- (IBAction)onRegisterSubmitClicked:(id)sender
{
    
    if ([[txt_name text]length] == 0 )
    {
        [self displayAlert:NSLocalizedString(@"err_name", nil)];
        
        txt_name.layer.cornerRadius = 8.0f;
        txt_name.layer.masksToBounds = YES;
        txt_name.layer.borderColor = [[UIColor redColor] CGColor];
        txt_name.layer.borderWidth = 1.0f;
        
        txt_email.layer.borderColor = [[UIColor clearColor] CGColor];
        txt_password.layer.borderColor = [[UIColor clearColor] CGColor];
    }
    else if ([[txt_email text] length] == 0)
    {
        [self displayAlert:NSLocalizedString(@"err_email", nil)];
        
        txt_email.layer.cornerRadius = 8.0f;
        txt_email.layer.masksToBounds = YES;
        txt_email.layer.borderColor = [[UIColor redColor] CGColor];
        txt_email.layer.borderWidth = 1.0f;
        
        txt_name.layer.borderColor = [[UIColor clearColor] CGColor];
        txt_password.layer.borderColor = [[UIColor clearColor] CGColor];
    }
    else if ([self stringIsValidEmail:[txt_email text]] == NO)
    {
        [self displayAlert:NSLocalizedString(@"err_invalid_email", nil)];
        
        txt_email.layer.cornerRadius = 8.0f;
        txt_email.layer.masksToBounds = YES;
        txt_email.layer.borderColor = [[UIColor redColor] CGColor];
        txt_email.layer.borderWidth = 1.0f;
        
        txt_name.layer.borderColor = [[UIColor clearColor] CGColor];
        txt_password.layer.borderColor = [[UIColor clearColor] CGColor];
    }
    else if ([[txt_password text] length] == 0)
    {
        [self displayAlert:NSLocalizedString(@"err_password", nil)];
        
        txt_password.layer.cornerRadius = 8.0f;
        txt_password.layer.masksToBounds = YES;
        txt_password.layer.borderColor = [[UIColor redColor] CGColor];
        txt_password.layer.borderWidth = 1.0f;
        
        txt_name.layer.borderColor = [[UIColor clearColor] CGColor];
        txt_email.layer.borderColor = [[UIColor clearColor] CGColor];
    }
    else
    {
        txt_name.layer.borderColor = [[UIColor clearColor] CGColor];
        txt_email.layer.borderColor = [[UIColor clearColor] CGColor];
        txt_password.layer.borderColor = [[UIColor clearColor] CGColor];
        
        NSString *urlAddress = [[NSString alloc] initWithFormat:@"%@cust_regist.php?db=%@&userid=%@&udid=%@&cuid=%@&email=%@&name=%@&password=%@&phone=%@&tag=APPS&device=ios&sc=%@", appdelegate.user_apipath, [self urlEncode:appdelegate.db], [self urlEncode:appdelegate.user], [self urlEncode:appdelegate.uuid], [self urlEncode:appdelegate.customer_id], [self urlEncode:[txt_email text]], [self urlEncode:[txt_name text]], [self urlEncode:[txt_password text]], [self urlEncode:[txt_telNo text]], appdelegate.cs];
        currentRequestMenu = @"registerUser";
        [self connectToServer:urlAddress];
        //NSLog(@"urlAddress:: %@", urlAddress);
    }
}

- (void)textFieldDidBeginEditing:(UITextField *)textField
{
    CGPoint scrollPoint = CGPointMake(0, textField.frame.origin.y - 100);
    [scrollView setContentOffset:scrollPoint animated:NO];
}

- (void)textFieldDidEndEditing:(UITextField *)textField
{
    [scrollView setContentOffset:CGPointZero animated:NO];
}

-(BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    return YES;
}

#pragma mark FACEBOOK LOGIN
- (void)loginViewShowingLoggedInUser:(FBLoginView *)_loginView {
    // if you become logged in, no longer flag to skip log in
    self.shouldSkipLogIn = NO;
    [view_profileView setHidden:NO];
    [view_loginView setHidden:YES];
    [MenuTableView setHidden:NO];
    
    if (FBSession.activeSession.isOpen)
    {
        [[FBRequest requestForMe] startWithCompletionHandler:
         ^(FBRequestConnection *connection, NSDictionary<FBGraphUser> *user, NSError *error) {
             if (!error)
             {
                 lbl_userName.text = user.name;
                 userProfileImage.profileID = [user objectForKey:@"id"];
                 appdelegate.hasLoggedIn = YES;
                 
                 [btn_logOutFBMyApp setTitle:NSLocalizedString(@"logout", nil) forState:UIControlStateNormal];
                 [btn_logOutFBMyApp setBackgroundColor:[UIColor colorWithRed:0.000 green:0.333 blue:0.557 alpha:1.000]];
                 
                 NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
                 [defaults setObject:@"YES" forKey:@"hasLoggedInFB"];
                 [defaults setObject:user.name forKey:@"customer_name"];
                 
                 NSString *hasAllowEmail = [defaults objectForKey:@"hasAllowEmail"];
                 NSLog(@"hasAllowEmail :%@", hasAllowEmail);
                 
                 if ([listOfsetting count]>1) {
                     for(int i=0; i<[listOfsetting count]; i++) {
                         if (i>0) {
                             [listOfsetting removeObjectAtIndex:i];
                             i--;
                         }
                     }
                     [MenuTableView reloadData];
                 }
                
                 if (![hasAllowEmail isEqualToString:@"YES"])
                 {
                     if ([[FBSession activeSession]isOpen]) {
                         [FBSession.activeSession requestNewReadPermissions:@[@"email",
                                                                              @"basic_info",]
                                                          completionHandler:^(FBSession *session,
                                                                              NSError *error){
                                                              // Handle new permissions callback
                                                              //NSLog(@"MaggieTest2");
                                                              [defaults setObject:@"YES" forKey:@"hasAllowEmail"];
                                                              [[NSUserDefaults standardUserDefaults] synchronize];
                                                              
                                                              NSString *urlAddress = [[NSString alloc] initWithFormat:@"%@cust_regist.php?db=%@&userid=%@&udid=%@&cuid=%@&email=%@&name=%@&password=%@&phone=%@&tag=FB&device=ios&sc=%@", appdelegate.user_apipath, appdelegate.db, appdelegate.user, appdelegate.uuid, appdelegate.customer_id, [self urlEncode:[user objectForKey:@"email"]], [self urlEncode:[lbl_userName text]],   [self urlEncode:@"default"], [self urlEncode:@""], appdelegate.cs];
                                                              currentRequestMenu = @"registerUserFromFB";
                                                              //NSLog(@"urlAddress:: %@", urlAddress);
                                                              [self connectToServer:urlAddress];
                                                          }];
                     }
                 }
                 [[NSUserDefaults standardUserDefaults] synchronize];
            }
         }];
    }
    
    //[self checkscorepoint];
    NSLog(@"%s",__PRETTY_FUNCTION__);
}

- (void)loginView:(FBLoginView *)loginView
      handleError:(NSError *)error{
    NSString *alertMessage = nil, *alertTitle;
    
    // Facebook SDK * error handling *
    // Error handling is an important part of providing a good user experience.
    // Since this sample uses the FBLoginView, this delegate will respond to
    // login failures, or other failures that have closed the session (such
    // as a token becoming invalid). Please see the [- postOpenGraphAction:]
    // and [- requestPermissionAndPost] on `SCViewController` for further
    // error handling on other operations.
    
    if (error.fberrorShouldNotifyUser) {
        // If the SDK has a message for the user, surface it. This conveniently
        // handles cases like password change or iOS6 app slider state.
        alertTitle = @"Something Went Wrong";
        alertMessage = error.fberrorUserMessage;
    } else if (error.fberrorCategory == FBErrorCategoryAuthenticationReopenSession) {
        // It is important to handle session closures as mentioned. You can inspect
        // the error for more context but this sample generically notifies the user.
        alertTitle = @"Session Error";
        alertMessage = @"Your current session is no longer valid. Please log in again.";
    } else if (error.fberrorCategory == FBErrorCategoryUserCancelled) {
        // The user has cancelled a login. You can inspect the error
        // for more context. For this sample, we will simply ignore it.
        NSLog(@"user cancelled login");
    } else {
        // For simplicity, this sample treats other errors blindly, but you should
        // refer to https://developers.facebook.com/docs/technical-guides/iossdk/errors/ for more information.
        alertTitle  = @"Unknown Error";
        alertMessage = @"Error. Please try again later.";
        NSLog(@"Unexpected error:%@", error);
    }
    
    if (alertMessage) {
        [[[UIAlertView alloc] initWithTitle:alertTitle
                                    message:alertMessage
                                   delegate:nil
                          cancelButtonTitle:@"OK"
                          otherButtonTitles:nil] show];
    }
    NSLog(@"%s",__PRETTY_FUNCTION__);
}

- (void)loginViewShowingLoggedOutUser:(FBLoginView *)loginView {
    // Facebook SDK * login flow *
    // It is important to always handle session closure because it can happen
    // externally; for example, if the current session's access token becomes
    // invalid. For this sample, we simply pop back to the landing page.
    AppDelegate *appDelegate = (AppDelegate *)[UIApplication sharedApplication].delegate;
    if (appDelegate.isNavigatingAwayFromLogin) {
        // The delay is for the edge case where a session is immediately closed after
        // logging in and our navigation controller is still animating a push.
        [self performSelector:@selector(logOut:) withObject:nil afterDelay:.5];
    } else {
        [self logOut:nil];
    }
    
    NSLog(@"%s",__PRETTY_FUNCTION__);
}

- (IBAction)logOut:(id)sender
{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [defaults setObject:@"NO" forKey:@"hasLoggedInFB"];
    [defaults setObject:@"NO" forKey:@"hasAllowEmail"];
    
    if ([[btn_logOutFBMyApp titleForState:UIControlStateNormal] isEqualToString:NSLocalizedString(@"logout", nil)])
    {
        [view_loginView setHidden:NO];
        [view_profileView setHidden:YES];
        [MenuTableView setHidden:YES];
        appdelegate.hasLoggedIn = NO;
        
        userProfileImage.profileID = nil;
        
        appdelegate.customer_id = @"0";
        [appdelegate checksum];
        
        [defaults setObject:@"0" forKey:@"customer_id"];
        [defaults setObject:@"0" forKey:@"customer_name"];
        [defaults setObject:@"0" forKey:@"customer_phone"];
        [defaults setObject:@"" forKey:@"login_id"];
        
        [defaults setObject:@"NO" forKey:@"hasLoggedInMyApp"];
    }

    if ([[defaults objectForKey:@"hasLoggedInMyApp"] isEqualToString:@"YES"] || [[defaults objectForKey:@"hasLoggedInFB"] isEqualToString:@"YES"])
    {
        [view_loginView setHidden:YES];
        [view_profileView setHidden:NO];
        [MenuTableView setHidden:NO];
        appdelegate.hasLoggedIn = YES;
        
        [btn_logOutFBMyApp setTitle:NSLocalizedString(@"logout", nil) forState:UIControlStateNormal];
        [btn_logOutFBMyApp setBackgroundColor:[UIColor redColor]];
        
        userProfileImage.profileID = nil;
        lbl_userName.text = [defaults objectForKey:@"customer_name"];
        if ([[defaults objectForKey:@"hasLoggedInMyApp"] isEqualToString:@"YES"]) {
            [lblloginsignedinas setHidden:NO];
            if ([loginpluginname isEqualToString:@"53"]) {
                [lbllogintext setHidden:NO];
                lbllogintext.text = [defaults objectForKey:@"login_id"];
            } else {
                lbllogintext.text = [defaults objectForKey:@"customer_name"];
            }
        } else {
            [lblloginsignedinas setHidden:YES];
            [lbllogintext setHidden:YES];
        }
    }
    else
    {
        /*  Mod by   :  Hazwan
         *  Date     :  5 Dec 2013
         *  Purpose  :  Temporarily hide both profile view and login view to 'disable' login capabilities at the moment
         *              - facilitating apps for Highscope, RoxyHP & M3Asia Indo. Remove code to 're-enable' login.
         *              Uncomment marked (*) lines below to revert to original code
         */
        //-- START
        //[view_profileView setHidden:YES];
        //[view_loginView setHidden:YES];
        
        if ([listOfsetting count]==1) {
            if ([loginpluginname isEqualToString:@"53"]) {
                [listOfsetting addObject:@"Change Password"];
            } else {
                [listOfsetting addObject:@"Scoreboard"];
                [listOfsetting addObject:@"Update Details"];
                [listOfsetting addObject:@"Change Password"];
            }
            [MenuTableView reloadData];
        }

        //[self checkscorepoint];
        
        [view_loginView setHidden:NO];
        [view_profileView setHidden:YES];
        [MenuTableView setHidden:YES];
        appdelegate.hasLoggedIn = NO;
        
        //-- END
    }
    
    [[NSUserDefaults standardUserDefaults] synchronize];
    [FBSession.activeSession closeAndClearTokenInformation];
}


- (IBAction)termscondlink:(id)sender
{
    sqlite3 *database;
    termsconds = @"";
    
    if(sqlite3_open([appdelegate.databasePath UTF8String], &database) == SQLITE_OK) {
        // Setup the SQL Statement and compile it for faster access
        
        NSString *theme_object = [[NSString alloc] initWithFormat:@"Select termscons_url from app_setup"];
        //NSLog(@"theme_objecttheme2:: %@", theme_object);
        const char *sqlStatement1 = [theme_object UTF8String];
        sqlite3_stmt *compiledStatement1;
        if(sqlite3_prepare_v2(database, sqlStatement1, -1, &compiledStatement1, NULL) == SQLITE_OK) {
            
            while(sqlite3_step(compiledStatement1) == SQLITE_ROW) {
                
                termsconds = [NSString stringWithUTF8String:(char *)sqlite3_column_text(compiledStatement1, 0)];
                
            }
        }
        // Release the compiled statement from memory
        sqlite3_finalize(compiledStatement1);
    }
    sqlite3_close(database);
    
    //NSLog(@"termsconds:: %@", termsconds);
    if ([termsconds length]>0) {
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:termsconds]];
        //[[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"https://www.m3asia.com"]];
    }
}

-(void)ChkUnChk:(id)sender
{
    
    UIButton *btn=(UIButton *)sender;
    NSString *Str=[NSString stringWithFormat:@"%ld",(long)btn.tag];
    BOOL flag=   [selectedBtnarr containsObject:Str];
    
    if (flag==YES)
    {
        chkunchk = NO;
        [btn setBackgroundImage:[UIImage imageNamed:@"" ] forState:UIControlStateNormal];
        [selectedBtnarr removeObject:Str];
        btn_submitRegist.enabled = NO;
    }
    else
    {
        chkunchk = YES;
        [selectedBtnarr addObject:Str];
        [btn setBackgroundImage:[UIImage imageNamed:@"checked.png"] forState:UIControlStateNormal];
        btn_submitRegist.enabled = YES;
    }

}

-(void) viewDidDisappear:(BOOL)animated {
    
    [super viewDidDisappear:animated];
    
    appdelegate.homeclicked = NO;
    [modalController dismissViewControllerAnimated:YES completion:nil];
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)dealloc {
	currentRequestMenu =nil;
    /*receivedData12 =nil;
    receivedData11 =nil;
	receivedData10 =nil;
    receivedData9 =nil;
    receivedData8 =nil;
	receivedData7 =nil;
    receivedData6 =nil;
    receivedData5 =nil;
	receivedData4 =nil;
    receivedData3 =nil;
	receivedData2 =nil;
    receivedData1 =nil; */
    listOfmenu =nil;
    listOfcategory =nil;
    listOfContent =nil;
    listOfappsetup =nil;
    listOfCompProfile =nil;
    listOfContentDet =nil;
    listOfProductHome =nil;
    listOfProductImg =nil;
    listOfProductCat =nil;
    listOfProductDet =nil;
    view_whatIsMyApp =nil;
    view_resetPassword =nil;
    view_resetPasswordResult =nil;
    view_registration =nil;
    view_registrationResult =nil;
    scrollView =nil;
    loginView =nil;
    view_loginView =nil;
    view_profileView =nil;
    userProfileImage =nil;
    lbl_userName =nil;
    txt_name =nil;
    txt_email =nil;
    txt_telNo =nil;
    txt_password =nil;
    txt_resetPassword =nil;
    txt_myAppPassword =nil;
    txt_myAppEmail =nil;
    btn_logOutFBMyApp =nil;
    view_popUpRegisterFailed =nil;
    view_popUpMain =nil;
    label_errorDesc =nil;
    txt_emailForgotPassword =nil;
    MenuTableView = nil;
    //[super dealloc];
}

@end
