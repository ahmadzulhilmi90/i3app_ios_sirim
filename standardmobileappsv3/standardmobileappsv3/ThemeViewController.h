//
//  ThemeViewController.h
//  standardmobileappsv3
//
//  Created by M3Online on 5/30/14.
//  Copyright (c) 2014 M3Online. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CustomView.h"

@class GADBannerView;

@interface ThemeViewController : UIViewController <CustomViewDelegate, UITableViewDataSource, UITableViewDelegate,UIViewControllerTransitioningDelegate> {

@private
    NSMutableArray *galleryImages_;
    
    NSMutableArray *listOfObject;
    NSDictionary *dicObject;
    NSString *page_id;
    NSString *pagedata;
    NSString *themepagenotification;  /*** Push Notification 27Aug2015 ***/
    
    CustomView *viewC;
    
    UIView *view_popoverMain;
    UIView *view_plain;
    UIButton *buttonclose;
    UIButton *buttonlogin;
    UILabel *lbllogin;
    
}

@property(nonatomic, strong) GADBannerView *bannerView;
/// The game text.
@property(nonatomic, strong) UILabel *gameText;

/// The play again button.
@property(nonatomic, strong) UIButton *playAgainButton;

/// The button to show rewarded video.
@property(strong, nonatomic) UIButton *showVideoButton;

/// The text indicating current coin count.
@property(strong, nonatomic) UILabel *coinCountLabel;

@property (nonatomic, strong) NSMutableArray *listOfObject;
@property (nonatomic, strong) NSString *page_id;
@property (nonatomic, strong) NSString *pagedata;
@property (nonatomic, strong) NSString *themepagenotification; /*** Push Notification 27Aug2015 ***/

@property (nonatomic, strong) CustomView *viewC;

@property (nonatomic, strong) UIView *view_popoverMain;
@property (nonatomic, strong) UIButton *buttonclose;
@property (nonatomic, strong) UIButton *buttonlogin;
@property (nonatomic, strong) UILabel *lbllogin;
@property (nonatomic, strong) UIView *view_plain;

@property (nonatomic, retain) NSMutableArray *listOfAdmob;

//- (IBAction)backToRoot:(id)sender;
/// Starts a new game. Shows an interstitial if it's ready.
/*- (IBAction)playAgain:(id)sender;

/// Shows a rewarded video.
- (IBAction)showVideo:(id)sender;

/// Pauses the game.
- (void)pauseGame;

/// Resumes the game.
- (void)resumeGame; */

@end
