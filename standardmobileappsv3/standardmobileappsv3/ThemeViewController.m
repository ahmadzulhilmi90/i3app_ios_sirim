//
//  Theme2ViewController.m
//  standardmobileappsv3
//
//  Created by M3Online on 5/30/14.
//  Copyright (c) 2014 M3Online. All rights reserved.
//

#import "ThemeViewController.h"
#import "AppDelegate.h"
#import "NSString+HTML.h"
#import "RootViewController.h"
#import "ViewController.h"
#import "InitialSlidingViewController.h"
#import "BTSlideInteractor.h"
@import GoogleMobileAds;

/// Constant for coin rewards.
//static const NSInteger GameOverReward = 1;

/// Starting time for game counter.
//static const NSInteger GameLength = 10;

/*typedef NS_ENUM(NSUInteger, GameState) {
    kGameStateNotStarted = 0,  ///< Game has not started.
    kGameStatePlaying = 1,     ///< Game is playing.
    kGameStatePaused = 2,      ///< Game is paused.
    kGameStateEnded = 3        ///< Game has ended.
};

/// The game length.
static const NSInteger kGameLength = 5; */

@interface ThemeViewController () /*<UIAlertViewDelegate, GADRewardBasedVideoAdDelegate>*/
@property (strong, nonatomic) BTSlideInteractor *interactor;
/*/// The interstitial ad.
@property(nonatomic, strong) GADInterstitial *interstitial;

/// Number of coins the user has earned.
@property(nonatomic, assign) NSInteger coinCount;

/// The game counter.
@property(nonatomic, assign) NSInteger counter;

/// The countdown timer.
@property(nonatomic, strong) NSTimer *timer;

/// The amount of time left in the game.
@property(nonatomic, assign) NSInteger timeLeft;

/// The state of the game.
@property(nonatomic, assign) GameState gameState;

/// The date that the timer was paused.
@property(nonatomic, strong) NSDate *pauseDate;

/// The last fire date before a pause.
@property(nonatomic, strong) NSDate *previousFireDate; */
@end

@implementation ThemeViewController
LoadingView *loadingView;

@synthesize listOfObject, page_id, pagedata, themepagenotification, viewC, view_popoverMain, buttonclose, buttonlogin, lbllogin, view_plain, bannerView, playAgainButton, gameText, showVideoButton, coinCountLabel, listOfAdmob; /*** Push Notification 27Aug2015 ***/

float delaytime;

InitialSlidingViewController *modalController;

__strong NSString *themecode;
__strong NSString *page_id;
__strong NSString *page_name;
__strong NSString *prevpage;
__strong NSString *aplugintype;
__strong NSString *aid;
__strong NSString *currentRequest;
__strong NSString *admob_type;
__strong NSString *admob_topID;
__strong NSString *attribute;

AppDelegate *appdelegate;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

-(void) processdata {
    
    //AppDelegate *appdelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    sqlite3 *database;
    
    if(sqlite3_open([appdelegate.databasePath UTF8String], &database) == SQLITE_OK) {
        // Setup the SQL Statement and compile it for faster access
        
        attribute = @"";
        // Setup the SQL Statement and compile it for faster access
        NSString *theme_admob = [[NSString alloc] initWithFormat:@"SELECT * from theme_object WHERE page_id = %@ AND slot_code='ADMOB'", page_id];
        //NSLog(@"theme_objectAdmob:: %@", theme_admob);
        const char *sqlStatementadmob = [theme_admob UTF8String];
        sqlite3_stmt *compiledStatementadmob;
        if(sqlite3_prepare_v2(database, sqlStatementadmob, -1, &compiledStatementadmob, NULL) == SQLITE_OK) {
            
            while(sqlite3_step(compiledStatementadmob) == SQLITE_ROW) {
                
                attribute = [NSString stringWithUTF8String:(char *)sqlite3_column_text(compiledStatementadmob, 6)];
                
                attribute = [attribute stringByDecodingHTMLEntities];
                attribute = [attribute stringByReplacingOccurrencesOfString:@"''" withString:@"'"];
                
            }
        }
        sqlite3_finalize(compiledStatementadmob);
    
        //NSLog(@"attribute:: %@", attribute);
        // Release the compiled statement from memory
        
        if ([attribute length]>0) {
            NSArray *myArray = [attribute componentsSeparatedByCharactersInSet:[NSCharacterSet characterSetWithCharactersInString:@"|"]];
            admob_type = [myArray objectAtIndex:0];
            admob_topID = [myArray objectAtIndex:1];
            //NSLog(@"myArray: %@", myArray);
            if ([admob_type isEqualToString:@"1"]) {
                NSString *urlAddress = [[NSString alloc] initWithFormat:@"%@admob.php?db=%@&userid=%@&udid=%@&cuid=%@&top_id=%@",appdelegate.user_apipath,appdelegate.db,appdelegate.user,appdelegate.uuid,appdelegate.customer_id,admob_topID];
                currentRequest = @"getAdmobSetting";
                //NSLog(@"urlAddressAdmob:: %@", urlAddress);
                [self connectToServer:urlAddress];
            }
        }
        
        NSString *theme_object = [[NSString alloc] initWithFormat:@"SELECT * from theme_object WHERE page_id = %@ Order by slot_code", page_id];
        //NSLog(@"theme_objecttheme2:: %@", theme_object);
        const char *sqlStatement1 = [theme_object UTF8String];
        sqlite3_stmt *compiledStatement1;
        if(sqlite3_prepare_v2(database, sqlStatement1, -1, &compiledStatement1, NULL) == SQLITE_OK) {
            
            listOfObject = [[NSMutableArray alloc] init];
            while(sqlite3_step(compiledStatement1) == SQLITE_ROW) {
                
                NSString *aidobj = [NSString stringWithUTF8String:(char *)sqlite3_column_text(compiledStatement1, 0)];
                NSString *athemecode = [NSString stringWithUTF8String:(char *)sqlite3_column_text(compiledStatement1, 1)];
                NSString *aslotcode = [NSString stringWithUTF8String:(char *)sqlite3_column_text(compiledStatement1, 2)];
                NSString *apageid = [NSString stringWithUTF8String:(char *)sqlite3_column_text(compiledStatement1, 3)];
                NSString *aobjcttype = [NSString stringWithUTF8String:(char *)sqlite3_column_text(compiledStatement1, 4)];
                NSString *aobjectvalue = [NSString stringWithUTF8String:(char *)sqlite3_column_text(compiledStatement1, 5)];
                NSString *aattribute = [NSString stringWithUTF8String:(char *)sqlite3_column_text(compiledStatement1, 6)];
                NSString *aactiontype = [NSString stringWithUTF8String:(char *)sqlite3_column_text(compiledStatement1, 7)];
                NSString *aactionvalue = [NSString stringWithUTF8String:(char *)sqlite3_column_text(compiledStatement1, 8)];
                NSString *aremark = [NSString stringWithUTF8String:(char *)sqlite3_column_text(compiledStatement1, 10)];
                
                aobjectvalue = [aobjectvalue stringByDecodingHTMLEntities];
                aremark = [aremark stringByDecodingHTMLEntities];
                aobjectvalue = [aobjectvalue stringByReplacingOccurrencesOfString:@"''" withString:@"'"];
                aremark = [aremark stringByReplacingOccurrencesOfString:@"''" withString:@"'"];
                
                dicObject = [NSDictionary dictionaryWithObjectsAndKeys:aidobj,@"id",athemecode,@"theme_code",aslotcode,@"slot_code",apageid,@"page_id",aobjcttype,@"object_type",aobjectvalue,@"object_value",aattribute,@"attribute",aactiontype,@"action_type",aactionvalue,@"action_value",aremark,@"remark",nil];
                [listOfObject addObject:dicObject];
                //NSLog(@"listOfObjecttheme2: %@", listOfObject);
                
                aidobj =nil;
                athemecode =nil;
                aslotcode =nil;
                apageid =nil;
                aobjcttype =nil;
                aobjectvalue =nil;
                aattribute =nil;
                aactiontype =nil;
                aactionvalue =nil;
                aremark =nil;
                
            }
        }
        // Release the compiled statement from memory
        sqlite3_finalize(compiledStatement1);
    }
    sqlite3_close(database);
    
}

-(void)loadView
{
    [super loadView];
    
    appdelegate.homeclicked = NO;
    
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    //self.title = appdelegate.iconname;
    self.navigationItem.backBarButtonItem = [[UIBarButtonItem alloc]initWithTitle:@"" style:UIBarButtonItemStylePlain target:nil action:nil];
    
    appdelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    
    self.navigationItem.backBarButtonItem = [[UIBarButtonItem alloc]initWithTitle:@"" style:UIBarButtonItemStylePlain target:nil action:nil];
    
    CGRect rect = CGRectMake(0,0,20,20);
    UIGraphicsBeginImageContext( rect.size );
    [[UIImage imageNamed:@"home.png"] drawInRect:rect];
    UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    //UIBarButtonItem *myHomeButton = [[UIBarButtonItem alloc] initWithTitle:appdelegate.homepage style:UIBarButtonItemStyleDone target:self action:@selector(myHomeAction:)];
    UIBarButtonItem *myHomeButton = [[UIBarButtonItem alloc] initWithImage:image style:UIBarButtonItemStyleDone target:self action:@selector(myHomeAction:)];
    
    NSArray *myButtonArray = [[NSArray alloc] initWithObjects:myHomeButton, nil];
    
    self.navigationItem.rightBarButtonItems = myButtonArray;
    image = nil;
    
    //Interstitial
    // Pause game when application is backgrounded.
    /*[[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(pauseGame)
                                                 name:UIApplicationDidEnterBackgroundNotification
                                               object:nil];
    
    // Resume game when application becomes active.
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(resumeGame)
                                                 name:UIApplicationDidBecomeActiveNotification
                                               object:nil];
    [self startNewGame]; */
    
    /* Rewarded Video
    [GADRewardBasedVideoAd sharedInstance].delegate = self;
    self.coinCount = 0;
    [self startNewGame]; */
    
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    //[self resumeGame];
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    //[self pauseGame];
}

#pragma mark Game logic

/*- (void)startNewGame {
    [self createAndLoadInterstitial];
    
    self.gameState = kGameStatePlaying;
    playAgainButton.hidden = YES;
    self.timeLeft = kGameLength;
    [self updateTimeLeft];
    self.timer = [NSTimer scheduledTimerWithTimeInterval:1.0f
                                                  target:self
                                                selector:@selector(decrementTimeLeft:)
                                                userInfo:nil
                                                 repeats:YES]; */
    /* Rewarded Video
    if (![[GADRewardBasedVideoAd sharedInstance] isReady]) {
        [self requestRewardedVideo];
    }
    self.gameState = kGameStatePlaying;
    self.playAgainButton.hidden = YES;
    self.showVideoButton.hidden = YES;
    self.counter = GameLength;
    self.gameText.text = [NSString stringWithFormat:@"%ld", (long)self.counter];
    self.timer = [NSTimer scheduledTimerWithTimeInterval:1.0
                                                  target:self
                                                selector:@selector(decrementCounter:)
                                                userInfo:nil
                                                 repeats:YES];
    self.timer.tolerance = GameLength * 0.1; */
/*}

- (void)requestRewardedVideo {
    GADRequest *request = [GADRequest request];
    [[GADRewardBasedVideoAd sharedInstance] loadRequest:request
                                           withAdUnitID:@"ca-app-pub-6079242097909886/1506460230"];
}

- (void)createAndLoadInterstitial {
    self.interstitial =
    [[GADInterstitial alloc] initWithAdUnitID:@"ca-app-pub-6079242097909886/9063238822"];
    
    GADRequest *request = [GADRequest request];
    // Request test ads on devices you specify. Your test device ID is printed to the console when
    // an ad request is made.
    request.testDevices = @[ kGADSimulatorID, @"2077ef9a63d2b398840261c8221a0c9a" ];
    [self.interstitial loadRequest:request];
}

- (void)updateTimeLeft {
    gameText.text = [NSString stringWithFormat:@"%ld seconds left!", (long)self.timeLeft];
}

- (void)decrementTimeLeft:(NSTimer *)timer {
    self.timeLeft--;
    [self updateTimeLeft];
    if (self.timeLeft == 0) {
        [self endGame];
    }
}

- (void)pauseGame {
    if (self.gameState != kGameStatePlaying) {
        return;
    }
    self.gameState = kGameStatePaused;
    
    // Record the relevant pause times.
    self.pauseDate = [NSDate date];
    self.previousFireDate = [self.timer fireDate];
    
    // Prevent the timer from firing while app is in background.
    [self.timer setFireDate:[NSDate distantFuture]];
}

- (void)resumeGame {
    if (self.gameState != kGameStatePaused) {
        return;
    }
    self.gameState = kGameStatePlaying;
    
    // Calculate amount of time the app was paused.
    float pauseTime = [self.pauseDate timeIntervalSinceNow] * -1;
    
    // Set the timer to start firing again.
    [self.timer setFireDate:[NSDate dateWithTimeInterval:pauseTime sinceDate:self.previousFireDate]];
}

- (void)endGame {
    self.gameState = kGameStateEnded;
    [self.timer invalidate];
    self.timer = nil;
    [[[UIAlertView alloc]
      initWithTitle:@"Game Over"
      message:[NSString stringWithFormat:@"You lasted %ld seconds", (long)kGameLength]
      delegate:self
      cancelButtonTitle:@"Ok"
      otherButtonTitles:nil] show]; */
    /* Rewarded Video
    self.timer = nil;
    self.gameState = kGameStateEnded;
    self.gameText.text = @"Game over!";
    if ([[GADRewardBasedVideoAd sharedInstance] isReady]) {
        self.showVideoButton.hidden = NO;
    }
    self.playAgainButton.hidden = NO;
    // Reward user with coins for finishing the game.
    [self earnCoins:GameOverReward]; */
/*}

- (void)setTimer:(NSTimer *)timer {
    [_timer invalidate];
    _timer = timer;
}

#pragma Interstitial button actions

- (void)decrementCounter:(NSTimer *)timer {
    self.counter--;
    if (self.counter > 0) {
        self.gameText.text = [NSString stringWithFormat:@"%ld", (long)self.counter];
    } else {
        [self endGame];
    }
}

- (void)earnCoins:(NSInteger)coins {
    self.coinCount += coins;
    [self.coinCountLabel setText:[NSString stringWithFormat:@"Coins: %ld", (long)self.coinCount]];
}

- (IBAction)showVideo:(id)sender {
    if ([[GADRewardBasedVideoAd sharedInstance] isReady]) {
        [[GADRewardBasedVideoAd sharedInstance] presentFromRootViewController:self];
    } else {
        [[[UIAlertView alloc]
          initWithTitle:@"Interstitial not ready"
          message:@"The interstitial didn't finish " @"loading or failed to load"
          delegate:self
          cancelButtonTitle:@"Drat"
          otherButtonTitles:nil] show];
    }
}

- (IBAction)playAgain:(id)sender {
    [self startNewGame];
} */

#pragma mark UIAlertViewDelegate implementation

/*- (void)alertView:(UIAlertView *)alertView willDismissWithButtonIndex:(NSInteger)buttonIndex {
    if (self.interstitial.isReady) {
        [self.interstitial presentFromRootViewController:self];
    } else {
        NSLog(@"Ad wasn't ready");
    }
    playAgainButton.hidden = NO;
    [self startNewGame];
}

#pragma mark GADRewardBasedVideoAdDelegate implementation

- (void)rewardBasedVideoAdDidReceiveAd:(GADRewardBasedVideoAd *)rewardBasedVideoAd {
    NSLog(@"Reward based video ad is received.");
}

- (void)rewardBasedVideoAdDidOpen:(GADRewardBasedVideoAd *)rewardBasedVideoAd {
    NSLog(@"Opened reward based video ad.");
}

- (void)rewardBasedVideoAdDidStartPlaying:(GADRewardBasedVideoAd *)rewardBasedVideoAd {
    NSLog(@"Reward based video ad started playing.");
}

- (void)rewardBasedVideoAdDidClose:(GADRewardBasedVideoAd *)rewardBasedVideoAd {
    NSLog(@"Reward based video ad is closed.");
    self.showVideoButton.hidden = YES;
}

- (void)rewardBasedVideoAd:(GADRewardBasedVideoAd *)rewardBasedVideoAd
   didRewardUserWithReward:(GADAdReward *)reward {
    NSString *rewardMessage =
    [NSString stringWithFormat:@"Reward received with currency %@ , amount %lf", reward.type,
     [reward.amount doubleValue]];
    NSLog(@"%@", rewardMessage);
    // Reward the user for watching the video.
    [self earnCoins:[reward.amount integerValue]];
    self.showVideoButton.hidden = YES;
}

- (void)rewardBasedVideoAdWillLeaveApplication:(GADRewardBasedVideoAd *)rewardBasedVideoAd {
    NSLog(@"Reward based video ad will leave application.");
}

- (void)rewardBasedVideoAd:(GADRewardBasedVideoAd *)rewardBasedVideoAd
    didFailToLoadWithError:(NSError *)error {
    NSLog(@"Reward based video ad failed to load.");
}

- (void)dealloc {
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:UIApplicationDidEnterBackgroundNotification
                                                  object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:UIApplicationDidBecomeActiveNotification
                                                  object:nil];
} */

//Slide Menu
- (void)willAnimateRotationToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation duration:(NSTimeInterval)duration
{
    //NSLog(@"view frame=%@ bounds=%@", NSStringFromCGRect(self.view.frame), NSStringFromCGRect(self.view.bounds));
}


- (void)showModalButtonWasTouched
{
    self.interactor = [[BTSlideInteractor alloc] init];
    self.interactor.presenting = YES;
    
    //BTModalViewController *modalController = [self.storyboard instantiateViewControllerWithIdentifier:@"ModalViewController"];
    modalController = [[InitialSlidingViewController alloc] init];
    modalController.modalPresentationStyle = UIModalPresentationCustom;
    modalController.transitioningDelegate = self.interactor;
    [self.navigationController presentViewController:modalController animated:YES completion:nil];
    //[self.navigationController popToViewController:modalController animated:NO];
    //[self.navigationController pushViewController:modalController animated:YES];
}
// End Slide Menu

-(void)myHomeAction:(UIBarButtonItem *)sender{
    
    appdelegate.nextPage = @"";
    
    if ([appdelegate.listOfVerMenu count]>0) {
        if (appdelegate.homeclicked==NO) {
            appdelegate.homeclicked = YES;
            [self showModalButtonWasTouched];
        } else {
            appdelegate.homeclicked = NO;
            [modalController dismissViewControllerAnimated:YES completion:nil];
        }
    } else {
        //AppDelegate *appdelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
        //NSLog(@"View Controllers : %@",[appdelegate.navController viewControllers]);
        NSMutableArray *navigationArray = [[NSMutableArray alloc] initWithArray: appdelegate.navController.viewControllers];
        
        for (UIViewController* viewController in appdelegate.navController.viewControllers) {
            
            //This if condition checks whether the viewController's class is MyGroupViewController
            // if true that means its the MyGroupViewController (which has been pushed at some point)
            if ([viewController isKindOfClass:[RootViewController class]] ) {
                
                // Here viewController is a reference of UIViewController base class of MyGroupViewController
                // but viewController holds MyGroupViewController  object so we can type cast it here
                //ThemeViewController *themeViewController = (ThemeViewController*)viewController;
                //[self.navigationController popToViewController:themeViewController animated:YES];
                [navigationArray removeObjectAtIndex: 0];
            }
        }
        appdelegate.navController.viewControllers = navigationArray;
        for (UIViewController* viewController in appdelegate.navController.viewControllers) {
            
            //This if condition checks whether the viewController's class is MyGroupViewController
            // if true that means its the MyGroupViewController (which has been pushed at some point)
            if ([viewController isKindOfClass:[ViewController class]] ) {
                
                // Here viewController is a reference of UIViewController base class of MyGroupViewController
                // but viewController holds MyGroupViewController  object so we can type cast it here
                ViewController *themeViewController = (ViewController*)viewController;
                [appdelegate.navController popToViewController:themeViewController animated:NO];
                //[self.navigationController popToViewController:viewController animated:YES];
            }
        }
        //NSLog(@"View Controllers111 : %@",appdelegate.navController.viewControllers);
    }
    
    [viewC removeFromSuperview];
    viewC = [[CustomView alloc]initWithFrame:self.view.bounds withArray:listOfObject withViewController:self withArrayMenu:appdelegate.listOfVerMenu withString:themepagenotification withBool:appdelegate.homeclicked withfirst:NO]; /*** Push Notification 27Aug2015 ***/
    viewC.delegate = self;
    [self.view addSubview:viewC];
    
}

-(void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    
    sqlite3 *database;
    NSString *prevpage;
    
    if(sqlite3_open([appdelegate.databasePath UTF8String], &database) == SQLITE_OK) {
        
        // Setup the SQL Statement and compile it for faster access
        NSString *aobjectvalue;
        NSString *theme_object = [[NSString alloc] initWithFormat:@"SELECT * from theme_object WHERE page_id = %@ AND slot_code='LOGIN'", page_id];
        //NSLog(@"theme_objecttheme2:: %@", theme_object);
        const char *sqlStatement1 = [theme_object UTF8String];
        sqlite3_stmt *compiledStatement1;
        if(sqlite3_prepare_v2(database, sqlStatement1, -1, &compiledStatement1, NULL) == SQLITE_OK) {
            
            listOfObject = [[NSMutableArray alloc] init];
            while(sqlite3_step(compiledStatement1) == SQLITE_ROW) {
                
                aobjectvalue = [NSString stringWithUTF8String:(char *)sqlite3_column_text(compiledStatement1, 5)];
                
                aobjectvalue = [aobjectvalue stringByDecodingHTMLEntities];
                aobjectvalue = [aobjectvalue stringByReplacingOccurrencesOfString:@"''" withString:@"'"];
                
            }
        }
        
        if ([aobjectvalue isEqualToString:@"1"]) {
            view_popoverMain=[[UIView alloc]initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height)];
            UIColor *background = [[UIColor alloc] initWithPatternImage:[UIImage imageNamed: @"behind_alert_view.png"]];
            [view_popoverMain setBackgroundColor:background];
            //view_popoverMain.alpha = 0.5;
            //view_popoverMain.backgroundColor = [UIColor colorWithWhite:0.6 alpha:0];
            //[self.view addSubview:view_popoverMain];
            //[view_popoverMain setHidden:YES];
            view_plain=[[UIView alloc]initWithFrame:CGRectMake(self.view.frame.size.width/2-140, self.view.frame.size.height/2-130, 280, 220)];
            [view_plain setBackgroundColor:[UIColor whiteColor]];
            
            lbllogin = [[UILabel alloc]initWithFrame:CGRectMake(view_plain.frame.size.width/2-120,view_plain.frame.size.height/2-60, 240, 40)];
            lbllogin.text = @"Please login to your account";
            lbllogin.backgroundColor = [UIColor clearColor];
            lbllogin.textColor = [UIColor blackColor];
            lbllogin.textAlignment = NSTextAlignmentCenter;
            
            buttonclose = [UIButton buttonWithType:UIButtonTypeCustom];
            [buttonclose addTarget:self
                            action:@selector(closePopover:)
                  forControlEvents:UIControlEventTouchUpInside];
            //[buttonclose setTitle:@"close" forState:UIControlStateNormal];
            [buttonclose setBackgroundImage:[UIImage imageNamed:@"btn_close_x.png"] forState:UIControlStateNormal];
            buttonclose.frame = CGRectMake(view_plain.frame.size.width-60, 0, 50.0, 50.0);
            //[view_popoverMain addSubview:buttonclose];
            
            buttonlogin = [UIButton buttonWithType:UIButtonTypeCustom];
            [buttonlogin addTarget:self
                            action:@selector(logIn:)
                  forControlEvents:UIControlEventTouchUpInside];
            [buttonlogin setTitle:@"Login" forState:UIControlStateNormal];
            [buttonlogin setBackgroundColor:[UIColor grayColor]];
            [buttonlogin setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
            buttonlogin.frame = CGRectMake(view_plain.frame.size.width/2-50, view_plain.frame.size.height/2, 100, 40.0);
            
            NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
            NSString *hasLoggedInFB = [defaults objectForKey:@"hasLoggedInFB"];
            NSString *hasLoggedInMyApp = [defaults objectForKey:@"hasLoggedInMyApp"];
            
            if ([hasLoggedInFB isEqualToString:@"YES"] || [hasLoggedInMyApp isEqualToString:@"YES"])
            {
                
                [self processdata];
                
                [viewC removeFromSuperview];
                viewC = [[CustomView alloc]initWithFrame:self.view.bounds withArray:listOfObject withViewController:self withArrayMenu:appdelegate.listOfVerMenu withString:themepagenotification withBool:appdelegate.homeclicked withfirst:NO]; // Push Notification 27Aug2015 //
                viewC.delegate = self;
                [self.view addSubview:viewC];
                
            }
            else {
                
                [self.view addSubview:view_popoverMain];
                [view_popoverMain addSubview:view_plain];
                [view_plain addSubview:buttonclose];
                [view_plain addSubview:buttonlogin];
                [view_plain addSubview:lbllogin];
                [self logIn:buttonlogin];
                //[view_popoverMain setHidden:NO];
                //[self.view bringSubviewToFront:view_popoverMain];
            }
        } else {
            [self processdata];
            
            [viewC removeFromSuperview];
            viewC = [[CustomView alloc]initWithFrame:self.view.bounds withArray:listOfObject withViewController:self withArrayMenu:appdelegate.listOfVerMenu withString:themepagenotification withBool:appdelegate.homeclicked withfirst:NO]; // Push Notification 27Aug2015 //
            viewC.delegate = self;
            [self.view addSubview:viewC];
        }
        aobjectvalue =nil;
        
        // Release the compiled statement from memory
        sqlite3_finalize(compiledStatement1);
        
        // Setup the SQL Statement and compile it for faster access
        NSString *temp = [[NSString alloc] initWithFormat:@"SELECT * from theme_page WHERE id = %@", page_id];
        //NSLog(@"temp:: %@", temp);
        const char *sqlStatement = [temp UTF8String];
        sqlite3_stmt *compiledStatement;
        if(sqlite3_prepare_v2(database, sqlStatement, -1, &compiledStatement, NULL) == SQLITE_OK) {
            while(sqlite3_step(compiledStatement) == SQLITE_ROW) {
                
                prevpage = [NSString stringWithUTF8String:(char *)sqlite3_column_text(compiledStatement, 3)];
                self.title = [NSString stringWithUTF8String:(char *)sqlite3_column_text(compiledStatement, 2)];
            }
        }
        // Release the compiled statement from memory
        sqlite3_finalize(compiledStatement);
    }
    sqlite3_close(database);
    //NSLog(@"prevpage :: %@", prevpage);
    
    if ([prevpage isEqualToString:@"0"]) {
        [buttonclose setHidden:YES];
    } else {
        [buttonclose setHidden:NO];
    }
    CGRect rect = CGRectMake(0,0,20,20);
    UIGraphicsBeginImageContext( rect.size );
    [[UIImage imageNamed:@"home.png"] drawInRect:rect];
    UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    if ([prevpage isEqualToString:@"0"]) {
        if ([appdelegate.listOfVerMenu count]>0) {
            [self.navigationItem setHidesBackButton:YES];
            [self.navigationItem.rightBarButtonItem setImage:image];
        } else {
            [self.navigationItem setHidesBackButton:YES];
            [self.navigationItem.rightBarButtonItem setImage:nil];
        }
    } else {
        [self.navigationItem setHidesBackButton:NO];
        [self.navigationItem.rightBarButtonItem setImage:image];
    }
    
    if ([pagedata isEqualToString:@"NoData"]) {
        //NSLog(@"pagedata :: %@", pagedata);
        [appdelegate.navController popToRootViewControllerAnimated:YES];
    }
    
}

/*** Push Notification 27Aug2015 ***/
-(void) viewDidDisappear:(BOOL)animated {
    
    if ([themepagenotification isEqualToString:@"Notification"]) {
        appdelegate.notify_actiontype = @"";
        appdelegate.notify_actionvalue = @"";
        appdelegate.notify_contentid = @"";
    }
    
    appdelegate.homeclicked = NO;
    [modalController dismissViewControllerAnimated:YES completion:nil];
    
    [super viewDidDisappear:animated];
    
}
/*** Push Notification 27Aug2015 ***/

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    
    return 0;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
   	return 44;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    return nil;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
}

- (IBAction)closePopover:(id)sender
{
    [view_popoverMain removeFromSuperview];
    //[view_popoverMain setHidden:NO];
}

- (IBAction)logIn:(id)sender
{
    NSString *nibFileName;
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone) {
        nibFileName = @"SettingViewController";
    } else {
        nibFileName = @"SettingViewController_ipad";
    }
    SettingViewController *s = [[SettingViewController alloc] initWithNibName:nibFileName bundle:nil];
    appdelegate.shouldReturnToPrevPage = YES;
    //LoyaltyViewController *l = [[LoyaltyViewController alloc] initWithNibName:@"LoyaltyViewController" bundle:nil];
    appdelegate.originVC = self;
    [appdelegate.navController pushViewController:s animated:NO];
    
    [view_popoverMain removeFromSuperview];
}

#pragma mark Connect To API
-(void) connectToServer:(NSString *) urlPath
{
    [self addLoadingView];
    
    NSString *urlAddress =[NSString stringWithFormat:@"%@", urlPath];
    
    /*NSURL *url = [NSURL URLWithString:urlAddress];
     
     NSURLRequest *requestObj = [NSURLRequest requestWithURL:url
     cachePolicy:NSURLRequestReloadIgnoringLocalAndRemoteCacheData
     timeoutInterval:30];
     
     NSURLConnection *theConnection=[[NSURLConnection alloc] initWithRequest:requestObj delegate:self];
     if (theConnection) {
     // Create the NSMutableData to hold the received data.
     // receivedData is an instance variable declared elsewhere.
     receivedData = [NSMutableData data];
     } else {
     // Inform the user that the connection failed.
     //[self displayAlert:@"Connection Error. Please try again later."];
     [self displayAlert:NSLocalizedString(@"MSG_CONNECTION_ERROR", nil)];
     [self removeLoadingView];
     } */
    [self getJsonResponse:urlAddress success:^(NSDictionary *responseDict) {
        
        //NSLog(@"%@",responseDict);
        [self successfunction:responseDict];
        
    } failure:^(NSError *error) {
        // release the connection, and the data object
        
        [self removeLoadingView];
        [self displayAlert:NSLocalizedString(@"MSG_CONNECTION_ERROR", nil)];
    }];
}

-(void)getJsonResponse : (NSString *)urlStr success : (void (^)(NSDictionary *responseDict))success failure:(void(^)(NSError* error))failure
{
    
    
    NSURLSession * session = [NSURLSession sharedSession];
    NSURL * url = [NSURL URLWithString: urlStr];
    
    
    // Asynchronously Api is hit here
    NSURLSessionDataTask * dataTask = [session dataTaskWithURL:url completionHandler:^(NSData *data, NSURLResponse *response, NSError *error)
                                       {
                                           
                                           //NSLog(@"%@",data);
                                           NSDictionary * json;
                                           if ([data length]>0) {
                                               json  = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
                                               //NSLog(@"%@",json);
                                               success(json);
                                           } else {
                                               json = nil;
                                               dispatch_async(dispatch_get_main_queue(), ^ {
                                                   [self removeLoadingView];
                                                   [self displayAlert:NSLocalizedString(@"MSG_CONNECTION_ERROR", nil)];
                                               });
                                           }
                                           
                                           
                                       }];
    
    [dataTask resume] ; // Executed First
    
    
}

-(void) successfunction:(NSDictionary *)responseDict {
    dispatch_async(dispatch_get_main_queue(), ^{
        //NSString *jsonString = [responseDict JSONRepresentation];
        [self removeLoadingView];
        
        if ([currentRequest isEqualToString:@"getAdmobSetting"])
        {
            listOfAdmob = [[NSMutableArray alloc]init];
            
            for(NSDictionary *row in responseDict)
            {
                //NSLog(@"[row] %@",[row objectForKey:@"id"]);
                NSMutableDictionary *mutableDic = [NSMutableDictionary dictionaryWithObjectsAndKeys:
                                                   [row objectForKey:@"id"],@"id",
                                                   [row objectForKey:@"top_id"],@"top_id",
                                                   [row objectForKey:@"user_id"],@"user_id",
                                                   [row objectForKey:@"app_id"],@"app_id",
                                                   [row objectForKey:@"and_type"],@"and_type",
                                                   [row objectForKey:@"and_unit_id"],@"and_unit_id",
                                                   [row objectForKey:@"ios_type"],@"ios_type",
                                                   [row objectForKey:@"ios_unit_id"],@"ios_unit_id",
                                                   nil];
                [listOfAdmob addObject:mutableDic];
            }
            //NSLog(@"listOfAdmob %@",listOfAdmob);
            if ([listOfAdmob count]>0) {
            // Initialize Google Mobile Ads SDK
            NSDictionary *data = [listOfAdmob objectAtIndex:0];
            NSString *appID = [data objectForKey:@"app_id"];
            [GADMobileAds configureWithApplicationID:appID];
            
            if ([attribute length]>0 && [admob_type isEqualToString:@"1"]) {
                [bannerView removeFromSuperview];
                bannerView=[[GADBannerView alloc]initWithFrame:CGRectMake(self.view.frame.size.width/2-160, self.view.frame.size.height-65, 320, 50)];
                //UIColor *background = [[UIColor alloc] initWithPatternImage:[UIImage imageNamed: @"behind_alert_view.png"]];
                //[view_popoverMain setBackgroundColor:background];
                //view_popoverMain.alpha = 0.5;
                //view_popoverMain.backgroundColor = [UIColor colorWithWhite:0.6 alpha:0];
                [self.view addSubview:bannerView];
                
                //NSLog(@"listOfAdmob:: %@", listOfAdmob);
                // Replace this ad unit ID with your own ad unit ID.
                NSDictionary *data = [listOfAdmob objectAtIndex:0];
                // Replace this ad unit ID with your own ad unit ID.
                //NSLog(@"ios_unit_id:: %@", [data objectForKey:@"ios_unit_id"]);
                bannerView.adUnitID = [data objectForKey:@"ios_unit_id"];
                bannerView.rootViewController = self;
                GADRequest *request = [GADRequest request];
                // Requests test ads on devices you specify. Your test device ID is printed to the console when
                // an ad request is made. GADBannerView automatically returns test ads when running on a
                // simulator.
                request.testDevices = @[ kGADSimulatorID ];
                //request.testDevices = @[ kGADSimulatorID, @"295a8039650af0f0650da93686f75cab" ];
                [bannerView loadRequest:request];
                [self.view bringSubviewToFront:bannerView];
                
                /*if (gameText == nil) {
                 [gameText removeFromSuperview];
                 gameText=[[UILabel alloc]initWithFrame:CGRectMake((self.view.frame.size.width/2)-(143/2), (self.view.frame.size.height-60)/2, 143, 29)];
                 //[gameText setBackgroundColor:[UIColor grayColor]];
                 [gameText setTextColor:[UIColor blackColor]];
                 [gameText setTextAlignment:NSTextAlignmentCenter];
                 //UIColor *background = [[UIColor alloc] initWithPatternImage:[UIImage imageNamed: @"behind_alert_view.png"]];
                 //[view_popoverMain setBackgroundColor:background];
                 //view_popoverMain.alpha = 0.5;
                 //view_popoverMain.backgroundColor = [UIColor colorWithWhite:0.6 alpha:0];
                 [self.view addSubview:gameText];
                 
                 [playAgainButton removeFromSuperview];
                 playAgainButton = [UIButton buttonWithType:UIButtonTypeCustom];
                 playAgainButton.frame = CGRectMake(self.view.frame.size.width/2-50, ((self.view.frame.size.height-60)/2)+40, 109, 41);
                 //[playAgainButton setBackgroundColor:[UIColor grayColor]];
                 [playAgainButton setTitle:@"Play Again" forState:UIControlStateNormal];
                 [playAgainButton setTitleColor:[UIColor blueColor] forState:UIControlStateNormal];
                 [playAgainButton addTarget:self
                 action:@selector(playAgain:)
                 forControlEvents:UIControlEventTouchUpInside];
                 playAgainButton.titleLabel.font = [UIFont systemFontOfSize:15];
                 //view_popoverMain.alpha = 0.5;
                 //view_popoverMain.backgroundColor = [UIColor colorWithWhite:0.6 alpha:0];
                 [self.view addSubview:playAgainButton];
                 [self.view bringSubviewToFront:playAgainButton];
                 }*/
                /* Rewarded video
                 if (gameText == nil) {
                 [gameText removeFromSuperview];
                 gameText=[[UILabel alloc]initWithFrame:CGRectMake((self.view.frame.size.width/2)-(143/2), (self.view.frame.size.height-60)/2, 143, 29)];
                 //[gameText setBackgroundColor:[UIColor grayColor]];
                 [gameText setTextColor:[UIColor blackColor]];
                 [gameText setTextAlignment:NSTextAlignmentCenter];
                 //UIColor *background = [[UIColor alloc] initWithPatternImage:[UIImage imageNamed: @"behind_alert_view.png"]];
                 //[view_popoverMain setBackgroundColor:background];
                 //view_popoverMain.alpha = 0.5;
                 //view_popoverMain.backgroundColor = [UIColor colorWithWhite:0.6 alpha:0];
                 [self.view addSubview:gameText];
                 
                 [playAgainButton removeFromSuperview];
                 playAgainButton = [UIButton buttonWithType:UIButtonTypeCustom];
                 playAgainButton.frame = CGRectMake(self.view.frame.size.width/2-50, ((self.view.frame.size.height-60)/2)+40, 109, 41);
                 //[playAgainButton setBackgroundColor:[UIColor grayColor]];
                 [playAgainButton setTitle:@"Play Again" forState:UIControlStateNormal];
                 [playAgainButton setTitleColor:[UIColor blueColor] forState:UIControlStateNormal];
                 [playAgainButton addTarget:self
                 action:@selector(playAgain:)
                 forControlEvents:UIControlEventTouchUpInside];
                 playAgainButton.titleLabel.font = [UIFont systemFontOfSize:15];
                 //view_popoverMain.alpha = 0.5;
                 //view_popoverMain.backgroundColor = [UIColor colorWithWhite:0.6 alpha:0];
                 [self.view addSubview:playAgainButton];
                 [self.view bringSubviewToFront:playAgainButton];
                 
                 [showVideoButton removeFromSuperview];
                 showVideoButton = [UIButton buttonWithType:UIButtonTypeCustom];
                 showVideoButton.frame = CGRectMake((self.view.frame.size.width/2)-(241/2), ((self.view.frame.size.height-60)/2)+100, 241, 30);
                 [showVideoButton setTitleColor:[UIColor blueColor] forState:UIControlStateNormal];
                 //[showVideoButton setBackgroundColor:[UIColor grayColor]];
                 [showVideoButton setTitle:@"Watch Video for 10 additional coins" forState:UIControlStateNormal];
                 [showVideoButton addTarget:self
                 action:@selector(showVideo:)
                 forControlEvents:UIControlEventTouchUpInside];
                 showVideoButton.titleLabel.font = [UIFont systemFontOfSize:15];
                 //view_popoverMain.alpha = 0.5;
                 //view_popoverMain.backgroundColor = [UIColor colorWithWhite:0.6 alpha:0];
                 [self.view addSubview:showVideoButton];
                 [self.view bringSubviewToFront:showVideoButton];
                 
                 [coinCountLabel removeFromSuperview];
                 coinCountLabel=[[UILabel alloc]initWithFrame:CGRectMake(10, self.view.frame.size.height-65, 63, 21)];
                 //[coinCountLabel setBackgroundColor:[UIColor grayColor]];
                 [coinCountLabel setTextColor:[UIColor blackColor]];
                 //UIColor *background = [[UIColor alloc] initWithPatternImage:[UIImage imageNamed: @"behind_alert_view.png"]];
                 //[view_popoverMain setBackgroundColor:background];
                 //view_popoverMain.alpha = 0.5;
                 //view_popoverMain.backgroundColor = [UIColor colorWithWhite:0.6 alpha:0];
                 [self.view addSubview:coinCountLabel];
                 } */
            }
            }
        }
        
    });
    
}

#pragma mark LoadingView
-(void) addLoadingView
{
    loadingView = [LoadingView loadingViewInView:self.view];
    
}

-(void) removeLoadingView
{
    [loadingView removeView];
}

#pragma mark displayAlert
-(void) displayAlert: (NSString *) stralert {
    NSString *str = [[NSString alloc] initWithFormat:@"%@", stralert];
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:appdelegate.iconname
                                                    message:str
                                                   delegate:self
                                          cancelButtonTitle:NSLocalizedString(@"done", nil)
                                          otherButtonTitles:nil];
    
    [alert show];
    str = nil;
    //[str release];
    //[alert release];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
