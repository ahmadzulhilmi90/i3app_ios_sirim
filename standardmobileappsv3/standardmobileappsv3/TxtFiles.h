//
//  TxtFiles.h
//  standardmobileappsv3
//
//  Created by M3Online on 4/25/14.
//  Copyright (c) 2014 M3Online. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface TxtFiles : NSObject {

NSFileManager *fileMgr;
NSString *homeDir;
NSString *filename;
NSString *filepath;



}


@property(nonatomic,retain) NSFileManager *fileMgr;
@property(nonatomic,retain) NSString *homeDir;
@property(nonatomic,retain) NSString *filename;
@property(nonatomic,retain) NSString *filepath;

-(NSString *) GetDocumentDirectory;
-(void)WriteToStringFile:(NSMutableString *)textToWrite andsetFileName:(NSString *)setFilename;
-(NSString *) readFromFile:(NSString *)setFilename;
-(NSString *) setFilenameuser;
-(NSString *) setFilenamethemepage;
-(NSString *) setFilenamethemeobject;
-(NSString *) setFilenamethemeplugin;
-(NSString *) setFilenamethemecolor;
-(NSString *) setFilenameappsetup;
-(NSString *) setFilenamecontentcategory;
-(NSString *) setFilenamecontents;
-(NSString *) setFilenameibeacondevice;
-(NSString *) setFilenameibeaconcontent;
-(NSString *) setFilenameibeaconxref;
-(NSString *) setFilenamewarranty;

@end
