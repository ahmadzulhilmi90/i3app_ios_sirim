//
//  TxtFiles.m
//  standardmobileappsv3
//
//  Created by M3Online on 4/25/14.
//  Copyright (c) 2014 M3Online. All rights reserved.
//

#import "TxtFiles.h"

@implementation TxtFiles
@synthesize fileMgr;
@synthesize homeDir;
@synthesize filename;
@synthesize filepath;


-(NSString *) setFilenameuser{
    filename = @"user.txt";
    
    return filename;
}

-(NSString *) setFilenamethemepage{
    filename = @"themepage.txt";
    
    return filename;
}

-(NSString *) setFilenamethemeobject{
    filename = @"themeobject.txt";
    
    return filename;
}

-(NSString *) setFilenamethemeplugin{
    filename = @"themeplugin.txt";
    
    return filename;
}

-(NSString *) setFilenamethemecolor{
    filename = @"themecolor.txt";
    
    return filename;
}

-(NSString *) setFilenameappsetup{
    filename = @"appsetup.txt";
    
    return filename;
}

-(NSString *) setFilenamecontentcategory{
    filename = @"contentcategory.txt";
    
    return filename;
}

-(NSString *) setFilenamecontents{
    filename = @"contents.txt";
    
    return filename;
}

-(NSString *) setFilenameibeacondevice{
    filename = @"ibeacondevice.txt";
    
    return filename;
}

-(NSString *) setFilenameibeaconcontent{
    filename = @"ibeaconcontent.txt";
    
    return filename;
}

-(NSString *) setFilenameibeaconxref{
    filename = @"ibeaconxref.txt";
    
    return filename;
}

-(NSString *) setFilenamewarranty{
    filename = @"warranty.txt";
    
    return filename;
}

/*
 Get a handle on the directory where to write and read our files. If
 it doesn't exist, it will be created.
 */

-(NSString *)GetDocumentDirectory{
    fileMgr = [NSFileManager defaultManager];
    homeDir = [NSHomeDirectory() stringByAppendingPathComponent:@"Documents"];
    
    return homeDir;
}


/*Create a new file*/
-(void)WriteToStringFile:(NSMutableString *)textToWrite andsetFileName:(NSString *)setFilename{
    filepath = [[NSString alloc] init];
    NSError *err;
    
    filepath = [self.GetDocumentDirectory stringByAppendingPathComponent:setFilename];
    
    BOOL ok = [textToWrite writeToFile:filepath atomically:YES encoding:NSUTF8StringEncoding error:&err];
    
    if (!ok) {
        NSLog(@"Error writing file at %@\n%@",
              filepath, [err localizedFailureReason]);
    }
    
}
/*
 Read the contents from file
 */
-(NSString *) readFromFile:(NSString *)setFilename
{
    filepath = [[NSString alloc] init];
    NSError *error;
    //NSString *title;
    filepath = [self.GetDocumentDirectory stringByAppendingPathComponent:setFilename];
    //NSLog(@"filepath: %@", filepath);
    NSString *txtInFile = [[NSString alloc] initWithContentsOfFile:filepath encoding:NSUTF8StringEncoding error:&error];
    
    if(!txtInFile)
    {
        /*UIAlertView *tellErr = [[UIAlertView alloc] initWithTitle:title message:@"Unable to get text from file." delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [tellErr show]; */
    }
    return txtInFile;
}

@end
