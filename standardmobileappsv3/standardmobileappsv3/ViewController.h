//
//  ViewController.h
//  standardmobileappsv3
//
//  Created by M3Online on 4/24/14.
//  Copyright (c) 2014 M3Online. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreLocation/CoreLocation.h>
#import <MessageUI/MessageUI.h>
#import <SDWebImage/UIButton+WebCache.h>
#import <SDWebImage/UIImageView+WebCache.h>
#import "LoadingView.h"

@interface ViewController : UIViewController <CLLocationManagerDelegate/*, NSURLConnectionDelegate*/> {
    
@private
    LoadingView *loadingView;
    NSString *currentRequestMenu;
    NSMutableArray *listOfObject;
    NSDictionary *dicObject;
    NSDictionary *dicImages;
    
    int current_idx;
    
    //int beaconFoundTimer;
    BOOL hasDisplayedNotification;
    
    BOOL enterbackground;
    
    NSDictionary *dicVerMenu;
    
}

@property (strong, nonatomic) NSString *currentRequestMenu;
@property (nonatomic, strong) UIImageView *myImageView;
@property (nonatomic, strong) NSMutableArray *listOfObject;

@property(nonatomic, strong)NSMutableArray *galleryImages; //Array holding the image file paths

@property (nonatomic, strong) UITableView *GridtableView;

@property (strong, nonatomic) NSMutableArray *arrayIBeacon;
@property (strong, nonatomic) NSMutableArray *arrayIBeaconList;
@property (strong, nonatomic) CLBeaconRegion *myBeaconRegion;
@property (strong, nonatomic) CLBeaconRegion *myBeaconRegion2;
@property (strong, nonatomic) CLBeaconRegion *myBeaconRegion3;
@property (strong, nonatomic) CLBeaconRegion *myBeaconRegion4;
@property (strong, nonatomic) CLBeaconRegion *myBeaconRegion5;

@property (strong, nonatomic) IBOutlet UIView *view_popoverMain;
@property (strong, nonatomic) IBOutlet UIButton *imgView_beaconPromo;

//@property (assign, nonatomic) int beaconFoundTimer;
@property (assign, nonatomic) int current_idx;
@property (assign, nonatomic) BOOL hasDisplayedNotification;

//@property (nonatomic, retain) NSMutableData *receivedData;

@property (assign, nonatomic) BOOL enterbackground;

//@property (strong, nonatomic) UIButton *ibeaconbutton;

-(void) connectToServer:(NSString *) urlPath;
-(void) addLoadingView;
- (IBAction)onCloseBeaconPopUp:(id)sender;
- (IBAction)onViewBeaconPromoTapped:(id)sender;

-(void) displayBeaconPromoPopUp:(NSString *)imgURL;
//- (IBAction)backToRoot:(UIStoryboardSegue *)segue;


@end
