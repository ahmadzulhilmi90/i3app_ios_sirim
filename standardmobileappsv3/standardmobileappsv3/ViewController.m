//
//  ViewController.m
//  standardmobileappsv3
//
//  Created by M3Online on 4/24/14.
//  Copyright (c) 2014 M3Online. All rights reserved.
//

#import "ViewController.h"
#import <SystemConfiguration/SystemConfiguration.h>
#import "TxtFiles.h"
#import "SBJson.h"
#import "AppDelegate.h"
#import <sqlite3.h>
#import "SCNavControllerDelegate.h"
#import "NSString+HTML.h"
#import <QuartzCore/QuartzCore.h>
#import "ThemeViewController.h"
#import "Reachability.h"

@interface ViewController ()

@end

@implementation ViewController
UIAlertView *alert;
UIActivityIndicatorView *indicator;

__strong NSString *themecode;
__strong NSString *page_id;
__strong NSString *page_name;
__strong NSString *prevuuid;
__strong NSString *prevmajor;
__strong NSString *prevminor;

__strong NSString *appsetupdate;
__strong NSString *contentsdate;
__strong NSString *content_categorydate;
__strong NSString *ibeacon_contentdate;
__strong NSString *ibeacon_devicedate;
__strong NSString *ibeacon_xrefdate;
__strong NSString *theme_colordate;
__strong NSString *theme_objectdate;
__strong NSString *theme_object_plugindate;
__strong NSString *theme_pagedate;
//__strong NSString *warrantydate;

__strong NSDate *datetheme_page1;
__strong NSDate *datetheme_object1;
__strong NSDate *datetheme_object_plugin1;
__strong NSDate *datecontent_category1;
__strong NSDate *dateibeacon_content1;
__strong NSDate *dateibeacon_devicedate1;
__strong NSDate *dateibeacon_xrefdate1;
__strong NSDate *datecontentsdate1;
__strong NSDate *dateappsetupdate1;
__strong NSDate *datetheme_colordate1;

__strong NSString *tqmsg;
__strong NSString *tqmsgtitle;

@synthesize currentRequestMenu, listOfObject/*, receivedData*/;
@synthesize arrayIBeacon, arrayIBeaconList, imgView_beaconPromo, /*ibeaconbutton, */view_popoverMain/*, beaconFoundTimer*/, current_idx, hasDisplayedNotification, enterbackground;

NSArray *ibeacon;
CLBeacon *foundBeacon;
BOOL hasBeacon;
//BOOL beaconButtonHasBeenAdded;
int beaconNotFoundTimer;

static NSString * uuid_ibeacon = @"B9407F30-F5F8-466E-AFF9-25556B57FE6D";

AppDelegate *appdelegate;

- (UIColor *) colorWithHexString:(NSString *)hexString {
    NSString *colorString = [[hexString stringByReplacingOccurrencesOfString: @"#" withString: @""] uppercaseString];
    CGFloat alpha, red, blue, green;
    alpha = 1.0f;
    red   = [self colorComponentFrom: colorString start: 0 length: 2];
    green = [self colorComponentFrom: colorString start: 2 length: 2];
    blue  = [self colorComponentFrom: colorString start: 4 length: 2];
    return [UIColor colorWithRed: red green: green blue: blue alpha: alpha];
}

- (CGFloat) colorComponentFrom:(NSString *)string start:(NSUInteger)start length:(NSUInteger)length {
    NSString *substring = [string substringWithRange: NSMakeRange(start, length)];
    NSString *fullHex = length == 2 ? substring : [NSString stringWithFormat: @"%@%@", substring, substring];
    unsigned hexComponent;
    [[NSScanner scannerWithString: fullHex] scanHexInt: &hexComponent];
    return hexComponent / 255.0;
}

-(id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super initWithCoder:aDecoder];
    if(self) {
        //_navDelegate = [SCNavControllerDelegate new];
        //self.delegate = _navDelegate;
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.view.backgroundColor = [UIColor clearColor];
	// Do any additional setup after loading the view, typically from a nib.
    
    [[NSNotificationCenter defaultCenter]addObserver:self
                                            selector:@selector(updateStuff:)
                                                name:UIApplicationDidEnterBackgroundNotification
                                              object:nil];
    [[NSNotificationCenter defaultCenter]addObserver:self
                                            selector:@selector(updateStuff:)
                                                name:UIApplicationWillEnterForegroundNotification
                                              object:nil];
    /*[[NSNotificationCenter defaultCenter]addObserver:self
                                            selector:@selector(updateStuff:)
                                                name:UIApplicationWillResignActiveNotification
                                              object:nil]; */
    /*[[NSNotificationCenter defaultCenter]addObserver:self
                                            selector:@selector(updateStuff:)
                                                name:UIApplicationWillTerminateNotification
                                              object:nil]; */
    
    appdelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];

    self.navigationItem.backBarButtonItem = [[UIBarButtonItem alloc]initWithTitle:@"" style:UIBarButtonItemStylePlain target:nil action:nil];
}

-(void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    
    //NSLog(@"viewDidAppearNotification");
    [self updateStuff:NO];
    
    if (!appdelegate.firsttime) [self processdata:YES];
}

-(void) updateStuff: (BOOL)enterbgnoti {
    if (!enterbgnoti && appdelegate.firsttime) {
        enterbackground = enterbgnoti;
    } else {
        enterbackground = YES;
    }
    if (appdelegate.showloading) {
        alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"loading", nil) message:nil delegate:self cancelButtonTitle:nil otherButtonTitles: nil];
        
        //a simple activity indicator:
        UIActivityIndicatorView *activityIndicator = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
        activityIndicator.frame= CGRectMake(50, 10, 37, 37);
        activityIndicator.autoresizingMask = UIViewAutoresizingFlexibleTopMargin | UIViewAutoresizingFlexibleRightMargin | UIViewAutoresizingFlexibleBottomMargin | UIViewAutoresizingFlexibleLeftMargin;
        [activityIndicator startAnimating];
        
        //the magic line below,
        //we associate the activity indicator to the alert view: (addSubview is not used)
        [alert setValue:activityIndicator forKey:@"accessoryView"];
        
        [alert show];
    
        // Adjust the indicator so it is up a few pixels from the bottom of the alert
        [UIView beginAnimations:nil context:nil];
        [UIView setAnimationDuration:0.0];
        [UIView setAnimationDelay:0.0];
        [UIView setAnimationTransition:UIViewAnimationTransitionNone forView:alert cache:YES];
        [UIView setAnimationDelegate:self];
        [UIView setAnimationDidStopSelector:@selector(startupAnimationDoneIndicatorLoadPage:finished:context:)];
        alert.alpha = 0.5;
        [UIView commitAnimations];
        
    } else {
        [self loadAllData];
    }
}

-(void) processdata :(BOOL)animatehome {
    //AppDelegate *appdelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    sqlite3 *database;
    __strong NSString *dataavai;
    
    themecode = @"";
    
    if(sqlite3_open([appdelegate.databasePath UTF8String], &database) == SQLITE_OK) {
        
        NSString *theme_color;
        NSString *icon_name;
        theme_color = @"";
        icon_name = @"";
        NSString *tc_color;
        NSString *tc_color1;
        NSString *titlebar_img;
        titlebar_img = @"";
        NSString *showtitle;
        NSString *background;
        background = @"";
        // Setup the SQL Statement and compile it for faster access
        NSString *appsetup = [[NSString alloc] initWithFormat:@"SELECT * from app_setup"];
        //NSLog(@"appsetup:: %@", appsetup);
        const char *sqlStatement0 = [appsetup UTF8String];
        sqlite3_stmt *compiledStatement0;
        if(sqlite3_prepare_v2(database, sqlStatement0, -1, &compiledStatement0, NULL) == SQLITE_OK) {
            //NSLog(@"temp11:: %@", temp);
            while(sqlite3_step(compiledStatement0) == SQLITE_ROW) {
                theme_color = [NSString stringWithUTF8String:(char *)sqlite3_column_text(compiledStatement0, 6)];
                icon_name = [NSString stringWithUTF8String:(char *)sqlite3_column_text(compiledStatement0, 1)];
                titlebar_img = [NSString stringWithUTF8String:(char *)sqlite3_column_text(compiledStatement0, 13)];
                showtitle = [NSString stringWithUTF8String:(char *)sqlite3_column_text(compiledStatement0, 14)];
                background = [NSString stringWithUTF8String:(char *)sqlite3_column_text(compiledStatement0, 15)];
                //NSLog(@"background:: %@", background);
                
            }
        }
        appdelegate.iconname = icon_name;
        self.title = appdelegate.iconname;
        
        if (![background isEqualToString:@""]) {
            appdelegate.backgroundT = background;
        }
        
        //NSLog(@"appdelegate.backgroundTMAin:: %@", appdelegate.backgroundT);
        
        // Release the compiled statement from memory
        sqlite3_finalize(compiledStatement0);
        
        //NSLog(@"theme_color:: %@", theme_color);
        if (![theme_color isEqualToString:@""]) {
            
            // Setup the SQL Statement and compile it for faster access
            NSString *themecolorcode = [[NSString alloc] initWithFormat:@"SELECT * from theme_color where name='%@'", theme_color];
            //NSLog(@"themecolorcode:: %@", themecolorcode);
            const char *sqlStatementtc = [themecolorcode UTF8String];
            sqlite3_stmt *compiledStatementtc;
            if(sqlite3_prepare_v2(database, sqlStatementtc, -1, &compiledStatementtc, NULL) == SQLITE_OK) {
                //NSLog(@"temp11:: %@", temp);
                while(sqlite3_step(compiledStatementtc) == SQLITE_ROW) {
                    tc_color = [NSString stringWithUTF8String:(char *)sqlite3_column_text(compiledStatementtc, 2)];
                    tc_color1 = [NSString stringWithUTF8String:(char *)sqlite3_column_text(compiledStatementtc, 3)];
                    appdelegate.themeColorCode = tc_color;
                    appdelegate.themeColorCode1 = tc_color1;
                }
            }
            // Release the compiled statement from memory
            sqlite3_finalize(compiledStatementtc);
            
            //NSLog(@"tc_color:: %@", tc_color);
            
            //maggie 19 Aug 2015 For Top Bar Background Image
            //bar_background.jpeg / radar.png
            if (![titlebar_img isEqualToString:@""]) {
                NSURL *imageURL = [NSURL URLWithString:titlebar_img];
                NSData *imageData = [NSData dataWithContentsOfURL:imageURL];
                UIImage *imgbarBg;
                //if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone) {
                    // For iPhone
                     CGRect rectmyImage;
                     //if (@available(iOS 11.0, *)) {
                        //UIWindow *mainWindow = [[[UIApplication sharedApplication] delegate] window];
                        //if (mainWindow.safeAreaInsets.top > 0.0) {
                            //rectmyImage = CGRectMake(0,15,self.view.frame.size.width,90);
                        //} else {
                            rectmyImage = CGRectMake(0,0,self.view.frame.size.width,65);
                        //}
                     //}
                     UIGraphicsBeginImageContext( rectmyImage.size );
                     [[UIImage imageWithData:imageData] drawInRect:rectmyImage];
                     UIImage *picture1BarBg = UIGraphicsGetImageFromCurrentImageContext();
                     UIGraphicsEndImageContext();
                     NSData *imageDataBarBg = UIImagePNGRepresentation(picture1BarBg);
                     imgbarBg=[UIImage imageWithData:imageDataBarBg];
                /*} else {
                    float heightToWidthRatio = 90 / 540;
                    float scaleFactor = 1;
                    if(heightToWidthRatio > 0) {
                        scaleFactor = 1;
                    } else {
                        scaleFactor = self.view.frame.size.width / 540;
                    }
                
                    CGSize newSize2 = CGSizeMake(self.view.frame.size.width, 60);
                    newSize2.width = 540 * scaleFactor;
                    newSize2.height = 90 * scaleFactor;
                
                    UIGraphicsBeginImageContext(newSize2);
                    [[UIImage imageWithData:imageData] drawInRect:CGRectMake(0,0,newSize2.width,newSize2.height)];
                    imgbarBg = UIGraphicsGetImageFromCurrentImageContext();
                    UIGraphicsEndImageContext();
                } */
                /*UIGraphicsBeginImageContextWithOptions(CGSizeMake(self.view.frame.size.width, 65), YES, [UIScreen mainScreen].scale);
                [[UIImage imageWithData:imageData] drawInRect:CGRectMake(0,0,self.view.frame.size.width,65)];
                UIImage* newImage = UIGraphicsGetImageFromCurrentImageContext();
                UIGraphicsEndImageContext(); */
                [appdelegate.navController.navigationBar setBackgroundImage:imgbarBg forBarMetrics:UIBarMetricsDefault];
                
                //if (imageData == nil) {
                    appdelegate.navController.navigationBar.barTintColor = [self colorWithHexString:tc_color];
                //}
            } else {
                appdelegate.navController.navigationBar.barTintColor = [self colorWithHexString:tc_color];
            }
            
            //NSLog(@"tc_color%@", tc_color);
            NSString *tc_colorStr = [NSString stringWithFormat:@"%@", [self colorWithHexString:tc_color]];
            NSArray *myArray = [tc_colorStr componentsSeparatedByCharactersInSet:[NSCharacterSet characterSetWithCharactersInString:@" "]];
            //NSLog(@"myArray: %@", myArray);
            if ([appdelegate.setbartextcolor length]>0) {
                if ([appdelegate.setbartextcolor isEqualToString:@"white"]) {
                    appdelegate.navController.navigationBar.tintColor = [UIColor whiteColor];
                } else {
                    appdelegate.navController.navigationBar.tintColor = [UIColor blackColor];
                }
                if ([showtitle isEqualToString:@"0"]) {
                    [appdelegate.navController.navigationBar setTitleTextAttributes:@{NSForegroundColorAttributeName : [UIColor clearColor]}];
                } else {
                    if ([appdelegate.setbartextcolor isEqualToString:@"white"]) {
                        [appdelegate.navController.navigationBar setTitleTextAttributes:@{NSForegroundColorAttributeName : [UIColor whiteColor]}];
                    } else {
                        [appdelegate.navController.navigationBar setTitleTextAttributes:@{NSForegroundColorAttributeName : [UIColor blackColor]}];
                    }
                }
            } else {
                if (round([[myArray objectAtIndex:1] floatValue]*255)>180 && round([[myArray objectAtIndex:2] floatValue]*255)>180 && round([[myArray objectAtIndex:3] floatValue]*255)>180) {
                    appdelegate.navController.navigationBar.tintColor = [UIColor blackColor];
                } else {
                    //if (round([[myArray objectAtIndex:1] floatValue]*255)==0 && round([[myArray objectAtIndex:2] floatValue]*255)==0 && round([[myArray objectAtIndex:3] floatValue]*255)==0) {
                        //appdelegate.navController.navigationBar.tintColor = [UIColor blackColor];
                    //} else {
                        appdelegate.navController.navigationBar.tintColor = [UIColor whiteColor];
                    //}
                }
                if ([showtitle isEqualToString:@"0"]) {
                    [appdelegate.navController.navigationBar setTitleTextAttributes:@{NSForegroundColorAttributeName : [UIColor clearColor]}];
                } else {
                    if (round([[myArray objectAtIndex:1] floatValue]*255)>180 && round([[myArray objectAtIndex:2] floatValue]*255)>180 && round([[myArray objectAtIndex:3] floatValue]*255)>180) {
                        [appdelegate.navController.navigationBar setTitleTextAttributes:@{NSForegroundColorAttributeName : [UIColor blackColor]}];
                    } else {
                        //if (round([[myArray objectAtIndex:1] floatValue]*255)==0 && round([[myArray objectAtIndex:2] floatValue]*255)==0 && round([[myArray objectAtIndex:3] floatValue]*255)==0) {
                            //[appdelegate.navController.navigationBar setTitleTextAttributes:@{NSForegroundColorAttributeName : [UIColor blackColor]}];
                        //} else {
                            [appdelegate.navController.navigationBar setTitleTextAttributes:@{NSForegroundColorAttributeName : [UIColor whiteColor]}];
                        //}
                    }
                }
            }
            appdelegate.navController.navigationBar.translucent = NO;
            
            // Setup the SQL Statement and compile it for faster access
            NSString *temp = [[NSString alloc] initWithFormat:@"SELECT * from theme_page WHERE prev_page = 0"];
            //NSLog(@"temp:: %@", temp);
            const char *sqlStatement = [temp UTF8String];
            sqlite3_stmt *compiledStatement;
            if(sqlite3_prepare_v2(database, sqlStatement, -1, &compiledStatement, NULL) == SQLITE_OK) {
                //NSLog(@"temp11:: %@", temp);
                while(sqlite3_step(compiledStatement) == SQLITE_ROW) {
                    //NSLog(@"temp22:: %@", temp);
                    page_id = [NSString stringWithUTF8String:(char *)sqlite3_column_text(compiledStatement, 0)];
                    themecode = [NSString stringWithUTF8String:(char *)sqlite3_column_text(compiledStatement, 1)];
                    page_name = [NSString stringWithUTF8String:(char *)sqlite3_column_text(compiledStatement, 2)];
                }
            }
            // Release the compiled statement from memory
            sqlite3_finalize(compiledStatement);
            
            appdelegate.homepage = page_name;
            dataavai = @"";
            
        } else {
            //[self alertStatus:@"Unable load data." :@"No Internet Connection"];
            dataavai = @"NoData";
        }
        theme_color =nil;
        tc_color =nil;
        tc_color1 =nil;
        
        //if (appdelegate.firsttime) {
        NSDictionary *dictionary;
        NSString *ibeaconsql = [[NSString alloc] initWithFormat:@"SELECT a.*, b.welcome_msg, b.welcome_img, b.page_id FROM ibeacon_device a join ibeacon_xref r ON r.device_id=a.id join ibeacon_content b ON b.id=r.content_id WHERE a.user_id='%@' ", appdelegate.user];
        //NSLog(@"ibeaconsql:: %@", ibeaconsql);
        //NSLog(@"themecode:: %@", themecode);
        const char *sqlStatementibeacon = [ibeaconsql UTF8String];
        sqlite3_stmt *compiledStatementibeacon;
        if(sqlite3_prepare_v2(database, sqlStatementibeacon, -1, &compiledStatementibeacon, NULL) == SQLITE_OK) {
            
            arrayIBeacon = [[NSMutableArray alloc] init];
            //listOfObject = [[NSMutableArray alloc] init];
            while(sqlite3_step(compiledStatementibeacon) == SQLITE_ROW) {
                
                NSString *beaconid = [NSString stringWithUTF8String:(char *)sqlite3_column_text(compiledStatementibeacon, 0)];
                NSString *beaconuuid = [NSString stringWithUTF8String:(char *)sqlite3_column_text(compiledStatementibeacon, 1)];
                NSString *beaconnotmsg = [NSString stringWithUTF8String:(char *)sqlite3_column_text(compiledStatementibeacon, 10)];
                NSString *beaconimage = [NSString stringWithUTF8String:(char *)sqlite3_column_text(compiledStatementibeacon, 11)];
                NSString *beaconuserid = [NSString stringWithUTF8String:(char *)sqlite3_column_text(compiledStatementibeacon, 6)];
                NSString *beaconpageid = [NSString stringWithUTF8String:(char *)sqlite3_column_text(compiledStatementibeacon, 12)];
                NSString *beaconmajor = [NSString stringWithUTF8String:(char *)sqlite3_column_text(compiledStatementibeacon, 2)];
                NSString *beaconminor = [NSString stringWithUTF8String:(char *)sqlite3_column_text(compiledStatementibeacon, 3)];
                NSString *beaconname = [NSString stringWithUTF8String:(char *)sqlite3_column_text(compiledStatementibeacon, 4)];
                NSString *beaconmessage = [NSString stringWithUTF8String:(char *)sqlite3_column_text(compiledStatementibeacon, 5)];
                
                beaconuuid = [beaconuuid stringByDecodingHTMLEntities];
                beaconuuid = [beaconuuid stringByReplacingOccurrencesOfString:@"''" withString:@"'"];
                beaconnotmsg = [beaconnotmsg stringByDecodingHTMLEntities];
                beaconnotmsg = [beaconnotmsg stringByReplacingOccurrencesOfString:@"''" withString:@"'"];
                beaconimage = [beaconimage stringByDecodingHTMLEntities];
                beaconimage = [beaconimage stringByReplacingOccurrencesOfString:@"''" withString:@"'"];
                beaconname = [beaconname stringByDecodingHTMLEntities];
                beaconname = [beaconname stringByReplacingOccurrencesOfString:@"''" withString:@"'"];
                beaconmessage = [beaconmessage stringByDecodingHTMLEntities];
                beaconmessage = [beaconmessage stringByReplacingOccurrencesOfString:@"''" withString:@"'"];
                
                dictionary = [NSDictionary dictionaryWithObjectsAndKeys:beaconid,@"id",beaconuuid,@"uuid",beaconnotmsg,@"notification_message",beaconimage,@"image",beaconuserid,@"user_id",beaconpageid,@"page_id", beaconmajor,@"major", beaconminor,@"minor", beaconname,@"name", beaconmessage,@"message",nil];
                [arrayIBeacon addObject:dictionary];
                //NSLog(@"dictionary: %@", dictionary);
                
                beaconid =nil;
                beaconuuid =nil;
                beaconnotmsg =nil;
                beaconimage =nil;
                beaconuserid =nil;
                beaconpageid =nil;
                beaconmajor =nil;
                beaconminor =nil;
                beaconname =nil;
                beaconmessage =nil;
            }
        }
        // Release the compiled statement from memory
        sqlite3_finalize(compiledStatementibeacon);
        //NSDictionary *dictionary = [response JSONValue];
        
        //NSLog(@"dictionarybeacon :%@ ", dictionary);
        NSDate* sourceDate = [NSDate date];
        
        NSTimeZone* sourceTimeZone = [NSTimeZone timeZoneWithAbbreviation:@"GMT"];
        NSTimeZone* destinationTimeZone = [NSTimeZone systemTimeZone];
        
        NSInteger sourceGMTOffset = [sourceTimeZone secondsFromGMTForDate:sourceDate];
        NSInteger destinationGMTOffset = [destinationTimeZone secondsFromGMTForDate:sourceDate];
        NSTimeInterval interval = destinationGMTOffset - sourceGMTOffset;
        
        NSDate* destinationDate = [[NSDate alloc] initWithTimeInterval:interval sinceDate:sourceDate];
        
        NSDictionary *dictionaryList;
        NSString *ibeaconsqllist = [[NSString alloc] initWithFormat:@"SELECT a.*, b.welcome_msg, b.welcome_img, b.page_id, b.limitperday, b.distance_range FROM ibeacon_device a join ibeacon_xref r ON r.device_id=a.id join ibeacon_content b ON b.id=r.content_id WHERE a.user_id='%@' and '%@' between start_date and end_date", appdelegate.user, destinationDate];
        //NSLog(@"ibeaconsqllist:: %@", ibeaconsqllist);
        //NSLog(@"themecode:: %@", themecode);
        const char *sqlStatementibeaconlist = [ibeaconsqllist UTF8String];
        sqlite3_stmt *compiledStatementibeaconlist;
        if(sqlite3_prepare_v2(database, sqlStatementibeaconlist, -1, &compiledStatementibeaconlist, NULL) == SQLITE_OK) {
            
            arrayIBeaconList = [[NSMutableArray alloc] init];
            //listOfObject = [[NSMutableArray alloc] init];
            while(sqlite3_step(compiledStatementibeaconlist) == SQLITE_ROW) {
                
                NSString *beaconid = [NSString stringWithUTF8String:(char *)sqlite3_column_text(compiledStatementibeaconlist, 0)];
                NSString *beaconuuid = [NSString stringWithUTF8String:(char *)sqlite3_column_text(compiledStatementibeaconlist, 1)];
                NSString *beaconnotmsg = [NSString stringWithUTF8String:(char *)sqlite3_column_text(compiledStatementibeaconlist, 10)];
                NSString *beaconimage = [NSString stringWithUTF8String:(char *)sqlite3_column_text(compiledStatementibeaconlist, 11)];
                NSString *beaconuserid = [NSString stringWithUTF8String:(char *)sqlite3_column_text(compiledStatementibeaconlist, 6)];
                NSString *beaconpageid = [NSString stringWithUTF8String:(char *)sqlite3_column_text(compiledStatementibeaconlist, 12)];
                NSString *beaconmajor = [NSString stringWithUTF8String:(char *)sqlite3_column_text(compiledStatementibeaconlist, 2)];
                NSString *beaconminor = [NSString stringWithUTF8String:(char *)sqlite3_column_text(compiledStatementibeaconlist, 3)];
                NSString *beaconname = [NSString stringWithUTF8String:(char *)sqlite3_column_text(compiledStatementibeaconlist, 4)];
                NSString *beaconmessage = [NSString stringWithUTF8String:(char *)sqlite3_column_text(compiledStatementibeaconlist, 5)];
                NSString *beaconlimitperday = [NSString stringWithUTF8String:(char *)sqlite3_column_text(compiledStatementibeaconlist, 13)];
                NSString *beacondistancerange = [NSString stringWithUTF8String:(char *)sqlite3_column_text(compiledStatementibeaconlist, 14)];
                
                beaconuuid = [beaconuuid stringByDecodingHTMLEntities];
                beaconuuid = [beaconuuid stringByReplacingOccurrencesOfString:@"''" withString:@"'"];
                beaconnotmsg = [beaconnotmsg stringByDecodingHTMLEntities];
                beaconnotmsg = [beaconnotmsg stringByReplacingOccurrencesOfString:@"''" withString:@"'"];
                beaconimage = [beaconimage stringByDecodingHTMLEntities];
                beaconimage = [beaconimage stringByReplacingOccurrencesOfString:@"''" withString:@"'"];
                beaconname = [beaconname stringByDecodingHTMLEntities];
                beaconname = [beaconname stringByReplacingOccurrencesOfString:@"''" withString:@"'"];
                beaconmessage = [beaconmessage stringByDecodingHTMLEntities];
                beaconmessage = [beaconmessage stringByReplacingOccurrencesOfString:@"''" withString:@"'"];
                
                dictionaryList = [NSDictionary dictionaryWithObjectsAndKeys:beaconid,@"id",beaconuuid,@"uuid",beaconnotmsg,@"notification_message",beaconimage,@"image",beaconuserid,@"user_id",beaconpageid,@"page_id", beaconmajor,@"major", beaconminor,@"minor", beaconname,@"name", beaconmessage,@"message", beaconlimitperday, @"limitperday", beacondistancerange, @"distance_range", nil];
                [arrayIBeaconList addObject:dictionaryList];
                //NSLog(@"dictionaryList: %@", dictionaryList);
                
                beaconid =nil;
                beaconuuid =nil;
                beaconnotmsg =nil;
                beaconimage =nil;
                beaconuserid =nil;
                beaconpageid =nil;
                beaconmajor =nil;
                beaconminor =nil;
                beaconname =nil;
                beaconmessage =nil;
            }
        }
        // Release the compiled statement from memory
        sqlite3_finalize(compiledStatementibeaconlist);
        
        NSString *vermenu_object = [[NSString alloc] initWithFormat:@"SELECT * from theme_object WHERE theme_code = 'MENU-VER' and object_type<>'0' Order by slot_code"];
        //NSLog(@"menu_object:: %@", menu_object);
        const char *sqlStatementVerMenu = [vermenu_object UTF8String];
        sqlite3_stmt *compiledStatementVerMenu;
        if(sqlite3_prepare_v2(database, sqlStatementVerMenu, -1, &compiledStatementVerMenu, NULL) == SQLITE_OK) {
            
            while(sqlite3_step(compiledStatementVerMenu) == SQLITE_ROW) {
                
                NSString *aidobj = [NSString stringWithUTF8String:(char *)sqlite3_column_text(compiledStatementVerMenu, 0)];
                NSString *athemecode = [NSString stringWithUTF8String:(char *)sqlite3_column_text(compiledStatementVerMenu, 1)];
                NSString *aslotcode = [NSString stringWithUTF8String:(char *)sqlite3_column_text(compiledStatementVerMenu, 2)];
                NSString *apageid = [NSString stringWithUTF8String:(char *)sqlite3_column_text(compiledStatementVerMenu, 3)];
                NSString *aobjcttype = [NSString stringWithUTF8String:(char *)sqlite3_column_text(compiledStatementVerMenu, 4)];
                NSString *aobjectvalue = [NSString stringWithUTF8String:(char *)sqlite3_column_text(compiledStatementVerMenu, 5)];
                NSString *aattribute = [NSString stringWithUTF8String:(char *)sqlite3_column_text(compiledStatementVerMenu, 6)];
                NSString *aactiontype = [NSString stringWithUTF8String:(char *)sqlite3_column_text(compiledStatementVerMenu, 7)];
                NSString *aactionvalue = [NSString stringWithUTF8String:(char *)sqlite3_column_text(compiledStatementVerMenu, 8)];
                NSString *aremark = [NSString stringWithUTF8String:(char *)sqlite3_column_text(compiledStatementVerMenu, 10)];
                
                aobjectvalue = [aobjectvalue stringByDecodingHTMLEntities];
                aremark = [aremark stringByDecodingHTMLEntities];
                aobjectvalue = [aobjectvalue stringByReplacingOccurrencesOfString:@"''" withString:@"'"];
                aremark = [aremark stringByReplacingOccurrencesOfString:@"''" withString:@"'"];
                
                dicVerMenu = [NSDictionary dictionaryWithObjectsAndKeys:aidobj,@"id",athemecode,@"theme_code",aslotcode,@"slot_code",apageid,@"page_id",aobjcttype,@"object_type",aobjectvalue,@"object_value",aattribute,@"attribute",aactiontype,@"action_type",aactionvalue,@"action_value",aremark,@"remark",nil];
                [appdelegate.listOfVerMenu addObject:dicVerMenu];
                
                aidobj =nil;
                athemecode =nil;
                aslotcode =nil;
                apageid =nil;
                aobjcttype =nil;
                aobjectvalue =nil;
                aattribute =nil;
                aactiontype =nil;
                aactionvalue =nil;
                aremark =nil;
                
            }
            
            //NSLog(@"listOfMenu:: %@", listOfMenu);
        }
        // Release the compiled statement from memory
        sqlite3_finalize(compiledStatementVerMenu);
        
        //NSLog(@"arrayIBeacon: %@", arrayIBeacon);
        
        //appdelegate.locationManager.delegate = self;
        //appdelegate.locationManager.desiredAccuracy = kCLLocationAccuracyKilometer;
        // A system version of 8 or greater is required to use requestAlwaysAuthorization]
        NSString *reqSysVer = @"8.0";
        NSString *currSysVer = [[UIDevice currentDevice] systemVersion];
        if ([currSysVer compare:reqSysVer options:NSNumericSearch] != NSOrderedAscending)
            //[appdelegate.locationManager requestAlwaysAuthorization];
        //[appdelegate.locationManager startUpdatingLocation];
        
        if ([arrayIBeaconList count] > 0)
            hasBeacon = YES;
        else
            hasBeacon = NO;
        
        int x=0;
        while (x < [arrayIBeacon count])
        {
            //NSLog(@"UUID from DB: %@",[[arrayIBeacon objectAtIndex:x] objectForKey:@"uuid"]);
            //NSLog(@"UUID declared: %@", uuid_ibeacon);
            
            if (x == 0)
            {
                NSUUID *uuid1 = [[NSUUID alloc] initWithUUIDString:[NSString stringWithFormat:@"%@",uuid_ibeacon]];
                // Setup a new region with that UUID and same identifier as the broadcasting beacon
                self.myBeaconRegion = [[CLBeaconRegion alloc ] initWithProximityUUID:uuid1
                                                                          identifier:[NSString stringWithFormat:@"%@",uuid_ibeacon]];
                self.myBeaconRegion.notifyEntryStateOnDisplay = YES;
                self.myBeaconRegion.notifyOnEntry = YES;
                
                
                // Tell location manager to start monitoring for the beacon region
                //[appdelegate.locationManager startMonitoringForRegion:self.myBeaconRegion];
                //[appdelegate.locationManager startRangingBeaconsInRegion:self.myBeaconRegion];
            }
            else if (x == 1)
            {
                NSUUID *uuid2 = [[NSUUID alloc] initWithUUIDString:[NSString stringWithFormat:@"%@",uuid_ibeacon]];
                // Setup a new region with that UUID and same identifier as the broadcasting beacon
                self.myBeaconRegion2 = [[CLBeaconRegion alloc ] initWithProximityUUID:uuid2
                                                                           identifier:[NSString stringWithFormat:@"%@",uuid_ibeacon]];
                self.myBeaconRegion2.notifyEntryStateOnDisplay = YES;
                self.myBeaconRegion2.notifyOnEntry = YES;
                
                // Tell location manager to start monitoring for the beacon region
                //[appdelegate.locationManager startMonitoringForRegion:self.myBeaconRegion2];
                //[appdelegate.locationManager startRangingBeaconsInRegion:self.myBeaconRegion2];
            }
            else if (x == 2)
            {
                NSUUID *uuid3 = [[NSUUID alloc] initWithUUIDString:[NSString stringWithFormat:@"%@",uuid_ibeacon]];
                // Setup a new region with that UUID and same identifier as the broadcasting beacon
                self.myBeaconRegion3 = [[CLBeaconRegion alloc ] initWithProximityUUID:uuid3
                                                                           identifier:[NSString stringWithFormat:@"%@",uuid_ibeacon]];
                self.myBeaconRegion3.notifyEntryStateOnDisplay = YES;
                self.myBeaconRegion3.notifyOnEntry = YES;
                
                // Tell location manager to start monitoring for the beacon region
                //[appdelegate.locationManager startMonitoringForRegion:self.myBeaconRegion3];
                //[appdelegate.locationManager startRangingBeaconsInRegion:self.myBeaconRegion3];
            }
            else if (x == 3)
            {
                NSUUID *uuid4 = [[NSUUID alloc] initWithUUIDString:[NSString stringWithFormat:@"%@",uuid_ibeacon]];
                // Setup a new region with that UUID and same identifier as the broadcasting beacon
                self.myBeaconRegion4 = [[CLBeaconRegion alloc ] initWithProximityUUID:uuid4
                                                                           identifier:[NSString stringWithFormat:@"%@",uuid_ibeacon]];
                self.myBeaconRegion4.notifyEntryStateOnDisplay = YES;
                self.myBeaconRegion4.notifyOnEntry = YES;
                
                // Tell location manager to start monitoring for the beacon region
                //[appdelegate.locationManager startMonitoringForRegion:self.myBeaconRegion4];
                //[appdelegate.locationManager startRangingBeaconsInRegion:self.myBeaconRegion4];
            }
            else if (x == 4)
            {
                NSUUID *uuid5 = [[NSUUID alloc] initWithUUIDString:[NSString stringWithFormat:@"%@",uuid_ibeacon]];
                // Setup a new region with that UUID and same identifier as the broadcasting beacon
                self.myBeaconRegion5 = [[CLBeaconRegion alloc ] initWithProximityUUID:uuid5
                                                                           identifier:[NSString stringWithFormat:@"%@",uuid_ibeacon]];
                self.myBeaconRegion5.notifyEntryStateOnDisplay = YES;
                self.myBeaconRegion5.notifyOnEntry = YES;
                
                // Tell location manager to start monitoring for the beacon region
                //[appdelegate.locationManager startMonitoringForRegion:self.myBeaconRegion5];
                //[appdelegate.locationManager startRangingBeaconsInRegion:self.myBeaconRegion5];
            }
            
            x += 1;
        }
        //} //firsttime ibeacon
    }
    sqlite3_close(database);
    
    currentRequestMenu = @"";
    if (![themecode isEqualToString:@""] && animatehome) {
        ThemeViewController *themeViewController = [[ThemeViewController alloc] init];
        themeViewController.page_id = page_id;
        themeViewController.pagedata = dataavai;
        themeViewController.themepagenotification = @"Normal"; /*** Push Notification 27Aug2015 ***/
        [appdelegate.navController pushViewController:themeViewController animated:NO];
        
    } //else {
        //[self alertStatus:@"Unable download data." :@"Error"];
    //}
}

-(void) dissmissIndicator {
    [alert dismissWithClickedButtonIndex:0 animated:NO];
}

-(void) goNextPage {
    
    //AppDelegate *appdelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    sqlite3 *database;
    NSString *themecodenextpage;
    NSString *page_idnextpage;
    NSString *page_namenextpage;
    
    page_idnextpage = @"";
    if(sqlite3_open([appdelegate.databasePath UTF8String], &database) == SQLITE_OK) {
        // Setup the SQL Statement and compile it for faster access
        NSString *temp = [[NSString alloc] initWithFormat:@"SELECT * from theme_page WHERE id = %@", appdelegate.nextPage];
        //NSLog(@"temp:: %@", temp);
        const char *sqlStatement = [temp UTF8String];
        sqlite3_stmt *compiledStatement;
        if(sqlite3_prepare_v2(database, sqlStatement, -1, &compiledStatement, NULL) == SQLITE_OK) {
            while(sqlite3_step(compiledStatement) == SQLITE_ROW) {
                page_idnextpage = [NSString stringWithUTF8String:(char *)sqlite3_column_text(compiledStatement, 0)];
                themecodenextpage = [NSString stringWithUTF8String:(char *)sqlite3_column_text(compiledStatement, 1)];
                page_namenextpage = [NSString stringWithUTF8String:(char *)sqlite3_column_text(compiledStatement, 2)];
                //prevpage = [NSString stringWithUTF8String:(char *)sqlite3_column_text(compiledStatement, 3)];
                
            }
        }
        // Release the compiled statement from memory
        sqlite3_finalize(compiledStatement);
    }
    sqlite3_close(database);
    
    NSString *urlAddress = [[NSString alloc] initWithFormat:@"%@point_setting.php?db=%@&userid=%@&udid=%@&cuid=%@", appdelegate.user_apipath, appdelegate.db, appdelegate.user, appdelegate.uuid, appdelegate.customer_id];
    currentRequestMenu = @"ibeaconCheckInSetting";
    [self connectToServer:urlAddress];
    
    if ([page_idnextpage length]>0) {
        ThemeViewController *themeViewController = [[ThemeViewController alloc] init];
        themeViewController.page_id = page_idnextpage;
        themeViewController.pagedata = @"";
        themeViewController.themepagenotification = @"Normal"; /*** Push Notification 27Aug2015 ***/
        [appdelegate.navController pushViewController:themeViewController animated:NO];
    }
}

- (void)startupAnimationDoneIndicator:(NSString *)animationID finished:(NSNumber *)finished context:(void *)context
{
    [self goNextPage];
    [self dissmissIndicator];
    alert =nil;
}

- (void)startupAnimationDoneIndicatorLoadPage:(NSString *)animationID finished:(NSNumber *)finished context:(void *)context
{
    //[self downloaddata:appdelegate.firsttime :@""];
    //dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
    [self loadAllData];
    //});
    
    //[self processdata];  //For Home page purpose: move to viewDidAppear
    [self dissmissIndicator];
    alert =nil;
}

- (void)loadAllData {
    if ([self isConnectionAvailable])
    {
        NSString *mobapp_id = @"";
        if ([appdelegate.db length] == 1) {
            mobapp_id = [NSString stringWithFormat:@"00%@", appdelegate.db];
        } else if ([appdelegate.db length] == 2) {
            mobapp_id = [NSString stringWithFormat:@"0%@", appdelegate.db];
        } else {
            mobapp_id = [NSString stringWithFormat:@"%@", appdelegate.db];
        }
        
        //appdelegate.user_apipath =[NSString stringWithFormat:@"http://app.m3online.com/app_%@/api/", mobapp_id];
        
        //NSLog(@"user: %@",appdelegate.user);
        //NSLog(@"user_apipath: %@",appdelegate.user_apipath);
        
        //NSLog(@"appdelegate.cs: %@",appdelegate.cs);
        NSString *urlAddress = [[NSString alloc] initWithFormat:@"%@check_date.php?db=%@&userid=%@&udid=%@&cuid=%@&device=ios&cs=%@", appdelegate.user_apipath, appdelegate.db, appdelegate.user, appdelegate.uuid, appdelegate.customer_id, appdelegate.cs];
        currentRequestMenu = @"checkdate";
        [self connectToServer:urlAddress];
        
        [self check_sqliteDate];
        //[self processdata];
        
    } else {
        [self downloaddata:appdelegate.firsttime :@"" :NO];
    }
}

-(void) checkdownloaddata:(NSString *)response :(BOOL)FailWithError {
    NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
    dateFormat.dateFormat = @"yyyy-MM-dd HH:mm:ss";
    __strong NSDate *datetheme_page = [dateFormat dateFromString:theme_pagedate];
    //NSLog(@"datetheme_objecttheme_objectdate: %@",theme_objectdate);
    __strong NSDate *datetheme_object = [dateFormat dateFromString:theme_objectdate];
    __strong NSDate *datetheme_object_plugin = [dateFormat dateFromString:theme_object_plugindate];
    __strong NSDate *datecontent_category = [dateFormat dateFromString:content_categorydate];
    __strong NSDate *dateibeacon_content = [dateFormat dateFromString:ibeacon_contentdate];
    __strong NSDate *dateibeacon_devicedate = [dateFormat dateFromString:ibeacon_devicedate];
    __strong NSDate *dateibeacon_xrefdate = [dateFormat dateFromString:ibeacon_xrefdate];
    __strong NSDate *datecontentsdate = [dateFormat dateFromString:contentsdate];
    __strong NSDate *dateappsetupdate = [dateFormat dateFromString:appsetupdate];
    __strong NSDate *datetheme_colordate = [dateFormat dateFromString:theme_colordate];
    
    [self downloaddata:appdelegate.firsttime :response :FailWithError];
    
    if ([currentRequestMenu isEqualToString:@"checkdate"]) {
        currentRequestMenu = @"themepage";
        if ([datetheme_page1 compare:datetheme_page] == NSOrderedDescending) {
            NSString *urlAddress1 = [[NSString alloc] initWithFormat:@"%@theme_page.php?db=%@&userid=%@&udid=%@&cuid=%@&device=ios&sc=%@", appdelegate.user_apipath, appdelegate.db, appdelegate.user, appdelegate.uuid, appdelegate.customer_id, appdelegate.cs];
            [self connectToServer:urlAddress1];
            //NSLog(@"urlAddress1: %@",urlAddress1);
        } else {
            [self checkdownloaddata:@"" :NO];
        }
    } else
    
    //NSLog(@"datetheme_object1: %@",datetheme_object1);
    //NSLog(@"datetheme_object: %@",datetheme_object);
    if ([currentRequestMenu isEqualToString:@"themepage"]) {
        currentRequestMenu = @"themeobject";
        if ([datetheme_object1 compare:datetheme_object] == NSOrderedDescending) {
            NSString *urlAddress2 = [[NSString alloc] initWithFormat:@"%@theme_object.php?db=%@&userid=%@&udid=%@&cuid=%@&device=ios&sc=%@", appdelegate.user_apipath, appdelegate.db, appdelegate.user, appdelegate.uuid, appdelegate.customer_id, appdelegate.cs];
            [self connectToServer:urlAddress2];
            //NSLog(@"urlAddress2: %@",urlAddress2);
        } else {
            [self checkdownloaddata:@"" :NO];
        }
    } else
    
    if ([currentRequestMenu isEqualToString:@"themeobject"]) {
        currentRequestMenu = @"themeplugin";
        if ([datetheme_object_plugin1 compare:datetheme_object_plugin] == NSOrderedDescending) {
            NSString *urlAddress3 = [[NSString alloc] initWithFormat:@"%@theme_object_plugin.php?db=%@&userid=%@&udid=%@&cuid=%@&device=ios&sc=%@", appdelegate.user_apipath, appdelegate.db, appdelegate.user, appdelegate.uuid, appdelegate.customer_id, appdelegate.cs];
            //NSLog(@"urlAddress3: %@",urlAddress3);
            [self connectToServer:urlAddress3];
        } else {
            [self checkdownloaddata:@"" :NO];
        }
    } else
    
    if ([currentRequestMenu isEqualToString:@"themeplugin"]) {
        currentRequestMenu = @"themecolor";
        if ([datetheme_colordate1 compare:datetheme_colordate] == NSOrderedDescending) {
            NSString *urlAddress4 = [[NSString alloc] initWithFormat:@"%@theme_color.php?db=%@&userid=%@&udid=%@&cuid=%@&device=ios&sc=%@", appdelegate.user_apipath, appdelegate.db, appdelegate.user, appdelegate.uuid, appdelegate.customer_id, appdelegate.cs];
            //
            //NSLog(@"urlAddress4: %@",urlAddress4);
            [self connectToServer:urlAddress4];
        } else {
            [self checkdownloaddata:@"" :NO];
        }
    } else
    
    //NSLog(@"dateappsetupdate1: %@",dateappsetupdate1);
    //NSLog(@"dateappsetupdate: %@",dateappsetupdate);
    if ([currentRequestMenu isEqualToString:@"themecolor"]) {
        currentRequestMenu = @"appsetup";
        if ([dateappsetupdate1 compare:dateappsetupdate] == NSOrderedDescending) {
            NSString *urlAddress5 = [[NSString alloc] initWithFormat:@"%@app_setup.php?db=%@&userid=%@&udid=%@&cuid=%@&device=ios&sc=%@", appdelegate.user_apipath, appdelegate.db, appdelegate.user, appdelegate.uuid, appdelegate.customer_id, appdelegate.cs];
            //NSLog(@"urlAddress5: %@", urlAddress5);
            [self connectToServer:urlAddress5];
        } else {
            [self checkdownloaddata:@"" :NO];
        }
    } else
    
    if ([currentRequestMenu isEqualToString:@"appsetup"]) {
        currentRequestMenu = @"contentcategory";
        if ([datecontent_category1 compare:datecontent_category] == NSOrderedDescending) {
            NSString *urlAddress6 = [[NSString alloc] initWithFormat:@"%@content_category.php?db=%@&userid=%@&udid=%@&cuid=%@&device=ios&sc=%@", appdelegate.user_apipath, appdelegate.db, appdelegate.user, appdelegate.uuid, appdelegate.customer_id, appdelegate.cs];
            //NSLog(@"urlAddress6: %@", urlAddress6);
            [self connectToServer:urlAddress6];
        } else {
            [self checkdownloaddata:@"" :NO];
        }
    } else
    
    //NSLog(@"datecontentsdate1: %@",datecontentsdate1);
    //NSLog(@"datecontentsdate: %@",datecontentsdate);
    if ([currentRequestMenu isEqualToString:@"contentcategory"]) {
        currentRequestMenu = @"contents";
        if ([datecontentsdate1 compare:datecontentsdate] == NSOrderedDescending) {
            NSString *urlAddress7 = [[NSString alloc] initWithFormat:@"%@contents.php?db=%@&userid=%@&udid=%@&cuid=%@&device=ios&sc=%@", appdelegate.user_apipath, appdelegate.db, appdelegate.user, appdelegate.uuid, appdelegate.customer_id, appdelegate.cs];
            //NSLog(@"urlAddress7: %@", urlAddress7);
            [self connectToServer:urlAddress7];
        } else {
            [self checkdownloaddata:@"" :NO];
        }
    } else
    
    if ([currentRequestMenu isEqualToString:@"contents"]) {
        currentRequestMenu = @"ibeaconxref";
        if ([dateibeacon_xrefdate1 compare:dateibeacon_xrefdate] == NSOrderedDescending) {
            NSString *urlAddress8 = [[NSString alloc] initWithFormat:@"%@ibeacon_xref.php?db=%@&userid=%@&udid=%@&cuid=%@&device=ios&sc=%@", appdelegate.user_apipath, appdelegate.db, appdelegate.user, appdelegate.uuid, appdelegate.customer_id, appdelegate.cs];
            //NSLog(@"urlAddress8: %@", urlAddress8);
            [self connectToServer:urlAddress8];
        } else {
            [self checkdownloaddata:@"" :NO];
        }
    } else
    
    if ([currentRequestMenu isEqualToString:@"ibeaconxref"]) {
        currentRequestMenu = @"ibeacondevice";
        if ([dateibeacon_devicedate1 compare:dateibeacon_devicedate] == NSOrderedDescending) {
            NSString *urlAddress9 = [[NSString alloc] initWithFormat:@"%@ibeacon_device.php?db=%@&userid=%@&udid=%@&cuid=%@&device=ios&sc=%@", appdelegate.user_apipath, appdelegate.db, appdelegate.user, appdelegate.uuid, appdelegate.customer_id, appdelegate.cs];
            //NSLog(@"urlAddress9: %@", urlAddress9);
            [self connectToServer:urlAddress9];
        } else {
            [self checkdownloaddata:@"" :NO];
        }
    } else
    
    if ([currentRequestMenu isEqualToString:@"ibeacondevice"]) {
        currentRequestMenu = @"ibeaconcontent";
        if ([dateibeacon_content1 compare:dateibeacon_content] == NSOrderedDescending) {
            NSString *urlAddress10 = [[NSString alloc] initWithFormat:@"%@ibeacon_content.php?db=%@&userid=%@&udid=%@&cuid=%@&device=ios&sc=%@", appdelegate.user_apipath, appdelegate.db, appdelegate.user, appdelegate.uuid, appdelegate.customer_id, appdelegate.cs];
            //NSLog(@"urlAddress10: %@", urlAddress10);
            [self connectToServer:urlAddress10];
        } else {
            [self checkdownloaddata:@"" :NO];
        }
    }
    
}

-(void) check_sqliteDate {
    sqlite3 *database;
    if(sqlite3_open([appdelegate.databasePath UTF8String], &database) == SQLITE_OK) {
        
        // Setup the SQL Statement and compile it for faster access
        NSString *sqlcheckdate = [[NSString alloc] initWithFormat:@"SELECT date_updated FROM app_setup WHERE user_id=%@ ORDER BY date_updated DESC LIMIT 1", appdelegate.user];
        //NSLog(@"sqlcheckdate:: %@", sqlcheckdate);
        const char *sqlStatementdate = [sqlcheckdate UTF8String];
        sqlite3_stmt *compiledStatementdate;
        if(sqlite3_prepare_v2(database, sqlStatementdate, -1, &compiledStatementdate, NULL) == SQLITE_OK) {
            //NSLog(@"temp11:: %@", temp);
            while(sqlite3_step(compiledStatementdate) == SQLITE_ROW) {
                appsetupdate = [NSString stringWithUTF8String:(char *)sqlite3_column_text(compiledStatementdate, 0)];
                //NSLog(@"appsetupdate:: %@", appsetupdate);
            }
        }
        
        // Setup the SQL Statement and compile it for faster access
        NSString *sqlcheckdate1 = [[NSString alloc] initWithFormat:@"SELECT date_updated FROM theme_color ORDER BY date_updated DESC LIMIT 1"];
        //NSLog(@"sqlcheckdate1:: %@", sqlcheckdate1);
        const char *sqlStatementdate1 = [sqlcheckdate1 UTF8String];
        sqlite3_stmt *compiledStatementdate1;
        if(sqlite3_prepare_v2(database, sqlStatementdate1, -1, &compiledStatementdate1, NULL) == SQLITE_OK) {
            //NSLog(@"temp11:: %@", temp);
            while(sqlite3_step(compiledStatementdate1) == SQLITE_ROW) {
                theme_colordate = [NSString stringWithUTF8String:(char *)sqlite3_column_text(compiledStatementdate1, 0)];
            }
        }
        
        // Setup the SQL Statement and compile it for faster access
        NSString *sqlcheckdate2 = [[NSString alloc] initWithFormat:@"SELECT c.date_updated FROM contents c LEFT JOIN content_category ct ON c.category_id=ct.id WHERE ct.is_hidden=0 AND c.user_id=%@  ORDER BY c.date_updated DESC LIMIT 1", appdelegate.user];
        //NSLog(@"sqlcheckdate2:: %@", sqlcheckdate2);
        const char *sqlStatementdate2 = [sqlcheckdate2 UTF8String];
        sqlite3_stmt *compiledStatementdate2;
        if(sqlite3_prepare_v2(database, sqlStatementdate2, -1, &compiledStatementdate2, NULL) == SQLITE_OK) {
            //NSLog(@"temp11:: %@", temp);
            while(sqlite3_step(compiledStatementdate2) == SQLITE_ROW) {
                contentsdate = [NSString stringWithUTF8String:(char *)sqlite3_column_text(compiledStatementdate2, 0)];
            }
            //NSLog(@"contentsdatesqlcheckdate2:: %@", contentsdate);
        }
        
        // Setup the SQL Statement and compile it for faster access
        NSString *sqlcheckdate3 = [[NSString alloc] initWithFormat:@"SELECT date_updated FROM content_category WHERE user_id=%@ and is_hidden=0 ORDER BY date_updated DESC LIMIT 1", appdelegate.user];
        //NSLog(@"sqlcheckdate3:: %@", sqlcheckdate3);
        const char *sqlStatementdate3 = [sqlcheckdate3 UTF8String];
        sqlite3_stmt *compiledStatementdate3;
        if(sqlite3_prepare_v2(database, sqlStatementdate3, -1, &compiledStatementdate3, NULL) == SQLITE_OK) {
            //NSLog(@"temp11:: %@", temp);
            while(sqlite3_step(compiledStatementdate3) == SQLITE_ROW) {
                content_categorydate = [NSString stringWithUTF8String:(char *)sqlite3_column_text(compiledStatementdate3, 0)];
            }
        }
        
        // Setup the SQL Statement and compile it for faster access
        NSString *sqlcheckdate4 = [[NSString alloc] initWithFormat:@"SELECT date_updated FROM ibeacon_content WHERE user_id=%@ ORDER BY date_updated DESC LIMIT 1", appdelegate.user];
        //NSLog(@"sqlcheckdate4:: %@", sqlcheckdate4);
        const char *sqlStatementdate4 = [sqlcheckdate4 UTF8String];
        sqlite3_stmt *compiledStatementdate4;
        if(sqlite3_prepare_v2(database, sqlStatementdate4, -1, &compiledStatementdate4, NULL) == SQLITE_OK) {
            //NSLog(@"temp11:: %@", temp);
            while(sqlite3_step(compiledStatementdate4) == SQLITE_ROW) {
                ibeacon_contentdate = [NSString stringWithUTF8String:(char *)sqlite3_column_text(compiledStatementdate4, 0)];
            }
        }
        
        // Setup the SQL Statement and compile it for faster access
        NSString *sqlcheckdate5 = [[NSString alloc] initWithFormat:@"SELECT date_updated FROM ibeacon_device WHERE user_id=%@ ORDER BY date_updated DESC LIMIT 1", appdelegate.user];
        //NSLog(@"sqlcheckdate5:: %@", sqlcheckdate5);
        const char *sqlStatementdate5 = [sqlcheckdate5 UTF8String];
        sqlite3_stmt *compiledStatementdate5;
        if(sqlite3_prepare_v2(database, sqlStatementdate5, -1, &compiledStatementdate5, NULL) == SQLITE_OK) {
            //NSLog(@"temp11:: %@", temp);
            while(sqlite3_step(compiledStatementdate5) == SQLITE_ROW) {
                ibeacon_devicedate = [NSString stringWithUTF8String:(char *)sqlite3_column_text(compiledStatementdate5, 0)];
            }
        }
        
        // Setup the SQL Statement and compile it for faster access
        NSString *sqlcheckdate6 = [[NSString alloc] initWithFormat:@"SELECT date_updated FROM ibeacon_xref WHERE user_id=%@ ORDER BY date_updated DESC LIMIT 1", appdelegate.user];
        //NSLog(@"sqlcheckdate6:: %@", sqlcheckdate6);
        const char *sqlStatementdate6 = [sqlcheckdate6 UTF8String];
        sqlite3_stmt *compiledStatementdate6;
        if(sqlite3_prepare_v2(database, sqlStatementdate6, -1, &compiledStatementdate6, NULL) == SQLITE_OK) {
            //NSLog(@"temp11:: %@", temp);
            while(sqlite3_step(compiledStatementdate6) == SQLITE_ROW) {
                ibeacon_xrefdate = [NSString stringWithUTF8String:(char *)sqlite3_column_text(compiledStatementdate6, 0)];
            }
        }
        
        // Setup the SQL Statement and compile it for faster access
        NSString *sqlcheckdate7 = [[NSString alloc] initWithFormat:@"SELECT date_updated FROM theme_object WHERE user_id=%@ ORDER BY date_updated DESC LIMIT 1", appdelegate.user];
        //NSLog(@"sqlcheckdate7:: %@", sqlcheckdate7);
        const char *sqlStatementdate7 = [sqlcheckdate7 UTF8String];
        sqlite3_stmt *compiledStatementdate7;
        if(sqlite3_prepare_v2(database, sqlStatementdate7, -1, &compiledStatementdate7, NULL) == SQLITE_OK) {
            //NSLog(@"temp11:: %@", temp);
            while(sqlite3_step(compiledStatementdate7) == SQLITE_ROW) {
                theme_objectdate = [NSString stringWithUTF8String:(char *)sqlite3_column_text(compiledStatementdate7, 0)];
            }
        }
        
        // Setup the SQL Statement and compile it for faster access
        NSString *sqlcheckdate8 = [[NSString alloc] initWithFormat:@"SELECT date_updated FROM theme_object_plugin WHERE user_id=%@ ORDER BY date_updated DESC LIMIT 1", appdelegate.user];
        //NSLog(@"sqlcheckdate8:: %@", sqlcheckdate8);
        const char *sqlStatementdate8 = [sqlcheckdate8 UTF8String];
        sqlite3_stmt *compiledStatementdate8;
        if(sqlite3_prepare_v2(database, sqlStatementdate8, -1, &compiledStatementdate8, NULL) == SQLITE_OK) {
            //NSLog(@"temp11:: %@", temp);
            while(sqlite3_step(compiledStatementdate8) == SQLITE_ROW) {
                theme_object_plugindate = [NSString stringWithUTF8String:(char *)sqlite3_column_text(compiledStatementdate8, 0)];
            }
        }
        
        // Setup the SQL Statement and compile it for faster access
        NSString *sqlcheckdate9 = [[NSString alloc] initWithFormat:@"SELECT date_updated FROM theme_page WHERE user_id=%@ ORDER BY date_updated DESC LIMIT 1) as theme_page", appdelegate.user];
        //NSLog(@"sqlcheckdate9:: %@", sqlcheckdate9);
        const char *sqlStatementdate9 = [sqlcheckdate9 UTF8String];
        sqlite3_stmt *compiledStatementdate9;
        if(sqlite3_prepare_v2(database, sqlStatementdate9, -1, &compiledStatementdate9, NULL) == SQLITE_OK) {
            //NSLog(@"temp11:: %@", temp);
            while(sqlite3_step(compiledStatementdate9) == SQLITE_ROW) {
                theme_pagedate = [NSString stringWithUTF8String:(char *)sqlite3_column_text(compiledStatementdate9, 0)];
            }
        }
        
        if ([appsetupdate length]==0 || [appsetupdate isEqualToString:@"0000-00-00 00:00:00"]) {
            appsetupdate = @"1800-01-01 00:00:00";
        }
        if ([theme_colordate length]==0 || [theme_colordate isEqualToString:@"0000-00-00 00:00:00"]) {
            theme_colordate = @"1800-01-01 00:00:00";
        }
        if ([contentsdate length]==0 || [contentsdate isEqualToString:@"0000-00-00 00:00:00"]) {
            contentsdate = @"1800-01-01 00:00:00";
        }
        if ([content_categorydate length]==0 || [content_categorydate isEqualToString:@"0000-00-00 00:00:00"]) {
            content_categorydate = @"1800-01-01 00:00:00";
        }
        if ([ibeacon_contentdate length]==0 || [ibeacon_contentdate isEqualToString:@"0000-00-00 00:00:00"]) {
            ibeacon_contentdate = @"1800-01-01 00:00:00";
        }
        if ([ibeacon_devicedate length]==0 || [ibeacon_devicedate isEqualToString:@"0000-00-00 00:00:00"]) {
            ibeacon_devicedate = @"1800-01-01 00:00:00";
        }
        if ([ibeacon_xrefdate length]==0 || [ibeacon_xrefdate isEqualToString:@"0000-00-00 00:00:00"]) {
            ibeacon_xrefdate = @"1800-01-01 00:00:00";
        }
        if ([theme_objectdate length]==0 || [theme_objectdate isEqualToString:@"0000-00-00 00:00:00"]) {
            theme_objectdate = @"1800-01-01 00:00:00";
        }
        if ([theme_object_plugindate length]==0 || [theme_object_plugindate isEqualToString:@"0000-00-00 00:00:00"]) {
            theme_object_plugindate = @"1800-01-01 00:00:00";
        }
        if ([theme_pagedate length]==0 || [theme_pagedate isEqualToString:@"0000-00-00 00:00:00"]) {
            theme_pagedate = @"1800-01-01 00:00:00";
        }
        
        /*
        // Setup the SQL Statement and compile it for faster access
        NSString *sqlcheckdate = [[NSString alloc] initWithFormat:@"SELECT (SELECT date_updated FROM app_setup WHERE user_id=%@ ORDER BY date_updated DESC LIMIT 1) as app_setup, (SELECT date_updated FROM theme_color ORDER BY date_updated DESC LIMIT 1) as theme_color, (SELECT c.date_updated FROM contents c LEFT JOIN content_category ct ON c.category_id=ct.id WHERE ct.is_hidden=0 AND c.user_id=%@  ORDER BY c.date_updated DESC LIMIT 1) as contents, (SELECT date_updated FROM content_category WHERE user_id=%@ and is_hidden=0 ORDER BY date_updated DESC LIMIT 1) as content_category, (SELECT date_updated FROM ibeacon_content WHERE user_id=%@ ORDER BY date_updated DESC LIMIT 1) as ibeacon_content, (SELECT date_updated FROM ibeacon_device WHERE user_id=%@ ORDER BY date_updated DESC LIMIT 1) as ibeacon_device, (SELECT date_updated FROM ibeacon_xref WHERE user_id=%@ ORDER BY date_updated DESC LIMIT 1) as ibeacon_xref, (SELECT date_updated FROM theme_object WHERE user_id=%@ ORDER BY date_updated DESC LIMIT 1) as theme_object, (SELECT date_updated FROM theme_object_plugin WHERE user_id=%@ ORDER BY date_updated DESC LIMIT 1) as theme_object_plugin, (SELECT date_updated FROM theme_page WHERE user_id=%@ ORDER BY date_updated DESC LIMIT 1) as theme_page", appdelegate.user,appdelegate.user,appdelegate.user,appdelegate.user,appdelegate.user,appdelegate.user,appdelegate.user,appdelegate.user,appdelegate.user];
        NSLog(@"sqlcheckdate:: %@", sqlcheckdate);
        const char *sqlStatementdate = [sqlcheckdate UTF8String];
        sqlite3_stmt *compiledStatementdate;
        if(sqlite3_prepare_v2(database, sqlStatementdate, -1, &compiledStatementdate, NULL) == SQLITE_OK) {
            //NSLog(@"temp11:: %@", temp);
            while(sqlite3_step(compiledStatementdate) == SQLITE_ROW) {
                appsetupdate = [NSString stringWithUTF8String:(char *)sqlite3_column_text(compiledStatementdate, 0)];
                theme_colordate = [NSString stringWithUTF8String:(char *)sqlite3_column_text(compiledStatementdate, 1)];
                contentsdate = [NSString stringWithUTF8String:(char *)sqlite3_column_text(compiledStatementdate, 2)];
                content_categorydate = [NSString stringWithUTF8String:(char *)sqlite3_column_text(compiledStatementdate, 3)];
                ibeacon_contentdate = [NSString stringWithUTF8String:(char *)sqlite3_column_text(compiledStatementdate, 4)];
                ibeacon_devicedate = [NSString stringWithUTF8String:(char *)sqlite3_column_text(compiledStatementdate, 5)];
                ibeacon_xrefdate = [NSString stringWithUTF8String:(char *)sqlite3_column_text(compiledStatementdate, 6)];
                theme_objectdate = [NSString stringWithUTF8String:(char *)sqlite3_column_text(compiledStatementdate, 7)];
                theme_object_plugindate = [NSString stringWithUTF8String:(char *)sqlite3_column_text(compiledStatementdate, 8)];
                theme_pagedate = [NSString stringWithUTF8String:(char *)sqlite3_column_text(compiledStatementdate, 9)];
                //warrantydate = [NSString stringWithUTF8String:(char *)sqlite3_column_text(compiledStatementdate, 8)];
            }
        } */
        
        /*NSLog(@"contentsdate: %@", contentsdate);
        NSLog(@"content_categorydate: %@", content_categorydate);
        NSLog(@"ibeacon_contentdate: %@", ibeacon_contentdate);
        NSLog(@"ibeacon_devicedate: %@", ibeacon_devicedate);
        NSLog(@"ibeacon_xrefdate: %@", ibeacon_xrefdate);
        NSLog(@"theme_objectdate: %@", theme_objectdate); */
        // Release the compiled statement from memory
        sqlite3_finalize(compiledStatementdate);
        
    }
    sqlite3_close(database);
}

#pragma mark Start Connection

/*-(void) connectToServer:(NSString *) urlPath {
    
    //AppDelegate *appdelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    
    NSURLRequest *request = [NSURLRequest requestWithURL:[NSURL URLWithString:urlPath]];
    
    NSError        *error = nil;
    NSURLResponse  *responsedata = nil;
    NSData *data = [NSURLConnection sendSynchronousRequest:request
                                         returningResponse:&responsedata error:&error];
    
    //NSStringEncoding encoding = CFStringConvertEncodingToNSStringEncoding(kCFStringEncodingGB_18030_2000);
    NSString *escapedStr = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
    //NSString *escapedStr = [[NSString alloc] initWithData:data encoding:encoding];
    NSString *response = [escapedStr stringByReplacingOccurrencesOfString:@"'" withString:@"''"];
    
    //NSLog(@"response: %@",response);
    
    if ([response length]==0)
        response = @"[{}]";
    
    if (error == nil) {
        [self downloaddata:appdelegate.firsttime :response];
    } else {
        [self downloaddata:YES :@""];
    } */
    /*if ([currentRequestMenu isEqualToString:@"themepage"])
    {
        //
        TxtFiles *files = [[TxtFiles alloc] init];
        
        [files WriteToStringFile:[response mutableCopy] andsetFileName:files.setFilenamethemepage];
    }
    
    if ([currentRequestMenu isEqualToString:@"themeobject"])
    {
        //
        TxtFiles *files = [[TxtFiles alloc] init];
        
        [files WriteToStringFile:[response mutableCopy] andsetFileName:files.setFilenamethemeobject];
    }
    
    if ([currentRequestMenu isEqualToString:@"themeplugin"])
    {
        //
        TxtFiles *files = [[TxtFiles alloc] init];
        
        [files WriteToStringFile:[response mutableCopy] andsetFileName:files.setFilenamethemeplugin];
    }
    
    if ([currentRequestMenu isEqualToString:@"themecolor"])
    {
        //
        TxtFiles *files = [[TxtFiles alloc] init];
        
        [files WriteToStringFile:[response mutableCopy] andsetFileName:files.setFilenamethemecolor];
    }
    
    if ([currentRequestMenu isEqualToString:@"appsetup"])
    {
        //
        TxtFiles *files = [[TxtFiles alloc] init];
        
        [files WriteToStringFile:[response mutableCopy] andsetFileName:files.setFilenameappsetup];
    }
    
    if ([currentRequestMenu isEqualToString:@"contentcategory"])
    {
        //
        TxtFiles *files = [[TxtFiles alloc] init];
        
        [files WriteToStringFile:[response mutableCopy] andsetFileName:files.setFilenamecontentcategory];
    }
    
    if ([currentRequestMenu isEqualToString:@"contents"])
    {
        //
        TxtFiles *files = [[TxtFiles alloc] init];
        
        [files WriteToStringFile:[response mutableCopy] andsetFileName:files.setFilenamecontents];
    }
    
    if ([currentRequestMenu isEqualToString:@"ibeacondevice"])
    {
        //
        TxtFiles *files = [[TxtFiles alloc] init];
        
        [files WriteToStringFile:[response mutableCopy] andsetFileName:files.setFilenameibeacondevice];
    }
    
    if ([currentRequestMenu isEqualToString:@"ibeaconcontent"])
    {
        //
        TxtFiles *files = [[TxtFiles alloc] init];
        
        [files WriteToStringFile:[response mutableCopy] andsetFileName:files.setFilenameibeaconcontent];
    }
    
    if ([currentRequestMenu isEqualToString:@"ibeaconxref"])
    {
        //
        TxtFiles *files = [[TxtFiles alloc] init];
        
        [files WriteToStringFile:[response mutableCopy] andsetFileName:files.setFilenameibeaconxref];
    }
    
    if ([currentRequestMenu isEqualToString:@"warranty"])
    {
        //
        TxtFiles *files = [[TxtFiles alloc] init];
        
        [files WriteToStringFile:[response mutableCopy] andsetFileName:files.setFilenamewarranty];
    } */
    
    /*if ([currentRequestMenu isEqualToString:@"checkdate"]) {
        
        NSDictionary *dictionary = [response JSONValue];
        
        NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
        dateFormat.dateFormat = @"yyyy-MM-dd HH:mm:ss";
        NSDate *datetheme_page = [dateFormat dateFromString:theme_pagedate];
        //NSLog(@"datetheme_objecttheme_objectdate: %@",theme_objectdate);
        NSDate *datetheme_object = [dateFormat dateFromString:theme_objectdate];
        NSDate *datetheme_object_plugin = [dateFormat dateFromString:theme_object_plugindate];
        NSDate *datecontent_category = [dateFormat dateFromString:content_categorydate];
        NSDate *dateibeacon_content = [dateFormat dateFromString:ibeacon_contentdate];
        NSDate *dateibeacon_devicedate = [dateFormat dateFromString:ibeacon_devicedate];
        NSDate *dateibeacon_xrefdate = [dateFormat dateFromString:ibeacon_xrefdate];
        NSDate *datecontentsdate = [dateFormat dateFromString:contentsdate];
        NSDate *dateappsetupdate = [dateFormat dateFromString:appsetupdate];
        NSDate *datetheme_colordate = [dateFormat dateFromString:theme_colordate];
        
        NSDate *datetheme_page1;
        NSDate *datetheme_object1;
        NSDate *datetheme_object_plugin1;
        NSDate *datecontent_category1;
        NSDate *dateibeacon_content1;
        NSDate *dateibeacon_devicedate1;
        NSDate *dateibeacon_xrefdate1;
        NSDate *datecontentsdate1;
        NSDate *dateappsetupdate1;
        NSDate *datetheme_colordate1;
        
		for(NSDictionary *row in dictionary)
        {
            
            //NSLog(@"datetheme_object1theme_object: %@",[row objectForKey:@"theme_object"]);
            datetheme_page1 = [dateFormat dateFromString:[row objectForKey:@"theme_page"]];
            datetheme_object1 = [dateFormat dateFromString:[row objectForKey:@"theme_object"]];
            datetheme_object_plugin1 = [dateFormat dateFromString:[row objectForKey:@"theme_object_plugin"]];
            //NSDate *datewarranty = [dateFormat dateFromString:warrantydate];
            //NSDate *datewarranty1 = [row objectForKey:@"theme_object_plugin"];
            
            datecontent_category1 = [dateFormat dateFromString:[row objectForKey:@"content_category"]];
            dateibeacon_content1 = [dateFormat dateFromString:[row objectForKey:@"ibeacon_content"]];
            dateibeacon_devicedate1 = [dateFormat dateFromString:[row objectForKey:@"ibeacon_device"]];
            dateibeacon_xrefdate1 = [dateFormat dateFromString:[row objectForKey:@"ibeacon_xref"]];
            datetheme_colordate1 = [dateFormat dateFromString:[row objectForKey:@"theme_color"]];
            datecontentsdate1 = [dateFormat dateFromString:[row objectForKey:@"contents"]];
            dateappsetupdate1 = [dateFormat dateFromString:[row objectForKey:@"app_setup"]];
                                               
        }
        
        if ([datetheme_page1 compare:datetheme_page] == NSOrderedDescending) {
            NSString *urlAddress1 = [[NSString alloc] initWithFormat:@"%@theme_page.php?db=%@&userid=%@&udid=%@&cuid=%@&device=ios&sc=%@", appdelegate.user_apipath, appdelegate.user, appdelegate.user, appdelegate.uuid, appdelegate.customer_id, appdelegate.cs];
            currentRequestMenu = @"themepage";
            [self connectToServer:urlAddress1];
            //NSLog(@"urlAddress1: %@",urlAddress1);
        }
        
        //NSLog(@"datetheme_object1: %@",datetheme_object1);
        //NSLog(@"datetheme_object: %@",datetheme_object);
        if ([datetheme_object1 compare:datetheme_object] == NSOrderedDescending) {
            NSString *urlAddress2 = [[NSString alloc] initWithFormat:@"%@theme_object.php?db=%@&userid=%@&udid=%@&cuid=%@&device=ios&sc=%@", appdelegate.user_apipath, appdelegate.user, appdelegate.user, appdelegate.uuid, appdelegate.customer_id, appdelegate.cs];
            currentRequestMenu = @"themeobject";
            [self connectToServer:urlAddress2];
           // NSLog(@"urlAddress2: %@",urlAddress2);
        }
        
        if ([datetheme_object_plugin1 compare:datetheme_object_plugin] == NSOrderedDescending) {
            NSString *urlAddress3 = [[NSString alloc] initWithFormat:@"%@theme_object_plugin.php?db=%@&userid=%@&udid=%@&cuid=%@&device=ios&sc=%@", appdelegate.user_apipath, appdelegate.user, appdelegate.user, appdelegate.uuid, appdelegate.customer_id, appdelegate.cs];
            currentRequestMenu = @"themeplugin";
            [self connectToServer:urlAddress3];
        }
        
        if ([datetheme_colordate1 compare:datetheme_colordate] == NSOrderedDescending) {
            NSString *urlAddress4 = [[NSString alloc] initWithFormat:@"%@theme_color.php?db=%@&userid=%@&udid=%@&cuid=%@&device=ios&sc=%@", appdelegate.user_apipath, appdelegate.user, appdelegate.user, appdelegate.uuid, appdelegate.customer_id, appdelegate.cs];
            currentRequestMenu = @"themecolor";
            //
            //NSLog(@"urlthemecolor: %@",urlAddress4);
            [self connectToServer:urlAddress4];
        }
        
        //NSLog(@"dateappsetupdate1: %@",dateappsetupdate1);
        //NSLog(@"dateappsetupdate: %@",dateappsetupdate);
        if ([dateappsetupdate1 compare:dateappsetupdate] == NSOrderedDescending) {
            NSString *urlAddress5 = [[NSString alloc] initWithFormat:@"%@app_setup.php?db=%@&userid=%@&udid=%@&cuid=%@&device=ios&sc=%@", appdelegate.user_apipath, appdelegate.user, appdelegate.user, appdelegate.uuid, appdelegate.customer_id, appdelegate.cs];
            currentRequestMenu = @"appsetup";
            //NSLog(@"urlAddress5: %@", urlAddress5);
            [self connectToServer:urlAddress5];
        }
        
        if ([datecontent_category1 compare:datecontent_category] == NSOrderedDescending) {
            NSString *urlAddress6 = [[NSString alloc] initWithFormat:@"%@content_category.php?db=%@&userid=%@&udid=%@&cuid=%@&device=ios&sc=%@", appdelegate.user_apipath, appdelegate.user, appdelegate.user, appdelegate.uuid, appdelegate.customer_id, appdelegate.cs];
            currentRequestMenu = @"contentcategory";
            //NSLog(@"urlAddress6: %@", urlAddress6);
            [self connectToServer:urlAddress6];
        }
        
        //NSLog(@"datecontentsdate1: %@",datecontentsdate1);
        //NSLog(@"datecontentsdate: %@",datecontentsdate);
        if ([datecontentsdate1 compare:datecontentsdate] == NSOrderedDescending) {
            NSString *urlAddress7 = [[NSString alloc] initWithFormat:@"%@contents.php?db=%@&userid=%@&udid=%@&cuid=%@&device=ios&sc=%@", appdelegate.user_apipath, appdelegate.user, appdelegate.user, appdelegate.uuid, appdelegate.customer_id, appdelegate.cs];
            currentRequestMenu = @"contents";
            //NSLog(@"urlAddress7: %@", urlAddress7);
            [self connectToServer:urlAddress7];
        }
        
        if ([dateibeacon_xrefdate1 compare:dateibeacon_xrefdate] == NSOrderedDescending) {
            NSString *urlAddress8 = [[NSString alloc] initWithFormat:@"%@ibeacon_xref.php?db=%@&userid=%@&udid=%@&cuid=%@&device=ios&sc=%@", appdelegate.user_apipath, appdelegate.user, appdelegate.user, appdelegate.uuid, appdelegate.customer_id, appdelegate.cs];
            currentRequestMenu = @"ibeaconxref";
            //NSLog(@"urlAddress8: %@", urlAddress8);
            [self connectToServer:urlAddress8];
        }
        
        if ([dateibeacon_devicedate1 compare:dateibeacon_devicedate] == NSOrderedDescending) {
            NSString *urlAddress9 = [[NSString alloc] initWithFormat:@"%@ibeacon_device.php?db=%@&userid=%@&udid=%@&cuid=%@&device=ios&sc=%@", appdelegate.user_apipath, appdelegate.user, appdelegate.user, appdelegate.uuid, appdelegate.customer_id, appdelegate.cs];
            currentRequestMenu = @"ibeacondevice";
            //NSLog(@"urlAddress8: %@", urlAddress8);
            [self connectToServer:urlAddress9];
        }
        
        if ([dateibeacon_content1 compare:dateibeacon_content] == NSOrderedDescending) {
            NSString *urlAddress10 = [[NSString alloc] initWithFormat:@"%@ibeacon_content.php?db=%@&userid=%@&udid=%@&cuid=%@&device=ios&sc=%@", appdelegate.user_apipath, appdelegate.user, appdelegate.user, appdelegate.uuid, appdelegate.customer_id, appdelegate.cs];
            currentRequestMenu = @"ibeaconcontent";
            //NSLog(@"urlAddress8: %@", urlAddress8);
            [self connectToServer:urlAddress10];
        }
    }
    
} */

#pragma mark Connect To API
-(void) connectToServer:(NSString *) urlPath
{
    [self addLoadingView];
	NSString *urlAddress =[NSString stringWithFormat:@"%@", urlPath];
	
    [self getJsonResponse:urlAddress success:^(NSDictionary *responseDict) {
        
        [self successfunction:responseDict];
        
    } failure:^(NSError *error) {
        
        [self removeLoadingView];
        NSLog(@"No internet connection available. Standard Mobile Apps requires internet connection to function properly.");
        [self checkdownloaddata:@"" :YES];
    }];
}

-(void)getJsonResponse : (NSString *)urlStr success : (void (^)(NSDictionary *responseDict))success failure:(void(^)(NSError* error))failure
{
    
    
    NSURLSession * session = [NSURLSession sharedSession];
    NSURL * url = [NSURL URLWithString: urlStr];
    
    NSURLSessionDataTask * dataTask = [session dataTaskWithURL:url completionHandler:^(NSData *data, NSURLResponse *response, NSError *error)
                                       {
                                           
                                           NSDictionary * json;
                                           if ([data length]>0) {
                                               json  = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
                                               success(json);
                                           } else {
                                               json = nil;
                                               dispatch_async(dispatch_get_main_queue(), ^ {
                                                   [self removeLoadingView];
                                               });
                                           }
                                           
                                           
                                       }];
    
    [dataTask resume] ; // Executed First
    
}

-(void) successfunction:(NSDictionary *)responseDict {
    dispatch_async(dispatch_get_main_queue(), ^{
        NSString *jsonString = [responseDict JSONRepresentation];
        //NSLog(@"Succeed! Received %ld bytes of data",(unsigned long)[receivedData length]);
        //NSString *escapedStr = [[NSString alloc] initWithData:receivedData encoding:NSUTF8StringEncoding];
        
        //NSString *escapedStr = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
        //NSString *escapedStr = [[NSString alloc] initWithData:data encoding:encoding];
        // NSString *response = [escapedStr stringByReplacingOccurrencesOfString:@"'" withString:@"''"];
        
        if ([responseDict count]==0)
            jsonString = @"[{}]";
        
        //NSLog(@"%@ response:: %@", currentRequestMenu, response);
        
        if ([currentRequestMenu isEqualToString:@"checkdate"]) {
            
            NSDictionary *dictionary = responseDict;
            
            for(NSDictionary *row in dictionary)
            {
                NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
                dateFormat.dateFormat = @"yyyy-MM-dd HH:mm:ss";
                //NSLog(@"datetheme_object1theme_object: %@",[row objectForKey:@"theme_object"]);
                if (![[row objectForKey:@"theme_page"] isEqual:[NSNull null]]) {
                    datetheme_page1 = [dateFormat dateFromString:[row objectForKey:@"theme_page"]];
                } else {
                    datetheme_page1 = [dateFormat dateFromString:@"1800-01-01 00:00:00"];
                }
                if (![[row objectForKey:@"theme_object"] isEqual:[NSNull null]]) {
                    datetheme_object1 = [dateFormat dateFromString:[row objectForKey:@"theme_object"]];
                } else {
                    datetheme_object1 = [dateFormat dateFromString:@"1800-01-01 00:00:00"];
                }
                if (![[row objectForKey:@"theme_object_plugin"] isEqual:[NSNull null]]) {
                    datetheme_object_plugin1 = [dateFormat dateFromString:[row objectForKey:@"theme_object_plugin"]];
                } else {
                    datetheme_object_plugin1 = [dateFormat dateFromString:@"1800-01-01 00:00:00"];
                }
                //NSDate *datewarranty = [dateFormat dateFromString:warrantydate];
                //NSDate *datewarranty1 = [row objectForKey:@"theme_object_plugin"];
                if (![[row objectForKey:@"content_category"] isEqual:[NSNull null]]) {
                    datecontent_category1 = [dateFormat dateFromString:[row objectForKey:@"content_category"]];
                } else {
                    datecontent_category1 = [dateFormat dateFromString:@"1800-01-01 00:00:00"];
                }
                if (![[row objectForKey:@"ibeacon_content"] isEqual:[NSNull null]]) {
                    dateibeacon_content1 = [dateFormat dateFromString:[row objectForKey:@"ibeacon_content"]];
                } else {
                    dateibeacon_content1 = [dateFormat dateFromString:@"1800-01-01 00:00:00"];
                }
                if (![[row objectForKey:@"ibeacon_device"] isEqual:[NSNull null]]) {
                    dateibeacon_devicedate1 = [dateFormat dateFromString:[row objectForKey:@"ibeacon_device"]];
                } else {
                    dateibeacon_devicedate1 = [dateFormat dateFromString:@"1800-01-01 00:00:00"];
                }
                if (![[row objectForKey:@"ibeacon_xref"] isEqual:[NSNull null]]) {
                    dateibeacon_xrefdate1 = [dateFormat dateFromString:[row objectForKey:@"ibeacon_xref"]];
                } else {
                    dateibeacon_xrefdate1 = [dateFormat dateFromString:@"1800-01-01 00:00:00"];
                }
                if (![[row objectForKey:@"theme_color"] isEqual:[NSNull null]]) {
                    datetheme_colordate1 = [dateFormat dateFromString:[row objectForKey:@"theme_color"]];
                } else {
                    datetheme_colordate1 = [dateFormat dateFromString:@"1800-01-01 00:00:00"];
                }
                if (![[row objectForKey:@"contents"] isEqual:[NSNull null]]) {
                    datecontentsdate1 = [dateFormat dateFromString:[row objectForKey:@"contents"]];
                } else {
                    datecontentsdate1 = [dateFormat dateFromString:@"1800-01-01 00:00:00"];
                }
                if (![[row objectForKey:@"app_setup"] isEqual:[NSNull null]]) {
                    dateappsetupdate1 = [dateFormat dateFromString:[row objectForKey:@"app_setup"]];
                } else {
                    dateappsetupdate1 = [dateFormat dateFromString:@"1800-01-01 00:00:00"];
                }
                
            }
            
            //[self processdata];
        } //else {
        //[self downloaddata:appdelegate.firsttime :response];
        //}
        [self removeLoadingView];
        
        if ([currentRequestMenu isEqualToString:@"ibeaconCheckInSetting"]) {
            //NSLog(@"response:: %@",response );
            //NSDictionary *dictionarybeacon = [arrayIBeaconList objectAtIndex:current_idx];
            NSDictionary *dictionary = responseDict;
            
            tqmsg = @"";
            tqmsgtitle = @"";
            for(NSDictionary *row in dictionary)
            {
                if ([[row objectForKey:@"plugin_code"] isEqualToString:@"ibeacon_checkin"]) {
                    NSString *urlAddress = [[NSString alloc] initWithFormat:@"%@point_earn.php?db=%@&userid=%@&udid=%@&cuid=%@&plugin_id=%@&setting_id=%@&data1=%@&data2=%@", appdelegate.user_apipath, appdelegate.db, appdelegate.user, appdelegate.uuid, appdelegate.customer_id, [row objectForKey:@"plugin_id"], [row objectForKey:@"id"], [row objectForKey:@"plugin_code"], [[arrayIBeaconList objectAtIndex:current_idx] objectForKey:@"id"]];
                    //NSLog(@"ibeaconCheckInPointurlAddress::%@", urlAddress);
                    currentRequestMenu = @"ibeaconCheckInPoint";
                    [self connectToServer:urlAddress];
                    
                    tqmsg = [row objectForKey:@"thankyou_msg"];
                    tqmsgtitle = [row objectForKey:@"title"];
                }
            }
        } else if ([currentRequestMenu isEqualToString:@"ibeaconCheckInPoint"]) {
            NSDictionary *dictionary = responseDict;
            //NSLog(@"ibeaconCheckInPointresponse:: %@",response );
            for(NSDictionary *row in dictionary)
            {
                //NSLog(@"ibeaconCheckInPoint::%@", [row objectForKey:@"desc"]);
                if ([[row objectForKey:@"status"] intValue]==1) {
                    __strong UIAlertView *myal = [[UIAlertView alloc] initWithTitle:tqmsgtitle message:tqmsg delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
                    [myal show];
                    [self performSelector:@selector(success:) withObject:myal afterDelay:3];
                }
            }
        } else {
            
            [self checkdownloaddata:jsonString :NO];
        }
        
    });
}

/*- (void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response
{
    //NSLog(@"didReceiveResponse");
    [receivedData setLength:0];
}

- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data
{
    [receivedData appendData:data];
    //NSLog(@"receivedData:: %@", receivedData);
}

- (void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error {
    // release the connection, and the data object
	
    //[connection release];
	//[receivedData release];
	
    [self removeLoadingView];
	//[self displayAlert:@"No internet connection available. Standard Mobile Apps requires internet connection to function properly."];
    //[self downloaddata:appdelegate.firsttime :@""];
    NSLog(@"No internet connection available. Standard Mobile Apps requires internet connection to function properly.");
    [self checkdownloaddata:@"" :YES];
}

- (void)connectionDidFinishLoading:(NSURLConnection *)connection
{
	NSLog(@"Succeed! Received %ld bytes of data",(unsigned long)[receivedData length]);
	NSString *escapedStr = [[NSString alloc] initWithData:receivedData encoding:NSUTF8StringEncoding];
	
	//NSString *escapedStr = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
    //NSString *escapedStr = [[NSString alloc] initWithData:data encoding:encoding];
    NSString *response = [escapedStr stringByReplacingOccurrencesOfString:@"'" withString:@"''"];
    
    if ([response length]==0)
        response = @"[{}]";
    
    //NSLog(@"%@ response:: %@", currentRequestMenu, response);
    
    if ([currentRequestMenu isEqualToString:@"checkdate"]) {
        
        NSDictionary *dictionary = [response JSONValue];
        
		for(NSDictionary *row in dictionary)
        {
            NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
            dateFormat.dateFormat = @"yyyy-MM-dd HH:mm:ss";
            //NSLog(@"datetheme_object1theme_object: %@",[row objectForKey:@"theme_object"]);
            if (![[row objectForKey:@"theme_page"] isEqual:[NSNull null]]) {
                datetheme_page1 = [dateFormat dateFromString:[row objectForKey:@"theme_page"]];
            } else {
                datetheme_page1 = [dateFormat dateFromString:@"1800-01-01 00:00:00"];
            }
            if (![[row objectForKey:@"theme_object"] isEqual:[NSNull null]]) {
                datetheme_object1 = [dateFormat dateFromString:[row objectForKey:@"theme_object"]];
            } else {
                datetheme_object1 = [dateFormat dateFromString:@"1800-01-01 00:00:00"];
            }
            if (![[row objectForKey:@"theme_object_plugin"] isEqual:[NSNull null]]) {
                datetheme_object_plugin1 = [dateFormat dateFromString:[row objectForKey:@"theme_object_plugin"]];
            } else {
                datetheme_object_plugin1 = [dateFormat dateFromString:@"1800-01-01 00:00:00"];
            }
            //NSDate *datewarranty = [dateFormat dateFromString:warrantydate];
            //NSDate *datewarranty1 = [row objectForKey:@"theme_object_plugin"];
            if (![[row objectForKey:@"content_category"] isEqual:[NSNull null]]) {
                datecontent_category1 = [dateFormat dateFromString:[row objectForKey:@"content_category"]];
            } else {
                datecontent_category1 = [dateFormat dateFromString:@"1800-01-01 00:00:00"];
            }
            if (![[row objectForKey:@"ibeacon_content"] isEqual:[NSNull null]]) {
                dateibeacon_content1 = [dateFormat dateFromString:[row objectForKey:@"ibeacon_content"]];
            } else {
                dateibeacon_content1 = [dateFormat dateFromString:@"1800-01-01 00:00:00"];
            }
            if (![[row objectForKey:@"ibeacon_device"] isEqual:[NSNull null]]) {
                dateibeacon_devicedate1 = [dateFormat dateFromString:[row objectForKey:@"ibeacon_device"]];
            } else {
                dateibeacon_devicedate1 = [dateFormat dateFromString:@"1800-01-01 00:00:00"];
            }
            if (![[row objectForKey:@"ibeacon_xref"] isEqual:[NSNull null]]) {
                dateibeacon_xrefdate1 = [dateFormat dateFromString:[row objectForKey:@"ibeacon_xref"]];
            } else {
                dateibeacon_xrefdate1 = [dateFormat dateFromString:@"1800-01-01 00:00:00"];
            }
            if (![[row objectForKey:@"theme_color"] isEqual:[NSNull null]]) {
                datetheme_colordate1 = [dateFormat dateFromString:[row objectForKey:@"theme_color"]];
            } else {
                datetheme_colordate1 = [dateFormat dateFromString:@"1800-01-01 00:00:00"];
            }
            if (![[row objectForKey:@"contents"] isEqual:[NSNull null]]) {
                datecontentsdate1 = [dateFormat dateFromString:[row objectForKey:@"contents"]];
            } else {
                datecontentsdate1 = [dateFormat dateFromString:@"1800-01-01 00:00:00"];
            }
            if (![[row objectForKey:@"app_setup"] isEqual:[NSNull null]]) {
                dateappsetupdate1 = [dateFormat dateFromString:[row objectForKey:@"app_setup"]];
            } else {
                dateappsetupdate1 = [dateFormat dateFromString:@"1800-01-01 00:00:00"];
            }
            
        }
        
        //[self processdata];
    } //else {
        //[self downloaddata:appdelegate.firsttime :response];
    //}
    [self removeLoadingView];
    
    if ([currentRequestMenu isEqualToString:@"ibeaconCheckInSetting"]) {
        //NSLog(@"response:: %@",response );
        //NSDictionary *dictionarybeacon = [arrayIBeaconList objectAtIndex:current_idx];
        NSDictionary *dictionary = [response JSONValue];
        
        tqmsg = @"";
        tqmsgtitle = @"";
        for(NSDictionary *row in dictionary)
        {
            if ([[row objectForKey:@"plugin_code"] isEqualToString:@"ibeacon_checkin"]) {
                NSString *urlAddress = [[NSString alloc] initWithFormat:@"%@point_earn.php?db=%@&userid=%@&udid=%@&cuid=%@&plugin_id=%@&setting_id=%@&data1=%@&data2=%@", appdelegate.user_apipath, appdelegate.db, appdelegate.user, appdelegate.uuid, appdelegate.customer_id, [row objectForKey:@"plugin_id"], [row objectForKey:@"id"], [row objectForKey:@"plugin_code"], [[arrayIBeaconList objectAtIndex:current_idx] objectForKey:@"id"]];
                //NSLog(@"ibeaconCheckInPointurlAddress::%@", urlAddress);
                currentRequestMenu = @"ibeaconCheckInPoint";
                [self connectToServer:urlAddress];
                
                tqmsg = [row objectForKey:@"thankyou_msg"];
                tqmsgtitle = [row objectForKey:@"title"];
            }
        }
    } else if ([currentRequestMenu isEqualToString:@"ibeaconCheckInPoint"]) {
        NSDictionary *dictionary = [response JSONValue];
        //NSLog(@"ibeaconCheckInPointresponse:: %@",response );
        for(NSDictionary *row in dictionary)
        {
            //NSLog(@"ibeaconCheckInPoint::%@", [row objectForKey:@"desc"]);
            if ([[row objectForKey:@"status"] intValue]==1) {
                __strong UIAlertView *myal = [[UIAlertView alloc] initWithTitle:tqmsgtitle message:tqmsg delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
                [myal show];
                [self performSelector:@selector(success:) withObject:myal afterDelay:3];
            }
        }
    } else {
        
        [self checkdownloaddata:response :NO];
    }
    
	//[connection release];
    connection = nil;
	//[response release];
    response = nil;
} */

-(void)success:(UIAlertView*)x{
    [x dismissWithClickedButtonIndex:-1 animated:NO];
}

-(void) addLoadingView {
	loadingView = [LoadingView loadingViewInView:self.view];
}

-(void) removeLoadingView
{
	[loadingView removeView];
}

- (void) downloaddata :(BOOL)firsttime :(NSString *)response  :(BOOL)FailWithError{
    NSString * timeStampValueSync = [NSString stringWithFormat:@"%ld", (long)[[NSDate date] timeIntervalSince1970]];
    
    sqlite3 *database;
    // Open the database from the users filessytem
    if(sqlite3_open([appdelegate.databasePath UTF8String], &database) == SQLITE_OK) {
        NSString *ReadData;
        if ((firsttime && ![self isConnectionAvailable]) || (firsttime && FailWithError)) {
            TxtFiles *readFile = [[TxtFiles alloc] init];
            ReadData = [readFile readFromFile:readFile.setFilenamethemepage];
        } else {
            if ([currentRequestMenu isEqualToString:@"themepage"]) {
                ReadData = response;
            } else {
                ReadData = @"[{}]";
            }
        }
        
        if (!([ReadData isEqualToString:@"[{}]"] || [ReadData isEqualToString:@""])) {
            SBJsonParser *jsonParser = [SBJsonParser new];
            NSDictionary *jsonData = (NSDictionary *) [jsonParser objectWithString:ReadData error:nil];
            
            //NSLog(@"jsonData: %@", jsonData);
            
            NSMutableDictionary *dictionary1 = [jsonData mutableCopy];
            //NSLog(@"theme_page:: %@", dictionary1);
            for(NSMutableDictionary *row in dictionary1){
                NSString *themepageid = [row objectForKey:@"id"];
                NSString *dateupdated = [row objectForKey:@"date_updated"];
                //NSString *name = [row objectForKey:@"name"];
                NSString *prevpage = [row objectForKey:@"prev_page"];
                //NSString *remark = [row objectForKey:@"remark"];
                NSString *theme_code = [row objectForKey:@"theme_code"];
                
                int rows=0;
                NSString *temp = [[NSString alloc] initWithFormat:@"SELECT * FROM theme_page WHERE id=%@ ",themepageid];
                //NSLog(@"temptheme_page:: %@", temp);
                const char *sqlStatement = [temp UTF8String];
                sqlite3_stmt *compiledStatement;
                if(sqlite3_prepare_v2(database, sqlStatement, -1, &compiledStatement, NULL) == SQLITE_OK) {
                    // Loop through the results and add them to the feeds array
                    
                    if (sqlite3_step(compiledStatement) == SQLITE_ERROR) {
                        NSAssert1(0,@"Error when counting rows  %s",sqlite3_errmsg(database));
                    } else {
                        rows = sqlite3_column_int(compiledStatement, 0);
                        //NSLog(@"SQLite Rows: %i", rows);
                    }
                    
                    NSString *tname;
                    if([row objectForKey:@"name"] == (NSString*)[NSNull null]) {
                        tname = @"";
                    } else {
                        tname = [[row objectForKey:@"name"] stringByReplacingOccurrencesOfString:@"'" withString:@"''"];
                    }
                    
                    NSString *tremark;
                    if([row objectForKey:@"remark"] == (NSString*)[NSNull null]) {
                        tremark = @"";
                    } else {
                        tremark = [[row objectForKey:@"remark"] stringByReplacingOccurrencesOfString:@"'" withString:@"''"];
                    }
                    
                    //while(sqlite3_step(compiledStatement) == SQLITE_ROW) {
                    if (rows > 0) {
                        //NSLog(@"database:::::::update conferencestheme_page");
                        static sqlite3_stmt *compiledupdateStatement;
                        sqlite3_exec(database, [[NSString stringWithFormat:@"update theme_page SET theme_code=%@, name='%@', prev_page=%@, user_id=%@, remark='%@', date_updated='%@', lastsync_date=%@ where id='%@' ", theme_code, tname, prevpage, appdelegate.user, tremark, dateupdated, timeStampValueSync, [row objectForKey:@"id"]] UTF8String], NULL, NULL, NULL);
                        sqlite3_finalize(compiledupdateStatement);
                        //NSLog(@"database:::::::update conferencestheme_page11");
                    } else {
                        //NSLog(@"database:::::::insert into conferencestheme_page");
                        static sqlite3_stmt *compiledinsertStatement;
                        sqlite3_exec(database, [[NSString stringWithFormat:@"insert into theme_page (id, theme_code, name, prev_page, user_id, remark, date_updated, lastsync_date) values ('%@', '%@', '%@', '%@', '%@', '%@', '%@', '%@')", themepageid, theme_code, tname, prevpage, appdelegate.user, tremark, dateupdated, timeStampValueSync] UTF8String], NULL, NULL, NULL);
                        sqlite3_finalize(compiledinsertStatement);
                    }
                }
                // Release the compiled statement from memory
                sqlite3_finalize(compiledStatement);
                
            }
            
            //NSLog(@"database:::::::Delete not synced posters %@",timeStampValueSync);
            static sqlite3_stmt *compiledinsertStatement3;
            sqlite3_exec(database, [[NSString stringWithFormat:@"Delete from theme_page where lastsync_date<>%@", timeStampValueSync] UTF8String], NULL, NULL, NULL);
            sqlite3_finalize(compiledinsertStatement3);
        }
        
        NSString *ReadData1;
        //NSLog(@"here");
        if ((firsttime && ![self isConnectionAvailable]) || (firsttime && FailWithError)) {
            TxtFiles *readFile1 = [[TxtFiles alloc] init];
            ReadData1 = [readFile1 readFromFile:readFile1.setFilenamethemeobject];
        } else {
            if ([currentRequestMenu isEqualToString:@"themeobject"]) {
                ReadData1 = response;
            } else {
                ReadData1 = @"[{}]";
            }
        }
        
        if (!([ReadData1 isEqualToString:@"[{}]"] || [ReadData1 isEqualToString:@""])) {
            SBJsonParser *jsonParser1 = [SBJsonParser new];
            NSDictionary *jsonData1 = (NSDictionary *) [jsonParser1 objectWithString:ReadData1 error:nil];
            
            //NSLog(@"jsonData1: %@", jsonData1);
            
            NSMutableDictionary *dictionary2 = [jsonData1 mutableCopy];
            //NSLog(@"theme_object:: %@", dictionary2);
            for(NSMutableDictionary *row in dictionary2){
                NSString *themeobjid = [row objectForKey:@"id"];
                NSString *obj_themecode = [row objectForKey:@"theme_code"];
                NSString *obj_slotcode = [row objectForKey:@"slot_code"];
                NSString *obj_pageid = [row objectForKey:@"page_id"];
                NSString *objtype = [row objectForKey:@"object_type"];
                //NSString *objvalue = [row objectForKey:@"object_value"];
                NSString *obj_actiontype = [row objectForKey:@"action_type"];
                NSString *obj_actionvalue = [row objectForKey:@"action_value"];
                NSString *obj_userid = [row objectForKey:@"user_id"];
                NSString *obj_attribute = [row objectForKey:@"attribute"];
                NSString *obj_dateupdate = [row objectForKey:@"date_updated"];
                
                int rows=0;
                NSString *temp = [[NSString alloc] initWithFormat:@"SELECT * FROM theme_object WHERE id=%@ ",themeobjid];
                //NSLog(@"temp:: %@", temp);
                const char *sqlStatement = [temp UTF8String];
                sqlite3_stmt *compiledStatement;
                if(sqlite3_prepare_v2(database, sqlStatement, -1, &compiledStatement, NULL) == SQLITE_OK) {
                    // Loop through the results and add them to the feeds array
                    
                    if (sqlite3_step(compiledStatement) == SQLITE_ERROR) {
                        NSAssert1(0,@"Error when counting rows  %s",sqlite3_errmsg(database));
                    } else {
                        rows = sqlite3_column_int(compiledStatement, 0);
                        //NSLog(@"SQLite Rows: %i", rows);
                    }
                    
                    NSString *obj_remark;
                    if([row objectForKey:@"remark"] == (NSString*)[NSNull null]) {
                        obj_remark = @"";
                    } else {
                        obj_remark = [[row objectForKey:@"remark"] stringByReplacingOccurrencesOfString:@"'" withString:@"''"];
                    }
                    
                    NSString *objvalue;
                    if([row objectForKey:@"object_value"] == (NSString*)[NSNull null]) {
                        objvalue = @"";
                    } else {
                        objvalue = [[row objectForKey:@"object_value"] stringByReplacingOccurrencesOfString:@"'" withString:@"''"];
                    }
                    
                    //while(sqlite3_step(compiledStatement) == SQLITE_ROW) {
                    if (rows > 0) {
                        //NSLog(@"database:::::::update conferencestheme_objecttheme_object");
                        static sqlite3_stmt *compiledupdateStatement;
                        sqlite3_exec(database, [[NSString stringWithFormat:@"update theme_object SET theme_code='%@', slot_code='%@', page_id=%@, object_type='%@', object_value='%@', attribute='%@', action_type='%@', action_value='%@', user_id=%@, remark='%@', date_updated='%@', lastsync_date=%@ where id='%@' ", obj_themecode, obj_slotcode, obj_pageid, objtype, objvalue, obj_attribute, obj_actiontype, obj_actionvalue, obj_userid, obj_remark, obj_dateupdate, timeStampValueSync, themeobjid] UTF8String], NULL, NULL, NULL);
                        sqlite3_finalize(compiledupdateStatement);
                    } else {
                        //NSLog(@"database:::::::insert into conferencestheme_object");
                        static sqlite3_stmt *compiledinsertStatement;
                        sqlite3_exec(database, [[NSString stringWithFormat:@"insert into theme_object (id, theme_code, slot_code, page_id, object_type, object_value, attribute, action_type, action_value, user_id, remark, date_updated, lastsync_date) values ('%@', '%@', '%@', '%@', '%@', '%@', '%@', '%@', '%@', '%@', '%@', '%@', '%@')", themeobjid, obj_themecode, obj_slotcode, obj_pageid, objtype, objvalue, obj_attribute, obj_actiontype, obj_actionvalue, obj_userid, obj_remark, obj_dateupdate, timeStampValueSync] UTF8String], NULL, NULL, NULL);
                        sqlite3_finalize(compiledinsertStatement);
                    }
                }
                // Release the compiled statement from memory
                sqlite3_finalize(compiledStatement);
            }
            
            //NSLog(@"database:::::::Delete not synced posters");
            static sqlite3_stmt *compiledinsertStatement3a;
            sqlite3_exec(database, [[NSString stringWithFormat:@"Delete from theme_object where lastsync_date<>%@", timeStampValueSync] UTF8String], NULL, NULL, NULL);
            sqlite3_finalize(compiledinsertStatement3a);
        }
        
        NSString *ReadData2;
        //NSLog(@"here");
        if ((firsttime && ![self isConnectionAvailable]) || (firsttime && FailWithError)) {
            TxtFiles *readFile2 = [[TxtFiles alloc] init];
            ReadData2 = [readFile2 readFromFile:readFile2.setFilenamethemeplugin];
        } else {
            if ([currentRequestMenu isEqualToString:@"themeplugin"]) {
                ReadData2 = response;
            } else {
                ReadData2 = @"[{}]";
            }
        }
        
        if (!([ReadData2 isEqualToString:@"[{}]"] || [ReadData2 isEqualToString:@""])) {
            SBJsonParser *jsonParser2 = [SBJsonParser new];
            NSDictionary *jsonData2 = (NSDictionary *) [jsonParser2 objectWithString:ReadData2 error:nil];
            
            //NSLog(@"jsonData2: %@", jsonData2);
            
            NSMutableDictionary *dictionary3 = [jsonData2 mutableCopy];
            //NSLog(@"dictionary1:: %@", dictionary1);
            for(NSMutableDictionary *row in dictionary3){
                NSString *pluginid = [row objectForKey:@"id"];
                NSString *plu_pageid = [row objectForKey:@"page_id"];
                NSString *plu_objid = [row objectForKey:@"object_id"];
                NSString *plu_id = [row objectForKey:@"plugin_id"];
                NSString *plu_type = [row objectForKey:@"plugin_type"];
                NSString *plu_userid = [row objectForKey:@"user_id"];
                NSString *plu_dateupded = [row objectForKey:@"date_updated"];
                
                int rows=0;
                NSString *temp = [[NSString alloc] initWithFormat:@"SELECT * FROM theme_object_plugin WHERE id=%@ ",pluginid];
                //NSLog(@"temp:: %@", temp);
                const char *sqlStatement = [temp UTF8String];
                sqlite3_stmt *compiledStatement;
                if(sqlite3_prepare_v2(database, sqlStatement, -1, &compiledStatement, NULL) == SQLITE_OK) {
                    // Loop through the results and add them to the feeds array
                    
                    if (sqlite3_step(compiledStatement) == SQLITE_ERROR) {
                        NSAssert1(0,@"Error when counting rows  %s",sqlite3_errmsg(database));
                    } else {
                        rows = sqlite3_column_int(compiledStatement, 0);
                        //NSLog(@"SQLite Rows: %i", rows);
                    }
                    
                    NSString *plu_value1;
                    if([row objectForKey:@"plugin_value1"] == (NSString*)[NSNull null]) {
                        plu_value1 = @"";
                    } else {
                        plu_value1 = [[row objectForKey:@"plugin_value1"] stringByReplacingOccurrencesOfString:@"'" withString:@"''"];
                    }
                    NSString *plu_value2;
                    if([row objectForKey:@"plugin_value2"] == (NSString*)[NSNull null]) {
                        plu_value2 = @"";
                    } else {
                        plu_value2 = [[row objectForKey:@"plugin_value2"] stringByReplacingOccurrencesOfString:@"'" withString:@"''"];
                    }
                    NSString *plu_value3;
                    if([row objectForKey:@"plugin_value3"] == (NSString*)[NSNull null]) {
                        plu_value3 = @"";
                    } else {
                        plu_value3 = [[row objectForKey:@"plugin_value3"] stringByReplacingOccurrencesOfString:@"'" withString:@"''"];
                    }
                    NSString *plu_remark;
                    if([row objectForKey:@"remark"] == (NSString*)[NSNull null]) {
                        plu_remark = @"";
                    } else {
                        plu_remark = [[row objectForKey:@"remark"] stringByReplacingOccurrencesOfString:@"'" withString:@"''"];
                    }
                    
                    //while(sqlite3_step(compiledStatement) == SQLITE_ROW) {
                    if (rows > 0) {
                        //NSLog(@"database:::::::update conferences");
                        static sqlite3_stmt *compiledupdateStatement;
                        sqlite3_exec(database, [[NSString stringWithFormat:@"update theme_object_plugin SET page_id='%@', object_id='%@', plugin_id='%@', plugin_type='%@', plugin_value1='%@', plugin_value2='%@', plugin_value3='%@', user_id=%@, remark='%@', date_updated='%@', lastsync_date='%@' where id='%@' ", plu_pageid, plu_objid, plu_id, plu_type, plu_value1, plu_value2, plu_value3, plu_userid, plu_remark, plu_dateupded, timeStampValueSync, pluginid] UTF8String], NULL, NULL, NULL);
                        sqlite3_finalize(compiledupdateStatement);
                    } else {
                        //NSLog(@"database:::::::insert into conferences");
                        static sqlite3_stmt *compiledinsertStatement;
                        sqlite3_exec(database, [[NSString stringWithFormat:@"insert into theme_object_plugin (id, page_id, object_id, plugin_type, plugin_value1, plugin_value2, plugin_value3, user_id, remark, date_updated, lastsync_date, plugin_id) values ('%@', '%@', '%@', '%@', '%@', '%@', '%@', '%@', '%@', '%@', '%@', '%@')", pluginid, plu_pageid, plu_objid, plu_type, plu_value1, plu_value2, plu_value3, plu_userid, plu_remark, plu_dateupded, timeStampValueSync, plu_id] UTF8String], NULL, NULL, NULL);
                        sqlite3_finalize(compiledinsertStatement);
                    }
                }
                // Release the compiled statement from memory
                sqlite3_finalize(compiledStatement);
            }
            
            //NSLog(@"database:::::::Delete not synced posters");
            static sqlite3_stmt *compiledinsertStatement3b;
            sqlite3_exec(database, [[NSString stringWithFormat:@"Delete from theme_object_plugin where lastsync_date<>%@", timeStampValueSync] UTF8String], NULL, NULL, NULL);
            sqlite3_finalize(compiledinsertStatement3b);
        }
        
        NSString *ReadData3;
        //NSLog(@"here");
        if ((firsttime && ![self isConnectionAvailable]) || (firsttime && FailWithError)) {
            TxtFiles *readFile3 = [[TxtFiles alloc] init];
            ReadData3 = [readFile3 readFromFile:readFile3.setFilenamethemecolor];
        } else {
            if ([currentRequestMenu isEqualToString:@"themecolor"]) {
                ReadData3 = response;
            } else {
                ReadData3 = @"[{}]";
            }
        }
        
        if (!([ReadData3 isEqualToString:@"[{}]"] || [ReadData3 isEqualToString:@""])) {
            SBJsonParser *jsonParser3 = [SBJsonParser new];
            NSDictionary *jsonData3 = (NSDictionary *) [jsonParser3 objectWithString:ReadData3 error:nil];
            
            //NSLog(@"jsonData3: %@", jsonData3);
            
            NSMutableDictionary *dictionary4 = [jsonData3 mutableCopy];
            //NSLog(@"dictionary1:: %@", dictionary1);
            for(NSMutableDictionary *row in dictionary4){
                NSString *t_themeid = [row objectForKey:@"id"];
                //NSString *t_name = [row objectForKey:@"name"];
                NSString *t_color = [row objectForKey:@"color"];
                NSString *t_color1 = [row objectForKey:@"color1"];
                NSString *t_color2 = [row objectForKey:@"color2"];
                NSString *t_color3 = [row objectForKey:@"color3"];
                NSString *t_color4 = [row objectForKey:@"color4"];
                NSString *t_dateupdate = [row objectForKey:@"date_updated"];
                
                int rows=0;
                NSString *temp = [[NSString alloc] initWithFormat:@"SELECT * FROM theme_color WHERE id=%@ ",t_themeid];
                //NSLog(@"temp:: %@", temp);
                const char *sqlStatement = [temp UTF8String];
                sqlite3_stmt *compiledStatement;
                if(sqlite3_prepare_v2(database, sqlStatement, -1, &compiledStatement, NULL) == SQLITE_OK) {
                    // Loop through the results and add them to the feeds array
                    
                    if (sqlite3_step(compiledStatement) == SQLITE_ERROR) {
                        NSAssert1(0,@"Error when counting rows  %s",sqlite3_errmsg(database));
                    } else {
                        rows = sqlite3_column_int(compiledStatement, 0);
                        //NSLog(@"SQLite Rows: %i", rows);
                    }
                    
                    NSString *t_name;
                    if([row objectForKey:@"name"] == (NSString*)[NSNull null]) {
                        t_name = @"";
                    } else {
                        t_name = [[row objectForKey:@"name"] stringByReplacingOccurrencesOfString:@"'" withString:@"''"];
                    }
                    
                    //while(sqlite3_step(compiledStatement) == SQLITE_ROW) {
                    if (rows > 0) {
                        //NSLog(@"database:::::::update conferences");
                        static sqlite3_stmt *compiledupdateStatement;
                        sqlite3_exec(database, [[NSString stringWithFormat:@"update theme_color SET name='%@', color='%@', color1='%@', color2='%@', color3='%@', color4='%@', lastsync_date=%@, date_updated='%@' where id='%@' ", t_name, t_color, t_color1, t_color2, t_color3, t_color4, timeStampValueSync, t_dateupdate, t_themeid] UTF8String], NULL, NULL, NULL);
                        sqlite3_finalize(compiledupdateStatement);
                    } else {
                        //NSLog(@"database:::::::insert into conferences");
                        static sqlite3_stmt *compiledinsertStatement;
                        sqlite3_exec(database, [[NSString stringWithFormat:@"insert into theme_color (id, name, color, color1, color2, color3, color4, lastsync_date, date_updated) values ('%@', '%@', '%@', '%@', '%@', '%@', '%@', '%@', '%@')", t_themeid, t_name, t_color, t_color1, t_color2, t_color3, t_color4, timeStampValueSync, t_dateupdate] UTF8String], NULL, NULL, NULL);
                        sqlite3_finalize(compiledinsertStatement);
                    }
                }
                // Release the compiled statement from memory
                sqlite3_finalize(compiledStatement);
            }
            
            //NSLog(@"database:::::::Delete not synced posters");
            static sqlite3_stmt *compiledinsertStatement3c;
            sqlite3_exec(database, [[NSString stringWithFormat:@"Delete from theme_color where lastsync_date<>%@", timeStampValueSync] UTF8String], NULL, NULL, NULL);
            sqlite3_finalize(compiledinsertStatement3c);
        }
        
        /*TxtFiles *readFile4 = [[TxtFiles alloc] init];
         NSString *ReadData4 = [readFile4 readFromFile:readFile4.setFilenameuser];
         
         SBJsonParser *jsonParser4 = [SBJsonParser new];
         NSDictionary *jsonData4 = (NSDictionary *) [jsonParser4 objectWithString:ReadData4 error:nil];
         
         //NSLog(@"jsonData4: %@", jsonData4);
         
         NSMutableDictionary *dictionary5 = [jsonData4 mutableCopy];
         //NSLog(@"dictionary1:: %@", dictionary1);
         for(NSMutableDictionary *row in dictionary5){
         NSString *userid = [row objectForKey:@"userid"];
         NSString *user_status = [row objectForKey:@"status"];
         NSString *user_package = [row objectForKey:@"package"];
         //NSString *user_desc = [row objectForKey:@"desc"];
         
         int rows=0;
         NSString *temp = [[NSString alloc] initWithFormat:@"SELECT * FROM user WHERE userid=%@ ",userid];
         //NSLog(@"temp:: %@", temp);
         const char *sqlStatement = [temp UTF8String];
         sqlite3_stmt *compiledStatement;
         if(sqlite3_prepare_v2(database, sqlStatement, -1, &compiledStatement, NULL) == SQLITE_OK) {
         // Loop through the results and add them to the feeds array
         
         if (sqlite3_step(compiledStatement) == SQLITE_ERROR) {
         NSAssert1(0,@"Error when counting rows  %s",sqlite3_errmsg(database));
         } else {
         rows = sqlite3_column_int(compiledStatement, 0);
         //NSLog(@"SQLite Rows: %i", rows);
         }
         
         NSString *user_desc;
         if([row objectForKey:@"desc"] == (NSString*)[NSNull null]) {
         user_desc = @"";
         } else {
         user_desc = [[row objectForKey:@"desc"] stringByReplacingOccurrencesOfString:@"'" withString:@"''"];
         }
         
         //while(sqlite3_step(compiledStatement) == SQLITE_ROW) {
         if (rows > 0) {
         //NSLog(@"database:::::::update conferences");
         static sqlite3_stmt *compiledupdateStatement;
         sqlite3_exec(database, [[NSString stringWithFormat:@"update user SET status='%@', package='%@', desc='%@', lastsync_date=%@ where userid='%@' ", user_status, user_package, user_desc, timeStampValueSync, userid] UTF8String], NULL, NULL, NULL);
         sqlite3_finalize(compiledupdateStatement);
         } else {
         //NSLog(@"database:::::::insert into conferences");
         static sqlite3_stmt *compiledinsertStatement;
         sqlite3_exec(database, [[NSString stringWithFormat:@"insert into user (userid, status, package, desc, lastsync_date) values ('%@', '%@', '%@', '%@', '%@')", userid, user_status, user_package, user_desc, timeStampValueSync] UTF8String], NULL, NULL, NULL);
         sqlite3_finalize(compiledinsertStatement);
         }
         }
         // Release the compiled statement from memory
         sqlite3_finalize(compiledStatement);
         
         }
         //NSLog(@"database:::::::Delete not synced posters");
         static sqlite3_stmt *compiledinsertStatement3d;
         sqlite3_exec(database, [[NSString stringWithFormat:@"Delete from user where lastsync_date<>%@", timeStampValueSync] UTF8String], NULL, NULL, NULL);
         sqlite3_finalize(compiledinsertStatement3d); */
        
        NSString *ReadData5;
        //NSLog(@"here");
        if ((firsttime && ![self isConnectionAvailable]) || (firsttime && FailWithError)) {
            TxtFiles *readFile5 = [[TxtFiles alloc] init];
            ReadData5 = [readFile5 readFromFile:readFile5.setFilenameappsetup];
        } else {
            if ([currentRequestMenu isEqualToString:@"appsetup"]) {
                ReadData5 = response;
            } else {
                ReadData5 = @"[{}]";
            }
        }
        
        if (!([ReadData5 isEqualToString:@"[{}]"] || [ReadData5 isEqualToString:@""])) {
            SBJsonParser *jsonParser5 = [SBJsonParser new];
            NSDictionary *jsonData5 = (NSDictionary *) [jsonParser5 objectWithString:ReadData5 error:nil];
            
            //NSLog(@"jsonData4: %@", jsonData4);
            
            NSMutableDictionary *dictionary6 = [jsonData5 mutableCopy];
            //NSLog(@"app_setup:: %@", dictionary6);
            for(NSMutableDictionary *row in dictionary6){
                NSString *appsetup_id = [row objectForKey:@"id"];
                NSString *name = [row objectForKey:@"name"];
                //NSString *icon_name = [row objectForKey:@"icon_name"];
                NSString *icon_pic = [row objectForKey:@"icon_pic"];
                //NSString *icon_pic_name = [row objectForKey:@"icon_pic_name"];
                NSString *splash_page = [row objectForKey:@"splash_page"];
                //NSString *splash_page_name = [row objectForKey:@"splash_page_name"];
                NSString *themecolor = [row objectForKey:@"themecolor"];
                NSString *user_id = [row objectForKey:@"user_id"];
                NSString *date_updated = [row objectForKey:@"date_updated"];
                NSString *show_title = [row objectForKey:@"show_title"];
                NSString *background = [row objectForKey:@"background"];
                
                int rows=0;
                NSString *temp = [[NSString alloc] initWithFormat:@"SELECT * FROM app_setup WHERE id=%@ ",appsetup_id];
                //NSLog(@"temp:: %@", temp);
                const char *sqlStatement = [temp UTF8String];
                sqlite3_stmt *compiledStatement;
                if(sqlite3_prepare_v2(database, sqlStatement, -1, &compiledStatement, NULL) == SQLITE_OK) {
                    // Loop through the results and add them to the feeds array
                    
                    if (sqlite3_step(compiledStatement) == SQLITE_ERROR) {
                        NSAssert1(0,@"Error when counting rows  %s",sqlite3_errmsg(database));
                    } else {
                        rows = sqlite3_column_int(compiledStatement, 0);
                        //NSLog(@"SQLite Rows: %i", rows);
                    }
                    
                    NSString *icon_name;
                    if([row objectForKey:@"icon_name"] == (NSString*)[NSNull null]) {
                        icon_name = @"";
                    } else {
                        icon_name = [[row objectForKey:@"icon_name"] stringByReplacingOccurrencesOfString:@"'" withString:@"''"];
                    }
                    
                    NSString *icon_pic_name;
                    if([row objectForKey:@"icon_pic"] == (NSString*)[NSNull null]) {
                        icon_pic_name = @"";
                    } else {
                        icon_pic_name = [[row objectForKey:@"icon_pic"] stringByReplacingOccurrencesOfString:@"'" withString:@"''"];
                    }
                    
                    NSString *splash_page_name;
                    if([row objectForKey:@"splash_page"] == (NSString*)[NSNull null]) {
                        splash_page_name = @"";
                    } else {
                        splash_page_name = [[row objectForKey:@"splash_page"] stringByReplacingOccurrencesOfString:@"'" withString:@"''"];
                    }
                    
                    NSString *ios_link_name;
                    if([row objectForKey:@"ios_link"] == (NSString*)[NSNull null]) {
                        ios_link_name = @"";
                    } else {
                        ios_link_name = [[row objectForKey:@"ios_link"] stringByReplacingOccurrencesOfString:@"'" withString:@"''"];
                    }
                    
                    NSString *titlebar_img;
                    if([row objectForKey:@"titlebar_img"] == (NSString*)[NSNull null]) {
                        titlebar_img = @"";
                    } else {
                        titlebar_img = [[row objectForKey:@"titlebar_img"] stringByReplacingOccurrencesOfString:@"'" withString:@"''"];
                    }
                    
                    NSString *termscons_url;
                    if([row objectForKey:@"termscons_url"] == (NSString*)[NSNull null]) {
                        termscons_url = @"";
                    } else {
                        termscons_url = [[row objectForKey:@"termscons_url"] stringByReplacingOccurrencesOfString:@"'" withString:@"''"];
                    }
                    //NSLog(@"ios_link %@", ios_link_name);
                    //while(sqlite3_step(compiledStatement) == SQLITE_ROW) {
                    if (rows > 0) {
                        //NSLog(@"database:::::::update conferences");
                        static sqlite3_stmt *compiledupdateStatement;
                        sqlite3_exec(database, [[NSString stringWithFormat:@"update app_setup SET user_id='%@', name='%@', icon_name='%@', icon_pic='%@', icon_pic_name=%@, splash_page='%@', splash_page_name='%@', themecolor='%@', lastsync_date='%@', date_updated='%@', ios_link='%@', termscons_url='%@', titlebar_img='%@', show_title='%@', background='%@' where id='%@' ", user_id, name, icon_name, icon_pic, icon_pic_name, splash_page, splash_page_name, themecolor, timeStampValueSync, date_updated, ios_link_name, termscons_url, titlebar_img, show_title, background, appsetup_id] UTF8String], NULL, NULL, NULL);
                        NSLog(@"compiledupdateStatement: %@", compiledupdateStatement);
                        sqlite3_finalize(compiledupdateStatement);
                    } else {
                        //NSLog(@"database:::::::insert into app_setup %@", [NSString stringWithFormat:@"insert into app_setup (id, user_id, name, icon_name, icon_pic, icon_pic_name, splash_page, splash_page_name, themecolor, lastsync_date, date_updated, ios_link) values ('%@', '%@', '%@', '%@', '%@', '%@', '%@', '%@', '%@', '%@', '%@', '%@')", appsetup_id, user_id, name, icon_name, icon_pic, icon_pic_name, splash_page, splash_page_name, themecolor, timeStampValueSync, date_updated, ios_link_name]);
                        static sqlite3_stmt *compiledinsertStatement;
                        sqlite3_exec(database, [[NSString stringWithFormat:@"insert into app_setup (id, user_id, name, icon_name, icon_pic, icon_pic_name, splash_page, splash_page_name, themecolor, lastsync_date, date_updated, ios_link, termscons_url, titlebar_img, show_title, background) values ('%@', '%@', '%@', '%@', '%@', '%@', '%@', '%@', '%@', '%@', '%@', '%@', '%@', '%@', '%@', '%@')", appsetup_id, user_id, name, icon_name, icon_pic, icon_pic_name, splash_page, splash_page_name, themecolor, timeStampValueSync, date_updated, ios_link_name, termscons_url, titlebar_img, show_title, background] UTF8String], NULL, NULL, NULL);
                        //NSLog(@"compiledinsertStatement: %s", [[NSString stringWithFormat:@"insert into app_setup (id, user_id, name, icon_name, icon_pic, icon_pic_name, splash_page, splash_page_name, themecolor, lastsync_date, date_updated, ios_link, termscons_url, titlebar_img, show_title) values ('%@', '%@', '%@', '%@', '%@', '%@', '%@', '%@', '%@', '%@', '%@', '%@', '%@', '%@', '%@')", appsetup_id, user_id, name, icon_name, icon_pic, icon_pic_name, splash_page, splash_page_name, themecolor, timeStampValueSync, date_updated, ios_link_name, termscons_url, titlebar_img, show_title] UTF8String]);
                        sqlite3_finalize(compiledinsertStatement);
                    }
                }
                // Release the compiled statement from memory
                sqlite3_finalize(compiledStatement);
                
            }
            //NSLog(@"database:::::::Delete not synced posters");
            static sqlite3_stmt *compiledinsertStatement3e;
            sqlite3_exec(database, [[NSString stringWithFormat:@"Delete from app_setup where lastsync_date<>%@", timeStampValueSync] UTF8String], NULL, NULL, NULL);
            sqlite3_finalize(compiledinsertStatement3e);
        }
        
        NSString *ReadData6;
        //NSLog(@"here");
        if ((firsttime && ![self isConnectionAvailable]) || (firsttime && FailWithError)) {
            TxtFiles *readFile6 = [[TxtFiles alloc] init];
            ReadData6 = [readFile6 readFromFile:readFile6.setFilenamecontentcategory];
        } else {
            if ([currentRequestMenu isEqualToString:@"contentcategory"]) {
                ReadData6 = response;
            } else {
                ReadData6 = @"[{}]";
            }
        }
        
        if (!([ReadData6 isEqualToString:@"[{}]"] || [ReadData6 isEqualToString:@""])) {
            SBJsonParser *jsonParser6 = [SBJsonParser new];
            NSDictionary *jsonData6 = (NSDictionary *) [jsonParser6 objectWithString:ReadData6 error:nil];
            
            //NSLog(@"jsonData6: %@", jsonData6);
            
            NSMutableDictionary *dictionary7 = [jsonData6 mutableCopy];
            //NSLog(@"content_category:: %@", dictionary7);
            for(NSMutableDictionary *row in dictionary7){
                NSString *cc_id = [row objectForKey:@"id"];
                //NSString *ccname = [row objectForKey:@"name"];
                NSString *ccplugin_id = [row objectForKey:@"plugin_id"];
                NSString *cclist_template = [row objectForKey:@"list_template"];
                NSString *ccuser_id = [row objectForKey:@"user_id"];
                NSString *ccis_hidden = [row objectForKey:@"is_hidden"];
                NSString *ccdate_updated = [row objectForKey:@"date_updated"];
                NSString *ctop_id = [row objectForKey:@"top_id"];
                NSString *ccallow_favorite = [row objectForKey:@"allow_favorite"];
                NSString *ccallow_comments = [row objectForKey:@"allow_comments"];
                NSString *ccparent_id = [row objectForKey:@"parent_id"];
                
                int rows=0;
                NSString *temp = [[NSString alloc] initWithFormat:@"SELECT * FROM content_category WHERE id=%@ ",cc_id];
                //NSLog(@"tempcontent_category:: %@", temp);
                const char *sqlStatement = [temp UTF8String];
                sqlite3_stmt *compiledStatement;
                if(sqlite3_prepare_v2(database, sqlStatement, -1, &compiledStatement, NULL) == SQLITE_OK) {
                    // Loop through the results and add them to the feeds array
                    
                    if (sqlite3_step(compiledStatement) == SQLITE_ERROR) {
                        NSAssert1(0,@"Error when counting rows  %s",sqlite3_errmsg(database));
                    } else {
                        rows = sqlite3_column_int(compiledStatement, 0);
                        //NSLog(@"SQLite Rows: %i", rows);
                    }
                    
                    NSString *ccname;
                    if([row objectForKey:@"name"] == (NSString*)[NSNull null]) {
                        ccname = @"";
                    } else {
                        ccname = [[row objectForKey:@"name"] stringByReplacingOccurrencesOfString:@"'" withString:@"''"];
                    }
                    
                    //while(sqlite3_step(compiledStatement) == SQLITE_ROW) {
                    if (rows > 0) {
                        //NSLog(@"database:::::::update conferences");
                        static sqlite3_stmt *compiledupdateStatementcat;
                        sqlite3_exec(database, [[NSString stringWithFormat:@"update content_category SET name='%@', plugin_id='%@', list_template='%@', user_id='%@', is_hidden=%@, date_updated='%@', lastsync_date='%@', top_id='%@', allow_favorite='%@', allow_comments='%@', parent_id='%@' where id='%@' ", ccname, ccplugin_id, cclist_template, ccuser_id, ccis_hidden, ccdate_updated, timeStampValueSync, ctop_id, ccallow_favorite, ccallow_comments, ccparent_id, cc_id] UTF8String], NULL, NULL, NULL);
                        sqlite3_finalize(compiledupdateStatementcat);
                    } else {
                        //NSLog(@"database:::::::insert into content_category");
                        static sqlite3_stmt *compiledinsertStatementcat;
                        sqlite3_exec(database, [[NSString stringWithFormat:@"insert into content_category (id, name, plugin_id, list_template, user_id, is_hidden, date_updated, lastsync_date, top_id, allow_favorite, allow_comments, parent_id) values ('%@', '%@', '%@', '%@', '%@', '%@', '%@', '%@', '%@', '%@', '%@', '%@')", cc_id, ccname, ccplugin_id, cclist_template, ccuser_id, ccis_hidden, ccdate_updated, timeStampValueSync, ctop_id, ccallow_favorite, ccallow_comments, ccparent_id] UTF8String], NULL, NULL, NULL);
                        //NSLog(@"compiledinsertStatement: %@", compiledinsertStatementcat);
                        sqlite3_finalize(compiledinsertStatementcat);
                    }
                }
                // Release the compiled statement from memory
                sqlite3_finalize(compiledStatement);
                
            }
            //NSLog(@"database:::::::Delete not synced posters");
            static sqlite3_stmt *compiledinsertStatement3f;
            sqlite3_exec(database, [[NSString stringWithFormat:@"Delete from content_category where lastsync_date<>%@", timeStampValueSync] UTF8String], NULL, NULL, NULL);
            sqlite3_finalize(compiledinsertStatement3f);
        }
        
        NSString *ReadData7;
        //NSLog(@"here: %@", currentRequestMenu);
        if ((firsttime && ![self isConnectionAvailable]) || (firsttime && FailWithError)) {
            TxtFiles *readFile7 = [[TxtFiles alloc] init];
            ReadData7 = [readFile7 readFromFile:readFile7.setFilenamecontents];
            //NSLog(@"firsttime");
        } else {
            if ([currentRequestMenu isEqualToString:@"contents"]) {
                ReadData7 = response;
                //NSLog(@"ReadData7: %@", ReadData7);
            } else {
                ReadData7 = @"[{}]";
            }
        }
        
        if (!([ReadData7 isEqualToString:@"[{}]"] || [ReadData7 isEqualToString:@""])) {
            SBJsonParser *jsonParser7 = [SBJsonParser new];
            NSDictionary *jsonData7 = (NSDictionary *) [jsonParser7 objectWithString:ReadData7 error:nil];
            
            //NSLog(@"jsonData6: %@", jsonData6);
            
            NSMutableDictionary *dictionary8 = [jsonData7 mutableCopy];
            //NSLog(@"contents:: %@", dictionary8);
            for(NSMutableDictionary *row in dictionary8){
                NSString *cont_id = [row objectForKey:@"id"];
                NSString *contcategory_id = [row objectForKey:@"category_id"];
                NSString *contuser_id = [row objectForKey:@"user_id"];
                NSString *contdate_updated = [row objectForKey:@"date_updated"];
                NSString *contorderno = [row objectForKey:@"orderno"];
                
                int rows=0;
                NSString *temp = [[NSString alloc] initWithFormat:@"SELECT * FROM contents WHERE id=%@ ",cont_id];
                //NSLog(@"temp:: %@", temp);
                const char *sqlStatement = [temp UTF8String];
                sqlite3_stmt *compiledStatement;
                if(sqlite3_prepare_v2(database, sqlStatement, -1, &compiledStatement, NULL) == SQLITE_OK) {
                    // Loop through the results and add them to the feeds array
                    
                    if (sqlite3_step(compiledStatement) == SQLITE_ERROR) {
                        NSAssert1(0,@"Error when counting rows  %s",sqlite3_errmsg(database));
                    } else {
                        rows = sqlite3_column_int(compiledStatement, 0);
                        //NSLog(@"SQLite Rows: %i", rows);
                    }
                    
                    NSString *conttitle;
                    if([row objectForKey:@"title"] == (NSString*)[NSNull null]) {
                        conttitle = @"";
                    } else {
                        conttitle = [[row objectForKey:@"title"] stringByReplacingOccurrencesOfString:@"'" withString:@"''"];
                    }
                    
                    NSString *contcall_title;
                    if([row objectForKey:@"call_title"] == (NSString*)[NSNull null]) {
                        contcall_title = @"";
                    } else {
                        contcall_title = [[row objectForKey:@"call_title"] stringByReplacingOccurrencesOfString:@"'" withString:@"''"];
                    }
                    
                    NSString *contemail_title;
                    if([row objectForKey:@"email_title"] == (NSString*)[NSNull null]) {
                        contemail_title = @"";
                    } else {
                        contemail_title = [[row objectForKey:@"email_title"] stringByReplacingOccurrencesOfString:@"'" withString:@"''"];
                    }
                    
                    NSString *contemail;
                    if([row objectForKey:@"email"] == (NSString*)[NSNull null]) {
                        contemail = @"";
                    } else {
                        contemail = [[row objectForKey:@"email"] stringByReplacingOccurrencesOfString:@"'" withString:@"''"];
                    }
                    
                    NSString *contdetails;
                    if([row objectForKey:@"details"] == (NSString*)[NSNull null]) {
                        contdetails = @"";
                    } else {
                        contdetails = [[row objectForKey:@"details"] stringByReplacingOccurrencesOfString:@"'" withString:@"''"];
                    }
                    
                    NSString *contlink_title;
                    if([row objectForKey:@"link_title"] == (NSString*)[NSNull null]) {
                        contlink_title = @"";
                    } else {
                        contlink_title = [[row objectForKey:@"link_title"] stringByReplacingOccurrencesOfString:@"'" withString:@"''"];
                    }
                    
                    NSString *contlink_url;
                    if([row objectForKey:@"link_url"] == (NSString*)[NSNull null]) {
                        contlink_url = @"";
                    } else {
                        contlink_url = [[row objectForKey:@"link_url"] stringByReplacingOccurrencesOfString:@"'" withString:@"''"];
                    }
                    
                    NSString *contmap_title;
                    if([row objectForKey:@"map_title"] == (NSString*)[NSNull null]) {
                        contmap_title = @"";
                    } else {
                        contmap_title = [[row objectForKey:@"map_title"] stringByReplacingOccurrencesOfString:@"'" withString:@"''"];
                    }
                    
                    NSString *contmap_lat;
                    if([row objectForKey:@"map_lat"] == (NSString*)[NSNull null]) {
                        contmap_lat = @"";
                    } else {
                        contmap_lat = [[row objectForKey:@"map_lat"] stringByReplacingOccurrencesOfString:@"'" withString:@"''"];
                    }
                    
                    NSString *contmap_lon;
                    if([row objectForKey:@"map_lon"] == (NSString*)[NSNull null]) {
                        contmap_lon = @"";
                    } else {
                        contmap_lon = [[row objectForKey:@"map_lon"] stringByReplacingOccurrencesOfString:@"'" withString:@"''"];
                    }
                    
                    NSString *contvideo_title;
                    if([row objectForKey:@"video_title"] == (NSString*)[NSNull null]) {
                        contvideo_title = @"";
                    } else {
                        contvideo_title = [[row objectForKey:@"video_title"] stringByReplacingOccurrencesOfString:@"'" withString:@"''"];
                    }
                    
                    NSString *contvideo_url;
                    if([row objectForKey:@"video_url"] == (NSString*)[NSNull null]) {
                        contvideo_url = @"";
                    } else {
                        contvideo_url = [[row objectForKey:@"video_url"] stringByReplacingOccurrencesOfString:@"'" withString:@"''"];
                    }
                    
                    NSString *contfb_title;
                    if([row objectForKey:@"fb_title"] == (NSString*)[NSNull null]) {
                        contfb_title = @"";
                    } else {
                        contfb_title = [[row objectForKey:@"fb_title"] stringByReplacingOccurrencesOfString:@"'" withString:@"''"];
                    }
                    
                    NSString *contfb_url;
                    if([row objectForKey:@"fb_url"] == (NSString*)[NSNull null]) {
                        contfb_url = @"";
                    } else {
                        contfb_url = [[row objectForKey:@"fb_url"] stringByReplacingOccurrencesOfString:@"'" withString:@"''"];
                    }
                    
                    NSString *conttelephone;
                    if([row objectForKey:@"telephone"] == (NSString*)[NSNull null]) {
                        conttelephone = @"";
                    } else {
                        conttelephone = [[row objectForKey:@"telephone"] stringByReplacingOccurrencesOfString:@"'" withString:@"''"];
                    }
                    
                    NSString *contimage;
                    if([row objectForKey:@"image"] == (NSString*)[NSNull null]) {
                        contimage = @"";
                    } else {
                        contimage = [[row objectForKey:@"image"] stringByReplacingOccurrencesOfString:@"'" withString:@"''"];
                    }
                    
                    NSString *contthumbnail;
                    if([row objectForKey:@"thumbnail"] == (NSString*)[NSNull null]) {
                        contthumbnail = @"";
                    } else {
                        contthumbnail = [[row objectForKey:@"thumbnail"] stringByReplacingOccurrencesOfString:@"'" withString:@"''"];
                    }
                    
                    NSString *contpricetitle;
                    if([row objectForKey:@"price_title"] == (NSString*)[NSNull null]) {
                        contpricetitle = @"";
                    } else {
                        contpricetitle = [[row objectForKey:@"price_title"] stringByReplacingOccurrencesOfString:@"'" withString:@"''"];
                    }
                    
                    NSString *contprice;
                    if([row objectForKey:@"price"] == (NSString*)[NSNull null]) {
                        contprice = @"";
                    } else {
                        contprice = [[row objectForKey:@"price"] stringByReplacingOccurrencesOfString:@"'" withString:@"''"];
                    }
                    
                    NSString *contvar1title;
                    if([row objectForKey:@"var1_title"] == (NSString*)[NSNull null]) {
                        contvar1title = @"";
                    } else {
                        contvar1title = [[row objectForKey:@"var1_title"] stringByReplacingOccurrencesOfString:@"'" withString:@"''"];
                    }
                    
                    NSString *contvar1;
                    if([row objectForKey:@"var1"] == (NSString*)[NSNull null]) {
                        contvar1 = @"";
                    } else {
                        contvar1 = [[row objectForKey:@"var1"] stringByReplacingOccurrencesOfString:@"'" withString:@"''"];
                    }
                    
                    NSString *contvar2title;
                    if([row objectForKey:@"var2_title"] == (NSString*)[NSNull null]) {
                        contvar2title = @"";
                    } else {
                        contvar2title = [[row objectForKey:@"var2_title"] stringByReplacingOccurrencesOfString:@"'" withString:@"''"];
                    }
                    
                    NSString *contvar2;
                    if([row objectForKey:@"var2"] == (NSString*)[NSNull null]) {
                        contvar2 = @"";
                    } else {
                        contvar2 = [[row objectForKey:@"var2"] stringByReplacingOccurrencesOfString:@"'" withString:@"''"];
                    }
                    
                    NSString *contvar3title;
                    if([row objectForKey:@"var3_title"] == (NSString*)[NSNull null]) {
                        contvar3title = @"";
                    } else {
                        contvar3title = [[row objectForKey:@"var3_title"] stringByReplacingOccurrencesOfString:@"'" withString:@"''"];
                    }
                    
                    NSString *contvar3;
                    if([row objectForKey:@"var3"] == (NSString*)[NSNull null]) {
                        contvar3 = @"";
                    } else {
                        contvar3 = [[row objectForKey:@"var3"] stringByReplacingOccurrencesOfString:@"'" withString:@"''"];
                    }
                    
                    NSString *contvar4title;
                    if([row objectForKey:@"var4_title"] == (NSString*)[NSNull null]) {
                        contvar4title = @"";
                    } else {
                        contvar4title = [[row objectForKey:@"var4_title"] stringByReplacingOccurrencesOfString:@"'" withString:@"''"];
                    }
                    
                    NSString *contvar4;
                    if([row objectForKey:@"var4"] == (NSString*)[NSNull null]) {
                        contvar4 = @"";
                    } else {
                        contvar4 = [[row objectForKey:@"var4"] stringByReplacingOccurrencesOfString:@"'" withString:@"''"];
                    }
                    
                    NSString *contvar5title;
                    if([row objectForKey:@"var5_title"] == (NSString*)[NSNull null]) {
                        contvar5title = @"";
                    } else {
                        contvar5title = [[row objectForKey:@"var5_title"] stringByReplacingOccurrencesOfString:@"'" withString:@"''"];
                    }
                    
                    NSString *contvar5;
                    if([row objectForKey:@"var5"] == (NSString*)[NSNull null]) {
                        contvar5 = @"";
                    } else {
                        contvar5 = [[row objectForKey:@"var5"] stringByReplacingOccurrencesOfString:@"'" withString:@"''"];
                    }
                    
                    //while(sqlite3_step(compiledStatement) == SQLITE_ROW) {
                    if (rows > 0) {
                        //NSLog(@"database:::::::update conferences");
                        static sqlite3_stmt *compiledupdateStatementcont;
                        sqlite3_exec(database, [[NSString stringWithFormat:@"update contents SET title='%@', category_id='%@', image='%@', thumbnail='%@', call_title='%@', telephone='%@', email_title='%@', email='%@', details='%@', link_title='%@', link_url='%@', map_title='%@', map_lat='%@', map_lon='%@', video_title='%@', video_url='%@', fb_title='%@', fb_url='%@', user_id='%@', orderno='%@', date_updated='%@', lastsync_date='%@', price_title='%@', price='%@', var1_title='%@', var1='%@', var2_title='%@', var2='%@', var3_title='%@', var3='%@', var4_title='%@', var4='%@', var5_title='%@', var5='%@' where id='%@' ", conttitle, contcategory_id, contimage, contthumbnail, contcall_title, conttelephone, contemail_title, contemail, contdetails, contlink_title, contlink_url, contmap_title, contmap_lat, contmap_lon, contvideo_title, contvideo_url, contfb_title, contfb_url, contuser_id, contorderno, contdate_updated, timeStampValueSync, contpricetitle, contprice, contvar1title, contvar1, contvar2title, contvar2, contvar3title, contvar3, contvar4title, contvar4, contvar5title, contvar5, cont_id] UTF8String], NULL, NULL, NULL);
                        //NSLog(@"compiledupdateStatementcont: %@", compiledupdateStatementcont);
                        sqlite3_finalize(compiledupdateStatementcont);
                    } else {
                        //NSLog(@"database:::::::insert into contents");
                        static sqlite3_stmt *compiledinsertStatementcont;
                        sqlite3_exec(database, [[NSString stringWithFormat:@"insert into contents (id, title, category_id, image, thumbnail, call_title, telephone, email_title, email, details, link_title, link_url, map_title, map_lat, map_lon, video_title, video_url, fb_title, fb_url, user_id, orderno, date_updated, lastsync_date, price_title, price, var1_title, var1, var2_title, var2, var3_title, var3, var4_title, var4, var5_title, var5) values ('%@', '%@', '%@', '%@', '%@', '%@', '%@', '%@', '%@', '%@', '%@', '%@', '%@', '%@', '%@', '%@', '%@', '%@', '%@', '%@', '%@', '%@', '%@', '%@', '%@', '%@', '%@', '%@', '%@', '%@', '%@', '%@', '%@', '%@', '%@')", cont_id, conttitle, contcategory_id, contimage, contthumbnail, contcall_title, conttelephone, contemail_title, contemail, contdetails, contlink_title, contlink_url, contmap_title, contmap_lat, contmap_lon, contvideo_title, contvideo_url, contfb_title, contfb_url, contuser_id, contorderno, contdate_updated, timeStampValueSync, contpricetitle, contprice, contvar1title, contvar1, contvar2title, contvar2, contvar3title, contvar3, contvar4title, contvar4, contvar5title, contvar5] UTF8String], NULL, NULL, NULL);
                        //NSLog(@"compiledinsertStatementcont: %@", compiledinsertStatementcont);
                        sqlite3_finalize(compiledinsertStatementcont);
                    }
                }
                // Release the compiled statement from memory
                sqlite3_finalize(compiledStatement);
                
            }
            //NSLog(@"database:::::::Delete not synced posters");
            static sqlite3_stmt *compiledinsertStatement3g;
            sqlite3_exec(database, [[NSString stringWithFormat:@"Delete from contents where lastsync_date<>%@", timeStampValueSync] UTF8String], NULL, NULL, NULL);
            sqlite3_finalize(compiledinsertStatement3g);
        }
        
        NSString *ReadData8;
        //NSLog(@"here");
        if ((firsttime && ![self isConnectionAvailable]) || (firsttime && FailWithError)) {
            TxtFiles *readFile8 = [[TxtFiles alloc] init];
            ReadData8 = [readFile8 readFromFile:readFile8.setFilenameibeacondevice];
        } else {
            if ([currentRequestMenu isEqualToString:@"ibeacondevice"]) {
                ReadData8 = response;
            } else {
                ReadData8 = @"[{}]";
            }
        }
        
        if (!([ReadData8 isEqualToString:@"[{}]"] || [ReadData8 isEqualToString:@""])) {
            SBJsonParser *jsonParser8 = [SBJsonParser new];
            NSDictionary *jsonData8 = (NSDictionary *) [jsonParser8 objectWithString:ReadData8 error:nil];
            
            //NSLog(@"jsonData6: %@", jsonData6);
            
            NSMutableDictionary *dictionary9 = [jsonData8 mutableCopy];
            //NSLog(@"ibeacondevice:: %@", dictionary9);
            for(NSMutableDictionary *row in dictionary9){
                NSString *ibdevice_id = [row objectForKey:@"id"];
                NSString *ibdevice_uuid = [row objectForKey:@"uuid"];
                NSString *ibdevice_major = [row objectForKey:@"major"];
                NSString *ibdevice_minor = [row objectForKey:@"minor"];
                //NSString *ibdevice_name = [row objectForKey:@"name"];
                //NSString *ibdevice_error_msg = [row objectForKey:@"error_msg"];
                NSString *ibdevice_userid = [row objectForKey:@"user_id"];
                NSString *ibdevice_status = [row objectForKey:@"status"];
                NSString *ibdevice_date_updated = [row objectForKey:@"date_updated"];
                
                int rows=0;
                NSString *temp = [[NSString alloc] initWithFormat:@"SELECT * FROM ibeacon_device WHERE id=%@ ",ibdevice_id];
                
                const char *sqlStatement = [temp UTF8String];
                sqlite3_stmt *compiledStatement;
                if(sqlite3_prepare_v2(database, sqlStatement, -1, &compiledStatement, NULL) == SQLITE_OK) {
                    // Loop through the results and add them to the feeds array
                    
                    if (sqlite3_step(compiledStatement) == SQLITE_ERROR) {
                        NSAssert1(0,@"Error when counting rows  %s",sqlite3_errmsg(database));
                    } else {
                        rows = sqlite3_column_int(compiledStatement, 0);
                        //NSLog(@"SQLite Rows: %i", rows);
                    }
                    
                    NSString *ibdevice_name;
                    if([row objectForKey:@"name"] == (NSString*)[NSNull null]) {
                        ibdevice_name = @"";
                    } else {
                        ibdevice_name = [[row objectForKey:@"name"] stringByReplacingOccurrencesOfString:@"'" withString:@"''"];
                    }
                    
                    NSString *ibdevice_error_msg;
                    if([row objectForKey:@"error_msg"] == (NSString*)[NSNull null]) {
                        ibdevice_error_msg = @"";
                    } else {
                        ibdevice_error_msg = [[row objectForKey:@"error_msg"] stringByReplacingOccurrencesOfString:@"'" withString:@"''"];
                    }
                    
                    //while(sqlite3_step(compiledStatement) == SQLITE_ROW) {
                    if (rows > 0) {
                        //NSLog(@"database:::::::update conferences");
                        static sqlite3_stmt *compiledupdateStatementidev;
                        sqlite3_exec(database, [[NSString stringWithFormat:@"update ibeacon_device SET uuid='%@', major='%@', minor='%@', name='%@', error_msg='%@', user_id='%@', status='%@', date_updated='%@', lastsync_date='%@' where id='%@' ", ibdevice_uuid, ibdevice_major, ibdevice_minor, ibdevice_name, ibdevice_error_msg, ibdevice_userid, ibdevice_status, ibdevice_date_updated, timeStampValueSync, ibdevice_id] UTF8String], NULL, NULL, NULL);
                        sqlite3_finalize(compiledupdateStatementidev);
                    } else {
                        //NSLog(@"database:::::::insert into contents");
                        static sqlite3_stmt *compiledinsertStatementidev;
                        sqlite3_exec(database, [[NSString stringWithFormat:@"insert into ibeacon_device (id, uuid, major, minor, name, error_msg, user_id, status, date_updated, lastsync_date) values ('%@', '%@', '%@', '%@', '%@', '%@', '%@', '%@', '%@', '%@')", ibdevice_id, ibdevice_uuid, ibdevice_major, ibdevice_minor, ibdevice_name, ibdevice_error_msg, ibdevice_userid, ibdevice_status, ibdevice_date_updated, timeStampValueSync] UTF8String], NULL, NULL, NULL);
                        //NSLog(@"ibeacon_device: %@", compiledinsertStatementidev);
                        sqlite3_finalize(compiledinsertStatementidev);
                    }
                }
                // Release the compiled statement from memory
                sqlite3_finalize(compiledStatement);
                
            }
            //NSLog(@"database:::::::Delete not synced posters");
            static sqlite3_stmt *compiledinsertStatement3h;
            sqlite3_exec(database, [[NSString stringWithFormat:@"Delete from ibeacon_device where lastsync_date<>%@", timeStampValueSync] UTF8String], NULL, NULL, NULL);
            sqlite3_finalize(compiledinsertStatement3h);
            
        }
        
        NSString *ReadData9;
        //NSLog(@"here");
        if ((firsttime && ![self isConnectionAvailable]) || (firsttime && FailWithError)) {
            TxtFiles *readFile9 = [[TxtFiles alloc] init];
            ReadData9 = [readFile9 readFromFile:readFile9.setFilenameibeaconcontent];
        } else {
            if ([currentRequestMenu isEqualToString:@"ibeaconcontent"]) {
                ReadData9 = response;
            } else {
                ReadData9 = @"[{}]";
            }
        }
        
        if (!([ReadData9 isEqualToString:@"[{}]"] || [ReadData9 isEqualToString:@""])) {
            SBJsonParser *jsonParser9 = [SBJsonParser new];
            NSDictionary *jsonData9 = (NSDictionary *) [jsonParser9 objectWithString:ReadData9 error:nil];
            
            //NSLog(@"jsonData6: %@", jsonData6);
            
            NSMutableDictionary *dictionary10 = [jsonData9 mutableCopy];
            //NSLog(@"ibeaconcontent:: %@", dictionary10);
            for(NSMutableDictionary *row in dictionary10){
                NSString *ibcontent_id = [row objectForKey:@"id"];
                NSString *ibcontent_device_id = [row objectForKey:@"device_id"];
                //NSString *ibcontent_welcome_msg = [row objectForKey:@"welcome_msg"];
                //NSString *ibcontent_welcome_img = [row objectForKey:@"welcome_img"];
                NSString *ibcontent_page_id = [row objectForKey:@"page_id"];
                NSString *ibcontent_start_date = [row objectForKey:@"start_date"];
                NSString *ibcontent_end_date = [row objectForKey:@"end_date"];
                NSString *ibcontent_userid = [row objectForKey:@"user_id"];
                NSString *ibcontent_status = [row objectForKey:@"status"];
                NSString *ibcontent_date_updated = [row objectForKey:@"date_updated"];
                NSString *limitperday = [row objectForKey:@"limitperday"];
                NSString *distance_range = [row objectForKey:@"distance_range"];
                
                int rows=0;
                NSString *temp = [[NSString alloc] initWithFormat:@"SELECT * FROM ibeacon_content WHERE id=%@ ",ibcontent_id];
                NSLog(@"temp:: %@", temp);
                const char *sqlStatement = [temp UTF8String];
                sqlite3_stmt *compiledStatement;
                if(sqlite3_prepare_v2(database, sqlStatement, -1, &compiledStatement, NULL) == SQLITE_OK) {
                    // Loop through the results and add them to the feeds array
                    
                    if (sqlite3_step(compiledStatement) == SQLITE_ERROR) {
                        NSAssert1(0,@"Error when counting rows  %s",sqlite3_errmsg(database));
                    } else {
                        rows = sqlite3_column_int(compiledStatement, 0);
                        //NSLog(@"SQLite Rows: %i", rows);
                    }
                    
                    NSString *ibcontent_welcome_msg;
                    if([row objectForKey:@"welcome_msg"] == (NSString*)[NSNull null]) {
                        ibcontent_welcome_msg = @"";
                    } else {
                        ibcontent_welcome_msg = [[row objectForKey:@"welcome_msg"] stringByReplacingOccurrencesOfString:@"'" withString:@"''"];
                    }
                    
                    NSString *ibcontent_welcome_img;
                    if([row objectForKey:@"welcome_img"] == (NSString*)[NSNull null]) {
                        ibcontent_welcome_img = @"";
                    } else {
                        ibcontent_welcome_img = [[row objectForKey:@"welcome_img"] stringByReplacingOccurrencesOfString:@"'" withString:@"''"];
                    }
                    
                    //while(sqlite3_step(compiledStatement) == SQLITE_ROW) {
                    if (rows > 0) {
                        //NSLog(@"database:::::::update conferences");
                        static sqlite3_stmt *compiledupdateStatementicont;
                        sqlite3_exec(database, [[NSString stringWithFormat:@"update ibeacon_content SET device_id='%@', welcome_msg='%@', welcome_img='%@', page_id='%@', start_date='%@', end_date='%@', user_id=%@, status='%@', limitperday='%@', distance_range='%@', date_updated='%@', lastsync_date='%@' where id='%@' ", ibcontent_device_id, ibcontent_welcome_msg, ibcontent_welcome_img, ibcontent_page_id, ibcontent_start_date, ibcontent_end_date, ibcontent_userid, ibcontent_status, limitperday, distance_range, ibcontent_date_updated, timeStampValueSync, ibcontent_id] UTF8String], NULL, NULL, NULL);
                        sqlite3_finalize(compiledupdateStatementicont);
                    } else {
                        //NSLog(@"database:::::::insert into ibeacon_device");
                        static sqlite3_stmt *compiledinsertStatementicont;
                        sqlite3_exec(database, [[NSString stringWithFormat:@"insert into ibeacon_content (id, device_id, welcome_msg, welcome_img, page_id, start_date, end_date, user_id, status, limitperday, distance_range, date_updated, lastsync_date) values ('%@', '%@', '%@', '%@', '%@', '%@', '%@', '%@', '%@', '%@', '%@', '%@', '%@')", ibcontent_id, ibcontent_device_id, ibcontent_welcome_msg, ibcontent_welcome_img, ibcontent_page_id, ibcontent_start_date, ibcontent_end_date, ibcontent_userid, ibcontent_status, limitperday, distance_range, ibcontent_date_updated, timeStampValueSync] UTF8String], NULL, NULL, NULL);
                        sqlite3_finalize(compiledinsertStatementicont);
                    }
                }
                // Release the compiled statement from memory
                sqlite3_finalize(compiledStatement);
                
            }
            //NSLog(@"database:::::::Delete not synced posters");
            static sqlite3_stmt *compiledinsertStatement3i;
            sqlite3_exec(database, [[NSString stringWithFormat:@"Delete from ibeacon_content where lastsync_date<>%@", timeStampValueSync] UTF8String], NULL, NULL, NULL);
            sqlite3_finalize(compiledinsertStatement3i);
        }
        
        NSString *ReadData10;
        //NSLog(@"here");
        if ((firsttime && ![self isConnectionAvailable]) || (firsttime && FailWithError)) {
            TxtFiles *readFile10 = [[TxtFiles alloc] init];
            ReadData10 = [readFile10 readFromFile:readFile10.setFilenameibeaconxref];
        } else {
            if ([currentRequestMenu isEqualToString:@"ibeaconxref"]) {
                ReadData10 = response;
            } else {
                ReadData10 = @"[{}]";
            }
        }
        
        if (!([ReadData10 isEqualToString:@"[{}]"] || [ReadData10 isEqualToString:@""])) {
            SBJsonParser *jsonParser10 = [SBJsonParser new];
            NSDictionary *jsonData10 = (NSDictionary *) [jsonParser10 objectWithString:ReadData10 error:nil];
            
            //NSLog(@"jsonData6: %@", jsonData6);
            
            NSMutableDictionary *dictionary11 = [jsonData10 mutableCopy];
            //NSLog(@"ibeaconcontent:: %@", dictionary11);
            for(NSMutableDictionary *row in dictionary11){
                NSString *ibxref_id = [row objectForKey:@"id"];
                NSString *ibxref_device_id = [row objectForKey:@"device_id"];
                NSString *ibxref_content_id = [row objectForKey:@"content_id"];
                NSString *ibxref_userid = [row objectForKey:@"user_id"];
                NSString *ibxref_date_updated = [row objectForKey:@"date_updated"];
                
                int rows=0;
                NSString *temp = [[NSString alloc] initWithFormat:@"SELECT * FROM ibeacon_xref WHERE id=%@ ",ibxref_id];
                //NSLog(@"temp:: %@", temp);
                const char *sqlStatement = [temp UTF8String];
                sqlite3_stmt *compiledStatement;
                if(sqlite3_prepare_v2(database, sqlStatement, -1, &compiledStatement, NULL) == SQLITE_OK) {
                    // Loop through the results and add them to the feeds array
                    
                    if (sqlite3_step(compiledStatement) == SQLITE_ERROR) {
                        NSAssert1(0,@"Error when counting rows  %s",sqlite3_errmsg(database));
                    } else {
                        rows = sqlite3_column_int(compiledStatement, 0);
                        //NSLog(@"SQLite Rows: %i", rows);
                    }
                    
                    //while(sqlite3_step(compiledStatement) == SQLITE_ROW) {
                    if (rows > 0) {
                        //NSLog(@"database:::::::update conferences");
                        static sqlite3_stmt *compiledupdateStatementixref;
                        sqlite3_exec(database, [[NSString stringWithFormat:@"update ibeacon_xref SET device_id='%@', content_id='%@', user_id=%@, date_updated='%@', lastsync_date='%@' where id='%@' ", ibxref_device_id, ibxref_content_id, ibxref_userid, ibxref_date_updated, timeStampValueSync, ibxref_id] UTF8String], NULL, NULL, NULL);
                        sqlite3_finalize(compiledupdateStatementixref);
                    } else {
                        //NSLog(@"database:::::::insert into ibeacon_device");
                        static sqlite3_stmt *compiledinsertStatementixref;
                        sqlite3_exec(database, [[NSString stringWithFormat:@"insert into ibeacon_xref (id, device_id, content_id, user_id, date_updated, lastsync_date) values ('%@', '%@', '%@', '%@', '%@', '%@')", ibxref_id, ibxref_device_id, ibxref_content_id, ibxref_userid, ibxref_date_updated, timeStampValueSync] UTF8String], NULL, NULL, NULL);
                        sqlite3_finalize(compiledinsertStatementixref);
                    }
                }
                // Release the compiled statement from memory
                sqlite3_finalize(compiledStatement);
                
            }
            //NSLog(@"database:::::::Delete not synced posters");
            static sqlite3_stmt *compiledinsertStatement3j;
            sqlite3_exec(database, [[NSString stringWithFormat:@"Delete from ibeacon_xref where lastsync_date<>%@", timeStampValueSync] UTF8String], NULL, NULL, NULL);
            sqlite3_finalize(compiledinsertStatement3j);
        }
        
        //self.user = [jsonData objectForKey:@"userid"];
        
        /*TxtFiles *readFile = [[TxtFiles alloc] init];
         NSString *ReadData = [readFile readFromFile];
         
         SBJsonParser *jsonParser = [SBJsonParser new];
         NSDictionary *jsonData = (NSDictionary *) [jsonParser objectWithString:ReadData error:nil];
         
         user = [jsonData objectForKey:@"userid"]; */
        
    }
    sqlite3_close(database);
    
    if ((firsttime && ![self isConnectionAvailable]) || (firsttime && FailWithError)) {
        //if ([self isConnectionAvailable])
        //{
            [self processdata:YES];
        //} else {
        //    [self processdata:NO];
        //}
        appdelegate.firsttime = NO;
    } else if ([currentRequestMenu isEqualToString:@"ibeaconcontent"]) {
        if (enterbackground) {
            [self processdata:NO];
        } else {
            [self processdata:YES];
        }
    }
        
}

- (void)pan:(UIPanGestureRecognizer *)recognizer
{
    if (recognizer.state == UIGestureRecognizerStateChanged ||
        recognizer.state == UIGestureRecognizerStateEnded)
    {
        
        UIView *draggedButton = recognizer.view;
        CGPoint translation = [recognizer translationInView:self.view];
        
        CGRect newButtonFrame = draggedButton.frame;
        
        newButtonFrame.origin.x += translation.x;
        newButtonFrame.origin.y += translation.y;
        
        if (newButtonFrame.origin.x < self.view.bounds.origin.x)
            newButtonFrame.origin.x = 0;
        else if (newButtonFrame.origin.x + newButtonFrame.size.width > self.view.bounds.size.width)
            newButtonFrame.origin.x = self.view.bounds.size.width - newButtonFrame.size.width;
        
        if (newButtonFrame.origin.y < self.view.bounds.origin.y)
            newButtonFrame.origin.y = 0;
        else if (newButtonFrame.origin.y + newButtonFrame.size.height > self.view.bounds.size.height)
            newButtonFrame.origin.y = self.view.bounds.size.height - newButtonFrame.size.height;
        
        draggedButton.frame = newButtonFrame;
        
        [recognizer setTranslation:CGPointZero inView:self.view];
    }
}

#pragma mark Region Delegates -- START
-(void)locationManager:(CLLocationManager *)manager didStartMonitoringForRegion:(CLRegion *)region
{
    NSLog(@"%s - %@",__PRETTY_FUNCTION__,region.identifier);
    
    /*if (self.myBeaconRegion)
        [appdelegate.locationManager startRangingBeaconsInRegion:self.myBeaconRegion];
    if (self.myBeaconRegion2)
        [appdelegate.locationManager startRangingBeaconsInRegion:self.myBeaconRegion2];
    if (self.myBeaconRegion3)
        [appdelegate.locationManager startRangingBeaconsInRegion:self.myBeaconRegion3];
    if (self.myBeaconRegion4)
        [appdelegate.locationManager startRangingBeaconsInRegion:self.myBeaconRegion4];
    if (self.myBeaconRegion5)
        [appdelegate.locationManager startRangingBeaconsInRegion:self.myBeaconRegion5];*/
}

-(void)locationManager:(CLLocationManager*)manager
       didRangeBeacons:(NSArray*)beacons
              inRegion:(CLBeaconRegion*)region
{
    ibeacon = [[NSArray alloc] initWithArray:beacons];
    
    foundBeacon = [beacons firstObject];
    
    NSLog(@"%s - uuid:%@ major:%@ minor:%@",__PRETTY_FUNCTION__,region.identifier, foundBeacon.major, foundBeacon.minor);
    BOOL found = NO;
    //ibeaconbutton = [[UIButton alloc] init];
    //ibeaconbutton = [UIButton buttonWithType:UIButtonTypeCustom];
    
    //NSLog(@"beacons %@", beacons);
    if ([beacons count] > 0)
    {
        beaconNotFoundTimer = 0;
        appdelegate.beaconFoundTimer += 1;
        //NSLog(@"beaconFoundTimer %d", appdelegate.beaconFoundTimer);
        
        NSString *popup;
        popup = @"";
        __strong NSString *dist_range=@"";
        NSPredicate *thePredicate = [NSPredicate predicateWithFormat:@"(uuid == %@) AND (major == %@) AND (minor == %@)",
                                     region.identifier, [foundBeacon.major stringValue], [foundBeacon.minor stringValue]];
        NSArray *filteredList = [arrayIBeaconList filteredArrayUsingPredicate:thePredicate];
        NSDictionary *selectedDict = [[NSDictionary alloc]init];
        selectedDict = [filteredList lastObject];
        if ([selectedDict count]) {
            dist_range = [NSString stringWithFormat:@"%@",[selectedDict objectForKey:@"distance_range"]];
        }
        
        //NSLog(@"dist_range ::%@",dist_range);
        int prox_success = 0;
        if ([[dist_range uppercaseString] isEqualToString:@"FAR"]) {
            if (foundBeacon.proximity == CLProximityFar) {
                prox_success = 1;
            }
        } else if ([[dist_range uppercaseString] isEqualToString:@"NEAR"]) {
            if (foundBeacon.proximity == CLProximityNear || foundBeacon.proximity == CLProximityImmediate) {
                prox_success = 1;
            }
        } else if ([[dist_range uppercaseString] isEqualToString:@"ANY"]) {
            if (foundBeacon.proximity == CLProximityNear || foundBeacon.proximity == CLProximityImmediate || foundBeacon.proximity == CLProximityFar) {
                prox_success = 1;
            }
        } else {
            prox_success = 0;
        }
        
        //NSLog(@"prox_success :: %d", prox_success);
        if (prox_success==1)
        {
            int x = 0;
            /*while (x<[arrayIBeaconList count])
             {
             if ([[[arrayIBeaconList objectAtIndex:x] objectForKey:@"uuid"] isEqualToString:region.identifier] &&
             [[[arrayIBeaconList objectAtIndex:x] objectForKey:@"major"] isEqualToString:[foundBeacon.major stringValue]] &&
             [[[arrayIBeaconList objectAtIndex:x] objectForKey:@"minor"] isEqualToString:[foundBeacon.minor stringValue]])
             { */
            if ([arrayIBeaconList count]==0) {
                if (appdelegate.ibeaconfrommenu) {
                    appdelegate.beaconFoundTimer = 0;
                    while (x<[arrayIBeacon count])
                    {
                        if ([uuid_ibeacon isEqualToString:region.identifier] &&
                            [[[arrayIBeacon objectAtIndex:x] objectForKey:@"major"] isEqualToString:[foundBeacon.major stringValue]] &&
                            [[[arrayIBeacon objectAtIndex:x] objectForKey:@"minor"] isEqualToString:[foundBeacon.minor stringValue]])
                        {
                            NSDictionary *dictionary = [arrayIBeacon objectAtIndex:x];
                            
                            NSString *errormsg = [dictionary objectForKey:@"message"];
                            
                            UILabel *labelerror = [[UILabel alloc] initWithFrame: CGRectMake(0, 0, self.view.frame.size.width, 60)];
                            [labelerror setFont:[UIFont boldSystemFontOfSize:16]];
                            labelerror.textColor = [UIColor blackColor];
                            labelerror.textAlignment = NSTextAlignmentLeft;
                            labelerror.backgroundColor = [UIColor clearColor];
                            labelerror.numberOfLines = 0;
                            
                            labelerror.text = errormsg;
                            //NSLog(@"labelerror :%@", labelerror.text);
                            __strong UIAlertView *alert1;
                            alert1 = [[UIAlertView alloc] initWithTitle:nil message:labelerror.text delegate:self cancelButtonTitle:@"OK" otherButtonTitles: nil];
                            [alert1 show];
                            
                            appdelegate.ibeaconfrommenu = NO;
                            
                        }
                        
                        x += 1;
                    }
                }
            } else {
                while (x<[arrayIBeaconList count])
                {
                    
                    if ([uuid_ibeacon isEqualToString:region.identifier] &&
                        [[[arrayIBeaconList objectAtIndex:x] objectForKey:@"major"] isEqualToString:[foundBeacon.major stringValue]] &&
                        [[[arrayIBeaconList objectAtIndex:x] objectForKey:@"minor"] isEqualToString:[foundBeacon.minor stringValue]])
                    {
                        
                        current_idx = x;
                        /*if ([prevuuid isEqualToString:region.identifier] &&
                         [prevmajor isEqualToString:[foundBeacon.major stringValue]] &&
                         [prevminor isEqualToString:[foundBeacon.minor stringValue]])
                         {
                         hasDisplayedNotification = YES;
                         } else {
                         hasDisplayedNotification = NO;
                         } */
                        //[self fireUpdateNotificationForStatus:[arrayIBeaconList objectAtIndex:x]];
                        //popup = @"popup";
                        //hasDisplayedNotification = NO;
                        found = YES;
                        [self fireUpdateNotificationForStatus:[arrayIBeaconList objectAtIndex:x]];
                    }
                    
                    x += 1;
                }
            }
        }
        
        if (prox_success==1) {
        if ([arrayIBeaconList count]>0) {
        if (!hasDisplayedNotification && found)
        {
            //NSLog(@"popup :: %@", popup);
            //if ([popup isEqualToString:@"popup"]) {
            //Only brings out notification if app is not in foreground
            if ([[UIApplication sharedApplication] applicationState] != UIApplicationStateActive)
            //if ([[UIApplication sharedApplication] applicationState] != UIApplicationStateBackground)
                [self displayLocalNotification:NO forRegion:region];
            else
            {
                //if (appdelegate.navigationController.visibleViewController.title != self.title)
                [self displayLocalNotification:YES forRegion:region];
            }
            
            if (appdelegate.ibeaconfrommenu) {
                appdelegate.beaconFoundTimer = 0;
                int x = 0;
                while (x<[arrayIBeacon count])
                {
                    if ([uuid_ibeacon isEqualToString:region.identifier] &&
                        [[[arrayIBeacon objectAtIndex:x] objectForKey:@"major"] isEqualToString:[foundBeacon.major stringValue]] &&
                        [[[arrayIBeacon objectAtIndex:x] objectForKey:@"minor"] isEqualToString:[foundBeacon.minor stringValue]])
                    {
                        NSDictionary *dictionary = [arrayIBeacon objectAtIndex:x];
                        
                        NSString *errormsg = [dictionary objectForKey:@"message"];
                        
                        UILabel *labelerror = [[UILabel alloc] initWithFrame: CGRectMake(0, 0, self.view.frame.size.width, 60)];
                        [labelerror setFont:[UIFont boldSystemFontOfSize:16]];
                        labelerror.textColor = [UIColor blackColor];
                        labelerror.textAlignment = NSTextAlignmentLeft;
                        labelerror.backgroundColor = [UIColor clearColor];
                        labelerror.numberOfLines = 0;
                        
                        labelerror.text = errormsg;
                        //NSLog(@"labelerror :%@", labelerror.text);
                        __strong UIAlertView *alert1;
                        alert1 = [[UIAlertView alloc] initWithTitle:nil message:labelerror.text delegate:self cancelButtonTitle:@"OK" otherButtonTitles: nil];
                        [alert1 show];
                        
                    }
                    
                    x += 1;
                }
                
                appdelegate.ibeaconfrommenu = NO;
            }
        } else {
            /*if ([[[arrayIBeaconList objectAtIndex:current_idx] objectForKey:@"uuid"] isEqualToString:region.identifier] &&
                [[[arrayIBeaconList objectAtIndex:current_idx] objectForKey:@"major"] isEqualToString:[foundBeacon.major stringValue]] &&
                [[[arrayIBeaconList objectAtIndex:current_idx] objectForKey:@"minor"] isEqualToString:[foundBeacon.minor stringValue]])
            {*/
            if ([arrayIBeaconList count]>0) {
               // NSLog(@"[arrayIBeaconList objectAtIndex:current_idx] %@", [[arrayIBeaconList objectAtIndex:current_idx] objectForKey:@"minor"]);
                //NSLog(@"[foundBeacon.minor stringValue] %@", [foundBeacon.minor stringValue]);
                if ([uuid_ibeacon isEqualToString:region.identifier] &&
                    [[[arrayIBeaconList objectAtIndex:current_idx] objectForKey:@"major"] isEqualToString:[foundBeacon.major stringValue]] &&
                    [[[arrayIBeaconList objectAtIndex:current_idx] objectForKey:@"minor"] isEqualToString:[foundBeacon.minor stringValue]])
                {
                    //int x = 0;
                    //while (x<[arrayIBeaconList count])
                    //{
                        if ([prevuuid isEqualToString:region.identifier] &&
                            [prevmajor isEqualToString:[foundBeacon.major stringValue]] &&
                            [prevminor isEqualToString:[foundBeacon.minor stringValue]])
                        {
                            if (appdelegate.beaconFoundTimer >= appdelegate.intervalNotification) //30secs 1800
                            {
                                appdelegate.beaconFoundTimer = 0;
                                //current_idx = x;
                                //NSLog(@"displayBeaconPromoPopUp30sec");
                                [self displayBeaconPromoPopUp:[[arrayIBeaconList objectAtIndex:current_idx] objectForKey:@"image"]];
                                hasDisplayedNotification = NO;
                                appdelegate.ibeaconfrommenu = NO;
                            }
                        } else {
                            appdelegate.beaconFoundTimer = 0;
                            //current_idx = x;
                            //NSLog(@"displayBeaconPromoPopUp111");
                            [self displayBeaconPromoPopUp:[[arrayIBeaconList objectAtIndex:current_idx] objectForKey:@"image"]];
                            hasDisplayedNotification = NO;
                            
                        }
                        //x += 1;
                        
                    //}
                    prevuuid = region.identifier;
                    prevmajor = [foundBeacon.major stringValue];
                    prevminor = [foundBeacon.minor stringValue];
                } else {
                    //NSLog(@"arrayIBeacon:: %@", arrayIBeacon);
                    if (appdelegate.ibeaconfrommenu) {
                        appdelegate.beaconFoundTimer = 0;
                        int x = 0;
                        while (x<[arrayIBeacon count])
                        {
                            if ([uuid_ibeacon isEqualToString:region.identifier] &&
                                [[[arrayIBeacon objectAtIndex:x] objectForKey:@"major"] isEqualToString:[foundBeacon.major stringValue]] &&
                                [[[arrayIBeacon objectAtIndex:x] objectForKey:@"minor"] isEqualToString:[foundBeacon.minor stringValue]])
                            {
                                NSDictionary *dictionary = [arrayIBeacon objectAtIndex:x];
                                
                                NSString *errormsg = [dictionary objectForKey:@"message"];
                                
                                UILabel *labelerror = [[UILabel alloc] initWithFrame: CGRectMake(0, 0, self.view.frame.size.width, 60)];
                                [labelerror setFont:[UIFont boldSystemFontOfSize:16]];
                                labelerror.textColor = [UIColor blackColor];
                                labelerror.textAlignment = NSTextAlignmentLeft;
                                labelerror.backgroundColor = [UIColor clearColor];
                                labelerror.numberOfLines = 0;
                                
                                labelerror.text = errormsg;
                                //NSLog(@"labelerror :%@", labelerror.text);
                                __strong UIAlertView *alert1;
                                alert1 = [[UIAlertView alloc] initWithTitle:nil message:labelerror.text delegate:self cancelButtonTitle:@"OK" otherButtonTitles: nil];
                                [alert1 show];
                                
                            }
                            
                            x += 1;
                        }
                        appdelegate.ibeaconfrommenu = NO;
                    }
                }
            }
        }
        }
        }
    }
    else
    {
        beaconNotFoundTimer += 1;
        //NSLog(@"beaconNotFoundTimer %d", beaconNotFoundTimer);
        if (beaconNotFoundTimer == appdelegate.intervalNotification) //30secs
        {
            //If within the time specified, no beacons are found,
            //app will display another notification once beacons are found
            
            hasDisplayedNotification = NO;
            beaconNotFoundTimer = 0;
            
            //if (hasBeacon && [CLLocationManager isMonitoringAvailableForClass:[CLBeaconRegion class]] && beaconButtonHasBeenAdded)
            if (hasBeacon && [CLLocationManager isMonitoringAvailableForClass:[CLBeaconRegion class]])
            {
                for (UIView *view in self.view.subviews)
                {
                    if (view.tag == 2406)
                    {
                        for (UIView *subview in view.subviews)
                        {
                            if (subview.tag == 2506)
                            {
                                [subview removeFromSuperview];
                                break;
                            }
                        }
                        break;
                    }
                }
                //beaconButtonHasBeenAdded = NO;
            }
        }
    }
}

- (void) displayLocalNotification:(BOOL)applicationIsActive forRegion:(CLRegion *)region
{
    //int x = 0;
    //while (x<[arrayIBeacon count])
    //{
        //if ([[[arrayIBeacon objectAtIndex:x] objectForKey:@"uuid"] isEqualToString:region.identifier])
        //{
    if (!applicationIsActive) {
        
        hasDisplayedNotification = YES;
        
        if ([[UIApplication sharedApplication] respondsToSelector:@selector(registerUserNotificationSettings:)])
        {
            [[UIApplication sharedApplication] registerUserNotificationSettings:[UIUserNotificationSettings settingsForTypes:(UIUserNotificationTypeSound | UIUserNotificationTypeAlert | UIUserNotificationTypeBadge) categories:nil]];
            [[UIApplication sharedApplication] registerForRemoteNotifications];
        }
        else
        {
            [[UIApplication sharedApplication] registerForRemoteNotificationTypes:(UIUserNotificationTypeBadge | UIUserNotificationTypeSound | UIUserNotificationTypeAlert)];
        }
        
        UILocalNotification *notification = [[UILocalNotification alloc] init];
        notification.soundName = UILocalNotificationDefaultSoundName;
        
        //if (!applicationIsActive)
            notification.applicationIconBadgeNumber = [[UIApplication sharedApplication] applicationIconBadgeNumber] + 1;
        
        NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
        NSString *name = @"";
        
        if ([[defaults objectForKey:@"hasLoggedInMyApp"] isEqualToString:@"YES"] || [[defaults objectForKey:@"hasLoggedInFB"] isEqualToString:@"YES"])
            name = [NSString stringWithFormat:@" %@",[defaults objectForKey:@"customer_name"]];
        
        notification.alertBody = [NSString stringWithFormat:@"Welcome%@. %@",name,[[arrayIBeaconList objectAtIndex:current_idx] objectForKey:@"notification_message"]];
        [self fireUpdateNotificationForStatus:[arrayIBeaconList objectAtIndex:current_idx]];
        //NSLog(@"displayBeaconPromoPopUp");
        
        sqlite3 *database;
        
        if(sqlite3_open([appdelegate.databasePath UTF8String], &database) == SQLITE_OK) {
            NSString *ibnot = [[NSString alloc] initWithFormat:@"SELECT * FROM ibeacon_notification WHERE alert_body='%@' ", [[arrayIBeaconList objectAtIndex:current_idx] objectForKey:@"notification_message"]];
            //NSLog(@"ibeacon_notification:: %@", ibnot);
            const char *sqlStatementibnot = [ibnot UTF8String];
            sqlite3_stmt *compiledStatementibnot;
            if(sqlite3_prepare_v2(database, sqlStatementibnot, -1, &compiledStatementibnot, NULL) == SQLITE_OK) {
                // Loop through the results and add them to the feeds array
                int rows1 = 0;
                while(sqlite3_step(compiledStatementibnot) == SQLITE_ROW) {
                    rows1 = 1;
                }
                
                //NSLog(@"SQLite Rows: %i", rows1);
                
                NSString *talertbody = [[arrayIBeaconList objectAtIndex:current_idx] objectForKey:@"notification_message"];
                
                talertbody = [talertbody stringByReplacingOccurrencesOfString:@"''" withString:@"'"];
                
                //NSLog(@"ibeacon_notificationtalertbody:: %@", talertbody);
                //NSLog(@"ibeacon_notificationdate:: %@", [NSDate date]);
                
                //while(sqlite3_step(compiledStatement) == SQLITE_ROW) {
                if (rows1 > 0) {
                    //NSLog(@"database:::::::ibeacon_notification Update");
                } else {
                    //NSLog(@"database:::::::insert into ibeacon_notification");
                    static sqlite3_stmt *compiledinsertStatement11;
                    sqlite3_exec(database, [[NSString stringWithFormat:@"insert into ibeacon_notification (alert_body, date_updated) values ('%@', '%@')", talertbody, [NSDate date]] UTF8String], NULL, NULL, NULL);
                    sqlite3_finalize(compiledinsertStatement11);
                    
                    [[UIApplication sharedApplication] presentLocalNotificationNow:notification];
                }
            }
            // Release the compiled statement from memory
            sqlite3_finalize(compiledStatementibnot);
            
            __strong NSDateFormatter *dateFormat = [[NSDateFormatter alloc]init];
            [dateFormat setDateFormat:@"yyyy-MM-dd"];
            NSString *todaydateString = [dateFormat stringFromDate:[NSDate date]];
            
            static sqlite3_stmt *compiledinsertStatementdelete;
            sqlite3_exec(database, [[NSString stringWithFormat:@"delete from ibeacon_notification WHERE date_updated < '%@' ", todaydateString] UTF8String], NULL, NULL, NULL);
            sqlite3_finalize(compiledinsertStatementdelete);
            
        }
        sqlite3_close(database);
    }
    
    [self displayBeaconPromoPopUp:[[arrayIBeaconList objectAtIndex:current_idx] objectForKey:@"image"]];
            //Get full list of beacons for this uuid
            /*NSString *urlAddress = [[NSString alloc] initWithFormat:@"%@ibeacon_list.php?ibeaconid=%@", appdelegate.user_apipath, [[arrayIBeacon objectAtIndex:current_idx] objectForKey:@"id"]];
            //NSLog(@"urlAddressbeaconlist %@", urlAddress);
            currentRequestMenu = @"ibeacon_list";
            [self connectToServer:urlAddress]; */
        //}
        //x += 1;
    //}
}

-(void) displayBeaconPromoPopUp:(NSString *)imgURL
{
    //[imgView_beaconPromo setImageWithURL:[NSURL URLWithString:imgURL]  placeholderImage:[UIImage imageNamed:@"placeholder.png"] usingActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
    UIImageView *imgV = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, imgView_beaconPromo.frame.size.width, imgView_beaconPromo.frame.size.height)];
    
    if (imgV== nil){
        UILabel *lablerror = [[UILabel alloc] initWithFrame: CGRectMake(0, 0, self.view.frame.size.width, 60)];
        [lablerror setFont:[UIFont boldSystemFontOfSize:16]];
        lablerror.textColor = [UIColor blackColor];
        lablerror.textAlignment = NSTextAlignmentLeft;
        lablerror.backgroundColor = [UIColor clearColor];
        lablerror.numberOfLines = 0;
        
        [view_popoverMain addSubview:lablerror];
    } else {
    // set scale to fill
        [imgView_beaconPromo addSubview:imgV];
        
        NSRange end = [imgURL rangeOfString:@"/" options:NSBackwardsSearch];
        NSString *shortString;
        if (end.location != NSNotFound) {
            shortString =[imgURL substringWithRange:NSMakeRange(end.location+1, imgURL.length-(end.location+1))];
        } else {
            shortString = imgURL;
        }
        
        BOOL success;
        
        // Create a FileManager object, we will use this to check the status
        // of the database and to copy it over if required
        NSFileManager *fileManager = [NSFileManager defaultManager];
        
        // Check if the database has already been created in the users filesystem
        success = [fileManager fileExistsAtPath:[[[NSBundle mainBundle] resourcePath] stringByAppendingPathComponent:shortString]];
        
        __block UIActivityIndicatorView *activityIndicator;
        __weak UIImageView *weakImageView = imgV;
        NSString *imagePath;
        if(success) {
            imagePath = [[[NSBundle mainBundle] resourcePath] stringByAppendingPathComponent:shortString];
            [weakImageView setImage:[UIImage imageNamed:shortString]];
        } else {
            imagePath = imgURL;
            /*[weakImageView setImageWithURL:[NSURL URLWithString:imagePath] placeholderImage:[UIImage imageNamed:@"No-Image-icon.png"] options:SDWebImageProgressiveDownload progress:^(NSInteger receivedSize, NSInteger expectedSize)
             {
                 if (!activityIndicator)
                 {
                     [weakImageView addSubview:activityIndicator = [UIActivityIndicatorView.alloc initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray]];
                     activityIndicator.center = weakImageView.center;
                     [activityIndicator startAnimating];
                 }
             }
                                 completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType)
             {
                 [activityIndicator removeFromSuperview];
                 activityIndicator = nil;
             }]; */
            dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0), ^ {
                [weakImageView sd_setImageWithURL:[NSURL URLWithString:imagePath]
                                 placeholderImage:[UIImage imageNamed:@"No-Image-icon.png"]
                                          options:SDWebImageProgressiveDownload
                                         progress:^(NSInteger receivedSize, NSInteger expectedSize, NSURL *targetURL) {
                                             dispatch_async(dispatch_get_main_queue(), ^ {
                                                 if (!activityIndicator) {
                                                     
                                                     [weakImageView addSubview:activityIndicator = [UIActivityIndicatorView.alloc initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray]];
                                                     activityIndicator.center = weakImageView.center;
                                                     [activityIndicator startAnimating];
                                                 }
                                             });
                                         }
                                        completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
                                            dispatch_async(dispatch_get_main_queue(), ^ {
                                                //[MBProgressHUD hideHUDForView:self.view animated:YES];
                                                [activityIndicator removeFromSuperview];
                                                activityIndicator = nil;
                                            });
                                        }];
            });
        }
        //NSLog(@"shortStringimagePath : %@", imagePath);
        imgV.contentMode  = UIViewContentModeScaleAspectFill;
        [imgV setClipsToBounds:YES];
    }
    
    [appdelegate.window addSubview:view_popoverMain];
}

- (IBAction)onCloseBeaconPopUp:(id)sender
{
    appdelegate.beaconFoundTimer = 0;
    [view_popoverMain removeFromSuperview];
}

- (IBAction)onViewBeaconPromoTapped:(id)sender
{
    
    appdelegate.beaconFoundTimer = 0;
    [view_popoverMain removeFromSuperview];
    
    NSDictionary *dictionary = [arrayIBeaconList objectAtIndex:current_idx];
    appdelegate.nextPage = [dictionary objectForKey:@"page_id"];
    
    if (appdelegate.showloading) {
        alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"loading", nil) message:nil delegate:self cancelButtonTitle:nil otherButtonTitles: nil];
        
        //a simple activity indicator:
        UIActivityIndicatorView *activityIndicator = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
        activityIndicator.frame= CGRectMake(50, 10, 37, 37);
        activityIndicator.autoresizingMask = UIViewAutoresizingFlexibleTopMargin | UIViewAutoresizingFlexibleRightMargin | UIViewAutoresizingFlexibleBottomMargin | UIViewAutoresizingFlexibleLeftMargin;
        [activityIndicator startAnimating];
        
        //the magic line below,
        //we associate the activity indicator to the alert view: (addSubview is not used)
        [alert setValue:activityIndicator forKey:@"accessoryView"];
        
        [alert show];
    
        // Adjust the indicator so it is up a few pixels from the bottom of the alert
        [UIView beginAnimations:nil context:nil];
        [UIView setAnimationDuration:0.0];
        [UIView setAnimationDelay:0.0];
        [UIView setAnimationTransition:UIViewAnimationTransitionNone forView:alert cache:YES];
        [UIView setAnimationDelegate:self];
        [UIView setAnimationDidStopSelector:@selector(startupAnimationDoneIndicator:finished:context:)];
        alert.alpha = 0.5;
        [UIView commitAnimations];
    } else {
        [self goNextPage];
    }
}


- (void)fireUpdateNotificationForStatus:(NSString*)status {
    // fire notification to update displayed status
    [[NSNotificationCenter defaultCenter] postNotificationName:@"Test"
                                                        object:Nil
                                                      userInfo:@{@"status" : status}];
}
#pragma mark Region Delegates -- END

#pragma mark End Connection

- (BOOL) isConnectionAvailable
{
    Reachability *networkReachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus networkStatus = [networkReachability currentReachabilityStatus];
    if (networkStatus == NotReachable) {
        //NSLog(@"There IS NO internet connection");
        return FALSE;
    } else {
        //NSLog(@"There IS internet connection");
        return TRUE;
    }
}

-(void) viewDidDisappear:(BOOL)animated {
    
    themecode =nil;
    page_id =nil;
    page_name =nil;
    
    [super viewDidDisappear:animated];
    
}

- (void) alertStatus:(NSString *)msg :(NSString *)title
{
    UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:title
                                                        message:msg
                                                       delegate:self
                                              cancelButtonTitle:@"OK"
                                              otherButtonTitles:nil, nil];
    
    [alertView show];
    [alertView removeFromSuperview];
    alertView =nil;
}

//- (IBAction)backToRoot:(UIStoryboardSegue *)segue {
    //NSLog(@"back to 2 from vc 4");
//}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
