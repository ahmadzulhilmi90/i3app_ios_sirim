//
//  WarrantyAddViewController.h
//  standardmobileappsv3
//
//  Created by M3Online on 6/20/14.
//  Copyright (c) 2014 M3Online. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface WarrantyAddViewController : UIViewController <UIImagePickerControllerDelegate, UINavigationControllerDelegate, UITextFieldDelegate, UIScrollViewDelegate, UIViewControllerTransitioningDelegate> {
    CGFloat animatedDistance;
}

@property (strong, nonatomic) IBOutlet UIView *view_add;
@property (strong, nonatomic) IBOutlet UIScrollView *view_addscroll;
@property (strong, nonatomic) IBOutlet UITextField *txt_product;
@property (strong, nonatomic) IBOutlet UITextField *txt_purchaseDate;
@property (strong, nonatomic) IBOutlet UITextField *txt_receiptPic;
@property (strong, nonatomic) IBOutlet UIButton *button_submit;
@property (strong, nonatomic) IBOutlet UILabel *lbl_title;
@property (strong, nonatomic) IBOutlet UILabel *lbl_product;
@property (strong, nonatomic) IBOutlet UILabel *lbl_purchaseDate;
@property (strong, nonatomic) IBOutlet UILabel *lbl_receiptPic;
@property (strong, nonatomic) IBOutlet UILabel *lbl_requiredproduct;
@property (strong, nonatomic) IBOutlet UILabel *lbl_requiredpurchDate;

@property (strong, nonatomic) IBOutlet UIView *view_add1;
@property (strong, nonatomic) IBOutlet UIScrollView *view_add1scroll;
@property (strong, nonatomic) IBOutlet UITextField *txt_productPic;
@property (strong, nonatomic) IBOutlet UITextField *txt_serialNo;
@property (strong, nonatomic) IBOutlet UITextField *txt_sku;
@property (strong, nonatomic) IBOutlet UITextField *txt_storeName;
@property (strong, nonatomic) IBOutlet UIButton *button_submit1;
@property (strong, nonatomic) IBOutlet UIButton *button_done1;
@property (strong, nonatomic) IBOutlet UIButton *button_skip1;
@property (strong, nonatomic) IBOutlet UILabel *lbl_title1;
@property (strong, nonatomic) IBOutlet UILabel *lbl_productPic;
@property (strong, nonatomic) IBOutlet UILabel *lbl_serialNo;
@property (strong, nonatomic) IBOutlet UILabel *lbl_sku;
@property (strong, nonatomic) IBOutlet UILabel *lbl_storeName;

@property (strong, nonatomic) IBOutlet UIView *view_add2;
@property (strong, nonatomic) IBOutlet UIScrollView *view_add2scroll;
@property (strong, nonatomic) IBOutlet UITextField *txt_warrantyLength;
@property (strong, nonatomic) IBOutlet UITextField *txt_warrantyCardPic;
@property (strong, nonatomic) IBOutlet UITextField *txt_comments;
@property (strong, nonatomic) IBOutlet UIButton *button_done2;
@property (strong, nonatomic) IBOutlet UIButton *button_skip2;
@property (strong, nonatomic) IBOutlet UILabel *lbl_warrantyLength;
@property (strong, nonatomic) IBOutlet UILabel *lbl_warrantyCardPic;
@property (strong, nonatomic) IBOutlet UILabel *lbl_comments;
@property (strong, nonatomic) IBOutlet UILabel *lbl_title2;

@property (strong, nonatomic) IBOutlet UIView *view_done;
@property (strong, nonatomic) IBOutlet UILabel *lbl_msgDone;
@property (strong, nonatomic) IBOutlet UILabel *lbl_titleDone;

@property (nonatomic, retain) IBOutlet UIImageView * imageView;
@property (nonatomic, retain) IBOutlet UIButton * choosePhotoBtn;
@property (nonatomic, retain) IBOutlet UIButton * takePhotoBtn;

@property (nonatomic, retain) IBOutlet UIImageView * imageView1;
@property (nonatomic, retain) IBOutlet UIButton * choosePhotoBtn1;
@property (nonatomic, retain) IBOutlet UIButton * takePhotoBtn1;

@property (nonatomic, retain) IBOutlet UIImageView * imageView2;
@property (nonatomic, retain) IBOutlet UIButton * choosePhotoBtn2;
@property (nonatomic, retain) IBOutlet UIButton * takePhotoBtn2;

@property (strong, nonatomic) NSString *cont_title;

-(IBAction) getPhoto:(id) sender;

- (IBAction)onSubmitClicked:(id)sender;
- (IBAction)onSubmit1Clicked:(id)sender;
- (IBAction)onDone1Clicked:(id)sender;
- (IBAction)onSkip1Clicked:(id)sender;
- (IBAction)onDone2Clicked:(id)sender;
- (IBAction)onSkip2Clicked:(id)sender;

@end
