//
//  WarrantyAddViewController.m
//  standardmobileappsv3
//
//  Created by M3Online on 6/20/14.
//  Copyright (c) 2014 M3Online. All rights reserved.
//

#import "WarrantyAddViewController.h"
#import "AppDelegate.h"
#import "SBJson.h"
#import <SystemConfiguration/SystemConfiguration.h>
#import "RootViewController.h"
#import "ViewController.h"
#import "InitialSlidingViewController.h"
#import "BTSlideInteractor.h"
#import "Reachability.h"

static const CGFloat KEYBOARD_ANIMATION_DURATION = 0.3;
static const CGFloat MINIMUM_SCROLL_FRACTION = 0.2;
static const CGFloat MAXIMUM_SCROLL_FRACTION = 0.8;
static const CGFloat PORTRAIT_KEYBOARD_HEIGHT = 216;

@interface WarrantyAddViewController ()
@property (strong, nonatomic) BTSlideInteractor *interactor;
@end

InitialSlidingViewController *modalController;

AppDelegate *appdelegate;
UIAlertView *alert;
UIActivityIndicatorView *indicator;
__strong LoadingView *loadingView;
__strong NSString *currentRequest;
//__strong NSMutableData *receivedData;
__strong NSString *warranty_id;
__strong NSString *imgViewType;

@implementation WarrantyAddViewController

@synthesize view_add, txt_product, txt_purchaseDate, txt_receiptPic, button_submit, lbl_product, lbl_purchaseDate, lbl_receiptPic, lbl_requiredproduct, lbl_requiredpurchDate, lbl_title;
@synthesize view_add1, txt_productPic, txt_serialNo, txt_sku, txt_storeName, button_submit1, button_done1, button_skip1, lbl_productPic, lbl_serialNo, lbl_sku, lbl_storeName, lbl_title1;
@synthesize view_add2, txt_warrantyLength, txt_warrantyCardPic, txt_comments, button_done2, button_skip2, lbl_comments, lbl_title2, lbl_warrantyCardPic, lbl_warrantyLength, view_add1scroll, view_add2scroll, view_addscroll;
@synthesize view_done, lbl_msgDone, lbl_titleDone;
@synthesize imageView,choosePhotoBtn, takePhotoBtn;
@synthesize imageView1,choosePhotoBtn1, takePhotoBtn1;
@synthesize imageView2,choosePhotoBtn2, takePhotoBtn2, cont_title;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
    //////NSLog(@">>> Entering *** %s <<<", __PRETTY_FUNCTION__);
    [self hideKeyPad];
}

- (IBAction) callHideKeyBoard:(id)sender{
	//////NSLog(@"testing...");
	[self hideKeyPad];
}

-(void) hideKeyPad {
    //////NSLog(@"#### %s",__PRETTY_FUNCTION__);
	[self.txt_product resignFirstResponder];
    [self.txt_comments resignFirstResponder];
    [self.txt_purchaseDate resignFirstResponder];
    [self.txt_serialNo resignFirstResponder];
	[self.txt_sku resignFirstResponder];
    [self.txt_storeName resignFirstResponder];
    [self.txt_warrantyLength resignFirstResponder];
}

-(BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    if (textField == txt_warrantyLength) {
        NSCharacterSet* numberCharSet = [NSCharacterSet characterSetWithCharactersInString:@"0123456789"];
        for (int i = 0; i < [string length]; ++i)
        {
            unichar c = [string characterAtIndex:i];
            if (![numberCharSet characterIsMember:c])
            {
                return NO;
            }
        }
    }
    
    return YES;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    UITapGestureRecognizer *gestureRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(hideKeyPad)];
	[self.view addGestureRecognizer:gestureRecognizer];
	gestureRecognizer.cancelsTouchesInView = NO;
    
    self.navigationItem.backBarButtonItem = [[UIBarButtonItem alloc]initWithTitle:@"" style:UIBarButtonItemStylePlain target:nil action:nil];
    
    appdelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    
    self.title = cont_title;
    appdelegate.homeclicked = NO;
    
    CGRect rect = CGRectMake(0,0,20,20);
    UIGraphicsBeginImageContext( rect.size );
    [[UIImage imageNamed:@"home.png"] drawInRect:rect];
    UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    UIBarButtonItem *myHomeButton = [[UIBarButtonItem alloc] initWithImage:image style:UIBarButtonItemStyleDone target:self action:@selector(myHomeAction:)];
    
    
    NSArray *myButtonArray = [[NSArray alloc] initWithObjects:myHomeButton, nil];
    
    self.navigationItem.rightBarButtonItems = myButtonArray;
    
    //[view_addscroll setFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height)];
    [self.view addSubview:view_add];
    
    view_addscroll = [[UIScrollView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height)];
    view_addscroll.backgroundColor = [UIColor whiteColor];
    [view_add addSubview:view_addscroll];
    
    lbl_title = [[UILabel alloc] initWithFrame:CGRectMake(20, 20, self.view.frame.size.width-40, 30)];
    [view_addscroll addSubview:lbl_title];
    UIFont *yourFont = [UIFont fontWithName:@"Helvetica-Bold" size:20];
    lbl_title.font = yourFont;
    [lbl_title setText:NSLocalizedString(@"addnew", nil)];
    //[lbl_title setTitleColor:[UIColor grayColor] forState:UIControlStateNormal];
    lbl_title.backgroundColor = [UIColor clearColor];
    lbl_title.textColor = [UIColor grayColor];
    lbl_title.opaque = NO;
    
    lbl_product = [[UILabel alloc] initWithFrame:CGRectMake(20, 68, self.view.frame.size.width-40, 20)];
    [view_addscroll addSubview:lbl_product];
    UIFont *yourFontpro = [UIFont fontWithName:@"Helvetica" size:17];
    lbl_product.font = yourFontpro;
    [lbl_product setText:NSLocalizedString(@"productname", nil)];
    //[lbl_title setTitleColor:[UIColor grayColor] forState:UIControlStateNormal];
    lbl_product.backgroundColor = [UIColor clearColor];
    lbl_product.textColor = [UIColor grayColor];
    lbl_product.opaque = NO;
    
    txt_product = [[UITextField alloc] initWithFrame:CGRectMake(20, 88, self.view.frame.size.width-40, 30)];
    txt_product.textColor = [UIColor blackColor];
    txt_product.font = [UIFont fontWithName:@"Helvetica" size:14];
    txt_product.backgroundColor=[UIColor whiteColor];
    //txt_product.text=@"Hello World";
    [txt_product setBorderStyle:UITextBorderStyleRoundedRect];
    [view_addscroll addSubview:txt_product];
    txt_product.delegate = self;
    
    lbl_requiredproduct = [[UILabel alloc] initWithFrame:CGRectMake(20, 117, self.view.frame.size.width-40, 20)];
    [view_addscroll addSubview:lbl_requiredproduct];
    UIFont *yourFontreq = [UIFont fontWithName:@"Helvetica" size:17];
    lbl_requiredproduct.font = yourFontreq;
    [lbl_requiredproduct setText:NSLocalizedString(@"required", nil)];
    //[lbl_title setTitleColor:[UIColor grayColor] forState:UIControlStateNormal];
    lbl_requiredproduct.backgroundColor = [UIColor clearColor];
    lbl_requiredproduct.textColor = [UIColor grayColor];
    lbl_requiredproduct.opaque = NO;
    
    lbl_purchaseDate = [[UILabel alloc] initWithFrame:CGRectMake(20, 146, self.view.frame.size.width-40, 20)];
    [view_addscroll addSubview:lbl_purchaseDate];
    UIFont *yourFontpurch = [UIFont fontWithName:@"Helvetica" size:17];
    lbl_purchaseDate.font = yourFontpurch;
    [lbl_purchaseDate setText:NSLocalizedString(@"purchasedate_dmy", nil)];
    //[lbl_title setTitleColor:[UIColor grayColor] forState:UIControlStateNormal];
    lbl_purchaseDate.backgroundColor = [UIColor clearColor];
    lbl_purchaseDate.textColor = [UIColor grayColor];
    lbl_purchaseDate.opaque = NO;
    
    txt_purchaseDate = [[UITextField alloc] initWithFrame:CGRectMake(20, 167, self.view.frame.size.width-40, 30)];
    txt_purchaseDate.textColor = [UIColor blackColor];
    txt_purchaseDate.font = [UIFont fontWithName:@"Helvetica" size:14];
    txt_purchaseDate.backgroundColor=[UIColor whiteColor];
    //txt_product.text=@"Hello World";
    [txt_purchaseDate setBorderStyle:UITextBorderStyleRoundedRect];
    [view_addscroll addSubview:txt_purchaseDate];
    txt_purchaseDate.delegate = self;
    
    lbl_requiredpurchDate = [[UILabel alloc] initWithFrame:CGRectMake(20, 198, self.view.frame.size.width-40, 20)];
    [view_addscroll addSubview:lbl_requiredpurchDate];
    UIFont *yourFontreqpurch = [UIFont fontWithName:@"Helvetica" size:17];
    lbl_requiredpurchDate.font = yourFontreqpurch;
    [lbl_requiredpurchDate setText:NSLocalizedString(@"required", nil)];
    //[lbl_title setTitleColor:[UIColor grayColor] forState:UIControlStateNormal];
    lbl_requiredpurchDate.backgroundColor = [UIColor clearColor];
    lbl_requiredpurchDate.textColor = [UIColor grayColor];
    lbl_requiredpurchDate.opaque = NO;
    
    lbl_receiptPic = [[UILabel alloc] initWithFrame:CGRectMake(20, 227, self.view.frame.size.width-40, 20)];
    [view_addscroll addSubview:lbl_receiptPic];
    UIFont *yourFontrec = [UIFont fontWithName:@"Helvetica" size:17];
    lbl_receiptPic.font = yourFontrec;
    [lbl_receiptPic setText:NSLocalizedString(@"receiptpicture", nil)];
    //[lbl_title setTitleColor:[UIColor grayColor] forState:UIControlStateNormal];
    lbl_receiptPic.backgroundColor = [UIColor clearColor];
    lbl_receiptPic.textColor = [UIColor grayColor];
    lbl_receiptPic.opaque = NO;

    takePhotoBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    [view_addscroll addSubview:takePhotoBtn];
    [takePhotoBtn setTitle:NSLocalizedString(@"takePhoto", nil) forState:UIControlStateNormal];
    [takePhotoBtn setTitleColor:[UIColor blueColor] forState:UIControlStateNormal];
    [takePhotoBtn addTarget:self action:@selector(getPhoto:) forControlEvents:UIControlEventTouchUpInside];
    takePhotoBtn.frame = CGRectMake(20, 266, 137, 37);
    
    choosePhotoBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    [view_addscroll addSubview:choosePhotoBtn];
    [choosePhotoBtn setTitle:NSLocalizedString(@"choosePhoto", nil) forState:UIControlStateNormal];
    [choosePhotoBtn setTitleColor:[UIColor blueColor] forState:UIControlStateNormal];
    [choosePhotoBtn addTarget:self action:@selector(getPhoto:) forControlEvents:UIControlEventTouchUpInside];
    choosePhotoBtn.frame = CGRectMake(20, 311, 135, 37);
    
    imageView = [[UIImageView alloc] initWithFrame:CGRectMake(164, 256, 135, 128)];
    //imageView.translatesAutoresizingMaskIntoConstraints = NO;
    //[imageView setContentMode:UIViewContentModeScaleAspectFit];
    //[imageView sizeToFit];
    [view_addscroll addSubview:imageView];
    //imageView.alpha = 1.0;
    
    button_submit = [UIButton buttonWithType:UIButtonTypeRoundedRect];
    [view_addscroll addSubview:button_submit];
    [button_submit setTitle:NSLocalizedString(@"submit", nil) forState:UIControlStateNormal];
    [button_submit setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [button_submit setBackgroundColor:[UIColor grayColor]];
    [button_submit addTarget:self action:@selector(onSubmitClicked:) forControlEvents:UIControlEventTouchUpInside];
    button_submit.frame = CGRectMake(20, 455, 107, 30);
    button_submit.layer.cornerRadius=6;
    
    view_addscroll.contentSize = CGSizeMake(view_addscroll.frame.size.width, view_addscroll.frame.size.height+100);
    
    view_add1scroll = [[UIScrollView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height)];
    view_add1scroll.backgroundColor = [UIColor whiteColor];
    [view_add1 addSubview:view_add1scroll];
    
    lbl_title1 = [[UILabel alloc] initWithFrame:CGRectMake(20, 10, self.view.frame.size.width-40, 30)];
    [view_add1scroll addSubview:lbl_title1];
    UIFont *yourFont1 = [UIFont fontWithName:@"Helvetica-Bold" size:20];
    lbl_title1.font = yourFont1;
    [lbl_title1 setText:NSLocalizedString(@"moredetails", nil)];
    //[lbl_title setTitleColor:[UIColor grayColor] forState:UIControlStateNormal];
    lbl_title1.backgroundColor = [UIColor clearColor];
    lbl_title1.textColor = [UIColor grayColor];
    lbl_title1.opaque = NO;
    
    lbl_productPic = [[UILabel alloc] initWithFrame:CGRectMake(20, 58, self.view.frame.size.width-40, 20)];
    [view_add1scroll addSubview:lbl_productPic];
    UIFont *yourFontppic = [UIFont fontWithName:@"Helvetica" size:17];
    lbl_productPic.font = yourFontppic;
    [lbl_productPic setText:NSLocalizedString(@"productpicture", nil)];
    //[lbl_title setTitleColor:[UIColor grayColor] forState:UIControlStateNormal];
    lbl_productPic.backgroundColor = [UIColor clearColor];
    lbl_productPic.textColor = [UIColor grayColor];
    lbl_productPic.opaque = NO;
    
    takePhotoBtn1 = [UIButton buttonWithType:UIButtonTypeCustom];
    [view_add1scroll addSubview:takePhotoBtn1];
    [takePhotoBtn1 setTitle:NSLocalizedString(@"takePhoto", nil) forState:UIControlStateNormal];
    [takePhotoBtn1 setTitleColor:[UIColor blueColor] forState:UIControlStateNormal];
    [takePhotoBtn1 addTarget:self action:@selector(getPhoto1:) forControlEvents:UIControlEventTouchUpInside];
    takePhotoBtn1.frame = CGRectMake(20, 105, 137, 37);
    
    choosePhotoBtn1 = [UIButton buttonWithType:UIButtonTypeCustom];
    [view_add1scroll addSubview:choosePhotoBtn1];
    [choosePhotoBtn1 setTitle:NSLocalizedString(@"choosePhoto", nil) forState:UIControlStateNormal];
    [choosePhotoBtn1 setTitleColor:[UIColor blueColor] forState:UIControlStateNormal];
    [choosePhotoBtn1 addTarget:self action:@selector(getPhoto1:) forControlEvents:UIControlEventTouchUpInside];
    choosePhotoBtn1.frame = CGRectMake(20, 150, 135, 37);
    
    imageView1 = [[UIImageView alloc] initWithFrame:CGRectMake(164, 87, 135, 128)];
    //imageView1.translatesAutoresizingMaskIntoConstraints = NO;
    //[imageView1 setContentMode:UIViewContentModeScaleAspectFit];
    //[imageView1 sizeToFit];
    [view_add1scroll addSubview:imageView1];
    
    lbl_serialNo = [[UILabel alloc] initWithFrame:CGRectMake(20, 223, self.view.frame.size.width-40, 20)];
    [view_add1scroll addSubview:lbl_serialNo];
    UIFont *yourFontsn = [UIFont fontWithName:@"Helvetica" size:17];
    lbl_product.font = yourFontsn;
    [lbl_serialNo setText:NSLocalizedString(@"serialno", nil)];
    //[lbl_title setTitleColor:[UIColor grayColor] forState:UIControlStateNormal];
    lbl_serialNo.backgroundColor = [UIColor clearColor];
    lbl_serialNo.textColor = [UIColor grayColor];
    lbl_serialNo.opaque = NO;
    
    txt_serialNo = [[UITextField alloc] initWithFrame:CGRectMake(20, 252, self.view.frame.size.width-40, 30)];
    txt_serialNo.textColor = [UIColor blackColor];
    txt_serialNo.font = [UIFont fontWithName:@"Helvetica" size:14];
    txt_serialNo.backgroundColor=[UIColor whiteColor];
    //txt_product.text=@"Hello World";
    [txt_serialNo setBorderStyle:UITextBorderStyleRoundedRect];
    [view_add1scroll addSubview:txt_serialNo];
    txt_serialNo.delegate = self;
    
    lbl_sku = [[UILabel alloc] initWithFrame:CGRectMake(20, 300, self.view.frame.size.width-40, 20)];
    [view_add1scroll addSubview:lbl_sku];
    UIFont *yourFontsku = [UIFont fontWithName:@"Helvetica" size:17];
    lbl_sku.font = yourFontsku;
    [lbl_sku setText:NSLocalizedString(@"modelsku", nil)];
    //[lbl_title setTitleColor:[UIColor grayColor] forState:UIControlStateNormal];
    lbl_sku.backgroundColor = [UIColor clearColor];
    lbl_sku.textColor = [UIColor grayColor];
    lbl_sku.opaque = NO;
    
    txt_sku = [[UITextField alloc] initWithFrame:CGRectMake(20, 329, self.view.frame.size.width-40, 30)];
    txt_sku.textColor = [UIColor blackColor];
    txt_sku.font = [UIFont fontWithName:@"Helvetica" size:14];
    txt_sku.backgroundColor=[UIColor whiteColor];
    //txt_product.text=@"Hello World";
    [txt_sku setBorderStyle:UITextBorderStyleRoundedRect];
    [view_add1scroll addSubview:txt_sku];
    txt_sku.delegate = self;
    
    lbl_storeName = [[UILabel alloc] initWithFrame:CGRectMake(20, 373, self.view.frame.size.width-40, 20)];
    [view_add1scroll addSubview:lbl_storeName];
    UIFont *yourFontstore = [UIFont fontWithName:@"Helvetica" size:17];
    lbl_storeName.font = yourFontstore;
    [lbl_storeName setText:NSLocalizedString(@"storename", nil)];
    //[lbl_title setTitleColor:[UIColor grayColor] forState:UIControlStateNormal];
    lbl_storeName.backgroundColor = [UIColor clearColor];
    lbl_storeName.textColor = [UIColor grayColor];
    lbl_storeName.opaque = NO;
    
    txt_storeName = [[UITextField alloc] initWithFrame:CGRectMake(20, 402, self.view.frame.size.width-40, 30)];
    txt_storeName.textColor = [UIColor blackColor];
    txt_storeName.font = [UIFont fontWithName:@"Helvetica" size:14];
    txt_storeName.backgroundColor=[UIColor whiteColor];
    //txt_product.text=@"Hello World";
    [txt_storeName setBorderStyle:UITextBorderStyleRoundedRect];
    [view_add1scroll addSubview:txt_storeName];
    txt_storeName.delegate = self;
    
    button_submit1 = [UIButton buttonWithType:UIButtonTypeRoundedRect];
    [view_add1scroll addSubview:button_submit1];
    [button_submit1 setTitle:NSLocalizedString(@"submit", nil) forState:UIControlStateNormal];
    [button_submit1 setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [button_submit1 setBackgroundColor:[UIColor grayColor]];
    [button_submit1 addTarget:self action:@selector(onSubmit1Clicked:) forControlEvents:UIControlEventTouchUpInside];
    button_submit1.frame = CGRectMake(20, 457, 86, 30);
    button_submit1.layer.cornerRadius=6;
    
    button_done1 = [UIButton buttonWithType:UIButtonTypeRoundedRect];
    [view_add1scroll addSubview:button_done1];
    [button_done1 setTitle:NSLocalizedString(@"done", nil) forState:UIControlStateNormal];
    [button_done1 setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [button_done1 setBackgroundColor:[UIColor grayColor]];
    [button_done1 addTarget:self action:@selector(onDone1Clicked:) forControlEvents:UIControlEventTouchUpInside];
    button_done1.frame = CGRectMake(114, 457, 86, 30);
    button_done1.layer.cornerRadius=6;
    
    button_skip1 = [UIButton buttonWithType:UIButtonTypeRoundedRect];
    [view_add1scroll addSubview:button_skip1];
    [button_skip1 setTitle:NSLocalizedString(@"skip", nil) forState:UIControlStateNormal];
    [button_skip1 setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [button_skip1 setBackgroundColor:[UIColor grayColor]];
    [button_skip1 addTarget:self action:@selector(onSkip1Clicked:) forControlEvents:UIControlEventTouchUpInside];
    button_skip1.frame = CGRectMake(208, 457, 86, 30);
    button_skip1.layer.cornerRadius=6;
    
    view_add1scroll.contentSize = CGSizeMake(view_add1scroll.frame.size.width, view_add1scroll.frame.size.height+100);
    
    
    view_add2scroll = [[UIScrollView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height)];
    view_add2scroll.backgroundColor = [UIColor whiteColor];
    [view_add2 addSubview:view_add2scroll];
    
    lbl_title2 = [[UILabel alloc] initWithFrame:CGRectMake(20, 10, self.view.frame.size.width-40, 30)];
    [view_add2scroll addSubview:lbl_title2];
    UIFont *yourFont2 = [UIFont fontWithName:@"Helvetica-Bold" size:20];
    lbl_title2.font = yourFont2;
    [lbl_title2 setText:NSLocalizedString(@"moredetails", nil)];
    //[lbl_title setTitleColor:[UIColor grayColor] forState:UIControlStateNormal];
    lbl_title2.backgroundColor = [UIColor clearColor];
    lbl_title2.textColor = [UIColor grayColor];
    lbl_title2.opaque = NO;

    lbl_warrantyLength = [[UILabel alloc] initWithFrame:CGRectMake(20, 59, self.view.frame.size.width-40, 20)];
    [view_add2scroll addSubview:lbl_warrantyLength];
    UIFont *yourFontwl = [UIFont fontWithName:@"Helvetica" size:17];
    lbl_warrantyLength.font = yourFontwl;
    [lbl_warrantyLength setText:NSLocalizedString(@"warrantylength", nil)];
    //[lbl_title setTitleColor:[UIColor grayColor] forState:UIControlStateNormal];
    lbl_warrantyLength.backgroundColor = [UIColor clearColor];
    lbl_warrantyLength.textColor = [UIColor grayColor];
    lbl_warrantyLength.opaque = NO;
    
    txt_warrantyLength = [[UITextField alloc] initWithFrame:CGRectMake(20, 88, self.view.frame.size.width-40, 30)];
    txt_warrantyLength.textColor = [UIColor blackColor];
    txt_warrantyLength.font = [UIFont fontWithName:@"Helvetica" size:14];
    txt_warrantyLength.backgroundColor=[UIColor whiteColor];
    //txt_product.text=@"Hello World";
    [txt_warrantyLength setBorderStyle:UITextBorderStyleRoundedRect];
    [view_add2scroll addSubview:txt_warrantyLength];
    txt_warrantyLength.delegate = self;
    
    lbl_warrantyCardPic = [[UILabel alloc] initWithFrame:CGRectMake(20, 133, self.view.frame.size.width-40, 20)];
    [view_add2scroll addSubview:lbl_warrantyCardPic];
    UIFont *yourFontwcp = [UIFont fontWithName:@"Helvetica" size:17];
    lbl_warrantyCardPic.font = yourFontwcp;
    [lbl_warrantyCardPic setText:NSLocalizedString(@"warrantypicture", nil)];
    //[lbl_title setTitleColor:[UIColor grayColor] forState:UIControlStateNormal];
    lbl_warrantyCardPic.backgroundColor = [UIColor clearColor];
    lbl_warrantyCardPic.textColor = [UIColor grayColor];
    lbl_warrantyCardPic.opaque = NO;
    
    takePhotoBtn2 = [UIButton buttonWithType:UIButtonTypeCustom];
    [view_add2scroll addSubview:takePhotoBtn2];
    [takePhotoBtn2 setTitle:NSLocalizedString(@"takePhoto", nil) forState:UIControlStateNormal];
    [takePhotoBtn2 setTitleColor:[UIColor blueColor] forState:UIControlStateNormal];
    [takePhotoBtn2 addTarget:self action:@selector(getPhoto2:) forControlEvents:UIControlEventTouchUpInside];
    takePhotoBtn2.frame = CGRectMake(20, 180, 137, 37);
    
    choosePhotoBtn2 = [UIButton buttonWithType:UIButtonTypeCustom];
    [view_add2scroll addSubview:choosePhotoBtn2];
    [choosePhotoBtn2 setTitle:NSLocalizedString(@"choosePhoto", nil) forState:UIControlStateNormal];
    [choosePhotoBtn2 setTitleColor:[UIColor blueColor] forState:UIControlStateNormal];
    [choosePhotoBtn2 addTarget:self action:@selector(getPhoto2:) forControlEvents:UIControlEventTouchUpInside];
    choosePhotoBtn2.frame = CGRectMake(20, 225, 135, 37);
    
    imageView2 = [[UIImageView alloc] initWithFrame:CGRectMake(164, 162, 135, 128)];
    //imageView1.translatesAutoresizingMaskIntoConstraints = NO;
    //[imageView1 setContentMode:UIViewContentModeScaleAspectFit];
    //[imageView1 sizeToFit];
    [view_add2scroll addSubview:imageView2];
    
    lbl_comments = [[UILabel alloc] initWithFrame:CGRectMake(20, 298, self.view.frame.size.width-40, 20)];
    [view_add2scroll addSubview:lbl_comments];
    UIFont *yourFontcm = [UIFont fontWithName:@"Helvetica" size:17];
    lbl_comments.font = yourFontcm;
    [lbl_comments setText:NSLocalizedString(@"comments", nil)];
    //[lbl_title setTitleColor:[UIColor grayColor] forState:UIControlStateNormal];
    lbl_comments.backgroundColor = [UIColor clearColor];
    lbl_comments.textColor = [UIColor grayColor];
    lbl_comments.opaque = NO;
    
    txt_comments = [[UITextField alloc] initWithFrame:CGRectMake(20, 327, self.view.frame.size.width-40, 30)];
    txt_comments.textColor = [UIColor blackColor];
    txt_comments.font = [UIFont fontWithName:@"Helvetica" size:14];
    txt_comments.backgroundColor=[UIColor whiteColor];
    //txt_product.text=@"Hello World";
    [txt_comments setBorderStyle:UITextBorderStyleRoundedRect];
    [view_add2scroll addSubview:txt_comments];
    txt_comments.delegate = self;
    
    button_done2 = [UIButton buttonWithType:UIButtonTypeRoundedRect];
    [view_add2scroll addSubview:button_done2];
    [button_done2 setTitle:NSLocalizedString(@"done", nil) forState:UIControlStateNormal];
    [button_done2 setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [button_done2 setBackgroundColor:[UIColor grayColor]];
    [button_done2 addTarget:self action:@selector(onDone2Clicked:) forControlEvents:UIControlEventTouchUpInside];
    button_done2.frame = CGRectMake(20, 381, 86, 30);
    button_done2.layer.cornerRadius=6;
    
    button_skip2 = [UIButton buttonWithType:UIButtonTypeRoundedRect];
    [view_add2scroll addSubview:button_skip2];
    [button_skip2 setTitle:NSLocalizedString(@"skip", nil) forState:UIControlStateNormal];
    [button_skip2 setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [button_skip2 setBackgroundColor:[UIColor grayColor]];
    [button_skip2 addTarget:self action:@selector(onSkip2Clicked:) forControlEvents:UIControlEventTouchUpInside];
    button_skip2.frame = CGRectMake(144, 381, 86, 30);
    button_skip2.layer.cornerRadius=6;
    
    view_add2scroll.contentSize = CGSizeMake(view_add2scroll.frame.size.width, view_add2scroll.frame.size.height+100);
    
    [lbl_titleDone setText:NSLocalizedString(@"done", nil)];
    lbl_msgDone.numberOfLines = 0;
    lbl_msgDone.backgroundColor = [UIColor whiteColor];
    lbl_msgDone.text = [NSString stringWithFormat:@"Your warranty details has been updated to the list."];
    [lbl_msgDone sizeToFit];
    
    UIDatePicker *datePicker = [[UIDatePicker alloc]init];
    [datePicker setDatePickerMode:UIDatePickerModeDate];
    [datePicker setDate:[NSDate date]];
    [datePicker addTarget:self action:@selector(updateTextField:) forControlEvents:UIControlEventValueChanged];
    [self.txt_purchaseDate setInputView:datePicker];
    self.txt_purchaseDate.text = [self formatDate:[NSDate date]];
}

- (NSString *)formatDate:(NSDate *)date
{
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateStyle:NSDateFormatterShortStyle];
    [dateFormatter setDateFormat:@"dd'-'MM'-'yyyy"];
    NSString *formattedDate = [dateFormatter stringFromDate:date];
    return formattedDate;
}

-(void)updateTextField:(id)sender
{
    UIDatePicker *picker = (UIDatePicker*)self.txt_purchaseDate.inputView;
    //self.txt_purchaseDate.text = [NSString stringWithFormat:@"%@",picker.date];
    self.txt_purchaseDate.text = [self formatDate:picker.date];
}


//Slide Menu
- (void)willAnimateRotationToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation duration:(NSTimeInterval)duration
{
    //NSLog(@"view frame=%@ bounds=%@", NSStringFromCGRect(self.view.frame), NSStringFromCGRect(self.view.bounds));
}


- (void)showModalButtonWasTouched
{
    self.interactor = [[BTSlideInteractor alloc] init];
    self.interactor.presenting = YES;
    
    //BTModalViewController *modalController = [self.storyboard instantiateViewControllerWithIdentifier:@"ModalViewController"];
    modalController = [[InitialSlidingViewController alloc] init];
    modalController.modalPresentationStyle = UIModalPresentationCustom;
    modalController.transitioningDelegate = self.interactor;
    [self.navigationController presentViewController:modalController animated:YES completion:nil];
    //[self.navigationController popToViewController:modalController animated:NO];
    //[self.navigationController pushViewController:modalController animated:YES];
}
// End Slide Menu

-(void)myHomeAction:(UIBarButtonItem *)sender{
    
    appdelegate.nextPage = @"";
    
    if ([appdelegate.listOfVerMenu count]>0) {
        if (appdelegate.homeclicked==NO) {
            appdelegate.homeclicked = YES;
            [self showModalButtonWasTouched];
        } else {
            appdelegate.homeclicked = NO;
            [modalController dismissViewControllerAnimated:YES completion:nil];
        }
    } else {
        //AppDelegate *appdelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
        //NSLog(@"View Controllers : %@",[appdelegate.navController viewControllers]);
        NSMutableArray *navigationArray = [[NSMutableArray alloc] initWithArray: appdelegate.navController.viewControllers];
        
        for (UIViewController* viewController in appdelegate.navController.viewControllers) {
            
            //This if condition checks whether the viewController's class is MyGroupViewController
            // if true that means its the MyGroupViewController (which has been pushed at some point)
            if ([viewController isKindOfClass:[RootViewController class]] ) {
                
                // Here viewController is a reference of UIViewController base class of MyGroupViewController
                // but viewController holds MyGroupViewController  object so we can type cast it here
                //ThemeViewController *themeViewController = (ThemeViewController*)viewController;
                //[self.navigationController popToViewController:themeViewController animated:YES];
                [navigationArray removeObjectAtIndex: 0];
            }
        }
        appdelegate.navController.viewControllers = navigationArray;
        for (UIViewController* viewController in appdelegate.navController.viewControllers) {
            
            //This if condition checks whether the viewController's class is MyGroupViewController
            // if true that means its the MyGroupViewController (which has been pushed at some point)
            if ([viewController isKindOfClass:[ViewController class]] ) {
                
                // Here viewController is a reference of UIViewController base class of MyGroupViewController
                // but viewController holds MyGroupViewController  object so we can type cast it here
                ViewController *themeViewController = (ViewController*)viewController;
                [appdelegate.navController popToViewController:themeViewController animated:NO];
                //[self.navigationController popToViewController:viewController animated:YES];
            }
        }
        //NSLog(@"View Controllers111 : %@",appdelegate.navController.viewControllers);
    }
    
}

- (IBAction)onSubmitClicked:(id)sender {
    
    if (appdelegate.showloading) {
        alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"loading", nil) message:nil delegate:self cancelButtonTitle:nil otherButtonTitles: nil];
        
        //a simple activity indicator:
        UIActivityIndicatorView *activityIndicator = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
        activityIndicator.frame= CGRectMake(50, 10, 37, 37);
        activityIndicator.autoresizingMask = UIViewAutoresizingFlexibleTopMargin | UIViewAutoresizingFlexibleRightMargin | UIViewAutoresizingFlexibleBottomMargin | UIViewAutoresizingFlexibleLeftMargin;
        [activityIndicator startAnimating];
        
        //the magic line below,
        //we associate the activity indicator to the alert view: (addSubview is not used)
        [alert setValue:activityIndicator forKey:@"accessoryView"];
        
        [alert show];
    
        // Adjust the indicator so it is up a few pixels from the bottom of the alert
        [UIView beginAnimations:nil context:nil];
        [UIView setAnimationDuration:0.0];
        [UIView setAnimationDelay:0.0];
        [UIView setAnimationTransition:UIViewAnimationTransitionNone forView:alert cache:YES];
        [UIView setAnimationDelegate:self];
        [UIView setAnimationDidStopSelector:@selector(startupAnimationDoneIndicatorStep2Submit:finished:context:)];
        alert.alpha = 0.5;
        [UIView commitAnimations];
    } else {
        [self goStep2Submit];
    }
    
}

- (IBAction)onSubmit1Clicked:(id)sender {
    
    if (appdelegate.showloading) {
        alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"loading", nil) message:nil delegate:self cancelButtonTitle:nil otherButtonTitles: nil];
        
        //a simple activity indicator:
        UIActivityIndicatorView *activityIndicator = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
        activityIndicator.frame= CGRectMake(50, 10, 37, 37);
        activityIndicator.autoresizingMask = UIViewAutoresizingFlexibleTopMargin | UIViewAutoresizingFlexibleRightMargin | UIViewAutoresizingFlexibleBottomMargin | UIViewAutoresizingFlexibleLeftMargin;
        [activityIndicator startAnimating];
        
        //the magic line below,
        //we associate the activity indicator to the alert view: (addSubview is not used)
        [alert setValue:activityIndicator forKey:@"accessoryView"];
        
        [alert show];
    
        // Adjust the indicator so it is up a few pixels from the bottom of the alert
        [UIView beginAnimations:nil context:nil];
        [UIView setAnimationDuration:0.0];
        [UIView setAnimationDelay:0.0];
        [UIView setAnimationTransition:UIViewAnimationTransitionNone forView:alert cache:YES];
        [UIView setAnimationDelegate:self];
        [UIView setAnimationDidStopSelector:@selector(startupAnimationDoneIndicatorStep3Submit:finished:context:)];
        alert.alpha = 0.5;
        [UIView commitAnimations];
    } else {
        [self goStep3Submit];
    }
    
}

- (IBAction)onDone1Clicked:(id)sender {
    
    if (appdelegate.showloading) {
        alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"loading", nil) message:nil delegate:self cancelButtonTitle:nil otherButtonTitles: nil];
        
        //a simple activity indicator:
        UIActivityIndicatorView *activityIndicator = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
        activityIndicator.frame= CGRectMake(50, 10, 37, 37);
        activityIndicator.autoresizingMask = UIViewAutoresizingFlexibleTopMargin | UIViewAutoresizingFlexibleRightMargin | UIViewAutoresizingFlexibleBottomMargin | UIViewAutoresizingFlexibleLeftMargin;
        [activityIndicator startAnimating];
        
        //the magic line below,
        //we associate the activity indicator to the alert view: (addSubview is not used)
        [alert setValue:activityIndicator forKey:@"accessoryView"];
        
        [alert show];
        
        // Adjust the indicator so it is up a few pixels from the bottom of the alert
        [UIView beginAnimations:nil context:nil];
        [UIView setAnimationDuration:0.0];
        [UIView setAnimationDelay:0.0];
        [UIView setAnimationTransition:UIViewAnimationTransitionNone forView:alert cache:YES];
        [UIView setAnimationDelegate:self];
        [UIView setAnimationDidStopSelector:@selector(startupAnimationDoneIndicatorStep3Done:finished:context:)];
        alert.alpha = 0.5;
        [UIView commitAnimations];
    } else {
        [self goStep3Done];
    }
    
}

- (IBAction)onSkip1Clicked:(id)sender {
    [self.view addSubview:view_add2];
    [view_add1 removeFromSuperview];
    [view_add removeFromSuperview];
    [view_done removeFromSuperview];
}

- (IBAction)onDone2Clicked:(id)sender {
    
    if (appdelegate.showloading) {
        alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"loading", nil) message:nil delegate:self cancelButtonTitle:nil otherButtonTitles: nil];
        
        //a simple activity indicator:
        UIActivityIndicatorView *activityIndicator = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
        activityIndicator.frame= CGRectMake(50, 10, 37, 37);
        activityIndicator.autoresizingMask = UIViewAutoresizingFlexibleTopMargin | UIViewAutoresizingFlexibleRightMargin | UIViewAutoresizingFlexibleBottomMargin | UIViewAutoresizingFlexibleLeftMargin;
        [activityIndicator startAnimating];
        
        //the magic line below,
        //we associate the activity indicator to the alert view: (addSubview is not used)
        [alert setValue:activityIndicator forKey:@"accessoryView"];
        
        [alert show];
    
        // Adjust the indicator so it is up a few pixels from the bottom of the alert
        [UIView beginAnimations:nil context:nil];
        [UIView setAnimationDuration:0.0];
        [UIView setAnimationDelay:0.0];
        [UIView setAnimationTransition:UIViewAnimationTransitionNone forView:alert cache:YES];
        [UIView setAnimationDelegate:self];
        [UIView setAnimationDidStopSelector:@selector(startupAnimationDoneIndicatorStepDone:finished:context:)];
        alert.alpha = 0.5;
        [UIView commitAnimations];
    } else {
        [self goStepDone];
    }
    
}

- (IBAction)onSkip2Clicked:(id)sender {
    warranty_id = @"0";
    [self.view addSubview:view_done];
    [view_add1 removeFromSuperview];
    [view_add removeFromSuperview];
    [view_add2 removeFromSuperview];
}

#pragma mark Connect To API
-(void) connectToServer:(NSString *) urlPath {
    [self addLoadingView];
	NSString *urlAddress =[NSString stringWithFormat:@"%@", urlPath];
	
	/*NSURL *url = [NSURL URLWithString:urlAddress];
	
	NSURLRequest *requestObj = [NSURLRequest requestWithURL:url
												cachePolicy:NSURLRequestReloadIgnoringLocalAndRemoteCacheData
											timeoutInterval:30];
	
	NSURLConnection *theConnection=[[NSURLConnection alloc] initWithRequest:requestObj delegate:self];
	if (theConnection) {
		// Create the NSMutableData to hold the received data.
		// receivedData is an instance variable declared elsewhere.
		receivedData = [NSMutableData data];
	} else {
		// Inform the user that the connection failed.
		//[self displayAlert:@"Connection Error. Please try again later."];
		[self displayAlert:NSLocalizedString(@"MSG_CONNECTION_ERROR", nil)];
        [self removeLoadingView];
	} */
    [self getJsonResponse:urlAddress success:^(NSDictionary *responseDict) {
        
        //NSLog(@"%@",responseDict);
        [self successfunction:responseDict];
        
    } failure:^(NSError *error) {
        // release the connection, and the data object
        
        [self removeLoadingView];
        [self displayAlert:NSLocalizedString(@"MSG_CONNECTION_ERROR", nil)];
    }];
}

-(void)getJsonResponse : (NSString *)urlStr success : (void (^)(NSDictionary *responseDict))success failure:(void(^)(NSError* error))failure
{
    
    
    NSURLSession * session = [NSURLSession sharedSession];
    NSURL * url = [NSURL URLWithString: urlStr];
    
    
    // Asynchronously Api is hit here
    NSURLSessionDataTask * dataTask = [session dataTaskWithURL:url completionHandler:^(NSData *data, NSURLResponse *response, NSError *error)
                                       {
                                           
                                           //NSLog(@"%@",data);
                                           NSDictionary * json;
                                           if ([data length]>0) {
                                               json  = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
                                               //NSLog(@"%@",json);
                                               success(json);
                                           } else {
                                               json = nil;
                                               dispatch_async(dispatch_get_main_queue(), ^ {
                                                   [self removeLoadingView];
                                                   [self displayAlert:NSLocalizedString(@"MSG_CONNECTION_ERROR", nil)];
                                               });
                                           }
                                           
                                           
                                       }];
    
    [dataTask resume] ; // Executed First
    
    
}

-(void) successfunction:(NSDictionary *)responseDict {
    //NSString *jsonString = [responseDict JSONRepresentation];
    dispatch_async(dispatch_get_main_queue(), ^{
        [self removeLoadingView];
    });
    
}

/*- (void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response
{
    [receivedData setLength:0];
}

- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data
{
    [receivedData appendData:data];
}

- (void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error {
    // release the connection, and the data object
	
    connection =nil;
	receivedData =nil;
	
    [self removeLoadingView];
	[self displayAlert:NSLocalizedString(@"MSG_CONNECTION_ERROR", nil)];
}

- (void)connectionDidFinishLoading:(NSURLConnection *)connection
{
	NSLog(@"Succeed! Received %d bytes of data",[receivedData length]);
    [self removeLoadingView];
} */

#pragma mark LoadingView
-(void) addLoadingView
{
	loadingView = [LoadingView loadingViewInView:self.view];
}

-(void) removeLoadingView
{
	[loadingView removeView];
}

#pragma mark displayAlert
-(void) displayAlert: (NSString *) stralert {
	NSString *str = [[NSString alloc] initWithFormat:@"%@", stralert];
	UIAlertView *alert = [[UIAlertView alloc] initWithTitle:appdelegate.iconname
													message:str
												   delegate:self
										  cancelButtonTitle:NSLocalizedString(@"done", nil)
										  otherButtonTitles:nil];
	
	[alert show];
	str =nil;
	alert =nil;
}


-(IBAction) getPhoto:(id) sender {
    imgViewType = @"imgView";
	UIImagePickerController * picker = [[UIImagePickerController alloc] init];
	picker.delegate = self;
	
	if((UIButton *) sender == choosePhotoBtn) {
		picker.sourceType = UIImagePickerControllerSourceTypeSavedPhotosAlbum;
	} else {
		picker.sourceType = UIImagePickerControllerSourceTypeCamera;
	}
	
    [appdelegate.navController presentViewController:picker animated:NO completion:nil];
}

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info {
	[picker dismissViewControllerAnimated:NO completion:nil];
    if ([imgViewType isEqualToString:@"imgView"]) {
        imageView.image = [info objectForKey:@"UIImagePickerControllerOriginalImage"];
    } else if ([imgViewType isEqualToString:@"imgView1"]) {
        imageView1.image = [info objectForKey:@"UIImagePickerControllerOriginalImage"];
    } else if ([imgViewType isEqualToString:@"imgView2"]) {
        imageView2.image = [info objectForKey:@"UIImagePickerControllerOriginalImage"];
    }
}

-(IBAction) getPhoto1:(id) sender {
    imgViewType = @"imgView1";

	UIImagePickerController * picker = [[UIImagePickerController alloc] init];
	picker.delegate = self;
	
	if((UIButton *) sender == choosePhotoBtn1) {
		picker.sourceType = UIImagePickerControllerSourceTypeSavedPhotosAlbum;
	} else {
		picker.sourceType = UIImagePickerControllerSourceTypeCamera;
	}
	
    [appdelegate.navController presentViewController:picker animated:NO completion:nil];
}

-(IBAction) getPhoto2:(id) sender {
    imgViewType = @"imgView2";

	UIImagePickerController * picker = [[UIImagePickerController alloc] init];
	picker.delegate = self;
	
	if((UIButton *) sender == choosePhotoBtn2) {
		picker.sourceType = UIImagePickerControllerSourceTypeSavedPhotosAlbum;
	} else {
		picker.sourceType = UIImagePickerControllerSourceTypeCamera;
	}
	
    [appdelegate.navController presentViewController:picker animated:NO completion:nil];
}

-(BOOL) textFieldShouldReturn: (UITextField *) textField {
    [textField resignFirstResponder];
    return YES;
}

-(void) dissmissIndicator {
    [alert dismissWithClickedButtonIndex:0 animated:NO];
}

-(void) goStep2Submit {
    if ([self isConnectionAvailable])
    {
        //Save Data
        if ([txt_product.text length]>0 && [txt_purchaseDate.text length]>0) {
            
            NSData *imageData = UIImageJPEGRepresentation(imageView.image, 90);
            // setting up the URL to post to
            NSString *urlString = [[NSString alloc] initWithFormat:@"%@warranty_submit.php?db=%@&userid=%@&udid=%@&cuid=%@&task=step1&title=%@&purchase_date=%@&device=ios&sc=%@",appdelegate.user_apipath,appdelegate.db,appdelegate.user,appdelegate.uuid,appdelegate.customer_id, txt_product.text, txt_purchaseDate.text, appdelegate.cs];
            
            NSString* encodedUrl = [urlString stringByAddingPercentEscapesUsingEncoding:
                                    NSUTF8StringEncoding];
            
            // setting up the request object now
            NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
            [request setURL:[NSURL URLWithString:encodedUrl]];
            [request setHTTPMethod:@"POST"];
            
            /*
             add some header info now
             we always need a boundary when we post a file
             also we need to set the content type
             
             You might want to generate a random boundary.. this is just the same
             as my output from wireshark on a valid html post
             */
            //NSString *boundary = [NSString stringWithString:@"---------------------------14737809831466499882746641449"];
            NSString *boundary = @"---------------------------14737809831466499882746641449";
            NSString *contentType = [NSString stringWithFormat:@"multipart/form-data; boundary=%@",boundary];
            [request addValue:contentType forHTTPHeaderField: @"Content-Type"];
            
            /*
             now lets create the body of the post
             */
            NSMutableData *body = [NSMutableData data];
            [body appendData:[[NSString stringWithFormat:@"\r\n--%@\r\n",boundary] dataUsingEncoding:NSUTF8StringEncoding]];
            //[body appendData:[[NSString stringWithString:@"Content-Disposition: form-data; name=\"userfile\"; filename=\"imgreceipt.jpg\"\r\n"] dataUsingEncoding:NSUTF8StringEncoding]];
            [body appendData:[@"Content-Disposition: form-data; name=\"uploaded_file\"; filename=\"imgreceipt.jpg\"\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
            //[body appendData:[[NSString stringWithString:@"Content-Type: application/octet-stream\r\n\r\n"] dataUsingEncoding:NSUTF8StringEncoding]];
            [body appendData:[@"Content-Type: application/octet-stream\r\n\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
            [body appendData:[NSData dataWithData:imageData]];
            [body appendData:[[NSString stringWithFormat:@"\r\n--%@--\r\n",boundary] dataUsingEncoding:NSUTF8StringEncoding]];
            // setting the body of the post to the reqeust
            [request setHTTPBody:body];
            
            // now lets make the connection to the web
            /*NSData *returnData = [NSURLConnection sendSynchronousRequest:request returningResponse:nil error:nil];
            NSString *returnString = [[NSString alloc] initWithData:returnData encoding:NSUTF8StringEncoding];
            
            //NSLog(@"returnString:: %@",returnString); */
            NSURLSession *session = [NSURLSession sharedSession];
            
            [[session dataTaskWithRequest:request completionHandler:^(NSData * _Nullable data, NSURLResponse * _Nullable response, NSError * _Nullable error) {
                
                if (data) {
                    
                    //if response is in string format
                    NSString *returnString = [[NSString alloc]initWithData:data encoding:NSUTF8StringEncoding];
                    NSDictionary *dictionary = [returnString JSONValue];
                    for(NSDictionary *row in dictionary){
                        warranty_id = [row objectForKey:@"warranty_id"];
                    }
                    //warranty_id = [jsonData objectForKey:@"desc"];
                    //NSLog(@"warranty_id:: %@",warranty_id);
                    
                    //if response in json format then
                    //id jsonObject = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:nil];
                    
                }
                
            }]resume];
            
            [self.view addSubview:view_add1];
            [view_add removeFromSuperview];
            [view_add2 removeFromSuperview];
            [view_done removeFromSuperview];
            
        } else {
            [self displayAlert:NSLocalizedString(@"fieldsblankmsg", nil)];
        }
    } else {
        [self displayAlert:NSLocalizedString(@"MSG_NO_INTERNET", nil)];
    }
}

- (void)startupAnimationDoneIndicatorStep2Submit:(NSString *)animationID finished:(NSNumber *)finished context:(void *)context
{
    [self goStep2Submit];
    [self dissmissIndicator];
    alert =nil;
}

-(void) goStep3Submit {
    if ([self isConnectionAvailable])
    {
        //Save Data
        NSData *imageData = UIImageJPEGRepresentation(imageView1.image, 90);
        // setting up the URL to post to
        NSString *urlString = [[NSString alloc] initWithFormat:@"%@warranty_submit.php?db=%@&userid=%@&udid=%@&cuid=%@&task=step2&warranty_id=%@&dealer=%@&serial_number=%@&model_number=%@&device=ios&sc=%@",appdelegate.user_apipath,appdelegate.db,appdelegate.user,appdelegate.uuid,appdelegate.customer_id, warranty_id, txt_storeName.text, txt_serialNo.text, txt_sku.text, appdelegate.cs];
        
        NSString* encodedUrl = [urlString stringByAddingPercentEscapesUsingEncoding:
                                NSUTF8StringEncoding];
        
        // setting up the request object now
        NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
        [request setURL:[NSURL URLWithString:encodedUrl]];
        [request setHTTPMethod:@"POST"];
        
        /*
         add some header info now
         we always need a boundary when we post a file
         also we need to set the content type
         
         You might want to generate a random boundary.. this is just the same
         as my output from wireshark on a valid html post
         */
        //NSString *boundary = [NSString stringWithString:@"---------------------------14737809831466499882746641449"];
        NSString *boundary = @"---------------------------14737809831466499882746641449";
        NSString *contentType = [NSString stringWithFormat:@"multipart/form-data; boundary=%@",boundary];
        [request addValue:contentType forHTTPHeaderField: @"Content-Type"];
        
        /*
         now lets create the body of the post
         */
        NSMutableData *body = [NSMutableData data];
        [body appendData:[[NSString stringWithFormat:@"\r\n--%@\r\n",boundary] dataUsingEncoding:NSUTF8StringEncoding]];
        //[body appendData:[[NSString stringWithString:@"Content-Disposition: form-data; name=\"userfile\"; filename=\"imgreceipt.jpg\"\r\n"] dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[@"Content-Disposition: form-data; name=\"uploaded_file\"; filename=\"imgproduct.jpg\"\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
        //[body appendData:[[NSString stringWithString:@"Content-Type: application/octet-stream\r\n\r\n"] dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[@"Content-Type: application/octet-stream\r\n\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[NSData dataWithData:imageData]];
        [body appendData:[[NSString stringWithFormat:@"\r\n--%@--\r\n",boundary] dataUsingEncoding:NSUTF8StringEncoding]];
        // setting the body of the post to the reqeust
        [request setHTTPBody:body];
        
        // now lets make the connection to the web
        /*NSData *returnData = [NSURLConnection sendSynchronousRequest:request returningResponse:nil error:nil];
        NSString *returnString = [[NSString alloc] initWithData:returnData encoding:NSUTF8StringEncoding];
        
        //NSLog(@"returnString:: %@",returnString); */
        NSURLSession *session = [NSURLSession sharedSession];
        
        [[session dataTaskWithRequest:request completionHandler:^(NSData * _Nullable data, NSURLResponse * _Nullable response, NSError * _Nullable error) {
            
            if (data) {
                
                //if response is in string format
                //NSString *returnString = [[NSString alloc]initWithData:data encoding:NSUTF8StringEncoding];
                
                //warranty_id = [jsonData objectForKey:@"desc"];
                //NSLog(@"warranty_id:: %@",warranty_id);
                
                //if response in json format then
                //id jsonObject = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:nil];
                
            }
            
        }]resume];
        
        //
        [self.view addSubview:view_add2];
        [view_add1 removeFromSuperview];
        [view_add removeFromSuperview];
        [view_done removeFromSuperview];
    } else {
        [self displayAlert:NSLocalizedString(@"MSG_NO_INTERNET", nil)];
    }
}

- (void)startupAnimationDoneIndicatorStep3Submit:(NSString *)animationID finished:(NSNumber *)finished context:(void *)context
{
    [self goStep3Submit];
    [self dissmissIndicator];
    alert =nil;
}

-(void) goStep3Done {
    if ([self isConnectionAvailable])
    {
        //Save Data
        NSData *imageData = UIImageJPEGRepresentation(imageView1.image, 90);
        // setting up the URL to post to
        NSString *urlString = [[NSString alloc] initWithFormat:@"%@warranty_submit.php?db=%@&userid=%@&udid=%@&cuid=%@&task=step2&warranty_id=%@&dealer=%@&serial_number=%@&model_number=%@&device=ios&sc=%@",appdelegate.user_apipath,appdelegate.db,appdelegate.user,appdelegate.uuid,appdelegate.customer_id, warranty_id, txt_storeName.text, txt_serialNo.text, txt_sku.text, appdelegate.cs];
        
        NSString* encodedUrl = [urlString stringByAddingPercentEscapesUsingEncoding:
                                NSUTF8StringEncoding];
        
        // setting up the request object now
        NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
        [request setURL:[NSURL URLWithString:encodedUrl]];
        [request setHTTPMethod:@"POST"];
        
        /*
         add some header info now
         we always need a boundary when we post a file
         also we need to set the content type
         
         You might want to generate a random boundary.. this is just the same
         as my output from wireshark on a valid html post
         */
        //NSString *boundary = [NSString stringWithString:@"---------------------------14737809831466499882746641449"];
        NSString *boundary = @"---------------------------14737809831466499882746641449";
        NSString *contentType = [NSString stringWithFormat:@"multipart/form-data; boundary=%@",boundary];
        [request addValue:contentType forHTTPHeaderField: @"Content-Type"];
        
        /*
         now lets create the body of the post
         */
        NSMutableData *body = [NSMutableData data];
        [body appendData:[[NSString stringWithFormat:@"\r\n--%@\r\n",boundary] dataUsingEncoding:NSUTF8StringEncoding]];
        //[body appendData:[[NSString stringWithString:@"Content-Disposition: form-data; name=\"userfile\"; filename=\"imgreceipt.jpg\"\r\n"] dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[@"Content-Disposition: form-data; name=\"uploaded_file\"; filename=\"imgproduct.jpg\"\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
        //[body appendData:[[NSString stringWithString:@"Content-Type: application/octet-stream\r\n\r\n"] dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[@"Content-Type: application/octet-stream\r\n\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[NSData dataWithData:imageData]];
        [body appendData:[[NSString stringWithFormat:@"\r\n--%@--\r\n",boundary] dataUsingEncoding:NSUTF8StringEncoding]];
        // setting the body of the post to the reqeust
        [request setHTTPBody:body];
        
        // now lets make the connection to the web
        /*NSData *returnData = [NSURLConnection sendSynchronousRequest:request returningResponse:nil error:nil];
        NSString *returnString = [[NSString alloc] initWithData:returnData encoding:NSUTF8StringEncoding];
        
        //NSLog(@"returnString:: %@",returnString); */
        NSURLSession *session = [NSURLSession sharedSession];
        
        [[session dataTaskWithRequest:request completionHandler:^(NSData * _Nullable data, NSURLResponse * _Nullable response, NSError * _Nullable error) {
            
            if (data) {
                
                //if response is in string format
                //NSString *returnString = [[NSString alloc]initWithData:data encoding:NSUTF8StringEncoding];
                
                //warranty_id = [jsonData objectForKey:@"desc"];
                //NSLog(@"warranty_id:: %@",warranty_id);
                
                //if response in json format then
                //id jsonObject = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:nil];
                
            }
            
        }]resume];
        
        warranty_id = @"0";
        //
        [self.view addSubview:view_done];
        [view_add1 removeFromSuperview];
        [view_add removeFromSuperview];
        [view_add2 removeFromSuperview];
    } else {
        [self displayAlert:NSLocalizedString(@"MSG_NO_INTERNET", nil)];
    }
}

- (void)startupAnimationDoneIndicatorStep3Done:(NSString *)animationID finished:(NSNumber *)finished context:(void *)context
{
    [self goStep3Done];
    [self dissmissIndicator];
    alert =nil;
}

-(void) goStepDone {
    if ([self isConnectionAvailable])
    {
        //Save Data
        NSData *imageData = UIImageJPEGRepresentation(imageView2.image, 90);
        // setting up the URL to post to
        NSString *urlString = [[NSString alloc] initWithFormat:@"%@warranty_submit.php?db=%@&userid=%@&udid=%@&cuid=%@&task=step3&warranty_id=%@&warranty_period=%@&comments=%@&device=ios&sc=%@",appdelegate.user_apipath,appdelegate.db,appdelegate.user,appdelegate.uuid,appdelegate.customer_id, warranty_id, txt_warrantyLength.text, txt_comments.text, appdelegate.cs];
        
        NSString* encodedUrl = [urlString stringByAddingPercentEscapesUsingEncoding:
                                NSUTF8StringEncoding];
        
        // setting up the request object now
        NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
        [request setURL:[NSURL URLWithString:encodedUrl]];
        [request setHTTPMethod:@"POST"];
        
        /*
         add some header info now
         we always need a boundary when we post a file
         also we need to set the content type
         
         You might want to generate a random boundary.. this is just the same
         as my output from wireshark on a valid html post
         */
        //NSString *boundary = [NSString stringWithString:@"---------------------------14737809831466499882746641449"];
        NSString *boundary = @"---------------------------14737809831466499882746641449";
        NSString *contentType = [NSString stringWithFormat:@"multipart/form-data; boundary=%@",boundary];
        [request addValue:contentType forHTTPHeaderField: @"Content-Type"];
        
        /*
         now lets create the body of the post
         */
        NSMutableData *body = [NSMutableData data];
        [body appendData:[[NSString stringWithFormat:@"\r\n--%@\r\n",boundary] dataUsingEncoding:NSUTF8StringEncoding]];
        //[body appendData:[[NSString stringWithString:@"Content-Disposition: form-data; name=\"userfile\"; filename=\"imgreceipt.jpg\"\r\n"] dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[@"Content-Disposition: form-data; name=\"uploaded_file\"; filename=\"imgwarranty.jpg\"\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
        //[body appendData:[[NSString stringWithString:@"Content-Type: application/octet-stream\r\n\r\n"] dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[@"Content-Type: application/octet-stream\r\n\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[NSData dataWithData:imageData]];
        [body appendData:[[NSString stringWithFormat:@"\r\n--%@--\r\n",boundary] dataUsingEncoding:NSUTF8StringEncoding]];
        // setting the body of the post to the reqeust
        [request setHTTPBody:body];
        
        // now lets make the connection to the web
        /*NSData *returnData = [NSURLConnection sendSynchronousRequest:request returningResponse:nil error:nil];
        NSString *returnString = [[NSString alloc] initWithData:returnData encoding:NSUTF8StringEncoding];
        
        //NSLog(@"returnString:: %@",returnString); */
        NSURLSession *session = [NSURLSession sharedSession];
        
        [[session dataTaskWithRequest:request completionHandler:^(NSData * _Nullable data, NSURLResponse * _Nullable response, NSError * _Nullable error) {
            
            if (data) {
                
                //if response is in string format
                //NSString *returnString = [[NSString alloc]initWithData:data encoding:NSUTF8StringEncoding];
                
                //if response in json format then
                //id jsonObject = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:nil];
                
            }
            
        }]resume];
        
        warranty_id = @"0";
        //
        [self.view addSubview:view_done];
        [view_add1 removeFromSuperview];
        [view_add removeFromSuperview];
        [view_add2 removeFromSuperview];
    } else {
        [self displayAlert:NSLocalizedString(@"MSG_NO_INTERNET", nil)];
    }
}

- (void)startupAnimationDoneIndicatorStepDone:(NSString *)animationID finished:(NSNumber *)finished context:(void *)context
{
    [self goStepDone];
    [self dissmissIndicator];
    alert =nil;
}

- (BOOL) isConnectionAvailable
{
    Reachability *networkReachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus networkStatus = [networkReachability currentReachabilityStatus];
    if (networkStatus == NotReachable) {
        //NSLog(@"There IS NO internet connection");
        return FALSE;
    } else {
        //NSLog(@"There IS internet connection");
        return TRUE;
    }
}

-(void)textFieldDidBeginEditing:(UITextField *)textField
{
    CGRect textFieldRect =
    [self.view.window convertRect:textField.bounds fromView:textField];
    CGRect viewRect =
    [self.view.window convertRect:self.view.bounds fromView:self.view];
    
    CGFloat midline = textFieldRect.origin.y + 0.5 * textFieldRect.size.height;
    CGFloat numerator =
    midline - viewRect.origin.y
    - MINIMUM_SCROLL_FRACTION * viewRect.size.height;
    CGFloat denominator =
    (MAXIMUM_SCROLL_FRACTION - MINIMUM_SCROLL_FRACTION)
    * viewRect.size.height;
    CGFloat heightFraction = numerator / denominator;
    if (heightFraction < 0.0)
    {
        heightFraction = 0.0;
    }
    else if (heightFraction > 1.0)
    {
        heightFraction = 1.0;
    }
    
    
    animatedDistance = floor(PORTRAIT_KEYBOARD_HEIGHT * heightFraction);
    
    CGRect viewFrame = self.view.frame;
    viewFrame.origin.y -= animatedDistance;
    
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationBeginsFromCurrentState:YES];
    [UIView setAnimationDuration:KEYBOARD_ANIMATION_DURATION];
    
    [self.view setFrame:viewFrame];
    
    [UIView commitAnimations];
}

- (void)textFieldDidEndEditing:(UITextField *)textField

{
    CGRect viewFrame = self.view.frame;
    viewFrame.origin.y += animatedDistance;
    
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationBeginsFromCurrentState:YES];
    [UIView setAnimationDuration:KEYBOARD_ANIMATION_DURATION];
    
    [self.view setFrame:viewFrame];
    
    [UIView commitAnimations];
    
}

-(void) viewDidDisappear:(BOOL)animated {
    
    [super viewDidDisappear:animated];
    
    appdelegate.homeclicked = NO;
    [modalController dismissViewControllerAnimated:YES completion:nil];
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
