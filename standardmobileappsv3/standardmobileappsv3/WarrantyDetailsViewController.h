//
//  WarrantyDetailsViewController.h
//  standardmobileappsv3
//
//  Created by M3Online on 6/24/14.
//  Copyright (c) 2014 M3Online. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <SDWebImage/UIButton+WebCache.h>
#import <SDWebImage/UIImageView+WebCache.h>

@interface WarrantyDetailsViewController : UIViewController <UITableViewDelegate, UITableViewDataSource, UIViewControllerTransitioningDelegate> {
    NSString *contId;
    NSString *conttitle;
    
    //NSMutableData* receivedData;
    NSString *currentRequest;
    NSMutableArray *listOfContent;
    NSDictionary *dicContent;
    UIScrollView *scrollview;
    
    NSMutableArray *MenuItems;
    NSMutableArray *MenuItems1;
    
    IBOutlet UITableView *tableView1;
    IBOutlet UITableView *tableView2;
}

@property (nonatomic, strong) NSString *contId;
@property (nonatomic, strong) NSString *conttitle;

//@property (nonatomic, strong) NSMutableData *receivedData;
@property (nonatomic, strong) NSString *currentRequest;
@property (nonatomic, strong) NSMutableArray *listOfContent;
@property (nonatomic, strong) UIScrollView *scrollview;

@property (nonatomic, strong) UITableView *tableView1;
@property (nonatomic, strong) UITableView *tableView2;

@end
