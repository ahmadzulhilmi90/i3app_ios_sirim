//
//  WarrantyDetailsViewController.m
//  standardmobileappsv3
//
//  Created by M3Online on 6/24/14.
//  Copyright (c) 2014 M3Online. All rights reserved.
//

#import "WarrantyDetailsViewController.h"
#import "AppDelegate.h"
#import "NSString+HTML.h"
#import "ItemCell.h"
#import "RootViewController.h"
#import "ViewController.h"
#import "InitialSlidingViewController.h"
#import "BTSlideInteractor.h"

@interface WarrantyDetailsViewController ()
@property (strong, nonatomic) BTSlideInteractor *interactor;
@end

@implementation WarrantyDetailsViewController

@synthesize contId, scrollview, tableView1, tableView2;
//@synthesize receivedData, currentRequest, listOfContent, conttitle;

InitialSlidingViewController *modalController;

AppDelegate *appdelegate;
__strong NSDate *expDate;

- (UIColor *) colorWithHexString:(NSString *)hexString {
    NSString *colorString = [[hexString stringByReplacingOccurrencesOfString: @"#" withString: @""] uppercaseString];
    CGFloat alpha, red, blue, green;
    alpha = 1.0f;
    red   = [self colorComponentFrom: colorString start: 0 length: 2];
    green = [self colorComponentFrom: colorString start: 2 length: 2];
    blue  = [self colorComponentFrom: colorString start: 4 length: 2];
    return [UIColor colorWithRed: red green: green blue: blue alpha: alpha];
}

- (CGFloat) colorComponentFrom:(NSString *)string start:(NSUInteger)start length:(NSUInteger)length {
    NSString *substring = [string substringWithRange: NSMakeRange(start, length)];
    NSString *fullHex = length == 2 ? substring : [NSString stringWithFormat: @"%@%@", substring, substring];
    unsigned hexComponent;
    [[NSScanner scannerWithString: fullHex] scanHexInt: &hexComponent];
    return hexComponent / 255.0;
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    self.view.backgroundColor = [UIColor whiteColor];
    appdelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    self.title = conttitle;
    appdelegate.homeclicked = NO;
    sqlite3 *database;
    
    if(sqlite3_open([appdelegate.databasePath UTF8String], &database) == SQLITE_OK) {
        NSString *theme_object = [[NSString alloc] initWithFormat:@"SELECT * from warranty WHERE id = %@", contId];
        //NSLog(@"theme_object warranty:: %@", theme_object);
        
        NSString *cid;
        NSString *cwarrantyperiod;
        NSString *cproname;
        NSString *cstorename;
        NSString *cpurchasedate;
        NSString *cexpdate;
        NSString *cserialnum;
        NSString *cmodel;
        NSString *cpropic;
        NSString *creceiptpic;
        NSString *cwcardpic;
        NSString *ccomments;
        NSString *cstatus;
        
        const char *sqlStatement3 = [theme_object UTF8String];
        sqlite3_stmt *compiledStatement3;
        if(sqlite3_prepare_v2(database, sqlStatement3, -1, &compiledStatement3, NULL) == SQLITE_OK) {
            listOfContent = [[NSMutableArray alloc] init];
            while(sqlite3_step(compiledStatement3) == SQLITE_ROW) {
                
                cid = [NSString stringWithUTF8String:(char *)sqlite3_column_text(compiledStatement3, 0)];
                cwarrantyperiod = [NSString stringWithUTF8String:(char *)sqlite3_column_text(compiledStatement3, 9)];
                cproname = [NSString stringWithUTF8String:(char *)sqlite3_column_text(compiledStatement3, 4)];
                cstorename = [NSString stringWithUTF8String:(char *)sqlite3_column_text(compiledStatement3, 5)];
                cpurchasedate = [NSString stringWithUTF8String:(char *)sqlite3_column_text(compiledStatement3, 8)];
                cserialnum = [NSString stringWithUTF8String:(char *)sqlite3_column_text(compiledStatement3, 6)];
                cmodel = [NSString stringWithUTF8String:(char *)sqlite3_column_text(compiledStatement3, 7)];
                cpropic = [NSString stringWithUTF8String:(char *)sqlite3_column_text(compiledStatement3, 11)];
                creceiptpic = [NSString stringWithUTF8String:(char *)sqlite3_column_text(compiledStatement3, 12)];
                cwcardpic = [NSString stringWithUTF8String:(char *)sqlite3_column_text(compiledStatement3, 13)];
                ccomments = [NSString stringWithUTF8String:(char *)sqlite3_column_text(compiledStatement3, 14)];
                cstatus = [NSString stringWithUTF8String:(char *)sqlite3_column_text(compiledStatement3, 17)];
                
                cproname = [cproname stringByDecodingHTMLEntities];
                cstorename = [cstorename stringByDecodingHTMLEntities];
                cpurchasedate = [cpurchasedate stringByDecodingHTMLEntities];
                cexpdate = [cexpdate stringByDecodingHTMLEntities];
                cserialnum = [cserialnum stringByDecodingHTMLEntities];
                cmodel = [cmodel stringByDecodingHTMLEntities];
                cpropic = [cpropic stringByDecodingHTMLEntities];
                creceiptpic = [creceiptpic stringByDecodingHTMLEntities];
                cwcardpic = [cwcardpic stringByDecodingHTMLEntities];
                ccomments = [ccomments stringByDecodingHTMLEntities];
                cproname = [cproname stringByReplacingOccurrencesOfString:@"''" withString:@"'"];
                cstorename = [cstorename stringByReplacingOccurrencesOfString:@"''" withString:@"'"];
                cpurchasedate = [cpurchasedate stringByReplacingOccurrencesOfString:@"''" withString:@"'"];
                cexpdate = [cexpdate stringByReplacingOccurrencesOfString:@"''" withString:@"'"];
                cserialnum = [cserialnum stringByReplacingOccurrencesOfString:@"''" withString:@"'"];
                cmodel = [cmodel stringByReplacingOccurrencesOfString:@"''" withString:@"'"];
                cpropic = [cpropic stringByReplacingOccurrencesOfString:@"''" withString:@"'"];
                creceiptpic = [creceiptpic stringByReplacingOccurrencesOfString:@"''" withString:@"'"];
                cwcardpic = [cwcardpic stringByReplacingOccurrencesOfString:@"''" withString:@"'"];
                ccomments = [ccomments stringByReplacingOccurrencesOfString:@"''" withString:@"'"];
                
                NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
                // this is imporant - we set our input date format to match our input string
                // if format doesn't match you'll get nil from your string, so be careful
                [dateFormatter setDateFormat:@"yyyy-MM-dd"];
                NSDate *dateFromString = [[NSDate alloc] init];
                // voila!
                dateFromString = [dateFormatter dateFromString:cpurchasedate];
                
                NSString *strDate;
                if ([cwarrantyperiod intValue]>0) {
                    NSDateComponents *dateComponents = [[NSDateComponents alloc] init];
                    [dateComponents setMonth:[cwarrantyperiod intValue]];
                    NSCalendar *calendar = [NSCalendar currentCalendar];
                    expDate = [calendar dateByAddingComponents:dateComponents toDate:dateFromString options:0];
                    
                    NSDateFormatter *dateFormatter1 = [[NSDateFormatter alloc] init];
                    [dateFormatter1 setDateFormat:@"yyyy-MM-dd"];
                    strDate = [dateFormatter1 stringFromDate:expDate];
                    //NSLog(@"%@", strDate);
                } else {
                    strDate = @"";
                }
                
                dicContent = [NSDictionary dictionaryWithObjectsAndKeys:cid,@"id",cwarrantyperiod,@"warranty_period",cproname,@"product_name",cstorename,@"store_name",cpurchasedate,@"purchase_date",strDate,@"expiry_date",cserialnum,@"serial_number",cmodel,@"model",cpropic,@"product_picture",creceiptpic,@"receipt_picture",cstatus,@"status",cwcardpic,@"warrantycard_picture",ccomments,@"comments",nil];
                [listOfContent addObject:dicContent];
                
                //NSLog(@"listOfContent::%@", listOfContent);
                
            }
        }
        // Release the compiled statement from memory
        sqlite3_finalize(compiledStatement3);
        
        scrollview = [[UIScrollView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height)];
        scrollview.backgroundColor = [UIColor whiteColor];
        scrollview.bounces = NO;
        [self.view addSubview:scrollview];
        
        int height=0;
        int noOfline;
        if([listOfContent count]>0){
            
            NSDictionary *dictionary = [listOfContent objectAtIndex:0];
            
            NSString *conproduct_name = [dictionary objectForKey:@"product_name"];
            NSString *constore_name = [dictionary objectForKey:@"store_name"];
            //NSString *conwarrantyperiod = [dictionary objectForKey:@"warranty_period"];
            NSString *conpurchase_date = [dictionary objectForKey:@"purchase_date"];
            NSString *conexpiry_date = [dictionary objectForKey:@"expiry_date"];
            NSString *conserial_number = [dictionary objectForKey:@"serial_number"];
            NSString *conmodel = [dictionary objectForKey:@"model"];
            NSString *conproduct_picture = [dictionary objectForKey:@"product_picture"];
            NSString *conreceipt_picture = [dictionary objectForKey:@"receipt_picture"];
            //NSString *fbtitle = [dictionary objectForKey:@"fb_title"];
            NSString *conwarrantycard_picture = [dictionary objectForKey:@"warrantycard_picture"];
            NSString *concomments = [dictionary objectForKey:@"comments"];
            
            MenuItems = [[NSMutableArray alloc] init];
            [MenuItems addObject:@"product_name"];
            [MenuItems addObject:@"store_name"];
            [MenuItems addObject:@"purchase_date"];
            [MenuItems addObject:@"expiry_date"];
            [MenuItems addObject:@"serial_number"];
            [MenuItems addObject:@"model"];
            [MenuItems addObject:@"comments"];
            //[MenuItems addObject:@"Twitter"];
            //NSLog(@"MenuItems::%@", MenuItems);
            
            int j;
            j=0;
            for(j=0; j<[MenuItems count]; j++) {
                //NSLog(@"[listOfMenuItems objectAtIndex:i] :: %@", [listOfMenuItems objectAtIndex:i]);
                if (([[MenuItems objectAtIndex:j] isEqualToString:@"product_name"] && [conproduct_name length]==0) ||
                     ([[MenuItems objectAtIndex:j] isEqualToString:@"store_name"] && [constore_name length]==0) ||
                    ([[MenuItems objectAtIndex:j] isEqualToString:@"purchase_date"] && [conpurchase_date length]==0) ||
                    ([[MenuItems objectAtIndex:j] isEqualToString:@"expiry_date"] && [conexpiry_date length]==0) ||
                    ([[MenuItems objectAtIndex:j] isEqualToString:@"serial_number"] && [conserial_number length]==0) ||
                    ([[MenuItems objectAtIndex:j] isEqualToString:@"model"] && [conmodel length]==0) /*||
                    ([[MenuItems objectAtIndex:j] isEqualToString:@"product_picture"] && [conproduct_picture length]==0) ||
                    ([[MenuItems objectAtIndex:j] isEqualToString:@"receipt_picture"] && [conreceipt_picture length]==0) ||
                    ([[MenuItems objectAtIndex:j] isEqualToString:@"warrantycard_picture"] && [conwarrantycard_picture length]==0)*/ || ([[MenuItems objectAtIndex:j] isEqualToString:@"comments"] && [concomments length]==0)) {
                    [MenuItems removeObjectAtIndex:j];
                    j--;
                    
                }
            }
            
            MenuItems1 = [[NSMutableArray alloc] init];
            [MenuItems1 addObject:@"product_picture"];
            [MenuItems1 addObject:@"receipt_picture"];
            [MenuItems1 addObject:@"warrantycard_picture"];
            //[MenuItems addObject:@"Twitter"];
            //NSLog(@"MenuItems1::%@", MenuItems1);
            
            int j1;
            j1=0;
            for(j1=0; j1<[MenuItems1 count]; j1++) {
                //NSLog(@"[listOfMenuItems objectAtIndex:i] :: %@", [listOfMenuItems objectAtIndex:i]);
                if (([[MenuItems1 objectAtIndex:j1] isEqualToString:@"product_picture"] && [conproduct_picture length]==0) ||
                    ([[MenuItems1 objectAtIndex:j1] isEqualToString:@"receipt_picture"] && [conreceipt_picture length]==0) ||
                    ([[MenuItems1 objectAtIndex:j1] isEqualToString:@"warrantycard_picture"] && [conwarrantycard_picture length]==0)) {
                    [MenuItems1 removeObjectAtIndex:j1];
                    j1--;
                    
                }
            }
        
            if ([conproduct_name length]>0) {
                
                UIImageView *ImageBarView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 260+60, self.view.frame.size.width, 60)];
                //NSString *myImage = [NSString stringWithFormat:@"%@_bar_maincategory.png", appdelegate.themecolor];
                //ImageBarView.image = [UIImage imageNamed:myImage];
                ImageBarView.alpha = 1.0;
                
                //AppDelegate *appdelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
                UIView *overlay;
                overlay = [[UIView alloc] initWithFrame:CGRectMake(0, 0, ImageBarView.frame.size.width, ImageBarView.frame.size.height)];
                overlay.backgroundColor = [self colorWithHexString:appdelegate.themeColorCode];
                overlay.alpha = 0.6f;
                //overlay.opaque = YES;
                
                [ImageBarView addSubview:overlay];
                
                UILabel *lblTitle = [[UILabel alloc] initWithFrame:CGRectMake(5, 5, self.view.frame.size.width, 60)];
                lblTitle.numberOfLines = 0;
                lblTitle.lineBreakMode = NSLineBreakByWordWrapping;
                UIFont *yourFont = [UIFont fontWithName:@"Helvetica-Bold" size:22];
                lblTitle.font = yourFont;
                lblTitle.text = conproduct_name;
                lblTitle.backgroundColor = [UIColor clearColor];
                lblTitle.textColor = [UIColor whiteColor];
                //lblTitle.text = @"This is some text in a UILabel which is long enough to wrap around the lines in said UILabel. This is a test, this is only a test.";
                [lblTitle sizeToFit];
                lblTitle.shadowColor = [UIColor blackColor];
                lblTitle.shadowOffset = CGSizeMake(0, 1);
                [ImageBarView addSubview:lblTitle];
                CGSize constrainedSize = CGSizeMake(lblTitle.frame.size.width  , 9999);
                
                NSDictionary *attributesDictionary = [NSDictionary dictionaryWithObjectsAndKeys:
                                                      yourFont, NSFontAttributeName,
                                                      nil];
                
                NSMutableAttributedString *string = [[NSMutableAttributedString alloc] initWithString:lblTitle.text attributes:attributesDictionary];
                
                CGRect requiredHeight = [string boundingRectWithSize:constrainedSize options:NSStringDrawingUsesLineFragmentOrigin context:nil];
                
                if (requiredHeight.size.width > lblTitle.frame.size.width) {
                    requiredHeight = CGRectMake(0,0, lblTitle.frame.size.width, requiredHeight.size.height);
                }
                CGRect newFrame = lblTitle.frame;
                newFrame.size.height = requiredHeight.size.height;
                lblTitle.frame = newFrame;
                
                noOfline = newFrame.size.height;
                
                ImageBarView.frame = CGRectMake(0, height, self.view.frame.size.width, noOfline+10);
                overlay.frame = CGRectMake(0, 0, ImageBarView.frame.size.width, ImageBarView.frame.size.height);
                [scrollview addSubview:ImageBarView];
                
                lblTitle =nil;
                ImageBarView =nil;
                
                height = height+noOfline;
            }
            
            tableView1 = [[UITableView alloc] initWithFrame:CGRectMake(0, height, self.view.frame.size.width, ([MenuItems count]*44)+50) style:UITableViewStyleGrouped];
            
            // must set delegate & dataSource, otherwise the the table will be empty and not responsive
            tableView1.delegate = self;
            tableView1.dataSource = self;
            
            tableView1.backgroundView = nil;
            tableView1.backgroundColor = [UIColor clearColor];
            
            // add to canvas
            [scrollview addSubview:tableView1];
            
            tableView2 = [[UITableView alloc] initWithFrame:CGRectMake(0, height+([MenuItems count]*44)+10, self.view.frame.size.width, ([MenuItems1 count]*(200+60+10))+100) style:UITableViewStyleGrouped];
            
            // must set delegate & dataSource, otherwise the the table will be empty and not responsive
            tableView2.delegate = self;
            tableView2.dataSource = self;
            
            tableView2.backgroundView = nil;
            tableView2.backgroundColor = [UIColor clearColor];
            [scrollview addSubview:tableView2];
            
            scrollview.contentSize = CGSizeMake(scrollview.frame.size.width, height +([MenuItems count]*44+50)+([MenuItems1 count]*(200+60+10))+200);
            
        }
        
        scrollview =nil;
    }
    sqlite3_close(database);
    
    self.navigationItem.backBarButtonItem = [[UIBarButtonItem alloc]initWithTitle:@"" style:UIBarButtonItemStylePlain target:nil action:nil];
    
    CGRect rect = CGRectMake(0,0,20,20);
    UIGraphicsBeginImageContext( rect.size );
    [[UIImage imageNamed:@"home.png"] drawInRect:rect];
    UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    UIBarButtonItem *myHomeButton = [[UIBarButtonItem alloc] initWithImage:image style:UIBarButtonItemStyleDone target:self action:@selector(myHomeAction:)];
    
    
    NSArray *myButtonArray = [[NSArray alloc] initWithObjects:myHomeButton, nil];
    
    self.navigationItem.rightBarButtonItems = myButtonArray;

}

//Slide Menu
- (void)willAnimateRotationToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation duration:(NSTimeInterval)duration
{
    //NSLog(@"view frame=%@ bounds=%@", NSStringFromCGRect(self.view.frame), NSStringFromCGRect(self.view.bounds));
}


- (void)showModalButtonWasTouched
{
    self.interactor = [[BTSlideInteractor alloc] init];
    self.interactor.presenting = YES;
    
    //BTModalViewController *modalController = [self.storyboard instantiateViewControllerWithIdentifier:@"ModalViewController"];
    modalController = [[InitialSlidingViewController alloc] init];
    modalController.modalPresentationStyle = UIModalPresentationCustom;
    modalController.transitioningDelegate = self.interactor;
    [self.navigationController presentViewController:modalController animated:YES completion:nil];
    //[self.navigationController popToViewController:modalController animated:NO];
    //[self.navigationController pushViewController:modalController animated:YES];
}
// End Slide Menu

-(void)myHomeAction:(UIBarButtonItem *)sender{
    
    appdelegate.nextPage = @"";
    
    if ([appdelegate.listOfVerMenu count]>0) {
        if (appdelegate.homeclicked==NO) {
            appdelegate.homeclicked = YES;
            [self showModalButtonWasTouched];
        } else {
            appdelegate.homeclicked = NO;
            [modalController dismissViewControllerAnimated:YES completion:nil];
        }
    } else {
        //AppDelegate *appdelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
        //NSLog(@"View Controllers : %@",[appdelegate.navController viewControllers]);
        NSMutableArray *navigationArray = [[NSMutableArray alloc] initWithArray: appdelegate.navController.viewControllers];
        
        for (UIViewController* viewController in appdelegate.navController.viewControllers) {
            
            //This if condition checks whether the viewController's class is MyGroupViewController
            // if true that means its the MyGroupViewController (which has been pushed at some point)
            if ([viewController isKindOfClass:[RootViewController class]] ) {
                
                // Here viewController is a reference of UIViewController base class of MyGroupViewController
                // but viewController holds MyGroupViewController  object so we can type cast it here
                //ThemeViewController *themeViewController = (ThemeViewController*)viewController;
                //[self.navigationController popToViewController:themeViewController animated:YES];
                [navigationArray removeObjectAtIndex: 0];
            }
        }
        appdelegate.navController.viewControllers = navigationArray;
        for (UIViewController* viewController in appdelegate.navController.viewControllers) {
            
            //This if condition checks whether the viewController's class is MyGroupViewController
            // if true that means its the MyGroupViewController (which has been pushed at some point)
            if ([viewController isKindOfClass:[ViewController class]] ) {
                
                // Here viewController is a reference of UIViewController base class of MyGroupViewController
                // but viewController holds MyGroupViewController  object so we can type cast it here
                ViewController *themeViewController = (ViewController*)viewController;
                [appdelegate.navController popToViewController:themeViewController animated:NO];
                //[self.navigationController popToViewController:viewController animated:YES];
            }
        }
        //NSLog(@"View Controllers111 : %@",appdelegate.navController.viewControllers);
    }
    
}

#pragma mark - UITableViewDataSource
// number of section(s), now I assume there is only 1 section
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

// number of row in the section, I assume there is only 1 row
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    //return 8;
    if (tableView == tableView1)
        return [MenuItems count];
    if (tableView == tableView2)
        return [MenuItems1 count];
    return 0;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (tableView == tableView2) {
        
        return 200+60+10;
    }
    
    if (tableView == tableView1) {
        NSDictionary *data = [listOfContent objectAtIndex:0];
        NSString *conproduct_name = [data objectForKey:@"product_name"];
        NSString *constore_name = [data objectForKey:@"store_name"];
        NSString *conserial_number = [data objectForKey:@"serial_number"];
        NSString *conmodel = [data objectForKey:@"model"];
        NSString *concomments = [data objectForKey:@"comments"];
        
        CGSize constraint = CGSizeMake(180, 20000.0f);
        CGFloat height = 0;
        
        if ([[MenuItems objectAtIndex:indexPath.row] isEqualToString:@"product_name"]) {
            CGSize size = [conproduct_name boundingRectWithSize:constraint options: NSStringDrawingUsesLineFragmentOrigin attributes: @{ NSFontAttributeName: [UIFont fontWithName:@"Arial" size:12.0f] } context: nil].size;
            
            height = height + MAX(size.height, 25.0f)+10;
        } else if ([[MenuItems objectAtIndex:indexPath.row] isEqualToString:@"store_name"]) {
            CGSize sizestore_name = [constore_name boundingRectWithSize:constraint options: NSStringDrawingUsesLineFragmentOrigin attributes: @{ NSFontAttributeName: [UIFont fontWithName:@"Arial" size:12.0f] } context: nil].size;
            
            height = height + MAX(sizestore_name.height, 25.0f)+10;
        } else if ([[MenuItems objectAtIndex:indexPath.row] isEqualToString:@"serial_number"]) {
            CGSize sizeserial_number = [conserial_number boundingRectWithSize:constraint options: NSStringDrawingUsesLineFragmentOrigin attributes: @{ NSFontAttributeName: [UIFont fontWithName:@"Arial" size:12.0f] } context: nil].size;
            
            height = height + MAX(sizeserial_number.height, 25.0f)+10;
        } else if ([[MenuItems objectAtIndex:indexPath.row] isEqualToString:@"model"]) {
            CGSize sizemodel = [conmodel boundingRectWithSize:constraint options: NSStringDrawingUsesLineFragmentOrigin attributes: @{ NSFontAttributeName: [UIFont fontWithName:@"Arial" size:12.0f] } context: nil].size;
            
            height = height + MAX(sizemodel.height, 25.0f)+10;
        } else if ([[MenuItems objectAtIndex:indexPath.row] isEqualToString:@"comments"]) {
            CGSize sizecomments = [concomments boundingRectWithSize:constraint options: NSStringDrawingUsesLineFragmentOrigin attributes: @{ NSFontAttributeName: [UIFont fontWithName:@"Arial" size:12.0f] } context: nil].size;
            
            height = height + MAX(sizecomments.height, 25.0f)+10;
        } else {
            height = height +44;
        }
        
        return height;
    }
    
	return 44;
}

// the cell will be returned to the tableView
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    //AppDelegate *appdelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    if ([listOfContent count] > 0) {
        NSDictionary *dictionary = [listOfContent objectAtIndex:0];
        
        NSString *conproduct_name = [dictionary objectForKey:@"product_name"];
        NSString *constore_name = [dictionary objectForKey:@"store_name"];
        NSString *conwarrantyperiod = [dictionary objectForKey:@"warranty_period"];
        NSString *conpurchase_date = [dictionary objectForKey:@"purchase_date"];
        NSString *conexpiry_date = [dictionary objectForKey:@"expiry_date"];
        NSString *conserial_number = [dictionary objectForKey:@"serial_number"];
        NSString *conmodel = [dictionary objectForKey:@"model"];
        NSString *conproduct_picture = [dictionary objectForKey:@"product_picture"];
        NSString *conreceipt_picture = [dictionary objectForKey:@"receipt_picture"];
        //NSString *fbtitle = [dictionary objectForKey:@"fb_title"];
        NSString *conwarrantycard_picture = [dictionary objectForKey:@"warrantycard_picture"];
        NSString *concomments = [dictionary objectForKey:@"comments"];
        NSString *constatus = [dictionary objectForKey:@"status"];
    
        if (tableView == tableView1) {
            //NSLog(@"tableView1:: %@", tableView1);
            static NSString *cellIdentifier = @"HistoryCell";
            
            // Similar to UITableViewCell, but
            ItemCell *cell = (ItemCell *)[tableView dequeueReusableCellWithIdentifier:cellIdentifier];
            
            if (cell == nil) {
                cell = [[ItemCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier];
                //cell.AccessoryType = UITableViewCellAccessoryDisclosureIndicator;
                cell.selectionStyle = UITableViewCellSelectionStyleNone;
            }
            
            //case 3: {
            if ([[MenuItems objectAtIndex:indexPath.row] isEqualToString:@"product_name"]) {
                
                cell.labelname.text = NSLocalizedString(@"productname", nil);
                CGSize constraint = CGSizeMake(180, 20000.0f);
                
                CGSize size = [conproduct_name boundingRectWithSize:constraint options: NSStringDrawingUsesLineFragmentOrigin attributes: @{ NSFontAttributeName: [UIFont fontWithName:@"Arial" size:12.0f] } context: nil].size;
                [cell.labelvalue setFrame:CGRectMake(120, 5, 180, MAX(size.height, 25.0f))];
                cell.labelvalue.text = [NSString stringWithFormat:@"%@", conproduct_name];
                cell.labelvalue.textColor = [UIColor blackColor];
                //break;
                
            }
            //case 4: {
            if ([[MenuItems objectAtIndex:indexPath.row] isEqualToString:@"store_name"]) {
                
                cell.labelname.text = NSLocalizedString(@"storename", nil);
                CGSize constraint = CGSizeMake(180, 20000.0f);
                
                CGSize size = [constore_name boundingRectWithSize:constraint options: NSStringDrawingUsesLineFragmentOrigin attributes: @{ NSFontAttributeName: [UIFont fontWithName:@"Arial" size:12.0f] } context: nil].size;
                [cell.labelvalue setFrame:CGRectMake(120, 5, 180, MAX(size.height, 25.0f))];
                cell.labelvalue.text = constore_name;
                cell.labelvalue.textColor = [UIColor blackColor];
                //break;
                
            }
            //case 5: {
            if ([[MenuItems objectAtIndex:indexPath.row] isEqualToString:@"purchase_date"]) {
                
                cell.labelname.text = NSLocalizedString(@"purchasedate_dmy", nil);
                cell.labelvalue.text = conpurchase_date;
                cell.labelvalue.textColor = [UIColor blackColor];
                //break;
                
            }
            
            //case 6: {
            if ([[MenuItems objectAtIndex:indexPath.row] isEqualToString:@"expiry_date"]) {
                
                cell.labelname.text = NSLocalizedString(@"expirydate", nil);
                cell.labelvalue.text = conexpiry_date;
                
                NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
                // this is imporant - we set our input date format to match our input string
                // if format doesn't match you'll get nil from your string, so be careful
                [dateFormatter setDateFormat:@"yyyy-MM-dd"];
                NSDate *dateFromString1 = [[NSDate alloc] init];
                // voila!
                dateFromString1 = [dateFormatter dateFromString:conpurchase_date];
                
                NSDateComponents *dateComponents = [[NSDateComponents alloc] init];
                [dateComponents setMonth:[conwarrantyperiod intValue]-1];
                NSCalendar *calendar = [NSCalendar currentCalendar];
                NSDate *expringDate = [calendar dateByAddingComponents:dateComponents toDate:dateFromString1 options:0];
                
                /*NSDateFormatter *dateFormatter1 = [[NSDateFormatter alloc] init];
                [dateFormatter1 setDateFormat:@"yyyy-MM-dd"];
                NSString *strDate = [dateFormatter1 stringFromDate:expringDate];
                NSLog(@"expringDate : %@", strDate); */
                if (![conpurchase_date isEqualToString:@""] && [conwarrantyperiod intValue]>0) {
                    if ([[NSDate date] compare:expDate] == NSOrderedDescending) {
                        cell.labelvalue.textColor = [UIColor redColor];
                    } else if ([[NSDate date] compare:expringDate] == NSOrderedDescending && [[NSDate date] compare:expDate] == NSOrderedAscending) {
                        cell.labelvalue.textColor = [UIColor orangeColor];
                    } else if ([constatus isEqualToString:@"2"]) {
                        cell.labelvalue.textColor = [UIColor greenColor];
                    } else {
                        cell.labelvalue.textColor = [UIColor blackColor];
                    }
                }
                //break;
                
            }
            
            if ([[MenuItems objectAtIndex:indexPath.row] isEqualToString:@"serial_number"]) {
                
                cell.labelname.text = NSLocalizedString(@"serialno", nil);
                CGSize constraint = CGSizeMake(180, 20000.0f);
                
                CGSize size = [conserial_number boundingRectWithSize:constraint options: NSStringDrawingUsesLineFragmentOrigin attributes: @{ NSFontAttributeName: [UIFont fontWithName:@"Arial" size:12.0f] } context: nil].size;
                [cell.labelvalue setFrame:CGRectMake(120, 5, 180, MAX(size.height, 25.0f))];
                cell.labelvalue.text = conserial_number;
                cell.labelvalue.textColor = [UIColor blackColor];
                //break;
                
            }
            
            if ([[MenuItems objectAtIndex:indexPath.row] isEqualToString:@"model"]) {
                
                cell.labelname.text = NSLocalizedString(@"modelsku", nil);
                CGSize constraint = CGSizeMake(180, 20000.0f);
                
                CGSize size = [conmodel boundingRectWithSize:constraint options: NSStringDrawingUsesLineFragmentOrigin attributes: @{ NSFontAttributeName: [UIFont fontWithName:@"Arial" size:12.0f] } context: nil].size;
                [cell.labelvalue setFrame:CGRectMake(120, 5, 180, MAX(size.height, 25.0f))];
                cell.labelvalue.text = conmodel;
                cell.labelvalue.textColor = [UIColor blackColor];
                //break;
                
            }
            
            if ([[MenuItems objectAtIndex:indexPath.row] isEqualToString:@"comments"]) {
                
                cell.labelname.text = NSLocalizedString(@"comments", nil);
                CGSize constraint = CGSizeMake(180, 20000.0f);
                
                CGSize size = [concomments boundingRectWithSize:constraint options: NSStringDrawingUsesLineFragmentOrigin attributes: @{ NSFontAttributeName: [UIFont fontWithName:@"Arial" size:12.0f] } context: nil].size;
                [cell.labelvalue setFrame:CGRectMake(120, 5, 180, MAX(size.height, 25.0f))];
                cell.labelvalue.text = concomments;
                cell.labelvalue.textColor = [UIColor blackColor];
                //break;
                
            }
            
            return cell;
            
        }
        
        //if (tableView == tableView2) {
           // NSLog(@"tableView2:: %@", tableView2);
            static NSString *CellIdentifier = @"CellIdentifier";
            UITableViewCell *cell = [tableView
                                     dequeueReusableCellWithIdentifier:CellIdentifier];
            
            UIImageView *imgViewMenu;
            const NSInteger LEFT_TAG = 1000;
            UILabel *menutext;
            const NSInteger RIGHT_TAG = 1001;
            
            if (cell == nil)
            {
                
                cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
                //cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
                cell.selectionStyle = UITableViewCellSelectionStyleNone;
                
                //if ([[dictionary objectForKey:@"object_type"] isEqualToString:@"img"]) {
                imgViewMenu = [[UIImageView alloc] initWithFrame:CGRectMake(5, 60, 200, 200)];
                
                imgViewMenu.tag = LEFT_TAG;
                [cell.contentView addSubview:imgViewMenu];
                
                //} else if ([[dictionary objectForKey:@"object_type"] isEqualToString:@"text"]) {
                
                menutext = [[UILabel alloc] initWithFrame: CGRectMake(5, 0, self.view.frame.size.width, 60)];
                
                menutext.tag = RIGHT_TAG;
                [menutext setFont:[UIFont boldSystemFontOfSize:16]];
                menutext.textColor = [UIColor grayColor];
                menutext.textAlignment = NSTextAlignmentLeft;
                menutext.backgroundColor = [UIColor clearColor];
                menutext.numberOfLines = 0;
                [cell.contentView addSubview:menutext];
                
            }
            else
            {
                //if ([[dictionary objectForKey:@"object_type"] isEqualToString:@"img"]) {
                imgViewMenu = (UIImageView *)[cell viewWithTag:LEFT_TAG];
                //} else if ([[dictionary objectForKey:@"object_type"] isEqualToString:@"text"]) {
                menutext = (UILabel *)[cell viewWithTag:RIGHT_TAG];
            }
            
            //NSLog(@"MenuItems111::%@", MenuItems1);
            if ([[MenuItems1 objectAtIndex:indexPath.row] isEqualToString:@"product_picture"]) {
                
                __block UIActivityIndicatorView *activityIndicator;
                __weak UIImageView *weakImageView = imgViewMenu;
                /*[weakImageView setImageWithURL:[NSURL URLWithString:conproduct_picture] placeholderImage:[UIImage imageNamed:@"No-Image-icon.png"] options:SDWebImageProgressiveDownload progress:^(NSInteger receivedSize, NSInteger expectedSize)
                 {
                     if (!activityIndicator)
                     {
                         [weakImageView addSubview:activityIndicator = [UIActivityIndicatorView.alloc initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray]];
                         activityIndicator.center = weakImageView.center;
                         [activityIndicator startAnimating];
                     }
                 }
                                     completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType)
                 {
                     [activityIndicator removeFromSuperview];
                     activityIndicator = nil;
                 }]; */
                dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0), ^ {
                    [weakImageView sd_setImageWithURL:[NSURL URLWithString:conproduct_picture]
                                     placeholderImage:[UIImage imageNamed:@"No-Image-icon.png"]
                                              options:SDWebImageProgressiveDownload
                                             progress:^(NSInteger receivedSize, NSInteger expectedSize, NSURL *targetURL) {
                                                 dispatch_async(dispatch_get_main_queue(), ^ {
                                                     if (!activityIndicator) {
                                                         
                                                         [weakImageView addSubview:activityIndicator = [UIActivityIndicatorView.alloc initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray]];
                                                         activityIndicator.center = weakImageView.center;
                                                         [activityIndicator startAnimating];
                                                     }
                                                 });
                                             }
                                            completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
                                                dispatch_async(dispatch_get_main_queue(), ^ {
                                                    //[MBProgressHUD hideHUDForView:self.view animated:YES];
                                                    [activityIndicator removeFromSuperview];
                                                    activityIndicator = nil;
                                                });
                                            }];
                });
                
                imgViewMenu.contentMode  = UIViewContentModeScaleAspectFill;
                [imgViewMenu setClipsToBounds:YES];
                
                menutext.text = [NSString stringWithFormat:NSLocalizedString(@"productpicture", nil)];
                //break;
                
            }
            
            if ([[MenuItems1 objectAtIndex:indexPath.row] isEqualToString:@"receipt_picture"]) {
                
                __block UIActivityIndicatorView *activityIndicator;
                __weak UIImageView *weakImageView = imgViewMenu;
                /*[weakImageView setImageWithURL:[NSURL URLWithString:conreceipt_picture] placeholderImage:[UIImage imageNamed:@"No-Image-icon.png"] options:SDWebImageProgressiveDownload progress:^(NSInteger receivedSize, NSInteger expectedSize)
                 {
                     if (!activityIndicator)
                     {
                         [weakImageView addSubview:activityIndicator = [UIActivityIndicatorView.alloc initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray]];
                         activityIndicator.center = weakImageView.center;
                         [activityIndicator startAnimating];
                     }
                 }
                                     completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType)
                 {
                     [activityIndicator removeFromSuperview];
                     activityIndicator = nil;
                 }]; */
                dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0), ^ {
                    [weakImageView sd_setImageWithURL:[NSURL URLWithString:conreceipt_picture]
                                     placeholderImage:[UIImage imageNamed:@"No-Image-icon.png"]
                                              options:SDWebImageProgressiveDownload
                                             progress:^(NSInteger receivedSize, NSInteger expectedSize, NSURL *targetURL) {
                                                 dispatch_async(dispatch_get_main_queue(), ^ {
                                                     if (!activityIndicator) {
                                                         
                                                         [weakImageView addSubview:activityIndicator = [UIActivityIndicatorView.alloc initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray]];
                                                         activityIndicator.center = weakImageView.center;
                                                         [activityIndicator startAnimating];
                                                     }
                                                 });
                                             }
                                            completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
                                                dispatch_async(dispatch_get_main_queue(), ^ {
                                                    //[MBProgressHUD hideHUDForView:self.view animated:YES];
                                                    [activityIndicator removeFromSuperview];
                                                    activityIndicator = nil;
                                                });
                                            }];
                });
                
                imgViewMenu.contentMode  = UIViewContentModeScaleAspectFill;
                [imgViewMenu setClipsToBounds:YES];
                
                menutext.text = [NSString stringWithFormat:NSLocalizedString(@"receiptpicture", nil)];
                //break;
                
            }
            
            if ([[MenuItems1 objectAtIndex:indexPath.row] isEqualToString:@"warrantycard_picture"]) {
                
                //NSLog(@"conwarrantycard_picture:: %@",conwarrantycard_picture);
                __block UIActivityIndicatorView *activityIndicator;
                __weak UIImageView *weakImageView = imgViewMenu;
                /*[weakImageView setImageWithURL:[NSURL URLWithString:conwarrantycard_picture] placeholderImage:[UIImage imageNamed:@"No-Image-icon.png"] options:SDWebImageProgressiveDownload progress:^(NSInteger receivedSize, NSInteger expectedSize)
                 {
                     if (!activityIndicator)
                     {
                         [weakImageView addSubview:activityIndicator = [UIActivityIndicatorView.alloc initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray]];
                         activityIndicator.center = weakImageView.center;
                         [activityIndicator startAnimating];
                     }
                 }
                                     completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType)
                 {
                     [activityIndicator removeFromSuperview];
                     activityIndicator = nil;
                 }]; */
                dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0), ^ {
                    [weakImageView sd_setImageWithURL:[NSURL URLWithString:conwarrantycard_picture]
                                     placeholderImage:[UIImage imageNamed:@"No-Image-icon.png"]
                                              options:SDWebImageProgressiveDownload
                                             progress:^(NSInteger receivedSize, NSInteger expectedSize, NSURL *targetURL) {
                                                 dispatch_async(dispatch_get_main_queue(), ^ {
                                                     if (!activityIndicator) {
                                                         
                                                         [weakImageView addSubview:activityIndicator = [UIActivityIndicatorView.alloc initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray]];
                                                         activityIndicator.center = weakImageView.center;
                                                         [activityIndicator startAnimating];
                                                     }
                                                 });
                                             }
                                            completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
                                                dispatch_async(dispatch_get_main_queue(), ^ {
                                                    //[MBProgressHUD hideHUDForView:self.view animated:YES];
                                                    [activityIndicator removeFromSuperview];
                                                    activityIndicator = nil;
                                                });
                                            }];
                });
                
                imgViewMenu.contentMode  = UIViewContentModeScaleAspectFill;
                [imgViewMenu setClipsToBounds:YES];
                
                menutext.text = [NSString stringWithFormat:NSLocalizedString(@"warrantypicture", nil)];
                //break;
                
            }
            
            return cell;
            imgViewMenu =nil;
            menutext =nil;
        //}
    }
    return nil;
}

-(void) viewDidDisappear:(BOOL)animated {
    
    [super viewDidDisappear:animated];
    
    appdelegate.homeclicked = NO;
    [modalController dismissViewControllerAnimated:YES completion:nil];
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
