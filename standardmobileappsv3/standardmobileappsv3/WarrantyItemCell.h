//
//  MSSItemCell.h
//  echoplus
//
//  Created by Hazwan on 7/17/13.
//
//
#import <UIKit/UIKit.h>

@interface WarrantyItemCell : UITableViewCell

@property (unsafe_unretained, nonatomic) id controller;
@property (unsafe_unretained, nonatomic) UITableView *tableView;

@property (unsafe_unretained, nonatomic) IBOutlet UILabel *titleLabel;
@property (unsafe_unretained, nonatomic) IBOutlet UILabel *descLabel;
@property (unsafe_unretained, nonatomic) IBOutlet UIImageView *img;

@end
