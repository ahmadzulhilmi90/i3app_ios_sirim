//
//  WarrantyViewController.h
//  standardmobileappsv3
//
//  Created by M3Online on 6/19/14.
//  Copyright (c) 2014 M3Online. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreLocation/CoreLocation.h>
#import <SDWebImage/UIButton+WebCache.h>
#import <SDWebImage/UIImageView+WebCache.h>

@interface WarrantyViewController : UIViewController <UITableViewDelegate, UITableViewDataSource, UITextFieldDelegate, UIViewControllerTransitioningDelegate>

@property (strong, nonatomic) IBOutlet UIView *view_login;
@property (strong, nonatomic) IBOutlet UIButton *button_add;
@property (strong, nonatomic) IBOutlet UIButton *button_expiring;
@property (strong, nonatomic) IBOutlet UIButton *button_search;

@property (strong, nonatomic) IBOutlet UIView *view_warrantyList;
@property (strong, nonatomic) NSMutableArray *arrayWarrantyList;
@property (strong, nonatomic) IBOutlet UITableView *table_warrantyList;
@property (strong, nonatomic) NSDictionary *dicObject;

@property (strong, nonatomic) IBOutlet UIView *view_search;
@property (strong, nonatomic) IBOutlet UITextField *txt_search;
@property (strong, nonatomic) IBOutlet UIButton *button_searchDetail;

@property (strong, nonatomic) NSString *currentRequestMenu;

@property (strong, nonatomic) NSString *cont_title;

- (IBAction)onWarrantyListClicked:(id)sender;
- (IBAction)onAddClicked:(id)sender;
- (IBAction)onSearchClicked:(id)sender;
- (IBAction)onSearchDetailClicked:(id)sender;

@end
