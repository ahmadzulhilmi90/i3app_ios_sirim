//
//  WarrantyViewController.m
//  standardmobileappsv3
//
//  Created by M3Online on 6/19/14.
//  Copyright (c) 2014 M3Online. All rights reserved.
//

#import "WarrantyViewController.h"
#import "SettingViewController.h"
#import "AppDelegate.h"
#import "WarrantyAddViewController.h"
#import "WarrantyDetailsViewController.h"
#import "LoadingView.h"
#import "NSString+HTML.h"
#import <sqlite3.h>
#import "TxtFiles.h"
#import "SBJson.h"
#import <SystemConfiguration/SystemConfiguration.h>
#import "RootViewController.h"
#import "ViewController.h"
#import "InitialSlidingViewController.h"
#import "BTSlideInteractor.h"
#import "Reachability.h"

@interface WarrantyViewController ()
@property (strong, nonatomic) BTSlideInteractor *interactor;
@end

float tabButtonHeight_iphone = 45.0f;
float tabButtonHeight_ipad = 65.0f;

InitialSlidingViewController *modalController;

AppDelegate *appdelegate;
__strong LoadingView *loadingView;
__strong NSString *currentRequest;
//__strong NSMutableData *receivedData;
__strong NSString *cont_id;

@implementation WarrantyViewController
UIAlertView *alert;
UIActivityIndicatorView *indicator;
@synthesize button_add, button_expiring, button_search;
@synthesize view_login, view_warrantyList, arrayWarrantyList, table_warrantyList, cont_title;
@synthesize view_search, txt_search, button_searchDetail, dicObject, currentRequestMenu;

- (UIColor *) colorWithHexString:(NSString *)hexString {
    NSString *colorString = [[hexString stringByReplacingOccurrencesOfString: @"#" withString: @""] uppercaseString];
    CGFloat alpha, red, blue, green;
    alpha = 1.0f;
    red   = [self colorComponentFrom: colorString start: 0 length: 2];
    green = [self colorComponentFrom: colorString start: 2 length: 2];
    blue  = [self colorComponentFrom: colorString start: 4 length: 2];
    return [UIColor colorWithRed: red green: green blue: blue alpha: alpha];
}

- (CGFloat) colorComponentFrom:(NSString *)string start:(NSUInteger)start length:(NSUInteger)length {
    NSString *substring = [string substringWithRange: NSMakeRange(start, length)];
    NSString *fullHex = length == 2 ? substring : [NSString stringWithFormat: @"%@%@", substring, substring];
    unsigned hexComponent;
    [[NSScanner scannerWithString: fullHex] scanHexInt: &hexComponent];
    return hexComponent / 255.0;
}

-(id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super initWithCoder:aDecoder];
    if(self) {
        //_navDelegate = [SCNavControllerDelegate new];
        //self.delegate = _navDelegate;
    }
    return self;
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (BOOL) isConnectionAvailable
{
    Reachability *networkReachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus networkStatus = [networkReachability currentReachabilityStatus];
    if (networkStatus == NotReachable) {
        //NSLog(@"There IS NO internet connection");
        return FALSE;
    } else {
        //NSLog(@"There IS internet connection");
        return TRUE;
    }
}

- (void)startupAnimationDoneIndicatorLoadPage:(NSString *)animationID finished:(NSNumber *)finished context:(void *)context
{
    [self loadAllData];
    [self processdata:@""];
    [self dissmissIndicator];
    alert =nil;
}

- (void) loadAllData {
    
    if ([self isConnectionAvailable])
    {
    
        NSString * timeStampValueSync = [NSString stringWithFormat:@"%ld", (long)[[NSDate date] timeIntervalSince1970]];
        
        sqlite3 *database;
        // Open the database from the users filessytem
        if(sqlite3_open([appdelegate.databasePath UTF8String], &database) == SQLITE_OK) {
            
            NSString *urlAddress11 = [[NSString alloc] initWithFormat:@"%@warranty.php?db=%@&userid=%@&udid=%@&cuid=%@&device=ios&sc=%@", appdelegate.user_apipath, appdelegate.db, appdelegate.user, appdelegate.uuid, appdelegate.customer_id, appdelegate.cs];
            currentRequestMenu = @"warranty";
            //NSLog(@"urlAddress11: %@", urlAddress11);
            [self connectToServer:urlAddress11];
            
            TxtFiles *readFile11 = [[TxtFiles alloc] init];
            NSString *ReadData11 = [readFile11 readFromFile:readFile11.setFilenamewarranty];
            
            SBJsonParser *jsonParser11 = [SBJsonParser new];
            NSDictionary *jsonData11 = (NSDictionary *) [jsonParser11 objectWithString:ReadData11 error:nil];
            
            //NSLog(@"jsonData6: %@", jsonData6);
            
            NSMutableDictionary *dictionary12 = [jsonData11 mutableCopy];
            //NSLog(@"warranty:: %@", dictionary12);
            for(NSMutableDictionary *row in dictionary12){
                NSString *war_id = [row objectForKey:@"id"];
                NSString *war_user_id = [row objectForKey:@"user_id"];
                NSString *war_customer_id = [row objectForKey:@"customer_id"];
                NSString *war_unregister_id = [row objectForKey:@"unregister_id"];
                
                NSString *war_title;
                if([row objectForKey:@"title"] == (NSString*)[NSNull null]) {
                    war_title = @"";
                } else {
                    war_title = [[row objectForKey:@"title"] stringByReplacingOccurrencesOfString:@"'" withString:@"''"];
                }
                
                NSString *war_dealer;
                if([row objectForKey:@"dealer"] == (NSString*)[NSNull null]) {
                    war_dealer = @"";
                } else {
                    war_dealer = [[row objectForKey:@"dealer"] stringByReplacingOccurrencesOfString:@"'" withString:@"''"];
                }
                
                NSString *war_serialnum;
                if([row objectForKey:@"serial_number"] == (NSString*)[NSNull null]) {
                    war_serialnum = @"";
                } else {
                    war_serialnum = [[row objectForKey:@"serial_number"] stringByReplacingOccurrencesOfString:@"'" withString:@"''"];
                }
                
                NSString *war_modelnum;
                if([row objectForKey:@"model_number"] == (NSString*)[NSNull null]) {
                    war_modelnum = @"";
                } else {
                    war_modelnum = [[row objectForKey:@"model_number"] stringByReplacingOccurrencesOfString:@"'" withString:@"''"];
                }
                
                NSString *war_purchasedate;
                if([row objectForKey:@"purchase_date"] == (NSString*)[NSNull null]) {
                    war_purchasedate = @"";
                } else {
                    war_purchasedate = [[row objectForKey:@"purchase_date"] stringByReplacingOccurrencesOfString:@"'" withString:@"''"];
                }
                
                NSString *war_period;
                if([row objectForKey:@"warranty_period"] == (NSString*)[NSNull null]) {
                    war_period = @"";
                } else {
                    war_period = [[row objectForKey:@"warranty_period"] stringByReplacingOccurrencesOfString:@"'" withString:@"''"];
                }
                
                NSString *war_icon;
                if([row objectForKey:@"icon"] == (NSString*)[NSNull null]) {
                    war_icon = @"";
                } else {
                    war_icon = [[row objectForKey:@"icon"] stringByReplacingOccurrencesOfString:@"'" withString:@"''"];
                }
                
                NSString *war_imagproduct;
                if([row objectForKey:@"img_product"] == (NSString*)[NSNull null]) {
                    war_imagproduct = @"";
                } else {
                    war_imagproduct = [[row objectForKey:@"img_product"] stringByReplacingOccurrencesOfString:@"'" withString:@"''"];
                }
                
                NSString *war_imgreceipt;
                if([row objectForKey:@"img_receipt"] == (NSString*)[NSNull null]) {
                    war_imgreceipt = @"";
                } else {
                    war_imgreceipt = [[row objectForKey:@"img_receipt"] stringByReplacingOccurrencesOfString:@"'" withString:@"''"];
                }
                
                NSString *war_imgwar;
                if([row objectForKey:@"img_warranty"] == (NSString*)[NSNull null]) {
                    war_imgwar = @"";
                } else {
                    war_imgwar = [[row objectForKey:@"img_warranty"] stringByReplacingOccurrencesOfString:@"'" withString:@"''"];
                }
                
                NSString *war_comments;
                if([row objectForKey:@"comments"] == (NSString*)[NSNull null]) {
                    war_comments = @"";
                } else {
                    war_comments = [[row objectForKey:@"comments"] stringByReplacingOccurrencesOfString:@"'" withString:@"''"];
                }
                
                NSString *war_dateinserted;
                if([row objectForKey:@"date_inserted"] == (NSString*)[NSNull null]) {
                    war_dateinserted = @"";
                } else {
                    war_dateinserted = [[row objectForKey:@"date_inserted"] stringByReplacingOccurrencesOfString:@"'" withString:@"''"];
                }
                
                NSString *war_dateupdated;
                if([row objectForKey:@"date_updated"] == (NSString*)[NSNull null]) {
                    war_dateupdated = @"";
                } else {
                    war_dateupdated = [[row objectForKey:@"date_updated"] stringByReplacingOccurrencesOfString:@"'" withString:@"''"];
                }
                
                NSString *war_status;
                if([row objectForKey:@"status"] == (NSString*)[NSNull null]) {
                    war_status = @"";
                } else {
                    war_status = [[row objectForKey:@"status"] stringByReplacingOccurrencesOfString:@"'" withString:@"''"];
                }
                
                NSString *war_brand;
                if([row objectForKey:@"brand"] == (NSString*)[NSNull null]) {
                    war_brand = @"";
                } else {
                    war_brand = [[row objectForKey:@"brand"] stringByReplacingOccurrencesOfString:@"'" withString:@"''"];
                }
                
                NSString *war_category;
                if([row objectForKey:@"category"] == (NSString*)[NSNull null]) {
                    war_category = @"";
                } else {
                    war_category = [[row objectForKey:@"category"] stringByReplacingOccurrencesOfString:@"'" withString:@"''"];
                }
                
                int rows=0;
                NSString *temp = [[NSString alloc] initWithFormat:@"SELECT * FROM warranty WHERE id=%@ ",war_id];
                //NSLog(@"temp:: %@", temp);
                const char *sqlStatement = [temp UTF8String];
                sqlite3_stmt *compiledStatement;
                if(sqlite3_prepare_v2(database, sqlStatement, -1, &compiledStatement, NULL) == SQLITE_OK) {
                    // Loop through the results and add them to the feeds array
                    
                    if (sqlite3_step(compiledStatement) == SQLITE_ERROR) {
                        NSAssert1(0,@"Error when counting rows  %s",sqlite3_errmsg(database));
                    } else {
                        rows = sqlite3_column_int(compiledStatement, 0);
                        //NSLog(@"SQLite Rows: %i", rows);
                    }
                    
                    //while(sqlite3_step(compiledStatement) == SQLITE_ROW) {
                    if (rows > 0) {
                        //NSLog(@"database:::::::update conferences");
                        static sqlite3_stmt *compiledupdateStatementwar;
                        sqlite3_exec(database, [[NSString stringWithFormat:@"update warranty SET user_id='%@', customer_id='%@', unregister_id='%@', title='%@', dealer='%@', serial_number='%@', model_number='%@', purchase_date='%@', warranty_period='%@', icon='%@', img_product='%@', img_receipt='%@', img_warranty='%@', comments='%@', date_inserted='%@', date_updated='%@', status='%@', brand='%@', category='%@', lastsync_date='%@' where id='%@' ", war_user_id, war_customer_id, war_unregister_id, war_title, war_dealer, war_serialnum, war_modelnum, war_purchasedate, war_period, war_icon, war_imagproduct, war_imgreceipt, war_imgwar, war_comments, war_dateinserted, war_dateupdated, war_status, war_brand, war_category, timeStampValueSync, war_id] UTF8String], NULL, NULL, NULL);
                        sqlite3_finalize(compiledupdateStatementwar);
                    } else {
                        //NSLog(@"database:::::::insert into ibeacon_device");
                        static sqlite3_stmt *compiledinsertStatementwar;
                        sqlite3_exec(database, [[NSString stringWithFormat:@"insert into warranty (id, user_id, customer_id, unregister_id, title, dealer, serial_number, model_number, purchase_date, warranty_period, icon, img_product, img_receipt, img_warranty, comments, date_inserted, date_updated, status, brand, category, lastsync_date) values ('%@', '%@', '%@', '%@', '%@', '%@', '%@', '%@', '%@', '%@', '%@', '%@', '%@', '%@', '%@', '%@', '%@', '%@', '%@', '%@', '%@')", war_id,war_user_id, war_customer_id, war_unregister_id, war_title, war_dealer, war_serialnum, war_modelnum, war_purchasedate, war_period, war_icon, war_imagproduct, war_imgreceipt, war_imgwar, war_comments, war_dateinserted, war_dateupdated, war_status, war_brand, war_category, timeStampValueSync] UTF8String], NULL, NULL, NULL);
                        sqlite3_finalize(compiledinsertStatementwar);
                    }
                }
                // Release the compiled statement from memory
                sqlite3_finalize(compiledStatement);
                
            }
            //NSLog(@"database:::::::Delete not synced posters");
            static sqlite3_stmt *compiledinsertStatement3k;
            sqlite3_exec(database, [[NSString stringWithFormat:@"Delete from warranty where lastsync_date<>%@", timeStampValueSync] UTF8String], NULL, NULL, NULL);
            sqlite3_finalize(compiledinsertStatement3k);
            
        }
    } else {
        [self displayAlert:NSLocalizedString(@"MSG_NO_INTERNET", nil)];
    }
}

- (void)viewDidAppear:(BOOL)animated {
    
    if (appdelegate.showloading) {
        alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"loading", nil) message:nil delegate:self cancelButtonTitle:nil otherButtonTitles: nil];
        
        //a simple activity indicator:
        UIActivityIndicatorView *activityIndicator = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
        activityIndicator.frame= CGRectMake(50, 10, 37, 37);
        activityIndicator.autoresizingMask = UIViewAutoresizingFlexibleTopMargin | UIViewAutoresizingFlexibleRightMargin | UIViewAutoresizingFlexibleBottomMargin | UIViewAutoresizingFlexibleLeftMargin;
        [activityIndicator startAnimating];
        
        //the magic line below,
        //we associate the activity indicator to the alert view: (addSubview is not used)
        [alert setValue:activityIndicator forKey:@"accessoryView"];
        
        [alert show];
    
        // Adjust the indicator so it is up a few pixels from the bottom of the alert
        [UIView beginAnimations:nil context:nil];
        [UIView setAnimationDuration:0.0];
        [UIView setAnimationDelay:0.0];
        [UIView setAnimationTransition:UIViewAnimationTransitionNone forView:alert cache:YES];
        [UIView setAnimationDelegate:self];
        [UIView setAnimationDidStopSelector:@selector(startupAnimationDoneIndicatorLoadPage:finished:context:)];
        alert.alpha = 0.5;
        [UIView commitAnimations];
    } else {
        [self loadAllData];
        [self processdata:@""];
    }

    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSString *hasLoggedInFB = [defaults objectForKey:@"hasLoggedInFB"];
    NSString *hasLoggedInMyApp = [defaults objectForKey:@"hasLoggedInMyApp"];
    
    if ([hasLoggedInFB isEqualToString:@"YES"] || [hasLoggedInMyApp isEqualToString:@"YES"])
    {
        if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone) {
            // For iPhone
            view_warrantyList.frame = CGRectMake(0, tabButtonHeight_iphone, self.view.frame.size.width, self.view.frame.size.height-tabButtonHeight_iphone);
        } else {
            view_warrantyList.frame = CGRectMake(0, tabButtonHeight_ipad, self.view.frame.size.width, self.view.frame.size.height-tabButtonHeight_ipad);
        }
        [self.view addSubview:view_warrantyList];
    }
    else
        [self.view addSubview:view_login];

}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    self.navigationItem.backBarButtonItem = [[UIBarButtonItem alloc]initWithTitle:@"" style:UIBarButtonItemStylePlain target:nil action:nil];
    
    appdelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    
    self.title = cont_title;
    appdelegate.homeclicked = NO;
    
    CGRect rect = CGRectMake(0,0,20,20);
    UIGraphicsBeginImageContext( rect.size );
    [[UIImage imageNamed:@"home.png"] drawInRect:rect];
    UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    UIBarButtonItem *myHomeButton = [[UIBarButtonItem alloc] initWithImage:image style:UIBarButtonItemStyleDone target:self action:@selector(myHomeAction:)];
    
    
    NSArray *myButtonArray = [[NSArray alloc] initWithObjects:myHomeButton, nil];
    
    self.navigationItem.rightBarButtonItems = myButtonArray;
    
    //arrayWarrantyList = [[NSMutableArray alloc]init];
    
    button_add.backgroundColor = [self colorWithHexString:appdelegate.themeColorCode];
    [button_add setTitle:NSLocalizedString(@"addnew", nil) forState:UIControlStateNormal];
    button_expiring.backgroundColor = [self colorWithHexString:appdelegate.themeColorCode];
    [button_expiring setTitle:NSLocalizedString(@"expiring", nil) forState:UIControlStateNormal];
    button_search.backgroundColor = [self colorWithHexString:appdelegate.themeColorCode];
    [button_search setTitle:NSLocalizedString(@"search", nil) forState:UIControlStateNormal];
    
    [button_searchDetail setTitle:NSLocalizedString(@"search", nil) forState:UIControlStateNormal];
    button_searchDetail.layer.cornerRadius=6;
    
    txt_search.delegate = self;
    
    arrayWarrantyList = [[NSMutableArray alloc] init];
    //[table_warrantyList setFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height-200)];
    
    [self processdata:@""];
}

//Slide Menu
- (void)willAnimateRotationToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation duration:(NSTimeInterval)duration
{
    //NSLog(@"view frame=%@ bounds=%@", NSStringFromCGRect(self.view.frame), NSStringFromCGRect(self.view.bounds));
}


- (void)showModalButtonWasTouched
{
    self.interactor = [[BTSlideInteractor alloc] init];
    self.interactor.presenting = YES;
    
    //BTModalViewController *modalController = [self.storyboard instantiateViewControllerWithIdentifier:@"ModalViewController"];
    modalController = [[InitialSlidingViewController alloc] init];
    modalController.modalPresentationStyle = UIModalPresentationCustom;
    modalController.transitioningDelegate = self.interactor;
    [self.navigationController presentViewController:modalController animated:YES completion:nil];
    //[self.navigationController popToViewController:modalController animated:NO];
    //[self.navigationController pushViewController:modalController animated:YES];
}
// End Slide Menu

-(void)myHomeAction:(UIBarButtonItem *)sender{
    
    appdelegate.nextPage = @"";
    
    if ([appdelegate.listOfVerMenu count]>0) {
        if (appdelegate.homeclicked==NO) {
            appdelegate.homeclicked = YES;
            [self showModalButtonWasTouched];
        } else {
            appdelegate.homeclicked = NO;
            [modalController dismissViewControllerAnimated:YES completion:nil];
        }
    } else {
        //AppDelegate *appdelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
        //NSLog(@"View Controllers : %@",[appdelegate.navController viewControllers]);
        NSMutableArray *navigationArray = [[NSMutableArray alloc] initWithArray: appdelegate.navController.viewControllers];
        
        for (UIViewController* viewController in appdelegate.navController.viewControllers) {
            
            //This if condition checks whether the viewController's class is MyGroupViewController
            // if true that means its the MyGroupViewController (which has been pushed at some point)
            if ([viewController isKindOfClass:[RootViewController class]] ) {
                
                // Here viewController is a reference of UIViewController base class of MyGroupViewController
                // but viewController holds MyGroupViewController  object so we can type cast it here
                //ThemeViewController *themeViewController = (ThemeViewController*)viewController;
                //[self.navigationController popToViewController:themeViewController animated:YES];
                [navigationArray removeObjectAtIndex: 0];
            }
        }
        appdelegate.navController.viewControllers = navigationArray;
        for (UIViewController* viewController in appdelegate.navController.viewControllers) {
            
            //This if condition checks whether the viewController's class is MyGroupViewController
            // if true that means its the MyGroupViewController (which has been pushed at some point)
            if ([viewController isKindOfClass:[ViewController class]] ) {
                
                // Here viewController is a reference of UIViewController base class of MyGroupViewController
                // but viewController holds MyGroupViewController  object so we can type cast it here
                ViewController *themeViewController = (ViewController*)viewController;
                [appdelegate.navController popToViewController:themeViewController animated:NO];
                //[self.navigationController popToViewController:viewController animated:YES];
            }
        }
        //NSLog(@"View Controllers111 : %@",appdelegate.navController.viewControllers);
    }
    
}

- (IBAction)onWarrantyListClicked:(id)sender {
    
    [self processdata:@"Expiring"];
    
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone) {
        // For iPhone
        view_warrantyList.frame = CGRectMake(0, tabButtonHeight_iphone, self.view.frame.size.width, self.view.frame.size.height-tabButtonHeight_iphone);
    } else {
        view_warrantyList.frame = CGRectMake(0, tabButtonHeight_ipad, self.view.frame.size.width, self.view.frame.size.height-tabButtonHeight_ipad);
    }
    [self.view addSubview:view_warrantyList];
    [view_search removeFromSuperview];
    
    /*currentRequest = @"warrantyList";
    NSString *urlAddress2 = [[NSString alloc] initWithFormat:@"%@warranty.php?db=116&userid=116&udid=123&cuid=0",appdelegate.user_apipath,appdelegate.user,appdelegate.customer_id,loyalty_id];
    [self connectToServer:urlAddress2]; */
}

- (IBAction)onAddClicked:(id)sender {
    NSString *nibFileName;
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone) {
        nibFileName = @"WarrantyAddViewController";
    } else {
        nibFileName = @"WarrantyAddViewController_ipad";
    }
    WarrantyAddViewController *warrantyAddViewController = [[WarrantyAddViewController alloc] initWithNibName:nibFileName bundle:nil];
    warrantyAddViewController.cont_title = cont_title;
    [appdelegate.navController pushViewController:warrantyAddViewController animated:NO];
}

- (IBAction)onSearchClicked:(id)sender {
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone) {
        // For iPhone
        view_search.frame = CGRectMake(0, tabButtonHeight_iphone, self.view.frame.size.width, self.view.frame.size.height-tabButtonHeight_iphone);
    } else {
        view_search.frame = CGRectMake(0, tabButtonHeight_ipad, self.view.frame.size.width, self.view.frame.size.height-tabButtonHeight_ipad);
    }
    [self.view addSubview:view_search];
    [view_warrantyList removeFromSuperview];
}

- (IBAction)onSearchDetailClicked:(id)sender {
    [self processdata:@"Search"];
    
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone) {
        // For iPhone
        view_warrantyList.frame = CGRectMake(0, tabButtonHeight_iphone+70, self.view.frame.size.width, self.view.frame.size.height-tabButtonHeight_iphone-70);
    } else {
        view_warrantyList.frame = CGRectMake(0, tabButtonHeight_ipad+70, self.view.frame.size.width, self.view.frame.size.height-tabButtonHeight_ipad-70);
    }
    [self.view addSubview:view_warrantyList];
    //[view_search removeFromSuperview];
    [txt_search resignFirstResponder];
}

#pragma mark UITableView Delegation
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [arrayWarrantyList count];
    //return 1;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    if(tableView == table_warrantyList) {
        if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone) {
            // For iPhone
            return 70;
        } else {
            return 110;
        }
    }
    
    return 50;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    //if (tableView == table_warrantyList) {
        static NSString *CellIdentifier = @"CellIdentifier";
        UITableViewCell *cell = [table_warrantyList dequeueReusableCellWithIdentifier:CellIdentifier];
        
        UIImageView *imgViewMenu;
        const NSInteger LEFT_TAG = 2000;
        UILabel *menutext;
        const NSInteger RIGHT_TAG = 2001;
        UILabel *date;
        const NSInteger RIGHT_TAG1 = 2002;
        UILabel *storename;
        const NSInteger RIGHT_TAG2 = 2003;
        //UILabel *expiring;
        //const NSInteger RIGHT_TAG3 = 2004;
        UILabel *expired;
        const NSInteger RIGHT_TAG4 = 2004;
        UILabel *verified;
        const NSInteger RIGHT_TAG5 = 2005;
        
        if (cell == nil)
        {
            
            cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
            //cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
            
            //if ([[dictionary objectForKey:@"object_type"] isEqualToString:@"img"]) {
            if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone) {
                // For iPhone
                imgViewMenu = [[UIImageView alloc] initWithFrame:CGRectMake(5, 5, 60, 60)];
            } else {
                imgViewMenu = [[UIImageView alloc] initWithFrame:CGRectMake(5, 5, 100, 100)];
            }
            imgViewMenu.tag = LEFT_TAG;
            [cell.contentView addSubview:imgViewMenu];
            
            //} else if ([[dictionary objectForKey:@"object_type"] isEqualToString:@"text"]) {
            
                if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone) {
                    // For iPhone
                    menutext = [[UILabel alloc] initWithFrame: CGRectMake(70, 5, table_warrantyList.frame.size.width-70-65, 20)];
                } else {
                    menutext = [[UILabel alloc] initWithFrame: CGRectMake(110, 5, table_warrantyList.frame.size.width-110-65, 33)];
                }
            menutext.tag = RIGHT_TAG;
            [menutext setFont:[UIFont boldSystemFontOfSize:16]];
            menutext.textColor = [UIColor grayColor];
            menutext.textAlignment = NSTextAlignmentLeft;
            menutext.backgroundColor = [UIColor clearColor];
            menutext.numberOfLines = 0;
            [cell.contentView addSubview:menutext];
            
            if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone) {
                // For iPhone
                date = [[UILabel alloc] initWithFrame: CGRectMake(70, 25, table_warrantyList.frame.size.width-70-65, 20)];
            } else {
                date = [[UILabel alloc] initWithFrame: CGRectMake(110, 38, table_warrantyList.frame.size.width-110-65, 33)];
            }
            date.tag = RIGHT_TAG1;
            [date setFont:[UIFont boldSystemFontOfSize:16]];
            date.textColor = [UIColor grayColor];
            date.textAlignment = NSTextAlignmentLeft;
            date.backgroundColor = [UIColor clearColor];
            date.numberOfLines = 0;
            [cell.contentView addSubview:date];
            
            if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone) {
                // For iPhone
                storename = [[UILabel alloc] initWithFrame: CGRectMake(70, 45, table_warrantyList.frame.size.width-70-65, 20)];
            } else {
                storename = [[UILabel alloc] initWithFrame: CGRectMake(110, 71, table_warrantyList.frame.size.width-110-65, 33)];
            }
            storename.tag = RIGHT_TAG2;
            [storename setFont:[UIFont boldSystemFontOfSize:16]];
            storename.textColor = [UIColor grayColor];
            storename.textAlignment = NSTextAlignmentLeft;
            storename.backgroundColor = [UIColor clearColor];
            storename.numberOfLines = 0;
            [cell.contentView addSubview:storename];
            
            /*if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone) {
                // For iPhone
                expiring = [[UILabel alloc] initWithFrame: CGRectMake(table_warrantyList.frame.size.width-65, 5, 60, 20)];
            } else {
                expiring = [[UILabel alloc] initWithFrame: CGRectMake(table_warrantyList.frame.size.width-65, 5, 60, 33)];
            }
            expiring.tag = RIGHT_TAG3;
            [expiring setFont:[UIFont systemFontOfSize:14]];
            expiring.textColor = [UIColor grayColor];
            expiring.textAlignment = NSTextAlignmentLeft;
            expiring.backgroundColor = [UIColor clearColor];
            expiring.numberOfLines = 0;
            [cell.contentView addSubview:expiring];
            //} */
            
            if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone) {
                // For iPhone
                expired = [[UILabel alloc] initWithFrame: CGRectMake(table_warrantyList.frame.size.width-65, 25, 60, 20)];
            } else {
                expired = [[UILabel alloc] initWithFrame: CGRectMake(table_warrantyList.frame.size.width-65, 38, 60, 33)];
            }
            expired.tag = RIGHT_TAG4;
            [expired setFont:[UIFont systemFontOfSize:14]];
            expired.textColor = [UIColor grayColor];
            expired.textAlignment = NSTextAlignmentLeft;
            expired.backgroundColor = [UIColor clearColor];
            expired.numberOfLines = 0;
            [cell.contentView addSubview:expired];
            
            if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone) {
                // For iPhone
                verified = [[UILabel alloc] initWithFrame: CGRectMake(table_warrantyList.frame.size.width-65, 45, 60, 20)];
            } else {
                verified = [[UILabel alloc] initWithFrame: CGRectMake(table_warrantyList.frame.size.width-65, 71, 60, 33)];
            }
            verified.tag = RIGHT_TAG5;
            [verified setFont:[UIFont systemFontOfSize:14]];
            verified.textColor = [UIColor grayColor];
            verified.textAlignment = NSTextAlignmentLeft;
            verified.backgroundColor = [UIColor clearColor];
            verified.numberOfLines = 0;
            [cell.contentView addSubview:verified];
            
        }
        else
        {
            //if ([[dictionary objectForKey:@"object_type"] isEqualToString:@"img"]) {
            imgViewMenu = (UIImageView *)[cell viewWithTag:LEFT_TAG];
            //} else if ([[dictionary objectForKey:@"object_type"] isEqualToString:@"text"]) {
            menutext = (UILabel *)[cell viewWithTag:RIGHT_TAG];
            date = (UILabel *)[cell viewWithTag:RIGHT_TAG1];
            storename = (UILabel *)[cell viewWithTag:RIGHT_TAG2];
            //expiring = (UILabel *)[cell viewWithTag:RIGHT_TAG3];
            expired = (UILabel *)[cell viewWithTag:RIGHT_TAG4];
            verified = (UILabel *)[cell viewWithTag:RIGHT_TAG5];
            //}
        }
        
        if([arrayWarrantyList count]>0)
        {
            NSDictionary *dictionary = [arrayWarrantyList objectAtIndex:indexPath.row];
            
            NSString *imgURL = [dictionary objectForKey:@"icon"];
            
            __block UIActivityIndicatorView *activityIndicator;
            __weak UIImageView *weakImageView = imgViewMenu;
            /*[weakImageView setImageWithURL:[NSURL URLWithString:imgURL] placeholderImage:[UIImage imageNamed:@"No-Image-icon.png"] options:SDWebImageProgressiveDownload progress:^(NSInteger receivedSize, NSInteger expectedSize)
             {
                 if (!activityIndicator)
                 {
                     [weakImageView addSubview:activityIndicator = [UIActivityIndicatorView.alloc initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray]];
                     activityIndicator.center = weakImageView.center;
                     [activityIndicator startAnimating];
                 }
             }
                                 completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType)
             {
                 [activityIndicator removeFromSuperview];
                 activityIndicator = nil;
             }]; */
            dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0), ^ {
                [weakImageView sd_setImageWithURL:[NSURL URLWithString:imgURL]
                                 placeholderImage:[UIImage imageNamed:@"No-Image-icon.png"]
                                          options:SDWebImageProgressiveDownload
                                         progress:^(NSInteger receivedSize, NSInteger expectedSize, NSURL *targetURL) {
                                             dispatch_async(dispatch_get_main_queue(), ^ {
                                                 if (!activityIndicator) {
                                                     
                                                     [weakImageView addSubview:activityIndicator = [UIActivityIndicatorView.alloc initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray]];
                                                     activityIndicator.center = weakImageView.center;
                                                     [activityIndicator startAnimating];
                                                 }
                                             });
                                         }
                                        completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
                                            dispatch_async(dispatch_get_main_queue(), ^ {
                                                //[MBProgressHUD hideHUDForView:self.view animated:YES];
                                                [activityIndicator removeFromSuperview];
                                                activityIndicator = nil;
                                            });
                                        }];
            });
            
            imgViewMenu.contentMode  = UIViewContentModeScaleAspectFill;
            [imgViewMenu setClipsToBounds:YES];
            
            menutext.text = [NSString stringWithFormat:@"%@", [dictionary objectForKey:@"title"]];
                date.text = [NSString stringWithFormat:@"%@", [dictionary objectForKey:@"purchase_date"]];
            storename .text = [NSString stringWithFormat:@"%@", [dictionary objectForKey:@"dealer"]];
            
            if (![[dictionary objectForKey:@"purchase_date"] isEqualToString:@""] && [[dictionary objectForKey:@"warranty_period"] intValue]>0) {
                    
                NSDate *today = [NSDate date];
                
                NSString *dateString = [NSString stringWithFormat:@"%@", [dictionary objectForKey:@"purchase_date"]];
                __strong NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
                // this is imporant - we set our input date format to match our input string
                // if format doesn't match you'll get nil from your string, so be careful
                [dateFormatter setDateFormat:@"yyyy-MM-dd"];
                NSDate *purchdate = [[NSDate alloc] init];
                // voila!
                purchdate = [dateFormatter dateFromString:dateString];
                
                int daysToAdd = 1;
                NSDate *newpurchdate = [purchdate dateByAddingTimeInterval:60*60*24*daysToAdd];
                
                __strong NSDateComponents *dateComponents = [[NSDateComponents alloc] init];
                [dateComponents setMonth:[[dictionary objectForKey:@"warranty_period"] intValue]];
                NSCalendar *calendar = [NSCalendar currentCalendar];
                NSDate *expiredDate = [calendar dateByAddingComponents:dateComponents toDate:newpurchdate options:0];
                
                __strong NSDateComponents *dateComponents1 = [[NSDateComponents alloc] init];
                [dateComponents1 setMonth:[[dictionary objectForKey:@"warranty_period"] intValue]-1];
                NSCalendar *calendar1 = [NSCalendar currentCalendar];
                NSDate *expiringDate = [calendar1 dateByAddingComponents:dateComponents1 toDate:newpurchdate options:0];
                
                //NSLog(@"today :: %@", today);
                //NSLog(@"%@expiredDate :: %@", [dictionary objectForKey:@"title"],expiredDate);
                //NSLog(@"%@expiringDate :: %@", [dictionary objectForKey:@"title"],expiringDate);
                if ([today compare:expiredDate] == NSOrderedDescending) {
                    expired.text = [NSString stringWithFormat:@"Expired"];
                    expired.backgroundColor = [UIColor redColor];
                } else if (([today compare:expiringDate] == NSOrderedDescending) && (([today compare:expiredDate] == NSOrderedAscending) || [today compare:expiredDate] == NSOrderedSame)) {
                    //"AND curdate() > DATE_ADD(purchase_date, INTERVAL (warranty_period-1) MONTH) AND curdate() < DATE_ADD(purchase_date, INTERVAL warranty_period MONTH) ";
                    expired.text = [NSString stringWithFormat:@"Expiring"];
                    expired.backgroundColor = [UIColor orangeColor];
                } else {
                    expired.text = @"";
                    expired.backgroundColor = [UIColor clearColor];
                }
            
            //}
            
            } else {
                expired.text = @"";
                expired.backgroundColor = [UIColor clearColor];
            }
            
            if ([[dictionary objectForKey:@"status"] isEqualToString:@"2"]) {
                verified.text = [NSString stringWithFormat:@"Verified"];
                verified.backgroundColor = [UIColor greenColor];
            } else {
                verified.text = @"";
                verified.backgroundColor = [UIColor clearColor];
            }
            
        }
    
        imgViewMenu =nil;
        menutext =nil;
        date =nil;
        storename =nil;
        expired =nil;
        //expiring =nil;
        verified =nil;
        CellIdentifier =nil;
    
        return cell;
    //}
    
    //return nil;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    //if (tableView == view_warrantyList)
    //{
        
        NSDictionary *dictionary = [arrayWarrantyList objectAtIndex:indexPath.row];
        
        cont_id = [dictionary objectForKey:@"id"];
        //cont_title = [dictionary objectForKey:@"title"];
        
        //ContentListViewController *f = [[ContentListViewController alloc] initWithNibName:@"ContentListViewController" bundle:[NSBundle mainBundle]];
        //[self.navigationController pushViewController:f animated:YES];
        if (appdelegate.showloading) {
            alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"loading", nil) message:nil delegate:self cancelButtonTitle:nil otherButtonTitles: nil];
            
            //a simple activity indicator:
            UIActivityIndicatorView *activityIndicator = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
            activityIndicator.frame= CGRectMake(50, 10, 37, 37);
            activityIndicator.autoresizingMask = UIViewAutoresizingFlexibleTopMargin | UIViewAutoresizingFlexibleRightMargin | UIViewAutoresizingFlexibleBottomMargin | UIViewAutoresizingFlexibleLeftMargin;
            [activityIndicator startAnimating];
            
            //the magic line below,
            //we associate the activity indicator to the alert view: (addSubview is not used)
            [alert setValue:activityIndicator forKey:@"accessoryView"];
            
            [alert show];
        
            // Adjust the indicator so it is up a few pixels from the bottom of the alert
            [UIView beginAnimations:nil context:nil];
            [UIView setAnimationDuration:0.0];
            [UIView setAnimationDelay:0.0];
            [UIView setAnimationTransition:UIViewAnimationTransitionNone forView:alert cache:YES];
            [UIView setAnimationDelegate:self];
            [UIView setAnimationDidStopSelector:@selector(startupAnimationDoneIndicator:finished:context:)];
            alert.alpha = 0.5;
            [UIView commitAnimations];
        } else {
            [self goNextPage];
        }
    
    //}
}

- (IBAction)login:(id)sender
{
    NSString *nibFileName;
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone) {
        nibFileName = @"SettingViewController";
    } else {
        nibFileName = @"SettingViewController_ipad";
    }
    SettingViewController *s = [[SettingViewController alloc] initWithNibName:nibFileName bundle:nil];
    appdelegate.shouldReturnToPrevPage = YES;
    //LoyaltyViewController *l = [[LoyaltyViewController alloc] initWithNibName:@"LoyaltyViewController" bundle:nil];
    appdelegate.originVC = self;
    [appdelegate.navController pushViewController:s animated:NO];
    
    [view_login removeFromSuperview];
}

-(void) processdata:(NSString *)type {
    
    //AppDelegate *appdelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    sqlite3 *database;
    
    if(sqlite3_open([appdelegate.databasePath UTF8String], &database) == SQLITE_OK) {
        // Setup the SQL Statement and compile it for faster access
        
        NSString *strsym = @"%";
        NSString *textsearch = [[NSString alloc] initWithFormat:@"%@%@%@", strsym, txt_search.text, strsym];
        
        NSString *theme_object;
        if ([type isEqualToString:@"Expiring"]) {
           theme_object = [[NSString alloc] initWithFormat:@"SELECT * from warranty where status > 0 and date() > date(purchase_date, '+' || (warranty_period-1) || ' month') AND date() < date(purchase_date, '+' || warranty_period || ' month') order by title"];
        } else if ([type isEqualToString:@"Search"]) {
            theme_object = [[NSString alloc] initWithFormat:@"SELECT * from warranty where status > 0 and (title like '%@' OR dealer like '%@' OR comments like '%@') order by title", textsearch, textsearch, textsearch];
        } else {
            theme_object = [[NSString alloc] initWithFormat:@"SELECT * from warranty where status > 0 order by title"];
        }
        
        [arrayWarrantyList removeAllObjects];
        //NSLog(@"theme_objectwarranty:: %@", theme_object);
        const char *sqlStatement1 = [theme_object UTF8String];
        sqlite3_stmt *compiledStatement1;
        if(sqlite3_prepare_v2(database, sqlStatement1, -1, &compiledStatement1, NULL) == SQLITE_OK) {
            
            while(sqlite3_step(compiledStatement1) == SQLITE_ROW) {
                
                NSString *aidobj = [NSString stringWithUTF8String:(char *)sqlite3_column_text(compiledStatement1, 0)];
                NSString *acustid = [NSString stringWithUTF8String:(char *)sqlite3_column_text(compiledStatement1, 2)];
                NSString *aunregid = [NSString stringWithUTF8String:(char *)sqlite3_column_text(compiledStatement1, 3)];
                NSString *atitle = [NSString stringWithUTF8String:(char *)sqlite3_column_text(compiledStatement1, 4)];
                NSString *adealer = [NSString stringWithUTF8String:(char *)sqlite3_column_text(compiledStatement1, 5)];
                NSString *aserialnum = [NSString stringWithUTF8String:(char *)sqlite3_column_text(compiledStatement1, 6)];
                NSString *amodelnum = [NSString stringWithUTF8String:(char *)sqlite3_column_text(compiledStatement1, 7)];
                NSString *apurchdate = [NSString stringWithUTF8String:(char *)sqlite3_column_text(compiledStatement1, 8)];
                NSString *awarranty_period = [NSString stringWithUTF8String:(char *)sqlite3_column_text(compiledStatement1, 9)];
                NSString *aicon = [NSString stringWithUTF8String:(char *)sqlite3_column_text(compiledStatement1, 10)];
                NSString *aimgprod = [NSString stringWithUTF8String:(char *)sqlite3_column_text(compiledStatement1, 11)];
                NSString *aimgreceipt = [NSString stringWithUTF8String:(char *)sqlite3_column_text(compiledStatement1, 12)];
                NSString *aimgwarranty = [NSString stringWithUTF8String:(char *)sqlite3_column_text(compiledStatement1, 13)];
                NSString *acomments = [NSString stringWithUTF8String:(char *)sqlite3_column_text(compiledStatement1, 14)];
                NSString *adateinsert = [NSString stringWithUTF8String:(char *)sqlite3_column_text(compiledStatement1, 15)];
                NSString *adateupd = [NSString stringWithUTF8String:(char *)sqlite3_column_text(compiledStatement1, 16)];
                NSString *astatus = [NSString stringWithUTF8String:(char *)sqlite3_column_text(compiledStatement1, 17)];
                NSString *abrand = [NSString stringWithUTF8String:(char *)sqlite3_column_text(compiledStatement1, 18)];
                NSString *acategory = [NSString stringWithUTF8String:(char *)sqlite3_column_text(compiledStatement1, 19)];
                
                atitle = [atitle stringByDecodingHTMLEntities];
                aserialnum = [aserialnum stringByDecodingHTMLEntities];
                amodelnum = [amodelnum stringByDecodingHTMLEntities];
                acomments = [acomments stringByDecodingHTMLEntities];
                abrand = [abrand stringByDecodingHTMLEntities];
                acategory = [acategory stringByDecodingHTMLEntities];
                atitle = [atitle stringByReplacingOccurrencesOfString:@"''" withString:@"'"];
                aserialnum = [aserialnum stringByReplacingOccurrencesOfString:@"''" withString:@"'"];
                amodelnum = [amodelnum stringByReplacingOccurrencesOfString:@"''" withString:@"'"];
                acomments = [acomments stringByReplacingOccurrencesOfString:@"''" withString:@"'"];
                abrand = [abrand stringByReplacingOccurrencesOfString:@"''" withString:@"'"];
                acategory = [acategory stringByReplacingOccurrencesOfString:@"''" withString:@"'"];
                
                dicObject = [NSDictionary dictionaryWithObjectsAndKeys:aidobj,@"id",acustid,@"customer_id",aunregid,@"unregister_id",atitle,@"title",adealer,@"dealer",aserialnum,@"serial_number",amodelnum,@"model_number",apurchdate,@"purchase_date",awarranty_period,@"warranty_period",aicon,@"icon",aimgprod,@"img_product",aimgreceipt,@"img_receipt",aimgwarranty,@"img_warranty",acomments,@"comments",adateinsert,@"date_inserted",adateupd,@"date_updated",astatus,@"status",abrand,@"brand",acategory,@"category",nil];
                [arrayWarrantyList addObject:dicObject];
                //NSLog(@"arrayWarrantyList: %@", arrayWarrantyList);
                
                aidobj =nil;
                acustid =nil;
                aunregid =nil;
                atitle =nil;
                adealer =nil;
                aserialnum =nil;
                amodelnum =nil;
                apurchdate =nil;
                awarranty_period =nil;
                aicon =nil;
                aimgprod =nil;
                aimgreceipt =nil;
                aimgwarranty =nil;
                acomments =nil;
                adateinsert =nil;
                adateupd =nil;
                astatus =nil;
                abrand =nil;
                acategory =nil;

            }
            
            [table_warrantyList reloadData];
        }
        // Release the compiled statement from memory
        sqlite3_finalize(compiledStatement1);
    }
    sqlite3_close(database);
    
}

-(void) dissmissIndicator {
    [alert dismissWithClickedButtonIndex:0 animated:NO];
}

-(void) goNextPage {
    NSString *nibFileName;
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone) {
        nibFileName = @"WarrantyDetailsViewController";
    } else {
        nibFileName = @"WarrantyDetailsViewController_ipad";
    }
    WarrantyDetailsViewController *warrantyDetailsViewController = [[WarrantyDetailsViewController alloc] initWithNibName:nibFileName bundle:nil];
    warrantyDetailsViewController.contId = cont_id;
    warrantyDetailsViewController.conttitle = cont_title;
    [appdelegate.navController pushViewController:warrantyDetailsViewController animated:NO];
}

- (void)startupAnimationDoneIndicator:(NSString *)animationID finished:(NSNumber *)finished context:(void *)context
{
    [self goNextPage];
    [self dissmissIndicator];
    alert =nil;
}

#pragma mark Start Connection

-(void) connectToServer:(NSString *) urlPath {
    
    //AppDelegate *appdelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    
    NSURLRequest *request = [NSURLRequest requestWithURL:[NSURL URLWithString:urlPath]];
    
    //NSError        *error = nil;
    //NSURLResponse  *responsedata = nil;
    /*NSData *data = [NSURLConnection sendSynchronousRequest:request
                                         returningResponse:&responsedata error:&error];
    //NSStringEncoding encoding = CFStringConvertEncodingToNSStringEncoding(kCFStringEncodingGB_18030_2000);
    NSString *escapedStr = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
    //NSString *escapedStr = [[NSString alloc] initWithData:data encoding:encoding]; */
    NSURLSession *session = [NSURLSession sharedSession];
    
    [[session dataTaskWithRequest:request completionHandler:^(NSData * _Nullable data, NSURLResponse * _Nullable response, NSError * _Nullable error) {
        
        if (data) {
            
            //if response is in string format
            NSString *escapedStr = [[NSString alloc]initWithData:data encoding:NSUTF8StringEncoding];
            NSString *response = [escapedStr stringByReplacingOccurrencesOfString:@"'" withString:@"''"];
            
            if ([response length]==0)
                response = @"[{}]";
            
            if (error == nil) {
                if ([currentRequestMenu isEqualToString:@"warranty"])
                {
                    //
                    TxtFiles *files = [[TxtFiles alloc] init];
                    
                    [files WriteToStringFile:[response mutableCopy] andsetFileName:files.setFilenamewarranty];
                }
            } else {
                [self displayAlert:NSLocalizedString(@"MSG_CONNECTION_ERROR", nil)];
            }
            
            //if response in json format then
            //id jsonObject = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:nil];
            
        }
        
    }]resume];
    
}

#pragma mark LoadingView
-(void) addLoadingView
{
	loadingView = [LoadingView loadingViewInView:self.view];
}

-(void) removeLoadingView
{
	[loadingView removeView];
}

#pragma mark displayAlert
-(void) displayAlert: (NSString *) stralert {
	NSString *str = [[NSString alloc] initWithFormat:@"%@", stralert];
	UIAlertView *alert = [[UIAlertView alloc] initWithTitle:appdelegate.iconname
													message:str
												   delegate:self
										  cancelButtonTitle:NSLocalizedString(@"done", nil)
										  otherButtonTitles:nil];
	
	[alert show];
	str =nil;
	alert =nil;
}

-(BOOL) textFieldShouldReturn:(UITextField *)textField{
    [textField resignFirstResponder];
    return YES;
}

-(void) viewDidDisappear:(BOOL)animated {
    
    [super viewDidDisappear:animated];
    
    appdelegate.homeclicked = NO;
    [modalController dismissViewControllerAnimated:YES completion:nil];
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
