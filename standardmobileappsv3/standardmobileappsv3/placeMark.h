//
//  placeMark.h
//  poi
//
//  Created by Wai Kheng Ng on 7/13/10.
//  Copyright 2010 M3 Online. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <MapKit/MapKit.h>

@interface placeMark : NSObject<MKAnnotation> {
	CLLocationCoordinate2D coordinate;
	NSString *title;
	NSString *subtitle;
	UIImage *a;
}
	
@property (nonatomic, readonly) CLLocationCoordinate2D coordinate;
@property (copy, nonatomic) NSString *title;
@property (copy, nonatomic) NSString *subtitle;

-(id)initWithCoordinate:(CLLocationCoordinate2D) coordinate;
@end
