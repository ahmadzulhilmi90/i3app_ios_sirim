//
//  placeMark.m
//  poi
//
//  Created by Wai Kheng Ng on 7/13/10.
//  Copyright 2010 M3 Online. All rights reserved.
//

#import "placeMark.h"

@implementation placeMark
@synthesize coordinate;
@synthesize title, subtitle;


-(id)initWithCoordinate:(CLLocationCoordinate2D) c{
	coordinate=c;
	NSLog(@"initWithCoordinate: %f, %f",c.latitude,c.longitude);
	return self;
}
@end
